import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import sys
import os

sys.path.insert(0, "../library/")
# sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library/")
# sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/nicholai_plotting_scripts/")

import processing_simulation as prosim
import nicholai_plot_helper as nplt
import processing_data as prodata
import processing_pd as propd


"""
This script is used to compare simulation and data for total charge reaching the photocathode.
"""

#PMT Paramters:
PMT_QE = .3 #Quantum efficiency of photocathode, 30% at ~350 nm peak (model: 9821B p1)
PMT_gain = 7e6 #Nominal gain is 7'000'000 at nominal A/lm (model: 9821B p1)
att_factor = 6.31 #used when attenuating the signal before the digitizer
q = 1.60217662E-19 #charge of a single electron


################################
# DETECTOR = NE213A
################################

#Importing GEANT4 data
path_sim = '/media/gheed/Seagate_Expansion_Drive1/data/simulation/'
df_pen_Cs137_pc =       prosim.loadData(path_sim + 'NE213/Cs137/pencilbeam/analysis_ej321pchar_h_sum_optphoton_photocathode_total.npz') #Data with no cuts
df_pen_Cs137_pc_CS_QE = prosim.loadData(path_sim + 'NE213/Cs137/pencilbeam/analysis_ej321pchar_h_sum_optphoton_photocathode_total_CS_QE.npz') #Data with Compton scattering and Quantum efficiency cut
df_iso_Cs137_pc =       prosim.loadData(path_sim + 'NE213/Cs137/isotropic/analysis_ej321pchar_h_sum_optphoton_photocathode_total.npz') #Data with Compton scattering cut
df_iso_Cs137_pc_CS_QE = prosim.loadData(path_sim + 'NE213/Cs137/isotropic/analysis_ej321pchar_h_sum_optphoton_photocathode_total_CS_QE.npz') #Data with Compton scattering and Quantum efficiency cut

#convering simulation charge to QDC values and correcting for PMT gain
df_pen_Cs137_pc.bin_centers         = prosim.chargeCalibration(df_pen_Cs137_pc.bin_centers * q * PMT_gain)
df_pen_Cs137_pc_CS_QE.bin_centers   = prosim.chargeCalibration(df_pen_Cs137_pc_CS_QE.bin_centers * q * PMT_gain)
df_iso_Cs137_pc.bin_centers         = prosim.chargeCalibration(df_iso_Cs137_pc.bin_centers * q * PMT_gain)
df_iso_Cs137_pc_CS_QE.bin_centers   = prosim.chargeCalibration(df_iso_Cs137_pc_CS_QE.bin_centers * q * PMT_gain)

#Importing real data (NE213A)
path_data = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop
df_Cs137_data = propd.load_parquet_merge(path_data, [1085, 1086, 1087, 1088, 1089, 1090, 1091, 1092, 1093, 1094]) #load and merge all separete data files

#Plotting the results (NE213A)
plt.figure(0)
plt.suptitle('NE213A: charge @ photocathode')
plt.subplot(2,1,1)
plt.title('Pencilbeam simulation')
plt.step(df_pen_Cs137_pc.bin_centers, df_pen_Cs137_pc.counts, lw = 3, label = 'Sim: Cs-137, no cuts, QDC calibrated')
plt.step(df_pen_Cs137_pc_CS_QE.bin_centers, df_pen_Cs137_pc_CS_QE.counts, lw = 3, label = 'Sim: Cs-137, CS+QE cuts, QDC calibrated')
plt.hist(df_Cs137_data.qdc_lg_ch1, bins=500, lw=2, histtype='step', label = 'Data: Cs-137')
plt.yscale('log')
plt.legend()
plt.ylabel('counts')
plt.xlim([-10e3, 150e3])
plt.subplot(2,1,2)
plt.title('Isotropic simulation')
plt.step(df_iso_Cs137_pc.bin_centers, df_iso_Cs137_pc.counts, lw = 3, label = 'Sim: Cs-137, no cuts, QDC calibrated')
plt.step(df_iso_Cs137_pc_CS_QE.bin_centers, df_iso_Cs137_pc_CS_QE.counts, lw = 3, label = 'Sim: Cs-137, CS+QE cuts, QDC calibrated')
plt.hist(df_Cs137_data.qdc_lg_ch1, bins=500, lw=2, histtype='step', label = 'Data: Cs-137')
plt.yscale('log')
plt.legend()
plt.ylabel('counts')
plt.xlabel('QDC [arb. units]')
plt.xlim([-10e3, 150e3])
plt.show()

################################
# DETECTOR = EJ321P
################################
# df_pen_Cs137_pc = prosim.loadData(path + 'Cs137/pencilbeam/analysis_ej321pchar_h_sum_optphoton_photocathode_total.npz') #Data with no cuts
# df_pen_Cs137_pc_CS_QE = prosim.loadData(path + 'Cs137/pencilbeam/analysis_ej321pchar_h_sum_optphoton_photocathode_total_CS_QE.npz') #Data with Compton scattering and Quantum efficiency cut
# df_iso_Cs137_pc = prosim.loadData(path + 'Cs137/isotropic/analysis_ej321pchar_h_sum_optphoton_photocathode_total_CS.npz') #Data with Compton scattering cut
# df_iso_Cs137_pc_CS_QE = prosim.loadData(path + 'Cs137/isotropic/analysis_ej321pchar_h_sum_optphoton_photocathode_total_CS_QE.npz') #Data with Compton scattering and Quantum efficiency cut
