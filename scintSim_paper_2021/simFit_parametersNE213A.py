"""
Fitting parameters used for fitting simulated data of NE213A "cup" detector.
The fitting parameters for each source is put into separeate methods which 
can be called and will return the fitted gaussian, including errors.
"""

import sys
import numpy as np
import processing_math as promath
import processing_simulation as prosim
import processing_data as prodata

# ::::::::::::::::::::::::::::
#            LG
# ::::::::::::::::::::::::::::

def Na22_511_LG(data, plot=False):
    # _____Na-22 (.511 MeV)Boundary conditions
    start = 100
    stop = 6000
    # y, x = np.histogram(data, bins=4096, range=[0,4096])
    # newData = prodata.getRandDist(x, y)
    counts, bin_edges = np.histogram(data, bins = 30, range=[0, stop*1.2])
    
    bin_centers = prodata.getBinCenters(bin_edges)
    Ef = 0.510999
    return prosim.calibrationFitBinned(counts, bin_centers, start, stop, Ef=Ef, plot=plot, compton=True)

def Cs137_662_LG(data, plot=False):
    #_____Cs-137 (0.6615 MeV)Boundary conditions
    start = 1000
    stop = 6500
    # y, x = np.histogram(data, bins=4096, range=[0,4096])
    # newData = prodata.getRandDist(x, y)
    counts, bin_edges = np.histogram(data, bins = 50, range=[0, stop*1.2])
    bin_centers = prodata.getBinCenters(bin_edges)
    Ef = 0.66166
    return prosim.calibrationFitBinned(counts, bin_centers, start, stop, Ef=Ef, plot=plot, compton=True)

def Na22_1275_LG(data, plot=False):
    #_____Na-22 (1.275 MeV)Boundary conditions
    start = 3000
    stop = 14500
    # y, x = np.histogram(data, bins=4096, range=[0,4096])
    # newData = prodata.getRandDist(x, y)
    counts, bin_edges = np.histogram(data, bins = 50, range=[0, stop*1.2])
    bin_centers = prodata.getBinCenters(bin_edges)
    Ef = 1.2745
    return prosim.calibrationFitBinned(counts, bin_centers, start, stop, Ef=Ef, plot=plot, compton=True)

def Co60_1173_LG(data, plot=False):
    #_____Co-60 (1.173 MeV) Boundary conditions
    start = 3000
    stop = 14000
    # y, x = np.histogram(data, bins=4096, range=[0,4096])
    # newData = prodata.getRandDist(x, y)
    counts, bin_edges = np.histogram(data, bins = 50, range=[0, stop*1.2])
    bin_centers = prodata.getBinCenters(bin_edges)
    Ef = 1.173
    return prosim.calibrationFitBinned(counts, bin_centers, start, stop, Ef=Ef, plot=plot, compton=True)

def Co60_1332_LG(data, plot=False):
    #_____Co-60 (1.3325 MeV) Boundary conditions
    start = 2500
    stop = 14500
    # y, x = np.histogram(data, bins=4096, range=[0,4096])
    # newData = prodata.getRandDist(x, y)
    counts, bin_edges = np.histogram(data, bins = 40, range=[0, stop*1.2])
    bin_centers = prodata.getBinCenters(bin_edges)
    Ef = 1.3325
    return prosim.calibrationFitBinned(counts, bin_centers, start, stop, Ef=Ef, plot=plot, compton=True)

def PuBe_2230_LG(data, plot=False):
    #_____PuBe (2.230 MeV) Boundary conditions
    start = 12800
    stop = 25000
    # y, x = np.histogram(data, bins=4096, range=[0,4096])
    # newData = prodata.getRandDist(x, y)
    counts, bin_edges = np.histogram(data, bins = 64, range=[0, stop*1.2])
    bin_centers = prodata.getBinCenters(bin_edges)
    Ef = 2.22325
    binRange = [start, stop]
    return prosim.calibrationFitBinned(counts, bin_centers, start, stop, Ef=Ef, plot=plot, compton=True)

def PuBe_2615_LG(data, plot=False):
    #_____Pb-208 (2.615 MeV) Boundary conditions
    start = 15000
    stop = 32000
    # y, x = np.histogram(data, bins=4096, range=[0,4096])
    # newData = prodata.getRandDist(x, y)
    counts, bin_edges = np.histogram(data, bins = 64, range=[0, stop*1.2])
    bin_centers = prodata.getBinCenters(bin_edges)
    Ef = 2.614533
    return prosim.calibrationFitBinned(counts, bin_centers, start, stop, Ef=Ef, plot=plot, compton=True)


def PuBe_4439_LG(data, plot=False):
    #_____PuBe (4.439 MeV) Boundary conditions
    start = 20000
    stop = 52000
    # y, x = np.histogram(data, bins=4096, range=[0,4096])
    # newData = prodata.getRandDist(x, y)
    counts, bin_edges = np.histogram(data, bins = 50, range=[0, stop*1.2])
    bin_centers = prodata.getBinCenters(bin_edges)
    Ef = 4.439
    return prosim.calibrationFitBinned(counts, bin_centers, start, stop, Ef=Ef, plot=plot, compton=True)


def PuBe_6130_LG(data, plot=False):
    #_____PuBe (6.130 MeV) Boundary conditions
    start = 44000
    stop = 70000
    # y, x = np.histogram(data, bins=4096, range=[0,4096])
    # newData = prodata.getRandDist(x, y)
    counts, bin_edges = np.histogram(data, bins = 30, range=[0, stop])
    bin_centers = prodata.getBinCenters(bin_edges)
    Ef = 6.128
    binEdges = list(range(start, stop, 2000))
    return prosim.calibrationFitBinned(counts, bin_centers, start*1.1, stop*1.1, Ef=Ef, plot=plot, compton=True)
    
def cosmic_LG(data, plot=False):
    #_____Cosmic muons Boundary conditions
    min = 98000
    max = 156000
    
    #Rebin data
    binCoeff = 4
    new_bin_centers, new_counts = prodata.reBinning(bin_centers, counts, binCoeff)
    binEdges = list(range(start, stop, 1000))
    #Calulate maximum energy dep based on stopping power
    distance = 9.4 #cm, travel lenght through the detector (vertical).
    Ef = prosim.muonStopPoly(3000)*distance #Get expected maximum deposited energy over a given distance.
    
    return prosim.calibrationFit(data, binEdges, Ef=Ef, plot=plot, compton=False)

# ::::::::::::::::::::::::::::
#            SG
# ::::::::::::::::::::::::::::

def Na22_511_SG(df, plot=False):
    # _____Na-22 (0.511 keV)Boundary conditions
    const_min = -np.inf
    mean_min  = -np.inf
    sigma_min = -np.inf
    const_max = np.inf
    mean_max  = 444
    sigma_max = np.inf
    fit_lim = [[const_min, mean_min, sigma_min],[const_max, mean_max, sigma_max]]
    min = 470
    max = 766
    Ef = 0.510999
    return promath.gaussFit(df, min, max, Ef, fit_lim, binFactor=1, plot=plot)

def Na22_1275_SG(df, plot=False):
    #_____Na-22 (1.275 keV)Boundary conditions
    const_min = -np.inf
    mean_min  = -np.inf
    sigma_min = -np.inf
    const_max = np.inf
    mean_max  = np.inf
    sigma_max = np.inf
    fit_lim = [[const_min, mean_min, sigma_min],[const_max, mean_max, sigma_max]]
    min = 1786
    max = 2485
    Ef = 1.2745
    return promath.gaussFit(df, min, max, Ef, fit_lim, binFactor=1, plot=plot)

def Co60_1333_SG(df, plot=False):
    #_____Co-60 (1.3325 MeV) Boundary conditions
    const_min = -np.inf
    mean_min  = -np.inf
    sigma_min = -np.inf
    const_max = np.inf
    mean_max  = np.inf
    sigma_max = np.inf
    fit_lim = [[const_min, mean_min, sigma_min],[const_max, mean_max, sigma_max]]
    min = 3125
    max = 876
    Ef = 1.3325
    return promath.gaussFit(df, min, max, Ef, fit_lim, binFactor=1, plot=plot)

def Cs137_662_SG(df, plot=False):
    #_____Cs-137 (0.6615 MeV)Boundary conditions
    const_min = -np.inf
    mean_min  = -np.inf
    sigma_min = -np.inf
    const_max = np.inf
    mean_max  = np.inf
    sigma_max = np.inf
    fit_lim = [[const_min, mean_min, sigma_min],[const_max, mean_max, sigma_max]]
    min = 338
    max = 485
    Ef = 0.66166
    return promath.gaussFit(df, min, max, Ef, fit_lim, binFactor=1, plot=plot)

def PuBe_2230_SG(df, plot=False):
    #_____PuBe (2.230 MeV) Boundary conditions
    const_min = -np.inf
    mean_min  = 900
    sigma_min = -np.inf
    const_max = np.inf
    mean_max  = np.inf
    sigma_max = np.inf
    fit_lim = [[const_min, mean_min, sigma_min],[const_max, mean_max, sigma_max]]
    min = 840
    max = 1092
    Ef = 2.22325
    return promath.gaussFit(df, min, max, Ef, fit_lim, binFactor=1, plot=plot)

def PuBe_4439_SG(df, plot=False):
    #_____PuBe (4.439 MeV) Boundary conditions
    const_min = -np.inf
    mean_min  = -np.inf
    sigma_min = -np.inf
    const_max = np.inf
    mean_max  = np.inf
    sigma_max = np.inf
    fit_lim = [[const_min, mean_min, sigma_min],[const_max, mean_max, sigma_max]]
    min = 2222
    max = 3277
    Ef = 4.439
    return promath.gaussFit(df, min, max, Ef, fit_lim, binFactor=1, plot=plot)
