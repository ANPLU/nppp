import sys
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from tqdm import tqdm

def optPhotonSum(df):
    """
    sums event-by-event data and returns new DataFrame with optical photon sum.
    """
    
    currentEvt = -1
    df = pd.DataFrame()
    df['optPhotonSum'] = 0
    df['optPhotonSumCompton'] = 0
    df['xLoc'] = 0
    df['yLoc'] = 0
    df['zLoc'] = 0
    df['optPhotonParentStartingEnergy'] = 0

    for i in tqdm(range(len(df.optPhotonSum))):
        if (currentEvt != df.evtNum[i] and currentEvt >= 0): #find break between events
            df.loc[i] = [df.optPhotonSum[i-1], df.optPhotonSumCompton[i-1], df.xLoc[i-1],df.yLoc[i-1], df.zLoc[i-1], df.optPhotonParentStartingEnergy[i-1]] #save last value
        currentEvt = df.evtNum[i] #save current value
    return df

path_sim = '/media/gheed/Seagate_Expansion_Drive1/data/simulation/'
names = ['evtNum', 
                            'optPhotonSum', 
                            'optPhotonSumQE', 
                            'optPhotonSumCompton', 
                            'optPhotonSumComptonQE', 
                            'xLoc', 
                            'yLoc', 
                            'zLoc', 
                            'CsMin', 
                            'optPhotonParentID', 
                            'optPhotonParentCreatorProcess', 
                            'optPhotonParentStartingEnergy']

df = pd.read_csv(   path_sim + "NE213A/4_44MeV/pencilbeam/CSV_optphoton_data_sum.csv", names=names)

df =  pd.concat([df, pd.read_csv( path_sim + "NE213A/4_44MeV/pencilbeam/part2/CSV_optphoton_data_sum.csv", names=names)])
df =  pd.concat([df, pd.read_csv( path_sim + "NE213A/4_44MeV/pencilbeam/part3/CSV_optphoton_data_sum.csv", names=names)])
df =  pd.concat([df, pd.read_csv( path_sim + "NE213A/4_44MeV/pencilbeam/part4/CSV_optphoton_data_sum.csv", names=names)])
df =  pd.concat([df, pd.read_csv( path_sim + "NE213A/4_44MeV/pencilbeam/part5/CSV_optphoton_data_sum.csv", names=names)])
df =  pd.concat([df, pd.read_csv( path_sim + "NE213A/4_44MeV/pencilbeam/part6/CSV_optphoton_data_sum.csv", names=names)])
df =  pd.concat([df, pd.read_csv( path_sim + "NE213A/4_44MeV/pencilbeam/part7/CSV_optphoton_data_sum.csv", names=names)])
df =  pd.concat([df, pd.read_csv( path_sim + "NE213A/4_44MeV/pencilbeam/part8/CSV_optphoton_data_sum.csv", names=names)])
df =  pd.concat([df, pd.read_csv( path_sim + "NE213A/4_44MeV/pencilbeam/part9/CSV_optphoton_data_sum.csv", names=names)])
df =  pd.concat([df, pd.read_csv( path_sim + "NE213A/4_44MeV/pencilbeam/part10/CSV_optphoton_data_sum.csv", names=names)])
df =  pd.concat([df, pd.read_csv( path_sim + "NE213A/4_44MeV/pencilbeam/part11/CSV_optphoton_data_sum.csv", names=names)])
df =  pd.concat([df, pd.read_csv( path_sim + "NE213A/4_44MeV/pencilbeam/part12/CSV_optphoton_data_sum.csv", names=names)])
df =  pd.concat([df, pd.read_csv( path_sim + "NE213A/4_44MeV/pencilbeam/part13/CSV_optphoton_data_sum.csv", names=names)])
df =  pd.concat([df, pd.read_csv( path_sim + "NE213A/4_44MeV/pencilbeam/part14/CSV_optphoton_data_sum.csv", names=names)])
df =  pd.concat([df, pd.read_csv( path_sim + "NE213A/4_44MeV/pencilbeam/part15/CSV_optphoton_data_sum.csv", names=names)])

# df = optPhotonSum(df)

#############################
centralCut = [2700, 3500]
tailCut = [1, 1500]
#############################


fig = plt.figure()
ax = fig.add_subplot(111, projection = '3d')
ax.set_xlabel("x-axis")
ax.set_ylabel("y-axis [beam direction]")
ax.set_zlabel("z-axis")
ax.scatter(df['xLoc'], df['yLoc'], df['zLoc'], marker='o', alpha=.15)

fig = plt.figure()
ax = fig.add_subplot(111, projection = '3d')
ax.set_xlabel("x-axis")
ax.set_ylabel("y-axis [beam direction]")
ax.set_zlabel("z-axis")
query = f'optPhotonSumCompton<{tailCut[1]} and optPhotonSumCompton>{tailCut[0]}'
ax.scatter(df.query(f'{query}').xLoc, df.query(f'{query}').yLoc, df.query(f'{query}').zLoc, marker='o', alpha=.15)


#############################
# Histograms showing the x, y and z interactions for the tail cut

plt.figure()
plt.suptitle('tailed distribution cut')
query = f'optPhotonSumCompton<{tailCut[1]} and optPhotonSumCompton>{tailCut[0]}'

plt.subplot(3,1,1)
plt.hist(df.query(f'{query}').xLoc, bins=256, range=[-55, 55], histtype='step', label='x-axis')
plt.xlim([-55,55])
plt.vlines(-47, ymin=0, ymax =200, lw=2, color='cyan')
plt.vlines(47, ymin=0, ymax =200, lw=2, color='cyan')
plt.vlines(-50, ymin=0, ymax =200, lw=2, color='black')
plt.vlines(50, ymin=0, ymax =200, lw=2, color='black')
# plt.yscale('log')
plt.legend()

plt.subplot(3,1,2)
plt.hist(df.query(f'{query}').yLoc, bins=256, range=[443, 530], histtype='step', label='y-axis [beam direction]')
plt.xlim([443,530])
# plt.ylim([0,4])
plt.vlines(450, ymin=0, ymax =20, lw=2, color='black')
plt.vlines(453, ymin=0, ymax =20, lw=2, color='cyan')
plt.vlines(515, ymin=0, ymax =20, lw=2, color='cyan')
# plt.yscale('log')
plt.legend()

plt.subplot(3,1,3)
plt.hist(df.query(f'{query}').zLoc, bins=256, range=[-55, 55], histtype='step', label='z-axis')
plt.xlim([-55,55])
plt.vlines(-47, ymin=0, ymax =200, lw=2, color='cyan')
plt.vlines(47, ymin=0, ymax =200, lw=2, color='cyan')
plt.vlines(-50, ymin=0, ymax =200, lw=2, color='black')
plt.vlines(50, ymin=0, ymax =200, lw=2, color='black')
# plt.yscale('log')
plt.legend()


#############################
# Histograms showing the x, y and z interactions for the central cut
plt.figure()
plt.suptitle('central distribution cut')
query = f'optPhotonSumCompton<{centralCut[1]} and optPhotonSumCompton>{centralCut[0]}'

plt.subplot(3,1,1)
plt.hist(df.query(f'{query}').xLoc, bins=256, range=[-55, 55], histtype='step', label='x-axis')
plt.xlim([-55,55])
plt.vlines(-47, ymin=0, ymax =200, lw=2, color='cyan')
plt.vlines(47, ymin=0, ymax =200, lw=2, color='cyan')
plt.vlines(-50, ymin=0, ymax =200, lw=2, color='black')
plt.vlines(50, ymin=0, ymax =200, lw=2, color='black')
# plt.yscale('log')
plt.legend()

plt.subplot(3,1,2)
plt.hist(df.query(f'{query}').yLoc, bins=256, range=[443, 530], histtype='step', label='y-axis [beam direction]')
plt.xlim([443,530])
# plt.ylim([0,4])
plt.vlines(450, ymin=0, ymax =20, lw=2, color='black')
plt.vlines(453, ymin=0, ymax =20, lw=2, color='cyan')
plt.vlines(515, ymin=0, ymax =20, lw=2, color='cyan')
# plt.yscale('log')
plt.legend()

plt.subplot(3,1,3)
plt.hist(df.query(f'{query}').zLoc, bins=256, range=[-55, 55], histtype='step', label='z-axis')
plt.xlim([-55,55])
plt.vlines(-47, ymin=0, ymax =200, lw=2, color='cyan')
plt.vlines(47, ymin=0, ymax =200, lw=2, color='cyan')
plt.vlines(-50, ymin=0, ymax =200, lw=2, color='black')
plt.vlines(50, ymin=0, ymax =200, lw=2, color='black')
# plt.yscale('log')
plt.legend()


#########################
# Histogram showing where the cuts are
plt.figure()
plt.title('Full spectra')
plt.hist(df.query('optPhotonSumCompton>0').optPhotonSumCompton, bins=50, histtype='step', range=[1,4000])
plt.fill_betweenx(range(0, 350), centralCut[0], centralCut[1], color='green', alpha=.35, label='central cut')
plt.fill_betweenx(range(0, 350), tailCut[0], tailCut[1], color='red', alpha=.35, label='tail cut')
# plt.xlim([0,600])
plt.legend()
plt.xlabel('# optPhotons')
plt.ylabel('counts')
plt.show()


#########################
# Histogram showing different cuts
plt.figure()
plt.title('Full spectra')
plt.hist(df.query('optPhotonSumCompton>0').optPhotonSumCompton, bins=80, range=[1,1100], lw=2, histtype='step', label='full distribution')
plt.hist(df.query('yLoc>458 and yLoc<510').optPhotonSumCompton, bins=80, range=[1,1100], lw=2, histtype='step',label='central cuts')
plt.hist(df.query('(yLoc>=451 and yLoc<=455) or (yLoc>=510 and yLoc<=516)').optPhotonSumCompton, bins=80, range=[1,1100], lw=2, histtype='step', label='tail cuts')
# plt.fill_betweenx(range(0, 30), centralCut[0], centralCut[1], color='green', alpha=.5, label='central cut')
# plt.fill_betweenx(range(0, 30), tailCut[0], tailCut[1], color='red', alpha=.5, label='tail cut')
plt.xlim([0,600])
plt.legend()
plt.xlabel('# optPhotons')
plt.ylabel('counts')
plt.show()


