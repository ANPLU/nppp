"""
Fitting parameters for gaussEdgeFit.py script for NE213A "cup" detector.
The fitting parameters for each source is put into separeate methods which 
can be called and will return the fitted gaussian, including errors.
Methods used is: comptonEdgeFit()
"""

import sys
import numpy as np
import processing_math as promath
import processing_simulation as prosim
import processing_data as prodata
import matplotlib.pyplot as plt

# ::::::::::::::::::::::::::::
#            LG
# ::::::::::::::::::::::::::::

def Na22_511_LG(df, bg=0, plot=False):
    # _____Na-22 (.511 MeV)Boundary conditions
    const_min = -np.inf
    mean_min  = -np.inf
    sigma_min = -np.inf
    const_max = np.inf
    mean_max  = np.inf
    sigma_max = np.inf
    fit_lim = [[const_min, mean_min, sigma_min],[const_max, mean_max, sigma_max]]
    min = 2100
    max = 4500
    Ef = 0.510999
    bgSub = False
    bgStep = 20
    binFactor = 40

    #Binning data and subtracting background
    numBins = np.int(round((max-min)/binFactor))
    binRange = ([min, max])
    val, bin_edges = np.histogram(df, numBins, binRange)#create binned data of the data 
    bin_centers = prodata.getBinCenters(bin_edges)
    
    bgval, bgbin_edges = np.histogram(bg, numBins, binRange)#create binned data of the background
    val = val - bgval/10 #subtract the background counts from the data counts
    
    return promath.comptonEdgeFitBinned(bin_centers, val, Ef, bgSub=bgSub, bgStep=bgStep, fit_lim=fit_lim, plot=plot)

def Na22_1275_LG(df, bg=0, plot=False):
    #_____Na-22 (1.275 MeV)Boundary conditions
    const_min = -np.inf
    mean_min  = -np.inf
    sigma_min = -np.inf
    const_max = np.inf
    mean_max  = np.inf
    sigma_max = np.inf
    fit_lim = [[const_min, mean_min, sigma_min],[const_max, mean_max, sigma_max]]
    min = 9200
    max = 15000
    Ef = 1.2745
    bgSub = False
    bgStep = 20
    binFactor = 40
    #Binning data and subtracting background
    numBins = np.int(round((max-min)/binFactor))
    binRange = ([min, max])
    val, bin_edges = np.histogram(df, numBins, binRange)#create binned data of the data 
    bin_centers = prodata.getBinCenters(bin_edges)
    bgval, bgbin_edges = np.histogram(bg, numBins, binRange)#create binned data of the background
    val = val - bgval/10 #subtract the background counts from the data counts
    return promath.comptonEdgeFitBinned(bin_centers, val, Ef, bgSub=bgSub, bgStep=bgStep, fit_lim=fit_lim, plot=plot)

def Co60_1332_LG(df, bg=0, plot=False):
    #_____Co-60 (1.3325 MeV) Boundary conditions
    const_min = -np.inf
    mean_min  = -np.inf
    sigma_min = -np.inf
    const_max = np.inf
    mean_max  = np.inf
    sigma_max = np.inf
    fit_lim = [[const_min, mean_min, sigma_min],[const_max, mean_max, sigma_max]]
    min = 8800
    max = 16000
    Ef = 1.3325
    bgSub = False
    bgStep = 20
    binFactor = 50
    #Binning data and subtracting background
    numBins = np.int(round((max-min)/binFactor))
    binRange = ([min, max])
    val, bin_edges = np.histogram(df, numBins, binRange)#create binned data of the data 
    bin_centers = prodata.getBinCenters(bin_edges)
    bgval, bgbin_edges = np.histogram(bg, numBins, binRange)#create binned data of the background
    val = val - bgval/10 #subtract the background counts from the data counts
    return promath.comptonEdgeFitBinned(bin_centers, val, Ef, bgSub=bgSub, bgStep=bgStep, fit_lim=fit_lim, plot=plot)

def Cs137_662_LG(df, bg=0, plot=False):
    #_____Cs-137 (0.6615 MeV)Boundary conditions
    const_min = -np.inf
    mean_min  = -np.inf
    sigma_min = -np.inf
    const_max = np.inf
    mean_max  = np.inf
    sigma_max = np.inf
    fit_lim = [[const_min, mean_min, sigma_min],[const_max, mean_max, sigma_max]]
    min = 3500
    max = 6500
    Ef = 0.66166
    bgSub = False
    bgStep = 20
    binFactor = 20
    #Binning data and subtracting background
    numBins = np.int(round((max-min)/binFactor))
    binRange = ([min, max])
    val, bin_edges = np.histogram(df, numBins, binRange)#create binned data of the data 
    bin_centers = prodata.getBinCenters(bin_edges)
    bgval, bgbin_edges = np.histogram(bg, numBins, binRange)#create binned data of the background
    val = val - bgval/10 #subtract the background counts from the data counts
    return promath.comptonEdgeFitBinned(bin_centers, val, Ef, bgSub=bgSub, bgStep=bgStep, fit_lim=fit_lim, plot=plot)

def PuBe_2230_LG(df, bg=0, plot=False):
    #_____PuBe (2.230 MeV) Boundary conditions
    const_min = -np.inf
    mean_min  = -np.inf
    sigma_min = -np.inf
    const_max = np.inf
    mean_max  = np.inf
    sigma_max = np.inf
    fit_lim = [[const_min, mean_min, sigma_min],[const_max, mean_max, sigma_max]]
    min = 16900
    max = 29000
    Ef = 2.22325
    bgSub = False
    bgStep = 10
    binFactor = 20
    #Binning data and subtracting background
    numBins = np.int(round((max-min)/binFactor))
    binRange = ([min, max])
    val, bin_edges = np.histogram(df, numBins, binRange)#create binned data of the data 
    bin_centers = prodata.getBinCenters(bin_edges)
    bgval, bgbin_edges = np.histogram(bg, numBins, binRange)#create binned data of the background
    val = val - bgval/10 #subtract the background counts from the data counts
    return promath.comptonEdgeFitBinned(bin_centers, val, Ef, bgSub=bgSub, bgStep=bgStep, fit_lim=fit_lim, plot=plot)

def Th232_2615_LG(df, bg=0, plot=False):
    #_____Th238 (2.615 MeV) Boundary conditions
    const_min = -np.inf
    mean_min  = -np.inf
    sigma_min = -np.inf
    const_max = np.inf
    mean_max  = np.inf
    sigma_max = np.inf
    fit_lim = [[const_min, mean_min, sigma_min],[const_max, mean_max, sigma_max]]
    min = 23000
    max = 30000
    Ef = 2.614533
    bgSub = False
    bgStep = 10
    binFactor = 100
    #Binning data and subtracting background
    numBins = np.int(round((max-min)/binFactor))
    binRange = ([min, max])
    val, bin_edges = np.histogram(df, numBins, binRange)#create binned data of the data 
    bin_centers = prodata.getBinCenters(bin_edges)
    bgval, bgbin_edges = np.histogram(bg, numBins, binRange)#create binned data of the background
    val = val/10 - bgval/10 #subtract the background counts from the data counts
    return promath.comptonEdgeFitBinned(bin_centers, val, Ef, bgSub=bgSub, bgStep=bgStep, fit_lim=fit_lim, plot=plot)


def PuBe_4439_LG(df, bg=0, plot=False):
    #_____PuBe (4.439 MeV) Boundary conditions
    const_min = -np.inf
    mean_min  = -np.inf
    sigma_min = -np.inf
    const_max = np.inf
    mean_max  = np.inf
    sigma_max = np.inf
    fit_lim = [[const_min, mean_min, sigma_min],[const_max, mean_max, sigma_max]]
    min = 37000
    max = 53000
    Ef = 4.439
    bgSub = False
    bgStep = 10
    binFactor = 200
    #Binning data and subtracting background
    numBins = np.int(round((max-min)/binFactor))
    binRange = ([min, max])
    val, bin_edges = np.histogram(df, numBins, binRange)#create binned data of the data 
    bin_centers = prodata.getBinCenters(bin_edges)
    bgval, bgbin_edges = np.histogram(bg, numBins, binRange)#create binned data of the background
    val = val/3 - bgval/10 #subtract the background counts from the data counts
    return promath.comptonEdgeFitBinned(bin_centers, val, Ef, bgSub=bgSub, bgStep=bgStep, fit_lim=fit_lim, plot=plot)

def PuBe_6130_LG(df, bg=0, plot=False):
    #_____PuBe (6.130 MeV) Boundary conditions
    const_min = -np.inf
    mean_min  = -np.inf
    sigma_min = -np.inf
    const_max = np.inf
    mean_max  = np.inf
    sigma_max = np.inf
    fit_lim = [[const_min, mean_min, sigma_min],[const_max, mean_max, sigma_max]]
    min = 47700
    max = 59000
    Ef = 6.128
    bgSub = True
    bgStep = 10
    binFactor = 180
    #Binning data and subtracting background
    numBins = np.int(round((max-min)/binFactor))
    binRange = ([min, max])
    val, bin_edges = np.histogram(df, numBins, binRange)#create binned data of the data 
    bin_centers = prodata.getBinCenters(bin_edges)
    bgval, bgbin_edges = np.histogram(bg, numBins, binRange)#create binned data of the background
    val = val/3 - bgval/10 #subtract the background counts from the data counts
    return promath.comptonEdgeFitBinned(bin_centers, val, Ef, bgSub=bgSub, bgStep=bgStep, fit_lim=fit_lim, plot=plot)

def AmBe_4439_LG(df, bg=0, plot=False):
    #_____PuBe (4.439 MeV) Boundary conditions
    const_min = -np.inf
    mean_min  = -np.inf
    sigma_min = -np.inf
    const_max = np.inf
    mean_max  = np.inf
    sigma_max = np.inf
    fit_lim = [[const_min, mean_min, sigma_min],[const_max, mean_max, sigma_max]]
    min = 37000
    max = 53000
    Ef = 4.439
    bgSub = False
    bgStep = 10
    binFactor = 200
    #Binning data and subtracting background
    numBins = np.int(round((max-min)/binFactor))
    binRange = ([min, max])
    val, bin_edges = np.histogram(df, numBins, binRange)#create binned data of the data 
    bin_centers = prodata.getBinCenters(bin_edges)
    bgval, bgbin_edges = np.histogram(bg, numBins, binRange)#create binned data of the background
    val = val/3 - bgval/10 #subtract the background counts from the data counts
    return promath.comptonEdgeFitBinned(bin_centers, val, Ef, bgSub=bgSub, bgStep=bgStep, fit_lim=fit_lim, plot=plot)

def AmBe_6130_LG(df, bg=0, plot=False):
    #_____PuBe (6.130 MeV) Boundary conditions
    const_min = -np.inf
    mean_min  = -np.inf
    sigma_min = -np.inf
    const_max = np.inf
    mean_max  = np.inf
    sigma_max = np.inf
    fit_lim = [[const_min, mean_min, sigma_min],[const_max, mean_max, sigma_max]]
    min = 42000
    max = 54000
    Ef = 6.128
    bgSub = True
    bgStep = 10
    binFactor = 300
    #Binning data and subtracting background
    numBins = np.int(round((max-min)/binFactor))
    binRange = ([min, max])
    val, bin_edges = np.histogram(df, numBins, binRange)#create binned data of the data 
    bin_centers = prodata.getBinCenters(bin_edges)
    bgval, bgbin_edges = np.histogram(bg, numBins, binRange)#create binned data of the background
    val = val/3 - bgval/10 #subtract the background counts from the data counts
    return promath.comptonEdgeFitBinned(bin_centers, val, Ef, bgSub=bgSub, bgStep=bgStep, fit_lim=fit_lim, plot=plot)

def cosmic_LG(data, bg=0, plot=False):
    #_____PuBe (4.439 MeV) Boundary conditions
    const_min = -np.inf
    mean_min  = -np.inf
    sigma_min = -np.inf
    const_max = np.inf
    mean_max  = np.inf
    sigma_max = np.inf
    fit_lim = [[const_min, mean_min, sigma_min],[const_max, mean_max, sigma_max]]
    min = 100000
    max = 165000
    binEdges = list(range(min, max, 1000))

    #Calulate maximum energy dep based on stopping power
    distance = 9.4 #cm, travel lenght through the detector (vertical).
    Ef = prosim.muonStopPoly(3000)*distance #Get expected maximum deposited energy over a given distance.

    return prosim.calibrationFit(data, binEdges, Ef, plot=plot, compton=False)





# ::::::::::::::::::::::::::::
#            SG
# ::::::::::::::::::::::::::::

def Na22_511_SG(df, plot=False):
    # _____Na-22 (0.511 keV)Boundary conditions
    const_min = -np.inf
    mean_min  = -np.inf
    sigma_min = -np.inf
    const_max = np.inf
    mean_max  = 444
    sigma_max = np.inf
    fit_lim = [[const_min, mean_min, sigma_min],[const_max, mean_max, sigma_max]]
    min = 470
    max = 766
    Ef = 0.510999
    return promath.comptonEdgeFit(df, min, max, Ef, fit_lim, binFactor=1, plot=plot)

def Na22_1275_SG(df, plot=False):
    #_____Na-22 (1.275 keV)Boundary conditions
    const_min = -np.inf
    mean_min  = -np.inf
    sigma_min = -np.inf
    const_max = np.inf
    mean_max  = np.inf
    sigma_max = np.inf
    fit_lim = [[const_min, mean_min, sigma_min],[const_max, mean_max, sigma_max]]
    min = 1786
    max = 2485
    Ef = 1.2745
    return promath.comptonEdgeFit(df, min, max, Ef, fit_lim, binFactor=1, plot=plot)

def Co60_1333_SG(df, plot=False):
    #_____Co-60 (1.3325 MeV) Boundary conditions
    const_min = -np.inf
    mean_min  = -np.inf
    sigma_min = -np.inf
    const_max = np.inf
    mean_max  = np.inf
    sigma_max = np.inf
    fit_lim = [[const_min, mean_min, sigma_min],[const_max, mean_max, sigma_max]]
    min = 3125
    max = 876
    Ef = 1.3325
    return promath.comptonEdgeFit(df, min, max, Ef, fit_lim, binFactor=1, plot=plot)

def Cs137_662_SG(df, plot=False):
    #_____Cs-137 (0.6615 MeV)Boundary conditions
    const_min = -np.inf
    mean_min  = -np.inf
    sigma_min = -np.inf
    const_max = np.inf
    mean_max  = np.inf
    sigma_max = np.inf
    fit_lim = [[const_min, mean_min, sigma_min],[const_max, mean_max, sigma_max]]
    min = 338
    max = 485
    Ef = 0.66166
    return promath.comptonEdgeFit(df, min, max, Ef, fit_lim, binFactor=1, plot=plot)

def PuBe_2230_SG(df, plot=False):
    #_____PuBe (2.230 MeV) Boundary conditions
    const_min = -np.inf
    mean_min  = 900
    sigma_min = -np.inf
    const_max = np.inf
    mean_max  = np.inf
    sigma_max = np.inf
    fit_lim = [[const_min, mean_min, sigma_min],[const_max, mean_max, sigma_max]]
    min = 840
    max = 1092
    Ef = 2.22325
    return promath.comptonEdgeFit(df, min, max, Ef, fit_lim, binFactor=1, plot=plot)

def PuBe_4439_SG(df, plot=False):
    #_____PuBe (4.439 MeV) Boundary conditions
    const_min = -np.inf
    mean_min  = -np.inf
    sigma_min = -np.inf
    const_max = np.inf
    mean_max  = np.inf
    sigma_max = np.inf
    fit_lim = [[const_min, mean_min, sigma_min],[const_max, mean_max, sigma_max]]
    min = 2222
    max = 3277
    Ef = 4.439
    return promath.comptonEdgeFit(df, min, max, Ef, fit_lim, binFactor=1, plot=plot)
