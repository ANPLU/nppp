#!/usr/bin/env python3
import sys

# IMPORT FROM LIBRARY DIRECTORY
sys.path.insert(0, "../library/")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "sim_analysis/")
sys.path.insert(0, "data_analysis/")

import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
import pandas as pd
import pylandau
from scipy.optimize import curve_fit
from scipy.stats import moyal

import processing_utility as prout
import processing_math as promath
import processing_data as prodata
import processing_pd as propd
import processing_simulation as prosim

PMT_gain = 7e6 #Nominal gain is 7'000'000 at nominal A/lm (model: 9821B p1)
att_factor_A = 6.14102564 #used when attenuating the signal before the digitizer.
att_factor_cosmic_A = 13.09339408 # #attenuation factor used when taking cosmic muon data.
att_factor_B = 5.93679379 #used when attenuating the signal before the digitizer.
att_factor_cosmic_B = 12.13059163# #attenuation factor used when taking cosmic muon data.
q = 1.60217662E-19 #charge of a single electron
simScaleQDC_NE213A = 1

path_sim = '/media/gheed/Seagate_Expansion_Drive1/data/simulation/'
df_Cs137_662_sim_pen =      prosim.loadData(path_sim + 'NE213A/4_44MeV/pencilbeam/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE.npz') #
df_Cs137_662_sim_pen.counts += prosim.loadData(path_sim + 'NE213A/4_44MeV/pencilbeam/part2/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE.npz').counts #
df_Cs137_662_sim_pen.counts += prosim.loadData(path_sim + 'NE213A/4_44MeV/pencilbeam/part3/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE.npz').counts
df_Cs137_662_sim_pen.counts += prosim.loadData(path_sim + 'NE213A/4_44MeV/pencilbeam/part4/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE.npz').counts
df_Cs137_662_sim_pen.counts += prosim.loadData(path_sim + 'NE213A/4_44MeV/pencilbeam/part5/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE.npz').counts
df_Cs137_662_sim_pen.counts += prosim.loadData(path_sim + 'NE213A/4_44MeV/pencilbeam/part6/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE.npz').counts
df_Cs137_662_sim_pen.counts += prosim.loadData(path_sim + 'NE213A/4_44MeV/pencilbeam/part7/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE.npz').counts
df_Cs137_662_sim_pen.counts += prosim.loadData(path_sim + 'NE213A/4_44MeV/pencilbeam/part8/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE.npz').counts

df_Cs137_662_sim_pen.bin_centers =      prosim.chargeCalibration(df_Cs137_662_sim_pen.bin_centers   * q * PMT_gain * simScaleQDC_NE213A)

x, y = prodata.reBinning(df_Cs137_662_sim_pen.bin_centers, df_Cs137_662_sim_pen.counts, 8)
y = y[::-1] #Reverse the data
# plt.step(x,y)
# plt.show()








###TEST PARAMETERS FOR FIT


def landauFunc(x, A, mu, std):
    return moyal.pdf(x, loc=mu, scale=std)*A


A = 1000000
mu = 600000
std = 1000


# popt_landau, pcov_landau = curve_fit(landauFunc, x, y, p0 = [A, mu, std]) 


plt.title('AmBe 4440 keV')
plt.step(x[200:-1], y[200:-1], lw=2, label='simulation')
x_new = np.arange(400000,800000,1)
# plt.plot(x_new, landauFunc(x_new, popt_landau[0], popt_landau[1], popt_landau[2]), lw=2, label='landau fit')
popt_gauss, err_gauss = promath.gaussFit(x[200:-1], y[200:-1], 585260, 613600, True) 
plt.plot(x_new, promath.gaussFunc(x_new, popt_gauss[0], popt_gauss[1], popt_gauss[2]), lw=2, label=f'gauss fit')
popt, pcov = curve_fit(promath.expGaussFunc, x[200:-1], y[200:-1], [300,599000,12000, 100])
plt.plot(x_new, promath.expGaussFunc(x_new, popt[0], popt[1], popt[2], popt[3]), lw=2, label=f'gauss fit exp')
plt.legend()
# plt.yscale('log')ll
plt.xlim([400000,700000])
plt.show()



###
# plt.figure()
# plt.plot(x,y, label='Cs137, isotropic')
# plt.xlim([25000,0])
# plt.figure()
# plt.step(df_Cs137_662_sim_pen.bin_centers, df_Cs137_662_sim_pen.counts, label='Cs137, isotropic')
# plt.xlim([0, 25000])
# plt.show()

# mu = 0
# eta = 1
# sigma = 2
# A = 1

# y = np.arange(0)
# y2 = np.arange(0)
# x = np.arange(-5,20, .01)
# for i in x:
#     y = np.append(y, pylandau.get_langau(i, mu, eta, sigma))
#     y2 = np.append(y2, pylandau.get_langau(i, mu, eta, sigma-1))

# plt.plot(x,y,label="nr1")
# plt.plot(x,y2*100, label="nr2")
# plt.legend()
# plt.show()


#TEST

# from scipy.stats import moyal
# fig, ax = plt.subplots(1, 1)    
# mean, var, skew, kurt = moyal.stats(moments='mvsk')     
# x = np.linspace(moyal.ppf(0.01), moyal.ppf(0.99), 100)
# ax.plot(x, moyal.pdf(x), 'r-', lw=5, alpha=0.6, label='moyal pdf') 
# # plt.show()




# plt.plot(x, moyal.pdf(x, loc=0, scale=1), lw=2, label='1')
# plt.plot(x, moyal.pdf(x, loc=0, scale=1), lw=2, label='2')
# plt.plot(x, moyal.pdf(x, loc=0, scale=1), lw=2, label='3')
# plt.show()