"""
Fitting parameters for gaussEdgeFit.py script for EJ-231P "cup" detector.
The fitting parameters for each source is put into separeate methods which 
can be called and will return the fitted gaussian, including errors.
Methods used is: comptonEdgeFit()
"""

import sys
import numpy as np
import processing_math as promath

# ::::::::::::::::::::::::::::
#            LG
# ::::::::::::::::::::::::::::

def Na22_511_LG(df, plot=False):
    # _____Na-22 (0.511 keV)Boundary conditions
    const_min = -np.inf
    mean_min  = -np.inf
    sigma_min = -np.inf
    const_max = np.inf
    mean_max  = np.inf
    sigma_max = np.inf
    fit_lim = [[const_min, mean_min, sigma_min],[const_max, mean_max, sigma_max]]
    min = 738
    max = 1236
    Ef = .511
    return promath.comptonEdgeFit(df, min, max, Ef, fit_lim, plot=plot)

def Na22_1275_LG(df, plot=False):
    #_____Na-22 (1.275 keV)Boundary conditions
    const_min = -np.inf
    mean_min  = -np.inf
    sigma_min = -np.inf
    const_max = np.inf
    mean_max  = np.inf
    sigma_max = np.inf
    fit_lim = [[const_min, mean_min, sigma_min],[const_max, mean_max, sigma_max]]
    min = 2865
    max = 3660
    Ef = 1.275
    return promath.comptonEdgeFit(df, min, max, Ef, fit_lim, binFactor=1, plot=plot)

def Co60_1333_LG(df, plot=False):
    #_____Co-60 (1.3325 MeV) Boundary conditions
    const_min = -np.inf
    mean_min  = 3330
    sigma_min = -np.inf
    const_max = np.inf
    mean_max  = np.inf
    sigma_max = np.inf
    fit_lim = [[const_min, mean_min, sigma_min],[const_max, mean_max, sigma_max]]
    min = 3220
    max = 4616
    Ef = 1.3325
    return promath.comptonEdgeFit(df, min, max, Ef, fit_lim, binFactor=1, plot=plot)

def Cs137_662_LG(df, plot=False):
    #_____Cs-137 (0.6615 MeV)Boundary conditions
    const_min = -np.inf
    mean_min  = -np.inf
    sigma_min = -np.inf
    const_max = np.inf
    mean_max  = np.inf
    sigma_max = np.inf
    fit_lim = [[const_min, mean_min, sigma_min], [const_max, mean_max, sigma_max]]
    min = 440
    max = 600
    Ef = .6615
    return promath.comptonEdgeFit(df, min, max, Ef, fit_lim, plot=plot)

def PuBe_2230_LG(df, plot=False):
    #_____PuBe (2.230 MeV) Boundary conditions
    const_min = -np.inf
    mean_min  = -np.inf
    sigma_min = -np.inf
    const_max = np.inf
    mean_max  = np.inf
    sigma_max = np.inf
    fit_lim = [[const_min, mean_min, sigma_min],[const_max, mean_max, sigma_max]]
    min = 5411
    max = 6620
    Ef = 2.230
    return promath.comptonEdgeFit(df, min, max, Ef, fit_lim, binFactor=10, plot=plot)

def PuBe_4439_LG(df, plot=False):
    #_____PuBe (4.439 MeV) Boundary conditions
    const_min = -np.inf
    mean_min  = -np.inf
    sigma_min = -np.inf
    const_max = np.inf
    mean_max  = np.inf
    sigma_max = np.inf
    fit_lim = [[const_min, mean_min, sigma_min],[const_max, mean_max, sigma_max]]
    min = 11730
    max = 14625
    Ef = 4.439
    return promath.comptonEdgeFit(df, min, max, Ef, fit_lim, binFactor=1, plot=plot)
