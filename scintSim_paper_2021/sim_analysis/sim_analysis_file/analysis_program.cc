/*
http://patorjk.com/software/taag/#p=display&h=0&v=0&f=Calvin%20S&t=segment%20iterator%20%5Bedep%5D)
*/

#include "GriffAnaUtils/All.hh"
#include "Core/Units.hh"
#include "Utils/ArrayMath.hh"
#include "SimpleHists/HistCollection.hh"
#include <iostream>

using namespace std;

//Griff analysis. See https://confluence.esss.lu.se/display/DG/Griff for more info.

int QuantumEff(float wavelength){
  //Function which determines the quantum efficiency of the current photon wavelength.
  //Will return 0 or 1 based on the quantum efficiency.
  //used together with an optical photon counter, this will take QE of the PMT into acount.
  std::array<const float, 57> wl = {280.49968,284.16044,287.82715,289.66495,292.42462,296.08836,298.851,300.6992,302.54888,304.39856,305.34863,309.0094,309.95354,310.8947,313.65438,316.4096,319.17373,319.20787,322.88348,325.63425,328.39689,330.24657,333.90734,338.4781,343.0444,348.50586,356.68245,366.65825,376.63109,384.7869,392.02975,400.17962,410.13613,419.18264,429.12578,437.2623,443.57882,452.61345,459.83849,467.05313,476.08183,483.29647,491.41962,499.54278,510.37513,517.59274,526.61699,534.73865,543.75547,550.97606,557.28664,567.22385,575.35888,585.31241,600.70662,618.82487,626.07663};
  std::array<const float, 57> QE = {0.00000,0.01058,0.02334,0.03119,0.04395,0.05573,0.06948,0.08077,0.09255,0.10433,0.11857,0.12937,0.14164,0.15293,0.1657,0.17699,0.19122,0.20251,0.21822,0.22804,0.24178,0.25356,0.26436,0.27614,0.28645,0.29282,0.29723,0.29673,0.29524,0.29278,0.28835,0.28392,0.27703,0.26917,0.25786,0.24901,0.2382,0.22641,0.21609,0.20233,0.18857,0.17482,0.16155,0.14828,0.13108,0.11831,0.10308,0.08932,0.07163,0.05984,0.04707,0.0338,0.02446,0.01659,0.00822,0.00084,0.00000};
  float currentQE = 0.;
  for(auto& i: wl){
        // double randNum = rand()/double(RAND_MAX); //Sudo-random number between 0-1
        // return randNum;//
    if (wavelength < i){
        currentQE = QE.at(std::distance(std::begin(wl),&i)); QE.at(std::distance(std::begin(wl),&i));
        break; //exit loop
    }
  }
  double randNum = rand()/double(RAND_MAX);
  if (randNum <= currentQE){ //Generate psudo rand. number to determine if photons should be counted '1' or not '0'
    // printf("QE = %f, Random num = %f, RETURN 1\n", currentQE, randNum);
    return 1; //count this photon
  }
  // printf("QE = %f, Random num = %f, RETURN 0\n", currentQE, randNum);
  return 0; //do not count this photon
}

// float QuantumEff(float wavelength){
// //Function which determines the quantum efficiency of the current photon wavelength.
// //Will return 0 or 1 based on the quantum efficiency.
// //used together with an optical photon counter, this will take QE of the PMT into acount.
//     for(auto& i: wl){
//         if (wavelength < i){
//             return QE.at(std::distance(std::begin(wl),&i));
//         }
//                 double randomnum = rand()/double(RAND_MAX);
//         std::cout << i << ": " << std::distance(std::begin(wl),&i) << ": "<< randomnum << std::endl;
//     }
//     return 0;
// }




int main(int argc,char**argv) {

  //:::::::::::::MANDATORY PART (do not touch)::::::::::::::::
  //Open .griff file(s) specified on the command line:
  GriffDataReader dr(argc,argv); //dr is used to loop through the GRIFF file
  //Extract and dump meta-data:
  dr.setup() -> dump(); //prints (dumps) information to terminal about the generator and geometry of the simulation
  //Check (as an example) that the simulated geometry is of the correct type:
  if (dr.setup() -> geo().getName() != "G4GeoEJ321Pchar/GeoEJ321Pchar")
    return 1;
  //Book histograms (see https://confluence.esss.lu.se/display/DG/SimpleHists for more info):
  SimpleHists::HistCollection hc; //Create short-hand for SimpleHist
  //::::::::::::::::::::::::::::::::::::::::::::::::::::::


  //::::::::::::PLACE YOUR CODE FROM HERE::::::::::::::::::::::::::

/*
███████╗██╗██╗     ████████╗███████╗██████╗ ███████╗        ██╗    ██╗████████╗███████╗██████╗  █████╗ ████████╗ ██████╗ ██████╗ ███████╗
██╔════╝██║██║     ╚══██╔══╝██╔════╝██╔══██╗██╔════╝       ██╔╝    ██║╚══██╔══╝██╔════╝██╔══██╗██╔══██╗╚══██╔══╝██╔═══██╗██╔══██╗██╔════╝
█████╗  ██║██║        ██║   █████╗  ██████╔╝███████╗      ██╔╝     ██║   ██║   █████╗  ██████╔╝███████║   ██║   ██║   ██║██████╔╝███████╗
██╔══╝  ██║██║        ██║   ██╔══╝  ██╔══██╗╚════██║     ██╔╝      ██║   ██║   ██╔══╝  ██╔══██╗██╔══██║   ██║   ██║   ██║██╔══██╗╚════██║
██║     ██║███████╗   ██║   ███████╗██║  ██║███████║    ██╔╝       ██║   ██║   ███████╗██║  ██║██║  ██║   ██║   ╚██████╔╝██║  ██║███████║
╚═╝     ╚═╝╚══════╝   ╚═╝   ╚══════╝╚═╝  ╚═╝╚══════╝    ╚═╝        ╚═╝   ╚═╝   ╚══════╝╚═╝  ╚═╝╚═╝  ╚═╝   ╚═╝    ╚═════╝ ╚═╝  ╚═╝╚══════╝*/
  //Implement neutron filter for TrackIterator
  GriffAnaUtils::TrackIterator neutrons(&dr); //Iterator to follow the track of the particle
  neutrons.addFilter(new GriffAnaUtils::TrackFilter_PDGCode(2112)); //Add particle ID filter to track iterator
  neutrons.addFilter(new GriffAnaUtils::TrackFilter_Primary); //Add primary particle filter to track iterator
  
  //Implement gamma filter for TrackIterator
  GriffAnaUtils::TrackIterator gammas(&dr); //Iterator to follow the track of the particle
  gammas.addFilter(new GriffAnaUtils::TrackFilter_PDGCode(22)); //Add particle ID filter to track iterator
  // gammas.addFilter(new GriffAnaUtils::TrackFilter_Primary); //Add primary particle filter to track iterator

  GriffAnaUtils::TrackIterator opticalphoton(&dr); //Iterator to follow the track of the particle (only scintillation photons)

  GriffAnaUtils::TrackIterator all_particles(&dr); //Iterator to follow the track of the particle (all particles)

  //Implement segment filter (scintillator all particles)
  GriffAnaUtils::SegmentIterator segments_detector(&dr); //Iterator for segements
  segments_detector.addFilter(new GriffAnaUtils::SegmentFilter_Volume("scintillator")); //Add volume name filter to segment iterator
  

  //Implement segment filter (optical photons in "lightguide_back")
  GriffAnaUtils::SegmentIterator segments_pmt(&dr); //Iterator for segements
  segments_pmt.addFilter(new GriffAnaUtils::SegmentFilter_Volume("lightguide_back")); //Add volume name filter to segment iterator


/*
██████╗ ███████╗███████╗██╗███╗   ██╗███████╗    ██╗  ██╗██╗███████╗████████╗ ██████╗  ██████╗ ██████╗  █████╗ ███╗   ███╗███████╗
██╔══██╗██╔════╝██╔════╝██║████╗  ██║██╔════╝    ██║  ██║██║██╔════╝╚══██╔══╝██╔═══██╗██╔════╝ ██╔══██╗██╔══██╗████╗ ████║██╔════╝
██║  ██║█████╗  █████╗  ██║██╔██╗ ██║█████╗      ███████║██║███████╗   ██║   ██║   ██║██║  ███╗██████╔╝███████║██╔████╔██║███████╗
██║  ██║██╔══╝  ██╔══╝  ██║██║╚██╗██║██╔══╝      ██╔══██║██║╚════██║   ██║   ██║   ██║██║   ██║██╔══██╗██╔══██║██║╚██╔╝██║╚════██║
██████╔╝███████╗██║     ██║██║ ╚████║███████╗    ██║  ██║██║███████║   ██║   ╚██████╔╝╚██████╔╝██║  ██║██║  ██║██║ ╚═╝ ██║███████║
╚═════╝ ╚══════╝╚═╝     ╚═╝╚═╝  ╚═══╝╚══════╝    ╚═╝  ╚═╝╚═╝╚══════╝   ╚═╝    ╚═════╝  ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝     ╚═╝╚══════╝*/
  //counter histogram: neutrons
  auto h_qualitative_neutrons = hc.bookCounts("h_qualitative_neutrons", "h_qualitative_neutrons"); //main figure
  auto c_initial_neutrons     = h_qualitative_neutrons -> addCounter("initial_neutrons"); //add category
  auto c_neutrons_entering    = h_qualitative_neutrons -> addCounter("neutrons_entering"); //plot category
  auto c_neutrons_stopped     = h_qualitative_neutrons -> addCounter("neutrons_stopped"); //plot category

  //counter histogram: gammas
  auto h_qualitative_gammas = hc.bookCounts("h_qualitative_gammas", "h_qualitative_gammas"); //main figure
  auto c_initial_gammas     = h_qualitative_gammas -> addCounter("initial_gammas"); //add category
  auto c_gammas_entering    = h_qualitative_gammas -> addCounter("gammas_entering"); //plot category
  auto c_gammas_stopped     = h_qualitative_gammas -> addCounter("gammas_stopped"); //plot category

  //source spectra (neutrons)
  auto h_energy_neutron_initial = hc.book1D("h_energy_neutron_initial", 512, 0, 10, "h_energy_neutron_initial");
  h_energy_neutron_initial -> setXLabel("Energy [MeV]");
  h_energy_neutron_initial -> setYLabel("Counts");

  //source spectra (gammas)
  auto h_energy_gamma_initial = hc.book1D("h_energy_gamma_initial", 512, 0, 10, "h_energy_gamma_initial");
  h_energy_gamma_initial -> setXLabel("Energy [MeV]");
  h_energy_gamma_initial -> setYLabel("Counts");

  //energy deposition in scintillator detector all particles
  auto h_eDep_total_scintillator = hc.book1D("h_eDep_total_scintillator", 1024, 0, 10, "h_eDep_total_scintillator");
  h_eDep_total_scintillator -> setXLabel("Energy [MeV]");
  h_eDep_total_scintillator -> setYLabel("Counts");

  //energy deposition in scintillator detector primary neutrons (this is take from initial proton (neutron daughter) kinetic energy)
  auto h_eDep_neutron_scintillator = hc.book1D("h_eDep_neutron_scintillator", 1024, 0, 10, "h_eDep_neutron_scintillator");
  h_eDep_neutron_scintillator -> setXLabel("Energy [MeV]");
  h_eDep_neutron_scintillator -> setYLabel("Counts");

  // Optical photon counter (number of scintillation photons)
  auto h_qualitative_optphoton_total = hc.bookCounts("h_qualitative_optphoton_total", "h_qualitative_optphoton_total"); //main figure
  auto c_opticalph_start        = h_qualitative_optphoton_total -> addCounter("Created in scintillator"); //add category
  auto c_opticalph_photocathode = h_qualitative_optphoton_total -> addCounter("Hits photocathode"); //add category
  h_qualitative_optphoton_total -> setYLabel("Counts");  

  // Optical photon counter gamma/neutron separate (number of scintillation photons)
  auto h_qualitative_optphoton_neutron_only = hc.bookCounts("h_qualitative_optphoton_neutron_only", "h_qualitative_optphoton_neutron_only"); //main figure
  auto c_opticalph_start_proton = h_qualitative_optphoton_neutron_only -> addCounter("Created in scintillator"); //add category
  auto c_opticalph_photocathode_proton = h_qualitative_optphoton_neutron_only -> addCounter("Hits photocathode"); //add category
  auto c_opticalph_photocathode_other = h_qualitative_optphoton_neutron_only -> addCounter("Hits photocathode other"); //hits in photocathode from optical photons created from other particles.
  h_qualitative_optphoton_neutron_only -> setYLabel("Counts");  

  // Light attenuation tracker 
  auto h_qualitative_optphoton_transport    = hc.bookCounts("h_qualitative_optphoton_transport", "h_qualitative_optphoton_transport"); //main figure
  auto c_light_attenuation_scint      = h_qualitative_optphoton_transport -> addCounter("Created in Scintillator"); 
  auto c_light_attenuation_scintpaint = h_qualitative_optphoton_transport -> addCounter("Enters Scint Paint"); 
  auto c_light_attenuation_alcup      = h_qualitative_optphoton_transport -> addCounter("Enters Al Cup"); 
  auto c_light_attenuation_borglass   = h_qualitative_optphoton_transport -> addCounter("Enters Borated glass");
  auto c_light_attenuation_lightguide = h_qualitative_optphoton_transport -> addCounter("Enters Light guide"); 
  auto c_light_attenuation_lGpaint    = h_qualitative_optphoton_transport -> addCounter("Enters LG Paint"); 
  auto c_light_attenuation_PMT        = h_qualitative_optphoton_transport -> addCounter("Enters PMT glass"); 
  auto c_light_attenuation_PC         = h_qualitative_optphoton_transport -> addCounter("Enters Photocathode"); 
  h_qualitative_optphoton_transport -> setYLabel("Counts");  
  
  // Opt photon tracker ending (determined in which volume scintillations photons ends their life)
  auto h_qualitative_optphoton_end = hc.bookCounts("h_qualitative_optphoton_end", "h_qualitative_optphoton_end"); //main figure
  auto c_light_end_scint            = h_qualitative_optphoton_end -> addCounter("Ends in Scintillator"); 
  auto c_light_end_scint_paint      = h_qualitative_optphoton_end -> addCounter("Ends in Scint Paint"); 
  auto c_light_end_alcup            = h_qualitative_optphoton_end -> addCounter("Ends Al Cup"); 
  auto c_light_end_borglass         = h_qualitative_optphoton_end -> addCounter("Ends in Borated glass");
  auto c_light_end_lightguide       = h_qualitative_optphoton_end -> addCounter("Ends in Light guide"); 
  auto c_light_end_lightguide_paint = h_qualitative_optphoton_end -> addCounter("Ends in LG paint"); 
  auto c_light_end_PMT              = h_qualitative_optphoton_end -> addCounter("Ends in PMT glass"); 
  auto c_light_end_PC               = h_qualitative_optphoton_end -> addCounter("Ends in Photocathode"); 
  h_qualitative_optphoton_end -> setYLabel("Counts"); 


  // Opt photon tracker relfelction counter (determine how many times a photon reflects in a volume) 
  // NOTE: Simulation output must be set to FULL!
  // auto h_qualitative_optphoton_reflection = hc.bookCounts("h_qualitative_optphoton_reflection", "h_qualitative_optphoton_reflection"); //main figure
  // auto c_light_reflection_scint            = h_qualitative_optphoton_reflection -> addCounter("Scintillator"); 
  // auto c_light_reflection_scint_paint      = h_qualitative_optphoton_reflection -> addCounter("Scint Paint"); 
  // auto c_light_reflection_alcup            = h_qualitative_optphoton_reflection -> addCounter("Al Cup"); 
  // auto c_light_reflection_borglass         = h_qualitative_optphoton_reflection -> addCounter("Borated glass");
  // auto c_light_reflection_lightguide       = h_qualitative_optphoton_reflection -> addCounter("Light guide"); 
  // auto c_light_reflection_lightguide_paint = h_qualitative_optphoton_reflection -> addCounter("LG paint"); 
  // auto c_light_reflection_PMT              = h_qualitative_optphoton_reflection -> addCounter("Ends in PMT glass"); 
  // auto c_light_reflection_PC               = h_qualitative_optphoton_reflection -> addCounter("Photocathode"); 
  // h_qualitative_optphoton_reflection -> setYLabel("Counts"); 


  //Optical photon energy (scintillation photon energy)
  auto h_energy_optphoton_initial = hc.book1D("h_energy_optphoton_initial", 41, 2.48, 3.10, "h_energy_optphoton_initial");
  h_energy_optphoton_initial -> setXLabel("Energy [eV]");
  h_energy_optphoton_initial -> setYLabel("Counts");  

  //Optical photon wavelenght (scintillation photon wavelenght)
  auto h_wavelength_optphoton_initial = hc.book1D("h_wavelength_optphoton_initial", 41, 400, 500, "h_wavelength_optphoton_initial");
  h_wavelength_optphoton_initial -> setXLabel("Wavelength [nm]");
  h_wavelength_optphoton_initial -> setYLabel("Counts");  

  //Neutron Scintillator light yield (number of scintillation photons per neutron making it to the photocathode, use protons as creators of scintillation light)
  auto h_sum_optphoton_photocathode_proton = hc.book1D("h_sum_optphoton_photocathode_proton", 512, 0, 512, "h_sum_optphoton_photocathode_proton");
  h_sum_optphoton_photocathode_proton -> setYLabel("counts");
  h_sum_optphoton_photocathode_proton -> setXLabel("Num. photons in photocathode");  
  
  //Neutron Scintillator light yield (number of scintillation photons per neutron created in scintillator, , use protons as creators of scintillation light)
  auto h_sum_optphoton_scintillator_proton = hc.book1D("h_sum_optphoton_scintillator_proton", 1024, 0, 1024, "h_sum_optphoton_scintillator_proton");
  h_sum_optphoton_scintillator_proton -> setYLabel("counts");
  h_sum_optphoton_scintillator_proton -> setXLabel("Num. photons in scintillator"); 

  //Total Scintillator light yield (number of scintillation photons per event making it to the photocathode, using all particles!)
  auto h_sum_optphoton_photocathode_total = hc.book1D("h_sum_optphoton_photocathode_total", 2048, 0, 2048, "h_sum_optphoton_photocathode_total");
  h_sum_optphoton_photocathode_total -> setYLabel("counts");
  h_sum_optphoton_photocathode_total -> setXLabel("Num. photons in photocathode");  
  auto counter_optphoton_photocathode_total = 0; //define counter for number of scintillations photons making it into the photo cathode.
  
  //Total Scintillator light yield (number of scintillation photons per event created in scintillator, using all particles!)
  auto h_sum_optphoton_scintillator_total = hc.book1D("h_sum_optphoton_scintillator_total", 7500, 0, 7500, "h_sum_optphoton_scintillator_total");
  h_sum_optphoton_scintillator_total -> setYLabel("counts");
  h_sum_optphoton_scintillator_total -> setXLabel("Num. photons in scintillator"); 
  auto counter_optphoton_scintillator_total = 0; //define counter for number of scintillations photons created in scintillator.

  //Total Scintillator light yield (number of scintillation photons per event making it to the photocathode, using only compton scattering at 180 deg!)
  auto h_sum_optphoton_photocathode_total_CS = hc.book1D("h_sum_optphoton_photocathode_total_CS", 1024, 0, 2048, "h_sum_optphoton_photocathode_total_CS");
  h_sum_optphoton_photocathode_total_CS -> setYLabel("counts");
  h_sum_optphoton_photocathode_total_CS -> setXLabel("Num. photons in photocathode (CS only)");  
  auto counter_optphoton_photocathode_total_CS = 0; //define counter for number of scintillations photons making it into the photo cathode.
  
  //Total Scintillator light yield (number of scintillation photons per event created in scintillator, using only compton scattering at 180 deg!)
  auto h_sum_optphoton_scintillator_total_CS = hc.book1D("h_sum_optphoton_scintillator_total_CS", 2500, 0, 5000, "h_sum_optphoton_scintillator_total_CS");
  h_sum_optphoton_scintillator_total_CS -> setYLabel("counts");
  h_sum_optphoton_scintillator_total_CS -> setXLabel("Num. photons in scintillator (CS only)"); 
  auto counter_optphoton_scintillator_total_CS = 0; //define counter for number of scintillations photons created in scintillator.

  //Total Scintillator light yield (number of scintillation photons per event hitting the photocathode, taking quantum efficiency into acount
  auto h_sum_optphoton_photocathode_total_QE = hc.book1D("h_sum_optphoton_photocathode_total_QE", 2048, 0, 2048, "h_sum_optphoton_photocathode_total_QE");
  h_sum_optphoton_photocathode_total_QE -> setYLabel("counts");
  h_sum_optphoton_photocathode_total_QE -> setXLabel("Num. photons in scintillator (incl. quantum efficiency)"); 
  auto counter_optphoton_photocathode_total_QE = 0; //define counter for number of scintillations photons in photocathode incl Quantum efficiency

  //Total Scintillator light yield (number of scintillation photons per event hitting the photocathode, taking quantum efficiency into acount
  auto h_sum_optphoton_photocathode_total_CS_QE = hc.book1D("h_sum_optphoton_photocathode_total_CS_QE", 1024, 0, 2048, "h_sum_optphoton_photocathode_total_CS_QE");
  h_sum_optphoton_photocathode_total_CS_QE -> setYLabel("counts");
  h_sum_optphoton_photocathode_total_CS_QE -> setXLabel("Num. photons in scintillator (incl. quantum efficiency and max CS cut)"); 
  auto counter_optphoton_photocathode_total_CS_QE = 0; //define counter for number of scintillations photons in photocathode incl Quantum efficiency

  //Total Scintillator light yield (number of scintillation photons per event hitting the photocathode, taking quantum efficiency into acount and including Z-cut
  auto h_sum_optphoton_photocathode_total_CS_QE_Zcut_1 = hc.book1D("h_sum_optphoton_photocathode_total_CS_QE_Zcut_1", 1024, 0, 2048, "h_sum_optphoton_photocathode_total_CS_QE_Zcut_1");
  h_sum_optphoton_photocathode_total_CS_QE_Zcut_1 -> setYLabel("counts");
  h_sum_optphoton_photocathode_total_CS_QE_Zcut_1 -> setXLabel("Num. photons in scintillator (incl. quantum efficiency, max CS cut and Z-cut)"); 
  auto counter_optphoton_photocathode_total_CS_QE_Zcut_1 = 0; 

  //Total Scintillator light yield (number of scintillation photons per event hitting the photocathode, taking quantum efficiency into acount and including Z-cut
  auto h_sum_optphoton_photocathode_total_CS_QE_Zcut_2 = hc.book1D("h_sum_optphoton_photocathode_total_CS_QE_Zcut_2", 1024, 0, 2048, "h_sum_optphoton_photocathode_total_CS_QE_Zcut_2");
  h_sum_optphoton_photocathode_total_CS_QE_Zcut_2 -> setYLabel("counts");
  h_sum_optphoton_photocathode_total_CS_QE_Zcut_2 -> setXLabel("Num. photons in scintillator (incl. quantum efficiency, max CS cut and Z-cut)"); 
  auto counter_optphoton_photocathode_total_CS_QE_Zcut_2 = 0; 

  //Total Scintillator light yield (number of scintillation photons per event hitting the photocathode, taking quantum efficiency into acount and including Z-cut
  auto h_sum_optphoton_photocathode_total_CS_QE_Zcut_3 = hc.book1D("h_sum_optphoton_photocathode_total_CS_QE_Zcut_3", 1024, 0, 2048, "h_sum_optphoton_photocathode_total_CS_QE_Zcut_3");
  h_sum_optphoton_photocathode_total_CS_QE_Zcut_3 -> setYLabel("counts");
  h_sum_optphoton_photocathode_total_CS_QE_Zcut_3 -> setXLabel("Num. photons in scintillator (incl. quantum efficiency, max CS cut and Z-cut)"); 
  auto counter_optphoton_photocathode_total_CS_QE_Zcut_3 = 0; 

  //Total Scintillator light yield (number of scintillation photons per event hitting the photocathode, taking quantum efficiency into acount and including Z-cut
  auto h_sum_optphoton_photocathode_total_CS_QE_Zcut_4 = hc.book1D("h_sum_optphoton_photocathode_total_CS_QE_Zcut_4", 1024, 0, 2048, "h_sum_optphoton_photocathode_total_CS_QE_Zcut_4");
  h_sum_optphoton_photocathode_total_CS_QE_Zcut_4 -> setYLabel("counts");
  h_sum_optphoton_photocathode_total_CS_QE_Zcut_4 -> setXLabel("Num. photons in scintillator (incl. quantum efficiency, max CS cut and Z-cut)"); 
  auto counter_optphoton_photocathode_total_CS_QE_Zcut_4 = 0; 

  //Total Scintillator light yield (number of scintillation photons per event hitting the photocathode, taking quantum efficiency into acount and including Z-cut
  auto h_sum_optphoton_photocathode_total_CS_QE_Zcut_5 = hc.book1D("h_sum_optphoton_photocathode_total_CS_QE_Zcut_5", 1024, 0, 2048, "h_sum_optphoton_photocathode_total_CS_QE_Zcut_5");
  h_sum_optphoton_photocathode_total_CS_QE_Zcut_5 -> setYLabel("counts");
  h_sum_optphoton_photocathode_total_CS_QE_Zcut_5 -> setXLabel("Num. photons in scintillator (incl. quantum efficiency, max CS cut and Z-cut)"); 
  auto counter_optphoton_photocathode_total_CS_QE_Zcut_5 = 0; 

  //Total Scintillator light yield (number of scintillation photons per event hitting the photocathode, taking quantum efficiency into acount and including Z-cut
  auto h_sum_optphoton_photocathode_total_CS_QE_Zcut_6 = hc.book1D("h_sum_optphoton_photocathode_total_CS_QE_Zcut_6", 1024, 0, 2048, "h_sum_optphoton_photocathode_total_CS_QE_Zcut_6");
  h_sum_optphoton_photocathode_total_CS_QE_Zcut_6 -> setYLabel("counts");
  h_sum_optphoton_photocathode_total_CS_QE_Zcut_6 -> setXLabel("Num. photons in scintillator (incl. quantum efficiency, max CS cut and Z-cut)"); 
  auto counter_optphoton_photocathode_total_CS_QE_Zcut_6 = 0; 

  //Total Scintillator light yield (number of scintillation photons per gamma making it to the photocathode)
  auto h_sum_optphoton_photocathode_gamma = hc.book1D("h_sum_optphoton_photocathode_gamma", 2048, 0, 2048, "h_sum_optphoton_photocathode_gamma");
  h_sum_optphoton_photocathode_gamma -> setYLabel("counts");
  h_sum_optphoton_photocathode_gamma -> setXLabel("Num. photons in photocathode");  
  auto counter_optphoton_photocathode_gamma = 0; //define counter for number of scintillations photons making it into the photo cathode per neutron.
  
  //Total Scintillator light yield (number of scintillation photons per gamma created in scintillator)
  auto h_sum_optphoton_scintillator_gamma = hc.book1D("h_sum_optphoton_scintillator_gamma", 7500, 0, 7500, "h_sum_optphoton_scintillator_gamma");
  h_sum_optphoton_scintillator_gamma -> setYLabel("counts");
  h_sum_optphoton_scintillator_gamma -> setXLabel("Num. photons in scintillator"); 
  auto counter_optphoton_scintillator_gamma = 0; //define counter for number of scintillations photons created in scintillator per neutron.

  //Scintillation photon eDep inside scintillator
  auto h_eDep_optphoton_scintillator = hc.book1D("h_eDep_optphoton_scintillator", 400, 0, 13000, "h_eDep_optphoton_scintillator");
  h_eDep_optphoton_scintillator -> setYLabel("counts");
  h_eDep_optphoton_scintillator -> setXLabel("Opt. photon eDep [eV]"); 
  auto counter_eDep_optphoton_scintillator = 0; //define counter for scintillator eDep in scintillator

  //Scintillation photon eDep at photocathode
  auto h_eDep_optphoton_photocathode = hc.book1D("h_eDep_optphoton_photocathode", 100, 0, 4000, "h_eDep_optphoton_photocathode");
  h_eDep_optphoton_photocathode -> setYLabel("counts");
  h_eDep_optphoton_photocathode -> setXLabel("Opt. photon eDep [eV]"); 
  auto counter_eDep_optphoton_photocathode = 0; //define counter for scintillator eDep in photocathode

  //Scintillation light yield at scintillator (units: #photons/eDep)
  auto h_light_yield_Num_scintillator = hc.book1D("h_light_yield_Num_scintillator", 400, 0, 3000, "h_light_yield_Num_scintillator");
  h_light_yield_Num_scintillator -> setYLabel("counts");
  h_light_yield_Num_scintillator -> setXLabel("Num. Opt. photon / MeVee"); 
  auto counter_light_yield_scintillator = 0; //define counter for light yield at in scintillator

  //Scintillation light yield at photocathode (units: #photons/eDep)
  auto h_light_yield_Num_photocathode = hc.book1D("h_light_yield_Num_photocathode", 400, 0, 3000, "h_light_yield_Num_photocathode");
  h_light_yield_Num_photocathode -> setYLabel("counts");
  h_light_yield_Num_photocathode -> setXLabel("Num. Opt. photon / MeVee"); 
  auto counter_light_yield_photocathode = 0; //define counter for light yield at in photocathode
  
  //Scintillation light yield at scintillator (units: #photons eDep[eV]/eDep[MeVee])
  auto h_light_yield_eDep_scintillator = hc.book1D("h_light_yield_eDep_scintillator", 400, 0, 3000, "h_light_yield_eDep_scintillator");
  h_light_yield_eDep_scintillator -> setYLabel("counts");
  h_light_yield_eDep_scintillator -> setXLabel("Opt. photon eDep [eV] / MeVee"); 

  //Scintillation light yield at photocathode (units: #photons eDep[eV]/eDep[MeVee])
  auto h_light_yield_eDep_photocathode = hc.book1D("h_light_yield_eDep_photocathode", 400, 0, 3000, "h_light_yield_eDep_photocathode");
  h_light_yield_eDep_photocathode -> setYLabel("counts");
  h_light_yield_eDep_photocathode -> setXLabel("Opt. photon eDep [eV] / MeVee"); 
  
  auto h_gamma_interaction_pos_Z_scintillator = hc.book1D("h_gamma_interaction_pos_Z_scintillator", 1700, -85, 85, "h_gamma_interaction_pos_Z_scintillator");
  h_gamma_interaction_pos_Z_scintillator -> setYLabel("counts");
  h_gamma_interaction_pos_Z_scintillator -> setXLabel("Interaction position [mm]"); 

  auto h_gamma_interaction_pos_Y_scintillator = hc.book1D("h_gamma_interaction_pos_Y_scintillator", 1400, -70, 70, "h_gamma_interaction_pos_Y_scintillator");
  h_gamma_interaction_pos_Y_scintillator -> setYLabel("counts");
  h_gamma_interaction_pos_Y_scintillator -> setXLabel("Interaction position [mm]"); 

  auto h_gamma_interaction_pos_X_scintillator = hc.book1D("h_gamma_interaction_pos_X_scintillator", 1400, -70, 70, "h_gamma_interaction_pos_X_scintillator");
  h_gamma_interaction_pos_X_scintillator -> setYLabel("counts");
  h_gamma_interaction_pos_X_scintillator -> setXLabel("Interaction position [mm]"); 

  auto h_gamma_interaction_pos_XY_scintillator = hc.book2D("h_gamma_interaction_pos_XY_scintillator", 1400, -70, 70, 1400, -70, 70, "h_gamma_interaction_pos_XY_scintillator");
  h_gamma_interaction_pos_XY_scintillator -> setYLabel("Interaction position X [mm]");
  h_gamma_interaction_pos_XY_scintillator -> setXLabel("Interaction position X [mm]"); 


  auto CSMIN = 475; //Maximum compton energy to look for when scattering 180 deg. (keV)
  auto c_light = 299792458 * pow(10, 9); //speed of light nm/s
  auto h_planck = 4.1357 * 1/pow(10, 15); //planks constant eV*s  

  /*
  ███████╗██╗   ██╗███████╗███╗   ██╗████████╗    ██╗      ██████╗  ██████╗ ██████╗ 
  ██╔════╝██║   ██║██╔════╝████╗  ██║╚══██╔══╝    ██║     ██╔═══██╗██╔═══██╗██╔══██╗
  █████╗  ██║   ██║█████╗  ██╔██╗ ██║   ██║       ██║     ██║   ██║██║   ██║██████╔╝
  ██╔══╝  ╚██╗ ██╔╝██╔══╝  ██║╚██╗██║   ██║       ██║     ██║   ██║██║   ██║██╔═══╝ 
  ███████╗ ╚████╔╝ ███████╗██║ ╚████║   ██║       ███████╗╚██████╔╝╚██████╔╝██║     
  ╚══════╝  ╚═══╝  ╚══════╝╚═╝  ╚═══╝   ╚═╝       ╚══════╝ ╚═════╝  ╚═════╝ ╚═╝     */

  while (dr.loopEvents()){ //loop over ALL events
    printf("<>-<>-<>-<>-<>-<>-<>-<>-<>-<>-<>-<>-<>-<>-<>-<>-<>-<>-<>\n");
    printf("        (Run/Event) = (%i/%i) \n", dr.runNumber(), dr.eventNumber());
    // printf("- Primary tracks = %i\n- Total tracks = %i\n",
    //       dr.nPrimaryTracks(),
    //       dr.nTracks());
    // printf("- dr.eventActive(): %i\n", dr.eventActive());
    // printf("- dr.eventIndexInCurrentFile(): %i\n", dr.eventIndexInCurrentFile());
    // printf("- dr.verifyEventDataIntegrity(): %i\n", dr.verifyEventDataIntegrity());
    // printf("- dr.setupChanged(): %i\n", dr.setupChanged());
    
    // reset counters and trackers....
    counter_optphoton_photocathode_total = 0; //reset counter for number of scintillations photons making it into the photocathode per event.
    counter_optphoton_scintillator_total = 0; //reset counter for number of scintillations photons created in scintillator per event.
    counter_optphoton_photocathode_total_CS = 0; //reset counter for number of scintillations photons into the photocathode per event COMPTON ONLY.
    counter_optphoton_scintillator_total_CS = 0; //reset counter for number of scintillations photons created in scintillator per event COMPTON ONLY.
    counter_optphoton_photocathode_total_QE = 0; //reset counter for number of scint photons in photocathode, scaled by quantum efficiency.
    counter_optphoton_photocathode_total_CS_QE = 0; //reset counter 
    counter_optphoton_photocathode_total_CS_QE_Zcut_1 = 0;//reset counter for Z distance cut
    counter_optphoton_photocathode_total_CS_QE_Zcut_2 = 0; //reset counter for Z distance cut
    counter_optphoton_photocathode_total_CS_QE_Zcut_3 = 0; //reset counter for Z distance cut
    counter_optphoton_photocathode_total_CS_QE_Zcut_4 = 0; //reset counter for Z distance cut
    counter_optphoton_photocathode_total_CS_QE_Zcut_5 = 0; //reset counter for Z distance cut
    counter_optphoton_photocathode_total_CS_QE_Zcut_6 = 0; //reset counter for Z distance cut
    
    counter_optphoton_photocathode_gamma = 0; //reset counter for number of scintillations photons making it into the photocathode per gamma event.
    counter_optphoton_scintillator_gamma = 0; //reset counter for number of scintillations photons created in scintillator per gamma event.
    counter_eDep_optphoton_scintillator = 0; //reset counter for scintillation photon eDep in scintillator
    counter_eDep_optphoton_photocathode = 0; //reset counter for scintillation photon eDep in photocathode
    counter_light_yield_scintillator = 0; //reset counter for light yield in scintillator
    counter_light_yield_photocathode = 0; //reset counter for light yield in photocathode
    

    /*
    ┌─┐┌─┐┌─┐┌┬┐┌─┐┌┐┌┌┬┐  ┬┌┬┐┌─┐┬─┐┌─┐┌┬┐┌─┐┬─┐  ┌─ ┌─┐┌┬┐┌─┐┌─┐ ─┐
    └─┐├┤ │ ┬│││├┤ │││ │   │ │ ├┤ ├┬┘├─┤ │ │ │├┬┘  │  ├┤  ││├┤ ├─┘  │
    └─┘└─┘└─┘┴ ┴└─┘┘└┘ ┴   ┴ ┴ └─┘┴└─┴ ┴ ┴ └─┘┴└─  └─ └─┘─┴┘└─┘┴   ─┘ all particles*/
    double edep=0; //define edep variable and set to zere (used in the loop below)
    while (auto segment = segments_detector.next()) {
      edep += segment->eDep(); //increment the eDep values for this particle and and all segements in has traversed inside the scintillator
    }
    if (edep>0){ //check if any energy was deposited (to prevent counting zeros)
      eDep_total_scintillator->fill(edep/Units::MeV); //Add current eDep value to histogram
    }

    /*
    ┌─┐┬  ┬    ┌─┐┌─┐┬─┐┌┬┐┬┌─┐┬  ┌─┐┌─┐
    ├─┤│  │    ├─┘├─┤├┬┘ │ ││  │  ├┤ └─┐
    ┴ ┴┴─┘┴─┘  ┴  ┴ ┴┴└─ ┴ ┴└─┘┴─┘└─┘└─┘*/
    while (auto particle = all_particles.next()){//looping across all particles
      if (particle -> pdgName() == "opticalphoton"){ //creator process is "Scintillation" (from John's custom physics list)
        if (particle -> segmentBegin() -> volumeName() == "scintillator"){ // Check if the optical photon origin is in the scintillator volume.
          
          if (particle -> getParent() -> pdgName() == "e-" && particle -> getParent() -> creatorProcess() == "compt" && particle -> getParent() -> startEKin()/Units::keV > CSMIN){
            counter_optphoton_scintillator_total_CS += 1; //increment opt photon in photocatode from COMPTON scattering at 180 deg (Cs137).
            // printf("pdgName = %s\n",particle->pdgNameCStr());
          }
          counter_optphoton_scintillator_total += 1; //increment scintillation photon counter in scintillator from any particle
          counter_light_yield_scintillator += 1; //increment scintillation photon counter in scintillator from any particle (used for light yield calculations)
          ++c_light_attenuation_scint; //counter for attenuation summary histogram
          counter_eDep_optphoton_scintillator += particle -> startEKin()/Units::eV; //sum for scintillator photon eDep in scintillator
        }

        // Check where the optical photon ends its life
        if (particle -> lastSegment() -> volumeName() == "scintillator"){
          ++c_light_end_scint;
        }
        if (particle -> lastSegment() -> volumeName() == "scintillator_paint"){
          ++c_light_end_scint_paint;
        }
        if (particle -> lastSegment() -> volumeName() == "Al_cup"){
          ++c_light_end_alcup;
        }
        if (particle -> lastSegment() -> volumeName() == "optical_window"){
          ++c_light_end_borglass;
        }
        if (particle -> lastSegment() -> volumeName() == "light_guide"){
          ++c_light_end_lightguide;
        }
        if (particle -> lastSegment() -> volumeName() == "light_guide_paint"){
          ++c_light_end_lightguide_paint;
        }
        if (particle -> lastSegment() -> volumeName() == "pmt_window"){
          ++c_light_end_PMT;
        }
        if (particle -> lastSegment() -> volumeName() == "photo_cathode"){
          ++c_light_end_PC;
        }

        auto scintpaint_flag      = 0; //set flag to avoid double counting of volumes
        auto alcup_flag           = 0; //set flag to avoid double counting of volumes
        auto optical_window_flag  = 0; //set flag to avoid double counting of volumes
        auto lightguide_flag      = 0; //set flag to avoid double counting of volumes
        auto lightguidepaint_flag = 0; //set flag to avoid double counting of volumes
        auto pmt_window_flag      = 0; //set flag to avoid double counting of volumes
        auto photo_cathode_flag   = 0; //set flag to avoid double counting of volumes
        auto posZ_1_flag          = 0; //set flag to avoid double counting of Z depth cuts
        auto posZ_2_flag          = 0; //set flag to avoid double counting of Z depth cuts
        auto posZ_3_flag          = 0; //set flag to avoid double counting of Z depth cuts
        auto posZ_4_flag          = 0; //set flag to avoid double counting of Z depth cuts
        auto posZ_5_flag          = 0; //set flag to avoid double counting of Z depth cuts
        auto posZ_6_flag          = 0; //set flag to avoid double counting of Z depth cuts

        auto seg0 = particle -> segmentBegin(); //set the starting segment.
        auto segE = particle -> segmentEnd(); //set the ending segment.
        auto seg = seg0; //set starting segment as first in loop.
        for (++seg; seg<segE; ++seg) { 

        //Check if optphoton enters a volume
          if (seg->volumeName() == "scintillator_paint" && scintpaint_flag == 0){ 
            ++c_light_attenuation_scintpaint; //counter for attenuation summary histogram
            scintpaint_flag = 1; //use flag to prevent double counting.
          }
          if (seg->volumeName() == "Al_cup" && alcup_flag == 0){ 
            ++c_light_attenuation_alcup; //counter for attenuation summary histogram
            alcup_flag = 1; //use flag to prevent double counting.
          }
          if (seg->volumeName() == "optical_window" && optical_window_flag == 0){ 
            ++c_light_attenuation_borglass; //counter for attenuation summary histogram
            optical_window_flag = 1; //use flag to prevent double counting.
          }
          if (seg->volumeName() == "light_guide" && lightguide_flag == 0){ 
            ++c_light_attenuation_lightguide; //counter for attenuation summary histogram
            lightguide_flag = 1; //use flag to prevent double counting.
          }
          if (seg->volumeName() == "light_guide_paint" && lightguidepaint_flag == 0){ 
            ++c_light_attenuation_lGpaint; //counter for attenuation summary histogram
            lightguidepaint_flag = 1; //use flag to prevent double counting.
          }
          if (seg->volumeName() == "pmt_window" && pmt_window_flag == 0){ 
            ++c_light_attenuation_PMT; //counter for attenuation summary histogram
            pmt_window_flag = 1; //use flag to prevent double counting.
          }
          if (seg->volumeName() == "photo_cathode" && photo_cathode_flag == 0){    
            counter_optphoton_photocathode_total += 1; //increment scintillation photon counter in photocathode from any particle
            counter_light_yield_photocathode += 1; //increment scintillation photon counter in photocathode from any particle (used for light yield calculations)
            ++c_light_attenuation_PC; //counter for attenuation summary histogram
            photo_cathode_flag = 1;  //use flag to prevent double counting.
            counter_eDep_optphoton_photocathode += particle -> startEKin()/Units::eV; //sum for scintillator photon eDep in scintillator                     
            
            if (particle -> getParent() -> pdgName() == "e-" && particle -> getParent() -> creatorProcess() == "compt" && particle -> getParent() -> startEKin()/Units::keV > CSMIN){
              counter_optphoton_photocathode_total_CS += 1; //increment opt photon in photocatode from COMPTON scattering at 180 deg (Cs137).
              
              if (QuantumEff(c_light*h_planck/(particle -> startEKin()/Units::eV))==1){ 
                counter_optphoton_photocathode_total_CS_QE += 1;//fill histogram including QE of PMT
                
                //Add cut for different interaction positions along the scintillator in z-direction
                auto seg02 = particle -> getParent() -> getParent() -> segmentBegin(); //set the starting segment of optphoton grandparent.
                auto segE2 = particle -> getParent() -> getParent() -> segmentEnd(); //set the ending segment of optphoton grandparent.
                auto seg2 = seg02; //set starting segment as first in loop.
                for (++seg2; seg2<segE2; ++seg2) { 
                  auto step0 = seg2->stepBegin();
                  auto stepE = seg2->stepEnd();
                  auto step = step0;
                  for (;step!=stepE;++step) {
                    
                    if (step->postProcessDefinedStep() == "compt" && seg2 -> volumeName() == "scintillator"){ 
                      h_gamma_interaction_pos_Z_scintillator -> fill(step->preLocalZ()/Units::mm);
                      h_gamma_interaction_pos_Y_scintillator -> fill(step->preLocalY()/Units::mm);
                      h_gamma_interaction_pos_X_scintillator -> fill(step->preLocalX()/Units::mm);
                      h_gamma_interaction_pos_XY_scintillator ->fill(step->preLocalX()/Units::mm, step->preLocalY()/Units::mm);

                      if(step->preLocalZ()/Units::mm >= -27.0 && step->preLocalZ()/Units::mm < -17.0 && posZ_1_flag == 0){
                        counter_optphoton_photocathode_total_CS_QE_Zcut_1 += 1;
                        posZ_1_flag = 1; //Flag used to prevent double counting of optphotons.
                      }
                      if(step->preLocalZ()/Units::mm >= -17.0 && step->preLocalZ()/Units::mm < -7.0 && posZ_2_flag == 0){
                        counter_optphoton_photocathode_total_CS_QE_Zcut_2 += 1;
                        posZ_2_flag = 1; //Flag used to prevent double counting of optphotons.
                      }
                      if(step->preLocalZ()/Units::mm >= -7.0 && step->preLocalZ()/Units::mm < 2.0 && posZ_3_flag == 0){
                        counter_optphoton_photocathode_total_CS_QE_Zcut_3 += 1;
                        posZ_3_flag = 1; //Flag used to prevent double counting of optphotons.
                      }
                      if(step->preLocalZ()/Units::mm >= 2.0 && step->preLocalZ()/Units::mm < 12.0 && posZ_4_flag == 0){
                        counter_optphoton_photocathode_total_CS_QE_Zcut_4 += 1;
                        posZ_4_flag = 1; //Flag used to prevent double counting of optphotons.
                      }
                      if(step->preLocalZ()/Units::mm >= 12.0 && step->preLocalZ()/Units::mm < 22.0 && posZ_5_flag == 0){
                        counter_optphoton_photocathode_total_CS_QE_Zcut_5 += 1;
                        posZ_5_flag = 1; //Flag used to prevent double counting of optphotons.
                      }
                      if(step->preLocalZ()/Units::mm >= 22.0 && step->preLocalZ()/Units::mm < 32.0 && posZ_6_flag == 0){
                        counter_optphoton_photocathode_total_CS_QE_Zcut_6 += 1;
                        posZ_6_flag = 1; //Flag used to prevent double counting of optphotons.
                      }
                    }
                  }
                }
              }
            }              
            if (QuantumEff(c_light*h_planck/(particle -> startEKin()/Units::eV))==1){ 
              counter_optphoton_photocathode_total_QE += 1;
            }
          } 
        } //end segment iterator
      } //end if scintillator creator process
    } //end all particle loop

    // printf("number of scint photons in scintillator: %i\n",counter_optphoton_scintillator_total);
    // printf("number of scint photons in photocathode: %i\n",counter_optphoton_photocathode_total);

    if (edep>0){
      
      if (counter_eDep_optphoton_scintillator>0){ //exclude zero count events
        h_eDep_optphoton_scintillator->fill(counter_eDep_optphoton_scintillator);
      }
      if (counter_eDep_optphoton_photocathode>0){ //exclude zero count events
        h_eDep_optphoton_photocathode->fill(counter_eDep_optphoton_photocathode);
      }
      if (counter_light_yield_scintillator>0){//exclude zero count events
        h_light_yield_Num_scintillator -> fill(counter_light_yield_scintillator/(edep/Units::MeV)); // Num/MeVee
      }     
      if (counter_light_yield_photocathode>0){//exclude zero count events
        h_light_yield_Num_photocathode -> fill(counter_light_yield_photocathode/(edep/Units::MeV)); // Num/MeVee
      }
      if (counter_eDep_optphoton_scintillator>0){ //exclude zero count events
        h_light_yield_eDep_scintillator->fill(counter_eDep_optphoton_scintillator/(edep/Units::MeV)); // eV/MeVee
      }
      if (counter_eDep_optphoton_photocathode>0){ //exclude zero count events
        h_light_yield_eDep_photocathode->fill(counter_eDep_optphoton_photocathode/(edep/Units::MeV)); // eV/MeVee
      }
    }
    if (counter_optphoton_scintillator_total>0){
      h_sum_optphoton_scintillator_total -> fill(counter_optphoton_scintillator_total);
    }
    if (counter_optphoton_photocathode_total>0){
      h_sum_optphoton_photocathode_total -> fill(counter_optphoton_photocathode_total);
    }  
    if (counter_optphoton_scintillator_total_CS>0){
      h_sum_optphoton_scintillator_total_CS -> fill(counter_optphoton_scintillator_total_CS);
    }
    if (counter_optphoton_photocathode_total_CS>0){
      h_sum_optphoton_photocathode_total_CS -> fill(counter_optphoton_photocathode_total_CS);
    }        
    if (counter_optphoton_photocathode_total_QE>0){
      h_sum_optphoton_photocathode_total_QE -> fill(counter_optphoton_photocathode_total_QE);
    }     
    if (counter_optphoton_photocathode_total_CS_QE>0){
      h_sum_optphoton_photocathode_total_CS_QE -> fill(counter_optphoton_photocathode_total_CS_QE);
    }
    if (counter_optphoton_photocathode_total_CS_QE_Zcut_1>0){
      h_sum_optphoton_photocathode_total_CS_QE_Zcut_1 -> fill(counter_optphoton_photocathode_total_CS_QE_Zcut_1);
    }    
    if (counter_optphoton_photocathode_total_CS_QE_Zcut_2>0){
      h_sum_optphoton_photocathode_total_CS_QE_Zcut_2 -> fill(counter_optphoton_photocathode_total_CS_QE_Zcut_2);
    }    
    if (counter_optphoton_photocathode_total_CS_QE_Zcut_3>0){
      h_sum_optphoton_photocathode_total_CS_QE_Zcut_3 -> fill(counter_optphoton_photocathode_total_CS_QE_Zcut_3);
    }    
    if (counter_optphoton_photocathode_total_CS_QE_Zcut_4>0){
      h_sum_optphoton_photocathode_total_CS_QE_Zcut_4 -> fill(counter_optphoton_photocathode_total_CS_QE_Zcut_4);
    }    
    if (counter_optphoton_photocathode_total_CS_QE_Zcut_5>0){
      h_sum_optphoton_photocathode_total_CS_QE_Zcut_5 -> fill(counter_optphoton_photocathode_total_CS_QE_Zcut_5);
    }    
    if (counter_optphoton_photocathode_total_CS_QE_Zcut_6>0){
      h_sum_optphoton_photocathode_total_CS_QE_Zcut_6 -> fill(counter_optphoton_photocathode_total_CS_QE_Zcut_6);
    }     
    


    /*
    ┌─┐┌─┐┌┬┐┬┌─┐┌─┐┬    ┌─┐┬ ┬┌─┐┌┬┐┌─┐┌┐┌  ┌─┐┬  ┬┌─┐┌┐┌┌┬┐  ┬  ┌─┐┌─┐┌─┐
    │ │├─┘ │ ││  ├─┤│    ├─┘├─┤│ │ │ │ ││││  ├┤ └┐┌┘├┤ │││ │   │  │ ││ │├─┘
    └─┘┴   ┴ ┴└─┘┴ ┴┴─┘  ┴  ┴ ┴└─┘ ┴ └─┘┘└┘  └─┘ └┘ └─┘┘└┘ ┴   ┴─┘└─┘└─┘┴  */

    while (auto particle = opticalphoton.next()){

      if (particle -> creatorProcess() == "Scintillation"){ //creator process is "Scintillation" (from John's custom physics list)

        auto seg0 = particle -> segmentBegin(); //set the starting segment.
        auto segE = particle -> segmentEnd(); //set the ending segment.
        
        auto seg = seg0; //set starting segment as first in loop.
        for (++seg; seg<segE; ++seg) { 
          if (seg->volumeName() == "photo_cathode"){ 
           
            ++c_opticalph_photocathode; //increment total number of optical photons entering the photocathode for qualitative histogram

            break; //optical photon found in photocathode, end segment loop.
          }
        }
        if (particle -> segmentBegin() -> volumeName() == "scintillator"){ // Check if the optical photon origin is in the scintillator volume.
          auto photon_energy = seg0 -> firstStep() -> preEKin()/Units::eV;

          h_energy_optphoton_initial -> fill(photon_energy); //Fill hist with initial optical photon energy created inside scintillator volume
         
          h_wavelength_optphoton_initial -> fill(c_light*h_planck/photon_energy); //Fill hist with initial optical photon wavelength created inside scintillator volume
          
          ++c_opticalph_start; //increment optical photon which is created in the scintillator volume for qualitative histogram

        }
      } //end if scintillator creator process
    } //end loop optical photon


    /*
    ┌┐┌┌─┐┬ ┬┌┬┐┬─┐┌─┐┌┐┌  ┌─┐┬  ┬┌─┐┌┐┌┌┬┐  ┬  ┌─┐┌─┐┌─┐
    │││├┤ │ │ │ ├┬┘│ ││││  ├┤ └┐┌┘├┤ │││ │   │  │ ││ │├─┘
    ┘└┘└─┘└─┘ ┴ ┴└─└─┘┘└┘  └─┘ └┘ └─┘┘└┘ ┴   ┴─┘└─┘└─┘┴  */
    while (auto particle = neutrons.next()){
      // printf(" ----------------  NEUTRON NUMBER %i (%f MeV) ---------------- \n", 
      //               dr.eventNumber(),
      //               particle->startEKin()/Units::MeV);
      
      auto c_optphoton_pc = 0; //temporary counter for number of scintillations photons making it into the photo cathode per neutron.
      auto c_optphoton_scint = 0; //temporary counter for number of scintillations photons created in scintillator per neutron.

      float neutron_edep = 0; //reset eDep energy to 0.

      ++c_initial_neutrons; //increment number-of-neutrons-from-source counter
      auto seg0 = particle -> segmentBegin(); //set the starting segment
      auto segE = particle -> segmentEnd(); //set the ending segment
      auto seg = seg0; //set the current segment
      
      auto nE = seg -> firstStep() -> preEKin(); //Save primary neutron energy

      h_energy_neutron_initial -> fill(nE/Units::MeV); //Fill initial neutron energy from source
      
      auto scintillator_flag = 0; //Flag to determine if the neutron has entered the scintillator volume
      

      for (++seg; seg<segE; ++seg) {
        // printf(">>>>>>>>>>>>> segStep: VolumeName() = %s \n",
        //         seg -> volumeNameCStr());  
        if (scintillator_flag == 0 && seg->volumeName() == "scintillator"){ //check if particle is in detector volume
          ++c_neutrons_entering; //increment neutrons-entering-detector counter
          break;
        }
      }

      auto segL = particle -> lastSegment();
      if (segL->volumeName() == "scintillator") { //check if particle stopped inside detector volume
        ++c_neutrons_stopped; //increment neutrons-stopped-in-detector counter
      }

      /*
      ┬  ┬┌─┐┬ ┬┌┬┐  ┬ ┬┬┌─┐┬  ┌┬┐  ┌┬┐┬─┐┌─┐┌─┐┬┌─┬┌┐┌┌─┐
      │  ││ ┬├─┤ │   └┬┘│├┤ │   ││   │ ├┬┘├─┤│  ├┴┐│││││ ┬
      ┴─┘┴└─┘┴ ┴ ┴    ┴ ┴└─┘┴─┘─┴┘   ┴ ┴└─┴ ┴└─┘┴ ┴┴┘└┘└─┘ (from neutrons)*/
      //scintillation photons would be 3rd generation particle from the initial neutron.
      if(particle->nDaughters() != 0) { //Check if the current neutron has a daughters (other particles created through its interactions)?
        // printf("**** Found daughters: ****\n");
        
        for(uint i=0; i<particle->nDaughters(); ++i) { //loop through all the neutron daughters found.
          // printf("- Neutron interaction with: %s %s %f eV \n",
          //        particle -> getDaughter(i) -> pdgNameCStr(),
          //        particle -> getDaughter(i) -> creatorProcessCStr(),
          //        particle -> getDaughter(i) -> startEKin()/Units::eV);              
  
          auto current_daughter = particle -> getDaughter(i); //save current neutron doughter.
          if(current_daughter -> nDaughters() != 0) { //check if the current neutrons daughter have daughters of its own. I.e protons which can ionize the material and produce sinctillation photons.
            
            // TODO: add scintillation counter per neutron for ANY particle not just protons.
            //       if (current_daughter -> getDaughter(j) -> creatorProcess() == "Scintillation"){ //creator process is "Scintillation" (from John's custom physics list)
            //                 if (c_optphoton_pc>0){ //exlude zero count events
            //   h_sum_optphoton_photocathode_total -> fill(c_optphoton_pc); //Fill number of scintillation photons making to the photo cathode per neutron (all particles).
            // }




            /*
            ┌─┐┬─┐┌─┐┌┬┐┌─┐┌┐┌  ┌┬┐┌─┐┬ ┬┌─┐┬ ┬┌┬┐┌─┐┬─┐┌─┐
            ├─┘├┬┘│ │ │ │ ││││   ││├─┤│ ││ ┬├─┤ │ ├┤ ├┬┘└─┐
            ┴  ┴└─└─┘ ┴ └─┘┘└┘  ─┴┘┴ ┴└─┘└─┘┴ ┴ ┴ └─┘┴└─└─┘*/
            if(current_daughter -> pdgName() == "proton") { //check if current neutron daughter is a proton

              // NEUTRON eDep ----
              neutron_edep += particle -> getDaughter(i) -> startEKin(); //add up proton starting energy as total neutron eDep (current neutron only)
              // -------------

              // printf(" -> (n,p) interaction: %f MeV, (%f) MeV \n",
              //           particle -> getDaughter(i) -> startEKin()/Units::MeV, neutron_edep/Units::MeV);
              
                                  
              for(uint j=0; j<current_daughter -> nDaughters(); ++j) { //loop through all neutrons grand-daughters from protons (these are optical photons)
              
              //check if optical photons end up in photocathode
                if (current_daughter -> getDaughter(j) -> creatorProcess() == "Scintillation"){ //creator process is "Scintillation" (from John's custom physics list)
                  // printf(" -> -> Proton interaction with: %s %s %f eV \n",r
                  //       current_daughter -> getDaughter(j) -> pdgNameCStr(),
                  //       current_daughter -> getDaughter(j) -> creatorProcessCStr(),
                  //       current_daughter -> getDaughter(j) -> startEKin()/Units::eV);

                  auto seg0_p = current_daughter -> getDaughter(j) -> segmentBegin(); //set the starting segment.
                  auto segE_p = current_daughter -> getDaughter(j) -> segmentEnd(); //set the ending segment.
                  auto seg_p = seg0_p; //set starting segment as first in loop.
                  if (seg0_p->volumeName() == "scintillator"){ 
                      c_optphoton_scint += 1; //increment scintillation photon counter in scintillator from protons
                      ++c_opticalph_start_proton;

                    for (++seg_p; seg_p<segE_p; ++seg_p) { 
                      if (seg_p->volumeName() == "photo_cathode"){ 
                        c_optphoton_pc += 1; //increment scintillation photon counter in photo cathode from protons
                        ++c_opticalph_photocathode_proton;
                        break;  //optical photon found, end segment loop and move on to next optical photon.
                      }
                    }
                  }
                }
              }//end proton daughter loop
            }//check if neutron daughter is a proton

            /*
            ┌─┐┌─┐┌┬┐┌┬┐┌─┐  ┌┬┐┌─┐┬ ┬┌─┐┬ ┬┌┬┐┌─┐┬─┐┌─┐
            │ ┬├─┤││││││├─┤   ││├─┤│ ││ ┬├─┤ │ ├┤ ├┬┘└─┐
            └─┘┴ ┴┴ ┴┴ ┴┴ ┴  ─┴┘┴ ┴└─┘└─┘┴ ┴ ┴ └─┘┴└─└─┘*/
            if(current_daughter -> pdgName() != "proton") { //check if current neutron daughter is not a proton.
              for(uint k=0; k<current_daughter -> nDaughters(); ++k) { //loop through all other daughters from non protons (these are optical photons)
              //check if optical photons end up in photocathode
                if (current_daughter -> getDaughter(k) -> creatorProcess() == "Scintillation"){ //creator process is "Scintillation" (from John's custom physics list)
                  auto seg0_g = current_daughter -> getDaughter(k) -> segmentBegin(); //set the starting segment.
                  auto segE_g = current_daughter -> getDaughter(k) -> segmentEnd(); //set the ending segment.
                  auto seg_g = seg0_g; //set starting segment as first in loop.
                  for (++seg_g; seg_g<segE_g; ++seg_g) { 
                    if (seg_g->volumeName() == "photo_cathode"){ 
                      ++c_opticalph_photocathode_other;
                      break; //optical photon found, end segment loop and move on to next optical photon.
                    }
                  }
                } 
              } //end gamma daughter loop
            }//check if neutron daughter is not a proton


          } //end if neutron daughter lenght > 0
        }//end of neutron daughter loop
      } //end if neutron has daughters
      
      
      // printf(" -> Optical photons (from protons) created: %i \n",
      //       c_optphoton_scint);
      // printf(" -> Optical photons (from protons) in PC: %i \n",
      //       c_optphoton_pc);

      if (c_optphoton_pc>0){ //exlude zero count events
        h_sum_optphoton_photocathode_proton -> fill(c_optphoton_pc); //Fill number of scintillation photons making to the photo cathode per neutron.
      }
      if (c_optphoton_scint>0){ //exclude zero count events
        h_sum_optphoton_scintillator_proton -> fill(c_optphoton_scint); //Fill number of scintillation photons created in scintillator per neutron.
      }
      if (neutron_edep>0){ //exlude zero energy events
      h_eDep_neutron_scintillator->fill(neutron_edep/Units::MeV); //Add current proton starting kinetic energy as eDep value to histogram
      }
    } //end of neutron even loop


    /*c_optphoton_scint_gamma = 0 
    ┌─┐┌─┐┌┬┐┌┬┐┌─┐  ┌─┐┬  ┬┌─┐┌┐┌┌┬┐  ┬  ┌─┐┌─┐┌─┐
    │ ┬├─┤││││││├─┤  ├┤ └┐┌┘├┤ │││ │   │  │ ││ │├─┘
    └─┘┴ ┴┴ ┴┴ ┴┴ ┴  └─┘ └┘ └─┘┘└┘ ┴   ┴─┘└─┘└─┘┴  */
    while (auto particle = gammas.next()){
    
      ++c_initial_gammas; //increment number-of-gammas-from-source counter
      auto seg0 = particle -> segmentBegin(); //set the starting segment
      auto segE = particle -> segmentEnd(); //set the ending segment
      auto seg = seg0; //set the current segment





      h_energy_gamma_initial -> fill(seg -> firstStep() -> preEKin()/Units::MeV); //Fill initial gamma energy from source
       
      for (++seg; seg<segE; ++seg) {
	      if (seg -> volumeName() == "scintillator"){ //check if particle is in detector volume
	        ++c_gammas_entering; //increment gammas-entering-detector counter
	      }
      }
      auto segL = particle -> lastSegment();
      if (segL -> volumeName() == "scintillator") { //check if particle stopped inside detector volume
	      ++c_gammas_stopped; //increment gammas-stopped-in-detector counter
      }

      if (particle->nDaughters() != 0) {
        for (uint i = 0; i < particle->nDaughters(); ++i){//loop through gammas daughters
          auto currentDaughter = particle->getDaughter(i);
          // if (currentDaughter -> pdgName() == "e-" && currentDaughter -> creatorProcess() == "compt" && currentDaughter -> startEKin()/Units::keV > 475){
          //       printf("- %s (%f MeV) interact with %s (%s, %f keV)\n", 
          //       currentDaughter->getParent()->pdgNameCStr(), 
          //       currentDaughter->getParent()->startEKin()/Units::MeV,
          //       currentDaughter->pdgNameCStr(), 
          //       currentDaughter->creatorProcessCStr(), 
          //       currentDaughter->startEKin()/Units::keV);
          // }
          if (currentDaughter->nDaughters() != 0) {
            for (uint k = 0; k < currentDaughter->nDaughters(); ++k){//loop through gammas grand-daughters
              if (currentDaughter->getDaughter(k)->creatorProcess() == "Scintillation"){//if the current grand daughter of the gamma ray is a scintillation photon
                
                auto segBegin = currentDaughter -> getDaughter(k) -> segmentBegin(); //set the starting segment
                auto segEnd = currentDaughter -> getDaughter(k) -> segmentEnd(); //set the ending segment
                auto segIt = segBegin; //set the starting segment for the iterator
                
                if (segBegin -> volumeName() == "scintillator"){//check if the scintillation photon originated in the scintillator volume                  
                  counter_optphoton_scintillator_gamma += 1; //increment counter
                }
                for (++segIt; segIt<segEnd; ++segIt) {
                  if (segIt -> volumeName() == "photo_cathode"){
                    counter_optphoton_photocathode_gamma += 1; //increment counter
                    break;
                  }
                }
              }
            }
          }
        }
      }
            // printf("trackID = %i, pdgName = %s, startEKin = %f eV, parentID = %i, creatorProcess = %s, nDaughters = %i\n",
      //         particle->trackID(), 
      //         particle->pdgNameCStr(), 
      //         particle->startEKin()/Units::eV,
      //         particle->parentID(),
      //         particle->creatorProcessCStr(),
      //         particle->nDaughters());


    } //End gamma particle loop 
    if (counter_optphoton_scintillator_gamma>0){ //exclude zero count events
      h_sum_optphoton_scintillator_gamma->fill(counter_optphoton_scintillator_gamma);
    }
    if (counter_optphoton_photocathode_gamma>0){ //exclude zero count events
      h_sum_optphoton_photocathode_gamma->fill(counter_optphoton_photocathode_gamma);
    }

  } //END OF MAIN EVENT LOOP


/*
███████╗ █████╗ ██╗   ██╗███████╗     █████╗ ███████╗    ███████╗██╗███╗   ███╗██████╗ ██╗     ███████╗██╗  ██╗██╗███████╗████████╗
██╔════╝██╔══██╗██║   ██║██╔════╝    ██╔══██╗██╔════╝    ██╔════╝██║████╗ ████║██╔══██╗██║     ██╔════╝██║  ██║██║██╔════╝╚══██╔══╝
███████╗███████║██║   ██║█████╗      ███████║███████╗    ███████╗██║██╔████╔██║██████╔╝██║     █████╗  ███████║██║███████╗   ██║   
╚════██║██╔══██║╚██╗ ██╔╝██╔══╝      ██╔══██║╚════██║    ╚════██║██║██║╚██╔╝██║██╔═══╝ ██║     ██╔══╝  ██╔══██║██║╚════██║   ██║   
███████║██║  ██║ ╚████╔╝ ███████╗    ██║  ██║███████║    ███████║██║██║ ╚═╝ ██║██║     ███████╗███████╗██║  ██║██║███████║   ██║   
╚══════╝╚═╝  ╚═╝  ╚═══╝  ╚══════╝    ╚═╝  ╚═╝╚══════╝    ╚══════╝╚═╝╚═╝     ╚═╝╚═╝     ╚══════╝╚══════╝╚═╝  ╚═╝╚═╝╚══════╝   ╚═╝ */
  hc.saveToFile("analysis_ej321pchar", true); //Save histograms to a file which can be browsed with ess_simplehists_browse:

  return 0;
}


