import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import sys
import os

sys.path.insert(0, "../../library/")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library/")

import processing_simulation as prosim
import processing_data as prodata
import processing_pd as propd

path = '/media/gheed/Seagate_Expansion_Drive1/data/simulation/'
f_pencil_total_edep      = 'pencil_beam_compare/pencil/analysis_ej321pchar_h_eDep_total_scintillator.npz'
f_cone_total_edep     = 'pencil_beam_compare/cone/analysis_ej321pchar_h_eDep_total_scintillator.npz'

f_pencil_sum_opt_photon_scint = 'pencil_beam_compare/pencil/analysis_ej321pchar_h_sum_optphoton_scintillator_total.npz'
f_cone_sum_opt_photon_scint   = 'pencil_beam_compare/cone/analysis_ej321pchar_h_sum_optphoton_scintillator_total.npz'

f_pencil_sum_opt_photon_pc = 'pencil_beam_compare/pencil/analysis_ej321pchar_h_sum_optphoton_photocathode_total.npz'
f_cone_sum_opt_photon_pc   = 'pencil_beam_compare/cone/analysis_ej321pchar_h_sum_optphoton_photocathode_total.npz'

f_pencil_light_yield_edep_scint = 'pencil_beam_compare/pencil/analysis_ej321pchar_h_light_yield_eDep_scintillator.npz'
f_cone_light_yield_edep_scint   = 'pencil_beam_compare/cone/analysis_ej321pchar_h_light_yield_eDep_scintillator.npz'

f_pencil_light_yield_edep_pc = 'pencil_beam_compare/pencil/analysis_ej321pchar_h_light_yield_eDep_photocathode.npz'
f_cone_light_yield_edep_pc   = 'pencil_beam_compare/cone/analysis_ej321pchar_h_light_yield_eDep_photocathode.npz'

f_pencil_light_yield_num_scint = 'pencil_beam_compare/pencil/analysis_ej321pchar_h_light_yield_Num_scintillator.npz'
f_cone_light_yield_num_scint   = 'pencil_beam_compare/cone/analysis_ej321pchar_h_light_yield_Num_scintillator.npz'

f_pencil_light_yield_num_pc = 'pencil_beam_compare/pencil/analysis_ej321pchar_h_light_yield_Num_photocathode.npz'
f_cone_light_yield_num_pc   = 'pencil_beam_compare/cone/analysis_ej321pchar_h_light_yield_Num_photocathode.npz'

#Compare total eDep - total opt. photon sum (scintillator and PC) - light yield (scintillator and PC)

pencil_total_edep = np.load(path+f_pencil_total_edep)
cone_total_edep = np.load(path+f_cone_total_edep)

pencil_sum_opt_photon_scint = np.load(path+f_pencil_sum_opt_photon_scint)
cone_sum_opt_photon_scint = np.load(path+f_cone_sum_opt_photon_scint)
pencil_sum_opt_photon_pc = np.load(path+f_pencil_sum_opt_photon_pc)
cone_sum_opt_photon_pc = np.load(path+f_cone_sum_opt_photon_pc)

pencil_light_yield_edep_scint = np.load(path+f_pencil_light_yield_edep_scint)
cone_light_yield_edep_scint = np.load(path+f_cone_light_yield_edep_scint)
pencil_light_yield_edep_pc = np.load(path+f_pencil_light_yield_edep_pc)
cone_light_yield_edep_pc = np.load(path+f_cone_light_yield_edep_pc)

pencil_light_yield_num_scint = np.load(path+f_pencil_light_yield_num_scint)
cone_light_yield_num_scint = np.load(path+f_cone_light_yield_num_scint)
pencil_light_yield_num_pc = np.load(path+f_pencil_light_yield_num_pc)
cone_light_yield_num_pc = np.load(path+f_cone_light_yield_num_pc)


plt.figure(0)
plt.title('eDep, 10000 evts.')
plt.plot(prodata.getBinCenters(pencil_total_edep['bin_edges']), pencil_total_edep['counts'], drawstyle='steps',  color='red', label='pencil beam')
plt.plot(prodata.getBinCenters(cone_total_edep['bin_edges']), cone_total_edep['counts'], drawstyle='steps', color='blue', label='cone beam')
plt.xlabel('Energy [MeVee]')
plt.ylabel('Counts')
plt.legend()

plt.figure(1)
plt.title('Sum opt. photons, 10000 evts.')
plt.step(prodata.getBinCenters(pencil_sum_opt_photon_scint['bin_edges']) , pencil_sum_opt_photon_scint['counts'], '-b', lw=2, label='pencil beam, in scintillator')
plt.step(prodata.getBinCenters(cone_sum_opt_photon_scint['bin_edges']) , cone_sum_opt_photon_scint['counts'], '--b', lw=2, label='cone beam, in scintillator')
plt.step(prodata.getBinCenters(pencil_sum_opt_photon_pc['bin_edges']) , pencil_sum_opt_photon_pc['counts'], '-r', lw=2, label='pencil beam, in photocathode')
plt.step(prodata.getBinCenters(cone_sum_opt_photon_pc['bin_edges']) , cone_sum_opt_photon_pc['counts'], '--r', lw=2, label='cone beam, in photocathode')
plt.xlabel('Num. opt. photons')
plt.ylabel('Counts')
plt.legend()

plt.figure(2)
plt.title('Light yield eDep, 10000 evts.')
plt.step(prodata.getBinCenters(pencil_light_yield_edep_scint['bin_edges']) , pencil_light_yield_edep_scint['counts'], '-b', lw=2, label='pencil beam, in scintillator')
plt.step(prodata.getBinCenters(cone_light_yield_edep_scint['bin_edges']) , cone_light_yield_edep_scint['counts'], '--b', lw=2, label='cone beam, in scintillatoe')
plt.step(prodata.getBinCenters(pencil_light_yield_edep_pc['bin_edges']) , pencil_light_yield_edep_pc['counts'], '-r', lw=2, label='pencil beam, in photocathode')
plt.step(prodata.getBinCenters(cone_light_yield_edep_pc['bin_edges']) , cone_light_yield_edep_pc['counts'], '--r', lw=2, label='cone beam, in photocathode')
plt.xlabel('Light Yield [eV]/[MeVee]')
plt.ylabel('Counts')
plt.legend()

plt.figure(3)
plt.title('Light yield Num, 10000 evts.')
plt.step(prodata.getBinCenters(pencil_light_yield_num_scint['bin_edges']) , pencil_light_yield_num_scint['counts'], '-b', lw=2,  label='pencil beam, in scintillator')
plt.step(prodata.getBinCenters(cone_light_yield_num_scint['bin_edges']) , cone_light_yield_num_scint['counts'], '--b', lw=2, label='cone beam, in scintillator')
plt.step(prodata.getBinCenters(pencil_light_yield_num_pc['bin_edges']) , pencil_light_yield_num_pc['counts'], '-r', lw=2, label='pencil beam, in photocathode')
plt.step(prodata.getBinCenters(cone_light_yield_num_pc['bin_edges']) , cone_light_yield_num_pc['counts'], '--r', lw=2, label='cone beam, in photocathode')
plt.xlabel('Light Yield [Num]/[MeVee]')
plt.ylabel('Counts')
plt.legend()

plt.show()