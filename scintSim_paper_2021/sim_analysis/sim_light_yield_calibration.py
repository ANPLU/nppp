import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import sys
import os

sys.path.insert(0, "../../library/")
# sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library/")
# sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/nicholai_plotting_scripts/")

import processing_simulation as prosim
import nicholai_plot_helper as nplt
import processing_data as prodata
import processing_pd as propd


"""
This script is used to calibrate the number of sicntillation photons from GEANT4 simulation to charge at the photocathode.
"""

#PMT Paramters:
PMT_QE = .3 #Quantum efficiency of photocathode, 30% at ~350 nm peak (9821B p1)
PMT_gain = 7e6 #Nominal gain is 7'000'000 at nominal A/lm (9821B p1)
att_factor = 6.31 #used when attenuating the signal before the digitizer

path = '/media/gheed/Seagate_Expansion_Drive1/data/simulation/'

#Importing scintillation photons at photocathode data.
df_pen_Cs137_pc_CS = prosim.loadData(path + 'Cs137/pencilbeam/analysis_ej321pchar_h_sum_optphoton_photocathode_total_CS.npz') #Data with Compton scattering cut
df_pen_Cs137_pc_CS_QE = prosim.loadData(path + 'Cs137/pencilbeam/analysis_ej321pchar_h_sum_optphoton_photocathode_total_CS_QE.npz') #Data with Compton scattering and Quantum efficiency cut
df_pen_Cs137_pc_QE = prosim.loadData(path + 'Cs137/pencilbeam/analysis_ej321pchar_h_sum_optphoton_photocathode_total_QE.npz') #Data with Quantum efficiency cut
df_iso_Cs137_pc_CS = prosim.loadData(path + 'Cs137/isotropic/analysis_ej321pchar_h_sum_optphoton_photocathode_total_CS.npz') #Data with Compton scattering cut
df_iso_Cs137_pc_CS_QE = prosim.loadData(path + 'Cs137/isotropic/analysis_ej321pchar_h_sum_optphoton_photocathode_total_CS_QE.npz') #Data with Compton scattering and Quantum efficiency cut
df_iso_Cs137_pc_QE = prosim.loadData(path + 'Cs137/isotropic/analysis_ej321pchar_h_sum_optphoton_photocathode_total_QE.npz') #Data with Quantum efficiency cut


plt.figure(0)
plt.title('optphoton @ photocathode (source: Cs137 - pencilbeam)')
plt.step(df_pen_Cs137_pc_CS.bin_centers/att_factor, df_pen_Cs137_pc_CS.counts, lw = 3, label = 'CS cut')
plt.step(df_pen_Cs137_pc_QE.bin_centers/att_factor, df_pen_Cs137_pc_QE.counts, lw = 3, label = 'QE cut')
plt.step(df_pen_Cs137_pc_CS_QE.bin_centers/att_factor, df_pen_Cs137_pc_CS_QE.counts, lw = 3, label = 'CS+QE cut')
plt.yscale('log')
plt.legend()
plt.ylabel('counts')
plt.xlabel('Number of optphotons')

plt.figure(1)
plt.title('optphoton @ photocathode (source: Cs137 - isotropic)')
plt.step(df_iso_Cs137_pc_CS.bin_centers/att_factor, df_iso_Cs137_pc_CS.counts, lw = 3, label = 'CS cut')
plt.step(df_iso_Cs137_pc_QE.bin_centers/att_factor, df_iso_Cs137_pc_QE.counts, lw = 3, label = 'QE cut')
plt.step(df_iso_Cs137_pc_CS_QE.bin_centers/att_factor, df_iso_Cs137_pc_CS_QE.counts, lw = 3, label = 'CS+QE cut')
plt.yscale('log')
plt.legend()
plt.ylabel('counts')
plt.xlabel('Number of optphotons')

plt.figure(2)
plt.title('optphoton @ photocathode CS+QE cut, (source: Cs137)')
plt.step(df_pen_Cs137_pc_CS_QE.bin_centers/att_factor*PMT_gain, df_pen_Cs137_pc_CS_QE.counts, lw = 3, label = 'pencilbeam')
plt.step(df_iso_Cs137_pc_CS_QE.bin_centers/att_factor*PMT_gain, df_iso_Cs137_pc_CS_QE.counts, lw = 3, label = 'isotropic')
plt.yscale('log')
plt.legend()
plt.ylabel('counts')
plt.xlabel('Number of optphotons')



plt.show()


#Figure showing the scintillation emission spectra and QE of PMT.
# fig, ax1 = plt.subplots()
# ax1.set_title('EJ321P emission spectra and 9821B_p1 EQ')
# ax1.set_xlabel('wavelenght [nm]')
# ax1.set_ylabel('counts', color='blue')
# ax1.step(df_Cs137_wavelenght_optphoton_initial.bin_centers, df_Cs137_wavelenght_optphoton_initial.counts, lw = 2, color='blue', label='EJ321P emission spectra')
# ax1.tick_params(axis='y', labelcolor='blue')
# ax2 = ax1.twinx()
# ax2.set_ylabel('QE [%]', color='red')
# ax2.plot(df_Cs137_wavelenght_optphoton_initial.bin_centers, prosim.getQE(df_Cs137_wavelenght_optphoton_initial.bin_centers)*100, lw = 2, color='red', label='9821B_p1 QE')
# ax2.tick_params(axis='y', labelcolor='red')
# ax2.set_ylim([5,35])
# fig.tight_layout() 
# ax2.grid(which='both')
# plt.show()