import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import sys
import os

sys.path.insert(0, "../../library/")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library/")

import processing_simulation as prosim
"""
Script is used to show the light emission spectrum of NE213A scintillator
"""

# a = prosim.NE213A_emission_spectra()

wl = np.arange(380,520, 4.516129032)
int = prosim.NE213A_emission_spectra(wl)
c = 299792458 #speed of light m/s
h = 4.1357e-15#Plancks constant eV*s

plt.plot(wl, int)
plt.show()

# for i in wl:
    # print(f"{round(prosim.NE213A_emission_spectra(wavelenght=i).item(),2)},")
    # print(f"{round(i,2)}*nm,")

