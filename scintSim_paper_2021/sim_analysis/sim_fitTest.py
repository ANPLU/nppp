#!/usr/bin/env python3
import sys
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit
from scipy.stats import skewnorm
from scipy.special import erf

# IMPORT FROM LIBRARY DIRECTORY
sys.path.insert(0, "../library/")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
# sys.path.insert(0, "sim_analysis/")
# sys.path.insert(0, "data_analysis/")

import nicholai_plot_helper as nplt
import processing_utility as prout
import processing_math as promath
import processing_data as prodata
import processing_pd as propd
import processing_simulation as prosim
import gaussEdgeFit_parametersEJ321P as geEJ321P
import gaussEdgeFit_parametersNE213A as geNE213A
import simFit_parametersNE213A as simNE213A

"""
Script for testing fitting of optical photons simulated data with non Gaussian functions.
"""


PMT_gain = 7e6 #Nominal gain is 7'000'000 at nominal A/lm (model: 9821B p1)
att_factor = 5.93679379 #used when attenuating the signal before the digitizer.
att_factor_cosmic = 12.13059163# #attenuation factor used when taking cosmic muon data.
q = 1.60217662E-19 #charge of a single electron
plot = False

# LOADING SIMULATION DATA
###############################
path_sim = '/media/gheed/Seagate_Expansion_Drive1/data/simulation/'
df_Cs137_662_sim_pen =      prosim.loadData(path_sim + 'NE213A/Cs137/pencilbeam/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE.npz') #
df_Na22_sim_pen =           prosim.loadData(path_sim + 'NE213A/Na22/pencilbeam/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE.npz') #
df_Na22_511_sim_pen =       prosim.loadData(path_sim + 'NE213A/Na22_511/pencilbeam/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE.npz') #
df_Na22_1275_sim_pen =      prosim.loadData(path_sim + 'NE213A/Na22_1275/pencilbeam/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE.npz') #
df_Co60_sim_pen =           prosim.loadData(path_sim + 'NE213A/Co60/pencilbeam/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE.npz') #
df_Co60_1173_sim_pen =      prosim.loadData(path_sim + 'NE213A/Co60_1173/pencilbeam/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE.npz') #
df_Co60_1332_sim_pen =      prosim.loadData(path_sim + 'NE213A/Co60_1332/pencilbeam/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE.npz') #
df_2230_sim_pen =           prosim.loadData(path_sim + 'NE213A/2_22MeV/pencilbeam/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE.npz') #
df_2615_sim_pen =           prosim.loadData(path_sim + 'NE213A/2_62MeV/pencilbeam/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE.npz') #
df_PuBe_mixed_sim_pen =     prosim.loadData(path_sim + 'NE213A/PuBe_mixed/pencilbeam/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE.npz') #
df_4439_sim_pen =           prosim.loadData(path_sim + 'NE213A/4_44MeV/pencilbeam/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE.npz') #
df_6130_sim_pen =           prosim.loadData(path_sim + 'NE213A/6_13MeV/pencilbeam/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE.npz')
df_cosmic_sim =             prosim.loadData(path_sim + 'NE213A/cosmic/3GeV/analysis_ej321pchar_optphoton_sum_photocathode_no_cut_QE.npz')
#add another data set for 4.44 MeV gammas to increase statistics
df_4439_sim_pen.counts =    prosim.loadData(path_sim + 'NE213A/4_44MeV/pencilbeam/part2/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE.npz').counts
df_4439_sim_pen.counts =    prosim.loadData(path_sim + 'NE213A/4_44MeV/pencilbeam/part3/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE.npz').counts
df_4439_sim_pen.counts =    prosim.loadData(path_sim + 'NE213A/4_44MeV/pencilbeam/part4/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE.npz').counts
#add another data set for 6.13 MeV gammas to increase statistics
df_6130_sim_pen.counts =    prosim.loadData(path_sim + 'NE213A/6_13MeV/pencilbeam/part2/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE.npz').counts
df_6130_sim_pen.counts =    prosim.loadData(path_sim + 'NE213A/6_13MeV/pencilbeam/part3/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE.npz').counts
#add another data set for 2.615 MeV gammas to increase statistics
df_2615_sim_pen.counts =    prosim.loadData(path_sim + 'NE213A/2_62MeV/pencilbeam/part2/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE.npz').counts #
df_2615_sim_pen.counts =    prosim.loadData(path_sim + 'NE213A/2_62MeV/pencilbeam/part3/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE.npz').counts #

df_Cs137_662_sim_iso =      prosim.loadData(path_sim + 'NE213A/Cs137/isotropic/analysis_ej321pchar_optphoton_sum_photocathode_no_cut_QE.npz') #
df_Na22_sim_iso =           prosim.loadData(path_sim + 'NE213A/Na22/isotropic/analysis_ej321pchar_optphoton_sum_photocathode_no_cut_QE.npz') #
df_Na22_511_sim_iso =       prosim.loadData(path_sim + 'NE213A/Na22_511/isotropic/analysis_ej321pchar_optphoton_sum_photocathode_no_cut_QE.npz') #
df_Na22_1275_sim_iso =      prosim.loadData(path_sim + 'NE213A/Na22_1275/isotropic/analysis_ej321pchar_optphoton_sum_photocathode_no_cut_QE.npz') #
df_Co60_sim_iso =           prosim.loadData(path_sim + 'NE213A/Co60/isotropic/analysis_ej321pchar_optphoton_sum_photocathode_no_cut_QE.npz') #
df_Co60_1173_sim_iso =      prosim.loadData(path_sim + 'NE213A/Co60_1173/isotropic/analysis_ej321pchar_optphoton_sum_photocathode_no_cut_QE.npz') #
df_Co60_1332_sim_iso =      prosim.loadData(path_sim + 'NE213A/Co60_1332/isotropic/analysis_ej321pchar_optphoton_sum_photocathode_no_cut_QE.npz') #
df_2230_sim_iso =           prosim.loadData(path_sim + 'NE213A/2_22MeV/isotropic/analysis_ej321pchar_optphoton_sum_photocathode_no_cut_QE.npz') #
df_2615_sim_iso =           prosim.loadData(path_sim + 'NE213A/2_62MeV/isotropic/analysis_ej321pchar_optphoton_sum_photocathode_no_cut_QE.npz') #
df_PuBe_mixed_sim_iso =     prosim.loadData(path_sim + 'NE213A/PuBe_mixed/isotropic/analysis_ej321pchar_optphoton_sum_photocathode_no_cut_QE.npz') #
df_4439_sim_iso =           prosim.loadData(path_sim + 'NE213A/4_44MeV/isotropic/analysis_ej321pchar_optphoton_sum_photocathode_no_cut_QE.npz') #
df_6130_sim_iso =           prosim.loadData(path_sim + 'NE213A/6_13MeV/isotropic/analysis_ej321pchar_optphoton_sum_photocathode_no_cut_QE.npz')

#####################################
# CALIBRATING SIMULATION DATA TO QDC
#####################################

# # # # # # # # # # # # # # # # # # # # #
# OFFSET PARAMETER FOR SIMULATED SPECTRA 
simScaleQDC_NE213A = .278
# # # # # # # # # # # # # # # # # # # # #

#calibrate simulated spectra to QDC channels
#isotropic
df_Cs137_662_sim_pen.bin_centers =      prosim.chargeCalibration(df_Cs137_662_sim_pen.bin_centers   * q * PMT_gain * simScaleQDC_NE213A)
df_Na22_sim_pen.bin_centers =           prosim.chargeCalibration(df_Na22_sim_pen.bin_centers        * q * PMT_gain * simScaleQDC_NE213A)
df_Na22_511_sim_pen.bin_centers =       prosim.chargeCalibration(df_Na22_511_sim_pen.bin_centers    * q * PMT_gain * simScaleQDC_NE213A)
df_Na22_1275_sim_pen.bin_centers =      prosim.chargeCalibration(df_Na22_1275_sim_pen.bin_centers   * q * PMT_gain * simScaleQDC_NE213A)
df_Co60_sim_pen.bin_centers =           prosim.chargeCalibration(df_Co60_sim_pen.bin_centers        * q * PMT_gain * simScaleQDC_NE213A)
df_Co60_1173_sim_pen.bin_centers =      prosim.chargeCalibration(df_Co60_1173_sim_pen.bin_centers   * q * PMT_gain * simScaleQDC_NE213A)
df_Co60_1332_sim_pen.bin_centers =      prosim.chargeCalibration(df_Co60_1332_sim_pen.bin_centers   * q * PMT_gain * simScaleQDC_NE213A)
df_2230_sim_pen.bin_centers =           prosim.chargeCalibration(df_2230_sim_pen.bin_centers        * q * PMT_gain * simScaleQDC_NE213A)
df_2615_sim_pen.bin_centers =           prosim.chargeCalibration(df_2615_sim_pen.bin_centers        * q * PMT_gain * simScaleQDC_NE213A)
# df_PuBe_mixed_sim_pen.bin_centers =     prosim.chargeCalibration(df_PuBe_mixed_sim_pen.bin_centers  * q * PMT_gain * simScaleQDC_NE213A)
df_4439_sim_pen.bin_centers =           prosim.chargeCalibration(df_4439_sim_pen.bin_centers        * q * PMT_gain * simScaleQDC_NE213A)
df_6130_sim_pen.bin_centers =           prosim.chargeCalibration(df_6130_sim_pen.bin_centers        * q * PMT_gain * simScaleQDC_NE213A)
df_cosmic_sim.bin_centers =             prosim.chargeCalibration(df_cosmic_sim.bin_centers          * q * PMT_gain * simScaleQDC_NE213A)

#pencilbeam
df_Cs137_662_sim_iso.bin_centers =      prosim.chargeCalibration(df_Cs137_662_sim_iso.bin_centers   * q * PMT_gain * simScaleQDC_NE213A)
df_Na22_sim_iso.bin_centers =           prosim.chargeCalibration(df_Na22_sim_iso.bin_centers        * q * PMT_gain * simScaleQDC_NE213A)
df_Na22_511_sim_iso.bin_centers =       prosim.chargeCalibration(df_Na22_511_sim_iso.bin_centers    * q * PMT_gain * simScaleQDC_NE213A)
df_Na22_1275_sim_iso.bin_centers =      prosim.chargeCalibration(df_Na22_1275_sim_iso.bin_centers   * q * PMT_gain * simScaleQDC_NE213A)
df_Co60_sim_iso.bin_centers =           prosim.chargeCalibration(df_Co60_sim_iso.bin_centers        * q * PMT_gain * simScaleQDC_NE213A)
df_Co60_1173_sim_iso.bin_centers =      prosim.chargeCalibration(df_Co60_1173_sim_iso.bin_centers   * q * PMT_gain * simScaleQDC_NE213A)
df_Co60_1332_sim_iso.bin_centers =      prosim.chargeCalibration(df_Co60_1332_sim_iso.bin_centers   * q * PMT_gain * simScaleQDC_NE213A)
df_2230_sim_iso.bin_centers =           prosim.chargeCalibration(df_2230_sim_iso.bin_centers        * q * PMT_gain * simScaleQDC_NE213A)
df_2615_sim_iso.bin_centers =           prosim.chargeCalibration(df_2615_sim_iso.bin_centers        * q * PMT_gain * simScaleQDC_NE213A)
df_PuBe_mixed_sim_iso.bin_centers =     prosim.chargeCalibration(df_PuBe_mixed_sim_iso.bin_centers  * q * PMT_gain * simScaleQDC_NE213A)
df_4439_sim_iso.bin_centers =           prosim.chargeCalibration(df_4439_sim_iso.bin_centers        * q * PMT_gain * simScaleQDC_NE213A)
df_6130_sim_iso.bin_centers =           prosim.chargeCalibration(df_6130_sim_iso.bin_centers        * q * PMT_gain * simScaleQDC_NE213A)

##############################
# FITTING SIMULATED DATA
##############################
sim_res_Cs137_662 = simNE213A.Cs137_662_LG( df_Cs137_662_sim_pen.bin_centers,   df_Cs137_662_sim_pen.counts,    plot=plot)
sim_res_Na22_511 =  simNE213A.Na22_511_LG(  df_Na22_511_sim_pen.bin_centers,    df_Na22_511_sim_pen.counts,     plot=plot)
sim_res_Na22_1275 = simNE213A.Na22_1275_LG( df_Na22_1275_sim_pen.bin_centers,   df_Na22_1275_sim_pen.counts,    plot=plot)
sim_res_Co60_1173 = simNE213A.Co60_1173_LG( df_Co60_1173_sim_pen.bin_centers,   df_Co60_1173_sim_pen.counts,    plot=plot)
sim_res_Co60_1332 = simNE213A.Co60_1332_LG( df_Co60_1332_sim_pen.bin_centers,   df_Co60_1332_sim_pen.counts,    plot=plot)
sim_res_PuBe_2230 = simNE213A.PuBe_2230_LG( df_2230_sim_pen.bin_centers,        df_2230_sim_pen.counts,         plot=plot)
sim_res_PuBe_2615 = simNE213A.PuBe_2615_LG( df_2615_sim_pen.bin_centers,        df_2615_sim_pen.counts,         plot=plot)
sim_res_PuBe_4439 = simNE213A.PuBe_4439_LG( df_4439_sim_pen.bin_centers,        df_4439_sim_pen.counts,         plot=plot)
sim_res_PuBe_6130 = simNE213A.PuBe_6130_LG( df_6130_sim_pen.bin_centers,        df_6130_sim_pen.counts,         plot=plot)
sim_res_cosmic =    simNE213A.cosmic_LG( df_cosmic_sim.bin_centers,             df_cosmic_sim.counts,           plot=plot)





popt, pcov = curve_fit(promath.expGaussFunc, df_Na22_1275_sim_pen.bin_centers[20:80], df_Na22_1275_sim_pen.counts[20:80], p0=[800, 2600, 300, 100])



plt.figure()
plt.step(df_Na22_1275_sim_pen.bin_centers, df_Na22_1275_sim_pen.counts)
plt.plot(np.arange(0,20000), promath.expGaussFunc(np.arange(0,20000), popt[0], popt[1], popt[2], popt[3]), lw=2, label='expGauss')
plt.plot(np.arange(0,20000), promath.gaussFunc(np.arange(0,20000), popt[0], popt[1]+popt[3], popt[2]), lw=2,  label='Gauss')
plt.legend()
plt.show()
