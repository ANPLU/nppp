import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import sys
import os

sys.path.insert(0, "../../library/")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library/")

import processing_simulation as prosim


#Emission spectra before increasing nuber of data points for the intensity and also scaling the right side of the distribution.

eljen_wl = np.arange(400, 502.5, 2.5) #Wavelenghts
eljen_int = values=[0.000, 0.043, 0.105, 0.147, 0.239, #Values taken from Eljen website
                0.429, 0.534, 0.648, 0.917, 1.000,  
                1.000, 0.979, 0.955, 0.913, 0.850,  
                0.819, 0.734, 0.696, 0.628, 0.591,  
                0.544, 0.507, 0.472, 0.451, 0.433,  
                0.412, 0.375, 0.345, 0.287, 0.257,  
                0.220, 0.193, 0.174, 0.154, 0.120,  
                0.110, 0.099, 0.088, 0.083, 0.060,   
                0.055]   

path = '/media/gheed/Seagate_Expansion_Drive1/data/simulation/'
filename_before = 'emission_comparison_EJ321P/before/analysis_ej321pchar_h_wavelength_optphoton_initial.npz'
filename_after  = 'emission_comparison_EJ321P/after/analysis_ej321pchar_h_wavelength_optphoton_initial.npz'#increased num entries from 21 to 41.
filename_after2 = 'emission_comparison_EJ321P/after2/analysis_ej321pchar_h_wavelength_optphoton_initial.npz' #41 entries and scaled the right side of the distribution

#Importing eDep values
df_before   = prosim.unbinning_npz(path+filename_before, binName = 'bin_edges', countsName = 'counts')
df_after    = prosim.unbinning_npz(path+filename_after, binName = 'bin_edges', countsName = 'counts')
df_after2   = prosim.unbinning_npz(path+filename_after2, binName = 'bin_edges', countsName = 'counts')

#Calculate the difference between ELJEN spectra and my simulated spectra 
# NOTE: this is to get a custom scaling array to applie to the th G4 code.
# hist, bin_edges = np.histogram(df_after, bins=numBins, range=binRange, weights=w)
# diff = np.arange(0)
# for i in range(len(hist)):
#     diff = np.append(diff, eljen_int[i]/hist[i])

# plt.plot(diff)
# plt.show()


plt.figure(0)
numBins = 41
binRange = [400, 500]
# w = np.empty(len(df_after)) #Make array same length as data set
# w.fill(1/91809)
# plt.hist(df_after, bins = numBins, range = binRange, weights = w, alpha = .5, label = 'After: 41 entries')

w = np.empty(len(df_after2)) #Make array same length as data set
w.fill(1/16779)
plt.hist(df_after2, bins = numBins, range = binRange, weights = w, alpha = .5, label = 'After: 41 entries + custom scaling')

w = np.empty(len(df_before)) #Make array same length as data set
w.fill(1/87890)
plt.hist(df_before, bins = numBins, range = binRange, weights = w, alpha = .5, label = 'Before: 21 entries')

plt.plot(eljen_wl, eljen_int, color='black', lw=2, label='Eljen Documentation')

# plt.plot(bin_edges[:-1], diff)
# plt.yscale('log')
plt.legend()
plt.title('Emission spectrum optical photons EJ321P')
plt.ylabel('counts')
plt.xlabel('Wavelength [nm]')
plt.show()

#Comparing original with modified emission spectrum intensity for EJ321P


# path = '/media/gheed/Seagate_Expansion_Drive1/data/simulation/'
# filename_modified_large     = 'emission_comparison_EJ321P/intensity_test/analysis_ej321pchar_modified_large_h_wavelength_optphoton_initial.npz'
# filename_modified           = 'emission_comparison_EJ321P/intensity_test/analysis_ej321pchar_modified_h_wavelength_optphoton_initial.npz'
# filename_original           = 'emission_comparison_EJ321P/intensity_test/analysis_ej321pchar_original_h_wavelength_optphoton_initial.npz'
# filename_modified_small     = 'emission_comparison_EJ321P/intensity_test/analysis_ej321pchar_modified_small_h_wavelength_optphoton_initial.npz'

# df_modified_large   = prosim.unbinning_npz(path+filename_modified_large, binName = 'bin_edges', countsName = 'counts')#using a factor 21.43
# df_modified_small   = prosim.unbinning_npz(path+filename_modified_small, binName = 'bin_edges', countsName = 'counts')#using a factor 1/1000
# df_modified         = prosim.unbinning_npz(path+filename_modified, binName = 'bin_edges', countsName = 'counts')#using a factor 21.43
# df_original         = prosim.unbinning_npz(path+filename_original, binName = 'bin_edges', countsName = 'counts')


# numBins = 41
# binRange = [400, 500]
# plt.figure(1)

# w = np.empty(len(df_original)) #Make array same length as data set
# w.fill(1/12892)
# plt.hist(df_original, bins=numBins, range=binRange, weights=w, histtype='step', lw=2, label='Original')

# w = np.empty(len(df_modified)) #Make array same length as data set
# w.fill(1/13663)
# plt.hist(df_modified, bins=numBins, range=binRange, weights=w, histtype='step', lw=2, label='Modified x21.43')

# w = np.empty(len(df_modified_large)) #Make array same length as data set
# w.fill(1/13880)
# plt.hist(df_modified_large, bins=numBins, range=binRange, weights=w, histtype='step', lw=2, label='Modified x1000')

# w = np.empty(len(df_modified_small)) #Make array same length as data set
# w.fill(1/241751)
# plt.hist(df_modified_small, bins=numBins, range=binRange, weights=w, histtype='step', lw=2, label='Modified x1/1000')

# plt.plot(eljen_wl, eljen_int, color='black', lw=2, label='Eljen Documented spectra')
# plt.legend()
# plt.show()

