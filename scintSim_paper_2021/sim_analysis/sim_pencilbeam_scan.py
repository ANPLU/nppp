import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import sys
import os

sys.path.insert(0, "../../library/")
# sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library/")
# sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/nicholai_plotting_scripts/")

import processing_simulation as prosim
import nicholai_plot_helper as nplt
import processing_data as prodata
import processing_pd as propd


"""
This script is used to compare simulations where the pencil beam (Cs137) is scanned across the surface of the detector (EJ321P)
"""

#PMT Paramters:
PMT_QE = .3 #Quantum efficiency of photocathode, 30% at ~350 nm peak (model: 9821B p1)
PMT_gain = 7e6 #Nominal gain is 7'000'000 at nominal A/lm (model: 9821B p1)
att_factor = 6.31 #used when attenuating the signal before the digitizer
q = 1.60217662E-19 #charge of a single electron


################################
# DETECTOR = EJ321P
################################

#Importing GEANT4 data
path_sim = '/media/gheed/Seagate_Expansion_Drive1/data/simulation/'
df_x0_pc_CS_QE = prosim.loadData(path_sim + 'Cs137/pencilbeam/analysis_ej321pchar_h_sum_optphoton_photocathode_total_CS_QE.npz') #Data with Compton scattering and Quantum efficiency cut

df_x10_pc_CS_QE = prosim.loadData(path_sim + 'Cs137/scanning/x10/analysis_ej321pchar_h_sum_optphoton_photocathode_total_CS_QE.npz') #Data with Compton scattering and Quantum efficiency cut

df_x20_pc_CS_QE = prosim.loadData(path_sim + 'Cs137/scanning/x20/analysis_ej321pchar_h_sum_optphoton_photocathode_total_CS_QE.npz') #Data with Compton scattering and Quantum efficiency cut

df_x30_pc_CS_QE = prosim.loadData(path_sim + 'Cs137/scanning/x30/analysis_ej321pchar_h_sum_optphoton_photocathode_total_CS_QE.npz') #Data with Compton scattering and Quantum efficiency cut

df_x40_pc_CS_QE = prosim.loadData(path_sim + 'Cs137/scanning/x40/analysis_ej321pchar_h_sum_optphoton_photocathode_total_CS_QE.npz') #Data with Compton scattering and Quantum efficiency cut

df_iso_pc_CS_QE = prosim.loadData(path_sim + 'Cs137/isotropic/analysis_ej321pchar_h_sum_optphoton_photocathode_total_CS_QE.npz') #Data with Compton scattering and Quantum efficiency cut

#normalize counts based on area and normalize to central irradiation.
a0  = np.pi*5**2
a10 = (np.pi*15**2-np.pi*5**2)/a0
a20 = (np.pi*25**2-np.pi*15**2)/a0
a30 = (np.pi*35**2-np.pi*25**2)/a0
a40 = (np.pi*45**2-np.pi*35**2)/a0

#calculate the convolution of all simulated steps along x in one spectra.
df_convoluted_pc_CS_QE_bin_centers = np.arange(0,2048,2)
df_convoluted_pc_CS_QE_counts = np.zeros(1024)
for i in range(1024):
    df_convoluted_pc_CS_QE_counts[i] += df_x0_pc_CS_QE.counts[i]*a0/a0
    df_convoluted_pc_CS_QE_counts[i] += df_x10_pc_CS_QE.counts[i]*a10
    df_convoluted_pc_CS_QE_counts[i] += df_x20_pc_CS_QE.counts[i]*a20
    df_convoluted_pc_CS_QE_counts[i] += df_x30_pc_CS_QE.counts[i]*a30
    df_convoluted_pc_CS_QE_counts[i] += df_x40_pc_CS_QE.counts[i]*a40
    

#convering simulation charge to QDC values and correcting for PMT gain
df_x0_pc_CS_QE.bin_centers    = prosim.chargeCalibration(df_x0_pc_CS_QE.bin_centers * q * PMT_gain)
df_x10_pc_CS_QE.bin_centers   = prosim.chargeCalibration(df_x10_pc_CS_QE.bin_centers * q * PMT_gain)
df_x20_pc_CS_QE.bin_centers   = prosim.chargeCalibration(df_x20_pc_CS_QE.bin_centers * q * PMT_gain)
df_x30_pc_CS_QE.bin_centers   = prosim.chargeCalibration(df_x30_pc_CS_QE.bin_centers * q * PMT_gain)
df_x40_pc_CS_QE.bin_centers   = prosim.chargeCalibration(df_x40_pc_CS_QE.bin_centers * q * PMT_gain)
df_iso_pc_CS_QE.bin_centers   = prosim.chargeCalibration(df_iso_pc_CS_QE.bin_centers * q * PMT_gain)
df_convoluted_pc_CS_QE_bin_centers = prosim.chargeCalibration(df_convoluted_pc_CS_QE_bin_centers * q * PMT_gain)

#Importing real data (EJ321P)
path_data = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop
df_Cs137_data = propd.load_parquet_merge(path_data, [1098, 1099, 1100, 1101, 1102, 1103], full=False) #load and merge all separete data files

#Plotting the results
plt.figure(0)
plt.suptitle('EJ321P: charge @ photocathode')

plt.subplot(2,1,1)
plt.title('pencil beam')
plt.step(df_x0_pc_CS_QE.bin_centers,  df_x0_pc_CS_QE.counts,  lw = 2, label = 'Sim: Cs-137, x=0mm, QDC cal.')
plt.step(df_x10_pc_CS_QE.bin_centers, df_x10_pc_CS_QE.counts, lw = 2, label = 'Sim: Cs-137, x=10mm, QDC cal.')
plt.step(df_x20_pc_CS_QE.bin_centers, df_x20_pc_CS_QE.counts, lw = 2, label = 'Sim: Cs-137, x=20mm, QDC cal.')
plt.step(df_x30_pc_CS_QE.bin_centers, df_x30_pc_CS_QE.counts, lw = 2, label = 'Sim: Cs-137, x=30mm, QDC cal.')
plt.step(df_x40_pc_CS_QE.bin_centers, df_x40_pc_CS_QE.counts, lw = 2, label = 'Sim: Cs-137, x=40mm, QDC cal.')
plt.hist(df_Cs137_data.qdc_lg_ch1, bins=500, range=[0,200000], lw=2, histtype='step', label = 'Data: Cs-137')
plt.yscale('log')
plt.legend()
plt.ylabel('counts')
plt.xlabel('QDC [arb. units]')
plt.xlim([-10e3, 100e3])

plt.subplot(2,1,2)
plt.title('isotropic beam compared')
plt.step(df_iso_pc_CS_QE.bin_centers/2.5,  df_iso_pc_CS_QE.counts/.2*.8,  lw = 2, label = 'Sim: Cs-137, isotropic, QDC cal.')
plt.step(df_convoluted_pc_CS_QE_bin_centers/2.5, df_convoluted_pc_CS_QE_counts*0.007/.2*0.8, lw = 2, label = 'Sim: Cs-137, convoluted pencil, area normalized, QDC cal.')
plt.hist(df_Cs137_data.qdc_lg_ch1, bins=1000, range=[0,40000], lw=2, histtype='step', label = 'Data: Cs-137')
# plt.yscale('log')
plt.legend()
plt.ylabel('counts')
plt.xlabel('QDC [arb. units]')
plt.xlim([-10e3, 100e3])
plt.ylim([0,1300])
# plt.ylim([0, 300])
plt.show()