import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import sys
import os

sys.path.insert(0, "../../library/")
# sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library/")
# sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/nicholai_plotting_scripts/")

import processing_simulation as prosim
import nicholai_plot_helper as nplt
import processing_data as prodata
import processing_pd as propd


"""
This script is used to compare simulations of different neutron energies pencilbeam and isotropic
"""

#PMT Paramters:
PMT_QE = .3 #Quantum efficiency of photocathode, 30% at ~350 nm peak (model: 9821B p1)
PMT_gain = 7e6 #Nominal gain is 7'000'000 at nominal A/lm (model: 9821B p1)
att_factor = 6.31 #used when attenuating the signal before the digitizer
q = 1.60217662E-19 #charge of a single electron


################################
# DETECTOR = EJ321P
################################

#Importing GEANT4 isotropic data
path_sim = '/media/gheed/Seagate_Expansion_Drive1/data/simulation/'
df_iso_edep_1MeV = prosim.loadData(path_sim + 'neutrons/isotropic/1MeV/analysis_ej321pchar_neutron_eDep_scintillator.npz') #Neutron eDep
df_iso_edep_2MeV = prosim.loadData(path_sim + 'neutrons/isotropic/2MeV/analysis_ej321pchar_neutron_eDep_scintillator.npz') #Neutron eDep
df_iso_edep_3MeV = prosim.loadData(path_sim + 'neutrons/isotropic/3MeV/analysis_ej321pchar_neutron_eDep_scintillator.npz') #Neutron eDep
df_iso_edep_4MeV = prosim.loadData(path_sim + 'neutrons/isotropic/4MeV/analysis_ej321pchar_neutron_eDep_scintillator.npz') #Neutron eDep
df_iso_edep_5MeV = prosim.loadData(path_sim + 'neutrons/isotropic/5MeV/analysis_ej321pchar_neutron_eDep_scintillator.npz') #Neutron eDep
df_iso_edep_6MeV = prosim.loadData(path_sim + 'neutrons/isotropic/6MeV/analysis_ej321pchar_neutron_eDep_scintillator.npz') #Neutron eDep
df_iso_edep_7MeV = prosim.loadData(path_sim + 'neutrons/isotropic/7MeV/analysis_ej321pchar_neutron_eDep_scintillator.npz') #Neutron eDep
df_iso_PC_1MeV = prosim.loadData(path_sim + 'neutrons/isotropic/1MeV/analysis_ej321pchar_optphoton_sum_photocathode_no_cut_QE.npz') #Neutron eDep
df_iso_PC_2MeV = prosim.loadData(path_sim + 'neutrons/isotropic/2MeV/analysis_ej321pchar_optphoton_sum_photocathode_no_cut_QE.npz') #Neutron eDep
df_iso_PC_3MeV = prosim.loadData(path_sim + 'neutrons/isotropic/3MeV/analysis_ej321pchar_optphoton_sum_photocathode_no_cut_QE.npz') #Neutron eDep
df_iso_PC_4MeV = prosim.loadData(path_sim + 'neutrons/isotropic/4MeV/analysis_ej321pchar_optphoton_sum_photocathode_no_cut_QE.npz') #Neutron eDep
df_iso_PC_5MeV = prosim.loadData(path_sim + 'neutrons/isotropic/5MeV/analysis_ej321pchar_optphoton_sum_photocathode_no_cut_QE.npz') #Neutron eDep
df_iso_PC_6MeV = prosim.loadData(path_sim + 'neutrons/isotropic/6MeV/analysis_ej321pchar_optphoton_sum_photocathode_no_cut_QE.npz') #Neutron eDep
df_iso_PC_7MeV = prosim.loadData(path_sim + 'neutrons/isotropic/7MeV/analysis_ej321pchar_optphoton_sum_photocathode_no_cut_QE.npz') #Neutron eDep

#Importing GEANT4 pencilbeam data
# path_sim = '/media/gheed/Seagate_Expansion_Drive1/data/simulation/'
# df_pen_edep_1MeV = prosim.loadData(path_sim + 'neutrons/pencilbeam/1MeV/analysis_ej321pchar_neutron_eDep_scintillator.npz') #Neutron eDep
# df_pen_edep_2MeV = prosim.loadData(path_sim + 'neutrons/pencilbeam/2MeV/analysis_ej321pchar_neutron_eDep_scintillator.npz') #Neutron eDep
# df_pen_edep_3MeV = prosim.loadData(path_sim + 'neutrons/pencilbeam/3MeV/analysis_ej321pchar_neutron_eDep_scintillator.npz') #Neutron eDep
# df_pen_edep_4MeV = prosim.loadData(path_sim + 'neutrons/pencilbeam/4MeV/analysis_ej321pchar_neutron_eDep_scintillator.npz') #Neutron eDep
# df_pen_edep_5MeV = prosim.loadData(path_sim + 'neutrons/pencilbeam/5MeV/analysis_ej321pchar_neutron_eDep_scintillator.npz') #Neutron eDep
# df_pen_edep_6MeV = prosim.loadData(path_sim + 'neutrons/pencilbeam/6MeV/analysis_ej321pchar_neutron_eDep_scintillator.npz') #Neutron eDep
# df_pen_edep_7MeV = prosim.loadData(path_sim + 'neutrons/pencilbeam/7MeV/analysis_ej321pchar_neutron_eDep_scintillator.npz') #Neutron eDep

# df_pen_PC_1MeV = prosim.loadData(path_sim + 'neutrons/pencilbeam/1MeV/analysis_ej321pchar_optphoton_sum_photocathode_no_cut_QE.npz') #Neutron eDep
# df_pen_PC_2MeV = prosim.loadData(path_sim + 'neutrons/pencilbeam/2MeV/analysis_ej321pchar_optphoton_sum_photocathode_no_cut_QE.npz') #Neutron eDep
# df_pen_PC_3MeV = prosim.loadData(path_sim + 'neutrons/pencilbeam/3MeV/analysis_ej321pchar_optphoton_sum_photocathode_no_cut_QE.npz') #Neutron eDep
# df_pen_PC_4MeV = prosim.loadData(path_sim + 'neutrons/pencilbeam/4MeV/analysis_ej321pchar_optphoton_sum_photocathode_no_cut_QE.npz') #Neutron eDep
# df_pen_PC_5MeV = prosim.loadData(path_sim + 'neutrons/pencilbeam/5MeV/analysis_ej321pchar_optphoton_sum_photocathode_no_cut_QE.npz') #Neutron eDep
# df_pen_PC_6MeV = prosim.loadData(path_sim + 'neutrons/pencilbeam/6MeV/analysis_ej321pchar_optphoton_sum_photocathode_no_cut_QE.npz') #Neutron eDep
# df_pen_PC_7MeV = prosim.loadData(path_sim + 'neutrons/pencilbeam/7MeV/analysis_ej321pchar_optphoton_sum_photocathode_no_cut_QE.npz') #Neutron eDep



#convering simulation charge to QDC values and correcting for PMT gain
df_iso_PC_1MeV.bin_centers = prosim.chargeCalibration(df_iso_PC_1MeV.bin_centers * q * PMT_gain)
df_iso_PC_2MeV.bin_centers = prosim.chargeCalibration(df_iso_PC_2MeV.bin_centers * q * PMT_gain)
df_iso_PC_3MeV.bin_centers = prosim.chargeCalibration(df_iso_PC_3MeV.bin_centers * q * PMT_gain)
df_iso_PC_4MeV.bin_centers = prosim.chargeCalibration(df_iso_PC_4MeV.bin_centers * q * PMT_gain)
df_iso_PC_5MeV.bin_centers = prosim.chargeCalibration(df_iso_PC_5MeV.bin_centers * q * PMT_gain)
df_iso_PC_6MeV.bin_centers = prosim.chargeCalibration(df_iso_PC_6MeV.bin_centers * q * PMT_gain)
df_iso_PC_7MeV.bin_centers = prosim.chargeCalibration(df_iso_PC_7MeV.bin_centers * q * PMT_gain)

# df_pen_PC_1MeV.bin_centers = prosim.chargeCalibration(df_pen_PC_1MeV.bin_centers * q * PMT_gain)
# df_pen_PC_2MeV.bin_centers = prosim.chargeCalibration(df_pen_PC_2MeV.bin_centers * q * PMT_gain)
# df_pen_PC_3MeV.bin_centers = prosim.chargeCalibration(df_pen_PC_3MeV.bin_centers * q * PMT_gain)
# df_pen_PC_4MeV.bin_centers = prosim.chargeCalibration(df_pen_PC_4MeV.bin_centers * q * PMT_gain)
# df_pen_PC_5MeV.bin_centers = prosim.chargeCalibration(df_pen_PC_5MeV.bin_centers * q * PMT_gain)
# df_pen_PC_6MeV.bin_centers = prosim.chargeCalibration(df_pen_PC_6MeV.bin_centers * q * PMT_gain)
# df_pen_PC_7MeV.bin_centers = prosim.chargeCalibration(df_pen_PC_7MeV.bin_centers * q * PMT_gain)

#Importing real data (EJ321P)
# path_data = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop
# df_Cs137_data = propd.load_parquet_merge(path_data, [1098, 1099, 1100, 1101, 1102, 1103], full=False) #load and merge all separete data files

#Plotting the results
plt.figure(0)
plt.suptitle('EJ321P: neutron edep')
plt.subplot(2,1,1)
plt.title('pencil beam')
# plt.step(df_pen_edep_1MeV.bin_centers,  df_pen_edep_1MeV.counts,  lw = 2, label = f'Sim: neutron edep, 1MeV, Evts = {df_pen_edep_1MeV.counts.sum()}')
# plt.step(df_pen_edep_2MeV.bin_centers,  df_pen_edep_2MeV.counts,  lw = 2, label = f'Sim: neutron edep, 2MeV, Evts = {df_pen_edep_2MeV.counts.sum()}')
# plt.step(df_pen_edep_3MeV.bin_centers,  df_pen_edep_3MeV.counts,  lw = 2, label = f'Sim: neutron edep, 3MeV, Evts = {df_pen_edep_3MeV.counts.sum()}')
# plt.step(df_pen_edep_4MeV.bin_centers,  df_pen_edep_4MeV.counts,  lw = 2, label = f'Sim: neutron edep, 4MeV, Evts = {df_pen_edep_4MeV.counts.sum()}')
# plt.step(df_pen_edep_5MeV.bin_centers,  df_pen_edep_5MeV.counts,  lw = 2, label = f'Sim: neutron edep, 5MeV, Evts = {df_pen_edep_5MeV.counts.sum()}')
# plt.step(df_pen_edep_6MeV.bin_centers,  df_pen_edep_6MeV.counts,  lw = 2, label = f'Sim: neutron edep, 6MeV, Evts = {df_pen_edep_6MeV.counts.sum()}')
# plt.step(df_pen_edep_7MeV.bin_centers,  df_pen_edep_7MeV.counts,  lw = 2, label = f'Sim: neutron edep, 7MeV, Evts = {df_pen_edep_7MeV.counts.sum()}')
plt.legend()
# plt.yscale('log')
plt.ylabel('counts')
plt.xlabel('Energy [MeV]')
# plt.xlim([-10e3, 100e3])
plt.subplot(2,1,2)
plt.title('isotropic beam')
plt.step(df_iso_edep_1MeV.bin_centers,  df_iso_edep_1MeV.counts,  lw = 2, label = f'Sim: neutron edep, 1MeV, Evts = {df_iso_edep_1MeV.counts.sum()}')
plt.step(df_iso_edep_2MeV.bin_centers,  df_iso_edep_2MeV.counts,  lw = 2, label = f'Sim: neutron edep, 2MeV, Evts = {df_iso_edep_2MeV.counts.sum()}')
plt.step(df_iso_edep_3MeV.bin_centers,  df_iso_edep_3MeV.counts,  lw = 2, label = f'Sim: neutron edep, 3MeV, Evts = {df_iso_edep_3MeV.counts.sum()}')
plt.step(df_iso_edep_4MeV.bin_centers,  df_iso_edep_4MeV.counts,  lw = 2, label = f'Sim: neutron edep, 4MeV, Evts = {df_iso_edep_4MeV.counts.sum()}')
plt.step(df_iso_edep_5MeV.bin_centers,  df_iso_edep_5MeV.counts,  lw = 2, label = f'Sim: neutron edep, 5MeV, Evts = {df_iso_edep_5MeV.counts.sum()}')
plt.step(df_iso_edep_6MeV.bin_centers,  df_iso_edep_6MeV.counts,  lw = 2, label = f'Sim: neutron edep, 6MeV, Evts = {df_iso_edep_6MeV.counts.sum()}')
plt.step(df_iso_edep_7MeV.bin_centers,  df_iso_edep_7MeV.counts,  lw = 2, label = f'Sim: neutron edep, 7MeV, Evts = {df_iso_edep_7MeV.counts.sum()}')
plt.legend()
# plt.yscale('log')
plt.ylabel('counts')
plt.xlabel('Energy [MeV]')
# plt.xlim([-10e3, 100e3])



#Plotting the results
plt.figure(1)
plt.suptitle('EJ321P: neutron scintillation light @ PC')
plt.subplot(2,1,1)
plt.title('pencil beam')
# plt.step(df_pen_PC_1MeV.bin_centers,  df_pen_PC_1MeV.counts,  lw = 2, label = f'Sim: 1MeV, Evts = {df_pen_PC_1MeV.counts.sum()}')
# plt.step(df_pen_PC_2MeV.bin_centers,  df_pen_PC_2MeV.counts,  lw = 2, label = f'Sim: 2MeV, Evts = {df_pen_PC_2MeV.counts.sum()}')
# plt.step(df_pen_PC_3MeV.bin_centers,  df_pen_PC_3MeV.counts,  lw = 2, label = f'Sim: 3MeV, Evts = {df_pen_PC_3MeV.counts.sum()}')
# plt.step(df_pen_PC_4MeV.bin_centers,  df_pen_PC_4MeV.counts,  lw = 2, label = f'Sim: 4MeV, Evts = {df_pen_PC_4MeV.counts.sum()}')
# plt.step(df_pen_PC_5MeV.bin_centers,  df_pen_PC_5MeV.counts,  lw = 2, label = f'Sim: 5MeV, Evts = {df_pen_PC_5MeV.counts.sum()}')
# plt.step(df_pen_PC_6MeV.bin_centers,  df_pen_PC_6MeV.counts,  lw = 2, label = f'Sim: 6MeV, Evts = {df_pen_PC_6MeV.counts.sum()}')
# plt.step(df_pen_PC_7MeV.bin_centers,  df_pen_PC_7MeV.counts,  lw = 2, label = f'Sim: 7MeV, Evts = {df_pen_PC_7MeV.counts.sum()}')
plt.legend()
plt.yscale('log')
plt.ylabel('counts')
plt.xlabel('Energy [MeV]')
plt.xlim([-1e3, 100e3])

plt.subplot(2,1,2)
plt.title('isotropic beam')
plt.step(df_iso_PC_1MeV.bin_centers,  df_iso_PC_1MeV.counts,  lw = 2, label = f'Sim: 1MeV, Evts = {df_iso_PC_1MeV.counts.sum()}')
plt.step(df_iso_PC_2MeV.bin_centers,  df_iso_PC_2MeV.counts,  lw = 2, label = f'Sim: 2MeV, Evts = {df_iso_PC_2MeV.counts.sum()}')
plt.step(df_iso_PC_3MeV.bin_centers,  df_iso_PC_3MeV.counts,  lw = 2, label = f'Sim: 3MeV, Evts = {df_iso_PC_3MeV.counts.sum()}')
plt.step(df_iso_PC_4MeV.bin_centers,  df_iso_PC_4MeV.counts,  lw = 2, label = f'Sim: 4MeV, Evts = {df_iso_PC_4MeV.counts.sum()}')
plt.step(df_iso_PC_5MeV.bin_centers,  df_iso_PC_5MeV.counts,  lw = 2, label = f'Sim: 5MeV, Evts = {df_iso_PC_5MeV.counts.sum()}')
plt.step(df_iso_PC_6MeV.bin_centers,  df_iso_PC_6MeV.counts,  lw = 2, label = f'Sim: 6MeV, Evts = {df_iso_PC_6MeV.counts.sum()}')
plt.step(df_iso_PC_7MeV.bin_centers,  df_iso_PC_7MeV.counts,  lw = 2, label = f'Sim: 7MeV, Evts = {df_iso_PC_7MeV.counts.sum()}')
plt.legend()
plt.yscale('log')
plt.ylabel('counts')
plt.xlim([-1e3, 100e3])
plt.xlabel('QDC [arb. units]')
# plt.xlim([-10e3, 100e3])

plt.show()