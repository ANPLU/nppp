import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import sys
import os

sys.path.insert(0, "../../library/")
# sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library/")
# sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/nicholai_plotting_scripts/")

import processing_simulation as prosim
import nicholai_plot_helper as nplt
import processing_data as prodata
import processing_pd as propd


"""
This script is used to compare simulations of different neutron energies pencilbeam and isotropic
"""

#PMT Paramters:
PMT_QE = .3 #Quantum efficiency of photocathode, 30% at ~350 nm peak (model: 9821B p1)
PMT_gain = 7e6 #Nominal gain is 7'000'000 at nominal A/lm (model: 9821B p1)
att_factor = 6.31 #used when attenuating the signal before the digitizer
q = 1.60217662E-19 #charge of a single electron


################################
# DETECTOR = EJ321P
################################

#Importing GEANT4 isotropic data
path_sim = '/media/gheed/Seagate_Expansion_Drive1/data/simulation/'
df_1k = prosim.loadData(path_sim + 'TMP/1000evt/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE.npz') 
df_10k = prosim.loadData(path_sim + 'TMP/10000evt/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE.npz') 
df_100k = prosim.loadData(path_sim + 'TMP/100000evt/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE.npz') 
df_1M = prosim.loadData(path_sim + 'Cs137/isotropic/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE.npz') 


plt.figure(0)
plt.title('Cs-137 source (isotropic)')
plt.step(df_1k.bin_centers, df_1k.counts, lw=2, label='1k events')
plt.step(df_10k.bin_centers, df_10k.counts, lw=2, label='10k events')
plt.step(df_100k.bin_centers, df_100k.counts, lw=2, label='100k events')
plt.step(df_1M.bin_centers, df_1M.counts, lw=2, label='1M events')
plt.yscale('log')
plt.xlabel('#optphotons, CS+QE @ PC')
plt.ylabel('counts')
plt.xlim([-10, 200])
plt.legend()
plt.show()