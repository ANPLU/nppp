#include <iostream>
#include <array>
#include <iterator>

//Define data for QE vs wavelength
std::array<const float, 57> wl = {280.49968,284.16044,287.82715,289.66495,292.42462,296.08836,298.851,300.6992,302.54888,304.39856,305.34863,309.0094,309.95354,310.8947,313.65438,316.4096,319.17373,319.20787,322.88348,325.63425,328.39689,330.24657,333.90734,338.4781,343.0444,348.50586,356.68245,366.65825,376.63109,384.7869,392.02975,400.17962,410.13613,419.18264,429.12578,437.2623,443.57882,452.61345,459.83849,467.05313,476.08183,483.29647,491.41962,499.54278,510.37513,517.59274,526.61699,534.73865,543.75547,550.97606,557.28664,567.22385,575.35888,585.31241,600.70662,618.82487,626.07663};
std::array<const float, 57> QE = {0.00000,0.01058,0.02334,0.03119,0.04395,0.05573,0.06948,0.08077,0.09255,0.10433,0.11857,0.12937,0.14164,0.15293,0.1657,0.17699,0.19122,0.20251,0.21822,0.22804,0.24178,0.25356,0.26436,0.27614,0.28645,0.29282,0.29723,0.29673,0.29524,0.29278,0.28835,0.28392,0.27703,0.26917,0.25786,0.24901,0.2382,0.22641,0.21609,0.20233,0.18857,0.17482,0.16155,0.14828,0.13108,0.11831,0.10308,0.08932,0.07163,0.05984,0.04707,0.0338,0.02446,0.01659,0.00822,0.00084,0.00000};

float QuantumEff(float wavelength){
//Function which determines the quantum efficiency of the current photon wavelength.
//Will return 0 or 1 based on the quantum efficiency.
//used together with an optical photon counter, this will take QE of the PMT into acount.
    for(auto& i: wl){
        if (wavelength < i){
            return QE.at(std::distance(std::begin(wl),&i));
        }
                double randomnum = rand()/double(RAND_MAX);
        std::cout << i << ": " << std::distance(std::begin(wl),&i) << ": "<< randomnum << std::endl;
    }
    return 0;
}

int main() {
    std::cout << QuantumEff(297.) << std::endl;
    std::cout << QuantumEff(297.) << std::endl;

}

//Use rand 0-1 and check if its greater or lower than QE value.
