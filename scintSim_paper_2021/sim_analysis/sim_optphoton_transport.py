import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import seaborn as sns
sns.set(style="ticks", color_codes=True)
import sys
import os

sys.path.insert(0, "../../library/")
# sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library/")
# sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/nicholai_plotting_scripts/")

import processing_simulation as prosim
import nicholai_plot_helper as nplt
import processing_data as prodata
import processing_pd as propd

#PMT Paramters:
PMT_QE = .3 #Quantum efficiency of photocathode, 30% at ~350 nm peak (9821B p1)
PMT_gain = 7e6 #Nominal gain is 7'000'000 at nominal A/lm (9821B p1)
att_factor = 6.31 #used when attenuating the signal before the digitizer

path = '/media/gheed/Seagate_Expansion_Drive1/data/simulation/'
filename_Cs137_enters = 'Cs137/isotropic/analysis_ej321pchar_h_qualitative_optphoton_transport.npz' 
filename_Cs137_ends   = 'Cs137/isotropic/analysis_ej321pchar_h_qualitative_optphoton_end.npz'
# qualitative
#Importing eDep values
df_enters = np.load(path+filename_Cs137_enters, allow_pickle = True)
df_enters = df_enters['countdict'].item()

df_ends = np.load(path+filename_Cs137_ends, allow_pickle = True)
df_ends = df_ends['countdict'].item()


enters_scintillator = df_enters['Created in Scintillator']
enters_scintpaint   = df_enters['Enters Scint Paint'] 
enters_alcup        = df_enters['Enters Al Cup']
enters_borglass     = df_enters['Enters Borated glass']
enters_lightguide   = df_enters['Enters Light guide']
enters_LGpaint      = df_enters['Enters LG Paint']
enters_PMT          = df_enters['Enters PMT glass']
enters_PC           = df_enters['Enters Photocathode']

ends_scintillator   = df_ends['Ends in Scintillator']
ends_scintpaint     = df_ends['Ends in Scint Paint']
ends_alcup          = df_ends['Ends Al Cup']
ends_borglass       = df_ends['Ends in Borated glass']
ends_lightguide     = df_ends['Ends in Light guide']
ends_LGpaint        = df_ends['Ends in LG paint']
ends_PMT            = df_ends['Ends in PMT glass']
ends_PC             = df_ends['Ends in Photocathode']


#calculating the sum of optical photons !! SANITY CHECK !!
killRatio = [ends_scintillator/enters_scintillator*100, 
            ends_scintpaint/enters_scintpaint*100,
            0,
            ends_borglass/enters_borglass*100, 
            ends_lightguide/enters_lightguide*100, 
            ends_LGpaint/enters_LGpaint*100, 
            ends_PMT/enters_PMT*100]


plt.figure(0)
plt.suptitle('Optical photon transmission study')
plt.subplot(3,1,1)
# plt.title('Scintillation photons enters (10k evts.)')
plt.bar(list(df_enters.keys()), list(df_enters.values()), color='blue', label='Enters volume')
plt.ylabel('counts')
plt.legend()
plt.tight_layout()

plt.subplot(3,1,2)
# plt.title('Scintillation photons ends (10k evts.)')
plt.bar(list(df_ends.keys()), list(df_ends.values()), color='red', label='Ends in volume')
plt.ylabel('counts')
plt.legend()
plt.yscale('log')
# plt.ylim(bottom=0.1)
plt.tight_layout()

plt.subplot(3,1,3)
# plt.title('Kill ratio')
plt.bar(['Scintillator', 'Scint. Paint', 'Al Cup', 'Borated Glass', 'Light Guide', 'LG Paint', 'PMT window'], killRatio, color='black', label='Kill ratio')
plt.ylabel('Ratio [%]')
plt.legend()
plt.ylim([0.00001,100])
plt.yscale('log')
plt.tight_layout()

plt.show()

