import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import sys
import os

sys.path.insert(0, "../../library/")
# sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library/")
# sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/nicholai_plotting_scripts/")

import processing_simulation as prosim
import nicholai_plot_helper as nplt
import processing_data as prodata
import processing_pd as propd


"""
This script is used to compare simulations where the depth of the interaction is used as the variable.
Several depth cuts have been made and are compared here. The result which is studied, is the number
of optphotons which reaches the photocathode.
"""

#PMT Paramters:
PMT_QE = .3 #Quantum efficiency of photocathode, 30% at ~350 nm peak (model: 9821B p1)
PMT_gain = 7e6 #Nominal gain is 7'000'000 at nominal A/lm (model: 9821B p1)
att_factor = 6.31 #used when attenuating the signal before the digitizer
q = 1.60217662E-19 #charge of a single electron


################################
# DETECTOR = EJ321P
################################

#Importing GEANT4 data
path_sim = '/media/gheed/Seagate_Expansion_Drive1/data/simulation/'
df_pen_normal_pc_CS_QE  = prosim.loadData(path_sim + 'Cs137/pencilbeam/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE.npz') #Data with Compton scattering and Quantum efficiency cut
df_pen_1_pc_CS_QE       = prosim.loadData(path_sim + 'Cs137/pencilbeam/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE_Zcut_1.npz') #Data with Compton scattering and Quantum efficiency cut
df_pen_2_pc_CS_QE       = prosim.loadData(path_sim + 'Cs137/pencilbeam/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE_Zcut_2.npz') #Data with Compton scattering and Quantum efficiency cut
df_pen_3_pc_CS_QE       = prosim.loadData(path_sim + 'Cs137/pencilbeam/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE_Zcut_3.npz') #Data with Compton scattering and Quantum efficiency cut
df_pen_4_pc_CS_QE       = prosim.loadData(path_sim + 'Cs137/pencilbeam/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE_Zcut_4.npz') #Data with Compton scattering and Quantum efficiency cut
df_pen_5_pc_CS_QE       = prosim.loadData(path_sim + 'Cs137/pencilbeam/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE_Zcut_5.npz') #Data with Compton scattering and Quantum efficiency cut
df_pen_6_pc_CS_QE       = prosim.loadData(path_sim + 'Cs137/pencilbeam/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE_Zcut_6.npz') #Data with Compton scattering and Quantum efficiency cut
df_pen_depth            = prosim.loadData(path_sim + 'Cs137/pencilbeam/analysis_ej321pchar_gamma_interaction_Z.npz') #Data with Compton scattering and Quantum efficiency cut
df_pen_normal_scint_CS  = prosim.loadData(path_sim + 'Cs137/pencilbeam/analysis_ej321pchar_optphoton_sum_scintillator_compton.npz') #Data with Compton scattering and Quantum efficiency cut

# df_iso_normal_pc_CS_QE  = prosim.loadData(path_sim + 'Cs137/isotropic/analysis_ej321pchar_h_sum_optphoton_photocathode_total_CS_QE.npz') #Data with Compton scattering and Quantum efficiency cut
# df_iso_1_pc_CS_QE       = prosim.loadData(path_sim + 'Cs137/depth_cut/isotropic/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE_Zcut_1.npz') #Data with Compton scattering and Quantum efficiency cut
# df_iso_2_pc_CS_QE       = prosim.loadData(path_sim + 'Cs137/depth_cut/isotropic/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE_Zcut_2.npz') #Data with Compton scattering and Quantum efficiency cut
# df_iso_3_pc_CS_QE       = prosim.loadData(path_sim + 'Cs137/depth_cut/isotropic/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE_Zcut_3.npz') #Data with Compton scattering and Quantum efficiency cut
# df_iso_4_pc_CS_QE       = prosim.loadData(path_sim + 'Cs137/depth_cut/isotropic/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE_Zcut_4.npz') #Data with Compton scattering and Quantum efficiency cut
# df_iso_5_pc_CS_QE       = prosim.loadData(path_sim + 'Cs137/depth_cut/isotropic/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE_Zcut_5.npz') #Data with Compton scattering and Quantum efficiency cut
# df_iso_6_pc_CS_QE       = prosim.loadData(path_sim + 'Cs137/depth_cut/isotropic/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE_Zcut_6.npz') #Data with Compton scattering and Quantum efficiency cut
# df_iso_depth            = prosim.loadData(path_sim + 'Cs137/depth_cut/isotropic/analysis_ej321pchar_gamma_interaction_Z.npz') #Data with Compton scattering and Quantum efficiency cut


#convering simulation charge to QDC values and correcting for PMT gain
df_pen_normal_pc_CS_QE.bin_centers  = prosim.chargeCalibration(df_pen_normal_pc_CS_QE.bin_centers * q * PMT_gain)
df_pen_1_pc_CS_QE.bin_centers       = prosim.chargeCalibration(df_pen_1_pc_CS_QE.bin_centers * q * PMT_gain)
df_pen_2_pc_CS_QE.bin_centers       = prosim.chargeCalibration(df_pen_2_pc_CS_QE.bin_centers * q * PMT_gain)
df_pen_3_pc_CS_QE.bin_centers       = prosim.chargeCalibration(df_pen_3_pc_CS_QE.bin_centers * q * PMT_gain)
df_pen_4_pc_CS_QE.bin_centers       = prosim.chargeCalibration(df_pen_4_pc_CS_QE.bin_centers * q * PMT_gain)
df_pen_5_pc_CS_QE.bin_centers       = prosim.chargeCalibration(df_pen_5_pc_CS_QE.bin_centers * q * PMT_gain)
df_pen_6_pc_CS_QE.bin_centers       = prosim.chargeCalibration(df_pen_6_pc_CS_QE.bin_centers * q * PMT_gain)
df_pen_normal_scint_CS.bin_centers  = prosim.chargeCalibration(df_pen_normal_scint_CS.bin_centers * q * PMT_gain)
# df_iso_normal_pc_CS_QE.bin_centers  = prosim.chargeCalibration(df_iso_normal_pc_CS_QE.bin_centers * q * PMT_gain)
# df_iso_1_pc_CS_QE.bin_centers       = prosim.chargeCalibration(df_iso_1_pc_CS_QE.bin_centers * q * PMT_gain)
# df_iso_2_pc_CS_QE.bin_centers       = prosim.chargeCalibration(df_iso_2_pc_CS_QE.bin_centers * q * PMT_gain)
# df_iso_3_pc_CS_QE.bin_centers       = prosim.chargeCalibration(df_iso_3_pc_CS_QE.bin_centers * q * PMT_gain)
# df_iso_4_pc_CS_QE.bin_centers       = prosim.chargeCalibration(df_iso_4_pc_CS_QE.bin_centers * q * PMT_gain)
# df_iso_5_pc_CS_QE.bin_centers       = prosim.chargeCalibration(df_iso_5_pc_CS_QE.bin_centers * q * PMT_gain)
# df_iso_6_pc_CS_QE.bin_centers       = prosim.chargeCalibration(df_iso_6_pc_CS_QE.bin_centers * q * PMT_gain)

#Importing real data (EJ321P)
# path_data = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop
# df_Cs137_data = propd.load_parquet_merge(path_data, [1098, 1099, 1100, 1101, 1102, 1103], full=False) #load and merge all separete data files
print('--------------------------------')
print(f"@ PC CS+QE sum = {np.trapz(df_pen_normal_pc_CS_QE.counts, df_pen_normal_pc_CS_QE.bin_centers)}")
print(f"@ PC CS+QE Zcuts sum = {np.trapz(df_pen_1_pc_CS_QE.counts,df_pen_1_pc_CS_QE.bin_centers)+np.trapz(df_pen_2_pc_CS_QE.counts,df_pen_2_pc_CS_QE.bin_centers)+np.trapz(df_pen_3_pc_CS_QE.counts,df_pen_3_pc_CS_QE.bin_centers)+np.trapz(df_pen_4_pc_CS_QE.counts,df_pen_4_pc_CS_QE.bin_centers)+np.trapz(df_pen_5_pc_CS_QE.counts,df_pen_5_pc_CS_QE.bin_centers)+np.trapz(df_pen_6_pc_CS_QE.counts,df_pen_6_pc_CS_QE.bin_centers)}")
print('--------------------------------')
df_pen_normal_scint_CS
#Plotting the results
plt.figure(0)
plt.suptitle('EJ321P')
plt.subplot(2,1,1)
plt.title('pencil beam')
plt.step(df_pen_normal_pc_CS_QE.bin_centers, df_pen_normal_pc_CS_QE.counts,  lw = 3, label = 'Sim: Cs-137, normal, QDC cal.')
plt.step(df_pen_1_pc_CS_QE.bin_centers, df_pen_1_pc_CS_QE.counts*4.8, lw = 3, label = 'Sim: Cs-137, Z=0-10mm, QDC cal.')
plt.step(df_pen_2_pc_CS_QE.bin_centers, df_pen_2_pc_CS_QE.counts*4.8, lw = 3, label = 'Sim: Cs-137, Z=10-20mm, QDC cal.')
plt.step(df_pen_3_pc_CS_QE.bin_centers, df_pen_3_pc_CS_QE.counts*4.8, lw = 3, label = 'Sim: Cs-137, Z=20-30mm, QDC cal.')
plt.step(df_pen_4_pc_CS_QE.bin_centers, df_pen_4_pc_CS_QE.counts*4.8, lw = 3, label = 'Sim: Cs-137, Z=30-40mm, QDC cal.')
plt.step(df_pen_5_pc_CS_QE.bin_centers, df_pen_5_pc_CS_QE.counts*4.8, lw = 3, label = 'Sim: Cs-137, Z=40-50mm, QDC cal.')
plt.step(df_pen_6_pc_CS_QE.bin_centers, df_pen_6_pc_CS_QE.counts*4.8, lw = 3, label = 'Sim: Cs-137, Z=50-60mm, QDC cal.')
# plt.hist(df_Cs137_data.qdc_lg_ch1, bins=500, range=[0,40000], lw=2, histtype='step', label = 'Data: Cs-137')
# plt.yscale('log')
plt.legend()
plt.ylabel('counts')
plt.xlabel('QDC [arb. units]')
plt.xlim([-5e3, 30e3])
plt.ylim([-1e1, 700])
plt.subplot(2,1,2)
plt.title('pencil beam')
plt.step(df_pen_depth.bin_centers, df_pen_depth.counts, lw=2, label = 'Sim: Interaction depth z-axis')
plt.yscale('log')
plt.legend()
plt.ylabel('counts')
plt.xlim([-50,50])
plt.xlabel('Interaction depth [mm]')


# plt.figure(1)
# plt.suptitle('EJ321P')
# plt.subplot(2,1,1)
# plt.title('pencil beam')
# plt.step(df_iso_normal_pc_CS_QE.bin_centers, df_iso_normal_pc_CS_QE.counts,  lw = 3, label = 'Sim: Cs-137, normal, QDC cal.')
# plt.step(df_iso_1_pc_CS_QE.bin_centers, df_iso_1_pc_CS_QE.counts, lw = 3, label = 'Sim: Cs-137, Z=0-10mm, QDC cal.')
# plt.step(df_iso_2_pc_CS_QE.bin_centers, df_iso_2_pc_CS_QE.counts, lw = 3, label = 'Sim: Cs-137, Z=10-20mm, QDC cal.')
# plt.step(df_iso_3_pc_CS_QE.bin_centers, df_iso_3_pc_CS_QE.counts, lw = 3, label = 'Sim: Cs-137, Z=20-30mm, QDC cal.')
# plt.step(df_iso_4_pc_CS_QE.bin_centers, df_iso_4_pc_CS_QE.counts, lw = 3, label = 'Sim: Cs-137, Z=30-40mm, QDC cal.')
# plt.step(df_iso_5_pc_CS_QE.bin_centers, df_iso_5_pc_CS_QE.counts, lw = 3, label = 'Sim: Cs-137, Z=40-50mm, QDC cal.')
# plt.step(df_iso_6_pc_CS_QE.bin_centers, df_iso_6_pc_CS_QE.counts, lw = 3, label = 'Sim: Cs-137, Z=50-60mm, QDC cal.')
# # plt.hist(df_Cs137_data.qdc_lg_ch1, bins=500, range=[0,40000], lw=2, histtype='step', label = 'Data: Cs-137')
# plt.yscale('log')
# plt.legend()
# plt.ylabel('counts')
# plt.xlabel('QDC [arb. units]')
# plt.xlim([-10e3, 100e3])
# plt.subplot(2,1,2)
# plt.title('pencil beam')
# plt.step(df_iso_depth.bin_centers, df_iso_depth.counts, lw=2, label = 'Sim: Interaction depth z-axis')
# plt.yscale('log')
# plt.legend()
# plt.ylabel('counts')
# plt.xlabel('Interaction depth [mm]')
# plt.xlim([-50,50])
plt.show()