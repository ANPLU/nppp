import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import sys
import os

sys.path.insert(0, "../../library/")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library/")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/nicholai_plotting_scripts/")

import gaussEdgeFit_parametersSimulation as gefSim
import processing_simulation as prosim
import nicholai_plot_helper as nplt
import processing_data as prodata
import processing_pd as propd

path = '/media/gheed/Seagate_Expansion_Drive1/data/simulation/'
filename_Na22_total_eDep      = 'Na22/analysis_ej321pchar_h_eDep_total_scintillator.npz'
filename_Cs137_total_eDep     = 'Cs137/analysis_ej321pchar_h_eDep_total_scintillator.npz'
filename_4_44_total_eDep      = '4_44MeV/analysis_ej321pchar_h_eDep_total_scintillator.npz'
filename_Co60_total_eDep      = 'Co60/analysis_ej321pchar_h_eDep_total_scintillator.npz'

filename_Na22_neutron_ly    = 'Na22/analysis_ej321pchar_h_sum_optphoton_photocathode_proton.npz'
filename_Cs137_neutron_ly   = 'Cs137/analysis_ej321pchar_h_sum_optphoton_photocathode_proton.npz'
filename_4_44_neutron_ly    = '4_44MeV/analysis_ej321pchar_h_sum_optphoton_photocathode_proton.npz'
filename_Co60_neutron_ly    = 'Co60/analysis_ej321pchar_h_sum_optphoton_photocathode_proton.npz'

filename_Na22_total_pc_ly    = 'Na22/analysis_ej321pchar_h_sum_optphoton_photocathode_total.npz'
filename_Cs137_total_pc_ly   = 'Cs137/analysis_ej321pchar_h_sum_optphoton_photocathode_total.npz'
filename_4_44_total_pc_ly    = '4_44MeV/analysis_ej321pchar_h_sum_optphoton_photocathode_total.npz'
filename_Co60_total_pc_ly    = 'Co60/analysis_ej321pchar_h_sum_optphoton_photocathode_total.npz'

filename_Na22_total_scint_ly    = 'Na22/analysis_ej321pchar_h_sum_optphoton_scintillator_total.npz'
filename_Cs137_total_scint_ly   = 'Cs137/analysis_ej321pchar_h_sum_optphoton_scintillator_total.npz'
filename_4_44_total_scint_ly    = '4_44MeV/analysis_ej321pchar_h_sum_optphoton_scintillator_total.npz'
filename_Co60_total_scint_ly    = 'Co60/analysis_ej321pchar_h_sum_optphoton_scintillator_total.npz'


#Importing eDep values
# data_Na22_total_eDep    = prosim.unbinning_npz(path+filename_Na22_total_eDep, binName = 'bin_edges', countsName = 'counts')
# data_Cs137_total_eDep   = prosim.unbinning_npz(path+filename_Cs137_total_eDep, binName = 'bin_edges', countsName = 'counts')
# data_4_44_total_eDep    = prosim.unbinning_npz(path+filename_4_44_total_eDep, binName = 'bin_edges', countsName = 'counts')
# data_Co60_total_eDep    = prosim.unbinning_npz(path+filename_Co60_total_eDep, binName = 'bin_edges', countsName = 'counts')

#Importing neutron light yeild values (in photocathode)
# data_Na22_neutron_ly  = prosim.unbinning_npz(path+filename_Na22_neutron_ly, binName = 'bin_edges', countsName = 'counts')
# data_Cs137_neutron_ly = prosim.unbinning_npz(path+filename_Cs137_neutron_ly, binName = 'bin_edges', countsName = 'counts')
# data_4_44_neutron_ly  = prosim.unbinning_npz(path+filename_4_44_neutron_ly, binName = 'bin_edges', countsName = 'counts')
# data_Co60_neutron_ly  = prosim.unbinning_npz(path+filename_Co60_neutron_ly, binName = 'bin_edges', countsName = 'counts')

#Importing total light yeild values (in photocathode, all particles)
# data_Na22_total_pc_ly    = prosim.unbinning_npz(path+filename_Na22_total_pc_ly, binName = 'bin_edges', countsName = 'counts')
# data_Cs137_total_pc_ly   = prosim.unbinning_npz(path+filename_Cs137_total_pc_ly, binName = 'bin_edges', countsName = 'counts')
# data_4_44_total_pc_ly  = prosim.unbinning_npz(path+filename_4_44_total_pc_ly, binName = 'bin_edges', countsName = 'counts')
# data_Co60_total_pc_ly  = prosim.unbinning_npz(path+filename_Co60_total_pc_ly, binName = 'bin_edges', countsName = 'counts')
# same as above bu wuthout unbinning script
data_Na22_total_pc_ly    = np.load(path+filename_Na22_total_pc_ly)
data_Cs137_total_pc_ly    = np.load(path+filename_Cs137_total_pc_ly)
data_4_44_total_pc_ly    = np.load(path+filename_4_44_total_pc_ly)
data_Co60_total_pc_ly    = np.load(path+filename_Co60_total_pc_ly)



#Importing total light yeild values (in scintillator, all particles)
# data_Na22_total_scint_ly = prosim.unbinning_npz(path+filename_Na22_total_scint_ly, binName = 'bin_edges', countsName = 'counts')
# data_Cs137_total_scint_ly = prosim.unbinning_npz(path+filename_Cs137_total_scint_ly, binName = 'bin_edges', countsName = 'counts')
# data_4_44_total_scint_ly = prosim.unbinning_npz(path+filename_4_44_total_scint_ly, binName = 'bin_edges', countsName = 'counts')
# data_Co60_total_scint_ly = prosim.unbinning_npz(path+filename_Co60_total_scint_ly, binName = 'bin_edges', countsName = 'counts')

# plt.figure(0)
# numBins = 2048
# binRange = [0, 10]
# plt.hist(data_Na22_total_eDep, bins = numBins, range = binRange, histtype = 'step', lw = 3, label = 'Na22')
# plt.hist(data_Cs137_total_eDep, bins = numBins, range = binRange, histtype = 'step', lw = 3, label = 'Cs137')
# plt.hist(data_4_44_total_eDep, bins = numBins, range = binRange, histtype = 'step', lw = 3, label = '4.44MeV')
# plt.hist(data_Co60_total_eDep, bins = numBins, range = binRange, histtype = 'step', lw = 3, label = 'Co60')
# plt.yscale('log')
# plt.legend()
# plt.title('Total energy deposition (gamma-sources)')
# plt.ylabel('counts')
# plt.xlabel('Energy [MeV]')

# plt.figure(1)
# numBins = 500
# binRange = [0, 2000]
# plt.hist(data_Na22_neutron_ly, bins = numBins, range = binRange, histtype = 'step', lw = 3, label = 'Na22')
# plt.hist(data_Cs137_neutron_ly, bins = numBins, range = binRange, histtype = 'step', lw = 3, label = 'Cs137')
# plt.hist(data_4_44_neutron_ly, bins = numBins, range = binRange, histtype = 'step', lw = 3, label = '4.44MeV')
# plt.hist(data_Co60_neutron_ly, bins = numBins, range = binRange, histtype = 'step', lw = 3, label = 'Co60')
# plt.yscale('log')
# plt.legend()
# plt.title('Sum scint. photons in photocathode (per neutron)')
# plt.ylabel('counts')
# plt.xlabel('Number of scint. photons')

# plt.figure(2)
# numBins = 500
# binRange = [0, 2000]
# plt.plot(prodata.getBinCenters(data_Na22_total_pc_ly['bin_edges']), data_Na22_total_pc_ly['counts'], lw = 3, label = 'Na22')
# plt.plot(prodata.getBinCenters(data_Cs137_total_pc_ly['bin_edges']), data_Cs137_total_pc_ly['counts'], lw = 3, label = 'Cs137')
# plt.plot(prodata.getBinCenters(data_4_44_total_pc_ly['bin_edges']), data_4_44_total_pc_ly['counts'], lw = 3, label = '4.44MeV')
# plt.plot(prodata.getBinCenters(data_Co60_total_pc_ly['bin_edges']), data_Co60_total_pc_ly['counts'], lw = 3, label = 'Co60')
# plt.yscale('log')
# plt.legend()
# plt.title('Sum scint. photons in photocathode (per gamma)')
# plt.ylabel('counts')
# plt.xlabel('Number of scint. photons')


# plt.figure(3)
# numBins = 5000
# binRange = [0, 10000]
# plt.hist(data_Na22_total_scint_ly, bins = numBins, range = binRange, histtype = 'step', lw = 3, label = 'Na22')
# plt.hist(data_Cs137_total_scint_ly, bins = numBins, range = binRange, histtype = 'step', lw = 3, label = 'Cs137')
# plt.hist(data_4_44_total_scint_ly, bins = numBins, range = binRange, histtype = 'step', lw = 3, label = '4.44MeV')
# plt.hist(data_Co60_total_scint_ly, bins = numBins, range = binRange, histtype = 'step', lw = 3, label = 'Co60')
# plt.yscale('log')
# plt.legend()
# plt.title('Sum scint. photons in scintillator (per gamma)')
# plt.ylabel('counts')
# plt.xlabel('Number of scint. photons')
# plt.show()



# :::::::::::::::::::::::::::::::::::::::::::::::::::::
#     LOAD AND FIT COMPTON EDGES for Simulation data
# :::::::::::::::::::::::::::::::::::::::::::::::::::::
def sim_cal():
    Na22_fit_511 = gefSim.Na22_511_sim(prodata.getBinCenters(data_Na22_total_pc_ly['bin_edges']), data_Na22_total_pc_ly['counts'], binned = True, plot = False) 
    Cs137_fit_662 = gefSim.Cs137_662_sim(prodata.getBinCenters(data_Cs137_total_pc_ly['bin_edges']), data_Cs137_total_pc_ly['counts'], binned = True, plot = False) 
    Na22_fit_1275 = gefSim.Na22_1275_sim(prodata.getBinCenters(data_Na22_total_pc_ly['bin_edges']), data_Na22_total_pc_ly['counts'], binned = True, plot = False) 


    
    
    # ::::::::::::::::::::::::::::::::::::::::
    #     SAVE CALIBRATION DATA TO DISK: EJ321P
    # ::::::::::::::::::::::::::::::::::::::::
    #save calibration data
    E_data = np.array([0.3407, 0.4772, 1.675])
    Sim_data_89 = np.array([Na22_fit_511['CE_89_x'], Cs137_fit_662['CE_89_x'], Na22_fit_1275['CE_89_x']])
    Sim_err_89 = np.array([Na22_fit_511['CE_89_x_error'], Cs137_fit_662['CE_89_x_error'], Na22_fit_1275['CE_89_x_error']])
    
    Sim_data_50 = np.array([Na22_fit_511['CE_50_x'], Cs137_fit_662['CE_50_x'], Na22_fit_1275['CE_50_x']])
    Sim_err_50 = np.array([Na22_fit_511['CE_50_x_error'], Cs137_fit_662['CE_50_x_error'], Na22_fit_1275['CE_50_x_error']])

    #save to disk
    np.save('calibration_sim_89.npy', (E_data, Sim_data_89, Sim_err_89))
    np.save('calibration_sim_50.npy', (E_data, Sim_data_50, Sim_err_50))

    res_89 = prodata.linearEnergyFit(E_data, Sim_data_89, Sim_err_89, plot=True)
    res_50 = prodata.linearEnergyFit(E_data, Sim_data_50, Sim_err_50, plot=True)

    return res_89, res_50
