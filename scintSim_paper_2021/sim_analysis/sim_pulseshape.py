import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import sys
import os

sys.path.insert(0, "../../library/")
# sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library/")
# sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/nicholai_plotting_scripts/")

import processing_simulation as prosim
import nicholai_plot_helper as nplt
import processing_data as prodata
import processing_pd as propd


"""
This script is used to compare simulated pulses with real once those from the detector (EJ321P).
Source: Cs-137
Detector: EJ321P
"""

#PMT Paramters:
PMT_QE = .3 #Quantum efficiency of photocathode, 30% at ~350 nm peak (model: 9821B p1)
PMT_gain = 7e6 #Nominal gain is 7'000'000 at nominal A/lm (model: 9821B p1)
att_factor = 6.31 #used when attenuating the signal before the digitizer
q = 1.60217662E-19 #charge of a single electron


#Importing GEANT4 data
path_sim = '/media/gheed/Seagate_Expansion_Drive1/data/simulation/'
sim_pen_pulse_avg = prosim.loadData(path_sim + 'Cs137/pulses/pencilbeam/analysis_ej321pchar_pulse_shape.npz') #Data with no cuts
sim_iso_pulse_avg = prosim.loadData(path_sim + 'Cs137/pulses/isotropic/analysis_ej321pchar_pulse_shape.npz') #Data with no cuts

#Importing real data
path_data = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop
df_data = propd.load_parquet(path_data+'run01146/', num_events = 20) #load and merge all separete data files
df_data = df_data.query("amplitude_ch1>10")
df_data = df_data.reset_index()
df_data.samples_ch1 = df_data.samples_ch1 - df_data.baseline_ch1

# for i in range(100):
#     plt.plot(df_data.samples_ch1[i], label=f"sample nr: {i}")
#     plt.legend()
#     plt.show()

#Plotting the results 
plt.figure(0)
plt.suptitle('Average pulses')
plt.subplot(2,1,1)
plt.title('Pencilbeam simulation')
plt.step(sim_pen_pulse_avg.bin_centers, sim_pen_pulse_avg.counts, lw = 2, label = 'Simulated Pulse, Cs-137')
plt.step(np.arange(0,1001,1)-205.5, df_data.samples_ch1[4]*-39, lw = 2, label = 'Data Pulse, Cs-137')
# plt.yscale('log')
plt.legend()
plt.ylabel('Counts')
plt.xlim([-20, 200])
plt.subplot(2,1,2)
plt.title('Isotropic simulation')
plt.step(sim_iso_pulse_avg.bin_centers, sim_iso_pulse_avg.counts, lw = 2, label = 'Simulated Pulse, Cs-137')
plt.step(np.arange(0,1001,1)-201, df_data.samples_ch1[4]*-1.85, lw=2, label = 'Data Pulse, Cs-137')
# plt.yscale('log')
plt.legend()
plt.ylabel('Counts')
plt.xlabel('Time [ns]')
plt.xlim([-20, 200])
plt.show()
