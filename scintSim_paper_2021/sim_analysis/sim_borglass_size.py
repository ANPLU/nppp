import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import sys
import os

sys.path.insert(0, "../../library/")
# sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library/")
# sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/nicholai_plotting_scripts/")

import processing_simulation as prosim
import nicholai_plot_helper as nplt
import processing_data as prodata
import processing_pd as propd

"""
This script is used to compare simulations where the size of the boroated glass is nominal and 
the same size as the light guide.

The idea is to check if scintillation light gets caught in the borated glass due to the size miss match between 
glass and light guide.

Source: Cs-137
Beam: pencil/isotropic
"""

#PMT Paramters:
PMT_QE = .3 #Quantum efficiency of photocathode, 30% at ~350 nm peak (model: 9821B p1)
PMT_gain = 7e6 #Nominal gain is 7'000'000 at nominal A/lm (model: 9821B p1)
att_factor = 6.31 #used when attenuating the signal before the digitizer
q = 1.60217662E-19 #charge of a single electron


################################
# DETECTOR = EJ321P
################################

#Importing GEANT4 data
path_sim = '/media/gheed/Seagate_Expansion_Drive1/data/simulation/'
df_small_iso_pc_CS_QE = prosim.loadData(path_sim + 'Cs137/borglass_small/isotropic/analysis_ej321pchar_h_sum_optphoton_photocathode_total_CS_QE.npz') #Data with Compton scattering and Quantum efficiency cut
df_small_pen_pc_CS_QE = prosim.loadData(path_sim + 'Cs137/borglass_small/pencilbeam/analysis_ej321pchar_h_sum_optphoton_photocathode_total_CS_QE.npz') #Data with Compton scattering and Quantum efficiency cut

df_nominal_iso_pc_CS_QE = prosim.loadData(path_sim + 'Cs137/isotropic/analysis_ej321pchar_h_sum_optphoton_photocathode_total_CS_QE.npz') #Data with Compton scattering and Quantum efficiency cut
df_nominal_pen_pc_CS_QE = prosim.loadData(path_sim + 'Cs137/pencilbeam/analysis_ej321pchar_h_sum_optphoton_photocathode_total_CS_QE.npz') #Data with Compton scattering and Quantum efficiency cut

#convering simulation charge to QDC values and correcting for PMT gain
df_small_iso_pc_CS_QE.bin_centers    = prosim.chargeCalibration(df_small_iso_pc_CS_QE.bin_centers * q * PMT_gain)
df_small_pen_pc_CS_QE.bin_centers   = prosim.chargeCalibration(df_small_pen_pc_CS_QE.bin_centers * q * PMT_gain)
df_nominal_iso_pc_CS_QE.bin_centers   = prosim.chargeCalibration(df_nominal_iso_pc_CS_QE.bin_centers * q * PMT_gain)
df_nominal_pen_pc_CS_QE.bin_centers   = prosim.chargeCalibration(df_nominal_pen_pc_CS_QE.bin_centers * q * PMT_gain)

#Importing real data (NE213)
path_data = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop
df_Cs137_data = propd.load_parquet_merge(path_data, [1098, 1099, 1100]) #load and merge all separete data files

#Plotting the results
plt.figure(0)
plt.suptitle('EJ321P: charge @ photocathode')

plt.subplot(2,1,1)
plt.title('pencil beam')
plt.step(df_small_pen_pc_CS_QE.bin_centers,  df_small_pen_pc_CS_QE.counts,  lw = 2, label = 'Sim: Cs-137, smaller, QDC calibrated')
plt.step(df_nominal_pen_pc_CS_QE.bin_centers, df_nominal_pen_pc_CS_QE.counts, lw = 2, label = 'Sim: Cs-137, nominal, QDC calibrated')

# plt.hist(df_Cs137_data.qdc_lg_ch1, bins=400, lw=2, histtype='step', label = 'Data: Cs-137')
plt.yscale('log')
plt.legend()
plt.ylabel('counts')
plt.xlabel('QDC [arb. units]')
plt.xlim([-10e3, 40e3])

plt.subplot(2,1,2)
plt.title('isotropic beam')
plt.step(df_small_iso_pc_CS_QE.bin_centers,  df_small_iso_pc_CS_QE.counts,  lw = 2, label = 'Sim: Cs-137, smaller, QDC calibrated')
plt.step(df_nominal_iso_pc_CS_QE.bin_centers, df_nominal_iso_pc_CS_QE.counts, lw = 2, label = 'Sim: Cs-137, nominal, QDC calibrated')
# plt.hist(df_Cs137_data.qdc_lg_ch1, bins=400, lw=2, histtype='step', label = 'Data: Cs-137')
plt.yscale('log')
plt.legend()
plt.ylabel('counts')
plt.xlabel('QDC [arb. units]')
plt.xlim([-10e3, 40e3])

plt.show()