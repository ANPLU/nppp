#!/usr/bin/env python3
import sys
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit

# IMPORT FROM LIBRARY DIRECTORY
sys.path.insert(0, "../library/")
sys.path.insert(0, "../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "sim_analysis/")
sys.path.insert(0, "data_analysis/")

import digidata
import nicholai_plot_helper as nplt
import processing_utility as prout
import processing_math as promath
import processing_data as prodata
import processing_pd as propd
import processing_simulation as prosim
import gaussEdgeFit_parametersEJ321P as geEJ321P
import gaussEdgeFit_parametersNE213A as geNE213A
import simFit_parametersNE213A as simNE213A

# from sim_energyCal import sim_cal
from data_energyCal import NE213A_lg_cal

"""
Script for running both data and simulation and calibrating data.
"""

PMT_gain = 7e6 #Nominal gain is 7'000'000 at nominal A/lm (model: 9821B p1)
att_factor_A = 6.14102564 #used when attenuating the signal before the digitizer.
att_factor_cosmic_A = 13.09339408 # #attenuation factor used when taking cosmic muon data.
att_factor_B = 6.15343348 #used when attenuating the signal before the digitizer.
att_factor_cosmic_B = 13.09360731# #attenuation factor used when taking cosmic muon data.
q = 1.60217662E-19 #charge of a single electron
plot = False


##############################
# LOADING DATA
##############################
path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop
Cs137_runs =    [1549]
Co60_runs =     [1605]
Na22_runs =     [1551]
Th238_runs =    [1600]
AmBe_runs =     [1601,1602,1603]
PuBe_runs =     [1606,1607,1608]
cosmic_runs =   list(range(1266,1296))
bg_runs =       list(range(1552,1600))

df_bg =         propd.load_parquet_merge(path, bg_runs,     keep_col=['qdc_lg_ch1'], full=False)
df_Na22 =       propd.load_parquet_merge(path, Na22_runs,   keep_col=['qdc_lg_ch1'], full=False)
df_Cs137 =      propd.load_parquet_merge(path, Cs137_runs,  keep_col=['qdc_lg_ch1'], full=False)
df_Th238 =      propd.load_parquet_merge(path, Th238_runs,  keep_col=['qdc_lg_ch1'], full=False)
df_Co60 =       propd.load_parquet_merge(path, Co60_runs,   keep_col=['qdc_lg_ch1'], full=False)
df_PuBe =       propd.load_parquet_merge(path, PuBe_runs,   keep_col=['qdc_lg_ch1'], full=False)
df_AmBe =       propd.load_parquet_merge(path, AmBe_runs,   keep_col=['qdc_lg_ch1'], full=False)
df_cosmic =     propd.load_parquet_merge(path, cosmic_runs, keep_col=['qdc_lg_ch1'], full=False)

#########################################
# CALIBRATING QDC FOR NE213A
#########################################
res_gauss, data_res_89, data_res_50 = NE213A_lg_cal(df_bg, df_Na22, df_Cs137, df_Co60, df_Th238, df_PuBe, df_AmBe, df_cosmic, plot=plot) #fit all compton edges for NE213A detector

###############################
# LOADING SIMULATION DATA
###############################
#Pencilbeam
path_sim = '/media/gheed/Seagate_Expansion_Drive1/data/simulation/'
df_Cs137_662_sim_pen =      prosim.loadData(path_sim + 'NE213A/Cs137/pencilbeam/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE.npz') #
df_Cs137_662_sim_pen_small =      prosim.loadData(path_sim + 'NE213A/Cs137/pencilbeam/smaller_cut/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE.npz') #
df_Na22_sim_pen =           prosim.loadData(path_sim + 'NE213A/Na22/pencilbeam/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE.npz') #
df_Na22_511_sim_pen =       prosim.loadData(path_sim + 'NE213A/Na22_511/pencilbeam/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE.npz') #
df_Na22_1275_sim_pen =      prosim.loadData(path_sim + 'NE213A/Na22_1275/pencilbeam/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE.npz') #
df_Co60_sim_pen =           prosim.loadData(path_sim + 'NE213A/Co60/pencilbeam/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE.npz') #
df_Co60_1173_sim_pen =      prosim.loadData(path_sim + 'NE213A/Co60_1173/pencilbeam/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE.npz') #
df_Co60_1332_sim_pen =      prosim.loadData(path_sim + 'NE213A/Co60_1332/pencilbeam/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE.npz') #
df_2615_sim_pen =           prosim.loadData(path_sim + 'NE213A/2_62MeV/pencilbeam/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE.npz') #
df_2615_sim_pen_small =     prosim.loadData(path_sim + 'NE213A/2_62MeV/pencilbeam/smaller_cut/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE.npz') #
df_4439_sim_pen =           prosim.loadData(path_sim + 'NE213A/4_44MeV/pencilbeam/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE.npz') #
# df_6130_sim_pen =           prosim.loadData(path_sim + 'NE213A/6_13MeV/pencilbeam/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE.npz') #
df_cosmic_sim =             prosim.loadData(path_sim + 'NE213A/cosmic/3GeV/analysis_ej321pchar_optphoton_sum_photocathode_no_cut_QE.npz') #
#add another data set for 4.44 MeV gammas to increase statistics
df_4439_sim_pen.counts +=    prosim.loadData(path_sim + 'NE213A/4_44MeV/pencilbeam/part2/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE.npz').counts
df_4439_sim_pen.counts +=    prosim.loadData(path_sim + 'NE213A/4_44MeV/pencilbeam/part3/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE.npz').counts
df_4439_sim_pen.counts +=    prosim.loadData(path_sim + 'NE213A/4_44MeV/pencilbeam/part4/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE.npz').counts
df_4439_sim_pen.counts +=    prosim.loadData(path_sim + 'NE213A/4_44MeV/pencilbeam/part5/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE.npz').counts
df_4439_sim_pen.counts +=    prosim.loadData(path_sim + 'NE213A/4_44MeV/pencilbeam/part6/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE.npz').counts
df_4439_sim_pen.counts +=    prosim.loadData(path_sim + 'NE213A/4_44MeV/pencilbeam/part7/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE.npz').counts
df_4439_sim_pen.counts +=    prosim.loadData(path_sim + 'NE213A/4_44MeV/pencilbeam/part8/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE.npz').counts
df_4439_sim_pen.counts +=    prosim.loadData(path_sim + 'NE213A/4_44MeV/pencilbeam/part9/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE.npz').counts
#add another data set for 6.13 MeV gammas to increase statistics
# df_6130_sim_pen.counts =    prosim.loadData(path_sim + 'NE213A/6_13MeV/pencilbeam/part2/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE.npz').counts
# df_6130_sim_pen.counts =    prosim.loadData(path_sim + 'NE213A/6_13MeV/pencilbeam/part3/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE.npz').counts
#add another data set for 2.615 MeV gammas to increase statistics
df_2615_sim_pen.counts +=    prosim.loadData(path_sim + 'NE213A/2_62MeV/pencilbeam/part2/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE.npz').counts #
df_2615_sim_pen.counts +=    prosim.loadData(path_sim + 'NE213A/2_62MeV/pencilbeam/part3/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE.npz').counts #
df_2615_sim_pen.counts +=    prosim.loadData(path_sim + 'NE213A/2_62MeV/pencilbeam/part4/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE.npz').counts #
df_2615_sim_pen.counts +=    prosim.loadData(path_sim + 'NE213A/2_62MeV/pencilbeam/part5/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE.npz').counts #
df_2615_sim_pen.counts +=    prosim.loadData(path_sim + 'NE213A/2_62MeV/pencilbeam/part6/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE.npz').counts #
df_2615_sim_pen.counts +=    prosim.loadData(path_sim + 'NE213A/2_62MeV/pencilbeam/part7/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE.npz').counts #
df_2615_sim_pen.counts +=    prosim.loadData(path_sim + 'NE213A/2_62MeV/pencilbeam/part8/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE.npz').counts #
df_2615_sim_pen_small.counts +=    prosim.loadData(path_sim + 'NE213A/2_62MeV/pencilbeam/smaller_cut/part2/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE.npz').counts #
df_2615_sim_pen_small.counts +=    prosim.loadData(path_sim + 'NE213A/2_62MeV/pencilbeam/smaller_cut/part3/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE.npz').counts #
df_2615_sim_pen_small.counts +=    prosim.loadData(path_sim + 'NE213A/2_62MeV/pencilbeam/smaller_cut/part4/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE.npz').counts #
df_2615_sim_pen_small.counts +=    prosim.loadData(path_sim + 'NE213A/2_62MeV/pencilbeam/smaller_cut/part5/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE.npz').counts #
df_2615_sim_pen_small.counts +=    prosim.loadData(path_sim + 'NE213A/2_62MeV/pencilbeam/smaller_cut/part6/analysis_ej321pchar_optphoton_sum_photocathode_compton_QE.npz').counts #

#Isotropic
df_Cs137_662_sim_iso =      prosim.loadData(path_sim + 'NE213A/Cs137/isotropic/analysis_ej321pchar_optphoton_sum_photocathode_no_cut_QE_smear.npz') #
df_Na22_sim_iso =           prosim.loadData(path_sim + 'NE213A/Na22/isotropic/analysis_ej321pchar_optphoton_sum_photocathode_no_cut_QE_smear.npz') #
df_Na22_511_sim_iso =       prosim.loadData(path_sim + 'NE213A/Na22_511/isotropic/analysis_ej321pchar_optphoton_sum_photocathode_no_cut_QE_smear.npz') #
df_Na22_1275_sim_iso =      prosim.loadData(path_sim + 'NE213A/Na22_1275/isotropic/analysis_ej321pchar_optphoton_sum_photocathode_no_cut_QE_smear.npz') #
df_Co60_sim_iso =           prosim.loadData(path_sim + 'NE213A/Co60/isotropic/analysis_ej321pchar_optphoton_sum_photocathode_no_cut_QE_smear.npz') #
df_Co60_1173_sim_iso =      prosim.loadData(path_sim + 'NE213A/Co60_1173/isotropic/analysis_ej321pchar_optphoton_sum_photocathode_no_cut_QE_smear.npz') #
df_Co60_1332_sim_iso =      prosim.loadData(path_sim + 'NE213A/Co60_1332/isotropic/analysis_ej321pchar_optphoton_sum_photocathode_no_cut_QE_smear.npz') #
df_2615_sim_iso =           prosim.loadData(path_sim + 'NE213A/2_62MeV/isotropic/analysis_ej321pchar_optphoton_sum_photocathode_no_cut_QE_smear.npz') #
df_4439_sim_iso =           prosim.loadData(path_sim + 'NE213A/4_44MeV/isotropic/analysis_ej321pchar_optphoton_sum_photocathode_no_cut_QE_smear.npz') #
# df_6130_sim_iso =           prosim.loadData(path_sim + 'NE213A/6_13MeV/isotropic/analysis_ej321pchar_optphoton_sum_photocathode_no_cut_QE_smear.npz')

#####################################
# CALIBRATING SIMULATION DATA TO QDC
#####################################

# # # # # # # # # # # # # # # # # # # # #
# OFFSET PARAMETER FOR SIMULATED SPECTRA 
simScaleQDC_NE213A = 1
# # # # # # # # # # # # # # # # # # # # #
#calibrate simulated spectra to QDC channels
#pencilbeam
df_Cs137_662_sim_pen.bin_centers =      prosim.chargeCalibration(df_Cs137_662_sim_pen.bin_centers   * q * PMT_gain * simScaleQDC_NE213A)
df_Cs137_662_sim_pen_small.bin_centers =      prosim.chargeCalibration(df_Cs137_662_sim_pen_small.bin_centers   * q * PMT_gain * simScaleQDC_NE213A)
df_Na22_sim_pen.bin_centers =           prosim.chargeCalibration(df_Na22_sim_pen.bin_centers        * q * PMT_gain * simScaleQDC_NE213A)
df_Na22_511_sim_pen.bin_centers =       prosim.chargeCalibration(df_Na22_511_sim_pen.bin_centers    * q * PMT_gain * simScaleQDC_NE213A)
df_Na22_1275_sim_pen.bin_centers =      prosim.chargeCalibration(df_Na22_1275_sim_pen.bin_centers   * q * PMT_gain * simScaleQDC_NE213A)
df_Co60_sim_pen.bin_centers =           prosim.chargeCalibration(df_Co60_sim_pen.bin_centers        * q * PMT_gain * simScaleQDC_NE213A)
df_Co60_1173_sim_pen.bin_centers =      prosim.chargeCalibration(df_Co60_1173_sim_pen.bin_centers   * q * PMT_gain * simScaleQDC_NE213A)
df_Co60_1332_sim_pen.bin_centers =      prosim.chargeCalibration(df_Co60_1332_sim_pen.bin_centers   * q * PMT_gain * simScaleQDC_NE213A)
# df_2230_sim_pen.bin_centers =           prosim.chargeCalibration(df_2230_sim_pen.bin_centers        * q * PMT_gain * simScaleQDC_NE213A)
df_2615_sim_pen.bin_centers =           prosim.chargeCalibration(df_2615_sim_pen.bin_centers        * q * PMT_gain * simScaleQDC_NE213A)
df_2615_sim_pen_small.bin_centers =     prosim.chargeCalibration(df_2615_sim_pen_small.bin_centers  * q * PMT_gain * simScaleQDC_NE213A)
# df_PuBe_mixed_sim_pen.bin_centers =     prosim.chargeCalibration(df_PuBe_mixed_sim_pen.bin_centers  * q * PMT_gain * simScaleQDC_NE213A)
df_4439_sim_pen.bin_centers =           prosim.chargeCalibration(df_4439_sim_pen.bin_centers        * q * PMT_gain * simScaleQDC_NE213A)
# df_6130_sim_pen.bin_centers =           prosim.chargeCalibration(df_6130_sim_pen.bin_centers        * q * PMT_gain * simScaleQDC_NE213A)
df_cosmic_sim.bin_centers =             prosim.chargeCalibration(df_cosmic_sim.bin_centers          * q * PMT_gain * simScaleQDC_NE213A)

#isotropic
df_Cs137_662_sim_iso.bin_centers =      prosim.chargeCalibration(df_Cs137_662_sim_iso.bin_centers   * q * PMT_gain * simScaleQDC_NE213A)
df_Na22_sim_iso.bin_centers =           prosim.chargeCalibration(df_Na22_sim_iso.bin_centers        * q * PMT_gain * simScaleQDC_NE213A)
df_Na22_511_sim_iso.bin_centers =       prosim.chargeCalibration(df_Na22_511_sim_iso.bin_centers    * q * PMT_gain * simScaleQDC_NE213A)
df_Na22_1275_sim_iso.bin_centers =      prosim.chargeCalibration(df_Na22_1275_sim_iso.bin_centers   * q * PMT_gain * simScaleQDC_NE213A)
df_Co60_sim_iso.bin_centers =           prosim.chargeCalibration(df_Co60_sim_iso.bin_centers        * q * PMT_gain * simScaleQDC_NE213A)
df_Co60_1173_sim_iso.bin_centers =      prosim.chargeCalibration(df_Co60_1173_sim_iso.bin_centers   * q * PMT_gain * simScaleQDC_NE213A)
df_Co60_1332_sim_iso.bin_centers =      prosim.chargeCalibration(df_Co60_1332_sim_iso.bin_centers   * q * PMT_gain * simScaleQDC_NE213A)
# df_2230_sim_iso.bin_centers =           prosim.chargeCalibration(df_2230_sim_iso.bin_centers        * q * PMT_gain * simScaleQDC_NE213A)
df_2615_sim_iso.bin_centers =           prosim.chargeCalibration(df_2615_sim_iso.bin_centers        * q * PMT_gain * simScaleQDC_NE213A)
# df_PuBe_mixed_sim_iso.bin_centers =     prosim.chargeCalibration(df_PuBe_mixed_sim_iso.bin_centers  * q * PMT_gain * simScaleQDC_NE213A)
df_4439_sim_iso.bin_centers =           prosim.chargeCalibration(df_4439_sim_iso.bin_centers        * q * PMT_gain * simScaleQDC_NE213A)
# df_6130_sim_iso.bin_centers =           prosim.chargeCalibration(df_6130_sim_iso.bin_centers        * q * PMT_gain * simScaleQDC_NE213A)


#######################################################
# FITTING SIMULATED DATA TO DETERIVE POSITON OF COMPTON EDGE
#######################################################
sim_fit = {
    '662':0,
    '511':0,
    '1275':0,
    '1173':0,
    '1332':0,
    # '2230':0,
    '2615':0,
    '4439':0,
    # '6130':0,
    'cosmic':0
}

#Cs137
min = 12600
max = 30000
popt, pcov = promath.gaussFit(df_Cs137_662_sim_iso.bin_centers, df_Cs137_662_sim_iso.counts, min, max, error=True)
sim_fit['662'] = popt

#Na22 511
min = 7890
max = 20000
popt, pcov = promath.gaussFit(df_Na22_511_sim_iso.bin_centers, df_Na22_511_sim_iso.counts, min, max, error=True)
sim_fit['511'] = popt

#Na22 1275
min = 32000
max = 53000
popt, pcov = promath.gaussFit(df_Na22_1275_sim_iso.bin_centers, df_Na22_1275_sim_iso.counts, min, max, error=True)
sim_fit['1275'] = popt

#Co60 1173
min = 30700
max = 50000
popt, pcov = promath.gaussFit(df_Co60_1173_sim_iso.bin_centers, df_Co60_1173_sim_iso.counts, min, max, error=True)
sim_fit['1173'] = popt

#Co60 1332
min = 33800
max = 60000
popt, pcov = promath.gaussFit(df_Co60_1332_sim_iso.bin_centers, df_Co60_1332_sim_iso.counts, min, max, error=True)
sim_fit['1332'] = popt

# #2230 keV
# min = 68000
# max = 96000
# popt, pcov = promath.gaussFit(df_2230_sim_iso.bin_centers, df_2230_sim_iso.counts, min, max, error=True)
# sim_fit['2230'] = popt

#2615 keV
min = 82600
max = 115000
popt, pcov = promath.gaussFit(df_2615_sim_iso.bin_centers, df_2615_sim_iso.counts, min, max, error=True)
sim_fit['2615'] = popt

#4439 keV
min = 135000
max = 190000
x, y = prodata.reBinning(df_4439_sim_iso.bin_centers, df_4439_sim_iso.counts, 2)
popt, pcov = promath.gaussFit(x, y, min, max, error=True)
sim_fit['4439'] = popt

#6130 keV
# min = 202000
# max = 260000
# x, y = prodata.reBinning(df_6130_sim_iso.bin_centers, df_6130_sim_iso.counts, 2)
# popt, pcov = promath.gaussFit(x, y, min, max, error=True)
# sim_fit['6130'] = popt

#cosmic
min = 500000
max = 1000000
x, y = prodata.reBinning(df_cosmic_sim_iso.bin_centers, df_cosmic_sim_iso.counts, 2)
popt, pcov = promath.gaussFit(x, y, min, max, error=True)
sim_fit['cosmic'] = popt



PMTGainEnergy = np.array([662,511,1275,2615,4439,19000])
PMTGain = np.array([
    res_gauss['662_gauss_mean']/sim_fit['662'][1],
    res_gauss['511_gauss_mean']/sim_fit['511'][1],
    res_gauss['1275_gauss_mean']/sim_fit['1275'][1],
    # res_gauss['1332_gauss_mean']/sim_fit['1332'][1],
    res_gauss['2615_gauss_mean']/sim_fit['2615'][1],
    res_gauss['PuBe_4439_gauss_mean']/sim_fit['4439'][1],
    res_gauss['gauss_mean']/sim_fit['cosmic'][1]])

energy = np.array([
    res_gauss['511_recoil_max'],
    res_gauss['662_recoil_max'],
    res_gauss['1332_recoil_max'],
    res_gauss['2615_recoil_max'],
    res_gauss['PuBe_4439_recoil_max']
])
fit = np.array([
    sim_fit['511'][1],
    sim_fit['662'][1],
    sim_fit['1332'][1],
    sim_fit['2615'][1],
    sim_fit['4439'][1]
])

def simFit(QDC):
    return 2.808e-5*QDC+0.09203
############################################
# PLOT FITTED SIM DATA
############################################
numBins = 1024
binRange = [0,25000]
xNew = np.arange(0,3000000)
plt.step(df_Cs137_662_sim_iso.bin_centers, df_Cs137_662_sim_iso.counts, label='Cs137')
plt.plot(xNew, promath.gaussFunc(xNew, sim_fit['662'][0], sim_fit['662'][1], sim_fit['662'][2]))
plt.legend()
plt.show()

plt.step(df_Na22_511_sim_iso.bin_centers, df_Na22_511_sim_iso.counts, label='Na22 511')
plt.plot(xNew, promath.gaussFunc(xNew, sim_fit['511'][0], sim_fit['511'][1], sim_fit['511'][2]))
plt.legend()
plt.show()

plt.step(df_Na22_1275_sim_iso.bin_centers, df_Na22_1275_sim_iso.counts, label='Na22 1275')
plt.plot(xNew, promath.gaussFunc(xNew, sim_fit['1275'][0], sim_fit['1275'][1], sim_fit['1275'][2]))
plt.legend()
plt.show()

plt.step(df_Co60_1173_sim_iso.bin_centers, df_Co60_1173_sim_iso.counts, label='Co60 1173')
plt.plot(xNew, promath.gaussFunc(xNew, sim_fit['1173'][0], sim_fit['1173'][1], sim_fit['1173'][2]))
plt.legend()
plt.show()

plt.step(df_Co60_1332_sim_iso.bin_centers, df_Co60_1332_sim_iso.counts, label='Co60 1331')
plt.plot(xNew, promath.gaussFunc(xNew, sim_fit['1332'][0], sim_fit['1332'][1], sim_fit['1332'][2]))
plt.legend()
plt.show()

# plt.step(df_2230_sim_iso.bin_centers, df_2230_sim_iso.counts, label='2230')
# plt.plot(xNew, promath.gaussFunc(xNew, sim_fit['2230'][0], sim_fit['2230'][1], sim_fit['2230'][2]))
# plt.legend()
# plt.show()

plt.step(df_2615_sim_iso.bin_centers, df_2615_sim_iso.counts, label='2615')
plt.plot(xNew, promath.gaussFunc(xNew, sim_fit['2615'][0], sim_fit['2615'][1], sim_fit['2615'][2]))
plt.legend()
plt.show()

x, y = prodata.reBinning(df_4439_sim_iso.bin_centers, df_4439_sim_iso.counts, 2)
plt.step(x, y, label='4439')
plt.plot(xNew, promath.gaussFunc(xNew, sim_fit['4439'][0], sim_fit['4439'][1], sim_fit['4439'][2]))
plt.legend()
plt.show()

# x, y = prodata.reBinning(df_6130_sim_iso.bin_centers, df_6130_sim_iso.counts, 2)
# plt.step(x, y, label='6130')
# plt.plot(xNew, promath.gaussFunc(xNew, sim_fit['6130'][0], sim_fit['6130'][1], sim_fit['6130'][2]))
# plt.legend()
# plt.show()

x, y = prodata.reBinning(df_cosmic_sim_iso.bin_centers, df_cosmic_sim_iso.counts, 2)
plt.step(x, y, label='cosmic')
plt.plot(xNew, promath.gaussFunc(xNew, sim_fit['cosmic'][0], sim_fit['cosmic'][1], sim_fit['cosmic'][2]))
plt.legend()
plt.show()


########################################
# DETERMINE PMT GAIN FOR EACH ENERGY
########################################
numBins = 1300
binRange = ([0,250000])
counts, bins = np.histogram(df_Cs137.qdc_lg_ch1*att_factor_A, numBins, binRange)
bins = prodata.getBinCenters(bins)
bgcounts, bgbins = np.histogram(df_bg.qdc_lg_ch1*att_factor_A, numBins, binRange)
counts = counts-bgcounts/48
xCoeff = np.arange(0.265, 1.45, 0.01)
chi2_min = np.inf

for i in xCoeff:
    chi2 = promath.chi2red(counts[15:30], df_Cs137_662_sim_iso.counts[15:30]*i, 1)
    if chi2<chi2_min:
        chi2_min = chi2
    print(f'xCoeff = {i}, chi2 = {chi2}, chi2_min = {chi2_min}')    


plot = True
# Cs-137 alignment
numBins = 2048
binRange = [0,250000]
counts, bins = np.histogram(df_Cs137.qdc_lg_ch1*att_factor_A, numBins, binRange)
bins = prodata.getBinCenters(bins)
bgcounts, bgbins = np.histogram(df_bg.qdc_lg_ch1*att_factor_A, numBins, binRange)
counts = counts-bgcounts/48

# plt.step(bins, counts)
# plt.step(bins[27:42], counts[27:42], lw=2, color='teal')
# plt.step(df_Cs137_662_sim_iso.bin_centers*.344, df_Cs137_662_sim_iso.counts*1.80)
# plt.step(df_Cs137_662_sim_iso.bin_centers[30:45]*.344, df_Cs137_662_sim_iso.counts[30:45]*1.80, lw=2, color='black')
# plt.show()

PMTGain_Cs137_662 = prosim.PMTGainAlign(
                                    bins, #x1
                                    counts, #y1
                                    df_Cs137_662_sim_iso.bin_centers, #x2
                                    df_Cs137_662_sim_iso.counts, #y2
                                    .25, #X off min
                                    .45, #X off max
                                    1, #Y off min
                                    3, #Y off max 
                                    0.001, #dX
                                    .1, #dY
                                    [28,41], #range1
                                    [31,44], #range2
                                    plot) #plot


# Na-22 511 alignment
numBins = 2048
binRange = ([0,250000])
counts, bins = np.histogram(df_Na22.qdc_lg_ch1*att_factor_A, numBins, binRange)
bins = prodata.getBinCenters(bins)
bgcounts, bgbins = np.histogram(df_bg.qdc_lg_ch1*att_factor_A, numBins, binRange)
counts = counts-bgcounts/48

# plt.step(bins, counts)
# plt.step(df_Na22_511_sim_iso.bin_centers*.35, df_Na22_511_sim_iso.counts*6)
# plt.show()

PMTGain_Na22_511 = prosim.PMTGainAlign(
                                    bins, #x1
                                    counts, #y1
                                    df_Na22_511_sim_iso.bin_centers, #x2
                                    df_Na22_511_sim_iso.counts, #y2
                                    .30, #X off min
                                    .40, #X off max
                                    4, #Y off min
                                    7, #Y off max 
                                    0.001, #dX
                                    .1, #dY
                                    [23,31], #range1
                                    [24,32], #range2
                                    plot) #plot


# Na-22 1275 alignment
numBins = 2048
binRange = ([0,250000])
counts, bins = np.histogram(df_Na22.qdc_lg_ch1*att_factor_A, numBins, binRange)
bins = prodata.getBinCenters(bins)
bgcounts, bgbins = np.histogram(df_bg.qdc_lg_ch1*att_factor_A, numBins, binRange)
counts = counts-bgcounts/48

# plt.step(bins, counts)
# plt.step(df_Na22_1275_sim_iso.bin_centers*.345, df_Na22_1275_sim_iso.counts*3)
# plt.show()

PMTGain_Na22_1275 = prosim.PMTGainAlign(
                                    bins, #x1
                                    counts, #y1
                                    df_Na22_1275_sim_iso.bin_centers, #x2
                                    df_Na22_1275_sim_iso.counts, #y2
                                    .30, #X off min
                                    .35, #X off max
                                    2.7, #Y off min
                                    3.5, #Y off max 
                                    0.001, #dX
                                    .1, #dY
                                    [75,100], #range1
                                    [77,102], #range2
                                    plot) #plot

# Co-60 1332 alignment
numBins = int(2016/2)
binRange = ([0,250000])
counts, bins = np.histogram(df_Co60.qdc_lg_ch1*att_factor_A, numBins, binRange)
bins = prodata.getBinCenters(bins)
bgcounts, bgbins = np.histogram(df_bg.qdc_lg_ch1*att_factor_A, numBins, binRange)
counts = counts-bgcounts/48

x,y = prodata.reBinning(df_Co60_1332_sim_iso.bin_centers, df_Co60_1332_sim_iso.counts, 2)
# plt.step(bins, counts)
# plt.step(df_Co60_1332_sim_iso.bin_centers*.335, df_Co60_1332_sim_iso.counts*1.7)
# plt.show()

PMTGain_Co60_1332 = prosim.PMTGainAlign(
                                    bins, #x1
                                    counts, #y1
                                    x, #x2
                                    y, #y2
                                    .2, #X off min
                                    .335, #X off max
                                    .1, #Y off min
                                    2, #Y off max 
                                    0.001, #dX
                                    .001, #dY
                                    [35,47], #range1
                                    [39,51], #range2
                                    plot) #plot

# Th-238 2615 alignment
numBins = int(2016/2)
binRange = ([0,250000])
counts, bins = np.histogram(df_Th238.qdc_lg_ch1*att_factor_A, numBins, binRange)
bins = prodata.getBinCenters(bins)
bgcounts, bgbins = np.histogram(df_bg.qdc_lg_ch1*att_factor_A, numBins, binRange)
counts = counts-bgcounts/48

x,y = prodata.reBinning(df_2615_sim_iso.bin_centers, df_2615_sim_iso.counts, 2)

PMTGain_Th238_2613 = prosim.PMTGainAlign(
                                    bins, #x1
                                    counts, #y1
                                    x, #x2
                                    y, #y2
                                    .30, #X off min
                                    .38, #X off max
                                    .01, #Y off min
                                    .9, #Y off max 
                                    0.001, #dX
                                    .001, #dY
                                    [100,120], #range1
                                    [102,122], #range2
                                    plot) #plot

# PuBe 4439 alignment
numBins = int(2016/2)
binRange = ([0,250000])
counts, bins = np.histogram(df_AmBe.qdc_lg_ch1*att_factor_A, numBins, binRange)
bins = prodata.getBinCenters(bins)
bgcounts, bgbins = np.histogram(df_bg.qdc_lg_ch1*att_factor_A, numBins, binRange)
counts = counts/3-bgcounts/48

x, y = prodata.reBinning(df_4439_sim_iso.bin_centers, df_4439_sim_iso.counts, 2)

PMTGain_PuBe_4439 = prosim.PMTGainAlign(
                                    bins, #x1
                                    counts, #y1
                                    x, #x2
                                    y, #y2
                                    .32, #X off min
                                    .37, #X off max
                                    .1, #Y off min
                                    2, #Y off max 
                                    0.001, #dX
                                    .01, #dY
                                    [162,190], #range1
                                    [162,190], #range2
                                    plot) #plot

# PuBe 6130 alignment
# numBins = 4600
# binRange = ([0,250000])
# counts, bins = np.histogram(df_AmBe.qdc_lg_ch1*att_factor_A, numBins, binRange)
# bins = prodata.getBinCenters(bins)
# bgcounts, bgbins = np.histogram(df_bg.qdc_lg_ch1*att_factor_A, numBins, binRange)
# counts = counts/3-bgcounts/48

# x ,y = prodata.reBinning(df_6130_sim_iso.bin_centers, df_6130_sim_iso.counts, 2)
# PMTGain_PuBe_6130 = prosim.PMTGainAlign(
#                                     bins, #x1
#                                     counts, #y1
#                                     x, #x2
#                                     y, #y2
#                                     .09, #X off min
#                                     .17, #X off max
#                                     15, #Y off min
#                                     20, #Y off max 
#                                     0.001, #dX
#                                     .1, #dY
#                                     [135,165], #range
#                                     plot) #plot

# Cosmic alignment
numBins = 128
binRange = ([0,250000])
counts, bins = np.histogram(df_cosmic.qdc_lg_ch1*att_factor_cosmic_A, numBins, binRange)
bins = prodata.getBinCenters(bins)

x ,y = prodata.reBinning(df_cosmic_sim.bin_centers, df_cosmic_sim.counts, 1)

# plt.step(bins, counts, label='Data')
# plt.step(x, y, label='Simulation')
# plt.legend()
# plt.show()


PMTGain_cosmic = prosim.PMTGainAlign(
                                    bins, #x1
                                    counts, #y1
                                    x, #x2
                                    y, #y2
                                    .01, #X off min
                                    .3, #X off max
                                    .01, #Y off min
                                    1, #Y off max 
                                    0.001, #dX
                                    .01, #dY
                                    [55,85], #range1
                                    [55,85], #range2
                                    plot) #plot



PMTGain = np.array([PMTGain_Cs137_662[0], PMTGain_Na22_511[0], PMTGain_Na22_1275[0], PMTGain_Th238_2613[0], PMTGain_PuBe_4439[0], PMTGain_cosmic[0]])
energy = np.array([.479, .341, 1.062, 2.382, 4.197, 20.000])
# channel = np.array([3770, 2350, 10500, 41500, 57444, 125215])

plt.scatter(energy, PMTGain, s=100)
plt.grid(which='both')
plt.xlabel('Energy [MeV$_{ee}$]')
plt.ylabel('PMT gain x $10^{6}$')
plt.ylim([0,0.4])
plt.show()
