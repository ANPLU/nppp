#loading simulation data.
from re import I
import sys
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit

# IMPORT FROM LIBRARY DIRECTORY
sys.path.insert(0, "../library/")
sys.path.insert(0, "../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "../")
sys.path.insert(0, "data_analysis/")

import processing_math as promath
import processing_data as prodata
import processing_pd as propd
import processing_simulation as prosim
import gaussEdgeFit_parametersEJ321P as geEJ321P
import gaussEdgeFit_parametersNE213A as geNE213A
import simFit_parametersNE213A as simNE213A

# from sim_energyCal import sim_cal
from data_energyCal import NE213A_lg_cal

path_sim = '/media/gheed/Seagate_Expansion_Drive1/data/simulation/'
names = names = ['evtNum', 
                'optPhotonSum', 
                'optPhotonSumQE', 
                'optPhotonSumCompton', 
                'optPhotonSumComptonQE', 
                'xLoc', 
                'yLoc', 
                'zLoc', 
                'CsMin', 
                'optPhotonParentID', 
                'optPhotonParentCreatorProcess', 
                'optPhotonParentStartingEnergy',
                'edep']

# df_Cs137_662_sim_iso =      pd.read_csv(path_sim + "NE213A/TMP/reflectivity_test/lightYield_8_0/CSV_optphoton_data_sum.csv", names=names)
# # df_Cs137_662_sim_iso.optPhotonSumQE = prosim.chargeCalibration(df_Cs137_662_sim_pen.optPhotonSumQE * q * PMT_gain)

# df_Cs137_662_sim_iso_old = np.genfromtxt(path_sim + 'NE213A/TMP/reflectivity_test/lightYield_8_0/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1] 
# # df_Cs137_662_sim_iso = pd.to_numeric(df_Cs137_662_sim_iso, downcast='float')
# # df_Cs137_662_sim_iso = prosim.chargeCalibration(df_Cs137_662_sim_iso * q * PMT_gain)

upstream_662    = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/066/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
downstream_662  = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/072/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
both_662        = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/070/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
none_662        = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/069/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
upstream_4440   = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/074/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
downstream_4440 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/073/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
both_4440       = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/071/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
none_4440       = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/067/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
both_662_300    = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/075/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
both_4440_300   = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/076/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]


plt.figure()
numBins = 1200
binRange = [0, 2400]
plt.subplot(1,2,1)
plt.title('662 keV')
# ybg, xbg = np.histogram(df_bg.qdc_lg_ch1*att_factor_A, bins=numBins, range=binRange)
# y, x = np.histogram(df_Th232.qdc_lg_ch1*att_factor_A, bins=numBins, range=binRange)
# x = prodata.getBinCenters(x)
# plt.step(x, (y-ybg), lw=2, color='black', label='Th232 Data')

# ybg, xbg = np.histogram(df_bg.qdc_lg_ch1*att_factor_A, bins=numBins, range=binRange)
# y, x = np.histogram(df_AmBe.qdc_lg_ch1*att_factor_A, bins=numBins, range=binRange)
# x = prodata.getBinCenters(x)
# plt.step(x, (y/3)-(ybg/10), lw=2, color='black', label='AmBe Data')

# ybg, xbg = np.histogram(df_bg.qdc_lg_ch1*att_factor_A, bins=numBins, range=binRange)
# y, x = np.histogram(df_Cs137.qdc_lg_ch1*att_factor_A, bins=numBins, range=binRange)
# x = prodata.getBinCenters(x)
# plt.step(x, y-(ybg/10), lw=2, color='black', label='Cs137 Data')

# y, x = np.histogram(upstream_662, bins=1200, range=[0, 1200])
# newData = prodata.getRandDist(x, y)
# y, x = np.histogram(newData, bins=numBins, range=binRange)
# x = prodata.getBinCenters(x)
# plt.step(x, y,  lw=2, label='up stream')
# y, x = np.histogram(downstream_662, bins=1200, range=[0, 1200])
# newData = prodata.getRandDist(x, y)
# y, x = np.histogram(newData, bins=numBins, range=binRange)
# x = prodata.getBinCenters(x)
# plt.step(x, y,  lw=2, label='down stream')
# y, x = np.histogram(none_662, bins=1200, range=[0, 1200])
# newData = prodata.getRandDist(x, y)
# y, x = np.histogram(newData, bins=numBins, range=binRange)
# x = prodata.getBinCenters(x)
# plt.step(x, y,  lw=2, label='no airgap')
y, x = np.histogram(both_662, bins=1200, range=[0, 1200])
newData = prodata.getRandDist(x, y)
y, x = np.histogram(newData, bins=numBins, range=binRange)
x = prodata.getBinCenters(x)
plt.step(x, y,  lw=2, label='up+down stream')
y, x = np.histogram(both_662_300, bins=1200, range=[0, 1200])
newData = prodata.getRandDist(x, y)
y, x = np.histogram(newData, bins=numBins, range=binRange)
x = prodata.getBinCenters(x)
plt.step(x, y,  lw=2, label='both 300um 662')
plt.xlim([0, 170])
plt.ylim([0, 1600])
plt.legend()



plt.subplot(1,2,2)
numBins = 300
binRange = [0, 2400]
plt.title('4400 keV')
# y, x = np.histogram(upstream_4440, bins=1200, range=[0, 1200])
# newData = prodata.getRandDist(x, y)
# y, x = np.histogram(newData, bins=numBins, range=binRange)
# x = prodata.getBinCenters(x)
# plt.step(x, y,  lw=2, label='up stream')
# y, x = np.histogram(downstream_4440, bins=1200, range=[0, 1200])
# newData = prodata.getRandDist(x, y)
# y, x = np.histogram(newData, bins=numBins, range=binRange)
# x = prodata.getBinCenters(x)
# plt.step(x, y,  lw=2, label='down stream')
# y, x = np.histogram(none_4440, bins=1200, range=[0, 1200])
# newData = prodata.getRandDist(x, y)
# y, x = np.histogram(newData, bins=numBins, range=binRange)
# x = prodata.getBinCenters(x)
# plt.step(x, y,  lw=2, label='no airgap')
y, x = np.histogram(both_4440, bins=1200, range=[0, 1200])
newData = prodata.getRandDist(x, y)
y, x = np.histogram(newData, bins=numBins, range=binRange)
x = prodata.getBinCenters(x)
plt.step(x, y,  lw=2, label='up+down stream')
y, x = np.histogram(both_4440_300, bins=1200, range=[0, 1200])
newData = prodata.getRandDist(x, y)
y, x = np.histogram(newData, bins=numBins, range=binRange)
x = prodata.getBinCenters(x)
plt.step(x, y,  lw=2, label='both 300um 4440')

plt.xlim([0, 1000])
plt.ylim([-20, 600])
plt.legend()

plt.show()



print( (1-np.mean(both_4440_300)/np.mean(both_4440))*100)
print( (1-np.mean(both_662_300)/np.mean(both_662))*100)

print(np.std(both_4440_300)/np.std(both_4440))