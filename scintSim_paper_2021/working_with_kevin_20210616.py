#loading simulation data.


path_sim = '/media/gheed/Seagate_Expansion_Drive1/data/simulation/'
names = names = ['evtNum', 
                'optPhotonSum', 
                'optPhotonSumQE', 
                'optPhotonSumCompton', 
                'optPhotonSumComptonQE', 
                'xLoc', 
                'yLoc', 
                'zLoc', 
                'CsMin', 
                'optPhotonParentID', 
                'optPhotonParentCreatorProcess', 
                'optPhotonParentStartingEnergy',
                'edep']

df_Cs137_662_sim_iso =      pd.read_csv(path_sim + "NE213A/TMP/reflectivity_test/lightYield_8_0/CSV_optphoton_data_sum.csv", names=names)
# df_Cs137_662_sim_iso.optPhotonSumQE = prosim.chargeCalibration(df_Cs137_662_sim_pen.optPhotonSumQE * q * PMT_gain)

df_Cs137_662_sim_iso_old = np.genfromtxt(path_sim + 'NE213A/TMP/reflectivity_test/lightYield_8_0/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1] 
# df_Cs137_662_sim_iso = pd.to_numeric(df_Cs137_662_sim_iso, downcast='float')
# df_Cs137_662_sim_iso = prosim.chargeCalibration(df_Cs137_662_sim_iso * q * PMT_gain)

df_Cs137_662_sim_iso_001 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/001/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
df_Cs137_662_sim_iso_002 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/002/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
df_Cs137_662_sim_iso_003 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/003/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
df_Cs137_662_sim_iso_004 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/004/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
df_Cs137_662_sim_iso_005 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/005/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
df_Cs137_662_sim_iso_006 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/006/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
df_Cs137_662_sim_iso_007 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/007/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
df_Cs137_662_sim_iso_008 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/008/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
df_Cs137_662_sim_iso_009 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/009/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
df_Cs137_662_sim_iso_010 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/010/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
df_Cs137_662_sim_iso_011 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/011/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
df_Cs137_662_sim_iso_012 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/012/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
df_Cs137_662_sim_iso_013 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/013/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
df_Cs137_662_sim_iso_014 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/014/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
df_Cs137_662_sim_iso_015 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/015/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
df_Cs137_662_sim_iso_016 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/016/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
df_Cs137_662_sim_iso_017 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/017/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
df_Cs137_662_sim_iso_018 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/018/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
df_Cs137_662_sim_iso_019 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/019/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
df_Cs137_662_sim_iso_020 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/020/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
df_Cs137_662_sim_iso_021 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/021/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
df_Cs137_662_sim_iso_022 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/022/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
df_Cs137_662_sim_iso_023 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/023/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
df_Cs137_662_sim_iso_024 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/024/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
df_Cs137_662_sim_iso_025 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/025/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
df_Cs137_662_sim_iso_026 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/026/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
df_Cs137_662_sim_iso_027 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/027/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
df_Cs137_662_sim_iso_028 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/028/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
df_Cs137_662_sim_iso_029 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/029/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
df_Cs137_662_sim_iso_030 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/030/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
df_Cs137_662_sim_iso_031 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/031/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
df_Cs137_662_sim_iso_032 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/032/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
df_Cs137_662_sim_iso_033 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/033/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
df_Cs137_662_sim_iso_034 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/034/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
df_Cs137_662_sim_iso_035 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/035/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
df_Cs137_662_sim_iso_036 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/036/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
df_Cs137_662_sim_iso_037 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/037/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
df_Cs137_662_sim_iso_038 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/038/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
df_Cs137_662_sim_iso_039 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/039/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
df_Cs137_662_sim_iso_040 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/040/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
df_Cs137_662_sim_iso_041 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/041/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
df_Cs137_662_sim_iso_042 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/042/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
df_Cs137_662_sim_iso_043 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/043/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
df_Cs137_662_sim_iso_044 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/044/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
df_Cs137_662_sim_iso_045 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/045/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
df_Cs137_662_sim_iso_046 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/046/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
df_Cs137_662_sim_iso_047 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/047/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
df_Cs137_662_sim_iso_048 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/048/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
df_Cs137_662_sim_iso_049 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/049/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
df_Cs137_662_sim_iso_050 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/050/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
df_Cs137_662_sim_iso_051 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/051/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
df_Cs137_662_sim_iso_052 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/052/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
df_Cs137_662_sim_iso_053 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/053/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
df_Cs137_662_sim_iso_054 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/054/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
df_Cs137_662_sim_iso_055 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/055/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
df_Cs137_662_sim_iso_056 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/056/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
df_Cs137_662_sim_iso_057 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/057/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
df_Cs137_662_sim_iso_058 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/058/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
df_Cs137_662_sim_iso_059 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/059/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
df_Cs137_662_sim_iso_060 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/060/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
df_Cs137_662_sim_iso_061 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/061/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
df_Cs137_662_sim_iso_062 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/062/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
df_Cs137_662_sim_iso_066 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/066/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
# df_Cs137_662_sim_iso_067 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/067/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
# df_Cs137_662_sim_iso_068 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/068/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
# df_Cs137_662_sim_iso_063 = np.genfromtxt(path_sim + 'NE213A/TMP/runs/Cs137/063/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]


# df_Cs137_662_sim_iso_057 = pd.read_csv(path_sim + "NE213A/TMP/runs/Cs137/057/CSV_optphoton_data_sum.csv", names=names)
# df_Cs137_662_sim_iso_061 = pd.read_csv(path_sim + "NE213A/TMP/runs/Cs137/061/CSV_optphoton_data_sum.csv", names=names)
# df_Cs137_662_sim_iso_062 = pd.read_csv(path_sim + "NE213A/TMP/runs/Cs137/062/CSV_optphoton_data_sum.csv", names=names)
# df_Cs137_662_sim_iso_063 = pd.read_csv(path_sim + "NE213A/TMP/runs/Cs137/063/CSV_optphoton_data_sum.csv", names=names)
# df_Cs137_662_sim_iso_064 = pd.read_csv(path_sim + "NE213A/TMP/runs/Cs137/064/CSV_optphoton_data_sum.csv", names=names)
# df_Cs137_662_sim_iso_066 = pd.read_csv(path_sim + "NE213A/TMP/runs/Cs137/066/CSV_optphoton_data_sum.csv", names=names)

# df_Cs137_662_sim_iso_test = np.genfromtxt(path_sim + 'NE213A/Cs137/isotropic/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]

plt.figure()
plt.title('Testing 2021-07-16')
numBins = 1200
binRange = [0, 2400*100]

# ybg, xbg = np.histogram(df_bg.qdc_lg_ch1*att_factor_A, bins=numBins, range=binRange)
# y, x = np.histogram(df_Th232.qdc_lg_ch1*att_factor_A, bins=numBins, range=binRange)
# x = prodata.getBinCenters(x)
# plt.step(x, (y-ybg), lw=2, color='black', label='Th232 Data')

# ybg, xbg = np.histogram(df_bg.qdc_lg_ch1*att_factor_A, bins=numBins, range=binRange)
# y, x = np.histogram(df_AmBe.qdc_lg_ch1*att_factor_A, bins=numBins, range=binRange)
# x = prodata.getBinCenters(x)
# plt.step(x, (y/3)-(ybg/10), lw=2, color='black', label='AmBe Data')

ybg, xbg = np.histogram(df_bg.qdc_lg_ch1*att_factor_A, bins=numBins, range=binRange)
y, x = np.histogram(df_Cs137.qdc_lg_ch1*att_factor_A, bins=numBins, range=binRange)
x = prodata.getBinCenters(x)
plt.step(x, y-(ybg/10), lw=2, color='black', label='Cs137 Data')

# y, x = np.histogram(df_Cs137_662_sim_iso_041, bins=1200, range=[0,1200])
# newData = getRandDist(x, y)
# y, x = np.histogram(newData*13, bins=numBins, range=binRange)
# x = prodata.getBinCenters(x)
# plt.step(x, y*28.8, label='041 - BENCHMARK')

# y, x = np.histogram(df_Cs137_662_sim_iso_042, bins=1200, range=[0,1200])
# newData = getRandDist(x, y)
# y, x = np.histogram(newData*13, bins=numBins, range=binRange) #14
# x = prodata.getBinCenters(x)
# plt.step(x, y*28.8, label='042 - Glisure SetPolish(0.0)') #37

# y, x = np.histogram(df_Cs137_662_sim_iso_043, bins=1200, range=[0,1200])
# newData = getRandDist(x, y)
# y, x = np.histogram(newData*9, bins=numBins, range=binRange) #14
# x = prodata.getBinCenters(x)
# plt.step(x, y*14.5, label='043 - Glisure SetPolish(0.33)') #37

# y, x = np.histogram(df_Cs137_662_sim_iso_066, bins=1200, range=[0,1200])
# newData = getRandDist(x, y)
# y, x = np.histogram(newData*9*6.5, bins=numBins, range=binRange) 
# x = prodata.getBinCenters(x)
# plt.step(x, y*16, label='066 - Glisure SetPolish(0.10) + 0.1 mm airgap') 

df_Cs137_662_sim_iso_066_smear = df_Cs137_662_sim_iso_066

for i in range(len(df_Cs137_662_sim_iso_066)):
    df_Cs137_662_sim_iso_066_smear[i] = prosim.gaussSmear(df_Cs137_662_sim_iso_066_smear[i], 0.14)    

y, x = np.histogram(df_Cs137_662_sim_iso_066_smear, bins=600, range=[0,600])
newData = getRandDist(x, y)
y, x = np.histogram(newData*9*8.2, bins=numBins, range=binRange)
x = prodata.getBinCenters(x)
plt.step(x, y*23.0,  lw=2, label='066 - Glisure SetPolish(0.10) + 0.1 mm airgap smear')

# y, x = np.histogram(df_Cs137_662_sim_iso_067, bins=1200, range=[0,1200])
# newData = getRandDist(x, y)
# y, x = np.histogram(newData*9*4, bins=numBins, range=binRange)
# x = prodata.getBinCenters(x)
# plt.step(x, y*5,  lw=2, label='067 - Glisure SetPolish(0.10) 4.44 MeV')

# df_Cs137_662_sim_iso_067_smear = df_Cs137_662_sim_iso_067

# for i in range(len(df_Cs137_662_sim_iso_067)):
#     df_Cs137_662_sim_iso_067_smear[i] = prosim.gaussSmear(df_Cs137_662_sim_iso_067_smear[i], 0.17)    

# y, x = np.histogram(df_Cs137_662_sim_iso_067_smear, bins=1200, range=[0, 1200])
# newData = getRandDist(x, y)
# y, x = np.histogram(newData*9*5.95, bins=numBins, range=binRange)
# x = prodata.getBinCenters(x)
# plt.step(x, y*5.3,  lw=2, label='067 - Glisure SetPolish(0.10) 4.44 MeV smear')


# y, x = np.histogram(df_Cs137_662_sim_iso_068, bins=1200, range=[0,1200])
# newData = getRandDist(x, y)
# y, x = np.histogram(newData*77, bins=numBins, range=binRange)
# x = prodata.getBinCenters(x)
# plt.step(x, y*16,  lw=2, label='068 - Glisure SetPolish(0.10) 2.62 MeV')

# df_Cs137_662_sim_iso_068_smear = df_Cs137_662_sim_iso_068

# for i in range(len(df_Cs137_662_sim_iso_068)):
#     df_Cs137_662_sim_iso_068_smear[i] = prosim.gaussSmear(df_Cs137_662_sim_iso_068_smear[i], 0.085)    

# y, x = np.histogram(df_Cs137_662_sim_iso_068_smear, bins=1200, range=[0, 1200])
# newData = getRandDist(x, y)
# y, x = np.histogram(newData*78.5, bins=numBins, range=binRange)
# x = prodata.getBinCenters(x)
# plt.step(x, y*19.75,  lw=2, label='068 - Glisure SetPolish(0.10) 2.62 MeV smear')


plt.xlim([0, 100000])
# plt.ylim([0, 21000])
plt.legend()
plt.show()


# plt.hist(test.optPhotonSum, bins=75, label="orignal", histtype='step')
# plt.hist(test.query('edep>50').optPhotonSum, bins=75, label=">50 keV", histtype='step')
# plt.legend()
# plt.show()


pencilbeam = pd.read_csv(path_sim + "NE213A/Cs137/pencilbeam/CSV_optphoton_data_sum.csv", names=names)
# test = pd.read_csv(path_sim + "NE213A/TMP/runs/Cs137/030/CSV_optphoton_data_sum.csv", names=names)
test = pd.read_csv(path_sim + "NE213A/TMP/runs/Cs137/059/CSV_optphoton_data_sum.csv", names=names)
test['linearFunc'] = promath.linearFunc(test.edep, 0.58, 0)

x = np.arange(0, 3000)
y = promath.linearFunc(x, 0.58, 0)
plt.plot(x,y, lw=2)
plt.hist2d(test.edep, test.optPhotonSum, bins=[150, 150], cmap=plt.cm.jet, norm=LogNorm() )
plt.xlabel('Edep [keV]')
plt.ylabel('Scintillation [ADC]')
plt.colorbar()
plt.show()


plt.subplot(1, 2, 1)
plt.title('higher LY')
y, x = np.histogram(test.optPhotonSum, bins=1200, range=[0,2400])
newData = prodata.getRandDist(x, y)
y, x = np.histogram(newData, bins=300, range=[0,2400])
x = prodata.getBinCenters(x)
plt.step(x, y, label='original')

y, x = np.histogram(test.query(f'optPhotonSum>linearFunc').optPhotonSum, bins=1200, range=[0,2400])
newData = prodata.getRandDist(x, y)
y, x = np.histogram(newData, bins=300, range=[0,2400])
x = prodata.getBinCenters(x)
plt.step(x, y, label='with cuts')
plt.legend()

plt.subplot(1, 2, 2)
plt.title('lower LY')
y, x = np.histogram(test.optPhotonSum, bins=1200, range=[0,2400])
newData = prodata.getRandDist(x, y)
y, x = np.histogram(newData, bins=300, range=[0,2400])
x = prodata.getBinCenters(x)
plt.step(x, y, label='original')

y, x = np.histogram(test.query(f'optPhotonSum<linearFunc').optPhotonSum, bins=1200, range=[0,2400])
newData = getRandDist(x, y)
y, x = np.histogram(newData, bins=300, range=[0,2400])
x = prodata.getBinCenters(x)
plt.step(x, y, label='with cuts')

plt.legend()
plt.show()


##############################################

plt.subplot(2, 2, 1)
plt.title('low LY')
plt.hist(test.query(f'optPhotonSum<linearFunc and optPhotonSumCompton>0').yLoc, bins=50)
plt.Circle((0,0),50,color='black')
plt.Circle((0,0),47,color='teal')
plt.xlabel('y-axis [mm]')
# plt.xlim([-60,60])
# plt.ylim([-60,60])

plt.subplot(2, 2, 2)
plt.title('high LY')
plt.hist(test.query(f'optPhotonSum>linearFunc and optPhotonSumCompton>0').yLoc, bins=50)
plt.Circle((0,0),50,color='black')
plt.Circle((0,0),47,color='teal')
# plt.xlim([-60,60])
# plt.ylim([-60,60])
plt.xlabel('y-axis [mm]')


###################################################################################
plt.subplot(2, 2, 1)
plt.title('low LY')
plt.hist2d(test.query(f'optPhotonSum<linearFunc and optPhotonSumCompton>0').xLoc, test.query(f'optPhotonSum<linearFunc and optPhotonSumCompton>0').zLoc, bins=[25, 25], cmap=plt.cm.jet, norm=LogNorm() )
plt.Circle((0,0),50,color='black')
plt.Circle((0,0),47,color='teal')
plt.xlabel('X-axis [mm]')
plt.xlabel('Z-axis [mm]')
plt.xlim([-60,60])
plt.ylim([-60,60])

plt.subplot(2, 2, 2)
plt.title('high LY')
plt.hist2d(test.query(f'optPhotonSum>linearFunc and optPhotonSumCompton>0').xLoc, test.query(f'optPhotonSum>linearFunc and optPhotonSumCompton>0').zLoc, bins=[25, 25], cmap=plt.cm.jet, norm=LogNorm() )
plt.Circle((0,0),50,color='black')
plt.Circle((0,0),47,color='teal')
plt.xlim([-60,60])
plt.ylim([-60,60])
plt.plot()
plt.xlabel('X-axis [mm]')
plt.ylabel('Z-axis [mm]')

plt.subplot(2, 2, 3)
plt.hist(test.query(f'optPhotonSum<linearFunc and optPhotonSumCompton>0').edep, bins=100, range=[460,600])
plt.xlim([460, 600])
plt.xlabel('Edep [keV]')

plt.subplot(2, 2, 4)
plt.hist(test.query(f'optPhotonSum>linearFunc and optPhotonSumCompton>0').edep, bins=100, range=[460,600])
plt.xlim([460, 600])
plt.xlabel('Edep [keV]')
plt.show()

###################################################################################

###################################################################################
plt.subplot(2, 2, 1)
plt.title('low LY')
plt.hist2d(test.query(f'optPhotonSumCompton>166 and optPhotonSumCompton<230 and edep>420 and edep<480').xLoc, test.query(f'optPhotonSumCompton>166 and optPhotonSumCompton<230 and edep>420 and edep<480').zLoc, bins=[25, 25], cmap=plt.cm.jet, norm=LogNorm() )
plt.Circle((0,0),50,color='black')
plt.Circle((0,0),47,color='teal')
plt.xlabel('X-axis [mm]')
plt.xlabel('Z-axis [mm]')
plt.xlim([-60,60])
plt.ylim([-60,60])

plt.subplot(2, 2, 2)
plt.title('high LY')
plt.hist2d(test.query(f'optPhotonSumCompton>280 and optPhotonSumCompton<350 and edep>420 and edep<480').xLoc, test.query(f'optPhotonSumCompton>280 and optPhotonSumCompton<350 and edep>420 and edep<480').zLoc, bins=[25, 25], cmap=plt.cm.jet, norm=LogNorm() )
plt.Circle((0,0),50,color='black')
plt.Circle((0,0),47,color='teal')
plt.xlim([-60,60])
plt.ylim([-60,60])
plt.plot()
plt.xlabel('X-axis [mm]')
plt.ylabel('Z-axis [mm]')

plt.subplot(2, 2, 3)
plt.hist(test.query(f'optPhotonSumCompton>166 and optPhotonSumCompton<230 and edep>420 and edep<480').edep, bins=100, range=[460,600])
plt.xlim([460, 600])
plt.xlabel('Edep [keV]')

plt.subplot(2, 2, 4)
plt.hist(test.query(f'optPhotonSumCompton>280 and optPhotonSumCompton<350 and edep>420 and edep<480').edep, bins=100, range=[460,600])
plt.xlim([460, 600])
plt.xlabel('Edep [keV]')
plt.show()

###################################################################################






numBins = 600
binRange = [0, 600*190]
plt.figure()
plt.title('001 no reflectivity')
y, x = np.histogram(df_Cs137_662_sim_iso_old*190, bins=numBins, range=binRange)
x = prodata.getBinCenters(x)
plt.step(x, y*5.5,  label='Cs137 simulation before')
y, x = np.histogram(df_Cs137_662_sim_iso_001*190, bins=numBins, range=binRange)
x = prodata.getBinCenters(x)
plt.step(x, y*5.5,  label='Cs137 simulation 001')
plt.legend()

plt.figure()
plt.title('002 old abs. lenght')
y, x = np.histogram(df_Cs137_662_sim_iso_old*190, bins=numBins, range=binRange)
x = prodata.getBinCenters(x)
plt.step(x, y*5.5,  label='Cs137 simulation before')
y, x = np.histogram(df_Cs137_662_sim_iso_002*190, bins=numBins, range=binRange)
x = prodata.getBinCenters(x)
plt.step(x, y*5.5, label='Cs137 simulation 002')
plt.legend()




plt.figure()
plt.title('New geometry test')
numBins = 600
binRange = [0, 600*190]
ybg, xbg = np.histogram(df_bg.qdc_lg_ch1*att_factor_A, bins=numBins, range=binRange)
y, x = np.histogram(df_Cs137.qdc_lg_ch1*att_factor_A, bins=numBins, range=binRange)
x = prodata.getBinCenters(x)
plt.step(x, y-(ybg/10), lw=2, color='black', label='Cs137 Data')

y, x = np.histogram(df_Cs137_662_sim_iso_021, bins=1200, range=[0,1200])
newData = getRandDist(x, y)
y, x = np.histogram(newData*72, bins=1200, range=[0,1200*220])
x = prodata.getBinCenters(x)
plt.step(x, y*5, label='Cs137 Al reflectivity + dialectric_metal')

y, x = np.histogram(df_Cs137_662_sim_iso_026, bins=1200, range=[0,1200])
newData = getRandDist(x, y)
y, x = np.histogram(newData*17, bins=1200, range=[0,1200*220])
x = prodata.getBinCenters(x)
plt.step(x, y*24, label='Cs137 Al reflectivity + new geometry')

y, x = np.histogram(df_Cs137_662_sim_iso_027, bins=1200, range=[0,1200])
newData = getRandDist(x, y)
y, x = np.histogram(newData*13, bins=1200, range=[0,1200*220])
x = prodata.getBinCenters(x)
plt.step(x, y*24, label='Cs137 Al reflectivity + new geometry + LG paint')

plt.legend()
plt.show()
# y, x = np.histogram(df_Cs137_662_sim_iso_022, bins=600, range=[0,600])
# newData = getRandDist(x, y)
# y, x = np.histogram(newData*12, bins=numBins, range=binRange)
# x = prodata.getBinCenters(x)
# plt.step(x, y*30,  lw=2, label='Cs137, old geometry, full iso')

# y, x = np.histogram(df_Cs137_662_sim_iso_023, bins=600, range=[0,600])
# newData = getRandDist(x, y)
# y, x = np.histogram(newData*12, bins=numBins, range=binRange)
# x = prodata.getBinCenters(x)
# plt.step(x, y*30,  lw=2, label='Cs137, new geometry, full iso')

# y, x = np.histogram(df_Cs137_662_sim_iso_024, bins=600, range=[0,600])
# newData = getRandDist(x, y)
# y, x = np.histogram(newData*100, bins=numBins, range=binRange)
# x = prodata.getBinCenters(x)
# plt.step(x, y*33,  lw=2, label='Cs137, new geometry, full iso, diale-diale')

# y, x = np.histogram(df_Cs137_662_sim_iso_025, bins=600, range=[0,600])
# newData = getRandDist(x, y)
# y, x = np.histogram(newData*13, bins=numBins, range=binRange)
# x = prodata.getBinCenters(x)
# plt.step(x, y*22,  lw=2, label='Cs137, new geometry, full iso, air')

# y, x = np.histogram(df_Cs137_662_sim_iso_019, bins=600, range=[0,600])
# newData = getRandDist(x, y)
# y, x = np.histogram(newData*190, bins=numBins, range=binRange)
# x = prodata.getBinCenters(x)
# plt.step(x, y*5.5, label='Cs137 no LG reflector, no cup reflector')



# df_Cs137_662_sim_iso_023_smear = df_Cs137_662_sim_iso_023

# for i in range(len(df_Cs137_662_sim_iso_023)):
#     df_Cs137_662_sim_iso_023_smear[i] = prosim.gaussSmear(df_Cs137_662_sim_iso_023[i], 0.20)    

# y, x = np.histogram(df_Cs137_662_sim_iso_023_smear, bins=600, range=[0,600])
# newData = getRandDist(x, y)
# y, x = np.histogram(newData*12, bins=numBins, range=binRange)
# x = prodata.getBinCenters(x)
# plt.step(x, y*30,  lw=2, label='Cs137, new geometry, full iso (SMEARING)')

# y, x = np.histogram(df_Cs137_662_sim_iso_test, bins=600, range=[0,600])
# newData = getRandDist(x, y)
# y, x = np.histogram(newData*12*4.5, bins=numBins, range=binRange)
# x = prodata.getBinCenters(x)
# plt.step(x, y*1.5, lw=2, label='Cs137, article, not full iso',)
plt.xlim([0, 10000])
plt.legend()
plt.show()




plt.figure()
plt.title('Reflective paint')
ybg, xbg = np.histogram(df_bg.qdc_lg_ch1*att_factor_A, bins=numBins, range=binRange)
y, x = np.histogram(df_Cs137.qdc_lg_ch1*att_factor_A, bins=numBins, range=binRange)
x = prodata.getBinCenters(x)
plt.step(x, y-(ybg/10), lw=2, color='black', label='Cs137 Data')

y, x = np.histogram(df_Cs137_662_sim_iso_old, bins=600, range=[0,600])
newData = getRandDist(x, y)
y, x = np.histogram(newData*190, bins=numBins, range=binRange)
x = prodata.getBinCenters(x)
plt.step(x, y*5.5,  label='Cs137 simulation original with paint')

y, x = np.histogram(df_Cs137_662_sim_iso_018, bins=600, range=[0,600])
newData = getRandDist(x, y)
y, x = np.histogram(newData*190, bins=numBins, range=binRange)
x = prodata.getBinCenters(x)
plt.step(x, y*5.5, label='Cs137 no LG reflector')

y, x = np.histogram(df_Cs137_662_sim_iso_019, bins=600, range=[0,600])
newData = getRandDist(x, y)
y, x = np.histogram(newData*190, bins=numBins, range=binRange)
x = prodata.getBinCenters(x)
plt.step(x, y*5.5, label='Cs137 no LG reflector, no cup reflector')

y, x = np.histogram(df_Cs137_662_sim_iso_020, bins=600, range=[0,600])
newData = getRandDist(x, y)
y, x = np.histogram(newData*220, bins=numBins, range=binRange)
x = prodata.getBinCenters(x)
plt.step(x, y*5.5, label='Cs137 Al reflectivity included')

y, x = np.histogram(df_Cs137_662_sim_iso_021, bins=1200, range=[0,1200])
newData = getRandDist(x, y)
y, x = np.histogram(newData*72, bins=1200, range=[0,1200*220])
x = prodata.getBinCenters(x)
plt.step(x, y*4.4, label='Cs137 Al reflectivity + dialectric_metal')

plt.xlim([0, 20000])
plt.legend()
plt.show()


plt.figure()
plt.title('index of refraction')
ybg, xbg = np.histogram(df_bg.qdc_lg_ch1*att_factor_A, bins=numBins, range=binRange)
y, x = np.histogram(df_Cs137.qdc_lg_ch1*att_factor_A, bins=numBins, range=binRange)
x = prodata.getBinCenters(x)
plt.step(x, y-(ybg/10), lw=2, color='black', label='Cs137 Data')

y, x = np.histogram(df_Cs137_662_sim_iso_003, bins=600, range=[0,600])
newData = getRandDist(x, y)
y, x = np.histogram(newData*190, bins=numBins, range=binRange)
x = prodata.getBinCenters(x)
plt.step(x, y*5.5, label='Cs137 simulation n=1')

# y, x = np.histogram(df_Cs137_662_sim_iso_017, bins=numBins, range=[0,600])
# newData = getRandDist(x, y)
# y, x = np.histogram(newData*190, bins=numBins, range=binRange)
# x = prodata.getBinCenters(x)
# plt.step(x, y*5.5, label='Cs137 simulation n=1.5')

y, x = np.histogram(df_Cs137_662_sim_iso_015, bins=numBins, range=[0,600])
newData = getRandDist(x, y)
y, x = np.histogram(newData*190, bins=numBins, range=binRange)
x = prodata.getBinCenters(x)
plt.step(x, y*5.5,  label='Cs137 simulation n=2')

y, x = np.histogram(df_Cs137_662_sim_iso_old, bins=numBins, range=[0,600])
newData = getRandDist(x, y)
y, x = np.histogram(newData*190, bins=numBins, range=binRange)
x = prodata.getBinCenters(x)
plt.step(x, y*5.5,  label='Cs137 simulation n=2.5')

# y, x = np.histogram(df_Cs137_662_sim_iso_016, bins=numBins, range=[0,600])
# newData = getRandDist(x, y)
# y, x = np.histogram(newData*190, bins=numBins, range=binRange)
# x = prodata.getBinCenters(x)
# plt.step(x, y*5.5,  label='Cs137 simulation n=3')

# y, x = np.histogram(df_Cs137_662_sim_iso_013, bins=numBins, range=[0,600])
# newData = getRandDist(x, y)
# y, x = np.histogram(newData*190, bins=numBins, range=binRange)
# x = prodata.getBinCenters(x)
# plt.step(x, y*5.5, label='Cs137 simulation n=4')

y, x = np.histogram(df_Cs137_662_sim_iso_014, bins=numBins, range=[0,600])
newData = getRandDist(x, y)
y, x = np.histogram(newData*190, bins=numBins, range=binRange)
x = prodata.getBinCenters(x)
plt.step(x, y*5.5, label='Cs137 simulation n=5')
plt.legend()
plt.show()


plt.figure()
plt.title('004/005 reflectivity 50/100%')
y, x = np.histogram(df_Cs137_662_sim_iso_old*190, bins=numBins, range=binRange)
x = prodata.getBinCenters(x)
plt.step(x, y*5.5,  label='Cs137 simulation before')
y, x = np.histogram(df_Cs137_662_sim_iso_004*190, bins=numBins, range=binRange)
x = prodata.getBinCenters(x)
plt.step(x, y*5.5*(1+1/16),  label='Cs137 simulation 004 50%')
y, x = np.histogram(df_Cs137_662_sim_iso_005*190, bins=numBins, range=binRange)
x = prodata.getBinCenters(x)
plt.step(x, y*5.5,  label='Cs137 simulation 005 100%')
plt.legend()

plt.figure()
plt.title('006 Incl. optical surfaces')
y, x = np.histogram(df_Cs137_662_sim_iso_old*190, bins=numBins, range=binRange)
x = prodata.getBinCenters(x)
plt.step(x, y*5.5,  label='Cs137 simulation before')
y, x = np.histogram(df_Cs137_662_sim_iso_006*190, bins=numBins, range=binRange)
x = prodata.getBinCenters(x)
plt.step(x, y*5.5,  label='Cs137 simulation 006')
plt.legend()


plt.figure()
plt.title('Light yield')
# y, x = np.histogram(df_Cs137_662_sim_iso_008, bins=600, range=[0,600])
# newData = getRandDist(x, y)
# y, x = np.histogram(newData*460, bins=numBins, range=binRange)
# x = prodata.getBinCenters(x)
# plt.step(x, y*7.0,  label='Cs137 simulation 4/keV')

y, x = np.histogram(df_Cs137_662_sim_iso_009, bins=600, range=[0,600])
newData = getRandDist(x, y)
y, x = np.histogram(newData*350, bins=numBins, range=binRange)
x = prodata.getBinCenters(x)
plt.step(x, y*6.6,  label='Cs137 simulation 5/keV')

# y, x = np.histogram(df_Cs137_662_sim_iso_010, bins=600, range=[0,600])
# newData = getRandDist(x, y)
# y, x = np.histogram(newData*290, bins=numBins, range=binRange)
# x = prodata.getBinCenters(x)
# plt.step(x, y*6.6,  label='Cs137 simulation 6/keV')

# y, x = np.histogram(df_Cs137_662_sim_iso_011, bins=600, range=[0,600])
# newData = getRandDist(x, y)
# y, x = np.histogram(newData*260, bins=numBins, range=binRange)
# x = prodata.getBinCenters(x)
# plt.step(x, y*6.5,  label='Cs137 simulation 7/keV')

y, x = np.histogram(df_Cs137_662_sim_iso_old, bins=600, range=[0,600])
newData = getRandDist(x, y)
y, x = np.histogram(newData*220, bins=numBins, range=binRange)
x = prodata.getBinCenters(x)
plt.step(x, y*6.3,  label='Cs137 simulation 8/keV')

# y, x = np.histogram(df_Cs137_662_sim_iso_007, bins=600, range=[0,600])
# newData = getRandDist(x, y)
# y, x = np.histogram(newData*140, bins=numBins, range=binRange)
# x = prodata.getBinCenters(x)
# plt.step(x, y*6.0,  label='Cs137 simulation 12/keV')

# y, x = np.histogram(df_Cs137_662_sim_iso_012, bins=600, range=[0,600])
# newData = getRandDist(x, y)
# y, x = np.histogram(newData*45, bins=numBins, range=binRange)
# x = prodata.getBinCenters(x)
# plt.step(x, y*5.5,  label='Cs137 simulation 36/keV')

ybg, xbg = np.histogram(df_bg.qdc_lg_ch1*att_factor_A, bins=numBins, range=binRange)
y, x = np.histogram(df_Cs137.qdc_lg_ch1*att_factor_A, bins=numBins, range=binRange)
x = prodata.getBinCenters(x)
plt.step(x, y-(ybg/10), lw=2, color='black', label='Cs137 Data')

plt.legend()
plt.xlim([0,600*190])
plt.show()



# counts, bins = np.histogram(df_Cs137_662_sim_iso_old, bins=500, range=binRange)
# newData = getRandDist(bins, counts)
# counts, bins = np.histogram(newData, bins=450, range=binRange)
# binCenters = prodata.getBinCenters(bins)
# plt.step(binCenters, counts,  label='Cs137 simulation random')

# counts, bins = np.histogram(df_Cs137_662_sim_iso_old, bins=500, range=binRange)
# newData = getEvenDist(bins, counts)
# counts, bins = np.histogram(newData, bins=450, range=binRange)
# binCenters = prodata.getBinCenters(bins)
# plt.step(binCenters, counts,  label='Cs137 simulation even')


def getRandDist(bins, data):
    """ calculates a random distribution for each bin based on bin content, thus allowing rebinning of histogram """
    return np.array([
        # get random number in the range lowedge < x < highedge for the current bin
        (bins[binno + 1] - bins[binno])*np.random.random_sample() + bins[binno]
        # loop over all bins
        for binno in range(0, len(bins)-1)
        # for each entry in bin; convert float to int; does not handle negative entries!
        for count in range (0, max(0,int(round(data[binno]))))
    ])

def getEvenDist(bins, data):
    """ calculates a random distribution for each bin based on bin content, thus allowing rebinning of histogram """
    return np.array([
        # get fraction number in the range lowedge < x < highedge for the current bin
        (bins[binno + 1] - bins[binno])*(count/data[binno]) + bins[binno]
        # loop over all bins
        for binno in range(0, len(bins)-1)
        # for each entry in bin; convert float to int; does not handle negative entries!
        for count in range (0, max(0,int(round(data[binno]))))
    ]) 