

# <examples/doc_fitting_withreport.py>
from numpy import exp, linspace, pi, random, sign, sin
import numpy as np
from lmfit import Parameters, fit_report, minimize
import matplotlib.pyplot as plt

path_sim = '/media/gheed/Seagate_Expansion_Drive1/data/simulation/'
sim_Cs137_iso =      np.genfromtxt(path_sim + 'NE213A/Cs137/isotropic/CSV_optphoton_sum_photocathode_no_cut_QE_smear.csv', delimiter=',') #


binRange = list(range(65, 250, 5))

counts, bin_edges = np.histogram(sim_Cs137_iso, binRange)
bin_centers =  (bin_edges[1:] + bin_edges[:-1]) / 2.0 

def plot_binned_data(axes, binedges, data, histtype='step', *args, **kwargs):
    """ from: https://stackoverflow.com/a/19361027 """
    #The dataset values are the bin centres
    x = (binedges[1:] + binedges[:-1]) / 2.0
    #The weights are the y-values of the input binned data
    weights = data
    return axes.hist(x, bins=binedges, weights=weights, histtype=histtype, *args, **kwargs) 

def residual(pars, x, data=None):
    """x is the model to be varied, compared against data, which is static"""

    vals = pars.valuesdict()
    scale = vals['scale']
    offset = vals['offset']
    
    hist, binEdges = np.histogram(x*offset, binRange)
    


    model = hist * scale# np.max(data) / np.max(hist)
    
    ans = (data - model).astype(np.float)
    # ans = sum(ans**2)
    
    # print(f'return = {ans}')    
    print(f'scale={scale}')
    print(f'offset={offset}')
    print('-------------')

    return ans



x = sim_Cs137_iso[::2]*2 #Only use every other entry to reduce the number of counts by ~2
noise = random.normal(scale=1000, size=counts.size) #Introduce noise in the data set
data = counts + noise #make data to be used to compare against


fit_params = Parameters() #Initial guesses for parameters
fit_params.add('scale', 
                value = 2.5,
                min = .5, 
                max = 5)
fit_params.add('offset', 
                value = .6,
                min = 0.05,
                max = 1)


out = minimize(residual, fit_params, args=(x,), kws={'data': data}, epsfcn=1e-1) #apply the minimization function
print(fit_report(out))
results = fit_report(out)

model_counts, bin_edges = np.histogram(x*out.params['offset'].value, binRange)
hist2, bin_edges = np.histogram(x, binRange)

fig, ax = plt.subplots()
plot_binned_data(ax, bin_edges, counts, label="Data no noise") 
# plot_binned_data(ax, bin_edges, data, label="Data with noise") 
plot_binned_data(ax, bin_edges, model_counts*out.params['scale'].value , lw = 2, label="Best scaling value") 
plot_binned_data(ax, bin_edges, hist2, label="Before scaling") 

plt.legend()
plt.show()


#Read more at: https://lmfit.github.io/lmfit-py/fitting.html#writing-a-fitting-function

# plt.hist(sim_Cs137_iso*1, numBins, binRange)
# plt.hist(sim_Cs137_iso*2, numBins, binRange)
# plt.hist(sim_Cs137_iso*3, numBins, binRange)

# plt.legend()
# plt.show()
