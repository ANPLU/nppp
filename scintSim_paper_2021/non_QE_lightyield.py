######TRIAL FOR NON  DATA################
df_Cs137_662_sim_pen =      pd.read_csv(path_sim + "NE213A/Cs137/pencilbeam/CSV_optphoton_data_sum.csv", names=names)
df_Na22_sim_pen =           pd.read_csv(path_sim + "NE213A/Na22/pencilbeam/CSV_optphoton_data_sum.csv", names=names)
df_Na22_511_sim_pen =       pd.read_csv(path_sim + "NE213A/Na22_511/pencilbeam/CSV_optphoton_data_sum.csv", names=names)
df_Na22_1275_sim_pen =      pd.read_csv(path_sim + "NE213A/Na22_1275/pencilbeam/CSV_optphoton_data_sum.csv", names=names)
df_Co60_sim_pen =           pd.read_csv(path_sim + "NE213A/Co60/pencilbeam/CSV_optphoton_data_sum.csv", names=names)
df_Co60_1173_sim_pen =      pd.read_csv(path_sim + "NE213A/Co60_1173/pencilbeam/CSV_optphoton_data_sum.csv", names=names)
df_Co60_1332_sim_pen =      pd.read_csv(path_sim + "NE213A/Co60_1332/pencilbeam/CSV_optphoton_data_sum.csv", names=names)
df_2615_sim_pen =           pd.read_csv(path_sim + "NE213A/2_62MeV/pencilbeam/CSV_optphoton_data_sum.csv", names=names)
df_4439_sim_pen =           pd.read_csv(path_sim + "NE213A/4_44MeV/pencilbeam/CSV_optphoton_data_sum.csv", names=names)

#INCREASE STATISTICS
df_Cs137_662_sim_pen =  pd.concat([df_Cs137_662_sim_pen, pd.read_csv(path_sim + "NE213A/Cs137/pencilbeam/part2/CSV_optphoton_data_sum.csv", names=names)])
df_Cs137_662_sim_pen =  pd.concat([df_Cs137_662_sim_pen, pd.read_csv(path_sim + "NE213A/Cs137/pencilbeam/part3/CSV_optphoton_data_sum.csv", names=names)])
df_Cs137_662_sim_pen =  pd.concat([df_Cs137_662_sim_pen, pd.read_csv(path_sim + "NE213A/Cs137/pencilbeam/part4/CSV_optphoton_data_sum.csv", names=names)])

df_Na22_sim_pen =       pd.concat([df_Na22_sim_pen, pd.read_csv(path_sim + "NE213A/Na22/pencilbeam/part2/CSV_optphoton_data_sum.csv", names=names)])
df_Na22_sim_pen =       pd.concat([df_Na22_sim_pen, pd.read_csv(path_sim + "NE213A/Na22/pencilbeam/part3/CSV_optphoton_data_sum.csv", names=names)])
df_Na22_sim_pen =       pd.concat([df_Na22_sim_pen, pd.read_csv(path_sim + "NE213A/Na22/pencilbeam/part4/CSV_optphoton_data_sum.csv", names=names)])

df_Na22_511_sim_pen =   pd.concat([df_Na22_511_sim_pen, pd.read_csv(path_sim + "NE213A/Na22_511/pencilbeam/part2/CSV_optphoton_data_sum.csv", names=names)])
df_Na22_511_sim_pen =   pd.concat([df_Na22_511_sim_pen, pd.read_csv(path_sim + "NE213A/Na22_511/pencilbeam/part3/CSV_optphoton_data_sum.csv", names=names)])
df_Na22_511_sim_pen =   pd.concat([df_Na22_511_sim_pen, pd.read_csv(path_sim + "NE213A/Na22_511/pencilbeam/part4/CSV_optphoton_data_sum.csv", names=names)])
df_Na22_511_sim_pen =   pd.concat([df_Na22_511_sim_pen, pd.read_csv(path_sim + "NE213A/Na22_511/pencilbeam/part5/CSV_optphoton_data_sum.csv", names=names)])
df_Na22_511_sim_pen =   pd.concat([df_Na22_511_sim_pen, pd.read_csv(path_sim + "NE213A/Na22_511/pencilbeam/part6/CSV_optphoton_data_sum.csv", names=names)])
df_Na22_511_sim_pen =   pd.concat([df_Na22_511_sim_pen, pd.read_csv(path_sim + "NE213A/Na22_511/pencilbeam/part7/CSV_optphoton_data_sum.csv", names=names)])

df_Na22_1275_sim_pen =  pd.concat([df_Na22_1275_sim_pen, pd.read_csv(path_sim + 'NE213A/Na22_1275/pencilbeam/part2/CSV_optphoton_data_sum.csv', names=names)])
df_Na22_1275_sim_pen =  pd.concat([df_Na22_1275_sim_pen, pd.read_csv(path_sim + 'NE213A/Na22_1275/pencilbeam/part3/CSV_optphoton_data_sum.csv', names=names)])
df_Na22_1275_sim_pen =  pd.concat([df_Na22_1275_sim_pen, pd.read_csv(path_sim + 'NE213A/Na22_1275/pencilbeam/part4/CSV_optphoton_data_sum.csv', names=names)])
df_Na22_1275_sim_pen =  pd.concat([df_Na22_1275_sim_pen, pd.read_csv(path_sim + 'NE213A/Na22_1275/pencilbeam/part5/CSV_optphoton_data_sum.csv', names=names)])
df_Na22_1275_sim_pen =  pd.concat([df_Na22_1275_sim_pen, pd.read_csv(path_sim + 'NE213A/Na22_1275/pencilbeam/part6/CSV_optphoton_data_sum.csv', names=names)])
df_Na22_1275_sim_pen =  pd.concat([df_Na22_1275_sim_pen, pd.read_csv(path_sim + 'NE213A/Na22_1275/pencilbeam/part7/CSV_optphoton_data_sum.csv', names=names)])

df_Co60_sim_pen =       pd.concat([df_Co60_sim_pen, pd.read_csv(path_sim + "NE213A/Co60/pencilbeam/part2/CSV_optphoton_data_sum.csv", names=names)])
df_Co60_sim_pen =       pd.concat([df_Co60_sim_pen, pd.read_csv(path_sim + "NE213A/Co60/pencilbeam/part3/CSV_optphoton_data_sum.csv", names=names)])
df_Co60_sim_pen =       pd.concat([df_Co60_sim_pen, pd.read_csv(path_sim + "NE213A/Co60/pencilbeam/part4/CSV_optphoton_data_sum.csv", names=names)])

df_Co60_1173_sim_pen =  pd.concat([df_Co60_1173_sim_pen, pd.read_csv(path_sim + 'NE213A/Co60_1173/pencilbeam/part2/CSV_optphoton_data_sum.csv', names=names)])
df_Co60_1173_sim_pen =  pd.concat([df_Co60_1173_sim_pen, pd.read_csv(path_sim + 'NE213A/Co60_1173/pencilbeam/part3/CSV_optphoton_data_sum.csv', names=names)])
df_Co60_1173_sim_pen =  pd.concat([df_Co60_1173_sim_pen, pd.read_csv(path_sim + 'NE213A/Co60_1173/pencilbeam/part4/CSV_optphoton_data_sum.csv', names=names)])
df_Co60_1173_sim_pen =  pd.concat([df_Co60_1173_sim_pen, pd.read_csv(path_sim + 'NE213A/Co60_1173/pencilbeam/part5/CSV_optphoton_data_sum.csv', names=names)])
df_Co60_1173_sim_pen =  pd.concat([df_Co60_1173_sim_pen, pd.read_csv(path_sim + 'NE213A/Co60_1173/pencilbeam/part6/CSV_optphoton_data_sum.csv', names=names)])
df_Co60_1173_sim_pen =  pd.concat([df_Co60_1173_sim_pen, pd.read_csv(path_sim + 'NE213A/Co60_1173/pencilbeam/part7/CSV_optphoton_data_sum.csv', names=names)])

df_Co60_1332_sim_pen =  pd.concat([df_Co60_1332_sim_pen, pd.read_csv(path_sim + 'NE213A/Co60_1332/pencilbeam/part2/CSV_optphoton_data_sum.csv', names=names)])
df_Co60_1332_sim_pen =  pd.concat([df_Co60_1332_sim_pen, pd.read_csv(path_sim + 'NE213A/Co60_1332/pencilbeam/part3/CSV_optphoton_data_sum.csv', names=names)])
df_Co60_1332_sim_pen =  pd.concat([df_Co60_1332_sim_pen, pd.read_csv(path_sim + 'NE213A/Co60_1332/pencilbeam/part4/CSV_optphoton_data_sum.csv', names=names)])
# df_Co60_1332_sim_pen =  pd.concat([df_Co60_1332_sim_pen, pd.read_csv(path_sim + 'NE213A/Co60_1332/pencilbeam/part5/CSV_optphoton_data_sum.csv', names=names)])
# df_Co60_1332_sim_pen =  pd.concat([df_Co60_1332_sim_pen, pd.read_csv(path_sim + 'NE213A/Co60_1332/pencilbeam/part6/CSV_optphoton_data_sum.csv', names=names)])
# df_Co60_1332_sim_pen =  pd.concat([df_Co60_1332_sim_pen, pd.read_csv(path_sim + 'NE213A/Co60_1332/pencilbeam/part7/CSV_optphoton_data_sum.csv', names=names)])

df_2615_sim_pen =       pd.concat([df_2615_sim_pen, pd.read_csv(path_sim + 'NE213A/2_62MeV/pencilbeam/part2/CSV_optphoton_data_sum.csv', names=names)])
df_2615_sim_pen =       pd.concat([df_2615_sim_pen, pd.read_csv(path_sim + 'NE213A/2_62MeV/pencilbeam/part3/CSV_optphoton_data_sum.csv', names=names)])
df_2615_sim_pen =       pd.concat([df_2615_sim_pen, pd.read_csv(path_sim + 'NE213A/2_62MeV/pencilbeam/part4/CSV_optphoton_data_sum.csv', names=names)])
df_2615_sim_pen =       pd.concat([df_2615_sim_pen, pd.read_csv(path_sim + 'NE213A/2_62MeV/pencilbeam/part5/CSV_optphoton_data_sum.csv', names=names)])
df_2615_sim_pen =       pd.concat([df_2615_sim_pen, pd.read_csv(path_sim + 'NE213A/2_62MeV/pencilbeam/part6/CSV_optphoton_data_sum.csv', names=names)])
# df_2615_sim_pen =       pd.concat([df_2615_sim_pen, pd.read_csv(path_sim + 'NE213A/2_62MeV/pencilbeam/part7/CSV_optphoton_data_sum.csv', names=names)])
# df_2615_sim_pen =       pd.concat([df_2615_sim_pen, pd.read_csv(path_sim + 'NE213A/2_62MeV/pencilbeam/part8/CSV_optphoton_data_sum.csv', names=names)])

df_4439_sim_pen =       pd.concat([df_4439_sim_pen, pd.read_csv(path_sim + 'NE213A/4_44MeV/pencilbeam/part2/CSV_optphoton_data_sum.csv', names=names)])
df_4439_sim_pen =       pd.concat([df_4439_sim_pen, pd.read_csv(path_sim + 'NE213A/4_44MeV/pencilbeam/part3/CSV_optphoton_data_sum.csv', names=names)])
df_4439_sim_pen =       pd.concat([df_4439_sim_pen, pd.read_csv(path_sim + 'NE213A/4_44MeV/pencilbeam/part4/CSV_optphoton_data_sum.csv', names=names)])
df_4439_sim_pen =       pd.concat([df_4439_sim_pen, pd.read_csv(path_sim + 'NE213A/4_44MeV/pencilbeam/part5/CSV_optphoton_data_sum.csv', names=names)])
df_4439_sim_pen =       pd.concat([df_4439_sim_pen, pd.read_csv(path_sim + 'NE213A/4_44MeV/pencilbeam/part6/CSV_optphoton_data_sum.csv', names=names)])



y, x = np.histogram(df_Na22_511_sim_pen.optPhotonSumCompton, bins=4096, range=[0, 4096])
df_Na22_511_sim_pen.optPhotonSumCompton = prodata.getRandDist(x, y)

y, x = np.histogram(df_Na22_1275_sim_pen.optPhotonSumCompton, bins=4096, range=[0, 4096])
df_Na22_1275_sim_pen.optPhotonSumCompton = prodata.getRandDist(x, y)

y, x = np.histogram(df_Co60_1173_sim_pen.optPhotonSumCompton, bins=4096, range=[0, 4096])
df_Co60_1173_sim_pen.optPhotonSumCompton = prodata.getRandDist(x, y)

y, x = np.histogram(df_Co60_1332_sim_pen.optPhotonSumCompton, bins=4096, range=[0, 4096])
df_Co60_1332_sim_pen.optPhotonSumCompton = prodata.getRandDist(x, y)

y, x = np.histogram(df_2615_sim_pen.optPhotonSumCompton, bins=4096, range=[0, 4096])
df_2615_sim_pen.optPhotonSumCompton = prodata.getRandDist(x, y)

y, x = np.histogram(df_4439_sim_pen.optPhotonSumCompton, bins=4096, range=[0, 4096])
df_4439_sim_pen.optPhotonSumCompton = prodata.getRandDist(x, y)


#convert integers to floats
df_Cs137_662_sim_pen.optPhotonSumCompton =    pd.to_numeric(df_Cs137_662_sim_pen.optPhotonSumCompton, downcast='float')
df_Na22_sim_pen.optPhotonSumCompton =         pd.to_numeric(df_Na22_sim_pen.optPhotonSumCompton, downcast='float')
df_Na22_511_sim_pen.optPhotonSumCompton =     pd.to_numeric(df_Na22_511_sim_pen.optPhotonSumCompton, downcast='float')
df_Na22_1275_sim_pen.optPhotonSumCompton =    pd.to_numeric(df_Na22_1275_sim_pen.optPhotonSumCompton, downcast='float')
df_Co60_1173_sim_pen.optPhotonSumCompton =    pd.to_numeric(df_Co60_1173_sim_pen.optPhotonSumCompton, downcast='float')
df_Co60_1332_sim_pen.optPhotonSumCompton =    pd.to_numeric(df_Co60_1332_sim_pen.optPhotonSumCompton, downcast='float')
df_2615_sim_pen.optPhotonSumCompton =         pd.to_numeric(df_2615_sim_pen.optPhotonSumCompton, downcast='float')
df_4439_sim_pen.optPhotonSumCompton =         pd.to_numeric(df_4439_sim_pen.optPhotonSumCompton, downcast='float')


#Reset index of all Pandas data sets:
df_Cs137_662_sim_pen =  df_Cs137_662_sim_pen.reset_index()
df_Na22_sim_pen =       df_Na22_sim_pen.reset_index()
df_Na22_511_sim_pen =   df_Na22_511_sim_pen.reset_index()
df_Na22_1275_sim_pen =  df_Na22_1275_sim_pen.reset_index()
df_Co60_sim_pen =       df_Co60_sim_pen.reset_index()
df_Co60_1173_sim_pen =  df_Co60_1173_sim_pen.reset_index()
df_Co60_1332_sim_pen =  df_Co60_1332_sim_pen.reset_index()
df_2615_sim_pen =       df_2615_sim_pen.reset_index()
df_4439_sim_pen =       df_4439_sim_pen.reset_index()


#calibrate simulated spectra to QDC channels
#pencilbeam
df_Cs137_662_sim_pen.optPhotonSumCompton =      prosim.chargeCalibration(df_Cs137_662_sim_pen.optPhotonSumCompton * q * PMT_gain)# * PMTgainCorrection['Cs137_662']
df_Na22_sim_pen.optPhotonSumCompton =           prosim.chargeCalibration(df_Na22_sim_pen.optPhotonSumCompton        * q * PMT_gain)# * PMTgainCorrection['Na22_1275']
df_Na22_511_sim_pen.optPhotonSumCompton =       prosim.chargeCalibration(df_Na22_511_sim_pen.optPhotonSumCompton    * q * PMT_gain)# * PMTgainCorrection['Na22_511']
df_Na22_1275_sim_pen.optPhotonSumCompton =      prosim.chargeCalibration(df_Na22_1275_sim_pen.optPhotonSumCompton   * q * PMT_gain)# * PMTgainCorrection['Na22_1275']
df_Co60_sim_pen.optPhotonSumCompton =           prosim.chargeCalibration(df_Co60_sim_pen.optPhotonSumCompton        * q * PMT_gain)# * PMTgainCorrection['Co60_1332']
df_Co60_1173_sim_pen.optPhotonSumCompton =      prosim.chargeCalibration(df_Co60_1173_sim_pen.optPhotonSumCompton   * q * PMT_gain)# * PMTgainCorrection['Co60_1332']
df_Co60_1332_sim_pen.optPhotonSumCompton =      prosim.chargeCalibration(df_Co60_1332_sim_pen.optPhotonSumCompton   * q * PMT_gain)# * PMTgainCorrection['Co60_1332']
df_2615_sim_pen.optPhotonSumCompton =           prosim.chargeCalibration(df_2615_sim_pen.optPhotonSumCompton        * q * PMT_gain)# * PMTgainCorrection['Th232_2615']
df_4439_sim_pen.optPhotonSumCompton =           prosim.chargeCalibration(df_4439_sim_pen.optPhotonSumCompton        * q * PMT_gain)# * PMTgainCorrection['AmBe_4439']



#Apply same smearing on pencilbeam data
df_Na22_511_sim_pen.optPhotonSumCompton = df_Na22_511_sim_pen.optPhotonSumCompton.apply(lambda x: prosim.gaussSmear(x, 0.23)) 
df_Cs137_662_sim_pen.optPhotonSumCompton = df_Cs137_662_sim_pen.optPhotonSumCompton.apply(lambda x: prosim.gaussSmear(x, 0.12)) 
df_Na22_1275_sim_pen.optPhotonSumCompton = df_Na22_1275_sim_pen.optPhotonSumCompton.apply(lambda x: prosim.gaussSmear(x, 0.09)) 
df_Co60_1173_sim_pen.optPhotonSumCompton = df_Co60_1173_sim_pen.optPhotonSumCompton.apply(lambda x: prosim.gaussSmear(x, 0.14)) 
df_Co60_1332_sim_pen.optPhotonSumCompton = df_Co60_1332_sim_pen.optPhotonSumCompton.apply(lambda x: prosim.gaussSmear(x, 0.14))
df_2615_sim_pen.optPhotonSumCompton = df_2615_sim_pen.optPhotonSumCompton.apply(lambda x: prosim.gaussSmear(x, 0.065))
df_4439_sim_pen.optPhotonSumCompton = df_4439_sim_pen.optPhotonSumCompton.apply(lambda x: prosim.gaussSmear(x, 0.12)) 


# meanGain = 3.2655079516647136

#Apply average gain on all data
df_Na22_511_sim_pen.optPhotonSumCompton = df_Na22_511_sim_pen.optPhotonSumCompton * meanGain/4
df_Cs137_662_sim_pen.optPhotonSumCompton = df_Cs137_662_sim_pen.optPhotonSumCompton * meanGain/4
df_Co60_1173_sim_pen.optPhotonSumCompton = df_Co60_1173_sim_pen.optPhotonSumCompton * meanGain/4
df_Na22_1275_sim_pen.optPhotonSumCompton = df_Na22_1275_sim_pen.optPhotonSumCompton * meanGain/4
df_Co60_1332_sim_pen.optPhotonSumCompton = df_Co60_1332_sim_pen.optPhotonSumCompton * meanGain/4
df_2615_sim_pen.optPhotonSumCompton = df_2615_sim_pen.optPhotonSumCompton * meanGain/4
df_4439_sim_pen.optPhotonSumCompton = df_4439_sim_pen.optPhotonSumCompton * meanGain/4


plot=True
#Fit the positions of average gain pencilbeam data sets
start = 100
stop = 6000*4.5
counts, bin_edges = np.histogram(df_Na22_511_sim_pen.optPhotonSumCompton, bins = 30, range=[0, stop*1.2])
bin_centers = prodata.getBinCenters(bin_edges)
Ef = 0.510999
sim_res_Na22_511_avg  =  prosim.calibrationFitBinned(counts, bin_centers, start, stop, Ef=0.510999, plot=plot, compton=True)

start = 2000
stop = 6500*4.5
counts, bin_edges = np.histogram(df_Cs137_662_sim_pen.optPhotonSumCompton, bins = 50, range=[0, stop*1.2])
bin_centers = prodata.getBinCenters(bin_edges)
Ef = 0.66166
sim_res_Cs137_662_avg  =  prosim.calibrationFitBinned(counts, bin_centers, start, stop, Ef=Ef, plot=plot, compton=True)

start = 7000
stop = 14000*4.5
counts, bin_edges = np.histogram(df_Co60_1173_sim_pen.optPhotonSumCompton, bins = 50, range=[0, stop*1.2])
bin_centers = prodata.getBinCenters(bin_edges)
Ef = 1.173
sim_res_Co60_1173_avg = prosim.calibrationFitBinned(counts, bin_centers, start, stop, Ef=Ef, plot=plot, compton=True)

start = 10000
stop = 13000*4.5
counts, bin_edges = np.histogram(df_Na22_1275_sim_pen.optPhotonSumCompton, bins = 50, range=[0, stop*1.2])
bin_centers = prodata.getBinCenters(bin_edges)
Ef = 1.2745
sim_res_Na22_1275_avg = prosim.calibrationFitBinned(counts, bin_centers, start, stop, Ef=Ef, plot=plot, compton=True)

start = 10000
stop = 14000*4.5
counts, bin_edges = np.histogram(df_Co60_1332_sim_pen.optPhotonSumCompton, bins = 40, range=[0, stop*1.2])
bin_centers = prodata.getBinCenters(bin_edges)
Ef = 1.3325
sim_res_Co60_1332_avg = prosim.calibrationFitBinned(counts, bin_centers, start, stop, Ef=Ef, plot=plot, compton=True)

start = 17000
stop = 32000*4.5
counts, bin_edges = np.histogram(df_2615_sim_pen.optPhotonSumCompton, bins = 64, range=[0, stop*1.2])
bin_centers = prodata.getBinCenters(bin_edges)
Ef = 2.614533
sim_res_PuBe_2615_avg = prosim.calibrationFitBinned(counts, bin_centers, start, stop, Ef=Ef, plot=plot, compton=True)

start = 30000
stop = 57500*4.5
counts, bin_edges = np.histogram(df_4439_sim_pen.optPhotonSumCompton, bins = 50, range=[0, stop*1.2])
bin_centers = prodata.getBinCenters(bin_edges)
Ef = 4.439
sim_res_PuBe_4439_avg = prosim.calibrationFitBinned(counts, bin_centers, start, stop, Ef=Ef, plot=plot, compton=True)






E_data2 = np.array([0.0 , 0.34066602, 0.4773368, 0.96319883, 1.06166721, 1.11810886, 2.38177893, 4.19740618])

weightedMean = np.array([0.0,  2783.29503379,  4170.099797, 9029.90890236, 10051.93516088, 10481.26227414, 22611.87972234, 37865.62093087])

weightedMean_new = np.array([ #mean value of simulation data fits
    np.average(df_Na22_511_sim_pen.query("optPhotonSumCompton>0").optPhotonSumCompton),
    np.average(df_Cs137_662_sim_pen.query(f'optPhotonSumCompton>{sim_res_Cs137_662_avg["gauss_mean"]-3*sim_res_Cs137_662_avg["gauss_std"]}').optPhotonSumCompton),
    np.average(df_Co60_1173_sim_pen.query(f'optPhotonSumCompton>{sim_res_Co60_1173_avg["gauss_mean"]-3*sim_res_Co60_1173_avg["gauss_std"]}').optPhotonSumCompton),
    np.average(df_Na22_1275_sim_pen.query(f'optPhotonSumCompton>{sim_res_Na22_1275_avg["gauss_mean"]-3*sim_res_Na22_1275_avg["gauss_std"]}').optPhotonSumCompton),
    np.average(df_Co60_1332_sim_pen.query(f'optPhotonSumCompton>{sim_res_Co60_1332_avg["gauss_mean"]-3*sim_res_Co60_1332_avg["gauss_std"]}').optPhotonSumCompton),
    np.average(df_2615_sim_pen.query(f'optPhotonSumCompton>{sim_res_PuBe_2615_avg["gauss_mean"]-3*sim_res_PuBe_2615_avg["gauss_std"]}').optPhotonSumCompton),
    np.average(df_4439_sim_pen.query(f'optPhotonSumCompton>{sim_res_PuBe_4439_avg["gauss_mean"]-3*sim_res_PuBe_4439_avg["gauss_std"]}').optPhotonSumCompton)])

weightedMean_new = np.append(0,  weightedMean_new)

ratio = weightedMean/weightedMean_new
ratioAvg = np.average(ratio[1:-1]) #0.22805959234768913