#!/usr/bin/env python3
from re import I
import sys
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
from numpy.core.arrayprint import SubArrayFormat
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit

# IMPORT FROM LIBRARY DIRECTORY
sys.path.insert(0, "../library/")
sys.path.insert(0, "../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "../")
sys.path.insert(0, "data_analysis/")

import processing_math as promath
import processing_data as prodata
import processing_pd as propd
import processing_simulation as prosim
import gaussEdgeFit_parametersEJ321P as geEJ321P
import gaussEdgeFit_parametersNE213A as geNE213A
import simFit_parametersNE213A as simNE213A

# from sim_energyCal import sim_cal
from data_energyCal import NE213A_lg_cal

"""
Script for running both data and simulation and calibrating data.
"""

PMT_gain = 4e6 #Nominal gain is 7'000'000 at nominal A/lm (model: 9821B p1)
att_factor_A = 6.14102564 #used when attenuating the signal before the digitizer.
att_factor_cosmic_A = 13.09339408 # #attenuation factor used when taking cosmic muon data.
att_factor_B = 6.15343348 #used when attenuating the signal before the digitizer.
att_factor_cosmic_B = 13.09360731# #attenuation factor used when taking cosmic muon data.
q = 1.60217662E-19 #charge of a single electron
plot = False


##############################
# LOADING DATA
##############################
path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop
bg_runs =       list(range(1552,1562))
Cs137_runs =    [1549]
Na22_runs =     [1551]
Th232_runs =    list(range(1641,1651))
Co60_runs =     [1605]
AmBe_runs =     [1601,1602,1603]
PuBe_runs =     [1606,1607,1608]
# cosmic_runs =   list(range(1266,1296))

df_bg =         propd.load_parquet_merge(path, bg_runs,     keep_col=['qdc_lg_ch1'], full=False)
df_Cs137 =      propd.load_parquet_merge(path, Cs137_runs,  keep_col=['qdc_lg_ch1'], full=False)
df_Na22 =       propd.load_parquet_merge(path, Na22_runs,   keep_col=['qdc_lg_ch1'], full=False)
df_Th232 =      propd.load_parquet_merge(path, Th232_runs,  keep_col=['qdc_lg_ch1'], full=False)*1.087 #compensate the gain difference between runs
df_Co60 =       propd.load_parquet_merge(path, Co60_runs,   keep_col=['qdc_lg_ch1'], full=False)
df_PuBe =       propd.load_parquet_merge(path, PuBe_runs,   keep_col=['qdc_lg_ch1'], full=False)
df_AmBe =       propd.load_parquet_merge(path, AmBe_runs,   keep_col=['qdc_lg_ch1'], full=False)


#########################################
# CALIBRATING QDC FOR NE213A
#########################################
res_gauss, data_res_89, data_res_50 = NE213A_lg_cal(df_bg, df_Na22, df_Cs137, df_Co60, df_Th232, df_PuBe, df_AmBe, plot=False) #fit all compton edges for NE213A detector

###############################
# LOADING SIMULATION DATA
###############################
#Pencilbeam
path_sim = '/media/gheed/Seagate_Expansion_Drive1/data/simulation/'
names = ['evtNum', 
        'optPhotonSum', 
        'optPhotonSumQE', 
        'optPhotonSumCompton', 
        'optPhotonSumComptonQE', 
        'xLoc', 
        'yLoc', 
        'zLoc', 
        'CsMin', 
        'optPhotonParentID', 
        'optPhotonParentCreatorProcess', 
        'optPhotonParentStartingEnergy',
        'edep']

df_Cs137_662_sim_pen =      pd.read_csv(path_sim + "NE213A/Cs137/pencilbeam/CSV_optphoton_data_sum.csv", names=names)
df_Na22_sim_pen =           pd.read_csv(path_sim + "NE213A/Na22/pencilbeam/CSV_optphoton_data_sum.csv", names=names)
df_Na22_511_sim_pen =       pd.read_csv(path_sim + "NE213A/Na22_511/pencilbeam/CSV_optphoton_data_sum.csv", names=names)
df_Na22_1275_sim_pen =      pd.read_csv(path_sim + "NE213A/Na22_1275/pencilbeam/CSV_optphoton_data_sum.csv", names=names)
df_Co60_sim_pen =           pd.read_csv(path_sim + "NE213A/Co60/pencilbeam/CSV_optphoton_data_sum.csv", names=names)
df_Co60_1173_sim_pen =      pd.read_csv(path_sim + "NE213A/Co60_1173/pencilbeam/CSV_optphoton_data_sum.csv", names=names)
df_Co60_1332_sim_pen =      pd.read_csv(path_sim + "NE213A/Co60_1332/pencilbeam/CSV_optphoton_data_sum.csv", names=names)
df_2615_sim_pen =           pd.read_csv(path_sim + "NE213A/2_62MeV/pencilbeam/CSV_optphoton_data_sum.csv", names=names)
df_4439_sim_pen =           pd.read_csv(path_sim + "NE213A/4_44MeV/pencilbeam/CSV_optphoton_data_sum.csv", names=names)

#INCREASE STATISTICS
df_Cs137_662_sim_pen =  pd.concat([df_Cs137_662_sim_pen, pd.read_csv(path_sim + "NE213A/Cs137/pencilbeam/part2/CSV_optphoton_data_sum.csv", names=names)])
df_Cs137_662_sim_pen =  pd.concat([df_Cs137_662_sim_pen, pd.read_csv(path_sim + "NE213A/Cs137/pencilbeam/part3/CSV_optphoton_data_sum.csv", names=names)])
df_Cs137_662_sim_pen =  pd.concat([df_Cs137_662_sim_pen, pd.read_csv(path_sim + "NE213A/Cs137/pencilbeam/part4/CSV_optphoton_data_sum.csv", names=names)])

df_Na22_sim_pen =       pd.concat([df_Na22_sim_pen, pd.read_csv(path_sim + "NE213A/Na22/pencilbeam/part2/CSV_optphoton_data_sum.csv", names=names)])
df_Na22_sim_pen =       pd.concat([df_Na22_sim_pen, pd.read_csv(path_sim + "NE213A/Na22/pencilbeam/part3/CSV_optphoton_data_sum.csv", names=names)])
df_Na22_sim_pen =       pd.concat([df_Na22_sim_pen, pd.read_csv(path_sim + "NE213A/Na22/pencilbeam/part4/CSV_optphoton_data_sum.csv", names=names)])

df_Na22_511_sim_pen =   pd.concat([df_Na22_511_sim_pen, pd.read_csv(path_sim + "NE213A/Na22_511/pencilbeam/part2/CSV_optphoton_data_sum.csv", names=names)])
df_Na22_511_sim_pen =   pd.concat([df_Na22_511_sim_pen, pd.read_csv(path_sim + "NE213A/Na22_511/pencilbeam/part3/CSV_optphoton_data_sum.csv", names=names)])
df_Na22_511_sim_pen =   pd.concat([df_Na22_511_sim_pen, pd.read_csv(path_sim + "NE213A/Na22_511/pencilbeam/part4/CSV_optphoton_data_sum.csv", names=names)])
df_Na22_511_sim_pen =   pd.concat([df_Na22_511_sim_pen, pd.read_csv(path_sim + "NE213A/Na22_511/pencilbeam/part5/CSV_optphoton_data_sum.csv", names=names)])
df_Na22_511_sim_pen =   pd.concat([df_Na22_511_sim_pen, pd.read_csv(path_sim + "NE213A/Na22_511/pencilbeam/part6/CSV_optphoton_data_sum.csv", names=names)])
df_Na22_511_sim_pen =   pd.concat([df_Na22_511_sim_pen, pd.read_csv(path_sim + "NE213A/Na22_511/pencilbeam/part7/CSV_optphoton_data_sum.csv", names=names)])

df_Na22_1275_sim_pen =  pd.concat([df_Na22_1275_sim_pen, pd.read_csv(path_sim + 'NE213A/Na22_1275/pencilbeam/part2/CSV_optphoton_data_sum.csv', names=names)])
df_Na22_1275_sim_pen =  pd.concat([df_Na22_1275_sim_pen, pd.read_csv(path_sim + 'NE213A/Na22_1275/pencilbeam/part3/CSV_optphoton_data_sum.csv', names=names)])
df_Na22_1275_sim_pen =  pd.concat([df_Na22_1275_sim_pen, pd.read_csv(path_sim + 'NE213A/Na22_1275/pencilbeam/part4/CSV_optphoton_data_sum.csv', names=names)])
df_Na22_1275_sim_pen =  pd.concat([df_Na22_1275_sim_pen, pd.read_csv(path_sim + 'NE213A/Na22_1275/pencilbeam/part5/CSV_optphoton_data_sum.csv', names=names)])
df_Na22_1275_sim_pen =  pd.concat([df_Na22_1275_sim_pen, pd.read_csv(path_sim + 'NE213A/Na22_1275/pencilbeam/part6/CSV_optphoton_data_sum.csv', names=names)])
df_Na22_1275_sim_pen =  pd.concat([df_Na22_1275_sim_pen, pd.read_csv(path_sim + 'NE213A/Na22_1275/pencilbeam/part7/CSV_optphoton_data_sum.csv', names=names)])

df_Co60_sim_pen =       pd.concat([df_Co60_sim_pen, pd.read_csv(path_sim + "NE213A/Co60/pencilbeam/part2/CSV_optphoton_data_sum.csv", names=names)])
df_Co60_sim_pen =       pd.concat([df_Co60_sim_pen, pd.read_csv(path_sim + "NE213A/Co60/pencilbeam/part3/CSV_optphoton_data_sum.csv", names=names)])
df_Co60_sim_pen =       pd.concat([df_Co60_sim_pen, pd.read_csv(path_sim + "NE213A/Co60/pencilbeam/part4/CSV_optphoton_data_sum.csv", names=names)])

df_Co60_1173_sim_pen =  pd.concat([df_Co60_1173_sim_pen, pd.read_csv(path_sim + 'NE213A/Co60_1173/pencilbeam/part2/CSV_optphoton_data_sum.csv', names=names)])
df_Co60_1173_sim_pen =  pd.concat([df_Co60_1173_sim_pen, pd.read_csv(path_sim + 'NE213A/Co60_1173/pencilbeam/part3/CSV_optphoton_data_sum.csv', names=names)])
df_Co60_1173_sim_pen =  pd.concat([df_Co60_1173_sim_pen, pd.read_csv(path_sim + 'NE213A/Co60_1173/pencilbeam/part4/CSV_optphoton_data_sum.csv', names=names)])
df_Co60_1173_sim_pen =  pd.concat([df_Co60_1173_sim_pen, pd.read_csv(path_sim + 'NE213A/Co60_1173/pencilbeam/part5/CSV_optphoton_data_sum.csv', names=names)])
df_Co60_1173_sim_pen =  pd.concat([df_Co60_1173_sim_pen, pd.read_csv(path_sim + 'NE213A/Co60_1173/pencilbeam/part6/CSV_optphoton_data_sum.csv', names=names)])
df_Co60_1173_sim_pen =  pd.concat([df_Co60_1173_sim_pen, pd.read_csv(path_sim + 'NE213A/Co60_1173/pencilbeam/part7/CSV_optphoton_data_sum.csv', names=names)])

df_Co60_1332_sim_pen =  pd.concat([df_Co60_1332_sim_pen, pd.read_csv(path_sim + 'NE213A/Co60_1332/pencilbeam/part2/CSV_optphoton_data_sum.csv', names=names)])
df_Co60_1332_sim_pen =  pd.concat([df_Co60_1332_sim_pen, pd.read_csv(path_sim + 'NE213A/Co60_1332/pencilbeam/part3/CSV_optphoton_data_sum.csv', names=names)])
df_Co60_1332_sim_pen =  pd.concat([df_Co60_1332_sim_pen, pd.read_csv(path_sim + 'NE213A/Co60_1332/pencilbeam/part4/CSV_optphoton_data_sum.csv', names=names)])
# df_Co60_1332_sim_pen =  pd.concat([df_Co60_1332_sim_pen, pd.read_csv(path_sim + 'NE213A/Co60_1332/pencilbeam/part5/CSV_optphoton_data_sum.csv', names=names)])
# df_Co60_1332_sim_pen =  pd.concat([df_Co60_1332_sim_pen, pd.read_csv(path_sim + 'NE213A/Co60_1332/pencilbeam/part6/CSV_optphoton_data_sum.csv', names=names)])
# df_Co60_1332_sim_pen =  pd.concat([df_Co60_1332_sim_pen, pd.read_csv(path_sim + 'NE213A/Co60_1332/pencilbeam/part7/CSV_optphoton_data_sum.csv', names=names)])

df_2615_sim_pen =       pd.concat([df_2615_sim_pen, pd.read_csv(path_sim + 'NE213A/2_62MeV/pencilbeam/part2/CSV_optphoton_data_sum.csv', names=names)])
df_2615_sim_pen =       pd.concat([df_2615_sim_pen, pd.read_csv(path_sim + 'NE213A/2_62MeV/pencilbeam/part3/CSV_optphoton_data_sum.csv', names=names)])
df_2615_sim_pen =       pd.concat([df_2615_sim_pen, pd.read_csv(path_sim + 'NE213A/2_62MeV/pencilbeam/part4/CSV_optphoton_data_sum.csv', names=names)])
df_2615_sim_pen =       pd.concat([df_2615_sim_pen, pd.read_csv(path_sim + 'NE213A/2_62MeV/pencilbeam/part5/CSV_optphoton_data_sum.csv', names=names)])
df_2615_sim_pen =       pd.concat([df_2615_sim_pen, pd.read_csv(path_sim + 'NE213A/2_62MeV/pencilbeam/part6/CSV_optphoton_data_sum.csv', names=names)])
# df_2615_sim_pen =       pd.concat([df_2615_sim_pen, pd.read_csv(path_sim + 'NE213A/2_62MeV/pencilbeam/part7/CSV_optphoton_data_sum.csv', names=names)])
# df_2615_sim_pen =       pd.concat([df_2615_sim_pen, pd.read_csv(path_sim + 'NE213A/2_62MeV/pencilbeam/part8/CSV_optphoton_data_sum.csv', names=names)])

df_4439_sim_pen =       pd.concat([df_4439_sim_pen, pd.read_csv(path_sim + 'NE213A/4_44MeV/pencilbeam/part2/CSV_optphoton_data_sum.csv', names=names)])
df_4439_sim_pen =       pd.concat([df_4439_sim_pen, pd.read_csv(path_sim + 'NE213A/4_44MeV/pencilbeam/part3/CSV_optphoton_data_sum.csv', names=names)])
df_4439_sim_pen =       pd.concat([df_4439_sim_pen, pd.read_csv(path_sim + 'NE213A/4_44MeV/pencilbeam/part4/CSV_optphoton_data_sum.csv', names=names)])
df_4439_sim_pen =       pd.concat([df_4439_sim_pen, pd.read_csv(path_sim + 'NE213A/4_44MeV/pencilbeam/part5/CSV_optphoton_data_sum.csv', names=names)])
df_4439_sim_pen =       pd.concat([df_4439_sim_pen, pd.read_csv(path_sim + 'NE213A/4_44MeV/pencilbeam/part6/CSV_optphoton_data_sum.csv', names=names)])
# df_4439_sim_pen =       pd.concat([df_4439_sim_pen, pd.read_csv(path_sim + 'NE213A/4_44MeV/pencilbeam/part7/CSV_optphoton_data_sum.csv', names=names)])
# df_4439_sim_pen =       pd.concat([df_4439_sim_pen, pd.read_csv(path_sim + 'NE213A/4_44MeV/pencilbeam/part8/CSV_optphoton_data_sum.csv', names=names)])
# df_4439_sim_pen =       pd.concat([df_4439_sim_pen, pd.read_csv(path_sim + 'NE213A/4_44MeV/pencilbeam/part9/CSV_optphoton_data_sum.csv', names=names)])
# df_4439_sim_pen =       pd.concat([df_4439_sim_pen, pd.read_csv(path_sim + 'NE213A/4_44MeV/pencilbeam/part10/CSV_optphoton_data_sum.csv', names=names)])
# df_4439_sim_pen =       pd.concat([df_4439_sim_pen, pd.read_csv(path_sim + 'NE213A/4_44MeV/pencilbeam/part11/CSV_optphoton_data_sum.csv', names=names)])
# df_4439_sim_pen =       pd.concat([df_4439_sim_pen, pd.read_csv(path_sim + 'NE213A/4_44MeV/pencilbeam/part12/CSV_optphoton_data_sum.csv', names=names)])
# df_4439_sim_pen =       pd.concat([df_4439_sim_pen, pd.read_csv(path_sim + 'NE213A/4_44MeV/pencilbeam/part13/CSV_optphoton_data_sum.csv', names=names)])
# df_4439_sim_pen =       pd.concat([df_4439_sim_pen, pd.read_csv(path_sim + 'NE213A/4_44MeV/pencilbeam/part14/CSV_optphoton_data_sum.csv', names=names)])
# df_4439_sim_pen =       pd.concat([df_4439_sim_pen, pd.read_csv(path_sim + 'NE213A/4_44MeV/pencilbeam/part15/CSV_optphoton_data_sum.csv', names=names)])

#Isotropic
df_Cs137_662_sim_iso =      np.genfromtxt(path_sim + 'NE213A/Cs137/isotropic/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
df_Na22_sim_iso =           pd.read_csv(path_sim + "NE213A/Na22/isotropic/CSV_optphoton_data_sum.csv", names=names)
df_Na22_511_sim_iso =       np.genfromtxt(path_sim + 'NE213A/Na22_511/isotropic/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1] 
df_Na22_1275_sim_iso =      np.genfromtxt(path_sim + 'NE213A/Na22_1275/isotropic/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
df_Co60_sim_iso =           pd.read_csv(path_sim + "NE213A/Co60/isotropic/CSV_optphoton_data_sum.csv", names=names)
df_Co60_1173_sim_iso =      np.genfromtxt(path_sim + 'NE213A/Co60_1173/isotropic/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1] 
df_Co60_1332_sim_iso =      np.genfromtxt(path_sim + 'NE213A/Co60_1332/isotropic/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1] 
df_2615_sim_iso =           np.genfromtxt(path_sim + 'NE213A/2_62MeV/isotropic/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]     
df_4439_sim_iso =           np.genfromtxt(path_sim + 'NE213A/4_44MeV/isotropic/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]

#Increasing statistics
#append extra data
df_Cs137_662_sim_iso =         np.append(df_Cs137_662_sim_iso, np.genfromtxt(path_sim + 'NE213A/Cs137/isotropic/part2/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])
df_Cs137_662_sim_iso =         np.append(df_Cs137_662_sim_iso, np.genfromtxt(path_sim + 'NE213A/Cs137/isotropic/part3/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])

df_Na22_sim_iso =          pd.concat([df_Na22_sim_iso, pd.read_csv(path_sim + "NE213A/Na22/isotropic/part2/CSV_optphoton_data_sum.csv", names=names)])
df_Na22_sim_iso =          pd.concat([df_Na22_sim_iso, pd.read_csv(path_sim + "NE213A/Na22/isotropic/part3/CSV_optphoton_data_sum.csv", names=names)])

df_Na22_511_sim_iso =      np.append(df_Na22_511_sim_iso, np.genfromtxt(path_sim + 'NE213A/Na22_511/isotropic/part2/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])
df_Na22_511_sim_iso =      np.append(df_Na22_511_sim_iso, np.genfromtxt(path_sim + 'NE213A/Na22_511/isotropic/part3/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])

df_Na22_1275_sim_iso =     np.append(df_Na22_1275_sim_iso, np.genfromtxt(path_sim + 'NE213A/Na22_1275/isotropic/part2/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])
df_Na22_1275_sim_iso =     np.append(df_Na22_1275_sim_iso, np.genfromtxt(path_sim + 'NE213A/Na22_1275/isotropic/part3/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])

df_Co60_sim_iso =          pd.concat([df_Co60_sim_iso, pd.read_csv(path_sim + "NE213A/Co60/isotropic/part2/CSV_optphoton_data_sum.csv", names=names)])
df_Co60_sim_iso =          pd.concat([df_Co60_sim_iso, pd.read_csv(path_sim + "NE213A/Co60/isotropic/part3/CSV_optphoton_data_sum.csv", names=names)])

df_Co60_1173_sim_iso =     np.append(df_Co60_1173_sim_iso, np.genfromtxt(path_sim + 'NE213A/Co60_1173/isotropic/part2/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])
df_Co60_1173_sim_iso =     np.append(df_Co60_1173_sim_iso, np.genfromtxt(path_sim + 'NE213A/Co60_1173/isotropic/part3/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])

df_Co60_1332_sim_iso =     np.append(df_Co60_1332_sim_iso, np.genfromtxt(path_sim + 'NE213A/Co60_1332/isotropic/part2/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])
df_Co60_1332_sim_iso =     np.append(df_Co60_1332_sim_iso, np.genfromtxt(path_sim + 'NE213A/Co60_1332/isotropic/part3/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])

df_2615_sim_iso =         np.append(df_2615_sim_iso, np.genfromtxt(path_sim + 'NE213A/2_62MeV/isotropic/part2/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])
df_2615_sim_iso =         np.append(df_2615_sim_iso, np.genfromtxt(path_sim + 'NE213A/2_62MeV/isotropic/part3/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])

df_4439_sim_iso =          np.append(df_4439_sim_iso, np.genfromtxt(path_sim + 'NE213A/4_44MeV/isotropic/part2/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])
df_4439_sim_iso =          np.append(df_4439_sim_iso, np.genfromtxt(path_sim + 'NE213A/4_44MeV/isotropic/part3/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])


#randomize and rebin data to minimize binning effects when plotting:
#pencilbeam
y, x = np.histogram(df_Cs137_662_sim_pen.optPhotonSumComptonQE, bins=4096, range=[0, 4096])
df_Cs137_662_sim_pen.optPhotonSumComptonQE = prodata.getRandDist(x, y)

y, x = np.histogram(df_Na22_511_sim_pen.optPhotonSumComptonQE, bins=4096, range=[0, 4096])
df_Na22_511_sim_pen.optPhotonSumComptonQE = prodata.getRandDist(x, y)

y, x = np.histogram(df_Na22_1275_sim_pen.optPhotonSumComptonQE, bins=4096, range=[0, 4096])
df_Na22_1275_sim_pen.optPhotonSumComptonQE = prodata.getRandDist(x, y)

y, x = np.histogram(df_Co60_1173_sim_pen.optPhotonSumComptonQE, bins=4096, range=[0, 4096])
df_Co60_1173_sim_pen.optPhotonSumComptonQE = prodata.getRandDist(x, y)

y, x = np.histogram(df_Co60_1332_sim_pen.optPhotonSumComptonQE, bins=4096, range=[0, 4096])
df_Co60_1332_sim_pen.optPhotonSumComptonQE = prodata.getRandDist(x, y)

y, x = np.histogram(df_2615_sim_pen.optPhotonSumComptonQE, bins=4096, range=[0, 4096])
df_2615_sim_pen.optPhotonSumComptonQE = prodata.getRandDist(x, y)

y, x = np.histogram(df_4439_sim_pen.optPhotonSumComptonQE, bins=4096, range=[0, 4096])
df_4439_sim_pen.optPhotonSumComptonQE = prodata.getRandDist(x, y)


#isotropic
y, x = np.histogram(df_Cs137_662_sim_iso, bins=4096, range=[0, 4096])
df_Cs137_662_sim_iso = prodata.getRandDist(x, y)

y, x = np.histogram(df_Na22_sim_iso.optPhotonSumQE, bins=4096, range=[0, 4096])
df_Na22_sim_iso.optPhotonSumQE = prodata.getRandDist(x, y)

y, x = np.histogram(df_Na22_511_sim_iso, bins=4096, range=[0, 4096])
df_Na22_511_sim_iso = prodata.getRandDist(x, y)

y, x = np.histogram(df_Na22_1275_sim_iso, bins=4096, range=[0, 4096])
df_Na22_1275_sim_iso = prodata.getRandDist(x, y)

y, x = np.histogram(df_Co60_sim_iso.optPhotonSumQE, bins=4096, range=[0, 4096])
df_Co60_sim_iso.optPhotonSumQE = prodata.getRandDist(x, y)

y, x = np.histogram(df_Co60_1173_sim_iso, bins=4096, range=[0, 4096])
df_Co60_1173_sim_iso = prodata.getRandDist(x, y)

y, x = np.histogram(df_Co60_1332_sim_iso, bins=4096, range=[0, 4096])
df_Co60_1332_sim_iso = prodata.getRandDist(x, y)

y, x = np.histogram(df_2615_sim_iso, bins=4096, range=[0, 4096])
df_2615_sim_iso = prodata.getRandDist(x, y)

y, x = np.histogram(df_4439_sim_iso, bins=4096, range=[0, 4096])
df_4439_sim_iso = prodata.getRandDist(x, y)

#convert integers to floats
df_Cs137_662_sim_pen.optPhotonSumComptonQE =    pd.to_numeric(df_Cs137_662_sim_pen.optPhotonSumComptonQE, downcast='float')
df_Na22_sim_pen.optPhotonSumComptonQE =         pd.to_numeric(df_Na22_sim_pen.optPhotonSumComptonQE, downcast='float')
df_Na22_511_sim_pen.optPhotonSumComptonQE =     pd.to_numeric(df_Na22_511_sim_pen.optPhotonSumComptonQE, downcast='float')
df_Na22_1275_sim_pen.optPhotonSumComptonQE =    pd.to_numeric(df_Na22_1275_sim_pen.optPhotonSumComptonQE, downcast='float')
df_Co60_1173_sim_pen.optPhotonSumComptonQE =    pd.to_numeric(df_Co60_1173_sim_pen.optPhotonSumComptonQE, downcast='float')
df_Co60_1332_sim_pen.optPhotonSumComptonQE =    pd.to_numeric(df_Co60_1332_sim_pen.optPhotonSumComptonQE, downcast='float')
df_2615_sim_pen.optPhotonSumComptonQE =         pd.to_numeric(df_2615_sim_pen.optPhotonSumComptonQE, downcast='float')
df_4439_sim_pen.optPhotonSumComptonQE =         pd.to_numeric(df_4439_sim_pen.optPhotonSumComptonQE, downcast='float')

df_Cs137_662_sim_iso =                          pd.to_numeric(df_Cs137_662_sim_iso, downcast='float')
df_Na22_sim_iso.optPhotonSumQE =                pd.to_numeric(df_Na22_sim_iso.optPhotonSumQE, downcast='float')
df_Na22_511_sim_iso =                           pd.to_numeric(df_Na22_511_sim_iso, downcast='float')
df_Na22_1275_sim_iso =                          pd.to_numeric(df_Na22_1275_sim_iso, downcast='float')
df_Co60_sim_iso.optPhotonSumQE =                pd.to_numeric(df_Co60_sim_iso.optPhotonSumQE, downcast='float')
df_Co60_1173_sim_iso =                          pd.to_numeric(df_Co60_1173_sim_iso, downcast='float')
df_Co60_1332_sim_iso =                          pd.to_numeric(df_Co60_1332_sim_iso, downcast='float')
df_2615_sim_iso =                               pd.to_numeric(df_2615_sim_iso, downcast='float')
df_4439_sim_iso =                               pd.to_numeric(df_4439_sim_iso, downcast='float')

#Reset index of all Pandas data sets:

df_Na22_sim_pen =       df_Na22_sim_pen.reset_index()
df_Na22_511_sim_pen =   df_Na22_511_sim_pen.reset_index()
df_Na22_1275_sim_pen =  df_Na22_1275_sim_pen.reset_index()
df_Co60_sim_pen =       df_Co60_sim_pen.reset_index()
df_Co60_1173_sim_pen =  df_Co60_1173_sim_pen.reset_index()
df_Co60_1332_sim_pen =  df_Co60_1332_sim_pen.reset_index()
df_2615_sim_pen =       df_2615_sim_pen.reset_index()
df_4439_sim_pen =       df_4439_sim_pen.reset_index()

df_Na22_sim_iso =       df_Na22_sim_iso.reset_index()
df_Co60_sim_iso =       df_Co60_sim_iso.reset_index()

#####################################
# CALIBRATING SIMULATION DATA TO QDC
#####################################
PMTgainCorrection = { #at 3e6 gain
        'Na22_511':0.95363008,
        'Cs137_662':0.86544635,
        'Na22_1275':0.79188096,
        'Co60_1332':0.74645863,
        'Th232_2615':0.81615480,
        'AmBe_4439':0.78407684}
PMTgainCorrectionErr = {
        'Na22_511':0.00573855,
        'Cs137_662':0.00190360,
        'Na22_1275':0.00127372,
        'Co60_1332':0.00144837,
        'Th232_2615':0.00102519,
        'AmBe_4439':0.00163580}

simScaleCorrection = {
        'Na22_511':18.8204808,
        'Cs137_662':4.46633762,
        'Na22_1275':6.69543092,
        'Co60_1332':1.97048215,
        'Th232_2615':0.32669851,
        'AmBe_4439':0.74514977}

# ---------------------------------------

# PMTgainCorrection2 = { #WITHOUT using charge calibration
#         'Na22_511':75.2689668,
#         'Cs137_662':73.9620404,
#         'Na22_1275':75.0925670,
#         'Co60_1332':70.0773420,
#         'Th232_2615':78.8435965,
#         'AmBe_4439':76.1751882}
# PMTgainCorrection2Err = { #WITHOUT using charge calibration
#         'Na22_511':0.00923365,
#         'Cs137_662':0.09409946,
#         'Na22_1275':0.01504274,
#         'Co60_1332':0.06678167,
#         'Th232_2615':0.06221055,
#         'AmBe_4439':0.03781189}

# simScaleCorrection2 = { #WITHOUT using charge calibration
#         'Na22_511':91.9092806,
#         'Cs137_662':23.3558785,
#         'Na22_1275':35.7934896,
#         'Co60_1332':11.4349884,
#         'Th232_2615':20.9122755,
#         'AmBe_4439':4.60750943}

# # # # # # # # # # # # # # # # # # # # #

#calibrate simulated spectra to QDC channels
#pencilbeam
df_Cs137_662_sim_pen.optPhotonSumComptonQE =      prosim.chargeCalibration(df_Cs137_662_sim_pen.optPhotonSumComptonQE * q * PMT_gain)# * PMTgainCorrection['Cs137_662']
df_Na22_sim_pen.optPhotonSumComptonQE =           prosim.chargeCalibration(df_Na22_sim_pen.optPhotonSumComptonQE        * q * PMT_gain)# * PMTgainCorrection['Na22_1275']
df_Na22_511_sim_pen.optPhotonSumComptonQE =       prosim.chargeCalibration(df_Na22_511_sim_pen.optPhotonSumComptonQE    * q * PMT_gain)# * PMTgainCorrection['Na22_511']
df_Na22_1275_sim_pen.optPhotonSumComptonQE =      prosim.chargeCalibration(df_Na22_1275_sim_pen.optPhotonSumComptonQE   * q * PMT_gain)# * PMTgainCorrection['Na22_1275']
df_Co60_sim_pen.optPhotonSumComptonQE =           prosim.chargeCalibration(df_Co60_sim_pen.optPhotonSumComptonQE        * q * PMT_gain)# * PMTgainCorrection['Co60_1332']
df_Co60_1173_sim_pen.optPhotonSumComptonQE =      prosim.chargeCalibration(df_Co60_1173_sim_pen.optPhotonSumComptonQE   * q * PMT_gain)# * PMTgainCorrection['Co60_1332']
df_Co60_1332_sim_pen.optPhotonSumComptonQE =      prosim.chargeCalibration(df_Co60_1332_sim_pen.optPhotonSumComptonQE   * q * PMT_gain)# * PMTgainCorrection['Co60_1332']
df_2615_sim_pen.optPhotonSumComptonQE =           prosim.chargeCalibration(df_2615_sim_pen.optPhotonSumComptonQE        * q * PMT_gain)# * PMTgainCorrection['Th232_2615']
df_4439_sim_pen.optPhotonSumComptonQE =           prosim.chargeCalibration(df_4439_sim_pen.optPhotonSumComptonQE        * q * PMT_gain)# * PMTgainCorrection['AmBe_4439']

#isotropic
df_Cs137_662_sim_iso =              prosim.chargeCalibration(df_Cs137_662_sim_iso   * q * PMT_gain) #* PMTgainCorrection['Cs137_662']
df_Na22_sim_iso.optPhotonSumQE =    prosim.chargeCalibration(df_Na22_sim_iso.optPhotonSumQE        * q * PMT_gain) #* PMTgainCorrection['Na22_1275']
df_Na22_511_sim_iso =               prosim.chargeCalibration(df_Na22_511_sim_iso    * q * PMT_gain) #* PMTgainCorrection['Na22_511']
df_Na22_1275_sim_iso =              prosim.chargeCalibration(df_Na22_1275_sim_iso   * q * PMT_gain) #* PMTgainCorrection['Na22_1275']
df_Co60_sim_iso.optPhotonSumQE =    prosim.chargeCalibration(df_Co60_sim_iso.optPhotonSumQE        * q * PMT_gain) #* PMTgainCorrection['Co60_1332']
df_Co60_1173_sim_iso =              prosim.chargeCalibration(df_Co60_1173_sim_iso   * q * PMT_gain) #* PMTgainCorrection['Co60_1332']
df_Co60_1332_sim_iso =              prosim.chargeCalibration(df_Co60_1332_sim_iso   * q * PMT_gain) #* PMTgainCorrection['Co60_1332']
df_2615_sim_iso =                   prosim.chargeCalibration(df_2615_sim_iso        * q * PMT_gain) #* PMTgainCorrection['Th232_2615']
df_4439_sim_iso =                   prosim.chargeCalibration(df_4439_sim_iso        * q * PMT_gain) #* PMTgainCorrection['AmBe_4439']



#Apply smearing on isotropic data
for i in range(len(df_Na22_511_sim_iso)):
    df_Na22_511_sim_iso[i] = prosim.gaussSmear(df_Na22_511_sim_iso[i], 0.23) * PMTgainCorrection['Na22_511']
for i in range(len(df_Cs137_662_sim_iso)):
    df_Cs137_662_sim_iso[i] = prosim.gaussSmear(df_Cs137_662_sim_iso[i], 0.12) * PMTgainCorrection['Cs137_662']
for i in range(len(df_Na22_1275_sim_iso)):
    df_Na22_1275_sim_iso[i] = prosim.gaussSmear(df_Na22_1275_sim_iso[i], 0.09) * PMTgainCorrection['Na22_1275']
for i in range(len(df_Co60_1173_sim_iso)):
    df_Co60_1173_sim_iso[i] = prosim.gaussSmear(df_Co60_1173_sim_iso[i], 0.14) * PMTgainCorrection['Co60_1332']
for i in range(len(df_Co60_1332_sim_iso)):
    df_Co60_1332_sim_iso[i] = prosim.gaussSmear(df_Co60_1332_sim_iso[i], 0.14) * PMTgainCorrection['Co60_1332']
for i in range(len(df_2615_sim_iso)):
    df_2615_sim_iso[i] = prosim.gaussSmear(df_2615_sim_iso[i], 0.065) * PMTgainCorrection['Th232_2615']
for i in range(len(df_4439_sim_iso)):
    df_4439_sim_iso[i] = prosim.gaussSmear(df_4439_sim_iso[i], 0.12) * PMTgainCorrection['AmBe_4439']

#Apply same smearing on pencilbeam data
df_Na22_511_sim_pen.optPhotonSumComptonQE = df_Na22_511_sim_pen.optPhotonSumComptonQE.apply(lambda x: prosim.gaussSmear(x, 0.23)) 
df_Na22_511_sim_pen_OG = df_Na22_511_sim_pen.copy() #make copy before applying gain
df_Na22_511_sim_pen.optPhotonSumComptonQE = df_Na22_511_sim_pen.optPhotonSumComptonQE * PMTgainCorrection['Na22_511']

df_Cs137_662_sim_pen.optPhotonSumComptonQE = df_Cs137_662_sim_pen.optPhotonSumComptonQE.apply(lambda x: prosim.gaussSmear(x, 0.12)) 
df_Cs137_662_sim_pen_OG = df_Cs137_662_sim_pen.copy() #make copy before applying gain
df_Cs137_662_sim_pen.optPhotonSumComptonQE = df_Cs137_662_sim_pen.optPhotonSumComptonQE * PMTgainCorrection['Cs137_662']

df_Na22_1275_sim_pen.optPhotonSumComptonQE = df_Na22_1275_sim_pen.optPhotonSumComptonQE.apply(lambda x: prosim.gaussSmear(x, 0.09)) 
df_Na22_1275_sim_pen_OG = df_Na22_1275_sim_pen.copy() #make copy before applying gain
df_Na22_1275_sim_pen.optPhotonSumComptonQE = df_Na22_1275_sim_pen.optPhotonSumComptonQE * PMTgainCorrection['Na22_1275']

df_Co60_1173_sim_pen.optPhotonSumComptonQE = df_Co60_1173_sim_pen.optPhotonSumComptonQE.apply(lambda x: prosim.gaussSmear(x, 0.14)) 
df_Co60_1173_sim_pen_OG = df_Co60_1173_sim_pen.copy() #make copy before applying gain
df_Co60_1173_sim_pen.optPhotonSumComptonQE = df_Co60_1173_sim_pen.optPhotonSumComptonQE * PMTgainCorrection['Co60_1332']

df_Co60_1332_sim_pen.optPhotonSumComptonQE = df_Co60_1332_sim_pen.optPhotonSumComptonQE.apply(lambda x: prosim.gaussSmear(x, 0.14))
df_Co60_1332_sim_pen_OG = df_Co60_1332_sim_pen.copy() #make copy before applying gain
df_Co60_1332_sim_pen.optPhotonSumComptonQE = df_Co60_1332_sim_pen.optPhotonSumComptonQE * PMTgainCorrection['Co60_1332']

df_2615_sim_pen.optPhotonSumComptonQE = df_2615_sim_pen.optPhotonSumComptonQE.apply(lambda x: prosim.gaussSmear(x, 0.065))
df_2615_sim_pen_OG = df_2615_sim_pen.copy() #make copy before applying gain
df_2615_sim_pen.optPhotonSumComptonQE = df_2615_sim_pen.optPhotonSumComptonQE * PMTgainCorrection['Th232_2615']

df_4439_sim_pen.optPhotonSumComptonQE = df_4439_sim_pen.optPhotonSumComptonQE.apply(lambda x: prosim.gaussSmear(x, 0.12)) 
df_4439_sim_pen_OG = df_4439_sim_pen.copy() #make copy before applying gain
df_4439_sim_pen.optPhotonSumComptonQE = df_4439_sim_pen.optPhotonSumComptonQE * PMTgainCorrection['AmBe_4439']


##############################
# FITTING SIMULATED DATA
##############################
plot = False

# sim_res_Na22_511  =  simNE213A.Na22_511_LG( df_Na22_511_sim_pen.query('optPhotonSumComptonQE>0').optPhotonSumComptonQE,    plot=plot)
# sim_res_Cs137_662 = simNE213A.Cs137_662_LG( df_Cs137_662_sim_pen.query('optPhotonSumComptonQE>0').optPhotonSumComptonQE,   plot=plot)
# sim_res_Co60_1173 = simNE213A.Co60_1173_LG( df_Co60_1173_sim_pen.query('optPhotonSumComptonQE>2000').optPhotonSumComptonQE,  plot=plot)
# sim_res_Na22_1275 = simNE213A.Na22_1275_LG( df_Na22_1275_sim_pen.query('optPhotonSumComptonQE>2000').optPhotonSumComptonQE,  plot=plot)
# sim_res_Co60_1332 = simNE213A.Co60_1332_LG( df_Co60_1332_sim_pen.query('optPhotonSumComptonQE>2000').optPhotonSumComptonQE,  plot=plot)
# sim_res_PuBe_2615 = simNE213A.PuBe_2615_LG( df_2615_sim_pen.query('optPhotonSumComptonQE>3000').optPhotonSumComptonQE,       plot=plot)
# sim_res_PuBe_4439 = simNE213A.PuBe_4439_LG( df_4439_sim_pen.query('optPhotonSumComptonQE>4000').optPhotonSumComptonQE,       plot=plot)

start = 500
stop = 6000
counts, bin_edges = np.histogram(df_Na22_511_sim_pen.optPhotonSumComptonQE, bins = 30, range=[0, stop*1.2])
bin_centers = prodata.getBinCenters(bin_edges)
Ef = 0.510999
sim_res_Na22_511  =  prosim.calibrationFitBinned(counts, bin_centers, start, stop, Ef=0.510999, plot=plot, compton=True)

start = 1000
stop = 6500
counts, bin_edges = np.histogram(df_Cs137_662_sim_pen.optPhotonSumComptonQE, bins = 50, range=[0, stop*1.2])
bin_centers = prodata.getBinCenters(bin_edges)
Ef = 0.66166
sim_res_Cs137_662  =  prosim.calibrationFitBinned(counts, bin_centers, start, stop, Ef=Ef, plot=plot, compton=True)

start = 3000
stop = 15000
counts, bin_edges = np.histogram(df_Co60_1173_sim_pen.optPhotonSumComptonQE, bins = 50, range=[0, stop*1.2])
bin_centers = prodata.getBinCenters(bin_edges)
Ef = 1.173
sim_res_Co60_1173 = prosim.calibrationFitBinned(counts, bin_centers, start, stop, Ef=Ef, plot=plot, compton=True)

start = 3000
stop = 15000
counts, bin_edges = np.histogram(df_Na22_1275_sim_pen.optPhotonSumComptonQE, bins = 50, range=[0, stop*1.2])
bin_centers = prodata.getBinCenters(bin_edges)
Ef = 1.2745
sim_res_Na22_1275 = prosim.calibrationFitBinned(counts, bin_centers, start, stop, Ef=Ef, plot=plot, compton=True)

start = 3000
stop = 14000
counts, bin_edges = np.histogram(df_Co60_1332_sim_pen.optPhotonSumComptonQE, bins = 40, range=[0, stop*1.2])
bin_centers = prodata.getBinCenters(bin_edges)
Ef = 1.3325
sim_res_Co60_1332 = prosim.calibrationFitBinned(counts, bin_centers, start, stop, Ef=Ef, plot=plot, compton=True)

start = 23000#13000
stop = 35000
counts, bin_edges = np.histogram(df_2615_sim_pen.optPhotonSumComptonQE, bins = 64, range=[0, stop*1.2])
bin_centers = prodata.getBinCenters(bin_edges)
Ef = 2.614533
sim_res_PuBe_2615 = prosim.calibrationFitBinned(counts, bin_centers, start, stop, Ef=Ef, plot=plot, compton=True)

start = 34000#20000
stop = 60000
counts, bin_edges = np.histogram(df_4439_sim_pen.optPhotonSumComptonQE, bins = 50, range=[0, stop*1.2])
bin_centers = prodata.getBinCenters(bin_edges)
Ef = 4.439
sim_res_PuBe_4439 = prosim.calibrationFitBinned(counts, bin_centers, start, stop, Ef=Ef, plot=plot, compton=True)

#############################################################

E_data = np.array([ #expected maximum energy deposition of simulated data gammas.
    sim_res_Na22_511['recoil_max'], 
    sim_res_Cs137_662['recoil_max'], 
    sim_res_Na22_1275['recoil_max'], 
    sim_res_Co60_1332['recoil_max'], 
    sim_res_PuBe_2615['recoil_max'], 
    sim_res_PuBe_4439['recoil_max'],
    ])
QDC_data = np.array([ #mean value of simulation data fits
    sim_res_Na22_511['gauss_mean'], 
    sim_res_Cs137_662['gauss_mean'], 
    sim_res_Na22_1275['gauss_mean'], 
    sim_res_Co60_1332['gauss_mean'], 
    sim_res_PuBe_2615['gauss_mean'],
    sim_res_PuBe_4439['gauss_mean'],
    ])
QDC_err = np.array([ #mean error of simulation data fits
    sim_res_Na22_511['gauss_mean_error'], 
    sim_res_Cs137_662['gauss_mean_error'],  
    sim_res_Na22_1275['gauss_mean_error'], 
    sim_res_Co60_1332['gauss_mean_error'],  
    sim_res_PuBe_2615['gauss_mean_error'],
    sim_res_PuBe_4439['gauss_mean_error'],
    ])
fit_std = np.array([ #standard diviation of simulation data fits
    sim_res_Na22_511['gauss_std'], 
    sim_res_Cs137_662['gauss_std'], 
    sim_res_Na22_1275['gauss_std'], 
    sim_res_Co60_1332['gauss_std'],  
    sim_res_PuBe_2615['gauss_std'],
    sim_res_PuBe_4439['gauss_std'],
    ])

fit_std_err = np.array([ 
    sim_res_Na22_511['gauss_std_error'], 
    sim_res_Cs137_662['gauss_std_error'], 
    sim_res_Na22_1275['gauss_std_error'], 
    sim_res_Co60_1332['gauss_std_error'], 
    sim_res_PuBe_2615['gauss_std_error'],
    sim_res_PuBe_4439['gauss_std_error'],
    ])
chi2reduced = np.array([ 
    sim_res_Na22_511['chi2reduced'], 
    sim_res_Cs137_662['chi2reduced'], 
    sim_res_Na22_1275['chi2reduced'], 
    sim_res_Co60_1332['chi2reduced'], 
    sim_res_PuBe_2615['chi2reduced'],
    sim_res_PuBe_4439['chi2reduced'],
    ])

# sim_fit_res = prodata.linearEnergyFit(E_data, QDC_data, QDC_err, plot=plot) #used to produce a fit of QDC to MeVee


##################
### GAIN PLOT ####
##################
def p0(x, A):
    return A

#order: Na22 511, Cs137, Na22 1275, Co60 1332, Th, AmBe

Gain =                              np.array([list(PMTgainCorrection.values())[0], list(PMTgainCorrection.values())[1], list(PMTgainCorrection.values())[2], list(PMTgainCorrection.values())[3], list(PMTgainCorrection.values())[4], list(PMTgainCorrection.values())[5] ])*PMT_gain/1e6
PMTgainCorrectionErrParameter =     np.array([list(PMTgainCorrectionErr.values())[0], list(PMTgainCorrectionErr.values())[1], list(PMTgainCorrectionErr.values())[2], list(PMTgainCorrectionErr.values())[3], list(PMTgainCorrectionErr.values())[4], list(PMTgainCorrectionErr.values())[5]])*PMT_gain/1e6
gain_smearing_error =               np.array([0.001366120218579, 0.004213483146067, 0.004347826086957, 0.004249291784702, 0.001351351351351, 0.005602240896359])
gain_optiomization_region_error =   np.array([0.005464480874317, 0.002808988764045, 0.002898550724638, 0.004249291784703, 0.008108108108108, 0.011904761904762])
qdc_cal_error =                     np.array([0.05452994962068587, 0.05452994962068587, 0.05452994962068587, 0.05452994962068587, 0.05452994962068587, 0.05452994962068587]) #relative error in the linear calibration from Charge to QDC ~5.5%

PMTgainCorrectionErrNew = Gain * np.sqrt(   (PMTgainCorrectionErrParameter/Gain)**2 +   #error of the the gain paramter (very small).
                                            gain_smearing_error**2 +             #error from varying the phenomeloical smearing parameter.
                                            gain_optiomization_region_error**2 + #error from varying the optimization regions.
                                            qdc_cal_error**2)                           #error from the charge to QDC calibration function.

fig, ax1 = plt.subplots()
ax2 = ax1.twinx()

ax1.errorbar(np.array([ E_data[1], E_data[4], E_data[5] ]), 
            np.array([ Gain[1], Gain[4], Gain[5] ]),
            yerr = np.array([PMTgainCorrectionErrNew[1], PMTgainCorrectionErrNew[4], PMTgainCorrectionErrNew[5]]),
            ls='', marker='o', ms=7, color='blue')

ax1.errorbar(np.array([E_data[0], E_data[2], 1.04 ]), 
            np.array([ Gain[0], Gain[2], Gain[3] ]), 
            yerr = np.array([ PMTgainCorrectionErrNew[0], PMTgainCorrectionErrNew[2], PMTgainCorrectionErrNew[3] ]),   
            ls='', marker='o', ms=7, color='red', fillstyle='none')

ax1.errorbar(np.array([ 1.04 ]), 
            np.array([ Gain[3] ]), 
            xerr = np.array([ 0.08 ]),
            ls='', marker='o', ms=7, color='red', fillstyle='none')

# PMTgainCorrectionErrOld = np.array(list(PMTgainCorrectionErr.values()))*PMT_gain/1e6
ax2.scatter(E_data, 425*Gain, alpha=0.)

popt, pcov = curve_fit(p0, E_data, Gain, sigma=PMTgainCorrectionErrNew, absolute_sigma=True) #constant fit
pcov = np.diag(np.sqrt(pcov)) #calculate the error from the covariance matrixS
ax1.hlines(popt[0], xmin=0, xmax=5, lw=1, ls='solid', color='black')
ax1.fill_between(np.arange(0, 6), np.array([popt[0], popt[0], popt[0], popt[0], popt[0], popt[0]])+pcov[0], np.array([popt[0], popt[0], popt[0], popt[0],popt[0],popt[0]])-pcov[0], color='black', alpha=.1)

ax1.set_ylabel('Relative gain ($10^6$)')
ax2.set_ylabel('Scintillation light yield gradient [MeV$_{ee}^{-1}$]')
ax1.set_xlabel(r'$E_{\rm CE}$ [MeV$_{ee}]$')
ax1.set_ylim([2.7, 4.1])
ax2.set_ylim([425*2.7, 425*4.1])
# plt.legend()
plt.show()

meanGain = popt[0]
meanGainError = pcov[0]
print(f'meanGainError = {round(meanGainError/meanGain*100,4)} %')


########################
### RESOLUTION PLOT ####
########################

#order Na22 511, Cs137, Na22 1275, Co60 1332, Th, AmBe
QDC_err_new = np.array([0.00538, 0.005397, 0.003281, 0.005284, 0.004053, 0.004053 ]) #derived from systematic study of assumptions
fit_std_err_new = np.array([0.025438, 0.034875, 0.043910, 0.058467, 0.047457, 0.042787]) #derived from systematic study of assumptions
resErrNew = fit_std/QDC_data * np.sqrt((fit_std_err_new)**2 + (QDC_err_new)**2 + (PMTgainCorrectionErrNew/Gain)**2)*100 #adding all error together quadratically

res_err_close = np.array([ resErrNew[1], resErrNew[4], resErrNew[5]  ])
res_err_open = np.array([ resErrNew[0], resErrNew[2], resErrNew[3]  ])

plt.errorbar(np.array([E_data[1], E_data[4], E_data[5]]), 
                        (np.array([fit_std[1], fit_std[4], fit_std[5]])/np.array([QDC_data[1], QDC_data[4], QDC_data[5]]))*100, 
                        yerr=res_err_close, 
                        ls='', marker='o', ms=7, color='blue', label='Single-energy sources')

plt.errorbar(np.array([E_data[0],E_data[2], 1.04]), 
                        (np.array([fit_std[0], fit_std[2], fit_std[3]])/np.array([QDC_data[0], QDC_data[2], QDC_data[3]]))*100, 
                        yerr=res_err_open, 
                        ls='', marker='o', ms=7, fillstyle='none', color='red', label='Double-energy sources')

plt.errorbar(np.array([ 1.04 ]), #make horizontal error bar for Co60 double-lines
            (np.array([ fit_std[3]])/np.array([QDC_data[3] ]))*100, 
            xerr = np.array([ 0.08 ]),
            ls='', marker='o', ms=7, color='red', fillstyle='none')

#Fitting with 1/E
# popt1, pcov1 = curve_fit(promath.oneOverE, E_data, fit_std/QDC_data*100, sigma=resErrNew, absolute_sigma=True)
# pcov1 = np.diag(np.sqrt(pcov1))
# x_val = np.arange(0, 5, 0.1)
# plt.plot(x_val, promath.oneOverE(x_val, popt1[0], popt1[1]), lw=2, color='black', linestyle='solid', label='y = A/E + B')
# plt.fill_between(x_val, promath.oneOverE(x_val, popt1[0]+pcov1[0], popt1[1]+pcov[1]), promath.oneOverE(x_val, popt1[0]-pcov1[0], popt1[1]-pcov1[1]), color='black', alpha=.2)

#Fitting with 1/sqrt(E)
popt2, pcov2 = curve_fit(promath.oneOverSqrtE, E_data, fit_std/QDC_data*100, sigma=resErrNew, absolute_sigma=True)
pcov2 = np.diag(np.sqrt(pcov2))
x_val = np.arange(0, 5, 0.1)
plt.plot(x_val, promath.oneOverSqrtE(x_val, popt2[0], popt2[1]), lw=2, color='black', linestyle='solid', label='y = A/$\sqrt{E}$ + B')
plt.fill_between(x_val, promath.oneOverSqrtE(x_val, popt2[0]+pcov2[0], popt2[1]+pcov2[1]), promath.oneOverSqrtE(x_val, popt2[0]-pcov2[0], popt2[1]-pcov2[1]), color='black', alpha=.1)

# chi2oneOverE = promath.chi2red(fit_std/QDC_data*100, 
#                           promath.oneOverE(E_data, popt1[0], popt1[1]),
#                           resErrNew,                                
#                           2)
# print('---- y = A/E + B ----')
# print(f'X2/ndf = {round(chi2oneOverE,2)}')
# print(f'A = {round(popt1[0], 2)} +/- {round(pcov1[0],2)} ({round(pcov1[0]/popt1[0]*100,2)}%)')
# print(f'B = {round(popt1[1], 2)} +/- {round(pcov1[1],2)} ({round(pcov1[1]/popt1[1]*100,2)}%)')

chi2oneOverSqrtE = promath.chi2red(fit_std/QDC_data*100, 
                          promath.oneOverSqrtE(E_data, popt2[0], popt2[1]),
                          resErrNew,                                
                          2)
print('---- y = A/sqrt(E) + B ----')
print(f'X2/ndf = {round(chi2oneOverSqrtE,2)}')
print(f'A = {round(popt2[0], 2)} +/- {round(pcov2[0],2)} ({round(pcov2[0]/popt2[0]*100,2)}%)')
print(f'B = {round(popt2[1], 2)} +/- {round(pcov2[1],2)} ({round(pcov2[1]/popt2[1]*100,2)}%)')

plt.xlabel(r'$E_{\rm CE}$ [MeV$_{ee}]$')
plt.ylabel('Resolution, $\sigma$ [%]')
# plt.legend()
plt.ylim([7,40])
plt.xlim([0,5])
plt.show()

# def smearFunc(E):
#     """
#     returns smearing values 0-1 for given energy in MeV.
#     """
#     popt, pcov = curve_fit(promath.oneOverE, E_data, fit_std/QDC_data)
#     pcov = np.diag(np.sqrt(pcov))
#     return promath.oneOverE(E, popt[0], popt[1])





##################################################################
# FIGURE showing agreement between isotropic simulation and data #
##################################################################
plt.subplot(3,1,1)
numBins = 150
binRange = [0, 25000]

w = np.empty(len(df_bg))
w.fill(1/10)
ybg, xbg = np.histogram(df_bg.qdc_lg_ch1*att_factor_A, numBins, binRange, weights=w)

w = np.empty(len(df_Cs137))
w.fill(1/1)
counts, bins = np.histogram(df_Cs137.qdc_lg_ch1*att_factor_A, numBins, binRange, weights=w)

plt.errorbar(bins[:-1], counts-ybg, yerr=np.sqrt(counts-ybg), ls='', marker='o', ms=5, label='Data', zorder=2)

y, x = np.histogram(df_Cs137_662_sim_iso, numBins, binRange)
x = prodata.getBinCenters(x)

plt.step(x , y*simScaleCorrection['Cs137_662'], lw=2,  color='orange', label='Simulation', zorder=1)
# plt.fill_between(x, y*simScaleCorrection['Cs137_662'], step='pre', alpha=0.25, color='orange')

plt.vlines([3500, 6500], ymin=0, ymax=47000, color='black', alpha=1, lw=2, zorder=3)
# plt.fill_betweenx(np.arange(0, 47000), x1=3500, x2=6500, color='blue', alpha=.05)  

plt.ylabel('counts')
plt.xlim([3000, 8000])
plt.ylim([0, 47000])
plt.xlabel('QDC [Arb. units]')
plt.legend()

plt.subplot(3,1,2)
numBins = 75
binRange = [0, 35000]

w = np.empty(len(df_bg))
w.fill(1/10)
ybg, xbg = np.histogram(df_bg.qdc_lg_ch1*att_factor_A, numBins, binRange, weights=w)

w = np.empty(len(df_Th232))
w.fill(1/10)
counts, bins = np.histogram(df_Th232.qdc_lg_ch1*att_factor_A, numBins, binRange, weights=w)

plt.errorbar(bins[:-1], (counts)-ybg, yerr=np.sqrt((counts)-ybg), ls='', marker='o', ms=5, label='Data', zorder=2)

y, x = np.histogram(df_2615_sim_iso, numBins, binRange)
x = prodata.getBinCenters(x)
plt.step(x , y*simScaleCorrection['Th232_2615'], lw=2,  color='orange', label='Simulation', zorder=1)
# plt.fill_between(x, y*simScaleCorrection['Th232_2615'], step='pre', alpha=0.25, color='orange')

plt.vlines([23000, 30000], ymin=0, ymax=1800, color='black',  alpha=1, lw=2, zorder=3)
# plt.fill_betweenx(np.arange(0, 1800), x1=23000, x2=30000, color='blue', alpha=.05)  

plt.ylabel('counts')
plt.xlim([21880, 33500])
plt.ylim([0, 1800])
plt.xlabel('QDC [Arb. units]')
plt.ylabel('counts')
plt.legend()

plt.subplot(3,1,3)
numBins = 30
binRange = [24500, 61000]

w = np.empty(len(df_bg))
w.fill(1/10)
ybg, xbg = np.histogram(df_bg.qdc_lg_ch1*att_factor_A, numBins, binRange, weights=w)

w = np.empty(len(df_AmBe))
w.fill(1/3)
counts, bins = np.histogram(df_AmBe.qdc_lg_ch1*att_factor_A, numBins, binRange, weights=w)
plt.errorbar(bins[:-1], counts-ybg, yerr=np.sqrt(counts-ybg), ls='', marker='o', ms=5, label='Data', zorder=2)

y, x = np.histogram(df_4439_sim_iso, numBins, binRange)
x = prodata.getBinCenters(x)
plt.step(x , y*simScaleCorrection['AmBe_4439'], lw=2,  color='orange', label='Simulation', zorder=1)
# plt.fill_between(x, y*simScaleCorrection['AmBe_4439'], step='pre', alpha=0.25, color='orange')

plt.vlines([37000, 53000], ymin=0, ymax=5500, color='black', alpha=1, lw=2, zorder=3)
# plt.fill_betweenx(np.arange(0, 5500), x1=37000, x2=53000, color='blue', alpha=.05)  


plt.xlim([34400, 60900])
plt.ylim([0, 5500])
plt.xlabel('QDC [Arb. units]')
plt.ylabel('counts')
plt.legend()
plt.show()

####################################################
### FIGURE Showing the location of all methods #####
####################################################
plt.subplot(3,1,1)
numBins = 135
binRange = [0, 25000]

w = np.empty(len(df_bg))
w.fill(1/10)
ybg, xbg = np.histogram(df_bg.qdc_lg_ch1*att_factor_A, numBins, binRange, weights=w)

w = np.empty(len(df_Cs137))
w.fill(1/1)
counts, bins = np.histogram(df_Cs137.qdc_lg_ch1*att_factor_A, numBins, binRange, weights=w)
bins = prodata.getBinCenters(bins)
plt.errorbar(bins, counts-ybg, yerr=np.sqrt(counts-ybg), ls='', marker='o', ms=5, label='Data', zorder=2)

xrange = np.arange(3500, 8100)
plt.plot(   xrange, 
            promath.gaussFunc(xrange, res_gauss['662_gauss_const']*9.2 ,res_gauss['662_gauss_mean'], res_gauss['662_gauss_std']),
            lw=1, 
            color='C0')

y, x = np.histogram(df_Cs137_662_sim_pen.optPhotonSumComptonQE, numBins, binRange)
x = prodata.getBinCenters(x)
plt.step(x , y*18*0.9, lw=2,  color='green', label='Simulation with cut', where='mid', zorder=1)
plt.fill_between(x, y*18*0.9, step='mid', alpha=0.25, color='green')

plt.vlines(res_gauss['662_CE_89_x'], 0, 70000, ls='-.', color='black')
plt.vlines(res_gauss['662_CE_50_x'], 0, 70000, ls=':', color='black')
plt.vlines(sim_res_Cs137_662['gauss_mean'], 0, 70000, ls='solid', lw=2, color='black')
plt.vlines(np.average(df_Cs137_662_sim_pen.query(f'optPhotonSumComptonQE>{sim_res_Cs137_662["gauss_mean"]-3*sim_res_Cs137_662["gauss_std"]}').optPhotonSumComptonQE), 0, 70000, color='black', lw=1, alpha=.5)

plt.plot(np.arange(0, 250000), promath.gaussFunc(np.arange(0, 250000),
                                                sim_res_Cs137_662['gauss_const']*21*0.9, 
                                                sim_res_Cs137_662['gauss_mean'], 
                                                sim_res_Cs137_662['gauss_std']), 
                                                lw=2, 
                                                color='black',
                                                linestyle='dashed')
# plt.xlim([1260, 8000])
plt.xlim([3100, 8000])
plt.ylim([0, 50000])
plt.xlabel('QDC [Arb. units]')
plt.ylabel('counts')
plt.legend()


plt.subplot(3,1,2)
numBins = 75
binRange = [0, 35000]

w = np.empty(len(df_bg))
w.fill(1/10)
ybg, xbg = np.histogram(df_bg.qdc_lg_ch1*att_factor_A, numBins, binRange, weights=w)

w = np.empty(len(df_Th232))
w.fill(1/10)
counts, bins = np.histogram(df_Th232.qdc_lg_ch1*att_factor_A, numBins, binRange, weights=w)
bins = prodata.getBinCenters(bins)
plt.errorbar(bins, (counts)-ybg, yerr=np.sqrt((counts)-ybg), ls='', marker='o', ms=5, label='Data', zorder=2)

xrange = np.arange(23000, 35000)
plt.plot(   xrange, 
            promath.gaussFunc(xrange, res_gauss['2615_gauss_const']*4.6, res_gauss['2615_gauss_mean'], res_gauss['2615_gauss_std']),
            lw=1, 
            color='C0')

y, x = np.histogram(df_2615_sim_pen.optPhotonSumComptonQE, numBins, binRange)
x = prodata.getBinCenters(x)
plt.step(x , y*2.5, lw=2,  color='green', label='Simulation with cut', where='mid', zorder=1)
plt.fill_between(x, y*2.5, step='mid', alpha=0.25, color='green')

plt.plot(np.arange(0, 250000), promath.gaussFunc(np.arange(0, 250000),
                                                sim_res_PuBe_2615['gauss_const']*1.9, 
                                                sim_res_PuBe_2615['gauss_mean'], 
                                                sim_res_PuBe_2615['gauss_std']), 
                                                lw=2, 
                                                color='black',
                                                linestyle='dashed')

plt.vlines(res_gauss['2615_CE_89_x'], 0, 27000, ls='-.', color='black')
plt.vlines(res_gauss['2615_CE_50_x'], 0, 27000, ls=':', color='black')
plt.vlines(sim_res_PuBe_2615['gauss_mean'], 0, 27000, ls='-', lw=2, color='black')
plt.vlines(np.average(df_2615_sim_pen.query(f'optPhotonSumComptonQE>{sim_res_PuBe_2615["gauss_mean"]-3*sim_res_PuBe_2615["gauss_std"]}').optPhotonSumComptonQE), 0, 27000, ls='-', lw=1, color='black', alpha=.5)

# plt.xlim([17500, 33500])
plt.xlim([20500, 33500])
plt.ylim([0, 1600])
plt.xlabel('QDC [Arb. units]')
plt.ylabel('counts')
plt.legend()


plt.subplot(3,1,3)
numBins = 50
binRange = [0, 61000]

w = np.empty(len(df_bg))
w.fill(1/10)
ybg, xbg = np.histogram(df_bg.qdc_lg_ch1*att_factor_A, numBins, binRange, weights=w)

w = np.empty(len(df_AmBe))
w.fill(1/3)
counts, bins = np.histogram(df_AmBe.qdc_lg_ch1*att_factor_A, numBins, binRange, weights=w)
bins = prodata.getBinCenters(bins)
plt.errorbar(bins, counts-ybg, yerr=np.sqrt(counts-ybg), ls='', marker='o', ms=5, label='Data', zorder=2)

xrange = np.arange(37500, 65000)
plt.plot(   xrange, 
            promath.gaussFunc(xrange, res_gauss['AmBe_4439_gauss_const']*6, res_gauss['AmBe_4439_gauss_mean'], res_gauss['AmBe_4439_gauss_std']),
            lw=1, 
            color='C0')

y, x = np.histogram(df_4439_sim_pen.optPhotonSumComptonQE, numBins, binRange)
x = prodata.getBinCenters(x)
plt.step(x , y*9, lw=2,  color='green', label='Simulation with cut', where='mid', zorder=1)
plt.fill_between(x, y*9, step='mid', alpha=0.25, color='green')

plt.plot(np.arange(0, 60000), promath.gaussFunc(np.arange(0, 60000),
                                                sim_res_PuBe_4439['gauss_const']*8, 
                                                sim_res_PuBe_4439['gauss_mean'], 
                                                sim_res_PuBe_4439['gauss_std']), 
                                                lw=2, 
                                                color='black',
                                                linestyle='dashed')

plt.vlines(res_gauss['AmBe_4439_CE_89_x'], 0, 27000, ls='-.', color='black')
plt.vlines(res_gauss['AmBe_4439_CE_50_x'], 0, 27000, ls=':', color='black')
plt.vlines(sim_res_PuBe_4439['gauss_mean'], 0, 27000, ls='-', lw=2, color='black')
plt.vlines(np.average(df_4439_sim_pen.query(f'optPhotonSumComptonQE>{sim_res_PuBe_4439["gauss_mean"]-3*sim_res_PuBe_4439["gauss_std"]}').optPhotonSumComptonQE), 0, 27000, ls='-', lw=1, color='black', alpha=.5)

# plt.xlim([22000, 60900])
plt.xlim([32400, 60900])
plt.ylim([0, 4400])
plt.xlabel('QDC [Arb. units]')
plt.ylabel('counts')
plt.legend()
plt.show()



#################################################
##### CALIBRATION PLOT OF ALL THREE METHODS #####
#################################################

#Apply average gain on all data
df_Na22_511_sim_pen_OG.optPhotonSumComptonQE = df_Na22_511_sim_pen_OG.optPhotonSumComptonQE * meanGain/4
df_Cs137_662_sim_pen_OG.optPhotonSumComptonQE = df_Cs137_662_sim_pen_OG.optPhotonSumComptonQE * meanGain/4
df_Co60_1173_sim_pen_OG.optPhotonSumComptonQE = df_Co60_1173_sim_pen_OG.optPhotonSumComptonQE * meanGain/4
df_Na22_1275_sim_pen_OG.optPhotonSumComptonQE = df_Na22_1275_sim_pen_OG.optPhotonSumComptonQE * meanGain/4
df_Co60_1332_sim_pen_OG.optPhotonSumComptonQE = df_Co60_1332_sim_pen_OG.optPhotonSumComptonQE * meanGain/4
df_2615_sim_pen_OG.optPhotonSumComptonQE = df_2615_sim_pen_OG.optPhotonSumComptonQE * meanGain/4
df_4439_sim_pen_OG.optPhotonSumComptonQE = df_4439_sim_pen_OG.optPhotonSumComptonQE * meanGain/4

#Fit the positions of average gain pencilbeam data sets
start = 100
stop = 6000
counts, bin_edges = np.histogram(df_Na22_511_sim_pen_OG.optPhotonSumComptonQE, bins = 30, range=[0, stop*1.2])
bin_centers = prodata.getBinCenters(bin_edges)
Ef = 0.510999
sim_res_Na22_511_avg  =  prosim.calibrationFitBinned(counts, bin_centers, start, stop, Ef=0.510999, plot=plot, compton=True)

start = 1000
stop = 6500
counts, bin_edges = np.histogram(df_Cs137_662_sim_pen_OG.optPhotonSumComptonQE, bins = 50, range=[0, stop*1.2])
bin_centers = prodata.getBinCenters(bin_edges)
Ef = 0.66166
sim_res_Cs137_662_avg  =  prosim.calibrationFitBinned(counts, bin_centers, start, stop, Ef=Ef, plot=plot, compton=True)

start = 3000
stop = 14000
counts, bin_edges = np.histogram(df_Co60_1173_sim_pen_OG.optPhotonSumComptonQE, bins = 50, range=[0, stop*1.2])
bin_centers = prodata.getBinCenters(bin_edges)
Ef = 1.173
sim_res_Co60_1173_avg = prosim.calibrationFitBinned(counts, bin_centers, start, stop, Ef=Ef, plot=plot, compton=True)

start = 2000
stop = 13000
counts, bin_edges = np.histogram(df_Na22_1275_sim_pen_OG.optPhotonSumComptonQE, bins = 50, range=[0, stop*1.2])
bin_centers = prodata.getBinCenters(bin_edges)
Ef = 1.2745
sim_res_Na22_1275_avg = prosim.calibrationFitBinned(counts, bin_centers, start, stop, Ef=Ef, plot=plot, compton=True)

start = 2500
stop = 14000
counts, bin_edges = np.histogram(df_Co60_1332_sim_pen_OG.optPhotonSumComptonQE, bins = 40, range=[0, stop*1.2])
bin_centers = prodata.getBinCenters(bin_edges)
Ef = 1.3325
sim_res_Co60_1332_avg = prosim.calibrationFitBinned(counts, bin_centers, start, stop, Ef=Ef, plot=plot, compton=True)

start = 13000
stop = 32000
counts, bin_edges = np.histogram(df_2615_sim_pen_OG.optPhotonSumComptonQE, bins = 64, range=[0, stop*1.2])
bin_centers = prodata.getBinCenters(bin_edges)
Ef = 2.614533
sim_res_PuBe_2615_avg = prosim.calibrationFitBinned(counts, bin_centers, start, stop, Ef=Ef, plot=plot, compton=True)

start = 20000
stop = 57500
counts, bin_edges = np.histogram(df_4439_sim_pen_OG.optPhotonSumComptonQE, bins = 50, range=[0, stop*1.2])
bin_centers = prodata.getBinCenters(bin_edges)
Ef = 4.439
sim_res_PuBe_4439_avg = prosim.calibrationFitBinned(counts, bin_centers, start, stop, Ef=Ef, plot=plot, compton=True)

weightedMean = np.array([ #mean value of simulation data fits
    np.average(df_Na22_511_sim_pen_OG.query("optPhotonSumComptonQE>0").optPhotonSumComptonQE),
    np.average(df_Cs137_662_sim_pen_OG.query(f'optPhotonSumComptonQE>{sim_res_Cs137_662_avg["gauss_mean"]-3*sim_res_Cs137_662_avg["gauss_std"]}').optPhotonSumComptonQE),
    np.average(df_Co60_1173_sim_pen_OG.query(f'optPhotonSumComptonQE>{sim_res_Co60_1173_avg["gauss_mean"]-3*sim_res_Co60_1173_avg["gauss_std"]}').optPhotonSumComptonQE),
    np.average(df_Na22_1275_sim_pen_OG.query(f'optPhotonSumComptonQE>{sim_res_Na22_1275_avg["gauss_mean"]-3*sim_res_Na22_1275_avg["gauss_std"]}').optPhotonSumComptonQE),
    np.average(df_Co60_1332_sim_pen_OG.query(f'optPhotonSumComptonQE>{sim_res_Co60_1332_avg["gauss_mean"]-3*sim_res_Co60_1332_avg["gauss_std"]}').optPhotonSumComptonQE),
    np.average(df_2615_sim_pen_OG.query(f'optPhotonSumComptonQE>{sim_res_PuBe_2615_avg["gauss_mean"]-3*sim_res_PuBe_2615_avg["gauss_std"]}').optPhotonSumComptonQE),
    np.average(df_4439_sim_pen_OG.query(f'optPhotonSumComptonQE>{sim_res_PuBe_4439_avg["gauss_mean"]-3*sim_res_PuBe_4439_avg["gauss_std"]}').optPhotonSumComptonQE)])

fittedMean = np.array([ #mean value of simulation data fits
    sim_res_Na22_511_avg['gauss_mean'], 
    sim_res_Cs137_662_avg['gauss_mean'], 
    sim_res_Co60_1173_avg['gauss_mean'], 
    sim_res_Na22_1275_avg['gauss_mean'], 
    sim_res_Co60_1332_avg['gauss_mean'],  
    sim_res_PuBe_2615_avg['gauss_mean'],
    sim_res_PuBe_4439_avg['gauss_mean']
    ])

E_data2 = np.array([ #ordered by acending energy
    sim_res_Na22_511_avg['recoil_max'], 
    sim_res_Cs137_662_avg['recoil_max'], 
    sim_res_Co60_1173_avg['recoil_max'], 
    sim_res_Na22_1275_avg['recoil_max'], 
    sim_res_Co60_1332_avg['recoil_max'], 
    sim_res_PuBe_2615_avg['recoil_max'], 
    sim_res_PuBe_4439_avg['recoil_max']
    ])

fit_std2 = np.array([ #standard diviation of simulation data fits
    sim_res_Na22_511_avg['gauss_std'], 
    sim_res_Cs137_662_avg['gauss_std'], 
    sim_res_Na22_1275_avg['gauss_std'], 
    sim_res_PuBe_2615_avg['gauss_std'],
    sim_res_PuBe_4439_avg['gauss_std'],
    ])

QDC_data2 = np.array([ #mean value of simulation data fits
    sim_res_Na22_511_avg['gauss_mean'], 
    sim_res_Cs137_662_avg['gauss_mean'], 
    sim_res_Na22_1275_avg['gauss_mean'], 
    sim_res_PuBe_2615_avg['gauss_mean'],
    sim_res_PuBe_4439_avg['gauss_mean'],
    ])

# gauss_std = np.array([res_gauss['511_gauss_std'], 
#     res_gauss['662_gauss_std'], 
#     res_gauss['1275_gauss_std'], 
#     res_gauss['2615_gauss_std'],
#     res_gauss['AmBe_4439_gauss_std']])

#ERROR PROPAGATION incl Co60, both lines
QDC_err_new = np.array([0.00538, 0.005397, 0.007508, 0.003281, 0.005284, 0.004053, 0.004053 ])
qdc_cal_error = np.array([0.05452994962068587, 0.05452994962068587, 0.05452994962068587, 0.05452994962068587, 0.05452994962068587, 0.05452994962068587, 0.05452994962068587]) #relative error in the linear calibration from Charge to QDC ~5.5%
gain_error = np.array([0.005632658, 0.005063977, 0.006009406, 0.005225437, 0.006009406, 0.008219949, 0.013157069])
PMTgainCorrectionErrParameter = np.array([list(PMTgainCorrectionErr.values())[0], list(PMTgainCorrectionErr.values())[1], list(PMTgainCorrectionErr.values())[3], list(PMTgainCorrectionErr.values())[2], list(PMTgainCorrectionErr.values())[3], list(PMTgainCorrectionErr.values())[4], list(PMTgainCorrectionErr.values())[5]])*PMT_gain/1e6
Gain = np.array([list(PMTgainCorrection.values())[0], list(PMTgainCorrection.values())[1], list(PMTgainCorrection.values())[3], list(PMTgainCorrection.values())[2], list(PMTgainCorrection.values())[3], list(PMTgainCorrection.values())[4], list(PMTgainCorrection.values())[5] ])*PMT_gain/1e6


fittedMeanError = fittedMean * np.sqrt(QDC_err_new**2 + 
                                        qdc_cal_error**2 +
                                        gain_error**2 +
                                        (PMTgainCorrectionErrParameter/Gain)**2)

weightedMeanError = weightedMean * np.sqrt(QDC_err_new**2 + 
                                        qdc_cal_error**2 +
                                        gain_error**2 +
                                        (PMTgainCorrectionErrParameter/Gain)**2)

###ADD ZERO TO DATA FOR FITTING####
# weightedMean = np.append(0,  weightedMean)
# weightedMeanError = np.append(1e-6,  weightedMeanError)
# fittedMean = np.append(0, fittedMean)
# fittedMeanError = np.append(1e-6, fittedMeanError)
# E_data2 = np.append(0, E_data2)

plt.figure()
xrange = np.arange(-1, 25, .01)
plt.subplot(3,1,1)

#fitting weighted mean
poptWM, pcovWM = curve_fit(promath.linearFunc, E_data2, weightedMean, sigma=weightedMeanError, absolute_sigma=True)
pcovWM = np.diag(np.sqrt(pcovWM))
poptWMSQ, pcovWMSQ = curve_fit(promath.quadraticFunc, E_data2, weightedMean, sigma=weightedMeanError, absolute_sigma=True)
pcovWMSQ = np.diag(np.sqrt(pcovWMSQ))

plt.plot(xrange, promath.linearFunc(xrange, poptWM[0], poptWM[1]), color='black', linestyle='solid', zorder=1)
plt.errorbar(E_data2+0.01, weightedMean, yerr=weightedMeanError, ls='', marker='o', ms=7, color='black', label='Average value', zorder=2, lw=2)

#fitting fitted mean
# poptFM, pcovFM = curve_fit(promath.linearFunc, E_data2, fittedMean, sigma=fittedMeanError, absolute_sigma=True)
# pcovFM = np.diag(np.sqrt(pcovFM))
# poptFMSQ, pcovFMSQ = curve_fit(promath.quadraticFunc, E_data2, fittedMean, sigma=fittedMeanError, absolute_sigma=True)
# pcovFMSQ = np.diag(np.sqrt(pcovFMSQ))

# plt.plot(xrange, promath.linearFunc(xrange, poptFM[0], poptFM[1]), color='black', linestyle='--', zorder=1, lw=2)
# plt.errorbar(E_data2-0.01, fittedMean, yerr=fittedMeanError, ls='', marker='o', mfc='none', ms=7, color='black', label='Fitted mean', zorder=2, lw=2)

plt.xlim([-.25, 5])
# plt.xlim([-.25, .75])
plt.ylim([-3500, 56000])
# plt.ylim([-2000, 7000])
plt.legend()
plt.ylabel('QDC [Arb. units]')

#KNOX AND MILLER
#oneOverE parameter error: 2.76242906e-01, 1.07222186e-04
# QDC_resolution_error = promath.oneOverE(data_res_89['QDC_data_89'], 7.67366706e+00, 6.09284577e-04)*data_res_89['QDC_data_89']
gauss_par_error = np.array([
                            np.sqrt(np.sum((np.array([res_gauss['511_gauss_const_error'],res_gauss['511_gauss_mean_error'],res_gauss['511_gauss_std_error']])/np.array([res_gauss['511_gauss_const'],res_gauss['511_gauss_mean'],res_gauss['511_gauss_std']]))**2)),
                            np.sqrt(np.sum((np.array([res_gauss['662_gauss_const_error'],res_gauss['662_gauss_mean_error'],res_gauss['662_gauss_std_error']])/np.array([res_gauss['662_gauss_const'],res_gauss['662_gauss_mean'],res_gauss['662_gauss_std']]))**2)),
                            np.sqrt(np.sum((np.array([res_gauss['1275_gauss_const_error'],res_gauss['1275_gauss_mean_error'],res_gauss['1275_gauss_std_error']])/np.array([res_gauss['1275_gauss_const'],res_gauss['1275_gauss_mean'],res_gauss['1275_gauss_std']]))**2)),
                            np.sqrt(np.sum((np.array([res_gauss['2615_gauss_const_error'],res_gauss['2615_gauss_mean_error'],res_gauss['2615_gauss_std_error']])/np.array([res_gauss['2615_gauss_const'],res_gauss['2615_gauss_mean'],res_gauss['2615_gauss_std']]))**2)),
                            np.sqrt(np.sum((np.array([res_gauss['AmBe_4439_gauss_const_error'],res_gauss['AmBe_4439_gauss_mean_error'],res_gauss['AmBe_4439_gauss_std_error']])/np.array([res_gauss['AmBe_4439_gauss_const'],res_gauss['AmBe_4439_gauss_mean'],res_gauss['AmBe_4439_gauss_std']]))**2)) 
                            ])

knox_error = data_res_89['QDC_data_89'] * np.sqrt( np.array([0.012116167221606, 0.006781822397403, 0.005567546355342, 0.004573721163491, 0.014912888655211])**2 + 
                                                    # np.array([.1812,.1812,.1812,.1812,.1812])**2 + #parameter error from fitting 1/E to sigma_val/QDC_val for charge calibration data
                                                    # promath.expFunc(data_res_89['QDC_data_89']/1000, 0.0011169 ,  0.01188372, -0.49372587)**2,
                                                    # (fit_std2/QDC_data2)**2 + #simulated detector resolution
                                                    # np.delete(resErrNew/100, 3)**2 + #error in simulated resolution
                                                    np.delete(qdc_cal_error,[0,1])**2,
                                                    gauss_par_error**2)

# knox_error = np.append(1e-6, knox_error)

plt.subplot(3,1,2)
plt.errorbar(data_res_89['E_data'],  data_res_89['QDC_data_89'], yerr=knox_error, ls='', mfc='none', marker='s', ms=7, color='C0', lw=2)

#fitting Knox and Miller
poptKnox, pcovKnox = curve_fit(promath.linearFunc, data_res_89['E_data'], data_res_89['QDC_data_89'], sigma=knox_error, absolute_sigma=True)
pcovKnox = np.diag(np.sqrt(pcovKnox))

poptKnoxSQ, pcovKnoxSQ = curve_fit(promath.quadraticFunc, data_res_89['E_data'], data_res_89['QDC_data_89'], sigma=knox_error, absolute_sigma=True)
pcovKnoxSQ = np.diag(np.sqrt(pcovKnoxSQ))

plt.plot(xrange, promath.linearFunc(xrange, poptKnox[0], poptKnox[1]), color='C0', label='Knox and Miller', linestyle='-.', zorder=1, lw=2)
plt.plot(xrange, promath.linearFunc(xrange, poptWM[0], poptWM[1]), color='black', linestyle='solid', zorder=1)
plt.xlim([-.25, 5])
# plt.xlim([-.25, .75])
plt.ylim([-3500, 56000])
# plt.ylim([-2000, 7000])
# plt.legend()
plt.ylabel('QDC [Arb. units]')

#FLYNN et al
flynn_error = data_res_50['QDC_data_50'] * np.sqrt(np.array([0.004149659863946,0.002585216392187,0.002704353136177,0.001711761457758,0.004718389456399])**2 + 
                                                    # np.array([.1812,.1812,.1812,.1812,.1812])**2 + #parameter error from fitting 1/E to sigma_val/QDC_val for charge calibration data
                                                    # promath.expFunc(data_res_50['QDC_data_50']/1000, 0.0011169 ,  0.01188372, -0.49372587)**2
                                                    # (fit_std2/QDC_data2)**2 + #simulated resolution of detector
                                                    # np.delete(resErrN ew/100, 3)**2 + #error in simulated resolution
                                                    np.delete(qdc_cal_error,[0,1])**2,
                                                    gauss_par_error**2)

# flynn_error = np.append(1e-6, flynn_error)

plt.subplot(3,1,3)
plt.errorbar(data_res_50['E_data'], data_res_50['QDC_data_50'], yerr=flynn_error, ls='', mfc='none', marker='v', ms=7, color='red', lw=2)

#fitting Flynn et al 
poptFlynn, pcovFlynn = curve_fit(promath.linearFunc, data_res_50['E_data'], data_res_50['QDC_data_50'], sigma=flynn_error, absolute_sigma=True)
pcovFlynn = np.diag(np.sqrt(pcovFlynn))

poptFlynnSQ, pcovFlynnSQ = curve_fit(promath.quadraticFunc, data_res_50['E_data'], data_res_50['QDC_data_50'], sigma=flynn_error, absolute_sigma=True)
pcovFlynnSQ = np.diag(np.sqrt(pcovFlynnSQ))

plt.plot(xrange, promath.linearFunc(xrange, poptFlynn[0], poptFlynn[1]), color='red', label='Flynn et al', linestyle='dotted', zorder=1, lw=2)
plt.plot(xrange, promath.linearFunc(xrange, poptWM[0], poptWM[1]), color='black', linestyle='solid', zorder=1)
plt.xlim([-.25, 5])
# plt.xlim([-.25, .75])
plt.ylim([-3500, 56000])
# plt.ylim([-2000, 7000])

plt.ylabel('QDC [Arb. units]')
plt.xlabel(r'$E_{\rm CE}$ [MeV$_{ee}$]')
plt.show()


chi2Knox89 = promath.chi2red(   data_res_89['QDC_data_89'], 
                                promath.linearFunc(data_res_89['E_data'],  poptKnox[0], poptKnox[1]),
                                knox_error,
                                2)
chi2Flynn50 = promath.chi2red(  data_res_50['QDC_data_50'], 
                                promath.linearFunc(data_res_50['E_data'], poptFlynn[0], poptFlynn[1]),
                                flynn_error,
                                2)
# chi2myMethodFM = promath.chi2red(  fittedMean, 
#                                 promath.linearFunc(E_data2, poptFM[0], poptFM[1]),
#                                 fittedMeanError,                                
#                                 2)
chi2myMethodWM = promath.chi2red(  weightedMean, 
                                promath.linearFunc(E_data2, poptWM[0], poptWM[1]),
                                weightedMeanError,
                                2)

print('-------------------------')
print(f'My work: Average mean:') 
k = np.round(poptWM[0],1)
kerr = np.round(pcovWM[0],1)
m = np.round(poptWM[1],1)
merr = np.round(pcovWM[1],1)
print(f'k = {k} +/- {kerr}') 
print(f'm = {m} +/- {merr}') 
print(f'redChi2 = {np.round(chi2myMethodWM, 2)}')
print('-------------------------')

# print(f'My work: Fitted mean:') 
# k = np.round(poptFM[0],1)
# kerr = np.round(pcovFM[0],1)
# m = np.round(poptFM[1],1)
# merr = np.round(pcovFM[1],1)
# print(f'k = {k} +/- {kerr}') 
# print(f'm = {m} +/- {merr}') 
# print(f'redChi2 = {np.round(chi2myMethodFM, 2)}')
# print('-------------------------')

print(f'Knox & Miller (89%):') 
k = np.round(poptKnox[0], 1)
kerr = np.round(pcovKnox[0], 1)
m = np.round(poptKnox[1], 1)
merr = np.round(pcovKnox[1], 1)
print(f'k = {k} +/- {kerr}') 
print(f'm = {m} +/- {merr}') 
print(f'redChi2 = {np.round(chi2Knox89, 2)}')
print('-------------------------')

print(f'Flynn et al (50%):') 
k = np.round(poptFlynn[0], 1)
kerr = np.round(pcovFlynn[0], 1)
m = np.round(poptFlynn[1], 1)
merr = np.round(pcovFlynn[1], 1)
print(f'k = {k} +/- {kerr}') 
print(f'm = {m} +/- {merr}') 
print(f'redChi2 = {np.round(chi2Flynn50, 2)}')
print('-------------------------')


def QDCtoScint(QDC_value):
    """
    takes QDC value and returns number of scint photons
    """
    return QDC_value*6.35e-15/(1.602176634e-19*meanGain*1e6)


# chi2Knox89SQ = promath.chi2red(   data_res_89['QDC_data_89'], 
#                                 promath.quadraticFunc(data_res_89['E_data'],  poptKnoxSQ[0], poptKnoxSQ[1], poptKnoxSQ[2]),
#                                 knox_error,
#                                 3)
# chi2Flynn50SQ = promath.chi2red(  data_res_50['QDC_data_50'], 
#                                 promath.quadraticFunc(data_res_50['E_data'], poptFlynnSQ[0], poptFlynnSQ[1], poptFlynnSQ[2]),
#                                 flynn_error,
#                                 3)
# chi2myMethodFMSQ = promath.chi2red(  fittedMean, 
#                                 promath.quadraticFunc(E_data2, poptFMSQ[0], poptFMSQ[1], poptFMSQ[2]),
#                                 fittedMeanError,                                
#                                 3)
# chi2myMethodWMSQ = promath.chi2red(  weightedMean, 
#                                 promath.quadraticFunc(E_data2, poptWMSQ[0], poptWMSQ[1], poptWMSQ[2]),
#                                 weightedMeanError,
#                                 3)

# print('-------------------------')
# print(f'My work: Average value:') 
# A = np.round(poptWMSQ[0],1)
# Aerr = np.round(pcovWMSQ[0],1)
# B = np.round(poptWMSQ[1],1)
# Berr = np.round(pcovWMSQ[1],1)
# C = np.round(poptWMSQ[2],1)
# Cerr = np.round(pcovWMSQ[2],1)
# print(f'A = {A} +/- {Aerr}') 
# print(f'B = {B} +/- {Berr}') 
# print(f'C = {C} +/- {Cerr}') 
# print(f'redChi2 = {np.round(chi2myMethodWMSQ, 2)}')


# print('-------------------------')
# print(f'My work: Fitted mean:') 
# A = np.round(poptFMSQ[0],1)
# Aerr = np.round(pcovFMSQ[0],1)
# B = np.round(poptFMSQ[1],1)
# Berr = np.round(pcovFMSQ[1],1)
# C = np.round(poptFMSQ[2],1)
# Cerr = np.round(pcovFMSQ[2],1)
# print(f'A = {A} +/- {Aerr}') 
# print(f'B = {B} +/- {Berr}') 
# print(f'C = {C} +/- {Cerr}') 
# print(f'redChi2 = {np.round(chi2myMethodFMSQ, 2)}')
# print('-------------------------')



# print(f'Knox & Miller (89%):') 
# A = np.round(poptKnoxSQ[0], 1)
# Aerr = np.round(pcovKnoxSQ[0], 1)
# B = np.round(poptKnoxSQ[1], 1)
# Berr = np.round(pcovKnoxSQ[1], 1)
# C = np.round(poptKnoxSQ[2], 1)
# Cerr = np.round(pcovKnoxSQ[2], 1)
# print(f'A = {A} +/- {Aerr}') 
# print(f'B = {B} +/- {Berr}') 
# print(f'C = {C} +/- {Cerr}') 
# print(f'redChi2 = {np.round(chi2Knox89SQ, 2)}')
# print('-------------------------')

# print(f'Flynn et al (50%):') 
# A = np.round(poptFlynnSQ[0], 1)
# Aerr = np.round(pcovFlynnSQ[0], 1)
# B = np.round(poptFlynnSQ[1], 1)
# Berr = np.round(pcovFlynnSQ[1], 1)
# C = np.round(poptFlynnSQ[2], 1)
# Cerr = np.round(pcovFlynnSQ[2], 1)
# print(f'A = {A} +/- {Aerr}') 
# print(f'B = {B} +/- {Berr}') 
# print(f'C = {C} +/- {Cerr}') 
# print(f'redChi2 = {np.round(chi2Flynn50SQ, 2)}')
# print('-------------------------')

#####################################################################


################################
# Double energy sources solved #
################################
#Figure 7
path_sim = '/media/gheed/Seagate_Expansion_Drive1/data/simulation/'
names = ['evtNum', 
        'optPhotonSum', 
        'optPhotonSumQE', 
        'optPhotonSumCompton', 
        'optPhotonSumComptonQE', 
        'xLoc', 
        'yLoc', 
        'zLoc', 
        'CsMin', 
        'optPhotonParentID', 
        'optPhotonParentCreatorProcess', 
        'optPhotonParentStartingEnergy',
        'edep']

df_Na22_sim_iso =       pd.read_csv(path_sim + "NE213A/Na22/isotropic/CSV_optphoton_data_sum.csv", names=names)
df_Na22_sim_iso =       pd.concat([df_Na22_sim_iso, pd.read_csv(path_sim + "NE213A/Na22/isotropic/part2/CSV_optphoton_data_sum.csv", names=names)])
df_Na22_sim_iso =       pd.concat([df_Na22_sim_iso, pd.read_csv(path_sim + "NE213A/Na22/isotropic/part3/CSV_optphoton_data_sum.csv", names=names)])

y, x = np.histogram(df_Na22_sim_iso.optPhotonSumQE, bins=4096, range=[0, 4096])
df_Na22_sim_iso.optPhotonSumQE =    prodata.getRandDist(x, y)
df_Na22_sim_iso.optPhotonSumQE =    pd.to_numeric(df_Na22_sim_iso.optPhotonSumQE, downcast='float')
df_Na22_sim_iso =                   df_Na22_sim_iso.reset_index()
df_Na22_sim_iso.optPhotonSumQE =    prosim.chargeCalibration(df_Na22_sim_iso.optPhotonSumQE * q * PMT_gain)

df_Na22_sim_iso.optPhotonSumQE = df_Na22_sim_iso.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, 0.09)) 


df_Co60_sim_iso =       pd.read_csv(path_sim + "NE213A/Co60/isotropic/CSV_optphoton_data_sum.csv", names=names)
df_Co60_sim_iso =       pd.concat([df_Co60_sim_iso, pd.read_csv(path_sim + "NE213A/Co60/isotropic/part2/CSV_optphoton_data_sum.csv", names=names)])
df_Co60_sim_iso =       pd.concat([df_Co60_sim_iso, pd.read_csv(path_sim + "NE213A/Co60/isotropic/part3/CSV_optphoton_data_sum.csv", names=names)])

y, x = np.histogram(df_Co60_sim_iso.optPhotonSumQE, bins=4096, range=[0, 4096])
df_Co60_sim_iso.optPhotonSumQE =    prodata.getRandDist(x, y)
df_Co60_sim_iso.optPhotonSumQE =    pd.to_numeric(df_Co60_sim_iso.optPhotonSumQE, downcast='float')
df_Co60_sim_iso =                   df_Co60_sim_iso.reset_index()
df_Co60_sim_iso.optPhotonSumQE =    prosim.chargeCalibration(df_Co60_sim_iso.optPhotonSumQE * q * PMT_gain)

df_Co60_sim_iso.optPhotonSumQE = df_Co60_sim_iso.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, 0.095)) 




plt.figure()
plt.subplot(2,1,1)
numBins = 60
binRange = [0, 20000]
w = np.empty(len(df_bg))
w.fill(1/10)
ybg, xbg = np.histogram(df_bg.qdc_lg_ch1*att_factor_A, numBins, binRange, weights=w)

scaleParameter = np.ones(len(ybg))
scaleParameter[20:-1] = scaleParameter[20:-1]*5

w = np.empty(len(df_Na22))
w.fill(1/1)
counts, bins = np.histogram(df_Na22.qdc_lg_ch1*att_factor_A, numBins, binRange, weights=w)
plt.errorbar(bins[:-1], (counts-ybg)*scaleParameter, yerr=np.sqrt(counts-ybg), ls='', marker='o', ms=5, label='Data', zorder=4)

y, x = np.histogram(df_Na22_sim_iso.optPhotonSumQE*.89, numBins, [-13500, 6500])
x = prodata.getBinCenters(x)
plt.step(x , y*25, lw=2, color='orange', label='Simulation', zorder=3)

y, x = np.histogram(df_Na22_sim_iso.optPhotonSumQE*.79, numBins, [6500, 26500])
x = prodata.getBinCenters(x)
plt.step(x , y*107, lw=2, color='orange', zorder=3)

y, x = np.histogram(df_Na22_511_sim_pen.query('optPhotonSumComptonQE>0').optPhotonSumComptonQE, numBins, binRange)
x = prodata.getBinCenters(x)
plt.step(x , y*30, lw=2,  color='red', where='mid', label='Simulation with cut', zorder=2)
plt.fill_between(x, y*30, step='mid', alpha=.4, color='red', zorder=2)

y, x = np.histogram(df_Na22_1275_sim_pen.query(f'optPhotonSumComptonQE>{sim_res_Na22_1275["gauss_mean"]-3*sim_res_Na22_1275["gauss_std"]}').optPhotonSumComptonQE, numBins, range=[6500, 26500])
x = prodata.getBinCenters(x)
plt.step(x , y*150, lw=2,  color='darkred', where='mid', zorder=1)
plt.fill_between(x, y*150, step='mid', alpha=.7, color='darkred', zorder=1)

plt.plot(np.arange(0, 6000), promath.gaussFunc(np.arange(0, 6000), 
                                                sim_res_Na22_511['gauss_const']*43, 
                                                sim_res_Na22_511['gauss_mean'], 
                                                sim_res_Na22_511['gauss_std']),
                                                color='black',
                                                linestyle='dashed',
                                                lw=2,
                                                zorder = 3)

plt.plot(np.arange(6000, 20000), promath.gaussFunc(np.arange(6000, 20000), 
                                                sim_res_Na22_1275['gauss_const']*144, 
                                                sim_res_Na22_1275['gauss_mean'], 
                                                sim_res_Na22_1275['gauss_std']),
                                                color='black',
                                                linestyle='dashed',
                                                lw=2,
                                                zorder = 3)

plt.vlines(sim_res_Na22_511['gauss_mean'], 0, 400000, ls='-', lw=2, color='black')

# plt.hist(df_Na22_511_sim_pen.query(f'optPhotonSumComptonQE>{sim_res_Na22_511["gauss_mean"]-2*sim_res_Na22_511["gauss_std"]} and optPhotonSumComptonQE<{sim_res_Na22_511["gauss_mean"]+2*sim_res_Na22_511["gauss_std"]}').optPhotonSumComptonQE, bins=200)
plt.vlines(np.mean(df_Na22_511_sim_pen.query(f'optPhotonSumComptonQE>{sim_res_Na22_511["gauss_mean"]-2*sim_res_Na22_511["gauss_std"]} and optPhotonSumComptonQE<{sim_res_Na22_511["gauss_mean"]+2*sim_res_Na22_511["gauss_std"]}').optPhotonSumComptonQE), 0, 1000, ls='-', lw=1, color='black', alpha=.5)
plt.vlines(sim_res_Na22_1275['gauss_mean'], 0, 400000, ls='-', lw=2, color='black')
plt.vlines(np.average(df_Na22_1275_sim_pen.query(f'optPhotonSumComptonQE>{sim_res_Na22_1275["gauss_mean"]-3*sim_res_Na22_1275["gauss_std"]}').optPhotonSumComptonQE), 0, 400000, ls='-', lw=1, color='black', alpha=.5)


plt.xlabel('QDC [arb. units]')
plt.ylabel('counts')
plt.legend()
# plt.yscale('log')
plt.xlim([2250, 12500])
plt.ylim([0, 400000])
# plt.show()


plt.subplot(2,1,2)

numBins = 80
binRange = [0, 20000]
w = np.empty(len(df_bg))
w.fill(1/10)
ybg, xbg = np.histogram(df_bg.qdc_lg_ch1*att_factor_A, numBins, binRange, weights=w)

w = np.empty(len(df_Co60))
w.fill(1/1)
counts, bins = np.histogram(df_Co60.qdc_lg_ch1*att_factor_A, numBins, binRange, weights=w)
plt.errorbar(bins[:-1], counts-ybg, yerr=np.sqrt(counts-ybg), ls='', marker='o', ms=5, label='Data', zorder=4)

y, x = np.histogram(df_Co60_sim_iso.optPhotonSumQE*0.805, numBins, binRange)
x = prodata.getBinCenters(x)
plt.step(x , y*1.93, lw=2, color='orange', label='Simulation', zorder=3)


y, x = np.histogram(df_Co60_1173_sim_pen.query(f'optPhotonSumComptonQE>{sim_res_Co60_1173["gauss_mean"]-3*sim_res_Co60_1173["gauss_std"]}').optPhotonSumComptonQE, numBins, binRange)
x = prodata.getBinCenters(x)
plt.step(x , y*8, lw=2,  color='magenta', label='Simulation with cut', where='mid', zorder=2)
plt.fill_between(x, y*8, step='mid', alpha=.5, color='magenta', zorder=2)

y, x = np.histogram(df_Co60_1332_sim_pen.query(f'optPhotonSumComptonQE>{sim_res_Co60_1332["gauss_mean"]-3*sim_res_Co60_1332["gauss_std"]}').optPhotonSumComptonQE,  numBins, binRange)
x = prodata.getBinCenters(x)
plt.step(x , y*14.8, lw=2,  color='darkmagenta', where='mid', zorder=3)
plt.fill_between(x, y*14.8, step='mid', alpha=.7, color='darkmagenta', zorder=3)

plt.plot(np.arange(2000, 20000), promath.gaussFunc(np.arange(2000, 20000), 
                                                sim_res_Co60_1173['gauss_const']*5.5, 
                                                sim_res_Co60_1173['gauss_mean'], 
                                                sim_res_Co60_1173['gauss_std']),
                                                color='black',
                                                linestyle='dashed',
                                                lw=2,
                                                zorder = 3)

plt.plot(np.arange(2000, 20000), promath.gaussFunc(np.arange(2000, 20000), 
                                                sim_res_Co60_1332['gauss_const']*9, 
                                                sim_res_Co60_1332['gauss_mean'], 
                                                sim_res_Co60_1332['gauss_std']),
                                                color='black',
                                                linestyle='dashed',
                                                lw=2,
                                                zorder = 3)



plt.vlines(sim_res_Co60_1173['gauss_mean'], 0, 27000, ls='-', lw=2, color='black')
plt.vlines(np.average(df_Co60_1173_sim_pen.query(f'optPhotonSumComptonQE>{sim_res_Co60_1173["gauss_mean"]-3*sim_res_Co60_1173["gauss_std"]}').optPhotonSumComptonQE), 0, 27000, ls='-', lw=1, color='black', alpha=.5)
plt.vlines(sim_res_Co60_1332['gauss_mean'], 0, 27000, ls='-', lw=2, color='black')
plt.vlines(np.average(df_Co60_1332_sim_pen.query(f'optPhotonSumComptonQE>{sim_res_Co60_1332["gauss_mean"]-3*sim_res_Co60_1332["gauss_std"]}').optPhotonSumComptonQE), 0, 27000, ls='-', lw=1, color='black', alpha=.5)


plt.xlabel('QDC [arb. units]')
plt.ylabel('counts')
plt.legend()
plt.xlim([7000, 13000])
plt.ylim([0, 10000])

plt.legend()
plt.show()



#####################################################################


#####################################
#Figure showing digitized waveforms
#####################################

df_waveforms = propd.load_parquet(path+'run01606/', num_events=1, keep_col=['samples_ch1', 'baseline_ch1', 'CFD_rise_ch1', 'amplitude_ch1','CFD_drop_ch1', 'qdc_lg_ch1']) 

df_waveforms = df_waveforms.reset_index()


# df_waveforms = propd.load_parquet(path+'run01549/', num_events=1, keep_col=['amplitude_ch1']) 

# df_waveforms = df_waveforms.reset_index()

evtn = 12 #event number

plt.plot(np.arange(0, 1001, 1), (df_waveforms.samples_ch1[evtn]-df_waveforms.baseline_ch1[evtn])*att_factor_A, lw=2)
plt.hlines(-25, xmin=0, xmax=1, alpha=1)
plt.hlines(0, xmin=0, xmax=1, alpha=1)
plt.vlines(df_waveforms.CFD_rise_ch1[evtn], ymin=-500, ymax=50, alpha=1, color='teal')

plt.vlines(df_waveforms.CFD_rise_ch1[evtn]-25, ymin=-500, ymax=50, alpha=1, color='black')
plt.vlines(df_waveforms.CFD_rise_ch1[evtn]-25+500, ymin=-500, ymax=50, alpha=1, color='black')

plt.ylabel('Amplitude [mV]')
plt.xlabel('Time [ns]')
plt.xlim([0.16, .72])
plt.ylim([-600, 50])
plt.show()


#######################################################
############ TESTING TAIL-TO-TOTAL RATIO ##############
#######################################################
#Na22 511
start = 0
stop = 8000
numBins = 250
counts, bin_edges = np.histogram(df_Na22_511_sim_pen.optPhotonSumComptonQE, bins = numBins, range=[0, stop*1.2])
bin_centers = prodata.getBinCenters(bin_edges)
Ef = 0.510999
fit  =  prosim.calibrationFitBinned(counts, bin_centers, start, stop, Ef=0.510999, plot=False, compton=True)
## rebin 
counts, bin_edges = np.histogram(df_Na22_511_sim_pen.query(f'optPhotonSumComptonQE>{fit["gauss_mean"]-3*fit["gauss_std"]} and optPhotonSumComptonQE < {fit["gauss_mean"]+3*fit["gauss_std"]}').optPhotonSumComptonQE, 
        bins = numBins, 
        range=[0, stop*1.2])
bin_centers = prodata.getBinCenters(bin_edges)
Na22_511_tail_ratio = np.trapz(counts, bin_centers) / np.trapz(promath.gaussFunc(np.arange(0,fit["gauss_mean"]*3), fit["gauss_const"], fit["gauss_mean"], fit["gauss_std"]), np.arange(0,fit["gauss_mean"]*3))

# plt.plot(bin_centers, counts, 
#         lw=2, 
#         color='black', 
#         label='cut data set')

# plt.hist(df_Na22_511_sim_pen.optPhotonSumComptonQE, 
#         bins=numBins, 
#         range=[0, stop*1.2], 
#         color='yellow', 
#         label='original data set')

# plt.plot(np.arange(0, fit['gauss_mean']*2), 
#         promath.gaussFunc(np.arange(0, fit['gauss_mean']*2), fit['gauss_const'],fit['gauss_mean'],fit['gauss_std']),
#         color='purple')
# plt.title(f'tail-to-total = {Na22_511_tail_ratio*100}%')
# plt.legend()
# plt.show()

#Cs137 662
start = 4000#1000
stop = 8000
numBins = 200
counts, bin_edges = np.histogram(df_Cs137_662_sim_pen.optPhotonSumComptonQE, bins = numBins, range=[0, stop*1.2])
bin_centers = prodata.getBinCenters(bin_edges)
Ef = 0.66166
fit  =  prosim.calibrationFitBinned(counts, bin_centers, start, stop, Ef=Ef, plot=False, compton=True)
## rebin 
counts, bin_edges = np.histogram(df_Cs137_662_sim_pen.query(f'optPhotonSumComptonQE>{fit["gauss_mean"]-3*fit["gauss_std"]} and optPhotonSumComptonQE < {fit["gauss_mean"]+3*fit["gauss_std"]}').optPhotonSumComptonQE, 
        bins=numBins, 
        range=[0, stop*1.2])
bin_centers = prodata.getBinCenters(bin_edges)
Cs137_662_tail_ratio = np.trapz(counts, bin_centers) / np.trapz(promath.gaussFunc(np.arange(0, fit["gauss_mean"]*2), fit["gauss_const"], fit["gauss_mean"], fit["gauss_std"]), np.arange(0,fit["gauss_mean"]*2))

# plt.plot(bin_centers, counts, 
#         lw=2, 
#         color='black', 
#         label='cut data set')

# plt.hist(df_Cs137_662_sim_pen.optPhotonSumComptonQE, 
#         bins=numBins, 
#         range=[0, stop*1.2], 
#         color='yellow', 
#         label='original data set')

# plt.plot(np.arange(0, fit['gauss_mean']*2), 
#         promath.gaussFunc(np.arange(0, fit['gauss_mean']*2), fit['gauss_const'],fit['gauss_mean'],fit['gauss_std']),
#         color='purple')
# plt.title(f'tail-to-total = {Cs137_662_tail_ratio*100}%')
# plt.legend()
# plt.show()

#Co60 1173
start = 7600
stop = 15000
numBins = 130
counts, bin_edges = np.histogram(df_Co60_1173_sim_pen.optPhotonSumComptonQE, bins = 130, range=[300, stop*1.2])
bin_centers = prodata.getBinCenters(bin_edges)
Ef = 1.173
fit = prosim.calibrationFitBinned(counts, bin_centers, start, stop, Ef=Ef, plot=False, compton=True)
## rebin 
counts, bin_edges = np.histogram(df_Co60_1173_sim_pen.query(f'optPhotonSumComptonQE>{fit["gauss_mean"]-3*fit["gauss_std"]} and optPhotonSumComptonQE < {fit["gauss_mean"]+3*fit["gauss_std"]}').optPhotonSumComptonQE, bins = numBins, range=[0, stop*1.2])
bin_centers = prodata.getBinCenters(bin_edges)
Co60_1173_tail_ratio = np.trapz(counts, bin_centers) / np.trapz(promath.gaussFunc(np.arange(0,fit["gauss_mean"]*2), fit["gauss_const"], fit["gauss_mean"], fit["gauss_std"]), np.arange(0,fit["gauss_mean"]*2))

# plt.plot(bin_centers, counts, 
#         lw=2, 
#         color='black', 
#         label='cut data set')

# plt.hist(df_Co60_1173_sim_pen.optPhotonSumComptonQE, 
#         bins=numBins, 
#         range=[0, stop*1.2], 
#         color='yellow', 
#         label='original data set')

# plt.plot(np.arange(0, fit['gauss_mean']*2), 
#         promath.gaussFunc(np.arange(0, fit['gauss_mean']*2), fit['gauss_const'],fit['gauss_mean'],fit['gauss_std']),
#         color='purple')
# plt.title(f'tail-to-total = {Co60_1173_tail_ratio*100}%')
# plt.legend()
# plt.show()

#Na22 1275
start = 4000#2000
stop = 13000
numBins = 120
counts, bin_edges = np.histogram(df_Na22_1275_sim_pen.optPhotonSumComptonQE, bins = numBins, range=[0, stop*1.2])
bin_centers = prodata.getBinCenters(bin_edges)
Ef = 1.2745
fit = prosim.calibrationFitBinned(counts, bin_centers, start, stop, Ef=Ef, plot=False, compton=True)
## rebin 
counts, bin_edges = np.histogram(df_Na22_1275_sim_pen.query(f'optPhotonSumComptonQE>{fit["gauss_mean"]-3*fit["gauss_std"]} and optPhotonSumComptonQE < {fit["gauss_mean"]+3*fit["gauss_std"]}').optPhotonSumComptonQE, bins = numBins, range=[0, stop*1.2])
bin_centers = prodata.getBinCenters(bin_edges)
Na22_1275_tail_ratio = np.trapz(counts, bin_centers) / np.trapz(promath.gaussFunc(np.arange(0,fit["gauss_mean"]*2), fit["gauss_const"], fit["gauss_mean"], fit["gauss_std"]), np.arange(0,fit["gauss_mean"]*2))

# plt.plot(bin_centers, counts, 
#         lw=2, 
#         color='black', 
#         label='cut data set')

# plt.hist(df_Na22_1275_sim_pen.optPhotonSumComptonQE, 
#         bins=numBins, 
#         range=[0, stop*1.2], 
#         color='yellow', 
#         label='original data set')

# plt.plot(np.arange(0, fit['gauss_mean']*2), 
#         promath.gaussFunc(np.arange(0, fit['gauss_mean']*2), fit['gauss_const'],fit['gauss_mean'],fit['gauss_std']),
#         color='purple')
# plt.title(f'tail-to-total = {Na22_1275_tail_ratio*100}%')
# plt.legend()
# plt.show()

#Co60 1332
start = 6000#2500
stop = 14000
numBins = 100
counts, bin_edges = np.histogram(df_Co60_1332_sim_pen.optPhotonSumComptonQE, bins = numBins, range=[0, stop*1.2])
bin_centers = prodata.getBinCenters(bin_edges)
Ef = 1.3325
fit = prosim.calibrationFitBinned(counts, bin_centers, start, stop, Ef=Ef, plot=False, compton=True)
Co60_1332_tail_ratio = np.trapz(counts, bin_centers) / np.trapz(promath.gaussFunc(np.arange(0,fit["gauss_mean"]*2), fit["gauss_const"], fit["gauss_mean"], fit["gauss_std"]), np.arange(0,fit["gauss_mean"]*2))
## rebin 
counts, bin_edges = np.histogram(df_Co60_1332_sim_pen.query(f'optPhotonSumComptonQE>{fit["gauss_mean"]-3*fit["gauss_std"]} and optPhotonSumComptonQE < {fit["gauss_mean"]+3*fit["gauss_std"]}').optPhotonSumComptonQE, bins = numBins, range=[0, stop*1.2])
bin_centers = prodata.getBinCenters(bin_edges)

# plt.plot(bin_centers, counts, 
#         lw=2, 
#         color='black', 
#         label='cut data set')

# plt.hist(df_Co60_1332_sim_pen.optPhotonSumComptonQE, 
#         bins=numBins, 
#         range=[0, stop*1.2], 
#         color='yellow', 
#         label='original data set')

# plt.plot(np.arange(0, fit['gauss_mean']*2), 
#         promath.gaussFunc(np.arange(0, fit['gauss_mean']*2), fit['gauss_const'],fit['gauss_mean'],fit['gauss_std']),
#         color='purple')
# plt.title(f'tail-to-total = {Co60_1332_tail_ratio*100}%')
# plt.legend()
# plt.show()


#Th232 2615
start = 20000#13000
stop = 32000
numBins = 100
counts, bin_edges = np.histogram(df_2615_sim_pen.optPhotonSumComptonQE, bins = numBins, range=[0, stop*1.2])
bin_centers = prodata.getBinCenters(bin_edges)
Ef = 2.614533
fit = prosim.calibrationFitBinned(counts, bin_centers, start, stop, Ef=Ef, plot=False, compton=True)
## rebin 
counts, bin_edges = np.histogram(df_2615_sim_pen.query(f'optPhotonSumComptonQE>{fit["gauss_mean"]-3*fit["gauss_std"]} and optPhotonSumComptonQE < {fit["gauss_mean"]+3*fit["gauss_std"]}').optPhotonSumComptonQE, bins = numBins, range=[0, stop*1.2])
bin_centers = prodata.getBinCenters(bin_edges)
Th232_2615_tail_ratio = np.trapz(counts, bin_centers) / np.trapz(promath.gaussFunc(np.arange(0,fit["gauss_mean"]*1.5), fit["gauss_const"], fit["gauss_mean"], fit["gauss_std"]), np.arange(0,fit["gauss_mean"]*1.5))

# plt.plot(bin_centers, counts, 
#         lw=2, 
#         color='black', 
#         label='cut data set')

# plt.hist(df_2615_sim_pen.optPhotonSumComptonQE, 
#         bins=numBins, 
#         range=[0, stop*1.2], 
#         color='yellow', 
#         label='original data set')

# plt.plot(np.arange(0, fit['gauss_mean']*2), 
#         promath.gaussFunc(np.arange(0, fit['gauss_mean']*2), fit['gauss_const'],fit['gauss_mean'],fit['gauss_std']),
#         color='purple')
# plt.title(f'tail-to-total = {Th232_2615_tail_ratio*100}%')
# plt.legend()
# plt.show()

#AmBe 4439
start = 36000#20000
stop = 57500
numBins = 80
counts, bin_edges = np.histogram(df_4439_sim_pen.optPhotonSumComptonQE, bins = numBins, range=[0, stop*1.2])
bin_centers = prodata.getBinCenters(bin_edges)
Ef = 4.439
fit = prosim.calibrationFitBinned(counts, bin_centers, start, stop, Ef=Ef, plot=False, compton=True)
## rebin 
counts, bin_edges = np.histogram(df_4439_sim_pen.query(f'optPhotonSumComptonQE>{fit["gauss_mean"]-3*fit["gauss_std"]} and optPhotonSumComptonQE < {fit["gauss_mean"]+3*fit["gauss_std"]}').optPhotonSumComptonQE, bins = numBins, range=[0, stop*1.2])
bin_centers = prodata.getBinCenters(bin_edges)
AmBe_4439_tail_ratio = np.trapz(counts, bin_centers) / np.trapz(promath.gaussFunc(np.arange(0,fit["gauss_mean"]*1.5), fit["gauss_const"], fit["gauss_mean"], fit["gauss_std"]), np.arange(0,fit["gauss_mean"]*1.5))

# plt.plot(bin_centers, counts, 
#         lw=2, 
#         color='black', 
#         label='cut data set')

# plt.hist(df_4439_sim_pen.optPhotonSumComptonQE, 
#         bins=numBins, 
#         range=[0, stop*1.2], 
#         color='yellow', 
#         label='original data set')

# plt.plot(np.arange(0, fit['gauss_mean']*2), 
#         promath.gaussFunc(np.arange(0, fit['gauss_mean']*2), fit['gauss_const'],fit['gauss_mean'],fit['gauss_std']),
#         color='purple')

# plt.title(f'tail-to-total = {AmBe_4439_tail_ratio*100}%')
# plt.legend()
# plt.show()

energy = np.array([511, 662, 1173, 1275, 1332, 2615, 4439])
ratio = np.array([Na22_511_tail_ratio, Cs137_662_tail_ratio, Co60_1173_tail_ratio, Na22_1275_tail_ratio, Co60_1332_tail_ratio, Th232_2615_tail_ratio, AmBe_4439_tail_ratio])
plt.plot(energy/1000, ratio*100)
plt.ylabel('tail-to-toal ratio [%]')
plt.xlabel('energy [MeV]')
plt.show()