#!/usr/bin/env python3
import sys
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit

# IMPORT FROM LIBRARY DIRECTORY
sys.path.insert(0, "../library/")
sys.path.insert(0, "../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "sim_analysis/")
sys.path.insert(0, "data_analysis/")

import digidata
import nicholai_plot_helper as nplt
import processing_utility as prout
import processing_math as promath
import processing_data as prodata
import processing_pd as propd
import processing_simulation as prosim
import gaussEdgeFit_parametersEJ321P as geEJ321P
import gaussEdgeFit_parametersNE213A as geNE213A
import simFit_parametersNE213A as simNE213A

# from sim_energyCal import sim_cal
from data_energyCal import NE213A_lg_cal

"""
Script for running both data and simulation and calibrating data.
"""

PMT_gain = 4e6 #Nominal gain is 7'000'000 at nominal A/lm (model: 9821B p1)
att_factor_A = 6.14102564 #used when attenuating the signal before the digitizer.
att_factor_cosmic_A = 13.09339408 # #attenuation factor used when taking cosmic muon data.
att_factor_B = 6.15343348 #used when attenuating the signal before the digitizer.
att_factor_cosmic_B = 13.09360731# #attenuation factor used when taking cosmic muon data.
q = 1.60217662E-19 #charge of a single electron
plot = False


##############################
# LOADING DATA
##############################
path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop
bg_runs =       list(range(1552,1562))
Cs137_runs =    [1549]
Na22_runs =     [1551]
Th232_runs =    list(range(1641,1651))
Co60_runs =     [1605]
AmBe_runs =     [1601,1602,1603]
PuBe_runs =     [1606,1607,1608]
# cosmic_runs =   list(range(1266,1296))


df_bg =         propd.load_parquet_merge(path, bg_runs,     keep_col=['qdc_lg_ch1'], full=False)
df_Cs137 =      propd.load_parquet_merge(path, Cs137_runs,  keep_col=['qdc_lg_ch1'], full=False)
df_Na22 =       propd.load_parquet_merge(path, Na22_runs,   keep_col=['qdc_lg_ch1'], full=False)
df_Th232 =      propd.load_parquet_merge(path, Th232_runs,  keep_col=['qdc_lg_ch1'], full=False)*1.087
df_Co60 =       propd.load_parquet_merge(path, Co60_runs,   keep_col=['qdc_lg_ch1'], full=False)
df_PuBe =       propd.load_parquet_merge(path, PuBe_runs,   keep_col=['qdc_lg_ch1'], full=False)
df_AmBe =       propd.load_parquet_merge(path, AmBe_runs,   keep_col=['qdc_lg_ch1'], full=False)
# df_cosmic =     propd.load_parquet_merge(path, cosmic_runs, keep_col=['qdc_lg_ch1'], full=False)

###############################
# LOADING SIMULATION DATA
###############################
path_sim = '/media/gheed/Seagate_Expansion_Drive1/data/simulation/'
Cs137_sim =         np.genfromtxt(path_sim + 'NE213A/Cs137/isotropic/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
Na22_sim =          np.genfromtxt(path_sim + 'NE213A/Na22/isotropic/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
Na22_511_sim =      np.genfromtxt(path_sim + 'NE213A/Na22_511/isotropic/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
Na22_1275_sim =     np.genfromtxt(path_sim + 'NE213A/Na22_1275/isotropic/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
# Co60_sim =          np.genfromtxt(path_sim + 'NE213A/Co60/isotropic/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
Co60_1173_sim =     np.genfromtxt(path_sim + 'NE213A/Co60_1173/isotropic/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
Co60_1332_sim =     np.genfromtxt(path_sim + 'NE213A/Co60_1332/isotropic/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
Th232_sim =         np.genfromtxt(path_sim + 'NE213A/2_62MeV/isotropic/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
AmBe_sim =          np.genfromtxt(path_sim + 'NE213A/4_44MeV/isotropic/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]

#append extra data
Cs137_sim =         np.append(Cs137_sim, np.genfromtxt(path_sim + 'NE213A/Cs137/isotropic/part2/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])
Cs137_sim =         np.append(Cs137_sim, np.genfromtxt(path_sim + 'NE213A/Cs137/isotropic/part3/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])

Na22_sim =          np.append(Na22_sim, np.genfromtxt(path_sim + 'NE213A/Na22/isotropic/part2/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])
Na22_sim =          np.append(Na22_sim, np.genfromtxt(path_sim + 'NE213A/Na22/isotropic/part3/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])

Na22_511_sim =      np.append(Na22_511_sim, np.genfromtxt(path_sim + 'NE213A/Na22_511/isotropic/part2/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])
Na22_511_sim =      np.append(Na22_511_sim, np.genfromtxt(path_sim + 'NE213A/Na22_511/isotropic/part3/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])

Na22_1275_sim =     np.append(Na22_1275_sim, np.genfromtxt(path_sim + 'NE213A/Na22_1275/isotropic/part2/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])
Na22_1275_sim =     np.append(Na22_1275_sim, np.genfromtxt(path_sim + 'NE213A/Na22_1275/isotropic/part3/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])

# Co60_sim =          np.append(Co60_sim, np.genfromtxt(path_sim + 'NE213A/Co_60/isotropic/part2/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])

Co60_1173_sim =     np.append(Co60_1173_sim, np.genfromtxt(path_sim + 'NE213A/Co60_1173/isotropic/part2/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])
Co60_1173_sim =     np.append(Co60_1173_sim, np.genfromtxt(path_sim + 'NE213A/Co60_1173/isotropic/part3/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])

Co60_1332_sim =     np.append(Co60_1332_sim, np.genfromtxt(path_sim + 'NE213A/Co60_1332/isotropic/part2/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])
Co60_1332_sim =     np.append(Co60_1332_sim, np.genfromtxt(path_sim + 'NE213A/Co60_1332/isotropic/part3/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])

Th232_sim =         np.append(Th232_sim, np.genfromtxt(path_sim + 'NE213A/2_62MeV/isotropic/part2/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])
Th232_sim =         np.append(Th232_sim, np.genfromtxt(path_sim + 'NE213A/2_62MeV/isotropic/part3/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])

AmBe_sim =          np.append(AmBe_sim, np.genfromtxt(path_sim + 'NE213A/4_44MeV/isotropic/part2/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])
AmBe_sim =          np.append(AmBe_sim, np.genfromtxt(path_sim + 'NE213A/4_44MeV/isotropic/part3/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])

#############################
# CONVERT INTO FLOAT 
#############################
Cs137_sim =         pd.to_numeric(Cs137_sim, downcast='float')
# Na22_sim =          pd.to_numeric(Na22_sim, downcast='float')
Na22_511_sim =      pd.to_numeric(Na22_511_sim, downcast='float')
Na22_1275_sim =     pd.to_numeric(Na22_1275_sim, downcast='float')
Co60_1173_sim =     pd.to_numeric(Co60_1173_sim, downcast='float')
Co60_1332_sim =     pd.to_numeric(Co60_1332_sim, downcast='float')
# Co60_sim =          pd.to_numeric(Co60_sim, downcast='float')
Th232_sim =         pd.to_numeric(Th232_sim, downcast='float')
AmBe_sim =          pd.to_numeric(AmBe_sim, downcast='float')

###############################
# CALIBRATING SIMULATION DATA
###############################
Cs137_sim =         prosim.chargeCalibration(Cs137_sim      * q * PMT_gain)
# Na22_sim =          prosim.chargeCalibration(Na22_sim       * q * PMT_gain)
Na22_511_sim =      prosim.chargeCalibration(Na22_511_sim   * q * PMT_gain)
Na22_1275_sim =     prosim.chargeCalibration(Na22_1275_sim  * q * PMT_gain)
Co60_1173_sim =     prosim.chargeCalibration(Co60_1173_sim  * q * PMT_gain)
Co60_1332_sim =     prosim.chargeCalibration(Co60_1332_sim  * q * PMT_gain)
# Co60_sim =          prosim.chargeCalibration(Co60_sim       * q * PMT_gain)
Th232_sim =         prosim.chargeCalibration(Th232_sim      * q * PMT_gain)
AmBe_sim =          prosim.chargeCalibration(AmBe_sim       * q * PMT_gain)

###############################
# RENAME NE213 DATA
###############################
Cs137_data =    df_Cs137
Na22_data =     df_Na22
Co60_data =     df_Co60
Th232_data =    df_Th232
AmBe_data =     df_AmBe

plot = True 

###############################
# PMT ALIGN Na22 511 keV
##############################
res = 0.23
Na22_511_sim_smear = np.zeros(len(Na22_511_sim))
for i in range(len(Na22_511_sim)):
    Na22_511_sim_smear[i] = prosim.gaussSmear(Na22_511_sim[i], res)

# countsSim, binsSim = np.histogram(Na22_511_sim_smear*1.327, bins=300, range=[0, 24000])
# binsSim = prodata.getBinCenters(binsSim)

# w = np.empty(len(df_bg))
# w.fill(1/10)
# countsBG, binsBG = np.histogram(df_bg.qdc_lg_ch1*att_factor_A, bins=300, range=[0, 24000], weights=w)

# counts, bins = np.histogram(Na22_data.qdc_lg_ch1*att_factor_A, bins=300, range=[0, 24000])
# bins = prodata.getBinCenters(bins)

# plt.plot(binsSim, countsSim*21.17, label='sim')
# plt.plot(bins, counts-countsBG, label='data-BG')
# plt.legend()
# plt.show()


Na22_511_results = prosim.PMTGainAlign( model = Na22_511_sim_smear, 
                                    data = Na22_data.qdc_lg_ch1*att_factor_A, 
                                    bg = df_bg.qdc_lg_ch1*att_factor_A,
                                    scale = 20,
                                    gain = 1.3,
                                    data_weight = 1,
                                    bg_weight = 10,
                                    scaleBound = [15 , 30],
                                    gainBound = [.9, 2],
                                    stepSize = 1e-2,
                                    # binRange = list(range(2750, 4510, 100)),
                                    binRange = list(range(2500, 4200, 75)),
                                    plot = plot)
Na22_511_results['smear'] = res


###############################
# PMT ALIGN Cs-137
###############################
res = 0.12
Cs137_sim_smear = np.zeros(len(Cs137_sim))
for i in range(len(Cs137_sim)):
    Cs137_sim_smear[i] = prosim.gaussSmear(Cs137_sim[i], res)


# countsSim, binsSim = np.histogram(Cs137_sim_smear*1.1, bins=150, range=[0, 24000])
# binsSim = prodata.getBinCenters(binsSim)

# w = np.empty(len(df_bg))
# w.fill(1/10)
# countsBG, binsBG = np.histogram(df_bg.qdc_lg_ch1*att_factor_A, bins=150, range=[0, 24000], weights=w)

# counts, bins = np.histogram(Cs137_data.qdc_lg_ch1*att_factor_A, bins=150, range=[0, 24000])
# bins = prodata.getBinCenters(bins)

# plt.plot(binsSim, countsSim*4, label='sim')
# plt.plot(bins, counts-countsBG, label='data-BG')
# plt.legend()
# plt.show()

Cs137_results = prosim.PMTGainAlign( model = Cs137_sim_smear, 
                                    data = Cs137_data.qdc_lg_ch1*att_factor_A, 
                                    bg = df_bg.qdc_lg_ch1*att_factor_A,
                                    scale = 21,
                                    gain = 1.1,
                                    data_weight = 1,
                                    bg_weight = 10,
                                    scaleBound = [3 , 5],
                                    gainBound = [.5, 1.5],
                                    stepSize = 1e-2,
                                    # binRange = list(range(3850, 7150, 200)),
                                    binRange = list(range(3100, 6500, 100)),
                                    plot = plot)
Cs137_results['smear'] = res


###############################
# PMT ALIGN Na22 full
###############################
# res = 0.10
# Na22_sim_smear = np.zeros(len(Na22_sim))
# for i in range(len(Na22_sim)):
#     Na22_sim_smear[i] = prosim.gaussSmear(Na22_sim[i], res)

# Na22_results = prosim.PMTGainAlign( model = Na22_sim_smear, 
#                                     data = Na22_data.qdc_lg_ch1*att_factor_A, 
#                                     bg = df_bg.qdc_lg_ch1*att_factor_A,
#                                     scale = 2.88,
#                                     gain = 0.347,
#                                     data_weight = 1,
#                                     bg_weight = 10,
#                                     scaleBound = [2.0 , 10],
#                                     gainBound = [0.1, 1.0],
#                                     stepSize = 1e-1,
#                                     binRange = list(range(9100*1.1, 12500*1.1, 130)),
#                                     # binRange = list(range(9100*1.1, 12500*1.1, 130)),
#                                     plot = plot)
# Na22_results['smear'] = res




###############################
# PMT ALIGN Na22 1275 keV
###############################
res = 0.09
Na22_1275_sim_smear = np.zeros(len(Na22_1275_sim))
for i in range(len(Na22_1275_sim)):
    Na22_1275_sim_smear[i] = prosim.gaussSmear(Na22_1275_sim[i], res)

# countsSim, binsSim = np.histogram(Na22_1275_sim_smear*1.09, bins=150, range=[0, 24000])
# binsSim = prodata.getBinCenters(binsSim)

# w = np.empty(len(df_bg))
# w.fill(1/10)
# countsBG, binsBG = np.histogram(df_bg.qdc_lg_ch1*att_factor_A, bins=150, range=[0, 24000], weights=w)

# counts, bins = np.histogram(Na22_data.qdc_lg_ch1*att_factor_A, bins=150, range=[0, 24000])
# bins = prodata.getBinCenters(bins)

# plt.plot(binsSim, countsSim*6, label='sim')
# plt.plot(bins, counts-countsBG, label='data-BG')
# plt.legend()
# plt.show()


Na22_1275_results = prosim.PMTGainAlign( model = Na22_1275_sim_smear, 
                                    data = Na22_data.qdc_lg_ch1*att_factor_A, 
                                    bg = df_bg.qdc_lg_ch1*att_factor_A,
                                    scale = 7,
                                    gain = 1,
                                    data_weight = 1,
                                    bg_weight = 10,
                                    scaleBound = [6 , 10],
                                    gainBound = [.5, 1.5],
                                    stepSize = 1e-2,
                                    # binRange = list(range(8190, 13750, 130)),
                                    binRange = list(range(9100, 12500, 130)),
                                    plot = plot)
Na22_1275_results['smear'] = res

###############################
# PMT ALIGN Co60 1332 keV
##############################
res = 0.14
Co60_1332_sim_smear = np.zeros(len(Co60_1332_sim))
for i in range(len(Co60_1332_sim)):
    Co60_1332_sim_smear[i] = prosim.gaussSmear(Co60_1332_sim[i], res)

countsSim, binsSim = np.histogram(Co60_1332_sim_smear*1.02, bins=150, range=[0, 30000])
# binsSim = prodata.getBinCenters(binsSim)

# w = np.empty(len(df_bg))
# w.fill(1/10)
# countsBG, binsBG = np.histogram(df_bg.qdc_lg_ch1*att_factor_A, bins=150, range=[0, 30000], weights=w)

# counts, bins = np.histogram(Co60_data.qdc_lg_ch1*att_factor_A, bins=150, range=[0, 30000])
# bins = prodata.getBinCenters(bins)

# plt.plot(binsSim, countsSim*2.055, label='sim')
# plt.plot(bins, counts-countsBG, label='data-BG')
# plt.legend()
# plt.show()

Co60_1332_results = prosim.PMTGainAlign( model = Co60_1332_sim_smear, 
                                    data = Co60_data.qdc_lg_ch1*att_factor_A, 
                                    bg = df_bg.qdc_lg_ch1*att_factor_A,
                                    scale = 2,
                                    gain = 1,
                                    data_weight = 1,
                                    bg_weight = 10,
                                    scaleBound = [1.8 , 3],
                                    gainBound = [.5, 1.5],
                                    stepSize = 1e-2,
                                    # binRange = list(range(7740, 14630, 150)),
                                    binRange = list(range(8600, 13300, 150)),
                                    plot = plot)
Co60_1332_results['smear'] = res




###############################
# PMT ALIGN Th232 2615 keV
###############################
res = 0.065
Th232_sim_smear = np.zeros(len(Th232_sim))
for i in range(len(Th232_sim)):
    Th232_sim_smear[i] = prosim.gaussSmear(Th232_sim[i], res)

# countsSim, binsSim = np.histogram(Th232_sim_smear*0.85, bins=65, range=[0, 35000])
# binsSim = prodata.getBinCenters(binsSim)

# w = np.empty(len(df_bg))
# w.fill(1/10)
# countsBG, binsBG = np.histogram(df_bg.qdc_lg_ch1*att_factor_A, bins=65, range=[0, 35000], weights=w)

# w = np.empty(len(Th232_data))
# w.fill(1/10)
# counts, bins = np.histogram(Th232_data.qdc_lg_ch1*att_factor_A, bins=65, range=[0, 35000], weights=w)
# bins = prodata.getBinCenters(bins)

# plt.plot(binsSim, countsSim*0.35, label='sim')
# plt.plot(bins, counts-countsBG, label='data-BG')
# plt.legend()
# plt.ylim([0, 15000])
# plt.show()

Th232_results = prosim.PMTGainAlign( model = Th232_sim_smear, 
                                    data = Th232_data.qdc_lg_ch1*att_factor_A, 
                                    bg = df_bg.qdc_lg_ch1*att_factor_A,
                                    scale = .35,
                                    gain = 0.85,
                                    data_weight = 10,
                                    bg_weight = 10,
                                    scaleBound = [.2, 1],
                                    gainBound = [.5, 1.5],
                                    stepSize = 1e-2,
                                    # binRange = list(range(20700, 33000, 500)),
                                    binRange = list(range(23000, 29000, 300)),
                                    plot = plot)
Th232_results['smear'] = res

###############################
# PMT ALIGN AmBe 4439 keV
###############################
res = 0.12
AmBe_sim_smear = np.zeros(len(AmBe_sim))
for i in range(len(AmBe_sim)):
    AmBe_sim_smear[i] = prosim.gaussSmear(AmBe_sim[i], res)

# countsSim, binsSim = np.histogram(AmBe_sim_smear*1.038, bins=75, range=[0, 100000])
# binsSim = prodata.getBinCenters(binsSim)

# w = np.empty(len(df_bg))
# w.fill(1/10)
# countsBG, binsBG = np.histogram(df_bg.qdc_lg_ch1*att_factor_A, bins=75, range=[0, 100000], weights=w)

# counts, bins = np.histogram(AmBe_data.qdc_lg_ch1*att_factor_A, bins=75, range=[0, 100000])
# bins = prodata.getBinCenters(bins)

# plt.plot(binsSim, countsSim*0.75, label='sim')
# plt.plot(bins, counts/3-countsBG, label='data-BG')
# plt.legend()
# plt.ylim([0,6000])
# plt.show()

AmBe_results = prosim.PMTGainAlign( model = AmBe_sim_smear, 
                                    data = AmBe_data.qdc_lg_ch1*att_factor_A, 
                                    bg = df_bg.qdc_lg_ch1*att_factor_A,
                                    scale = 0.75,
                                    gain = 1,
                                    data_weight = 3,
                                    bg_weight = 10,
                                    scaleBound = [.5 , 1.1],
                                    gainBound = [.5, 1.5],
                                    stepSize = 1e-2,
                                    # binRange = list(range(33300, 58300, 1000)),
                                    binRange = list(range(37000, 53000, 500)),
                                    plot = plot)
AmBe_results['smear'] = res



