"""
Fitting parameters for gaussEdgeFit.py script for simulated data from EJ-231P "cup" detector.
The fitting parameters for each source is put into separeate methods which 
can be called and will return the fitted gaussian, including errors.
Methods used is: comptonEdgeFit()
"""

import sys
import numpy as np
sys.path.insert(0, "../library/") #Import my own libraries
import processing_math as promath

def Na22_511_sim(data, counts, plot=False, binned=False):
    # _____Na-22 (0.511 keV)Boundary conditions
    const_min = -np.inf
    mean_min  = -np.inf
    sigma_min = -np.inf
    const_max = np.inf
    mean_max  = np.inf
    sigma_max = np.inf
    fit_lim = [[const_min, mean_min, sigma_min],[const_max, mean_max, sigma_max]]
    min = 35
    max = 65
    Ef = .511
    if not binned:
        return promath.comptonEdgeFit(data, min, max, Ef, fit_lim, plot=plot)
    else:
        return promath.comptonEdgeFitBinned(data, counts, min, max, Ef, fit_lim, plot=plot)

def Na22_1275_sim(data, counts, plot=False, binned=False):
    #_____Na-22 (1.275 keV)Boundary conditions
    const_min = -np.inf
    mean_min  = -np.inf#126
    sigma_min = -np.inf
    const_max = np.inf
    mean_max  = np.inf
    sigma_max = np.inf
    fit_lim = [[const_min, mean_min, sigma_min],[const_max, mean_max, sigma_max]]
    min = 118
    max = 228
    Ef = 1.275
    if not binned:
        return promath.comptonEdgeFit(data, min, max, Ef, fit_lim, binfactor=5, plot=plot)
    else:
        return promath.comptonEdgeFitBinned(data, counts, min, max, Ef, fit_lim, plot=plot)

def Co60_1333_sim(data, counts, plot=False, binned=False):
    #_____Co-60 (1.3325 MeVgaussEdgeFit_parametersSimulation
    const_max = np.inf
    mean_max  = np.inf
    sigma_max = np.inf
    fit_lim = [[const_min, mean_min, sigma_min],[const_max, mean_max, sigma_max]]
    min = 124
    max = 235
    Ef = 1.3325
    if not binned:
        return promath.comptonEdgeFit(data, min, max, Ef, fit_lim, binFactor=10, plot=plot)
    else:
        return promath.comptonEdgeFitBinned(data, counts, min, max, Ef, fit_lim, plot=plot)

def Cs137_662_sim(data, counts, plot=False, binned=False):
    #_____Cs-137 (0.6615 MeV)Boundary conditions
    const_min = -np.inf
    mean_min  = -np.inf
    sigma_min = -np.inf
    const_max = np.inf
    mean_max  = np.inf
    sigma_max = np.inf
    fit_lim = [[const_min, mean_min, sigma_min],[const_max, mean_max, sigma_max]]
    min = 48
    max = 91
    Ef = .6615
    if not binned:
        return promath.comptonEdgeFit(data, min, max, Ef, fit_lim, binFactor=2, plot=plot)
    else:
        return promath.comptonEdgeFitBinned(data, counts, min, max, Ef, fit_lim, plot=plot)

def PuBe_2230_sim(data, counts, plot=False, binned=False):
    #_____PuBe (2.230 MeV) Boundary conditions
    const_min = -np.inf
    mean_min  = -np.inf
    sigma_min = -np.inf
    const_max = np.inf
    mean_max  = np.inf
    sigma_max = np.inf
    fit_lim = [[const_min, mean_min, sigma_min],[const_max, mean_max, sigma_max]]
    min = 1
    max = 1
    Ef = 2.230
    if not binned:
        return promath.comptonEdgeFit(data, min, max, Ef, fit_lim, plot=plot)
    else:
        return promath.comptonEdgeFitBinned(data, counts, min, max, Ef, fit_lim, plot=plot)

def PuBe_4439_sim(data, counts, plot=False, binned=False):
    #_____PuBe (4.439 MeV) Boundary conditions
    const_min = -np.inf
    mean_min  = -np.inf
    sigma_min = -np.inf
    const_max = np.inf
    mean_max  = np.inf
    sigma_max = np.inf
    fit_lim = [[const_min, mean_min, sigma_min],[const_max, mean_max, sigma_max]]
    min = 11730
    max = 14625
    Ef = 4.439    
    if not binned:
        return promath.comptonEdgeFit(data, min, max, Ef, fit_lim, plot=plot)
    else:
        return promath.comptonEdgeFitBinned(data, counts, min, max, Ef, fit_lim, plot=plot)