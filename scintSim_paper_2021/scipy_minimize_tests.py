#!/usr/bin/env python3
import sys
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit

# IMPORT FROM LIBRARY DIRECTORY
sys.path.insert(0, "../library/")
sys.path.insert(0, "../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "sim_analysis/")
sys.path.insert(0, "data_analysis/")

import digidata
import nicholai_plot_helper as nplt
import processing_utility as prout
import processing_math as promath
import processing_data as prodata
import processing_pd as propd
import processing_simulation as prosim
import gaussEdgeFit_parametersEJ321P as geEJ321P
import gaussEdgeFit_parametersNE213A as geNE213A
import simFit_parametersNE213A as simNE213A

PMT_gain = 7e6 #Nominal gain is 7'000'000 at nominal A/lm (model: 9821B p1)
att_factor_A = 6.14102564 #used when attenuating the signal before the digitizer.
att_factor_cosmic_A = 13.09339408 # #attenuation factor used when taking cosmic muon data.
att_factor_B = 6.15343348 #used when attenuating the signal before the digitizer.
att_factor_cosmic_B = 13.09360731# #attenuation factor used when taking cosmic muon data.
q = 1.60217662E-19 #charge of a single electron
plot = False



def func(data, sim, x_off, y_off, numBins, binRange):
    """
    Give a binRange based on the 'data'
    """
    #numBins = 1024
    #binRange = [0, 1024]
    data_hist, data_bins = np.histogram(data, numBins, binRange)
    sim_hist, sim_bins = np.histogram(sim*x_off, numBins, binRange)
    
    return (( (sim_hist*y_off) - data_hist)*( (sim_hist*y_off) - data_hist)).sum


path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop
Cs137_runs =    [1549]
bg_runs =       list(range(1552,1600))
df_bg =         propd.load_parquet_merge(path, bg_runs,     keep_col=['qdc_lg_ch1'], full=False)
df_Cs137 =      propd.load_parquet_merge(path, Cs137_runs,  keep_col=['qdc_lg_ch1'], full=False)

path_sim = '/media/gheed/Seagate_Expansion_Drive1/data/simulation/'
sim_Cs137_iso =      np.genfromtxt(path_sim + 'NE213A/Cs137/isotropic/CSV_optphoton_sum_photocathode_no_cut_QE_smear.csv', delimiter=',') #
# sim_Cs137_pen =      np.genfromtxt(path_sim + 'NE213A/Cs137/isotropic/CSV_optphoton_sum_photocathode_compton_QE.csv', delimiter=',') #
df_Cs137_662_sim_iso =      prosim.loadData(path_sim + 'NE213A/Cs137/isotropic/analysis_ej321pchar_optphoton_sum_photocathode_no_cut_QE_smear.npz') #

df_Cs137_662_sim_iso.bin_edges =      prosim.chargeCalibration(df_Cs137_662_sim_iso.bin_edges   * q * PMT_gain)
sim_Cs137_iso =      prosim.chargeCalibration(sim_Cs137_iso   * q * PMT_gain)
# sim_Cs137_pen =      prosim.chargeCalibration(sim_Cs137_pen   * q * PMT_gain)

numBins = 4096
# binRange = [0,721953]
binRange = [0,4096]

# plt.step(df_Cs137_662_sim_iso.bin_edges, df_Cs137_662_sim_iso.counts, label='Prebinned sim')
plt.step(newData['bin_edges'][1::], newData['counts'], label='Prebinned sim')
plt.hist(sim_Cs137_iso, numBins, binRange, histtype='step', lw=2, label='Unbinned sim')
# plt.hist(df_Cs137.qdc_lg_ch1*att_factor_A, numBins, binRange, histtype='step', label='Unbinned data')
plt.legend()
plt.show()

# def minimize(func, )

# scipy.optimize.minimize

# INFO:
# https://scipy-lectures.org/advanced/mathematical_optimization/index.html




from numpy import exp, linspace, random, loadtxt, pi, sqrt
from scipy.optimize import curve_fit
from lmfit import Model
import matplotlib.pyplot as plt

def gaussian(x, amp, cen, wid):
    return amp * exp(-(x-cen)**2 / wid)

x = linspace(-10, 10, 101)
y = gaussian(x, 2.33, 0.21, 1.51) + random.normal(0, 0.2, x.size)

init_vals = [1, 0, 1]  # for [amp, cen, wid]
best_vals, covar = curve_fit(gaussian, x, y, p0=init_vals)

gmodel = Model(gaussian)
print('parameter names: {}'.format(gmodel.param_names))
print('independent variables: {}'.format(gmodel.independent_vars))

x_eval = linspace(0, 10, 201)
y_eval = gmodel.eval(x=x_eval, cen=0.5, amp=100, wid=2.0)

result = gmodel.fit(y, x=x, cen=0.5, amp=10, wid=2.0)

print(result.fit_report())

plt.plot(x, y, 'bo')
plt.plot(x, result.init_fit, 'k--', label='initial fit')
plt.plot(x, result.best_fit, 'r-', label='best fit')
plt.legend(loc='best')
plt.show()
