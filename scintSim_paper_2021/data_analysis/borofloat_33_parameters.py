import matplotlib.pyplot as plt
import numpy as np
from scipy import interpolate

#Borofloat 33 data taken from: https://www.schott.com/borofloat/english/attribute/optical/index.html
# Material is 5mm thick
wl = [277.1991, 280.3382, 282.8515, 285.1580, 288.0971, 291.6649, 294.6050, 294.6050, 294.6050, 297.5476, 299.4381, 301.1206, 303.2228, 304.2759, 305.5378, 306.8000, 308.2729, 309.3270, 310.3801, 311.2229, 312.4858, 313.7488, 315.4302, 317.7395, 320.0478, 322.3554, 325.0807, 328.4336, 331.9950, 336.1833, 340.5801, 345.3947, 350.2082, 354.3933, 358.5788, 364.6461, 369.0398, 374.6883, 378.6632, 382.8469, 387.0310, 391.8428, 396.4449, 400.2106, 450, 500]
trans = [0.0000,0.0168, 0.6218, 1.0756, 2.2857, 4.5546, 8.9412, 13.7815, 18.6218, 24.5210, 27.8487, 31.6303, 36.0168, 39.0420, 41.9160, 44.9412, 48.5714, 52.0504, 55.0756, 57.6471, 60.9748, 64.3025, 67.6303, 71.1092, 74.1345, 76.8571, 79.2773, 81.6975, 83.8151, 85.6303, 87.1429, 88.3529, 89.1092, 89.5630, 90.1681, 90.3193, 90.4706, 90.4706, 90.4706, 90.3193, 90.3193, 90.3193, 90.1681, 90.1681, 90.1681, 90.1681]
n1 = 1 #Air
n2 = 1.47 #Borofloat
# P = 2*n2/(n2**2+1) #Reflection factor
# R = ((n1-n2)/(n1+n2))**2#Reflectance from https://glassproperties.com/reflection/


# plt.plot(wl,trans)
# plt.show()



# CALCULATE THE ABSORPTION LENGHT FOR BOROFLOAT 33
wl = [277.1991, 280.3382, 282.8515, 285.1580, 288.0971, 291.6649, 294.6050, 294.6050, 294.6050, 297.5476, 299.4381, 301.1206, 303.2228, 304.2759, 305.5378, 306.8000, 308.2729, 309.3270, 310.3801, 311.2229, 312.4858, 313.7488, 315.4302, 317.7395, 320.0478, 322.3554, 325.0807, 328.4336, 331.9950, 336.1833, 340.5801, 345.3947, 350.2082, 354.3933, 358.5788, 364.6461, 369.0398, 374.6883, 378.6632, 382.8469, 387.0310, 391.8428, 396.4449, 400.2106, 450, 500] #https://www.schott.com/borofloat/english/attribute/optical/index.html
trans = [0.0000,0.0168, 0.6218, 1.0756, 2.2857, 4.5546, 8.9412, 13.7815, 18.6218, 24.5210, 27.8487, 31.6303, 36.0168, 39.0420, 41.9160, 44.9412, 48.5714, 52.0504, 55.0756, 57.6471, 60.9748, 64.3025, 67.6303, 71.1092, 74.1345, 76.8571, 79.2773, 81.6975, 83.8151, 85.6303, 87.1429, 88.3529, 89.1092, 89.5630, 90.1681, 90.3193, 90.4706, 90.4706, 90.4706, 90.3193, 90.3193, 90.3193, 90.1681, 90.1681, 90.1681, 90.1681] #https://www.schott.com/borofloat/english/attribute/optical/index.html
transFunc = interpolate.interp1d(wl, trans) #interpolate between values

wl2 = np.arange(400,505, 5) #define new wavelengths
totalTrans = transFunc(wl2) #calculate new transmission values for given wavelengths

n1 = [1.00029, 1.00029, 1.00029, 1.00029, 1.00029, 1.00029, 1.00029, 1.00029, 1.00029, 1.00029, 1.00029, 1.00029, 1.00029, 1.00029, 1.00029, 1.00029, 1.00029, 1.00029, 1.00029, 1.00029, 1.00029]
n2 = [1.4839, 1.4833, 1.4828, 1.4823, 1.4818, 1.4813, 1.4808, 1.4803, 1.4799, 1.4795, 1.4791, 1.4787, 1.4783, 1.4779, 1.4776, 1.4772, 1.4768, 1.4765, 1.4762, 1.4759, 1.4756]
P = np.arange(0)
internalTrans = np.arange(0)

for i in range(len(n2)):
    P = np.append(P, (2*n2[i]/(n2[i]**2+1)))
    internalTrans = np.append(internalTrans, totalTrans[i]/P[i])
absCoeff = -6.5/np.log(internalTrans/100) #calculate the absorption lenght using internal transmission
absCoeff2 = -6.5/np.log(totalTrans/100) #calculate the absorption lenght using internal transmission

plt.title('Borofloat 33 light transmission and abs. lenghth')
plt.plot(wl2, internalTrans, lw=2, label='Internal Transmission')
plt.plot(wl2, totalTrans, lw=2, label='Total Transmission')
plt.plot(wl2, absCoeff, lw=2, label='Absoprtion lenght (internal) [mm]')
plt.plot(wl2, absCoeff2, lw=2, label='Absoprtion lenght (total) [mm]')
plt.ylabel('Transmission [%]')
plt.xlabel('Wavelenght [nm]')
# plt.ylim([0,100])
plt.legend()
plt.show()