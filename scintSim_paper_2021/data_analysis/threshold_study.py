#!/usr/bin/env python3
import sys
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit
import dask.dataframe as dd

# IMPORT FROM LIBRARY DIRECTORY
# sys.path.insert(0, "../../library/")
# sys.path.insert(0, "../../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/") #Import my own libraries

import digidata
import processing_utility as prout
import processing_math as promath
import processing_data as prodata
import processing_pd as propd
import processing_simulation as prosim


"""
Script for studying the effects of varuing the threshold on the digitizer. Source Cs-137

TODO:
thr vs baseline
thr vs LiveTime
thr vs amplitude
thr vs QDC
thr vs rate
"""

path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop

df1000 = propd.load_parquet(path+'run01609/')
df990 = propd.load_parquet(path+'run01610/')
df980 = propd.load_parquet(path+'run01611/')
df970 = propd.load_parquet(path+'run01612/')
df960 = propd.load_parquet(path+'run01613/')
df950 = propd.load_parquet(path+'run01614/')
df940 = propd.load_parquet(path+'run01615/')
df930 = propd.load_parquet(path+'run01616/')
df920 = propd.load_parquet(path+'run01617/')
df910 = propd.load_parquet(path+'run01618/')
df900 = propd.load_parquet(path+'run01619/')
df890 = propd.load_parquet(path+'run01620/')
df880 = propd.load_parquet(path+'run01621/')
df870 = propd.load_parquet(path+'run01622/')
df860 = propd.load_parquet(path+'run01623/')
df850 = propd.load_parquet(path+'run01624/')
df840 = propd.load_parquet(path+'run01625/')
df830 = propd.load_parquet(path+'run01626/')
df820 = propd.load_parquet(path+'run01627/')
df810 = propd.load_parquet(path+'run01628/')
df800 = propd.load_parquet(path+'run01629/')


numBins = 2048
binRange = [0,2000]
plt.hist(df1000.baseline_ch1, bins=numBins, range=binRange, histtype='step', label='1000')
plt.hist(df990.baseline_ch1, bins=numBins, range=binRange, histtype='step', label='990')
plt.hist(df980.baseline_ch1, bins=numBins, range=binRange, histtype='step', label='980')
plt.hist(df970.baseline_ch1, bins=numBins, range=binRange, histtype='step', label='970')
plt.hist(df960.baseline_ch1, bins=numBins, range=binRange, histtype='step', label='960')
plt.hist(df950.baseline_ch1, bins=numBins, range=binRange, histtype='step', label='950')
plt.hist(df940.baseline_ch1, bins=numBins, range=binRange, histtype='step', label='940')
plt.hist(df930.baseline_ch1, bins=numBins, range=binRange, histtype='step', label='930')
plt.hist(df920.baseline_ch1, bins=numBins, range=binRange, histtype='step', label='920')
plt.hist(df910.baseline_ch1, bins=numBins, range=binRange, histtype='step', label='910')
plt.hist(df900.baseline_ch1, bins=numBins, range=binRange, histtype='step', label='900')
plt.hist(df890.baseline_ch1, bins=numBins, range=binRange, histtype='step', label='890')
plt.hist(df880.baseline_ch1, bins=numBins, range=binRange, histtype='step', label='880')
plt.hist(df870.baseline_ch1, bins=numBins, range=binRange, histtype='step', label='870')
plt.hist(df860.baseline_ch1, bins=numBins, range=binRange, histtype='step', label='860')
plt.hist(df850.baseline_ch1, bins=numBins, range=binRange, histtype='step', label='850')
plt.hist(df840.baseline_ch1, bins=numBins, range=binRange, histtype='step', label='840')
plt.hist(df830.baseline_ch1, bins=numBins, range=binRange, histtype='step', label='830')
plt.hist(df820.baseline_ch1, bins=numBins, range=binRange, histtype='step', label='820')
plt.hist(df810.baseline_ch1, bins=numBins, range=binRange, histtype='step', label='810')
plt.hist(df800.baseline_ch1, bins=numBins, range=binRange, histtype='step', label='800')
plt.legend()
plt.show()


threshold = np.arange(1000,840,-10)
baseline_avg = (df1000.baseline_ch1.mean(), 
                df990.baseline_ch1.mean(), 
                df980.baseline_ch1.mean(), 
                df970.baseline_ch1.mean(), 
                df960.baseline_ch1.mean(), 
                df950.baseline_ch1.mean(), 
                df940.baseline_ch1.mean(), 
                df930.baseline_ch1.mean(), 
                df920.baseline_ch1.mean(), 
                df910.baseline_ch1.mean(), 
                df900.baseline_ch1.mean(), 
                df890.baseline_ch1.mean(), 
                df880.baseline_ch1.mean(), 
                df870.baseline_ch1.mean(), 
                df860.baseline_ch1.mean(),
                df850.baseline_ch1.mean())

# plt.hist(df840.baseline_ch1, bins=numBins, range=binRange, histtype='step', label='840')
# plt.hist(df830.baseline_ch1, bins=numBins, range=binRange, histtype='step', label='830')
# plt.hist(df820.baseline_ch1, bins=numBins, range=binRange, histtype='step', label='820')
# plt.hist(df810.baseline_ch1, bins=numBins, range=binRange, histtype='step', label='810')
# plt.hist(df800.baseline_ch1, bins=numBins, range=binRange, histtype='step', label='800')

plt.plot(threshold, baseline_avg)
plt.show()


baseline_avg = (prodata.liveTimeCalc(df1000).baseline_ch1.mean(), 
                df990.baseline_ch1.mean(), 
                df980.baseline_ch1.mean(), 
                df970.baseline_ch1.mean(), 
                df960.baseline_ch1.mean(), 
                df950.baseline_ch1.mean(), 
                df940.baseline_ch1.mean(), 
                df930.baseline_ch1.mean(), 
                df920.baseline_ch1.mean(), 
                df910.baseline_ch1.mean(), 
                df900.baseline_ch1.mean(), 
                df890.baseline_ch1.mean(), 
                df880.baseline_ch1.mean(), 
                df870.baseline_ch1.mean(), 
                df860.baseline_ch1.mean(), 
                df850.baseline_ch1.mean())

