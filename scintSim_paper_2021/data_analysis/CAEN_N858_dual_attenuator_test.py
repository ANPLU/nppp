import matplotlib.pyplot as plt
import numpy as np
from scipy import interpolate

#Script for testing the attenuation coefficient of the CAEN N858 dual attenuator module
#A signal generator (Gaussian) was used to look at the different attenuation cofficients produced by the module

def attCoeffdB(dB, input):
    return pow(10, -dB/20)*input
    
dB = np.array([0, 1, 3, 6, 12, 16, 20, 23]) #set attenuation on the module

QDC_data_A = np.array([57.48, 50.95, 40.55, 28.81, 14.76, 9.36, 5.96, 4.39]) #integration values of pulses 
ph_data_A = np.array([1.18, 1.06, .84, .59, .3, .19, .12, .08]) #pulse height of pulses

QDC_data_B = np.array([57.35, 51.06, 40.53, 28.61, 14.45, 9.32, 5.95, 4.38]) #integration values of pulses 
ph_data_B = np.array([1.18, 1.06, 0.84, 0.59, 0.292, 0.19, 0.11, 0.08]) #pulse height of

theory_coeff = 1/pow(10, -dB/20)*1 #theoretical values for attenuation coefficients, voltage.
A_QDC_coeff = QDC_data_A[0]/QDC_data_A
A_ph_coeff =  ph_data_A[0]/ph_data_A

B_QDC_coeff = QDC_data_B[0]/QDC_data_B
B_ph_coeff =  ph_data_B[0]/ph_data_B

plt.title("Attenuation coefficents")
plt.scatter(dB, A_QDC_coeff, label='A: Integration values')
# plt.scatter(dB, A_ph_coeff, label='A: pulseheight values')
plt.scatter(dB, B_QDC_coeff, label='B: Integration values')
# plt.scatter(dB, B_ph_coeff, label='B: pulseheight values')
plt.scatter(dB, theory_coeff, label='Theoretical values')
plt.legend()
plt.ylabel('Att. coefficient')
plt.xlabel("deBedeBs'")
plt.yscale('log')
plt.grid(which='both')
plt.xlim([0,25])
plt.show()

