#!/usr/bin/env python3
import sys
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit
import dask.dataframe as dd

# IMPORT FROM LIBRARY DIRECTORY
# sys.path.insert(0, "../../library/")
# sys.path.insert(0, "../../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/") #Import my own libraries

import digidata
import processing_utility as prout
import processing_math as promath
import processing_data as prodata
import processing_pd as propd
import processing_simulation as prosim


"""
Script for calibrating data with LaBr detector
"""
path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop

# ###STUDY THE COSMIC MUON PEAK###
# bg_LaBr_2 =     propd.load_parquet(path+'run01495/', full=True)
# plt.figure()
# plt.title('LaBr Background run')
# plt.hist(bg_LaBr_2.qdc_lg_ch1, bins=256, histtype='step', label='LG')
# plt.hist(bg_LaBr_2.qdc_sg_ch1, bins=256, histtype='step', label='SG')
# plt.legend()
# plt.yscale('log')
# plt.ylabel('counts')
# plt.xlabel('QDC')

# plt.figure()
# plt.title('LaBr Background run')
# plt.hist(bg_LaBr_2.amplitude_ch1, bins=256, histtype='step', label='Amplitude')
# plt.legend()
# plt.yscale('log')
# plt.ylabel('counts')
# plt.xlabel('Amplitude')

# plt.show()

# for i in bg_LaBr_2.query("amplitude_ch1<300").samples_ch1:
#     plt.title('LaBr waveform')
#     plt.plot(i)
#     plt.grid(which='both')
#     plt.show()






def LaBrEcal_quad(QDC):
    """
    Quick and dirty energy calibration done by visual inspection quadratic
    """
    return 9.35749198e-9*QDC**2+1.61954885e-04*QDC+2.89077210e-01

LT_LaBr = {
    'bg':0.999953125021153,
    'AmBe': 0.9997446353149092,
    'PuBe': 0.9996208242418188,
    'Co-60': 0.9992667020436935,
    'Th-238': 0.9999171664915868,
    'Cs-137': 0.9999052184038779,
    'Na-22': 0.9996534860975269
    }

#IMPORTING DATA
bg_LaBr =       propd.load_parquet_merge(path, list(range(1495, 1511))+list(range(1516, 1531))+list(range(1534, 1549)), keep_col=['qdc_lg_ch1', 'qdc_sg_ch1', 'evtno'], full=False)
# bg_LaBr =       propd.load_parquet_merge(path, list(range(1495, 1497)), keep_col=['qdc_lg_ch1', 'qdc_sg_ch1', 'evtno'], full=False)
Co60_LaBr =     propd.load_parquet(path+'run01511/', keep_col=['qdc_lg_ch1', 'qdc_sg_ch1', 'evtno'], full=False)
Th238_LaBr =    propd.load_parquet(path+'run01512/', keep_col=['qdc_lg_ch1', 'qdc_sg_ch1', 'evtno'], full=False)
Cs137_LaBr =    propd.load_parquet(path+'run01513/', keep_col=['qdc_lg_ch1', 'qdc_sg_ch1', 'evtno'], full=False)
Na22_LaBr =     propd.load_parquet(path+'run01531/', keep_col=['qdc_lg_ch1', 'qdc_sg_ch1', 'evtno'], full=False)
AmBe_LaBr =     propd.load_parquet_merge(path, [1514, 1515], keep_col=['qdc_lg_ch1', 'qdc_sg_ch1', 'evtno'], full=False)
PuBe_LaBr =     propd.load_parquet_merge(path, [1532, 1533], keep_col=['qdc_lg_ch1', 'qdc_sg_ch1', 'evtno'], full=False)

#Define the background
numBins = 2048
binRange = [0,50000]
ybg, xbg = np.histogram(bg_LaBr.qdc_lg_ch1, numBins, binRange)
ybg = ybg/(LT_LaBr['bg']*46)


##CALIBRATION OF DATA
LaBr_energies = [0.662, 1.173, 1.332, 0.511, 1.275, 2.614, 4.439] #define energies for calibration
LaBr_peaks = [] #define holder for peak positions



#Fitting Cs137 spectra
y, x = np.histogram(Cs137_LaBr.qdc_lg_ch1, numBins, binRange)
x = prodata.getBinCenters(x)
y = y-ybg
popt, pcov = promath.gaussFit(x, y-363, start=1850, stop=2250, error=True)
LaBr_peaks.append(popt[1])

#Fitting Co60 spectra
y, x = np.histogram(Co60_LaBr.qdc_lg_ch1, numBins, binRange)
x = prodata.getBinCenters(x)
y = y-ybg
popt, pcov = promath.gaussFit(x, y-5200, start=4090, stop=4500, error=True)
LaBr_peaks.append(popt[1])
popt, pcov = promath.gaussFit(x, y-376, start=4700, stop=5300, error=True)
LaBr_peaks.append(popt[1])

#Fitting Na22 spectra
y, x = np.histogram(Na22_LaBr.qdc_lg_ch1, numBins, binRange)
x = prodata.getBinCenters(x)
y = y-ybg
popt, pcov = promath.gaussFit(x, y-5500, start=1280, stop=1580, error=True)
LaBr_peaks.append(popt[1])
popt, pcov = promath.gaussFit(x, y-200, start=4500, stop=4900, error=True)
LaBr_peaks.append(popt[1])

#Fitting Th238 spectra
y, x = np.histogram(Th238_LaBr.qdc_lg_ch1, numBins, binRange)
x = prodata.getBinCenters(x)
y = y-ybg
popt, pcov = promath.gaussFit(x, y-20, start=9200, stop=9600, error=True)
LaBr_peaks.append(popt[1])

#Fitting AmBe spectra
y, x = np.histogram(AmBe_LaBr.qdc_lg_ch1, numBins, binRange)
x = prodata.getBinCenters(x)
y = y-ybg
popt, pcov = promath.gaussFit(x, y-111, start=13800, stop=14400, error=True)
LaBr_peaks.append(popt[1])


#CALIBRATE WITH QUADRATIC FUNCTION
popt, pcov = curve_fit(promath.quadraticFunc, LaBr_peaks, LaBr_energies)






##PLOTTING DATA
numBins = 2048

binRange = [0,50000]
plt.title('LaBr detector')

ybg, xbg = np.histogram(bg_LaBr.qdc_lg_ch1, numBins, binRange)
ybg = ybg/(LT_LaBr['bg']*46)
plt.step(LaBrEcal_quad(xbg[:-1]), ybg, lw=2, label='Background')

# y, x = np.histogram(Co60_LaBr.qdc_lg_ch1, numBins, binRange)
# plt.step(LaBrEcal_quad(x[:-1]), (y/(LT_LaBr['Co-60']*1))-ybg, lw=2, label='Co60 (45 cm)')

# y, x = np.histogram(Th238_LaBr.qdc_lg_ch1, numBins, binRange)
# plt.step(LaBrEcal_quad(x[:-1]), (y/(LT_LaBr['Th-238']*1))-ybg, lw=2, label='Th-238 (45 cm)')

# y, x = np.histogram(Cs137_LaBr.qdc_lg_ch1, numBins, binRange)
# plt.step(LaBrEcal_quad(x[:-1]), (y/(LT_LaBr['Cs-137']*1))-ybg, lw=2, label='Cs-137 (45 cm)')

# y, x = np.histogram(Na22_LaBr.qdc_lg_ch1, numBins, binRange)
# plt.step(LaBrEcal_quad(x[:-1]), (y/(LT_LaBr['Na-22']*1))-ybg, lw=2, label='Na-22 (45 cm)')

# y, x = np.histogram(AmBe_LaBr.qdc_lg_ch1, numBins, binRange)
# plt.step(LaBrEcal_quad(x[:-1]), (y/(LT_LaBr['AmBe']*2))-ybg, lw=2, label='AmBe (200 cm)')

# y, x = np.histogram(PuBe_LaBr.qdc_lg_ch1, numBins, binRange)
# plt.step(LaBrEcal_quad(x[:-1]), (y/(LT_LaBr['PuBe']*2))-ybg, lw=2, label='PuBe (200 cm)')

plt.ylabel('counts')
plt.xlabel('Energy [MeV$_{ee}$]')
plt.yscale('log')
plt.legend()
plt.show()






def load_parquet_merge(path, runNum, ignore_cols, compute=False):
    """
    Wrapper method for loading mutiple run numbers and concateanating into one data set. 
    Uses digidata.load_parquet()

    Parameters:
    path...........path to parquet files [string]
    runNum........list of run numbers [integer].
    ignore_cols...columns which to exlude when reading in files [list of strings]
    compute.......If False method will return dask.dataframe object, if True methods will return pandas.dataframe object.

    -------------
    Nicholai Mauritzson
    2020-12-11
    """
    #TODO: make selector to use pandas instead of dask to speed up loading process.
    if isinstance(runNum, list):
        print(f'Now loading {len(runNum)} run(s):')
        ddTMP = []
        for run in tqdm(runNum):
            ddTMP.append(digidata.read_parquet(path+f'run0{run}/', ignore_cols))
        ddata = dd.concat(ddTMP)
    else:
        print('TypeError: runNum needs to be a list of integers...')

    if compute:
        print('- computing data...')
        return ddata.compute()
    else:
        return ddata

