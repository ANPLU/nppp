#!/usr/bin/env python3
import sys
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit

# IMPORT FROM MASTER HOME DIRECTORY
# sys.path.insert(0, "../")
sys.path.insert(0, "../../library/")
import processing_math as promath
import processing_data as prodata
import processing_pd as propd
# sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries

"""
Script will fit data of charge injected signals from digitizer, fit each one and derive a linear 
correlation between integration value (ADC) and charge.
Can be used to calibrate long gate values from digitizer data
"""

#IMPORT DATA
path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop
data_100 = pd.read_parquet(path+"run01075/")
data_200 = pd.read_parquet(path+"run01076/")
data_300 = pd.read_parquet(path+"run01077/")
data_400 = pd.read_parquet(path+"run01078/")
data_500 = pd.read_parquet(path+"run01079/")
data_600 = pd.read_parquet(path+"run01080/")
data_700 = pd.read_parquet(path+"run01081/")
data_800 = pd.read_parquet(path+"run01082/")
data_900 = pd.read_parquet(path+"run01083/")
data_1000 = pd.read_parquet(path+"run01084/")

# numBins = 2000
# binRange = [0, 20000]
# plt.hist(data_100.qdc_lg_ch1, bins=numBins, range=binRange)
# plt.hist(data_200.qdc_lg_ch1, bins=numBins, range=binRange)
# plt.hist(data_300.qdc_lg_ch1, bins=numBins, range=binRange)
# plt.hist(data_400.qdc_lg_ch1, bins=numBins, range=binRange)
# plt.hist(data_500.qdc_lg_ch1, bins=numBins, range=binRange)
# plt.hist(data_600.qdc_lg_ch1, bins=numBins, range=binRange)
# plt.hist(data_700.qdc_lg_ch1, bins=numBins, range=binRange)
# plt.hist(data_800.qdc_lg_ch1, bins=numBins, range=binRange)
# plt.hist(data_900.qdc_lg_ch1, bins=numBins, range=binRange)
# plt.hist(data_1000.qdc_lg_ch1, bins=numBins, range=binRange)
# plt.show()

# #FIT DATA
const_val = np.arange(0)
const_val_err = np.arange(0)
QDC_val = np.arange(0)
QDC_val_err = np.arange(0)
sigma_val = np.arange(0)
sigma_val_err = np.arange(0)

val, bins = np.histogram(data_100['qdc_lg_ch1'], bins = 200, range = [800, 1000])
par, err = promath.gaussFit(prodata.getBinCenters(bins), val, error=True)
plt.plot(bins[:-1], val)
plt.plot(bins[:-1], promath.gaussFunc(bins[:-1], par[0], par[1], par[2]), label=f'$\sigma=${par[2]}')
plt.legend()
# plt.show()
const_val = np.append(const_val, par[0])
QDC_val = np.append(QDC_val, par[1])
sigma_val = np.append(sigma_val, par[2])
const_val_err = np.append(const_val_err, par[0])
QDC_val_err = np.append(QDC_val_err, err[1])
sigma_val_err = np.append(sigma_val_err, err[2])

val, bins = np.histogram(data_200['qdc_lg_ch1'], bins = 200, range = [2400, 2600])
par, err = promath.gaussFit(prodata.getBinCenters(bins), val, error=True)
plt.plot(bins[:-1], val)
plt.plot(bins[:-1], promath.gaussFunc(bins[:-1], par[0], par[1], par[2]), label=f'$\sigma=${par[2]}')
plt.legend()
# plt.show()
const_val = np.append(const_val, par[0])
QDC_val = np.append(QDC_val, par[1])
sigma_val = np.append(sigma_val, par[2])
const_val_err = np.append(const_val_err, par[0])
QDC_val_err = np.append(QDC_val_err, err[1])
sigma_val_err = np.append(sigma_val_err, err[2])

val, bins = np.histogram(data_300['qdc_lg_ch1'], bins = 200, range = [4100, 4300])
par, err = promath.gaussFit(prodata.getBinCenters(bins), val, error=True)
plt.plot(bins[:-1], val)
plt.plot(bins[:-1], promath.gaussFunc(bins[:-1], par[0], par[1], par[2]), label=f'$\sigma=${par[2]}')
plt.legend()
# plt.show()
const_val = np.append(const_val, par[0])
QDC_val = np.append(QDC_val, par[1])
sigma_val = np.append(sigma_val, par[2])
const_val_err = np.append(const_val_err, par[0])
QDC_val_err = np.append(QDC_val_err, err[1])
sigma_val_err = np.append(sigma_val_err, err[2])

val, bins = np.histogram(data_400['qdc_lg_ch1'], bins = 200, range = [5820, 6020])
par, err = promath.gaussFit(prodata.getBinCenters(bins), val, error=True)
plt.plot(bins[:-1], val)
plt.plot(bins[:-1], promath.gaussFunc(bins[:-1], par[0], par[1], par[2]), label=f'$\sigma=${par[2]}')
plt.legend()
# plt.show()
const_val = np.append(const_val, par[0])
QDC_val = np.append(QDC_val, par[1])
sigma_val = np.append(sigma_val, par[2])
const_val_err = np.append(const_val_err, par[0])
QDC_val_err = np.append(QDC_val_err, err[1])
sigma_val_err = np.append(sigma_val_err, err[2])

val, bins = np.histogram(data_500['qdc_lg_ch1'], bins = 300, range = [7500, 7800])
par, err = promath.gaussFit(prodata.getBinCenters(bins), val, error=True)
plt.plot(bins[:-1], val)
plt.plot(bins[:-1], promath.gaussFunc(bins[:-1], par[0], par[1], par[2]), label=f'$\sigma=${par[2]}')
plt.legend()
# plt.show()
const_val = np.append(const_val, par[0])
QDC_val = np.append(QDC_val, par[1])
sigma_val = np.append(sigma_val, par[2])
const_val_err = np.append(const_val_err, par[0])
QDC_val_err = np.append(QDC_val_err, err[1])
sigma_val_err = np.append(sigma_val_err, err[2])

val, bins = np.histogram(data_600['qdc_lg_ch1'], bins = 300, range = [9250, 9550])
par, err = promath.gaussFit(prodata.getBinCenters(bins), val, error=True)
plt.plot(bins[:-1], val)
plt.plot(bins[:-1], promath.gaussFunc(bins[:-1], par[0], par[1], par[2]), label=f'$\sigma=${par[2]}')
plt.legend()
# plt.show()
const_val = np.append(const_val, par[0])
QDC_val = np.append(QDC_val, par[1])
sigma_val = np.append(sigma_val, par[2])
const_val_err = np.append(const_val_err, par[0])
QDC_val_err = np.append(QDC_val_err, err[1])
sigma_val_err = np.append(sigma_val_err, err[2])

val, bins = np.histogram(data_700['qdc_lg_ch1'], bins = 300, range = [11050, 11350])
par, err = promath.gaussFit(prodata.getBinCenters(bins), val, error=True)
plt.plot(bins[:-1], val)
plt.plot(bins[:-1], promath.gaussFunc(bins[:-1], par[0], par[1], par[2]), label=f'$\sigma=${par[2]}')
plt.legend()
# plt.show()
const_val = np.append(const_val, par[0])
QDC_val = np.append(QDC_val, par[1])
sigma_val = np.append(sigma_val, par[2])
const_val_err = np.append(const_val_err, par[0])
QDC_val_err = np.append(QDC_val_err, err[1])
sigma_val_err = np.append(sigma_val_err, err[2])

val, bins = np.histogram(data_800['qdc_lg_ch1'], bins = 300, range = [12850, 13150])
par, err = promath.gaussFit(prodata.getBinCenters(bins), val, error=True)
plt.plot(bins[:-1], val)
plt.plot(bins[:-1], promath.gaussFunc(bins[:-1], par[0], par[1], par[2]), label=f'$\sigma=${par[2]}')
plt.legend()
# plt.show()
const_val = np.append(const_val, par[0])
QDC_val = np.append(QDC_val, par[1])
sigma_val = np.append(sigma_val, par[2])
const_val_err = np.append(const_val_err, par[0])
QDC_val_err = np.append(QDC_val_err, err[1])
sigma_val_err = np.append(sigma_val_err, err[2])

val, bins = np.histogram(data_900['qdc_lg_ch1'], bins = 300, range = [14600, 14900])
par, err = promath.gaussFit(prodata.getBinCenters(bins), val, error=True)
plt.plot(bins[:-1], val)
plt.plot(bins[:-1], promath.gaussFunc(bins[:-1], par[0], par[1], par[2]), label=f'$\sigma=${par[2]}')
plt.legend()
# plt.show()
const_val = np.append(const_val, par[0])
QDC_val = np.append(QDC_val, par[1])
sigma_val = np.append(sigma_val, par[2])
const_val_err = np.append(const_val_err, par[0])
QDC_val_err = np.append(QDC_val_err, err[1])
sigma_val_err = np.append(sigma_val_err, err[2])

val, bins = np.histogram(data_1000['qdc_lg_ch1'], bins = 300, range = [16400, 16700])
par, err = promath.gaussFit(prodata.getBinCenters(bins), val, error=True)
plt.plot(bins[:-1], val)
plt.plot(bins[:-1], promath.gaussFunc(bins[:-1], par[0], par[1], par[2]), label=f'$\sigma=${par[2]}')
plt.legend()
plt.show()
const_val = np.append(const_val, par[0])
QDC_val = np.append(QDC_val, par[1])
sigma_val = np.append(sigma_val, par[2])

const_val_err = np.append(const_val_err, par[0])
QDC_val_err = np.append(QDC_val_err, err[1])
sigma_val_err = np.append(sigma_val_err, err[2])

#SHOWING SIGMA VS QDC
def expFunc(x, A, B):
    return A*B**x

def oneOverE(E,A):
    return A/E

def oneOverE2(E,A,B):
    return A/E**2+B

def specialExp(x, A, B, C, D):
    return A+B*x+C*np.exp(D*x)

# plt.plot(np.arange(0,20000,2000), sigma_val)
plt.plot(QDC_val/1000, sigma_val/QDC_val)
# from scipy import interpolate
# func = interpolate.interp1d(QDC_val, sigma_val/QDC_val)
# popt, pcov = curve_fit(promath.quadraticFunc, QDC_val, sigma_val)
# plt.plot(np.arange(0,40000), promath.quadraticFunc(np.arange(0,40000), popt[0], popt[1], popt[2]), label='quadratic fit')

# popt, pcov = curve_fit(promath.oneOverE, QDC_val, sigma_val/QDC_val)
# plt.plot(np.arange(900,40000), promath.oneOverE(np.arange(900,40000), popt[0], popt[1]), label='A/E+B')

# popt, pcov = curve_fit(oneOverE, QDC_val, sigma_val/QDC_val)
# plt.plot(np.arange(900,17000), oneOverE(np.arange(900,17000), popt[0]), label='A/E')

popt, pcov = curve_fit(promath.expFunc , QDC_val/1000, sigma_val/QDC_val, p0=[0.001, .01, -0.49])
plt.plot(np.arange(900, 16000)/1000, promath.expFunc(np.arange(900, 16000)/1000, popt[0], popt[1], popt[2]), label='A+Be^(C*x)')
pcov = np.sqrt(np.diag(pcov))
print('------------------------------')
print(f'Parameters {popt}')
print(f'Error {pcov}')
print(f'Relative errors {pcov/popt*100} %')
print(f'ExpFunc total rel. error: {round(np.sqrt((pcov/popt)[0]**2 + (pcov/popt)[1]**2 + (pcov/popt)[2]**2)*100,2)}%')


popt, pcov = curve_fit(specialExp , QDC_val/1000, sigma_val/QDC_val, p0=[0.001, .1, .01, -0.49])
plt.plot(np.arange(900, 16000)/1000, specialExp(np.arange(900, 16000)/1000, popt[0], popt[1], popt[2], popt[3]), label='A+Bx+C*e^(D*x)')
pcov = np.sqrt(np.diag(pcov))
print('------------------------------')
print(f'Parameters {popt}')
print(f'Error {pcov}')
print(f'Relative errors {pcov/popt*100} %')
print(f'Special ExpFunc total rel. error: {round(np.sqrt((pcov/popt)[0]**2 + (pcov/popt)[1]**2 + (pcov/popt)[2]**2 + (pcov/popt)[3]**2)*100,2)}%')

# popt, pcov = curve_fit(promath.expFunc, QDC_val, sigma_val)
# plt.plot(np.arange(0,40000), promath.expFunc(np.arange(0,40000), popt[0], popt[1], -popt[2]), label='exponential fit (e)')

# popt, pcov = curve_fit(expFunc, QDC_val, (sigma_val/QDC_val))
# plt.plot(np.arange(0,40000), expFunc(np.arange(0,40000), popt[0], popt[1]), label='exponential fit (a*b^x)')

plt.legend()
# plt.ylim([0,0.009])
plt.ylabel('sigma/QDC')
plt.xlabel('QDC')
plt.show()

#PHYSICAL CONSTANTS
Vin = np.array([.1, .2, .3, .4, .5, .6, .7, .8, .9, 1]) #Voltage of input signal [V]
C = 111e-12 #capacitance of circuit [F]
R = 983.6 #Resistance of circuit [Ohm]
q = 1.602176634e-19 #fundamental charge
Q = Vin*C #total charge
Nume = Q/q #number of electrons

PMT_QE = .3 #Quantum efficiency of photocathode, 30% at ~350 nm peak (9821B p1)
PMT_gain = 7e6 #Nominal gain is 7'000'000 at nominal A/lm (9821B p1)
att_factor = 6.31 #used when attenuating the signal before the digitizer

#FIT THE CALIBRATION DATA
popt, pcov = curve_fit(promath.linearFunc, Q, QDC_val, p0=[1e13, 8000])

#PLOT THE CALIBRATION DATA
# plt.scatter(Q, QDC_val, label='Data')
# plt.plot(np.arange(.1e-11, 1.2e-10, .1e-11), promath.linearFunc(np.arange(.1e-11, 1.2e-10, .1e-11), popt[0], popt[1]), '--r', lw=2, label=f'Linear fit: $(y={round(popt[0],5)}x+{round(popt[1],5)})')
# plt.xlabel('Charge [C]')
# plt.ylabel('QCD [a.u.]')
# plt.xlim([0, 1.3e-10])
# plt.legend()
# plt.show()

# # :::::::::FOR EJ321P ::::::::::::::
# #IMPORT Cs137 data
# path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop

# #LOAD Cs137 data and apply attenuation factor
# Cs137data = propd.load_parquet_merge(path, [1085, 1086, 1087, 1088, 1089, 1090, 1091, 1092, 1093, 1094]) #load and merge data
# yData, xData = np.histogram((Cs137data.qdc_lg_ch1*att_factor), bins = 512, range=[0, 40000]) #bin data
# xData = prodata.getBinCenters(xData) #get bin centers

# #LOAD Cs137 pencil beam simulation with edep and Compton scattering cuts 
# path_pen = '/media/gheed/Seagate_Expansion_Drive1/data/simulation/Cs137/pencilbeam/' #path on Nicholais laptop
# f_sim_pen = 'analysis_ej321pchar_h_sum_optphoton_photocathode_total_CS_QE.npz'
# Cs137sim_pen = np.load(path_pen+f_sim_pen)
# Cs137sim_pen_bins = prodata.getBinCenters(Cs137sim_pen['bin_edges'])*q*PMT_gain*popt[0]+popt[1] #Apply PMT gain and calibration to QDC values
# Cs137sim_pen_counts = Cs137sim_pen['counts']

# #LOAD Cs137 pencil beam simulation
# path_pen0 = '/media/gheed/Seagate_Expansion_Drive1/data/simulation/Cs137/pencilbeam/' #path on Nicholais laptop
# f_sim_pen0 = 'analysis_ej321pchar_h_sum_optphoton_photocathode_total.npz'
# Cs137sim_pen0 = np.load(path_pen0+f_sim_pen0)
# Cs137sim_pen0_bins = prodata.getBinCenters(Cs137sim_pen0['bin_edges'])*q*PMT_gain*popt[0]+popt[1] #Apply PMT gain and calibration to QDC values
# Cs137sim_pen0_counts = Cs137sim_pen0['counts']

# #LOAD Cs137 isotropic simulation with edep and Compton scattering cuts 
# path_iso = '/media/gheed/Seagate_Expansion_Drive1/data/simulation/Cs137/isotropic/' #path on Nicholais laptop
# f_sim_iso = 'analysis_ej321pchar_h_sum_optphoton_photocathode_total_CS_QE.npz'
# Cs137sim_iso = np.load(path_iso+f_sim_iso)
# Cs137sim_iso_bins = prodata.getBinCenters(Cs137sim_iso['bin_edges'])*q*PMT_gain*popt[0]+popt[1] #Apply PMT gain and calibration to QDC values
# Cs137sim_iso_counts = Cs137sim_iso['counts']

# #LOAD Cs137 isotropic simulation
# path_iso0 = '/media/gheed/Seagate_Expansion_Drive1/data/simulation/Cs137/isotropic/' #path on Nicholais laptop
# f_sim_iso0 = 'analysis_ej321pchar_h_sum_optphoton_photocathode_totall.npz'
# Cs137sim_iso0 = np.load(path_iso0+f_sim_iso0)
# Cs137sim_iso0_bins = prodata.getBinCenters(Cs137sim_iso0['bin_edges'])*q*PMT_gain*popt[0]+popt[1] #Apply PMT gain and calibration to QDC values
# Cs137sim_iso0_counts = Cs137sim_iso0['counts']

# plt.figure(1)
# plt.subplot(2,1,1)
# plt.suptitle('Total charge after PMT (EJ321P)')
# plt.plot(xData, yData/100, lw=2, color='blue', label='Cs-137 (data)')
# plt.plot(Cs137sim_iso_bins, Cs137sim_iso_counts, lw=2,  label='Cs-137 (simulation, isotropic)')
# plt.plot(Cs137sim_iso0_bins, Cs137sim_iso0_counts, lw=2,  label='Cs-137 (simulation, isotropic, no cuts)')
# plt.plot(Cs137sim_pen_bins, Cs137sim_pen_counts, lw=2, label='Cs-137 (simulation, pencil beam)')
# plt.plot(Cs137sim_pen0_bins, Cs137sim_pen0_counts, lw=2,  label='Cs-137 (simulation, pencil beam, no cuts)')
# plt.xlabel('QDC [a.u.]')
# plt.ylabel('Counts')
# # plt.yscale('log')
# # plt.xlim([-500, 40000])
# # plt.ylim([1,90000])
# plt.legend()
# plt.subplot(2,1,2)
# plt.plot(xData, yData/100, lw=2, color='blue', label='Cs-137 (data)')
# plt.plot(Cs137sim_iso_bins, Cs137sim_iso_counts, lw=2, color='green', label='Cs-137 (simulation, isotropic)')
# plt.plot(Cs137sim_iso0_bins, Cs137sim_iso0_counts, lw=2, color='red', label='Cs-137 (simulation, isotropic, no cuts)')
# plt.plot(Cs137sim_pen_bins, Cs137sim_pen_counts, lw=2, label='Cs-137 (simulation, pencil beam)')
# plt.plot(Cs137sim_pen0_bins, Cs137sim_pen0_counts, lw=2,  label='Cs-137 (simulation, pencil beam, no cuts)')
# plt.xlabel('QDC [a.u.]')
# plt.ylabel('Counts')
# plt.yscale('log')
# # plt.xlim([-500, 40000])
# # plt.ylim([1,90000])
# plt.legend()
# plt.show()

# # :::::::::FOR NE213::::::::::::::
# #IMPORT Cs137 data
# path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop

# #LOAD Cs137 data and apply attenuation factor
# Cs137data = propd.load_parquet_merge(path, [1085, 1086, 1087, 1088, 1089, 1090, 1091, 1092, 1093, 1094]) #load and merge data
# yData, xData = np.histogram((Cs137data.qdc_lg_ch1*att_factor), bins = 512, range=[0, 40000]) #bin data
# xData = prodata.getBinCenters(xData) #get bin centers

# #LOAD Cs137 pencilbeam simulation with edep and Compton scattering cuts 
# path_pen = '/media/gheed/Seagate_Expansion_Drive1/data/simulation/NE213/Cs137/pencilbeam/' #path on Nicholais laptop
# f_sim_pen = 'analysis_ej321pchar_h_sum_optphoton_photocathode_total_CS.npz'
# Cs137sim_pen = np.load(path_pen+f_sim_pen)
# Cs137sim_pen_bins = prodata.getBinCenters(Cs137sim_pen['bin_edges'])*q*PMT_gain*popt[0]+popt[1] #Apply PMT gain and calibration to QDC values
# Cs137sim_pen_counts = Cs137sim_pen['counts']

# #LOAD Cs137 pencilbeam simulation 
# path_pen0 = '/media/gheed/Seagate_Expansion_Drive1/data/simulation/NE213/Cs137/pencilbeam/' #path on Nicholais laptop
# f_sim_pen0 = 'analysis_ej321pchar_h_sum_optphoton_photocathode_total.npz'
# Cs137sim_pen0 = np.load(path_pen0+f_sim_pen0)
# Cs137sim_pen0_bins = prodata.getBinCenters(Cs137sim_pen0['bin_edges'])*q*PMT_gain*popt[0]+popt[1] #Apply PMT gain and calibration to QDC values
# Cs137sim_pen0_counts = Cs137sim_pen0['counts']

# plt.figure(2)
# plt.subplot(2,1,1)
# plt.suptitle('Total charge after PMT (NE213)')
# plt.plot(xData, yData/100, lw=2, color='blue', label='Cs-137 (data)')
# plt.plot(Cs137sim_pen_bins, Cs137sim_pen_counts, lw=2, color='green', label='Cs-137 (simulation, pencil beam)')
# plt.plot(Cs137sim_pen0_bins, Cs137sim_pen0_counts, lw=2, color='red', label='Cs-137 (simulation, pencil beam, no cuts)')
# plt.xlabel('QDC [a.u.]')
# plt.ylabel('Counts')
# # plt.yscale('log')
# # plt.xlim([-500, 40000])
# # plt.ylim([1,90000])
# plt.legend()
# plt.subplot(2,1,2)
# plt.plot(xData, yData/100, lw=2, color='blue', label='Cs-137 (data)')
# plt.plot(Cs137sim_pen_bins, Cs137sim_pen_counts, lw=2, color='green', label='Cs-137 (simulation, pencil beam)')
# plt.plot(Cs137sim_pen0_bins, Cs137sim_pen0_counts, lw=2, color='red', label='Cs-137 (simulation, pencil beam, no cuts)')
# plt.xlabel('QDC [a.u.]')
# plt.ylabel('Counts')
# plt.yscale('log')
# # plt.xlim([-500, 40000])
# # plt.ylim([1,90000])
# plt.legend()
# plt.show()








