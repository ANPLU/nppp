#!/usr/bin/env python3
import sys
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit

# IMPORT FROM MASTER HOME DIRECTORY
# sys.path.insert(0, "../")
sys.path.insert(0, "../../library/")
import processing_math as promath
import processing_data as prodata
import processing_pd as propd
# sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries

"""
A script used to study the waveforms from the data sets.
"""
path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop
Cs137_runs =    [1196, 1197, 1198, 1199, 1200, 1201, 1202, 1203, 1204, 1205]
Co60_runs =     [1226, 1227, 1228, 1229, 1230, 1231, 1232, 1233, 1234, 1235]
Na22_runs =     [1186, 1187, 1188, 1189, 1190, 1191, 1192, 1193, 1194, 1195]
PuBe_runs =     [1306, 1307, 1308, 1309, 1310, 1311, 1312, 1313, 1314, 1315]
AmBe_runs =     [1216, 1217, 1218, 1219, 1220, 1221, 1222, 1223, 1224, 1225]    
Th238_runs =    [1206, 1207, 1208, 1209, 1210, 1211, 1212, 1213, 1214, 1215]
cosmic_runs =   [1266, 1267, 1268, 1269, 1270, 1271, 1272, 1273 ,1274, 1275, 1276, 1277, 1278 ,1279, 1280, 1281, 1282, 1283, 1284, 1285, 1286 ,1287, 1288, 1289, 1290, 1291, 1292, 1293, 1294, 1295]
bg_runs =       [1176, 1177, 1178, 1179, 1180, 1181, 1182, 1183, 1184, 1185]

keep_col = ['samples_ch1', 'CFD_rise_ch1', 'qdc_lg_ch1', 'qdc_sg_ch1', 'evtno', 'baseline_ch1']

df_bg =         propd.load_parquet(path+'/run01176/', num_events=100, keep_col=keep_col, full=False)
df_Na22 =       propd.load_parquet(path+'/run01186/', num_events=100, keep_col=keep_col, full=False)
df_Cs137 =      propd.load_parquet(path+'/run01196/', num_events=100, keep_col=keep_col, full=False)
df_Th238 =      propd.load_parquet(path+'/run01206/', num_events=100, keep_col=keep_col, full=False)
df_Co60 =       propd.load_parquet(path+'/run01296/', num_events=100, keep_col=keep_col, full=False)
df_PuBe =       propd.load_parquet(path+'/run01306/', num_events=100, keep_col=keep_col, full=False)
df_AmBe =       propd.load_parquet(path+'/run01216/', num_events=100, keep_col=keep_col, full=False)
df_cosmic =     propd.load_parquet(path+'/run01266/', num_events=100, keep_col=keep_col, full=False)

#background
df_bg = df_bg.reset_index()
for i,x in enumerate(df_bg.samples_ch1):
    plt.plot(x - df_bg.baseline_ch1[i], color='blue')
    plt.vlines(df_bg.CFD_rise_ch1[i]-20, 0, -50, color='black', label='start')
    plt.vlines(df_bg.CFD_rise_ch1[i]+500-20, 0, -50, color='orange', label='LG end')
    plt.vlines(df_bg.CFD_rise_ch1[i]+50-20, 0, -50, color='red', label='SG')
    plt.legend()
    plt.show()


#cosmics
df_cosmic = df_cosmic.reset_index()
for i, x in enumerate(df_cosmic.samples_ch1):
    plt.plot(x - df_cosmic.baseline_ch1[i], color='blue')
    plt.vlines(df_cosmic.CFD_rise_ch1[i]-20, 0, -50, color='black', label='start')
    plt.vlines(df_cosmic.CFD_rise_ch1[i]+500-20, 0, -50, color='orange', label='LG end')
    plt.vlines(df_cosmic.CFD_rise_ch1[i]+50-20, 0, -50, color='red', label='SG')
    plt.legend()
    plt.show()

#df_PuBe
df_PuBe = df_PuBe.reset_index()
for i, x in enumerate(df_PuBe.samples_ch1):
    plt.plot(x - df_PuBe.baseline_ch1[i], color='blue')
    plt.vlines(df_PuBe.CFD_rise_ch1[i]-20, 0, -50, color='black', label='start')
    plt.vlines(df_PuBe.CFD_rise_ch1[i]+500-20, 0, -50, color='orange', label='LG end')
    plt.vlines(df_PuBe.CFD_rise_ch1[i]+50-20, 0, -50, color='red', label='SG')
    plt.legend()
    plt.show()


#df_AmBe
df_AmBe = df_AmBe.reset_index()
for i, x in enumerate(df_AmBe.samples_ch1):
    plt.plot(x - df_AmBe.baseline_ch1[i], color='blue')
    plt.vlines(df_AmBe.CFD_rise_ch1[i]-20, 0, -50, color='black', label='start')
    plt.vlines(df_AmBe.CFD_rise_ch1[i]+500-20, 0, -50, color='orange', label='LG end')
    plt.vlines(df_AmBe.CFD_rise_ch1[i]+50-20, 0, -50, color='red', label='SG')
    plt.legend()
    plt.show()