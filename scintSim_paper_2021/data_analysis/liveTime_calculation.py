#!/usr/bin/env python3
import sys
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit
import dask.dataframe as dd

# IMPORT FROM LIBRARY DIRECTORY
# sys.path.insert(0, "../../library/")
# sys.path.insert(0, "../../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/") #Import my own libraries

import digidata
import processing_utility as prout
import processing_math as promath
import processing_data as prodata
import processing_pd as propd
import processing_simulation as prosim


"""
Script for determining the Live Time of data
"""
path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop


bg_LaBr =       propd.load_parquet_merge(path, list(range(1495, 1511))+list(range(1516, 1531))+list(range(1534, 1549)), keep_col=['qdc_lg_ch1', 'qdc_sg_ch1', 'evtno'], full=False)

Co60_LaBr =     propd.load_parquet(path+'run01511/', keep_col=['qdc_lg_ch1', 'qdc_sg_ch1', 'evtno'], full=False)
Th238_LaBr =    propd.load_parquet(path+'run01512/', keep_col=['qdc_lg_ch1', 'qdc_sg_ch1', 'evtno'], full=False)
Cs137_LaBr =    propd.load_parquet(path+'run01513/', keep_col=['qdc_lg_ch1', 'qdc_sg_ch1', 'evtno'], full=False)
Na22_LaBr =     propd.load_parquet(path+'run01531/', keep_col=['qdc_lg_ch1', 'qdc_sg_ch1', 'evtno'], full=False)
AmBe_LaBr =     propd.load_parquet_merge(path, [1514, 1515], keep_col=['qdc_lg_ch1', 'qdc_sg_ch1', 'evtno'], full=False)
PuBe_LaBr =     propd.load_parquet_merge(path, [1532, 1533], keep_col=['qdc_lg_ch1', 'qdc_sg_ch1', 'evtno'], full=False)

numBins = 2048
binRange = [0,50000]
plt.title('LaBr detector')
# ybg, xbg = np.histogram(bg_LaBr.qdc_lg_ch1, numBins, binRange)
# plt.step(LaBrEcal_quad(xbg[:-1]), ybg, lw=2, label='Background')

# y, x = np.histogram(Co60_LaBr.qdc_lg_ch1, numBins, binRange)
# plt.step(LaBrEcal_quad(x[:-1]), y, lw=2, label='Co60 (45 cm)')

# y, x = np.histogram(Th238_LaBr.qdc_lg_ch1, numBins, binRange)
# plt.step(LaBrEcal_quad(x[:-1]), y, lw=2, label='Th-238 (45 cm)')

# y, x = np.histogram(Cs137_LaBr.qdc_lg_ch1, numBins, binRange)
# plt.step(LaBrEcal_quad(x[:-1]), y, lw=2, label='Cs-137 (45 cm)')

# y, x = np.histogram(Na22_LaBr.qdc_lg_ch1, numBins, binRange)
# plt.step(LaBrEcal_quad(x[:-1]), y, lw=2, label='Na-22 (45 cm)')

# y, x = np.histogram(AmBe_LaBr.qdc_lg_ch1, numBins, binRange)
# plt.step(LaBrEcal_quad(x[:-1]), y, lw=2, label='AmBe (200 cm)')

# y, x = np.histogram(PuBe_LaBr.qdc_lg_ch1, numBins, binRange)
# plt.step(LaBrEcal_quad(x[:-1]), y, lw=2, label='PuBe (200 cm)')

y, x = np.histogram(PuBe_LaBr.qdc_lg_ch1, numBins, binRange)
plt.step(LaBrEcal_quad(x[:-1]), y-ybg, lw=2, label='PuBe (200 cm)')


plt.ylabel('counts')
plt.xlabel('QDC')
plt.yscale('log')
plt.legend()
plt.show()



### LIVE TIME COMAPRISON ###

LT_single = []
ddf_list = []

# import pandas as pd
runs = []
path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop
run_numbers = list(range(1534,1540))

columns = ['evtno']
excl_columns = ['samples_ch1', 'samples_ch2', 'amplitude_ch2', 'digitizer','peak_index_ch1'
, 'CFD_rise_ch1','min_val_ch1', 'max_val_ch1','CFD_drop_ch1',                'amplitude_ch3' ,'amplitude_ch4', 'amplitude_ch5',
                'qdc_lg_ch1', 'qdc_lg_ch2', 'qdc_lg_ch3',
                'qdc_lg_ch4', 'qdc_lg_ch5', 'tof_ch2', 'tof_ch3', 'tof_ch4', 'tof_ch5']

df_temp = propd.load_parquet_merge(path, list(range(1534,1540)), keep_col=columns)
prodata.liveTimeCalc(df_temp, False)
for run in run_numbers:
    print(f'Loading run {run:05d}')
    df_temp = digidata.read_parquet(path+f'run0{run}/', excl_columns)
#     runs.append(df_temp)
#     LT_single.append(prodata.liveTimeCalc(df_temp, False))
# df = pd.concat(runs) 

# 92% with dd
# 37% with pd

# for run in tqdm(range(1376, 1385)):
#     ddf = digidata.read_parquet(path+f'run0{run}/', ['samples_ch1'])  
#     LT_single.append(prodata.liveTimeCalc(ddf.compute(), False)) 
#     ddf_list.append(ddf)


# df_merge = dd.concat(ddf_list)
# LT_merge = prodata.liveTimeCalc(df_merge, True) 


def load_parquet_merge(path, runNum, ignore_cols, compute=False):
    """
    Wrapper method for loading mutiple run numbers and concateanating into one data set. 
    Uses digidata.load_parquet()

    Parameters:
    path...........path to parquet files [string]
    runNum........list of run numbers [integer].
    ignore_cols...columns which to exlude when reading in files [list of strings]
    compute.......If False method will return dask.dataframe object, if True methods will return pandas.dataframe object.

    -------------
    Nicholai Mauritzson
    2020-12-11
    """
    #TODO: make selector to use pandas instead of dask to speed up loading process.
    if isinstance(runNum, list):
        print(f'Now loading {len(runNum)} run(s):')
        ddTMP = []
        for run in tqdm(runNum):
            ddTMP.append(digidata.read_parquet(path+f'run0{run}/', ignore_cols))
        ddata = dd.concat(ddTMP)
    else:
        print('TypeError: runNum needs to be a list of integers...')

    if compute:
        print('- computing data...')
        return ddata.compute()
    else:
        return ddata



    #LaBr LiveTimes
    #background: 0.999953125021153



##TESTING LIVE TIME DIFFERENCE WITH AMPLITUDE
df = load_parquet_merge(path, [1552], excl_columns, compute=True)   

for i in range(0,200,10):
    LT = prodata.getLiveTime(df.query(f'amplitude_ch1>{i}') )
    print(f'Amplitude>{i} gives LiveTime = {LT}')

plt.hist(df.query('amplitude_ch1>20 and amplitude_ch1<900').evtdiff, bins=15, range=[0,15], histtype='step', label='amp>20') 
plt.hist(df.query('amplitude_ch1>50 and amplitude_ch1<900').evtdiff, bins=15, range=[0,15], histtype='step', label='amp>50') 
plt.hist(df.query('amplitude_ch1>100 and amplitude_ch1<900').evtdiff, bins=15, range=[0,15], histtype='step', label='amp>100') 
plt.hist(df.query('amplitude_ch1>200 and amplitude_ch1<900').evtdiff, bins=15, range=[0,15], histtype='step', label='amp>200') 
# plt.hist(df.query('amplitude_ch1<900 and amplitude_ch1<900').evtdiff, bins=15, range=[0,15], histtype='step', lw=2, label='amp<990') 
plt.legend()
plt.show()

plt.hist(df.evtdiff, bins=256)
plt.show()
