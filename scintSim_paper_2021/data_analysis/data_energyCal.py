#!/usr/bin/env python3
import sys
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
import pandas as pd
from tqdm import tqdm
# IMPORT FROM MASTER HOME DIRECTORY
sys.path.insert(0, "../")
sys.path.insert(0, "../library/")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries

import gaussEdgeFit_parametersEJ321P as gefEJ321P #Library with fit parameters for compton edges.
import gaussEdgeFit_parametersNE213A as gefNE213A #Library with fit parameters for compton edges.

import processing_data as prodata
import processing_pd as propd

"""
Script for obtaining calibration data in MeVee for QDC spectra.

TODO: Script should return calibration equation. This can then manually be inserted into the lightYield.py script for final analysis.
"""
# ::::::::::::::::::::::::::::::::::::::::
#  STANDARD PARAMETERS FOR ALL DETECTORS
# ::::::::::::::::::::::::::::::::::::::::
include_col = ['qdc_lg_ch1']
att_factor_A = 6.14102564 #used when attenuating the signal before the digitizer.
att_factor_cosmic_A = 13.09339408 # #attenuation factor used when taking cosmic muon data.
att_factor_B = 6.15343348 #used when attenuating the signal before the digitizer.
att_factor_cosmic_B = 13.09360731# #attenuation factor used when taking cosmic muon data.
path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop


def NE213A_lg_cal(bg, Na22, Cs137, Co60, Th232, PuBe, AmBe, plot=False): 
    
    # Cs137_runs =    [1549]
    # Co60_runs =     [1605]
    # Na22_runs =     [1551]
    # Th232_runs =    [1600]
    # AmBe_runs =     [1601,1602,1603]
    # PuBe_runs =     [1606,1607,1608]
    # cosmic_runs =   list(range(1266,1296))
    # bg_runs =       list(range(1552,1600))

    # ::::::::::::::::::::::::::::::::::::::::::::::::::::
    #     LOAD BACKGROUND DATA
    # ::::::::::::::::::::::::::::::::::::::::::::::::::::
    # bg = propd.load_parquet_merge(path, bg_runs, keep_col=include_col, full=False)

    # ::::::::::::::::::::::::::::::::::::::::::::::::::::
    #     LOAD AND FIT COMPTON EDGES Na-22: NE213A
    # ::::::::::::::::::::::::::::::::::::::::::::::::::::
    # Na22 = propd.load_parquet_merge(path, Na22_runs, keep_col=include_col, full=False)
    Na22_511_fit = gefNE213A.Na22_511_LG(Na22.qdc_lg_ch1*att_factor_A, bg.qdc_lg_ch1*att_factor_A, plot=plot) 
    Na22_1275_fit = gefNE213A.Na22_1275_LG(Na22.qdc_lg_ch1*att_factor_A, bg.qdc_lg_ch1*att_factor_A, plot=plot)
    # del Na22

    # # ::::::::::::::::::::::::::::::::::::::::::::::::::::
    # #     LOAD AND FIT COMPTON EDGES Cs137: NE213A
    # # ::::::::::::::::::::::::::::::::::::::::::::::::::::
    # Cs137 = propd.load_parquet_merge(path, Cs137_runs, keep_col=include_col, full=False)
    Cs137_662_fit = gefNE213A.Cs137_662_LG(Cs137.qdc_lg_ch1*att_factor_A, bg.qdc_lg_ch1*att_factor_A, plot=plot) 
    # del Cs137

    # ::::::::::::::::::::::::::::::::::::::::::::::::::::
    #     LOAD AND FIT COMPTON EDGES Co-60: NE213A
    # ::::::::::::::::::::::::::::::::::::::::::::::::::::
    # Co60 = propd.load_parquet_merge(path, Co60_runs, keep_col=include_col, full=False)
    # Co60_1332_fit = gefNE213A.Co60_1332_LG(Co60.qdc_lg_ch1*att_factor_A, bg.qdc_lg_ch1*att_factor_A, plot=plot) 
    # del Co60

    # ::::::::::::::::::::::::::::::::::::::::::::::::::::
    #     LOAD AND FIT COMPTON EDGES Th232: NE213A
    # ::::::::::::::::::::::::::::::::::::::::::::::::::::
    # Th232 = propd.load_parquet_merge(path, Th232_runs, keep_col=include_col, full=False)
    Th232_2615_fit = gefNE213A.Th232_2615_LG(Th232.qdc_lg_ch1*att_factor_A, bg.qdc_lg_ch1*att_factor_A, plot=plot)
    # del Th232

    # ::::::::::::::::::::::::::::::::::::::::::::::::::::
    #     LOAD AND FIT COMPTON EDGES PuBe: NE213A
    # ::::::::::::::::::::::::::::::::::::::::::::::::::::
    # PuBe = propd.load_parquet_merge(path, PuBe_runs, keep_col=include_col, full=False)
    PuBe_4439_fit = gefNE213A.PuBe_4439_LG(PuBe.qdc_lg_ch1*att_factor_A, bg.qdc_lg_ch1*att_factor_A, plot=plot)
    # PuBe_6130_fit = gefNE213A.PuBe_6130_LG(PuBe.qdc_lg_ch1*att_factor_A, bg.qdc_lg_ch1*att_factor_A, plot=plot)
    # del PuBe

    # ::::::::::::::::::::::::::::::::::::::::::::::::::::
    #     LOAD AND FIT COMPTON EDGES PuBe: NE213A
    # ::::::::::::::::::::::::::::::::::::::::::::::::::::
    # AmBe = propd.load_parquet_merge(path, AmBe_runs, keep_col=include_col, full=False)
    AmBe_4439_fit = gefNE213A.AmBe_4439_LG(AmBe.qdc_lg_ch1*att_factor_A, bg.qdc_lg_ch1*att_factor_A, plot=plot)
    # AmBe_6130_fit = gefNE213A.AmBe_6130_LG(AmBe.qdc_lg_ch1*att_factor_A, bg.qdc_lg_ch1*att_factor_A, plot=plot)
    # del AmBe

    # ::::::::::::::::::::::::::::::::::::::::::::::::::::
    #     LOAD AND FIT COMPTON EDGES Cosmic muons: NE213A
    # ::::::::::::::::::::::::::::::::::::::::::::::::::::
    # cosmic = propd.load_parquet_merge(path, cosmic_runs, keep_col=['qdc_lg_ch1'], full=False)
    # cosmic_fit = gefNE213A.cosmic_LG(cosmic['qdc_lg_ch1']*att_factor_cosmic_A, plot=plot)
    # del cosmic

    # ::::::::::::::::::::::::::::::::::::::::::::::::::::
    #     COMPILE GAUSSIAN FITTING VALUES FOR ALL SOURCES
    # ::::::::::::::::::::::::::::::::::::::::::::::::::::
   
    res_gauss = {
        '511_hist':Na22_511_fit['hist'],
        '511_bins':Na22_511_fit['bins'],
        '511_CE_89_x':Na22_511_fit['CE_89_x'],
        '511_CE_89_x_error':Na22_511_fit['CE_89_x_error'],
        '511_CE_89_y':Na22_511_fit['CE_89_y'],
        '511_CE_89_y_error':Na22_511_fit['CE_89_y_error'],
        '511_CE_50_x':Na22_511_fit['CE_50_x'],
        '511_CE_50_x_error':Na22_511_fit['CE_50_x_error'],
        '511_CE_50_y':Na22_511_fit['CE_50_y'],
        '511_CE_50_y_error':Na22_511_fit['CE_50_y_error'],
        '511_gauss_const':Na22_511_fit['gauss_const'],
        '511_gauss_const_error':Na22_511_fit['gauss_const_error'],
        '511_gauss_mean':Na22_511_fit['gauss_mean'],
        '511_gauss_mean_error':Na22_511_fit['gauss_mean_error'],
        '511_gauss_std':Na22_511_fit['gauss_std'],
        '511_gauss_std_error':Na22_511_fit['gauss_std_error'],
        '511_Ef':Na22_511_fit['Ef'],
        '511_recoil_max':Na22_511_fit['recoil_max'],
        
        '1275_hist':Na22_1275_fit['hist'],
        '1275_bins':Na22_1275_fit['bins'],
        '1275_CE_89_x':Na22_1275_fit['CE_89_x'],
        '1275_CE_89_x_error':Na22_1275_fit['CE_89_x_error'],
        '1275_CE_89_y':Na22_1275_fit['CE_89_y'],
        '1275_CE_89_y_error':Na22_1275_fit['CE_89_y_error'],
        '1275_CE_50_x':Na22_1275_fit['CE_50_x'],
        '1275_CE_50_x_error':Na22_1275_fit['CE_50_x_error'],
        '1275_CE_50_y':Na22_1275_fit['CE_50_y'],
        '1275_CE_50_y_error':Na22_1275_fit['CE_50_y_error'],
        '1275_gauss_const':Na22_1275_fit['gauss_const'],
        '1275_gauss_const_error':Na22_1275_fit['gauss_const_error'],
        '1275_gauss_mean':Na22_1275_fit['gauss_mean'],
        '1275_gauss_mean_error':Na22_1275_fit['gauss_mean_error'],
        '1275_gauss_std':Na22_1275_fit['gauss_std'],
        '1275_gauss_std_error':Na22_1275_fit['gauss_std_error'],
        '1275_Ef':Na22_1275_fit['Ef'],
        '1275_recoil_max':Na22_1275_fit['recoil_max'],
    
        '662_hist':Cs137_662_fit['hist'],
        '662_bins':Cs137_662_fit['bins'],
        '662_CE_89_x':Cs137_662_fit['CE_89_x'],
        '662_CE_89_x_error':Cs137_662_fit['CE_89_x_error'],
        '662_CE_89_y':Cs137_662_fit['CE_89_y'],
        '662_CE_89_y_error':Cs137_662_fit['CE_89_y_error'],
        '662_CE_50_x':Cs137_662_fit['CE_50_x'],
        '662_CE_50_x_error':Cs137_662_fit['CE_50_x_error'],
        '662_CE_50_y':Cs137_662_fit['CE_50_y'],
        '662_CE_50_y_error':Cs137_662_fit['CE_50_y_error'],
        '662_gauss_const':Cs137_662_fit['gauss_const'],
        '662_gauss_const_error':Cs137_662_fit['gauss_const_error'],
        '662_gauss_mean':Cs137_662_fit['gauss_mean'],
        '662_gauss_mean_error':Cs137_662_fit['gauss_mean_error'],
        '662_gauss_std':Cs137_662_fit['gauss_std'],
        '662_gauss_std_error':Cs137_662_fit['gauss_std_error'],
        '662_Ef':Cs137_662_fit['Ef'],
        '662_recoil_max':Cs137_662_fit['recoil_max'],

        # '1332_hist':Co60_1332_fit['hist'],
        # '1332_bins':Co60_1332_fit['bins'],
        # '1332_CE_89_x':Co60_1332_fit['CE_89_x'],
        # '1332_CE_89_x_error':Co60_1332_fit['CE_89_x_error'],
        # '1332_CE_89_y':Co60_1332_fit['CE_89_y'],
        # '1332_CE_89_y_error':Co60_1332_fit['CE_89_y_error'],
        # '1332_CE_50_x':Co60_1332_fit['CE_50_x'],
        # '1332_CE_50_x_error':Co60_1332_fit['CE_50_x_error'],
        # '1332_CE_50_y':Co60_1332_fit['CE_50_y'],
        # '1332_CE_50_y_error':Co60_1332_fit['CE_50_y_error'],
        # '1332_gauss_const':Co60_1332_fit['gauss_const'],
        # '1332_gauss_const_error':Co60_1332_fit['gauss_const_error'],
        # '1332_gauss_mean':Co60_1332_fit['gauss_mean'],
        # '1332_gauss_mean_error':Co60_1332_fit['gauss_mean_error'],
        # '1332_gauss_std':Co60_1332_fit['gauss_std'],
        # '1332_gauss_std_error':Co60_1332_fit['gauss_std_error'],
        # '1332_Ef':Co60_1332_fit['Ef'],
        # '1332_recoil_max':Co60_1332_fit['recoil_max'],

        '2615_hist':Th232_2615_fit['hist'],
        '2615_bins':Th232_2615_fit['bins'],
        '2615_CE_89_x':Th232_2615_fit['CE_89_x'],
        '2615_CE_89_x_error':Th232_2615_fit['CE_89_x_error'],
        '2615_CE_89_y':Th232_2615_fit['CE_89_y'],
        '2615_CE_89_y_error':Th232_2615_fit['CE_89_y_error'],
        '2615_CE_50_x':Th232_2615_fit['CE_50_x'],
        '2615_CE_50_x_error':Th232_2615_fit['CE_50_x_error'],
        '2615_CE_50_y':Th232_2615_fit['CE_50_y'],
        '2615_CE_50_y_error':Th232_2615_fit['CE_50_y_error'],
        '2615_gauss_const':Th232_2615_fit['gauss_const'],
        '2615_gauss_const_error':Th232_2615_fit['gauss_const_error'],
        '2615_gauss_mean':Th232_2615_fit['gauss_mean'],
        '2615_gauss_mean_error':Th232_2615_fit['gauss_mean_error'],
        '2615_gauss_std':Th232_2615_fit['gauss_std'],
        '2615_gauss_std_error':Th232_2615_fit['gauss_std_error'],
        '2615_Ef':Th232_2615_fit['Ef'],
        '2615_recoil_max':Th232_2615_fit['recoil_max'],
        
        'PuBe_4439_hist':PuBe_4439_fit['hist'],
        'PuBe_4439_bins':PuBe_4439_fit['bins'],
        'PuBe_4439_CE_89_x':PuBe_4439_fit['CE_89_x'],
        'PuBe_4439_CE_89_x_error':PuBe_4439_fit['CE_89_x_error'],
        'PuBe_4439_CE_89_y':PuBe_4439_fit['CE_89_y'],
        'PuBe_4439_CE_89_y_error':PuBe_4439_fit['CE_89_y_error'],
        'PuBe_4439_CE_50_x':PuBe_4439_fit['CE_50_x'],
        'PuBe_4439_CE_50_x_error':PuBe_4439_fit['CE_50_x_error'],
        'PuBe_4439_CE_50_y':PuBe_4439_fit['CE_50_y'],
        'PuBe_4439_CE_50_y_error':PuBe_4439_fit['CE_50_y_error'],
        'PuBe_4439_gauss_const':PuBe_4439_fit['gauss_const'],
        'PuBe_4439_gauss_const_error':PuBe_4439_fit['gauss_const_error'],
        'PuBe_4439_gauss_mean':PuBe_4439_fit['gauss_mean'],
        'PuBe_4439_gauss_mean_error':PuBe_4439_fit['gauss_mean_error'],
        'PuBe_4439_gauss_std':PuBe_4439_fit['gauss_std'],
        'PuBe_4439_gauss_std_error':PuBe_4439_fit['gauss_std_error'],
        'PuBe_4439_Ef':PuBe_4439_fit['Ef'],
        'PuBe_4439_recoil_max':PuBe_4439_fit['recoil_max'],
        
        'AmBe_4439_hist':AmBe_4439_fit['hist'],
        'AmBe_4439_bins':AmBe_4439_fit['bins'],
        'AmBe_4439_CE_89_x':AmBe_4439_fit['CE_89_x'],
        'AmBe_4439_CE_89_x_error':AmBe_4439_fit['CE_89_x_error'],
        'AmBe_4439_CE_89_y':AmBe_4439_fit['CE_89_y'],
        'AmBe_4439_CE_89_y_error':AmBe_4439_fit['CE_89_y_error'],
        'AmBe_4439_CE_50_x':AmBe_4439_fit['CE_50_x'],
        'AmBe_4439_CE_50_x_error':AmBe_4439_fit['CE_50_x_error'],
        'AmBe_4439_CE_50_y':AmBe_4439_fit['CE_50_y'],
        'AmBe_4439_CE_50_y_error':AmBe_4439_fit['CE_50_y_error'],
        'AmBe_4439_gauss_const':AmBe_4439_fit['gauss_const'],
        'AmBe_4439_gauss_const_error':AmBe_4439_fit['gauss_const_error'],
        'AmBe_4439_gauss_mean':AmBe_4439_fit['gauss_mean'],
        'AmBe_4439_gauss_mean_error':AmBe_4439_fit['gauss_mean_error'],
        'AmBe_4439_gauss_std':AmBe_4439_fit['gauss_std'],
        'AmBe_4439_gauss_std_error':AmBe_4439_fit['gauss_std_error'],
        'AmBe_4439_Ef':AmBe_4439_fit['Ef'],
        'AmBe_4439_recoil_max':AmBe_4439_fit['recoil_max']

        # 'cosmic_gauss_const':cosmic_fit['gauss_const'],
        # 'cosmic_gauss_const_error':cosmic_fit['gauss_const_error'],
        # 'cosmic_gauss_mean':cosmic_fit['gauss_mean'],
        # 'cosmic_gauss_mean_error':cosmic_fit['gauss_mean_error'],
        # 'cosmic_gauss_std':cosmic_fit['gauss_std'],
        # 'cosmic_gauss_std_error':cosmic_fit['gauss_std_error'],
        # 'cosmic_chi2reduced':cosmic_fit['chi2reduced'],
        # 'cosmic_recoil_max':cosmic_fit['recoil_max']
        }

    # ::::::::::::::::::::::::::::::::::::::::::::::::::::
    #     SAVE CALIBRATION DATA TO DISK: NE213
    # ::::::::::::::::::::::::::::::::::::::::::::::::::::
    #save calibration data
    E_data = np.array([ 
        Na22_511_fit['recoil_max'], #Location of feature in MeV
        Cs137_662_fit['recoil_max'], 
        Na22_1275_fit['recoil_max'], 
        # Co60_1332_fit['recoil_max'], 
        Th232_2615_fit['recoil_max'],
        PuBe_4439_fit['recoil_max']]) 

    QDC_data_89 = np.array([
        Na22_511_fit['CE_89_x'],       
        Cs137_662_fit['CE_89_x'],        
        Na22_1275_fit['CE_89_x'],          
        # Co60_1332_fit['CE_89_x'],
        Th232_2615_fit['CE_89_x'],
        PuBe_4439_fit['CE_89_x']])

    QDC_err_89 = np.array([
        Na22_511_fit['CE_89_x_error'],    
        Cs137_662_fit['CE_89_x_error'],  
        Na22_1275_fit['CE_89_x_error'],    
        # Co60_1332_fit['CE_89_x_error'],    
        Th232_2615_fit['CE_89_x_error'],
        PuBe_4439_fit['CE_89_x_error']])

    QDC_data_50 = np.array([ 
        Na22_511_fit['CE_50_x'],       
        Cs137_662_fit['CE_50_x'],        
        Na22_1275_fit['CE_50_x'],          
        # Co60_1332_fit['CE_50_x'],
        Th232_2615_fit['CE_50_x'],
        PuBe_4439_fit['CE_50_x']])

    QDC_err_50 = np.array([
        Na22_511_fit['CE_50_x_error'], 
        Cs137_662_fit['CE_50_x_error'],  
        Na22_1275_fit['CE_50_x_error'],    
        # Co60_1332_fit['CE_50_x_error'],    
        Th232_2615_fit['CE_50_x_error'],
        PuBe_4439_fit['CE_50_x_error']])

    #save to disk
    # np.save('calibration_NE213A_lg_qdc_89.npy', (E_data, QDC_data_89, QDC_err_89))
    # np.save('calibration_NE213A_lg_qdc_50.npy', (E_data, QDC_data_50, QDC_err_50))

    res_89_energy = prodata.linearEnergyFit(E_data, QDC_data_89, QDC_err_89, plot=False)
    res_50_energy = prodata.linearEnergyFit(E_data*1.04, QDC_data_50, QDC_err_50, plot=False)

    #add cosmic muon to return (but keeping it form being used for the linear fit above)
    # E_data =        np.append(E_data,       cosmic_fit['recoil_max'])
    # QDC_data_89 =   np.append(QDC_data_89,  cosmic_fit['gauss_mean'])
    # QDC_err_89 =    np.append(QDC_err_89,   cosmic_fit['gauss_mean_error'])
    # QDC_data_50 =   np.append(QDC_data_50,  cosmic_fit['gauss_mean'])
    # QDC_err_50 =    np.append(QDC_err_50,   cosmic_fit['gauss_mean_error'])
    
    res_89_energy['E_data'] = E_data
    res_89_energy['QDC_data_89'] = QDC_data_89
    res_89_energy['QDC_err_89'] = QDC_err_89
    res_50_energy['E_data'] = E_data*1.04
    res_50_energy['QDC_data_50'] = QDC_data_50
    res_50_energy['QDC_err_50'] = QDC_err_50

    return res_gauss, res_89_energy, res_50_energy