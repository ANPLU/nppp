#!/usr/bin/env python3
from scipy.optimize.minpack import curve_fit


def gaussP2Func(x, gauss_cont, gauss_mean, gauss_sigma, a):
    """
    Gauss func + second degree oly.
    """
    return gauss_cont * np.exp(-(x - gauss_mean)**2 / (2 * gauss_sigma**2)) + a*x*x

plot=True


start = 1000
stop = 7000
counts, bin_edges = np.histogram(df_Na22_511_sim_pen.optPhotonSumComptonQE, bins = 30, range=[0, stop*1.2])
bin_centers = prodata.getBinCenters(bin_edges)
Ef = 0.510999
sim_res_Na22_511  =  prosim.calibrationFitBinned(counts, bin_centers, start, stop, Ef=Ef, plot=plot, compton=True)

######################################
# startNew = 1000
# stopNew = 7000
# # Get range to fit from input parameters.
# y = []
# x = []
# for i in range(len(bin_centers)):
#     if bin_centers[i] >= startNew and bin_centers[i] <= stopNew:
#         x.append(bin_centers[i]) #Get x-values in range to fit
#         y.append(counts[i]) #Get y-values in range to fit

# numSig = 2
# popt, pcov = curve_fit(gaussP2Func, x, y, p0=[2500, 3100, 1000,1])
# plt.plot(xRange, gaussP2Func(xRange, popt[0], popt[1], popt[2], popt[3]),label='Fit')
# plt.plot(bin_centers, counts,label='Data')
# plt.plot(x, y ,label='DataSelection')
# plt.vlines(x=sim_res_Na22_511['gauss_mean']-numSig*sim_res_Na22_511['gauss_std'], ymin=0, ymax=3500, color='black')
# plt.vlines(x=sim_res_Na22_511['gauss_mean']+numSig*sim_res_Na22_511['gauss_std'], ymin=0, ymax=3500, color='black')
# avg = np.mean(df_Na22_511_sim_pen.query(f'optPhotonSumComptonQE>{sim_res_Na22_511["gauss_mean"]-numSig*sim_res_Na22_511["gauss_std"]} and optPhotonSumComptonQE<{sim_res_Na22_511["gauss_mean"]+numSig*sim_res_Na22_511["gauss_std"]} and optPhotonSumComptonQE>0').optPhotonSumComptonQE)
# ratio = (popt[1]-avg)/avg
# plt.title(f'percent diff: {round(ratio*100,3)}%, a= {popt[3]}')
# plt.xlim([-1000,7500])
# plt.ylim([0,4000])
# plt.legend()
# plt.show()


# avg = np.mean(df_Na22_511_sim_pen.query(f'optPhotonSumComptonQE>{sim_res_Na22_511["gauss_mean"]-numSig*sim_res_Na22_511["gauss_std"]} and optPhotonSumComptonQE<{sim_res_Na22_511["gauss_mean"]+numSig*sim_res_Na22_511["gauss_std"]} and optPhotonSumComptonQE>0').optPhotonSumComptonQE)
# avg_err = np.sqrt(avg)
# print(f'E_CE = 340 keV | avg = {round(avg,3)} | avg_err = {round(avg_err,3)} | mu = {round(popt[1],3)} | mu_err = {round(sim_res_Na22_511["gauss_mean_error"],3)}')
# print('-----------------------------------------------------------------------------------------')
# ratio = (popt[1]-avg)/avg

start = 1000#4100
stop = 8000
counts, bin_edges = np.histogram(df_Cs137_662_sim_pen.optPhotonSumComptonQE, bins = 50, range=[0, stop*1.2])
bin_centers = prodata.getBinCenters(bin_edges)
Ef = 0.66166
sim_res_Cs137_662  =  prosim.calibrationFitBinned(counts, bin_centers, start, stop, Ef=Ef, plot=plot, compton=True)

start = 4000#9700
stop = 16000
counts, bin_edges = np.histogram(df_Na22_1275_sim_pen.optPhotonSumComptonQE, bins = 50, range=[0, stop*1.2])
bin_centers = prodata.getBinCenters(bin_edges)
Ef = 1.2745
sim_res_Na22_1275 = prosim.calibrationFitBinned(counts, bin_centers, start, stop, Ef=Ef, plot=plot, compton=True)

start = 21000
stop = 35000
counts, bin_edges = np.histogram(df_2615_sim_pen.optPhotonSumComptonQE, bins = 64, range=[0, stop*1.2])
bin_centers = prodata.getBinCenters(bin_edges)
Ef = 2.614533
sim_res_PuBe_2615 = prosim.calibrationFitBinned(counts, bin_centers, start, stop, Ef=Ef, plot=plot, compton=True)

start = 35000
stop = 60000
counts, bin_edges = np.histogram(df_4439_sim_pen.optPhotonSumComptonQE, bins = 50, range=[0, stop*1.2])
bin_centers = prodata.getBinCenters(bin_edges)
Ef = 4.439
sim_res_PuBe_4439 = prosim.calibrationFitBinned(counts, bin_centers, start, stop, Ef=Ef, plot=plot, compton=True)





xRange = np.arange(-10000,100000)
numSig = 3.0
#### Na22 511 keV ####
plt.figure(figsize=(12,7.5))
numBins = 30 
binRange = [0, 7000*1.2] 
plt.subplot(2,3,1)
plt.title('Na22 340')
plt.hist(df_Na22_511_sim_pen.optPhotonSumComptonQE, bins=numBins, range=binRange, label='data', histtype='step')
plt.plot(xRange, promath.gaussFunc(xRange, sim_res_Na22_511['gauss_const'], sim_res_Na22_511['gauss_mean'],sim_res_Na22_511['gauss_std']), label='gaussFit', color='red', lw=2)

avg = np.mean(df_Na22_511_sim_pen.query(f'optPhotonSumComptonQE>{sim_res_Na22_511["gauss_mean"]-numSig*sim_res_Na22_511["gauss_std"]} and optPhotonSumComptonQE<{sim_res_Na22_511["gauss_mean"]+numSig*sim_res_Na22_511["gauss_std"]} and optPhotonSumComptonQE>0').optPhotonSumComptonQE)
avg_err = np.sqrt(avg)
print(f'E_CE = 340 keV | avg = {round(avg,3)} | avg_err = {round(avg_err,3)} | mu = {round(sim_res_Na22_511["gauss_mean"],3)} | mu_err = {round(sim_res_Na22_511["gauss_mean_error"],3)}')
print('-----------------------------------------------------------------------------------------')
ratio = (sim_res_Na22_511["gauss_mean"]-avg)/avg
plt.vlines(x=sim_res_Na22_511['gauss_mean']-numSig*sim_res_Na22_511['gauss_std'], ymin=0, ymax=3500, color='black', label=f"+-{numSig} sigma")
plt.vlines(x=sim_res_Na22_511['gauss_mean']+numSig*sim_res_Na22_511['gauss_std'], ymin=0, ymax=3500, color='black', label=f'ratio ={round(ratio*100,2)}%')
plt.xlim([-2000,8500])
plt.legend()
# plt.show()

#### Cs137 662 keV ####
numBins = 50 
binRange = [0, 8000*1.2] 
plt.subplot(2,3,2)
plt.title('Cs137 480')
plt.hist(df_Cs137_662_sim_pen.optPhotonSumComptonQE, bins=numBins, range=binRange, label='data', histtype='step')
plt.plot(xRange, promath.gaussFunc(xRange, sim_res_Cs137_662['gauss_const'], sim_res_Cs137_662['gauss_mean'],sim_res_Cs137_662['gauss_std']), label='gaussFit', color='red', lw=2)

avg = np.mean(df_Cs137_662_sim_pen.query(f'optPhotonSumComptonQE>{sim_res_Cs137_662["gauss_mean"]-numSig*sim_res_Cs137_662["gauss_std"]} and optPhotonSumComptonQE<{sim_res_Cs137_662["gauss_mean"]+numSig*sim_res_Cs137_662["gauss_std"]} and optPhotonSumComptonQE>0').optPhotonSumComptonQE)
avg_err = np.sqrt(avg)
print(f'E_CE = 480 keV | avg = {round(avg,3)} | avg_err = {round(avg_err,3)} | mu = {round(sim_res_Cs137_662["gauss_mean"],3)} | mu_err = {round(sim_res_Cs137_662["gauss_mean_error"],3)}')
print('-----------------------------------------------------------------------------------------')
ratio = (sim_res_Cs137_662["gauss_mean"]-avg)/avg
plt.vlines(x=sim_res_Cs137_662['gauss_mean']-numSig*sim_res_Cs137_662['gauss_std'], ymin=0, ymax=800, color='black', label=f"+-{numSig} sigma")
plt.vlines(x=sim_res_Cs137_662['gauss_mean']+numSig*sim_res_Cs137_662['gauss_std'], ymin=0, ymax=800, color='black', label=f'ratio ={round(ratio*100,2)}%')
plt.xlim([-500,10000])
plt.legend()
# plt.show()

#### Na22 1275 keV ####
numBins = 50 
binRange = [0, 16000*1.2] 
plt.subplot(2,3,3)
plt.title('Na22 1060')
plt.hist(df_Na22_1275_sim_pen.optPhotonSumComptonQE, bins=numBins, range=binRange, label='data', histtype='step')
plt.plot(xRange, promath.gaussFunc(xRange, sim_res_Na22_1275['gauss_const'], sim_res_Na22_1275['gauss_mean'],sim_res_Na22_1275['gauss_std']), label='gaussFit', color='red', lw=2)

avg = np.mean(df_Na22_1275_sim_pen.query(f'optPhotonSumComptonQE>{sim_res_Na22_1275["gauss_mean"]-numSig*sim_res_Na22_1275["gauss_std"]} and optPhotonSumComptonQE<{sim_res_Na22_1275["gauss_mean"]+numSig*sim_res_Na22_1275["gauss_std"]}').optPhotonSumComptonQE)
avg_err = np.sqrt(avg)
print(f'E_CE = 1060 keV | avg = {round(avg,3)} | avg_err = {round(avg_err,3)} | mu = {round(sim_res_Na22_1275["gauss_mean"],3)} | mu_err = {round(sim_res_Na22_1275["gauss_mean_error"],3)}')
print('-----------------------------------------------------------------------------------------')
ratio = (sim_res_Na22_1275["gauss_mean"]-avg)/avg
plt.vlines(x=sim_res_Na22_1275['gauss_mean']-numSig*sim_res_Na22_1275['gauss_std'], ymin=0, ymax=1000, color='black', label=f"+-{numSig} sigma")
plt.vlines(x=sim_res_Na22_1275['gauss_mean']+numSig*sim_res_Na22_1275['gauss_std'], ymin=0, ymax=1000, color='black', label=f'ratio ={round(ratio*100,2)}%')
plt.xlim([-500,19000])
plt.legend()
# plt.show()

#### Th232 2380 keV ####
numBins = 64 
binRange = [0, 35000*1.2] 
plt.subplot(2,3,4)
plt.title('Th232 2380')
plt.hist(df_2615_sim_pen.optPhotonSumComptonQE, bins=numBins, range=binRange, label='data', histtype='step')
plt.plot(xRange, promath.gaussFunc(xRange, sim_res_PuBe_2615['gauss_const'], sim_res_PuBe_2615['gauss_mean'],sim_res_PuBe_2615['gauss_std']), label='gaussFit', color='red', lw=2)

avg = np.mean(df_2615_sim_pen.query(f'optPhotonSumComptonQE>{sim_res_PuBe_2615["gauss_mean"]-numSig*sim_res_PuBe_2615["gauss_std"]} and optPhotonSumComptonQE<{sim_res_PuBe_2615["gauss_mean"]+numSig*sim_res_PuBe_2615["gauss_std"]}').optPhotonSumComptonQE)
avg_err = np.sqrt(avg)
print(f'E_CE = 2380 keV | avg = {round(avg,3)} | avg_err = {round(avg_err,3)} | mu = {round(sim_res_PuBe_2615["gauss_mean"],3)} | mu_err = {round(sim_res_PuBe_2615["gauss_mean_error"],3)}')
print('-----------------------------------------------------------------------------------------')
ratio = (sim_res_PuBe_2615["gauss_mean"]-avg)/avg
plt.vlines(x=sim_res_PuBe_2615['gauss_mean']-numSig*sim_res_PuBe_2615['gauss_std'], ymin=0, ymax=450, color='black', label=f"+-{numSig} sigma")
plt.vlines(x=sim_res_PuBe_2615['gauss_mean']+numSig*sim_res_PuBe_2615['gauss_std'], ymin=0, ymax=450, color='black', label=f'ratio ={round(ratio*100,2)}%')
plt.xlim([5000,40000])
plt.ylim([0, 500])
plt.legend()
# plt.show()

#### AmBe 4200 keV ####
numBins = 50 
binRange = [0, 60000*1.2] 
plt.subplot(2,3,5)
plt.title('AmBe 4200')
plt.hist(df_4439_sim_pen.optPhotonSumComptonQE, bins=numBins, range=binRange, label='data', histtype='step')
plt.plot(xRange, promath.gaussFunc(xRange, sim_res_PuBe_4439['gauss_const'], sim_res_PuBe_4439['gauss_mean'],sim_res_PuBe_4439['gauss_std']), label='gaussFit', color='red', lw=2)

avg = np.mean(df_4439_sim_pen.query(f'optPhotonSumComptonQE>{sim_res_PuBe_4439["gauss_mean"]-numSig*sim_res_PuBe_4439["gauss_std"]} and optPhotonSumComptonQE<{sim_res_PuBe_4439["gauss_mean"]+numSig*sim_res_PuBe_4439["gauss_std"]}').optPhotonSumComptonQE)
avg_err = np.sqrt(avg)
print(f'E_CE = 4200 keV | avg = {round(avg,3)} | avg_err = {round(avg_err,3)} | mu = {round(sim_res_PuBe_4439["gauss_mean"],3)} | mu_err = {round(sim_res_PuBe_4439["gauss_mean_error"],3)}')
print('-----------------------------------------------------------------------------------------')
ratio = (sim_res_PuBe_4439["gauss_mean"]-avg)/avg
plt.vlines(x=sim_res_PuBe_4439['gauss_mean']-numSig*sim_res_PuBe_4439['gauss_std'], ymin=0, ymax=250, color='black', label=f"+-{numSig} sigma")
plt.vlines(x=sim_res_PuBe_4439['gauss_mean']+numSig*sim_res_PuBe_4439['gauss_std'], ymin=0, ymax=250, color='black', label=f'ratio ={round(ratio*100,2)}%')
plt.xlim([5000,75000])
plt.ylim([0, 300])
plt.legend()
plt.show()
