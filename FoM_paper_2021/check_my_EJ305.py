#!/usr/bin/env python3
from re import I
import sys
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
from numpy.core.arrayprint import SubArrayFormat
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit

# from matplotlib.colors import LinearSegmentedColormap


# IMPORT FROM LIBRARY DIRECTORY
sys.path.insert(0, "../library/")
sys.path.insert(0, "../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "../")
sys.path.insert(0, "data_analysis/")

import processing_math as promath
import processing_data as prodata
import processing_pd as propd
import figure_style as fs

########################################
######## DETECTOR SETTINGS #############
########################################


distance = 0.959
att_factor_11dB = 3.55
att_factor_12dB = 3.98
att_factor_16_5dB = 6.68
gTOFRange = [0.5e-9, 6e-9]
randTOFRange = [-710e-9,-10e-9]
nTOFRange = [28.1e-9, 80e-9]#[28.1e-9, 60e-9]

nLength = np.abs(nTOFRange[1]-nTOFRange[0])
gLength = np.abs(gTOFRange[1]-gTOFRange[0])
randLength = np.abs(randTOFRange[1]-randTOFRange[0])

path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop

detector = "EJ305"
run1 = 3038
numRuns = 201
runListEJ305 = np.arange(run1, run1+numRuns)

detector = "NE213A"
run1 = 2121
numRuns = 140
runListNE213A = np.arange(run1, run1+numRuns)

YAP3_ratio = np.arange(0)
YAP4_ratio = np.arange(0)
YAP5_ratio = np.arange(0)

for i in range(50):
    print('-------------------------------------------------')
    print(f'Current runnumber = {i}')
    
    df_EJ305 =  propd.load_parquet_merge(path, [runListEJ305[i]],  keep_col=[ 'amplitude_ch3', 'amplitude_ch4', 'amplitude_ch5', 'tof_ch3', 'tof_ch4', 'tof_ch5'], full=False)
    df_NE213A = propd.load_parquet_merge(path, [runListNE213A[i]], keep_col=[ 'amplitude_ch3', 'amplitude_ch4', 'amplitude_ch5', 'tof_ch3', 'tof_ch4', 'tof_ch5'], full=False)
    EJ305query3 = '84<tof_ch3<130 and amplitude_ch3>20'
    EJ305query4 = '84<tof_ch4<130 and amplitude_ch4>20'
    EJ305query5 = '84<tof_ch5<130 and amplitude_ch5>20'
    
    NE213Aquery3 = '89<tof_ch3<133 and amplitude_ch3>20'
    NE213Aquery4 = '89<tof_ch4<135 and amplitude_ch4>20'
    NE213Aquery5 = '89<tof_ch5<135 and amplitude_ch5>20'

    print('(NE213A/EJ305)')
    print(f'YAP3: ({len(df_NE213A.query(NE213Aquery3))}/{len(df_EJ305.query(EJ305query3))}) = {round(len(df_NE213A.query(NE213Aquery3))/len(df_EJ305.query(EJ305query3)),4)*100}%')
    print(f'YAP4: ({len(df_NE213A.query(NE213Aquery4))}/{len(df_EJ305.query(EJ305query4))}) = {round(len(df_NE213A.query(NE213Aquery4))/len(df_EJ305.query(EJ305query4)),4)*100}%')
    print(f'YAP5: ({len(df_NE213A.query(NE213Aquery5))}/{len(df_EJ305.query(EJ305query5))}) = {round(len(df_NE213A.query(NE213Aquery5))/len(df_EJ305.query(EJ305query5)),4)*100}%')
    YAP3_ratio = np.append(YAP3_ratio, len(df_NE213A.query(NE213Aquery3))/len(df_EJ305.query(EJ305query3)))
    YAP4_ratio = np.append(YAP4_ratio, len(df_NE213A.query(NE213Aquery4))/len(df_EJ305.query(EJ305query4)))
    YAP5_ratio = np.append(YAP5_ratio, len(df_NE213A.query(NE213Aquery5))/len(df_EJ305.query(EJ305query5)))


plt.scatter(np.arange(50), YAP3_ratio*100, color='blue')
plt.scatter(np.arange(50), YAP4_ratio*100, color='green')
plt.scatter(np.arange(50), YAP5_ratio*100, color='red')
plt.title('NE213A / EJ305 number of events')
plt.xlabel('runs')
plt.ylabel('ratio [\%]')
plt.show()