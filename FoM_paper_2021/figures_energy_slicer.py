#!/usr/bin/env python3
from re import I
import sys
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
from numpy.core.arrayprint import SubArrayFormat
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit

# IMPORT FROM LIBRARY DIRECTORY
sys.path.insert(0, "../library/")
sys.path.insert(0, "../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "../")
sys.path.insert(0, "data_analysis/")

import processing_math as promath
import processing_data as prodata
import processing_pd as propd
import processing_simulation as prosim


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#    DEFINE PARAMTERS FOR SLICE
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
gTOFRange = [2e-9, 7e-9] #Define range of gamma-flash
randTOFRange = [-800e-9,-10e-9] #Define range of random events

gLength = np.abs(gTOFRange[1]-gTOFRange[0]) #Calculate the lenght of cut for gammas
randLength = np.abs(randTOFRange[1]-randTOFRange[0]) #Calculate lenght of cut for random events
nLength = np.empty(0)
distance = 0.959 #distance between source and center of detector

#CALCULATE HOW LONG THE TOF SLICE IN TIME WAS [ns]
dE = 0.25
for i in np.arange(1.5, 6.0, 0.5):
    # dt = prodata.EnergytoTOF(round(i-dE, 2), distance) - prodata.EnergytoTOF(round(i+dE, 2), distance)
    nLength = np.append(nLength, prodata.EnergytoTOF(round(i-dE, 2), distance) - prodata.EnergytoTOF(round(i+dE, 2), distance))
    print(f'dE = {round(i-dE, 2)}-{round(i+dE, 2)} MeV -> center = {(round(i-dE, 2)+round(i+dE, 2))/2} -> dt = {round( (prodata.EnergytoTOF(round(i-dE, 2), distance)-prodata.EnergytoTOF(round(i+dE, 2), distance))*1e9, 4)} ns')


# # ###LOADING SLICE DATA ####
EJ305_PS_YAP2,  EJ305_PS_YAP3,  EJ305_PS_YAP4,  EJ305_PS_YAP5 =     prodata.sliceLoad("data/NE213A/sliced/PS/", 15, 'PS')
# EJ331_PS_YAP2,  EJ331_PS_YAP3,  EJ331_PS_YAP4,  EJ331_PS_YAP5 =     prodata.sliceLoad("data/EJ331/sliced/PS/", 11, 'PS')
# NE213A_PS_YAP2, NE213A_PS_YAP3, NE213A_PS_YAP4, NE213A_PS_YAP5 =    prodata.sliceLoad("data/NE2132A/sliced/PS/", 11, 'PS')

# EJ305_LG_YAP2,  EJ305_PS_YAP3,  EJ305_PS_YAP4,  EJ305_PS_YAP5 =     prodata.sliceLoad("data/EJ305/sliced/PS/", 11, 'PS')
# EJ331_PS_YAP2,  EJ331_PS_YAP3,  EJ331_PS_YAP4,  EJ331_PS_YAP5 =     prodata.sliceLoad("data/EJ331/sliced/PS/", 11, 'PS')
# NE213A_PS_YAP2, NE213A_PS_YAP3, NE213A_PS_YAP4, NE213A_PS_YAP5 =    prodata.sliceLoad("data/NE2132A/sliced/PS/", 11, 'PS')

# EJ305_PS_YAP2 = EJ305_PS_YAP2.reset_index(drop=True)
# EJ305_PS_YAP3 = EJ305_PS_YAP3.reset_index(drop=True)
# EJ305_PS_YAP4 = EJ305_PS_YAP4.reset_index(drop=True)
# EJ305_PS_YAP5 = EJ305_PS_YAP5.reset_index(drop=True)

# EJ331_PS_YAP2 = EJ331_PS_YAP2.reset_index(drop=True)
# EJ331_PS_YAP3 = EJ331_PS_YAP3.reset_index(drop=True)
# EJ331_PS_YAP4 = EJ331_PS_YAP4.reset_index(drop=True)
# EJ331_PS_YAP5 = EJ331_PS_YAP5.reset_index(drop=True)

# NE213A_PS_YAP2 = NE213A_PS_YAP2.reset_index(drop=True)
# NE213A_PS_YAP3 = NE213A_PS_YAP3.reset_index(drop=True)
# NE213A_PS_YAP4 = NE213A_PS_YAP4.reset_index(drop=True)
# NE213A_PS_YAP5 = NE213A_PS_YAP5.reset_index(drop=True)

EJ305_PS_YAP2 = prodata.tofEnergySlicerRebin(EJ305_PS_YAP2, reBinFactor = 5, skipCols = 5)
EJ305_PS_YAP3 = prodata.tofEnergySlicerRebin(EJ305_PS_YAP3, reBinFactor = 5, skipCols = 5)
EJ305_PS_YAP4 = prodata.tofEnergySlicerRebin(EJ305_PS_YAP4, reBinFactor = 5, skipCols = 5)
EJ305_PS_YAP5 = prodata.tofEnergySlicerRebin(EJ305_PS_YAP5, reBinFactor = 5, skipCols = 5)


EJ305_PS_binCenter, EJ305_PS_counts = prodata.sliceRandomSubtraction(EJ305_PS_YAP2, EJ305_PS_YAP3, EJ305_PS_YAP4, EJ305_PS_YAP5, randLength, nLength, numBins=100, binRange=[-0, 0.6])

for index, energy in enumerate(np.arange(15, 60, 5)):
    plt.subplot(3, 3, index+1)
    print(f'{round(energy,2)}MeV')
    plt.step(EJ305_PS_binCenter, EJ305_PS_counts[f'MeV{energy}'], label=f'{energy/10} MeV')
    plt.legend()

plt.show()










# for index, energy in enumerate(np.arange(15, 60, 5)):
#     plt.subplot(3, 3, index+1)
#     print(f'{round(energy,2)}MeV')
#     plt.hist(dfReBin.query(f'MeV{energy}>0 and MeV{energy}<0.75')[f"MeV{energy}"], bins=100, range=[0, 0.75], label=f'MeV{energy}, avg={np.round(np.average(dfReBin.query(f"MeV{energy}>0 and MeV{energy}<0.75")[f"MeV{energy}"].dropna()),2)}')
#     plt.legend()
# plt.show()
