#!/usr/bin/env python3
from re import I
import sys
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
from numpy.core.arrayprint import SubArrayFormat
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit

# from matplotlib.colors import LinearSegmentedColormap


# IMPORT FROM LIBRARY DIRECTORY
sys.path.insert(0, "../library/")
sys.path.insert(0, "../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "../")
sys.path.insert(0, "data_analysis/")

import processing_math as promath
import processing_data as prodata
import processing_pd as propd
import processing_simulation as prosim
import figure_style as fs

    
"""
Separate script to keep code clean when making plots
"""

# ┌─┐┌─┐┌┬┐┌┬┐┬┌┐┌┌─┐  ┌─┐┌─┐┬─┐┌─┐┌┬┐┌─┐┌┬┐┌─┐┬─┐┌─┐
# └─┐├┤  │  │ │││││ ┬  ├─┘├─┤├┬┘├─┤│││├┤  │ ├┤ ├┬┘└─┐
# └─┘└─┘ ┴  ┴ ┴┘└┘└─┘  ┴  ┴ ┴┴└─┴ ┴┴ ┴└─┘ ┴ └─┘┴└─└─┘
titleParamters = dict(ha='left', va='center', fontsize=10, color='black')
tofCh = 3
path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop
att_factor_12dB = 3.98
att_factor_11dB = 3.55
att_factor_16_5dB = 6.68
NE213A_calPath = '/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/Ecal'
EJ305_calPath = '/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/EJ305/Ecal'

#import data for raw and tagged spectra
# df_NE213A = propd.load_parquet_merge(path, np.arange(2121,2121+20,1), keep_col=[f'qdc_lg_ch{tofCh}',f'tof_ch{tofCh}','qdc_lg_ch1','qdc_ps_ch1'], full=False)
# df_EJ305 =  propd.load_parquet_merge(path, np.arange(2040,2040+20,1), keep_col=[f'qdc_lg_ch{tofCh}',f'tof_ch{tofCh}','qdc_lg_ch1','qdc_ps_ch1'], full=False)
# df_EJ331 =  propd.load_parquet_merge(path, np.arange(1880,1880+20,1), keep_col=[f'qdc_lg_ch{tofCh}',f'tof_ch{tofCh}','qdc_lg_ch1','qdc_ps_ch1'], full=False)
# df_EJ321P = propd.load_parquet_merge(path, np.arange(2423,2423+20,1), keep_col=[f'qdc_lg_ch{tofCh}',f'tof_ch{tofCh}','qdc_lg_ch1','qdc_ps_ch1'], full=False)


# ┌─┐┬┌┐┌┌─┐┬  ┌─┐┌─┐  ┌─┐┌─┐  ┌─┐┬┌─┐┬ ┬┬─┐┌─┐┌─┐
# └─┐│││││ ┬│  ├┤ └─┐  ├─┘└─┐  ├┤ ││ ┬│ │├┬┘├┤ └─┐
# └─┘┴┘└┘└─┘┴─┘└─┘└─┘  ┴  └─┘  └  ┴└─┘└─┘┴└─└─┘└─┘
def NE213A_PS_singles(plot=True, width=345):
    """
    returns FOM for both YAP thresholds and Neutron thresholds as pandas DataFrame
    """
    print('Singles PS figure: NE213A')
    #########################################
    ### YAP DETECTOR THRESHOLD FIGURE #######
    #########################################
    YAPthr_FOM_integral = np.empty(0)
    YAPthr_FOM_integral_err = np.empty(0)
    numBins = np.arange(0, 1, 0.01)
    baseThr = 500
    start_g = 0.14
    stop_g = 0.3
    start_n = 0.3
    stop_n = 0.54
    
    for count, currenThr in enumerate(np.arange(500, 8500, 500)):
        y, x = np.histogram(df_NE213A.query(f'qdc_lg_ch{tofCh}>{currenThr} and qdc_lg_ch1>{baseThr}').qdc_ps_ch1, bins=numBins)
        x = prodata.getBinCenters(x)
        popt_g, pcov_g = promath.gaussFit(x,y, start=start_g, stop=stop_g, error=True)
        popt_n, pcov_n = promath.gaussFit(x,y, start=start_n, stop=stop_n, error=True)
        FOM_yap = prodata.PSFOM(popt_n[1], popt_g[1], popt_n[2]*2*np.sqrt(2*np.log(2)), popt_g[2]*2*np.sqrt(2*np.log(2)))
        YAPthr_FOM_integral = np.append(YAPthr_FOM_integral, FOM_yap)
        YAPthr_FOM_integral_err = np.append(YAPthr_FOM_integral_err, 0)
        if plot:
            if count == 0: #initialize figure and set style once.
                fs.set_style(textwidth=width)
                plt.figure()
                plt.suptitle('NE213A YAP detector thr [SINGLES]')
            plt.subplot(10,2,count+1)
            plt.step(x, y, color='black')
            plt.plot(x, promath.gaussFunc(x, popt_g[0], popt_g[1], popt_g[2]), color='tomato',lw=2)
            plt.plot(x, promath.gaussFunc(x, popt_n[0], popt_n[1], popt_n[2]), color='royalblue',lw=2)
            plt.text(0.02, np.max(y)*1.05, f'FOM:singleGauss: {round(FOM_yap,2)}$\pm${round(0,2)}', **titleParamters)
            plt.xlim([0, 0.75])
            plt.ylim([0, np.max(y)*1.3])

    #########################################
    ### NEUTRON DETECTOR THRESHOLD FIGURE ###
    #########################################
    Nthr_FOM_numeric = np.empty(0)
    Nthr_FOM_numeric_err = np.empty(0)
    
    for count, currenThr in enumerate(np.arange(500, 8500, 500)):
        y, x = np.histogram(df_NE213A.query(f'qdc_lg_ch{tofCh}>{baseThr} and qdc_lg_ch1>{currenThr}').qdc_ps_ch1, bins=numBins)
        x = prodata.getBinCenters(x)
        popt_g, pcov_g = promath.gaussFit(x,y, start=start_g, stop=stop_g, error=True)
        popt_n, pcov_n = promath.gaussFit(x,y, start=start_n, stop=stop_n, error=True)
        FOM_n = prodata.PSFOM(popt_n[1], popt_g[1], popt_n[2]*2*np.sqrt(2*np.log(2)), popt_g[2]*2*np.sqrt(2*np.log(2)))
        Nthr_FOM_numeric = np.append(Nthr_FOM_numeric, FOM_n)
        Nthr_FOM_numeric_err = np.append(Nthr_FOM_numeric_err, 0)
        if plot:
            if count == 0: #initialize figure and set style once.
                fs.set_style(textwidth=width)
                plt.figure()
                plt.suptitle('NE213A Neutron detector thr [SINGLES]')
            plt.subplot(10,2,count+1)
            plt.step(x, y, color='black')
            plt.plot(x, promath.gaussFunc(x, popt_g[0], popt_g[1], popt_g[2]), color='tomato',lw=2)
            plt.plot(x, promath.gaussFunc(x, popt_n[0], popt_n[1], popt_n[2]), color='royalblue',lw=2)
            plt.text(0.02, np.max(y)*1.05, f'FOM:singleGauss: {round(FOM_n,2)}$\pm${round(0,2)}', **titleParamters)
            plt.xlim([0, 0.75])
            plt.ylim([0, np.max(y)*1.3])

    return pd.DataFrame({'YAPthr_FOM': YAPthr_FOM_integral, 
                        'YAPthr_FOM_err': YAPthr_FOM_integral_err, 
                        'Nthr_FOM': Nthr_FOM_numeric, 
                        'Nthr_FOM_err': Nthr_FOM_numeric_err})

def EJ331_PS_singles(plot=True, width=345):
    """
    returns FOM for both YAP thresholds and Neutron thresholds as pandas DataFrame
    """
    print('Singles PS figure: EJ331')
    #########################################
    ### YAP DETECTOR THRESHOLD FIGURE #######
    #########################################
    YAPthr_FOM_integral = np.empty(0)
    YAPthr_FOM_integral_err = np.empty(0)
    numBins = np.arange(0, 1, 0.01)
    baseThr = 500
    start_g = 0.11
    stop_g = 0.21
    start_n = 0.21
    stop_n = 0.4
    
    for count, currenThr in enumerate(np.arange(500, 8500, 500)):
        y, x = np.histogram(df_EJ331.query(f'qdc_lg_ch{tofCh}>{currenThr} and qdc_lg_ch1>{baseThr}').qdc_ps_ch1, bins=numBins)
        x = prodata.getBinCenters(x)
        popt_g, pcov_g = promath.gaussFit(x,y, start=start_g, stop=stop_g, error=True)
        popt_n, pcov_n = promath.gaussFit(x,y, start=start_n, stop=stop_n, error=True)
        FOM_yap = prodata.PSFOM(popt_n[1], popt_g[1], popt_n[2]*2*np.sqrt(2*np.log(2)), popt_g[2]*2*np.sqrt(2*np.log(2)))
        YAPthr_FOM_integral = np.append(YAPthr_FOM_integral, FOM_yap)
        YAPthr_FOM_integral_err = np.append(YAPthr_FOM_integral_err, 0)
        if plot:
            if count == 0: #initialize figure and set style once.
                fs.set_style(textwidth=width)
                plt.figure()
                plt.suptitle('EJ331 YAP detector thr [SINGLES]')
            plt.subplot(10,2,count+1)
            plt.step(x, y, color='black')
            plt.plot(x, promath.gaussFunc(x, popt_g[0], popt_g[1], popt_g[2]), color='tomato',lw=2)
            plt.plot(x, promath.gaussFunc(x, popt_n[0], popt_n[1], popt_n[2]), color='royalblue',lw=2)
            plt.text(0.02, np.max(y)*1.05, f'FOM:singleGauss: {round(FOM_yap,2)}$\pm${round(0,2)}', **titleParamters)
            plt.xlim([0, 0.75])
            plt.ylim([0, np.max(y)*1.3])

    #########################################
    ### NEUTRON DETECTOR THRESHOLD FIGURE ###
    #########################################
    Nthr_FOM_numeric = np.empty(0)
    Nthr_FOM_numeric_err = np.empty(0)
    
    for count, currenThr in enumerate(np.arange(500, 8500, 500)):
        y, x = np.histogram(df_EJ331.query(f'qdc_lg_ch{tofCh}>{baseThr} and qdc_lg_ch1>{currenThr}').qdc_ps_ch1, bins=numBins)
        x = prodata.getBinCenters(x)
        popt_g, pcov_g = promath.gaussFit(x,y, start=start_g, stop=stop_g, error=True)
        popt_n, pcov_n = promath.gaussFit(x,y, start=start_n, stop=stop_n, error=True)
        FOM_n = prodata.PSFOM(popt_n[1], popt_g[1], popt_n[2]*2*np.sqrt(2*np.log(2)), popt_g[2]*2*np.sqrt(2*np.log(2)))
        Nthr_FOM_numeric = np.append(Nthr_FOM_numeric, FOM_n)
        Nthr_FOM_numeric_err = np.append(Nthr_FOM_numeric_err, 0)
        if plot:
            if count == 0: #initialize figure and set style once.
                fs.set_style(textwidth=width)
                plt.figure()
                plt.suptitle('EJ331 Neutron detector thr [SINGLES]')
            plt.subplot(10,2,count+1)
            plt.step(x, y, color='black')
            plt.plot(x, promath.gaussFunc(x, popt_g[0], popt_g[1], popt_g[2]), color='tomato',lw=2)
            plt.plot(x, promath.gaussFunc(x, popt_n[0], popt_n[1], popt_n[2]), color='royalblue',lw=2)
            plt.text(0.02, np.max(y)*1.05, f'FOM:singleGauss: {round(FOM_n,2)}$\pm${round(0,2)}', **titleParamters)
            plt.xlim([0, 0.75])
            plt.ylim([0, np.max(y)*1.3])

    return pd.DataFrame({'YAPthr_FOM': YAPthr_FOM_integral, 
                        'YAPthr_FOM_err': YAPthr_FOM_integral_err, 
                        'Nthr_FOM': Nthr_FOM_numeric, 
                        'Nthr_FOM_err': Nthr_FOM_numeric_err})

def EJ305_PS_singles(plot=True, width=345):
    """
    returns FOM for both YAP thresholds and Neutron thresholds as pandas DataFrame
    """
    print('Singles PS figure: EJ305')
    #########################################
    ### YAP DETECTOR THRESHOLD FIGURE #######
    #########################################
    YAPthr_FOM_integral = np.empty(0)
    YAPthr_FOM_integral_err = np.empty(0)
    numBins = np.arange(0, 1, 0.01)
    baseThr = 500
    start_g = 0.16
    stop_g = 0.195
    start_n = 0.2
    stop_n = 0.27
    
    for count, currenThr in enumerate(np.arange(500, 8500, 500)):
        y, x = np.histogram(df_EJ305.query(f'qdc_lg_ch{tofCh}>{currenThr} and qdc_lg_ch1>{baseThr}').qdc_ps_ch1, bins=numBins)
        x = prodata.getBinCenters(x)
        popt_g, pcov_g = promath.gaussFit(x,y, start=start_g, stop=stop_g, error=True)
        popt_n, pcov_n = promath.gaussFit(x,y, start=start_n, stop=stop_n, error=True)
        FOM_yap = prodata.PSFOM(popt_n[1], popt_g[1], popt_n[2]*2*np.sqrt(2*np.log(2)), popt_g[2]*2*np.sqrt(2*np.log(2)))
        YAPthr_FOM_integral = np.append(YAPthr_FOM_integral, FOM_yap)
        YAPthr_FOM_integral_err = np.append(YAPthr_FOM_integral_err, 0)
        if plot:
            if count == 0: #initialize figure and set style once.
                fs.set_style(textwidth=width)
                plt.figure()
                plt.suptitle('EJ305 YAP detector thr [SINGLES]')
            plt.subplot(10,2,count+1)
            plt.step(x, y, color='black')
            plt.plot(x, promath.gaussFunc(x, popt_g[0], popt_g[1], popt_g[2]), color='tomato',lw=2)
            plt.plot(x, promath.gaussFunc(x, popt_n[0], popt_n[1], popt_n[2]), color='royalblue',lw=2)
            plt.text(0.02, np.max(y)*1.05, f'FOM:singleGauss: {round(FOM_yap,2)}$\pm${round(0,2)}', **titleParamters)
            plt.xlim([0, 0.75])
            plt.ylim([0, np.max(y)*1.3])

    #########################################
    ### NEUTRON DETECTOR THRESHOLD FIGURE ###
    #########################################
    Nthr_FOM_numeric = np.empty(0)
    Nthr_FOM_numeric_err = np.empty(0)
    
    for count, currenThr in enumerate(np.arange(500, 8500, 500)):
        y, x = np.histogram(df_EJ305.query(f'qdc_lg_ch{tofCh}>{baseThr} and qdc_lg_ch1>{currenThr}').qdc_ps_ch1, bins=numBins)
        x = prodata.getBinCenters(x)
        popt_g, pcov_g = promath.gaussFit(x,y, start=start_g, stop=stop_g, error=True)
        popt_n, pcov_n = promath.gaussFit(x,y, start=start_n, stop=stop_n, error=True)
        FOM_n = prodata.PSFOM(popt_n[1], popt_g[1], popt_n[2]*2*np.sqrt(2*np.log(2)), popt_g[2]*2*np.sqrt(2*np.log(2)))
        Nthr_FOM_numeric = np.append(Nthr_FOM_numeric, FOM_n)
        Nthr_FOM_numeric_err = np.append(Nthr_FOM_numeric_err, 0)
        if plot:
            if count == 0: #initialize figure and set style once.
                fs.set_style(textwidth=width)
                plt.figure()
                plt.suptitle('EJ305 Neutron detector thr [SINGLES]')
            plt.subplot(10,2,count+1)
            plt.step(x, y, color='black')
            plt.plot(x, promath.gaussFunc(x, popt_g[0], popt_g[1], popt_g[2]), color='tomato',lw=2)
            plt.plot(x, promath.gaussFunc(x, popt_n[0], popt_n[1], popt_n[2]), color='royalblue',lw=2)
            plt.text(0.02, np.max(y)*1.05, f'FOM:singleGauss: {round(FOM_n,2)}$\pm${round(0,2)}', **titleParamters)
            plt.xlim([0, 0.75])
            plt.ylim([0, np.max(y)*1.3])

    return pd.DataFrame({'YAPthr_FOM': YAPthr_FOM_integral, 
                        'YAPthr_FOM_err': YAPthr_FOM_integral_err, 
                        'Nthr_FOM': Nthr_FOM_numeric, 
                        'Nthr_FOM_err': Nthr_FOM_numeric_err})

def EJ321P_PS_singles(plot=True, width=345):
    """
    returns FOM for both YAP thresholds and Neutron thresholds as pandas DataFrame
    """
    print('Singles PS figure: EJ321P')
    #########################################
    ### YAP DETECTOR THRESHOLD FIGURE #######
    #########################################
    YAPthr_FOM_integral = np.empty(0)
    YAPthr_FOM_integral_err = np.empty(0)
    numBins = np.arange(0, 1, 0.01)
    baseThr = 500
    start_g = 0.2
    stop_g = 0.36
    start_n = 0.2
    stop_n = 0.36
    
    for count, currenThr in enumerate(np.arange(500, 8500, 500)):
        y, x = np.histogram(df_EJ321P.query(f'qdc_lg_ch{tofCh}>{currenThr} and qdc_lg_ch1>{baseThr}').qdc_ps_ch1, bins=numBins)
        x = prodata.getBinCenters(x)
        popt_g, pcov_g = promath.gaussFit(x,y, start=start_g, stop=stop_g, error=True)
        popt_n, pcov_n = promath.gaussFit(x,y, start=start_n, stop=stop_n, error=True)
        FOM_yap = prodata.PSFOM(popt_n[1], popt_g[1], popt_n[2]*2*np.sqrt(2*np.log(2)), popt_g[2]*2*np.sqrt(2*np.log(2)))
        YAPthr_FOM_integral = np.append(YAPthr_FOM_integral, FOM_yap)
        YAPthr_FOM_integral_err = np.append(YAPthr_FOM_integral_err, 0)
        if plot:
            if count == 0: #initialize figure and set style once.
                fs.set_style(textwidth=width)
                plt.figure()
                plt.suptitle('EJ321P YAP detector thr [SINGLES]')
            plt.subplot(10,2,count+1)
            plt.step(x, y, color='black')
            plt.plot(x, promath.gaussFunc(x, popt_g[0], popt_g[1], popt_g[2]), color='tomato',lw=2)
            plt.plot(x, promath.gaussFunc(x, popt_n[0], popt_n[1], popt_n[2]), color='royalblue',lw=2)
            plt.text(0.02, np.max(y)*1.05, f'FOM:singleGauss: {round(FOM_yap,2)}$\pm${round(0,2)}', **titleParamters)
            plt.xlim([0, 0.75])
            plt.ylim([0, np.max(y)*1.3])

    #########################################
    ### NEUTRON DETECTOR THRESHOLD FIGURE ###
    #########################################
    Nthr_FOM_numeric = np.empty(0)
    Nthr_FOM_numeric_err = np.empty(0)
    
    for count, currenThr in enumerate(np.arange(500, 8500, 500)):
        y, x = np.histogram(df_EJ321P.query(f'qdc_lg_ch{tofCh}>{baseThr} and qdc_lg_ch1>{currenThr}').qdc_ps_ch1, bins=numBins)
        x = prodata.getBinCenters(x)
        popt_g, pcov_g = promath.gaussFit(x,y, start=start_g, stop=stop_g, error=True)
        popt_n, pcov_n = promath.gaussFit(x,y, start=start_n, stop=stop_n, error=True)
        FOM_n = prodata.PSFOM(popt_n[1], popt_g[1], popt_n[2]*2*np.sqrt(2*np.log(2)), popt_g[2]*2*np.sqrt(2*np.log(2)))
        Nthr_FOM_numeric = np.append(Nthr_FOM_numeric, FOM_n)
        Nthr_FOM_numeric_err = np.append(Nthr_FOM_numeric_err, 0)
        if plot:
            if count == 0: #initialize figure and set style once.
                fs.set_style(textwidth=width)
                plt.figure()
                plt.suptitle('EJ321P Neutron detector thr [SINGLES]')
            plt.subplot(10,2,count+1)
            plt.step(x, y, color='black')
            plt.plot(x, promath.gaussFunc(x, popt_g[0], popt_g[1], popt_g[2]), color='tomato',lw=2)
            plt.plot(x, promath.gaussFunc(x, popt_n[0], popt_n[1], popt_n[2]), color='royalblue',lw=2)
            plt.text(0.02, np.max(y)*1.05, f'FOM:singleGauss: {round(FOM_n,2)}$\pm${round(0,2)}', **titleParamters)
            plt.xlim([0, 0.75])
            plt.ylim([0, np.max(y)*1.3])

    return pd.DataFrame({'YAPthr_FOM': YAPthr_FOM_integral, 
                        'YAPthr_FOM_err': YAPthr_FOM_integral_err, 
                        'Nthr_FOM': Nthr_FOM_numeric, 
                        'Nthr_FOM_err': Nthr_FOM_numeric_err})

def NE213A_EJ305_PS_singles(plot=False, width=345):
    """
    Calculated FOM data vs threshold for singles events for both NE213A and EJ305 detectors
    """
    #Def paramters
    numBins = np.arange(0, 1, 0.005)
    baseThr = 500
    tofCh = 3

    #Fit limits for NE213A    
    NE213A_start_g = 0.19
    NE213A_stop_g = 0.26
    NE213A_start_n = 0.32
    NE213A_stop_n = 0.48
    
    #Fit limits for EJ305
    EJ305_start_g = 0.167
    EJ305_stop_g = 0.187
    EJ305_start_n = 0.213
    EJ305_stop_n = 0.248

    #Define containers:
    NE213A_FOM = np.empty(0)

    NE213A_mean_n = np.empty(0)
    NE213A_std_n = np.empty(0)

    NE213A_mean_g = np.empty(0)
    NE213A_std_g = np.empty(0)
    
    EJ305_FOM = np.empty(0)

    EJ305_mean_n = np.empty(0)
    EJ305_std_n = np.empty(0)

    EJ305_mean_g = np.empty(0)
    EJ305_std_g = np.empty(0)
    
    thr = np.empty(0)

    #Load data for NE213A
    df_NE213A = propd.load_parquet_merge(path, np.arange(2121,2121+20,1), keep_col=[f'qdc_lg_ch{3}',f'tof_ch{3}','qdc_lg_ch1','qdc_ps_ch1'], full=False)
    # #Calibrate NE213A qdc
    df_NE213A.qdc_lg_ch1 = prodata.calibrateMyDetector(NE213A_calPath, df_NE213A.qdc_lg_ch1)*att_factor_12dB
    #Load data for EJ305
    df_EJ305 =  propd.load_parquet_merge(path, np.arange(2830,2830+20,1), keep_col=[f'qdc_lg_ch{3}',f'tof_ch{3}','qdc_lg_ch1','qdc_ps_ch1'], full=False)
    # #Caibrate EJ305 qdc
    df_EJ305.qdc_lg_ch1 = prodata.calibrateMyDetector(EJ305_calPath, df_EJ305.qdc_lg_ch1)*att_factor_16_5dB

    for currentThr in np.arange(250, 4250, 250):
        print(f'-> Current threshold: {currentThr}')
        #NE213A FOM calculations
        y, x = np.histogram(df_NE213A.query(f'qdc_lg_ch{tofCh}>{baseThr} and qdc_lg_ch1>{currentThr/1000}').qdc_ps_ch1, bins=numBins)
        x = prodata.getBinCenters(x)
        popt_g, pcov_g = promath.gaussFit(x,y, start=NE213A_start_g, stop=NE213A_stop_g, error=True)
        popt_n, pcov_n = promath.gaussFit(x,y, start=NE213A_start_n, stop=NE213A_stop_n, error=True)
        NE213A_FOM_current = prodata.PSFOM(popt_n[1], popt_g[1], popt_n[2]*2*np.sqrt(2*np.log(2)), popt_g[2]*2*np.sqrt(2*np.log(2)))
        NE213A_FOM_err_current = 0

        print('-------------NE213A----------------------')
        print(f'avg_n = {round(popt_n[1],3)}')
        print(f'std_n = {round(popt_n[2],3)}')
        print(f'avg_g = {round(popt_g[1],3)}')
        print(f'std_g = {round(popt_g[2],3)}')
        print(f'FOM = {round(NE213A_FOM_current,3)}')
        
        NE213A_FOM = np.append(NE213A_FOM, NE213A_FOM_current)

        NE213A_mean_n = np.append(NE213A_mean_n, popt_n[1])
        NE213A_std_n = np.append(NE213A_std_n, popt_n[2])

        NE213A_mean_g = np.append(NE213A_mean_g, popt_g[1])
        NE213A_std_g = np.append(NE213A_std_g, popt_g[2])

        if plot:
            plt.title(f'NE213A, FOM={round(NE213A_FOM_current,3)}')
            plt.step(x, y, color='black')
            plt.plot(numBins, promath.gaussFunc(numBins, popt_g[0], popt_g[1], popt_g[2]), color='tomato',lw=2)
            plt.plot(numBins, promath.gaussFunc(numBins, popt_n[0], popt_n[1], popt_n[2]), color='royalblue',lw=2)
            plt.xlim([0, 0.75])
            plt.ylim([0, np.max(y)*1.3])
            plt.show()

        #EJ305 FOM calculations
        y, x = np.histogram(df_EJ305.query(f'qdc_lg_ch{tofCh}>{baseThr} and qdc_lg_ch1>{currentThr/1000}').qdc_ps_ch1, bins=np.arange(0.1,0.4,0.001))
        x = prodata.getBinCenters(x)
        
        popt_g, pcov_g = promath.gaussFit(x,y, start=EJ305_start_g, stop=EJ305_stop_g, error=True)
        popt_n, pcov_n = promath.gaussFit(x,y, start=EJ305_start_n, stop=EJ305_stop_n, error=True)
        EJ305_FOM_current = prodata.PSFOM(popt_n[1], popt_g[1], popt_n[2]*2*np.sqrt(2*np.log(2)), popt_g[2]*2*np.sqrt(2*np.log(2)))

        EJ305_FOM = np.append(EJ305_FOM, EJ305_FOM_current)
        
        EJ305_mean_n = np.append(EJ305_mean_n, popt_n[1])
        EJ305_std_n = np.append(EJ305_std_n, popt_n[2])

        EJ305_mean_g = np.append(EJ305_mean_g, popt_g[1])
        EJ305_std_g = np.append(EJ305_std_g, popt_g[2])

        if plot:
            plt.title(f'EJ305, FOM={round(NE213A_FOM_current,3)}')
            plt.step(x, y, color='black')
            plt.plot(numBins, promath.gaussFunc(numBins, popt_g[0], popt_g[1], popt_g[2]), color='tomato',lw=2)
            plt.plot(numBins, promath.gaussFunc(numBins, popt_n[0], popt_n[1], popt_n[2]), color='royalblue',lw=2)
            plt.xlim([0, 0.75])
            plt.ylim([0, np.max(y)*1.3])
            plt.show()

        thr = np.append(thr, currentThr)

        print('-------------EJ305----------------------')
        print(f'avg_n = {round(popt_n[1],3)}')
        print(f'std_n = {round(popt_n[2],3)}')
        print(f'avg_g = {round(popt_g[1],3)}')
        print(f'std_g = {round(popt_g[2],3)}')
        print(f'FOM = {round(EJ305_FOM_current,3)}')
    
    
    return pd.DataFrame({'threshold': thr,
                        'NE213A_mean_n': NE213A_mean_n,
                        'NE213A_std_n': NE213A_std_n,
                        'NE213A_mean_g': NE213A_mean_g,
                        'NE213A_std_g': NE213A_std_g,
                        'NE213A_FOM': NE213A_FOM, 
                        'EJ305_mean_n': EJ305_mean_n,
                        'EJ305_std_n': EJ305_std_n,
                        'EJ305_mean_g': EJ305_mean_g,
                        'EJ305_std_g': EJ305_std_g,
                        'EJ305_FOM': EJ305_FOM})

# ┌┬┐┌─┐┌─┐┌─┐┌─┐┌┬┐  ┌─┐┌─┐  ┌─┐┬┌─┐┬ ┬┬─┐┌─┐┌─┐
#  │ ├─┤│ ┬│ ┬├┤  ││  ├─┘└─┐  ├┤ ││ ┬│ │├┬┘├┤ └─┐
#  ┴ ┴ ┴└─┘└─┘└─┘─┴┘  ┴  └─┘  └  ┴└─┘└─┘┴└─└─┘└─┘
def NE213A_PS_tagged(plot=True, width=345):
    """
    returns FOM for both YAP thresholds and Neutron thresholds as pandas DataFrame
    """
    print('tagged PS figure: NE213A')
    #########################################
    ### YAP DETECTOR THRESHOLD FIGURE #######
    #########################################
    YAPthr_FOM_integral = np.empty(0)
    YAPthr_FOM_integral_err = np.empty(0)
    numBins = np.arange(0, 1, 0.01)
    baseThr = 500
    start_g = 0.14
    stop_g = 0.3
    start_n = 0.3
    stop_n = 0.54
    
    for count, currenThr in enumerate(np.arange(500, 8500, 500)):
        y, x = np.histogram(df_NE213A.query(f'qdc_lg_ch{tofCh}>{currenThr} and qdc_lg_ch1>{baseThr} and tof_ch{tofCh}>60 and tof_ch{tofCh}<160').qdc_ps_ch1, bins=numBins)
        x = prodata.getBinCenters(x)
        popt_g, pcov_g = promath.gaussFit(x,y, start=start_g, stop=stop_g, error=True)
        popt_n, pcov_n = promath.gaussFit(x,y, start=start_n, stop=stop_n, error=True)
        FOM_yap = prodata.PSFOM(popt_n[1], popt_g[1], popt_n[2]*2*np.sqrt(2*np.log(2)), popt_g[2]*2*np.sqrt(2*np.log(2)))
        YAPthr_FOM_integral = np.append(YAPthr_FOM_integral, FOM_yap)
        YAPthr_FOM_integral_err = np.append(YAPthr_FOM_integral_err, 0)
        if plot:
            if count == 0: #initialize figure and set style once.
                fs.set_style(textwidth=width)
                plt.figure()
                plt.suptitle('NE213A YAP detector thr [TAGGED]')
            plt.subplot(10,2,count+1)
            plt.step(x, y, color='black')
            plt.plot(x, promath.gaussFunc(x, popt_g[0], popt_g[1], popt_g[2]), color='tomato',lw=2)
            plt.plot(x, promath.gaussFunc(x, popt_n[0], popt_n[1], popt_n[2]), color='royalblue',lw=2)
            plt.text(0.02, np.max(y)*1.05, f'FOM:singleGauss: {round(FOM_yap,2)}$\pm${round(0,2)}', **titleParamters)
            plt.xlim([0, 0.75])
            plt.ylim([0, np.max(y)*1.3])

    #########################################
    ### NEUTRON DETECTOR THRESHOLD FIGURE ###
    #########################################
    Nthr_FOM_numeric = np.empty(0)
    Nthr_FOM_numeric_err = np.empty(0)
    
    for count, currenThr in enumerate(np.arange(500, 8500, 500)):
        y, x = np.histogram(df_NE213A.query(f'qdc_lg_ch{tofCh}>{baseThr} and qdc_lg_ch1>{currenThr} and tof_ch{tofCh}>60 and tof_ch{tofCh}<160').qdc_ps_ch1, bins=numBins)
        x = prodata.getBinCenters(x)
        popt_g, pcov_g = promath.gaussFit(x,y, start=start_g, stop=stop_g, error=True)
        popt_n, pcov_n = promath.gaussFit(x,y, start=start_n, stop=stop_n, error=True)
        FOM_n = prodata.PSFOM(popt_n[1], popt_g[1], popt_n[2]*2*np.sqrt(2*np.log(2)), popt_g[2]*2*np.sqrt(2*np.log(2)))
        Nthr_FOM_numeric = np.append(Nthr_FOM_numeric, FOM_n)
        Nthr_FOM_numeric_err = np.append(Nthr_FOM_numeric_err, 0)
        if plot:
            if count == 0: #initialize figure and set style once.
                fs.set_style(textwidth=width)
                plt.figure()
                plt.suptitle('NE213A Neutron detector thr [TAGGED]')
            plt.subplot(10,2,count+1)
            plt.step(x, y, color='black')
            plt.plot(x, promath.gaussFunc(x, popt_g[0], popt_g[1], popt_g[2]), color='tomato',lw=2)
            plt.plot(x, promath.gaussFunc(x, popt_n[0], popt_n[1], popt_n[2]), color='royalblue',lw=2)
            plt.text(0.02, np.max(y)*1.05, f'FOM:singleGauss: {round(FOM_n,2)}$\pm${round(0,2)}', **titleParamters)
            plt.xlim([0, 0.75])
            plt.ylim([0, np.max(y)*1.3])

    return pd.DataFrame({'YAPthr_FOM': YAPthr_FOM_integral, 
                        'YAPthr_FOM_err': YAPthr_FOM_integral_err, 
                        'Nthr_FOM': Nthr_FOM_numeric, 
                        'Nthr_FOM_err': Nthr_FOM_numeric_err})

def EJ331_PS_tagged(plot=True, width=345):
    """
    returns FOM for both YAP thresholds and Neutron thresholds as pandas DataFrame
    """
    print('tagged PS figure: EJ331')
    #########################################
    ### YAP DETECTOR THRESHOLD FIGURE #######
    #########################################
    YAPthr_FOM_integral = np.empty(0)
    YAPthr_FOM_integral_err = np.empty(0)
    numBins = np.arange(0, 1, 0.01)
    baseThr = 500
    start_g = 0.11
    stop_g = 0.21
    start_n = 0.21
    stop_n = 0.4
    
    for count, currenThr in enumerate(np.arange(500, 8500, 500)):
        y, x = np.histogram(df_EJ331.query(f'qdc_lg_ch{tofCh}>{currenThr} and qdc_lg_ch1>{baseThr} and tof_ch{tofCh}>47 and tof_ch{tofCh}<147').qdc_ps_ch1, bins=numBins)
        x = prodata.getBinCenters(x)
        popt_g, pcov_g = promath.gaussFit(x,y, start=start_g, stop=stop_g, error=True)
        popt_n, pcov_n = promath.gaussFit(x,y, start=start_n, stop=stop_n, error=True)
        FOM_yap = prodata.PSFOM(popt_n[1], popt_g[1], popt_n[2]*2*np.sqrt(2*np.log(2)), popt_g[2]*2*np.sqrt(2*np.log(2)))
        YAPthr_FOM_integral = np.append(YAPthr_FOM_integral, FOM_yap)
        YAPthr_FOM_integral_err = np.append(YAPthr_FOM_integral_err, 0)
        if plot:
            if count == 0: #initialize figure and set style once.
                fs.set_style(textwidth=width)
                plt.figure()
                plt.suptitle('EJ331 YAP detector thr [TAGGED]')
            plt.subplot(10,2,count+1)
            plt.step(x, y, color='black')
            plt.plot(x, promath.gaussFunc(x, popt_g[0], popt_g[1], popt_g[2]), color='tomato',lw=2)
            plt.plot(x, promath.gaussFunc(x, popt_n[0], popt_n[1], popt_n[2]), color='royalblue',lw=2)
            plt.text(0.02, np.max(y)*1.05, f'FOM:singleGauss: {round(FOM_yap,2)}$\pm${round(0,2)}', **titleParamters)
            plt.xlim([0, 0.75])
            plt.ylim([0, np.max(y)*1.3])

    #########################################
    ### NEUTRON DETECTOR THRESHOLD FIGURE ###
    #########################################
    Nthr_FOM_numeric = np.empty(0)
    Nthr_FOM_numeric_err = np.empty(0)
    
    for count, currenThr in enumerate(np.arange(500, 8500, 500)):
        y, x = np.histogram(df_EJ331.query(f'qdc_lg_ch{tofCh}>{baseThr} and qdc_lg_ch1>{currenThr} and tof_ch{tofCh}>47 and tof_ch{tofCh}<147').qdc_ps_ch1, bins=numBins)
        x = prodata.getBinCenters(x)
        popt_g, pcov_g = promath.gaussFit(x,y, start=start_g, stop=stop_g, error=True)
        popt_n, pcov_n = promath.gaussFit(x,y, start=start_n, stop=stop_n, error=True)
        FOM_n = prodata.PSFOM(popt_n[1], popt_g[1], popt_n[2]*2*np.sqrt(2*np.log(2)), popt_g[2]*2*np.sqrt(2*np.log(2)))
        Nthr_FOM_numeric = np.append(Nthr_FOM_numeric, FOM_n)
        Nthr_FOM_numeric_err = np.append(Nthr_FOM_numeric_err, 0)
        if plot:
            if count == 0: #initialize figure and set style once.
                fs.set_style(textwidth=width)
                plt.figure()
                plt.suptitle('EJ331 Neutron detector thr [TAGGED]')
            plt.subplot(10,2,count+1)
            plt.step(x, y, color='black')
            plt.plot(x, promath.gaussFunc(x, popt_g[0], popt_g[1], popt_g[2]), color='tomato',lw=2)
            plt.plot(x, promath.gaussFunc(x, popt_n[0], popt_n[1], popt_n[2]), color='royalblue',lw=2)
            plt.text(0.02, np.max(y)*1.05, f'FOM:singleGauss: {round(FOM_n,2)}$\pm${round(0,2)}', **titleParamters)
            plt.xlim([0, 0.75])
            plt.ylim([0, np.max(y)*1.3])

    return pd.DataFrame({'YAPthr_FOM': YAPthr_FOM_integral, 
                        'YAPthr_FOM_err': YAPthr_FOM_integral_err, 
                        'Nthr_FOM': Nthr_FOM_numeric, 
                        'Nthr_FOM_err': Nthr_FOM_numeric_err})

def EJ305_PS_tagged(plot=True, width=345):
    """
    returns FOM for both YAP thresholds and Neutron thresholds as pandas DataFrame
    """
    print('tagged PS figure: EJ305')
    #########################################
    ### YAP DETECTOR THRESHOLD FIGURE #######
    #########################################
    YAPthr_FOM_integral = np.empty(0)
    YAPthr_FOM_integral_err = np.empty(0)
    numBins = np.arange(0, 1, 0.01)
    baseThr = 500
    start_g = 0.16
    stop_g = 0.195
    start_n = 0.2
    stop_n = 0.27
    
    for count, currenThr in enumerate(np.arange(500, 8500, 500)):
        y, x = np.histogram(df_EJ305.query(f'qdc_lg_ch{tofCh}>{currenThr} and qdc_lg_ch1>{baseThr} and tof_ch{tofCh}>60 and tof_ch{tofCh}<160').qdc_ps_ch1, bins=numBins)
        x = prodata.getBinCenters(x)
        popt_g, pcov_g = promath.gaussFit(x,y, start=start_g, stop=stop_g, error=True)
        popt_n, pcov_n = promath.gaussFit(x,y, start=start_n, stop=stop_n, error=True)
        FOM_yap = prodata.PSFOM(popt_n[1], popt_g[1], popt_n[2]*2*np.sqrt(2*np.log(2)), popt_g[2]*2*np.sqrt(2*np.log(2)))
        YAPthr_FOM_integral = np.append(YAPthr_FOM_integral, FOM_yap)
        YAPthr_FOM_integral_err = np.append(YAPthr_FOM_integral_err, 0)
        if plot:
            if count == 0: #initialize figure and set style once.
                fs.set_style(textwidth=width)
                plt.figure()
                plt.suptitle('EJ305 YAP detector thr [TAGGED]')
            plt.subplot(10,2,count+1)
            plt.step(x, y, color='black')
            plt.plot(x, promath.gaussFunc(x, popt_g[0], popt_g[1], popt_g[2]), color='tomato',lw=2)
            plt.plot(x, promath.gaussFunc(x, popt_n[0], popt_n[1], popt_n[2]), color='royalblue',lw=2)
            plt.text(0.02, np.max(y)*1.05, f'FOM:singleGauss: {round(FOM_yap,2)}$\pm${round(0,2)}', **titleParamters)
            plt.xlim([0, 0.75])
            plt.ylim([0, np.max(y)*1.3])

    #########################################
    ### NEUTRON DETECTOR THRESHOLD FIGURE ###
    #########################################
    Nthr_FOM_numeric = np.empty(0)
    Nthr_FOM_numeric_err = np.empty(0)
    
    for count, currenThr in enumerate(np.arange(500, 8500, 500)):
        y, x = np.histogram(df_EJ305.query(f'qdc_lg_ch{tofCh}>{baseThr} and qdc_lg_ch1>{currenThr} and tof_ch{tofCh}>60 and tof_ch{tofCh}<160').qdc_ps_ch1, bins=numBins)
        x = prodata.getBinCenters(x)
        popt_g, pcov_g = promath.gaussFit(x,y, start=start_g, stop=stop_g, error=True)
        popt_n, pcov_n = promath.gaussFit(x,y, start=start_n, stop=stop_n, error=True)
        FOM_n = prodata.PSFOM(popt_n[1], popt_g[1], popt_n[2]*2*np.sqrt(2*np.log(2)), popt_g[2]*2*np.sqrt(2*np.log(2)))
        Nthr_FOM_numeric = np.append(Nthr_FOM_numeric, FOM_n)
        Nthr_FOM_numeric_err = np.append(Nthr_FOM_numeric_err, 0)
        if plot:
            if count == 0: #initialize figure and set style once.
                fs.set_style(textwidth=width)
                plt.figure()
                plt.suptitle('EJ305 Neutron detector thr [TAGGED]')
            plt.subplot(10,2,count+1)
            plt.step(x, y, color='black')
            plt.plot(x, promath.gaussFunc(x, popt_g[0], popt_g[1], popt_g[2]), color='tomato',lw=2)
            plt.plot(x, promath.gaussFunc(x, popt_n[0], popt_n[1], popt_n[2]), color='royalblue',lw=2)
            plt.text(0.02, np.max(y)*1.05, f'FOM:singleGauss: {round(FOM_n,2)}$\pm${round(0,2)}', **titleParamters)
            plt.xlim([0, 0.75])
            plt.ylim([0, np.max(y)*1.3])

    return pd.DataFrame({'YAPthr_FOM': YAPthr_FOM_integral, 
                        'YAPthr_FOM_err': YAPthr_FOM_integral_err, 
                        'Nthr_FOM': Nthr_FOM_numeric, 
                        'Nthr_FOM_err': Nthr_FOM_numeric_err})

def EJ321P_PS_tagged(plot=True, width=345):
    """
    returns FOM for both YAP thresholds and Neutron thresholds as pandas DataFrame
    """
    print('tagged PS figure: EJ321P')
    #########################################
    ### YAP DETECTOR THRESHOLD FIGURE #######
    #########################################
    YAPthr_FOM_integral = np.empty(0)
    YAPthr_FOM_integral_err = np.empty(0)
    numBins = np.arange(0, 1, 0.01)
    baseThr = 500
    start_g = 0.2
    stop_g = 0.36
    start_n = 0.2
    stop_n = 0.36
    
    for count, currenThr in enumerate(np.arange(500, 8500, 500)):
        y, x = np.histogram(df_EJ321P.query(f'qdc_lg_ch{tofCh}>{currenThr} and qdc_lg_ch1>{baseThr} and tof_ch{tofCh}>60 and tof_ch{tofCh}<160').qdc_ps_ch1, bins=numBins)
        x = prodata.getBinCenters(x)
        popt_g, pcov_g = promath.gaussFit(x,y, start=start_g, stop=stop_g, error=True)
        popt_n, pcov_n = promath.gaussFit(x,y, start=start_n, stop=stop_n, error=True)
        FOM_yap = prodata.PSFOM(popt_n[1], popt_g[1], popt_n[2]*2*np.sqrt(2*np.log(2)), popt_g[2]*2*np.sqrt(2*np.log(2)))
        YAPthr_FOM_integral = np.append(YAPthr_FOM_integral, FOM_yap)
        YAPthr_FOM_integral_err = np.append(YAPthr_FOM_integral_err, 0)
        if plot:
            if count == 0: #initialize figure and set style once.
                fs.set_style(textwidth=width)
                plt.figure()
                plt.suptitle('EJ321P YAP detector thr [TAGGED]')
            plt.subplot(10,2,count+1)
            plt.step(x, y, color='black')
            plt.plot(x, promath.gaussFunc(x, popt_g[0], popt_g[1], popt_g[2]), color='tomato',lw=2)
            plt.plot(x, promath.gaussFunc(x, popt_n[0], popt_n[1], popt_n[2]), color='royalblue',lw=2)
            plt.text(0.02, np.max(y)*1.05, f'FOM:singleGauss: {round(FOM_yap,2)}$\pm${round(0,2)}', **titleParamters)
            plt.xlim([0, 0.75])
            plt.ylim([0, np.max(y)*1.3])

    #########################################
    ### NEUTRON DETECTOR THRESHOLD FIGURE ###
    #########################################
    Nthr_FOM_numeric = np.empty(0)
    Nthr_FOM_numeric_err = np.empty(0)
    
    for count, currenThr in enumerate(np.arange(500, 8500, 500)):
        y, x = np.histogram(df_EJ321P.query(f'qdc_lg_ch{tofCh}>{baseThr} and qdc_lg_ch1>{currenThr} and tof_ch{tofCh}>60 and tof_ch{tofCh}<160').qdc_ps_ch1, bins=numBins)
        x = prodata.getBinCenters(x)
        popt_g, pcov_g = promath.gaussFit(x,y, start=start_g, stop=stop_g, error=True)
        popt_n, pcov_n = promath.gaussFit(x,y, start=start_n, stop=stop_n, error=True)
        FOM_n = prodata.PSFOM(popt_n[1], popt_g[1], popt_n[2]*2*np.sqrt(2*np.log(2)), popt_g[2]*2*np.sqrt(2*np.log(2)))
        Nthr_FOM_numeric = np.append(Nthr_FOM_numeric, FOM_n)
        Nthr_FOM_numeric_err = np.append(Nthr_FOM_numeric_err, 0)
        if plot:
            if count == 0: #initialize figure and set style once.
                fs.set_style(textwidth=width)
                plt.figure()
                plt.suptitle('EJ321P Neutron detector thr [TAGGED]')
            plt.subplot(10,2,count+1)
            plt.step(x, y, color='black')
            plt.plot(x, promath.gaussFunc(x, popt_g[0], popt_g[1], popt_g[2]), color='tomato',lw=2)
            plt.plot(x, promath.gaussFunc(x, popt_n[0], popt_n[1], popt_n[2]), color='royalblue',lw=2)
            plt.text(0.02, np.max(y)*1.05, f'FOM:singleGauss: {round(FOM_n,2)}$\pm${round(0,2)}', **titleParamters)
            plt.xlim([0, 0.75])
            plt.ylim([0, np.max(y)*1.3])

    return pd.DataFrame({'YAPthr_FOM': YAPthr_FOM_integral, 
                        'YAPthr_FOM_err': YAPthr_FOM_integral_err, 
                        'Nthr_FOM': Nthr_FOM_numeric, 
                        'Nthr_FOM_err': Nthr_FOM_numeric_err})

# ┌─┐┌─┐┬─┐┬─┐┌─┐┬  ┌─┐┌┬┐┌─┐┌┬┐  ┌─┐┌─┐  ┌─┐┬┌─┐┬ ┬┬─┐┌─┐┌─┐
# │  │ │├┬┘├┬┘├┤ │  ├─┤ │ ├┤  ││  ├─┘└─┐  ├┤ ││ ┬│ │├┬┘├┤ └─┐
# └─┘└─┘┴└─┴└─└─┘┴─┘┴ ┴ ┴ └─┘─┴┘  ┴  └─┘  └  ┴└─┘└─┘┴└─└─┘└─┘
def NE213A_PS_correlated(plot=True, width=345):
    """
    returns FOM for both YAP thresholds and Neutron thresholds as pandas DataFrame
    """
    print('correlated PS figure: NE213A')
    xlim = [0.05, 0.607]

    start_g = 0.17
    stop_g = 0.27
    start_n = 0.3
    stop_n = 0.5
    lim_g = [start_g, stop_g]
    lim_n = [start_n, stop_n]
    
    #############################################
    ### NEUTRON DETECTOR THRESHOLD FIGURE #######
    #############################################
    Nthr_FOM_numeric = np.empty(0)
    Nthr_FOM_numeric_err = np.empty(0)
    Nthr_FOM_fit = np.empty(0)
    Nthr_FOM_fit_err = np.empty(0)

    # ToTn = np.empty(0)
    # NPGn = np.empty(0)
    # NPGn2 = np.empty(0)
    # parameters = np.array([[1120, 0.2413, 0.1, 4000, 0.421, 0.1], #thr = 500
    #                         [707, 0.242, 0.05, 2032, 0.425, 0.1], #thr = 1000
    #                         [459, 0.241, 0.1, 1450, 0.420, 0.1], #thr = 1500
    #                         [1100, 0.245, 0.1, 3870, 0.423, 0.1], #thr = 2000
    #                         [1100, 0.245, 0.1, 3870, 0.423, 0.1], #thr = 2500
    #                         [1100, 0.245, 0.1, 3870, 0.423, 0.1], #thr = 3000
    #                         [1100, 0.245, 0.1, 3870, 0.423, 0.1], #thr = 3500
    #                         [1100, 0.245, 0.1, 3870, 0.423, 0.1], #thr = 4000
    #                         [150, 0.237, 0.1, 311, 0.423, 0.1], #thr = 4500
    #                         [150, 0.245, 0.1, 311, 0.394, 0.1], #thr = 5000
    #                         [150, 0.245, 0.1, 311, 0.394, 0.1], #thr = 5500
    #                         [150, 0.245, 0.1, 311, 0.394, 0.1], #thr = 6000
    #                         [150, 0.245, 0.1, 311, 0.394, 0.1], #thr = 6500
    #                         [79, 0.236, 0.1, 58, 0.389, 0.1], #thr = 7000
    #                         [75, 0.233, 0.05, 22, 0.393, 0.05], #thr = 7500
    #                         [59, 0.219, 0.05, 15, 0.38, 0.05], #thr = 8000
    #                         [40, 0.225, 0.05, 15, 0.38, 0.05], #thr = 8500
    #                         [38, 0.221, 0.05, 5, 0.38, 0.05], #thr = 9000
    #                         [22, 0.223, 0.05, 14, 0.392, 0.05], #thr = 9500
    #                         [16, 0.221, 0.05, 10, 0.391, 0.05]]) #thr = 10000
    yellowColor = 'tomato'
    greenColor = 'tomato'
    redColor = 'tomato'
    colorPalette = [yellowColor, yellowColor, greenColor, greenColor, greenColor, greenColor, greenColor, greenColor, greenColor, greenColor, greenColor, greenColor, greenColor, greenColor, redColor, redColor, redColor, redColor, redColor, redColor]

    for count, currenThr in enumerate(np.arange(250, 4250, 250)):
        # print(parameters[count])
        x, yg, yn = prodata.YAPmergerThreshold(path = '/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/sliced/thresholds', type='N', threshold=currenThr)
        popt_g, pcov_g = promath.gaussFit(x, yg, start=start_g, stop=stop_g, error=True)
        popt_n, pcov_n = promath.gaussFit(x, yn, start=start_n, stop=stop_n, error=True)
        #fitted FOM
        FOM_fit = prodata.PSFOM(popt_n[1], popt_g[1], popt_n[2]*2*np.sqrt(2*np.log(2)), popt_g[2]*2*np.sqrt(2*np.log(2)))
        FOM_fit_err = promath.errorPropMulti(FOM_fit, (popt_n[1]-popt_g[1], popt_n[2]+popt_g[2]), (promath.errorPropAdd((pcov_n[1], pcov_g[1])), promath.errorPropAdd((pcov_n[2], pcov_g[2]))))
        Nthr_FOM_fit = np.append(Nthr_FOM_fit, FOM_fit)
        Nthr_FOM_fit_err = np.append(Nthr_FOM_fit_err, FOM_fit_err)

        #numeric FOM
        FOM_numeric, FOM_numeric_err = prodata.PSFOMnumeric(x, x, yn, yg, fitLim_n=lim_n, fitLim_g=lim_g, error=True) #numeric method
        Nthr_FOM_numeric = np.append(Nthr_FOM_numeric, FOM_numeric)
        Nthr_FOM_numeric_err = np.append(Nthr_FOM_numeric_err, FOM_numeric_err)

        # ToTcurrent = prodata.ToT_binned(x, yn, 0.165, 0.284, 0.284, 0.588, ratio=True) #calculate tail-to-total neutron
        # ToTn = np.append(ToTn, ToTcurrent)

        # NPGcurrent = prodata.ToT_binned(x, yn, 0.165, 0.284, 0.284, 0.588, ratio=False) #calculate tail-to-total neutron
        # NPGn = np.append(NPGn, NPGcurrent)

        # NPGcurrent2, popt2, pcov2 = prodata.npgBimodalFit(x, yn, start=0, stop=0, parameters=parameters[count], return_par=True)
        # NPGn2 = np.append(NPGn2, NPGcurrent2)

        if plot:
            if count == 0: #initialize figure and set style once.
                fs.set_style(textwidth=width)
                plt.figure()
                plt.suptitle('NE213A neutron detector thr [CORRELATED]')
            ax = plt.subplot(8, 2, count+1)
            yg, yn = prodata.PSscaler(x, yg, yn, plot=False) #Visually scale data.
            plt.step(x, yg, color='black',alpha=0.5)
            plt.fill_between(x, y1=yg, y2=0, step='pre', color='white', alpha=0.85)
            plt.step(x, yn, color='black',alpha=0.5)
            plt.fill_between(x[0:31], y1=yn[0:31], y2=0, step='pre', color=colorPalette[count], alpha=0.30)
            plt.fill_between(x[30:-1], y1=yn[30:-1], y2=0, step='pre', color='royalblue', alpha=0.80)
            plt.vlines(popt_g[1], ymin=0, ymax=np.max(yg)*1.1, alpha=0.4, color='black')
            plt.vlines(popt_n[1], ymin=0, ymax=np.max(yg)*1.1, alpha=0.4, color='black')
            # plt.plot(x, promath.gaussFunc(x, popt_n[0],popt_n[1],popt_n[2]), color='black', alpha=0.6)
            # plt.plot(x, promath.gaussFunc(x, popt_g[0],popt_g[1],popt_g[2]), color='black', alpha=0.6)
            # plt.plot(x, promath.gaussFuncBimodal(x, popt2[0], popt2[1], popt2[2], popt2[3], popt2[4], popt2[5]), color='purple', alpha=0.6, lw=5)
            # plt.plot(x, promath.gaussFunc(x, popt2[0],popt2[1],popt2[2]), color='black', alpha=0.6, lw=2)
            plt.text(0.07, np.max(yg)*0.35, f'FOM: {round(FOM_numeric,1)}$\pm${round(FOM_numeric_err,1)}', **titleParamters)
            if count+1 >= 15:
                plt.xlabel('PS')
            if count+1 <= 2:
                chi2 = promath.chi2red(yg, promath.gaussFunc(x, popt_g[0],popt_g[1],popt_g[2]), np.sqrt(np.where(yg<=0, 1, yg)), 3)
                plt.text(0.07, np.max(yg)*1.0, f'$\chi^2$ = {round(chi2, 1)}', **titleParamters) 
            calPath = '/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/Ecal'
            plt.text(0.253, np.max(yg)*1.05, f'{round(currenThr/1000,3)} MeVee', **titleParamters)
            plt.xlim(xlim)
            plt.ylim([0, np.max(yg)*1.4])
            plt.subplots_adjust(hspace = .001 ,wspace = .001)
            if (count % 2) != 0: #check if even
                plt.gca().yaxis.set_label_position("right")
                ax.yaxis.tick_right()
                
    return pd.DataFrame({'FOM_numeric': Nthr_FOM_numeric, 
                        'FOM_numeric_err': Nthr_FOM_numeric_err,
                        'FOM_fit':Nthr_FOM_fit,
                        'FOM_fit_err':Nthr_FOM_fit_err})

def EJ331_PS_correlated(plot=True, width=345):
    """
    returns FOM for both YAP thresholds and Neutron thresholds as pandas DataFrame
    """
    print('correlated PS figure: EJ331')
    
    start_g = 0.11
    stop_g = 0.25
    start_n = 0.21
    stop_n = 0.318
    lim_g = [start_g, stop_g]
    lim_n = [start_n, stop_n]

    #############################################
    ### NEUTRON DETECTOR THRESHOLD FIGURE #######
    #############################################
    Nthr_FOM_numeric = np.empty(0)
    Nthr_FOM_numeric_err = np.empty(0)
    Nthr_FOM_fit = np.empty(0)
    Nthr_FOM_fit_err = np.empty(0)

    # ToTn = np.empty(0)
    # NPGn = np.empty(0)
    # NPGn2 = np.empty(0)
    # parameters = np.array([[1120, 0.2413, 0.1, 4000, 0.421, 0.1], #thr = 500
    #                         [707, 0.242, 0.05, 2032, 0.425, 0.1], #thr = 1000
    #                         [459, 0.241, 0.1, 1450, 0.420, 0.1], #thr = 1500
    #                         [1100, 0.245, 0.1, 3870, 0.423, 0.1], #thr = 2000
    #                         [1100, 0.245, 0.1, 3870, 0.423, 0.1], #thr = 2500
    #                         [1100, 0.245, 0.1, 3870, 0.423, 0.1], #thr = 3000
    #                         [1100, 0.245, 0.1, 3870, 0.423, 0.1], #thr = 3500
    #                         [1100, 0.245, 0.1, 3870, 0.423, 0.1], #thr = 4000
    #                         [150, 0.237, 0.1, 311, 0.423, 0.1], #thr = 4500
    #                         [150, 0.245, 0.1, 311, 0.394, 0.1], #thr = 5000
    #                         [150, 0.245, 0.1, 311, 0.394, 0.1], #thr = 5500
    #                         [150, 0.245, 0.1, 311, 0.394, 0.1], #thr = 6000
    #                         [150, 0.245, 0.1, 311, 0.394, 0.1], #thr = 6500
    #                         [79, 0.236, 0.1, 58, 0.389, 0.1], #thr = 7000
    #                         [75, 0.233, 0.05, 22, 0.393, 0.05], #thr = 7500
    #                         [59, 0.219, 0.05, 15, 0.38, 0.05], #thr = 8000
    #                         [40, 0.225, 0.05, 15, 0.38, 0.05], #thr = 8500
    #                         [38, 0.221, 0.05, 5, 0.38, 0.05], #thr = 9000
    #                         [22, 0.223, 0.05, 14, 0.392, 0.05], #thr = 9500
    #                         [16, 0.221, 0.05, 10, 0.391, 0.05]]) #thr = 10000

    for count, currenThr in enumerate(np.arange(250, 4250, 250)):
        # print(parameters[count])
        x, yg, yn = prodata.YAPmergerThreshold(path = '/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/EJ305/sliced/thresholds', type='N', threshold=currenThr)
        popt_g, pcov_g = promath.gaussFit(x, yg, start=start_g, stop=stop_g, error=True)
        popt_n, pcov_n = promath.gaussFit(x, yn, start=start_n, stop=stop_n, error=True)
        #fitted FOM
        FOM_fit = prodata.PSFOM(popt_n[1], popt_g[1], popt_n[2]*2*np.sqrt(2*np.log(2)), popt_g[2]*2*np.sqrt(2*np.log(2)))
        FOM_fit_err = promath.errorPropMulti(FOM_fit, (popt_n[1]-popt_g[1], popt_n[2]+popt_g[2]), (promath.errorPropAdd((pcov_n[1], pcov_g[1])), promath.errorPropAdd((pcov_n[2], pcov_g[2]))))
        Nthr_FOM_fit = np.append(Nthr_FOM_fit, FOM_fit)
        Nthr_FOM_fit_err = np.append(Nthr_FOM_fit_err, FOM_fit_err)

        #numeric FOM
        FOM_numeric, FOM_numeric_err = prodata.PSFOMnumeric(x, x, yn, yg, lim_n=lim_n, lim_g=lim_g, error=True) #numeric method
        Nthr_FOM_numeric = np.append(Nthr_FOM_numeric, FOM_numeric)
        Nthr_FOM_numeric_err = np.append(Nthr_FOM_numeric_err, FOM_numeric_err)

        # ToTcurrent = prodata.ToT_binned(x, yn, 0.165, 0.284, 0.284, 0.588, ratio=True) #calculate tail-to-total neutron
        # ToTn = np.append(ToTn, ToTcurrent)

        # NPGcurrent = prodata.ToT_binned(x, yn, 0.165, 0.284, 0.284, 0.588, ratio=False) #calculate tail-to-total neutron
        # NPGn = np.append(NPGn, NPGcurrent)

        # NPGcurrent2, popt2, pcov2 = prodata.npgBimodalFit(x, yn, start=0, stop=0, parameters=parameters[count], return_par=True)
        # NPGn2 = np.append(NPGn2, NPGcurrent2)

        if plot:
            if count == 0: #initialize figure and set style once.
                fs.set_style(textwidth=width)
                plt.figure()
                plt.suptitle('EJ305 neutron detector thr [CORRELATED]')
            ax = plt.subplot(8, 2, count+1)
            yg, yn = prodata.PSscaler(x, yg, yn, plot=False) #Visually scale data.
            plt.step(x, yg, color='black',alpha=0.5)
            plt.fill_between(x, y1=yg, y2=0, step='pre', color='white', alpha=0.85)
            plt.step(x, yn, color='black',alpha=0.5)
            plt.fill_between(x, y1=yn, y2=0, step='pre', color='royalblue', alpha=0.80)
            plt.vlines(popt_g[1], ymin=0, ymax=np.max(yg)*1.1, alpha=0.4, color='black')
            plt.vlines(popt_n[1], ymin=0, ymax=np.max(yg)*1.1, alpha=0.4, color='black')
            # plt.plot(x, promath.gaussFunc(x, popt_n[0],popt_n[1],popt_n[2]), color='black', alpha=0.6)
            # plt.plot(x, promath.gaussFunc(x, popt_g[0],popt_g[1],popt_g[2]), color='black', alpha=0.6)
            # plt.plot(x, promath.gaussFuncBimodal(x, popt2[0], popt2[1], popt2[2], popt2[3], popt2[4], popt2[5]), color='purple', alpha=0.6, lw=5)
            # plt.plot(x, promath.gaussFunc(x, popt2[0],popt2[1],popt2[2]), color='black', alpha=0.6, lw=2)
            plt.text(0.07, np.max(yg)*0.35, f'FOM: {round(FOM_numeric,1)}$\pm${round(FOM_numeric_err,1)}', **titleParamters)
            if count+1 >= 15:
                plt.xlabel('PS')
            if count+1 <= 2:
                chi2 = promath.chi2red(yg, promath.gaussFunc(x, popt_g[0],popt_g[1],popt_g[2]), np.sqrt(np.where(yg<=0, 1, yg)), 3)
                plt.text(0.07, np.max(yg)*1.0, f'$\chi^2$ = {round(chi2, 1)}', **titleParamters) 
            calPath = '/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/Ecal'
            plt.text(0.253, np.max(yg)*1.05, f'{round(currenThr/1000,3)} MeVee', **titleParamters)
            plt.xlim(xlim)
            plt.ylim([0, np.max(yg)*1.4])
            plt.subplots_adjust(hspace = .001 ,wspace = .001)
            if (count % 2) != 0: #check if even
                plt.gca().yaxis.set_label_position("right")
                ax.yaxis.tick_right()
                
    return pd.DataFrame({'Nthr_FOM': Nthr_FOM_numeric, 
                        'Nthr_FOM_err': Nthr_FOM_numeric_err,
                        'Nthr_FOM_fit':Nthr_FOM_fit,
                        'Nthr_FOM_fit_err':Nthr_FOM_fit_err})  

def EJ305_PS_correlated(plot=True, width=345):
    """
    returns FOM for both YAP thresholds and Neutron thresholds as pandas DataFrame
    """
    print('correlated PS figure: EJ305')
    xlim = [0.05, 0.4]

    start_g = 0.16
    stop_g = 0.20
    start_n = 0.2
    stop_n = 0.3
    lim_g = [start_g, stop_g]
    lim_n = [start_n, stop_n]
    
    #############################################
    ### NEUTRON DETECTOR THRESHOLD FIGURE #######
    #############################################
    Nthr_FOM_numeric = np.empty(0)
    Nthr_FOM_numeric_err = np.empty(0)
    Nthr_FOM_fit = np.empty(0)
    Nthr_FOM_fit_err = np.empty(0)

    ToTn = np.empty(0)
    NPGn = np.empty(0)
    NPGn2 = np.empty(0)
    parameters = np.array([[1120, 0.2413, 0.1, 4000, 0.245, 0.1], #thr = 500
                            [707, 0.242, 0.05, 2032, 0.245, 0.1], #thr = 1000
                            [459, 0.241, 0.1, 1450, 0.245, 0.1], #thr = 1500
                            [1100, 0.245, 0.1, 3870, 0.245, 0.1], #thr = 2000
                            [1100, 0.245, 0.1, 3870, 0.245, 0.1], #thr = 2500
                            [1100, 0.245, 0.1, 3870, 0.245, 0.1], #thr = 3000
                            [1100, 0.245, 0.1, 3870, 0.245, 0.1], #thr = 3500
                            [1100, 0.245, 0.1, 3870, 0.245, 0.1], #thr = 4000
                            [150, 0.237, 0.1, 311, 0.245, 0.1], #thr = 4500
                            [150, 0.245, 0.1, 311, 0.245, 0.1], #thr = 5000
                            [150, 0.245, 0.1, 311, 0.245, 0.1], #thr = 5500
                            [150, 0.245, 0.1, 311, 0.245, 0.1], #thr = 6000
                            [150, 0.245, 0.1, 311, 0.245, 0.1], #thr = 6500
                            [79, 0.236, 0.1, 58, 0.245, 0.1], #thr = 7000
                            [75, 0.233, 0.05, 22, 0.245, 0.05], #thr = 7500
                            [59, 0.219, 0.05, 15, 0.245, 0.05], #thr = 8000
                            [40, 0.225, 0.05, 15, 0.245, 0.05], #thr = 8500
                            [38, 0.221, 0.05, 5, 0.245, 0.05], #thr = 9000
                            [22, 0.223, 0.05, 14, 0.245, 0.05], #thr = 9500
                            [16, 0.221, 0.05, 10, 0.245, 0.05]]) #thr = 10000
    yellowColor = 'tomato'
    greenColor = 'tomato'
    redColor = 'tomato'
    colorPalette = [yellowColor, yellowColor, greenColor, greenColor, greenColor, greenColor, greenColor, greenColor, greenColor, greenColor, greenColor, greenColor, greenColor, greenColor, redColor, redColor, redColor, redColor, redColor, redColor]

    for count, currenThr in enumerate(np.arange(500, 8500, 500)):
        # print(parameters[count])
        x, yg, yn = prodata.YAPmergerThreshold(path = '/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/EJ305/sliced/thresholds', type='N', threshold=currenThr)
        popt_g, pcov_g = promath.gaussFit(x, yg, start=start_g, stop=stop_g, error=True)
        popt_n, pcov_n = promath.gaussFit(x, yn, start=start_n, stop=stop_n, error=True)
        #fitted FOM
        FOM_fit = prodata.PSFOM(popt_n[1], popt_g[1], popt_n[2]*2*np.sqrt(2*np.log(2)), popt_g[2]*2*np.sqrt(2*np.log(2)))
        FOM_fit_err = promath.errorPropMulti(FOM_fit, (popt_n[1]-popt_g[1], popt_n[2]+popt_g[2]), (promath.errorPropAdd((pcov_n[1], pcov_g[1])), promath.errorPropAdd((pcov_n[2], pcov_g[2]))))
        Nthr_FOM_fit = np.append(Nthr_FOM_fit, FOM_fit)
        Nthr_FOM_fit_err = np.append(Nthr_FOM_fit_err, FOM_fit_err)

        #numeric FOM
        FOM_n, FOM_n_err = prodata.PSFOMnumeric(x, x, yn, yg, lim_n=lim_n, lim_g=lim_g, error=True) #numeric method
        Nthr_FOM_numeric = np.append(Nthr_FOM_numeric, FOM_n)
        Nthr_FOM_numeric_err = np.append(Nthr_FOM_numeric_err, FOM_n_err)

        ToTcurrent = prodata.ToT_binned(x, yn, 0.165, 0.284, 0.284, 0.588, ratio=True) #calculate tail-to-total neutron
        ToTn = np.append(ToTn, ToTcurrent)

        NPGcurrent = prodata.ToT_binned(x, yn, 0.165, 0.284, 0.284, 0.588, ratio=False) #calculate tail-to-total neutron
        NPGn = np.append(NPGn, NPGcurrent)

        # NPGcurrent2, popt2, pcov2 = prodata.npgBimodalFit(x, yn, start=0, stop=0, parameters=parameters[count], return_par=True)
        # NPGn2 = np.append(NPGn2, NPGcurrent2)

        if plot:
            if count == 0: #initialize figure and set style once.
                fs.set_style(textwidth=width)
                plt.figure()
                plt.suptitle('EJ305 neutron detector thr [CORRELATED]')
            ax = plt.subplot(8, 2, count+1)
            plt.step(x, yg, color='black',alpha=0.5)
            plt.fill_between(x, y1=yg, y2=0, step='pre', color='white', alpha=0.85)
            plt.step(x, yn, color='black',alpha=0.5)
            # plt.fill_between(x[0:31], y1=yn[0:31], y2=0, step='pre', color=colorPalette[count], alpha=0.30)
            plt.fill_between(x, y1=yn, y2=0, step='pre', color='royalblue', alpha=0.80)
            plt.vlines(popt_g[1], ymin=0, ymax=np.max(yg)*1.1, alpha=0.4, color='black')
            plt.vlines(popt_n[1], ymin=0, ymax=np.max(yg)*1.1, alpha=0.4, color='black')
            plt.plot(x, promath.gaussFunc(x, popt_n[0],popt_n[1],popt_n[2]), color='black', alpha=0.6)
            plt.plot(x, promath.gaussFunc(x, popt_g[0],popt_g[1],popt_g[2]), color='black', alpha=0.6)
            # plt.plot(x, promath.gaussFuncBimodal(x, popt2[0], popt2[1], popt2[2], popt2[3], popt2[4], popt2[5]), color='purple', alpha=0.6, lw=5)
            # plt.plot(x, promath.gaussFunc(x, popt2[0],popt2[1],popt2[2]), color='black', alpha=0.6, lw=2)
            plt.text(0.07, np.max(yn)*0.35, f'FOM: {round(FOM_n,1)}$\pm${round(FOM_n_err,1)}', **titleParamters)
            if count+1 >= 15:
                plt.xlabel('PS')
            if count+1 <= 2:
                chi2 = promath.chi2red(yg, promath.gaussFunc(x, popt_g[0],popt_g[1],popt_g[2]), np.sqrt(np.where(yg<=0, 1, yg)), 3)
                plt.text(0.07, np.max(yn)*1.0, f'$\chi^2$ = {round(chi2, 1)}', **titleParamters) 
            calPath = '/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/Ecal'
            plt.text(0.253, np.max(yn)*1.05, f'{round(prodata.calibrateMyDetector(calPath, currenThr*att_factor_12dB),2)} MeVee', **titleParamters)
            plt.xlim(xlim)
            plt.ylim([0, np.max(yn)*1.4])
            plt.subplots_adjust(hspace = .001 ,wspace = .001)
            if (count % 2) != 0: #check if even
                plt.gca().yaxis.set_label_position("right")
                ax.yaxis.tick_right()

    return pd.DataFrame({'FOM_numeric': Nthr_FOM_numeric, 
                        'FOM_numeric_err': Nthr_FOM_numeric_err,
                        'FOM_fit':Nthr_FOM_fit,
                        'FOM_fit_err':Nthr_FOM_fit_err,
                        'ToT': ToTn,
                        'NPG': NPGn})

def EJ321P_PS_correlated(plot=True, width=345):
    """
    returns FOM for both YAP thresholds and Neutron thresholds as pandas DataFrame
    """
    print('correlated PS figure: EJ321P')
    #########################################
    ### YAP DETECTOR THRESHOLD FIGURE #######
    #########################################
    YAPthr_FOM_integral = np.empty(0)
    YAPthr_FOM_integral_err = np.empty(0)
    
    start_g = 0.2
    stop_g = 0.33
    start_n = 0.15
    stop_n = 0.38
    lim_g = [start_g, stop_g]
    lim_n = [start_n, stop_n]

    for count, currenThr in enumerate(np.arange(500, 8500, 500)):
        x, yg, yn = prodata.YAPmergerThreshold(path = '/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/EJ321P/sliced/thresholds', type='YAP', threshold=currenThr)
        
        FOM_yap, FOM_yap_err = prodata.PSFOMnumeric(x, x, yn, yg, lim_n=lim_n, lim_g=lim_g, error=True)
        YAPthr_FOM_integral = np.append(YAPthr_FOM_integral, FOM_yap)
        YAPthr_FOM_integral_err = np.append(YAPthr_FOM_integral_err, FOM_yap_err)
        if plot:
            if count == 0: #initialize figure and set style once.
                fs.set_style(textwidth=width)
                plt.figure()
                plt.suptitle('EJ321P YAP detector thr [CORRELATED]')
            plt.subplot(10,2,count+1)
            plt.step(x, yg, color='tomato')
            plt.step(x, yn, color='royalblue')
            plt.text(0.02, np.max(yg)*1.05, f'FOM: {round(FOM_yap,2)}$\pm${round(FOM_yap_err,2)}', **titleParamters)
            plt.xlim([0, 0.75])
            plt.ylim([0, np.max(yg)*1.4])

    #############################################
    ### NEUTRON DETECTOR THRESHOLD FIGURE #######
    #############################################
    Nthr_FOM_numeric = np.empty(0)
    Nthr_FOM_numeric_err = np.empty(0)

    for count, currenThr in enumerate(np.arange(500, 8500, 500)):
        x, yg, yn = prodata.YAPmergerThreshold(path = '/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/EJ321P/sliced/thresholds', type='N', threshold=currenThr)
        
        FOM_n, FOM_n_err = prodata.PSFOMnumeric(x, x, yn, yg, lim_n=lim_n, lim_g=lim_g, error=True)
        Nthr_FOM_numeric = np.append(Nthr_FOM_numeric, FOM_n)
        Nthr_FOM_numeric_err = np.append(Nthr_FOM_numeric_err, FOM_n_err)
        if plot:
            if count == 0: #initialize figure and set style once.
                fs.set_style(textwidth=width)
                plt.figure()
                plt.suptitle('EJ321P neutron detector thr [CORRELATED]')
            plt.subplot(10,2,count+1)
            plt.step(x, yg, color='tomato')
            plt.step(x, yn, color='royalblue')
            plt.text(0.02, np.max(yg)*1.05, f'FOM: {round(FOM_n,2)}$\pm${round(FOM_n_err,2)}', **titleParamters)
            plt.xlim([0, 0.75])
            plt.ylim([0, np.max(yg)*1.4])

    return pd.DataFrame({'YAPthr_FOM': YAPthr_FOM_integral, 
                        'YAPthr_FOM_err': YAPthr_FOM_integral_err, 
                        'Nthr_FOM': Nthr_FOM_numeric, 
                        'Nthr_FOM_err': Nthr_FOM_numeric_err})

def NE213A_EJ305_PS_correlated(plot=True, width=345):
    """
    Makes figure for NE213A and EJ305 correlated PS data
    """
    stdFactor = 3

    #fit limits for NE213A
    NE213A_start_g = 0.19
    NE213A_stop_g = 0.25
    NE213A_start_n = 0.32
    NE213A_stop_n = 0.42
    NE213A_lim_g = [NE213A_start_g, NE213A_stop_g]
    NE213A_lim_n = [NE213A_start_n, NE213A_stop_n]

    #fit limits for EJ305
    EJ305_start_g = 0.146
    EJ305_stop_g = 0.196
    EJ305_start_n = 0.203
    EJ305_stop_n = 0.270
    EJ305_lim_g = [EJ305_start_g, EJ305_stop_g]
    EJ305_lim_n = [EJ305_start_n, EJ305_stop_n]

    #Define containers:
    NE213A_FOM = np.empty(0)
    NE213A_FOM_err = np.empty(0)

    NE213A_mean_n = np.empty(0)
    NE213A_err_mean_n = np.empty(0)
    NE213A_std_n = np.empty(0)
    NE213A_err_std_n = np.empty(0)

    NE213A_mean_g = np.empty(0)
    NE213A_err_mean_g = np.empty(0)
    NE213A_std_g = np.empty(0)
    NE213A_err_std_g = np.empty(0)
    
    EJ305_FOM = np.empty(0)
    EJ305_FOM_err = np.empty(0)

    EJ305_mean_n = np.empty(0)
    EJ305_err_mean_n = np.empty(0)
    EJ305_std_n = np.empty(0)
    EJ305_err_std_n = np.empty(0)

    EJ305_mean_g = np.empty(0)
    EJ305_err_mean_g = np.empty(0)
    EJ305_std_g = np.empty(0)
    EJ305_err_std_g = np.empty(0)

    thr = np.empty(0)


    print('correlated PS figure: NE213A and EJ305')

    count = np.arange(1, 31, 1)
    idx = 0
    for currentThr in np.arange(250, 4000, 250): 

        #NE213A
        print('###################################')
        print(f'-> Current threshold: {currentThr}')
        x = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/sliced/thresholds/keV{currentThr}/x.npy')
        yg = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/sliced/thresholds/keV{currentThr}/yGamma.npy')
        yn = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/sliced/thresholds/keV{currentThr}/yNeutron.npy')
        
        # x, yg, yn = prodata.YAPmergerThreshold(path = '/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/sliced/thresholds', type='N', threshold=currentThr)
        #Slice away NPG data for visulization purposes
        x_new, yn_new = prodata.binDataSlicer(x, yn, 0.3, 1)

        #calculate FOM for NE213A
        NE213A_FOM_current, NE213A_FOM_err_current, NE213A_param = prodata.PSFOMnumeric(x_new, x, yn_new, yg, fitLim_n=NE213A_lim_n, fitLim_g=NE213A_lim_g, stdFactor=stdFactor, error=True, plot=False, return_param=True)

        NE213A_FOM = np.append(NE213A_FOM, NE213A_FOM_current)
        NE213A_FOM_err = np.append(NE213A_FOM_err, NE213A_FOM_err_current)

        NE213A_mean_n = np.append(NE213A_mean_n, NE213A_param['mean_n'])
        NE213A_err_mean_n = np.append(NE213A_err_mean_n, NE213A_param['err_mean_n'])
        NE213A_std_n = np.append(NE213A_std_n, NE213A_param['std_n'])
        NE213A_err_std_n = np.append(NE213A_err_std_n, NE213A_param['err_std_n'])

        NE213A_mean_g = np.append(NE213A_mean_g, NE213A_param['mean_g'])
        NE213A_err_mean_g = np.append(NE213A_err_mean_g, NE213A_param['err_mean_g'])
        NE213A_std_g = np.append(NE213A_std_g, NE213A_param['std_g'])
        NE213A_err_std_g = np.append(NE213A_err_std_g, NE213A_param['err_std_g'])

        if plot:
            if idx == 0: #initialize figure and set style once.
                fs.set_style(textwidth=width)
                plt.figure()

            ax = plt.subplot(15, 2, count[idx])

            yg, yn_new = prodata.PSscaler(x, yg, yn_new, plot=False) #Visually scale data.                        


            plt.step(x, yg, color='black', lw=0.5, zorder=1)
            plt.fill_between(x, y1=yg, y2=0, step='pre', color='tomato', alpha=0.8, zorder=1)
            plt.step(x_new, yn_new, color='black', lw=0.5, zorder=3)
            plt.fill_between(x_new, y1=yn_new, y2=0, step='pre', color='royalblue', alpha=0.6, zorder=2)

            if currentThr == 3750:
                plt.xlabel('PS')
            else:
                plt.tick_params(axis = 'x',
                            which = 'both',
                            top = False,
                            bottom = False,
                            labelbottom = False)
            plt.tick_params(axis = 'y',
                            which = 'both',
                            left = False,
                            top = False,
                            labelleft = False)
            plt.xlim([0.1, 0.6])
            plt.ylim([0, np.max(yg)*1.4])
            plt.subplots_adjust(hspace = .001 ,wspace = .001)

        
        #EJ305
        x = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/EJ305/sliced/thresholds/keV{currentThr}/x.npy')
        yg = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/EJ305/sliced/thresholds/keV{currentThr}/yGamma.npy')
        yn = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/EJ305/sliced/thresholds/keV{currentThr}/yNeutron.npy')

        #Scale data visually
        yg, yn = prodata.PSscaler(x, yg, yn, plot=False) #Visually scale data.
               
        #Calculate FOM double Gaussian method
        doubleGaussParameters = pd.read_pickle('/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/EJ305/doubleGaussParameters/fitParams_thr')
        query = f'energy=={currentThr}'
        param = [   doubleGaussParameters.query(query).amp1.item(),  #amp 1
                    doubleGaussParameters.query(query).mean1.item(), #mean 1
                    doubleGaussParameters.query(query).std1.item(),  #std 1
                    doubleGaussParameters.query(query).amp2.item(),  #amp 2
                    doubleGaussParameters.query(query).mean2.item(), #mean 2
                    doubleGaussParameters.query(query).std2.item()]  #std 2
        # print(param)

        EJ305_FOM_DG_current, EJ305_FOM_err_DG_current, EJ305_param_DG = prodata.PSFOMdoubleGauss(x, x, yn, yg, fitLim_n=[0.100, 0.350], fitLim_g=EJ305_lim_g, param=param, error=True, plot=False, return_param=True)
        
        g = promath.gaussFunc(x, EJ305_param_DG['amp_n1'].item(), EJ305_param_DG['mean_n1'].item(), EJ305_param_DG['std_n1'].item()) #calculate NPG gaussin y-values for subtraction

        #calculate FOM for EJ305
        EJ305_FOM_current, EJ305_FOM_err_current, EJ305_param = prodata.PSFOMnumeric(x, x, (yn-g), yg, fitLim_n=EJ305_lim_n, fitLim_g=EJ305_lim_g, stdFactor=stdFactor, error=True, plot=False, return_param=True)
        EJ305_FOM = np.append(EJ305_FOM, EJ305_FOM_current)
        EJ305_FOM_err = np.append(EJ305_FOM_err, EJ305_FOM_err_current)

        EJ305_mean_n = np.append(EJ305_mean_n, EJ305_param['mean_n'])
        EJ305_err_mean_n = np.append(EJ305_err_mean_n, EJ305_param['err_mean_n'])
        EJ305_std_n = np.append(EJ305_std_n, EJ305_param['std_n'])
        EJ305_err_std_n = np.append(EJ305_err_std_n, EJ305_param['err_std_n'])

        EJ305_mean_g = np.append(EJ305_mean_g, EJ305_param['mean_g'])
        EJ305_err_mean_g = np.append(EJ305_err_mean_g, EJ305_param['err_mean_g'])
        EJ305_std_g = np.append(EJ305_std_g, EJ305_param['std_g'])
        EJ305_err_std_g = np.append(EJ305_err_std_g, EJ305_param['err_std_g'])

        thr = np.append(thr, currentThr)

        if plot:               
            ax = plt.subplot(15, 2, count[idx]+1) 
            
            
            #Slice away NPG data for visulization purposes
            

            if currentThr>6000:
                plt.step(x, yg, color='white', lw=0.5, zorder=1)
                plt.fill_between(x, y1=yg, y2=0, step='pre', color='white', alpha=0.8, zorder=1)
                plt.step(x, (yn-g), color='white', lw=0.5, zorder=3)
                plt.fill_between(x, y1=(yn-g), y2=0, step='pre', color='white', alpha=0.6, zorder=2)
            else:
                plt.step(x, yg, color='black', lw=0.5, zorder=1)
                plt.fill_between(x, y1=yg, y2=0, step='pre', color='tomato', alpha=0.8, zorder=1)
                plt.step(x, (yn-g), color='black', lw=0.5, zorder=3)
                plt.fill_between(x, y1=(yn-g), y2=0, step='pre', color='royalblue', alpha=0.6, zorder=2)


            if currentThr == 3750:
                plt.xlabel('PS')
            else:
                plt.tick_params(axis = 'x',
                            which = 'both',
                            top = False,
                            bottom = False,
                            labelbottom = False)
            plt.tick_params(axis = 'y',
                            which = 'both',
                            left = False,
                            top = False,
                            labelleft = False)
            plt.xlim([0.1, 0.35])
            plt.ylim([0, np.max(yg)*1.4])
            plt.subplots_adjust(hspace = .001 ,wspace = .001)
            idx += 2

    return pd.DataFrame({'threshold': thr,
                        'NE213A_mean_n': NE213A_mean_n,
                        'NE213A_err_mean_n': NE213A_err_mean_n,
                        'NE213A_std_n': NE213A_std_n,
                        'NE213A_err_std_n': NE213A_err_std_n,
                        'NE213A_mean_g': NE213A_mean_g,
                        'NE213A_err_mean_g': NE213A_err_mean_g,
                        'NE213A_std_g': NE213A_std_g,
                        'NE213A_err_std_g': NE213A_err_std_g,
                        'NE213A_FOM': NE213A_FOM, 
                        'NE213A_FOM_err': NE213A_FOM_err,
                        'EJ305_mean_n': EJ305_mean_n,
                        'EJ305_err_mean_n': EJ305_err_mean_n,
                        'EJ305_std_n': EJ305_std_n,
                        'EJ305_err_std_n': EJ305_err_std_n,
                        'EJ305_mean_g': EJ305_mean_g,
                        'EJ305_err_mean_g': EJ305_err_mean_g,
                        'EJ305_std_g': EJ305_std_g,
                        'EJ305_err_std_g': EJ305_err_std_g,
                        'EJ305_FOM': EJ305_FOM, 
                        'EJ305_FOM_err': EJ305_FOM_err})
# ┌┐┌┌─┐┬ ┬┌┬┐┬─┐┌─┐┌┐┌  ┌┬┐┌─┐┌─┐  ┌─┐┬  ┬┌─┐┌─┐  ┌─┐┬┌─┐┬ ┬┬─┐┌─┐┌─┐
# │││├┤ │ │ │ ├┬┘│ ││││   │ │ │├┤   └─┐│  ││  ├┤   ├┤ ││ ┬│ │├┬┘├┤ └─┐
# ┘└┘└─┘└─┘ ┴ ┴└─└─┘┘└┘   ┴ └─┘└    └─┘┴─┘┴└─┘└─┘  └  ┴└─┘└─┘┴└─└─┘└─┘

def NE213A_PS_TOF(plot=True, width=345):
    """
    returns FOM for different neutron kinetic energies
    """
    print('Neutron Kinetic energy PS figure: NE213A')
    xlim = [0.05, 0.607]

    start_g = 0.14
    stop_g = 0.29
    start_n = 0.3
    stop_n = 0.57
    lim_g = [start_g, stop_g]
    lim_n = [start_n, stop_n]
    
    #############################################
    ### NEUTRON DETECTOR THRESHOLD FIGURE #######
    #############################################
    Nthr_FOM_numeric = np.empty(0)
    Nthr_FOM_numeric_err = np.empty(0)
    Nthr_FOM_fit = np.empty(0)
    Nthr_FOM_fit_err = np.empty(0)

    for count, E in enumerate(np.arange(1500, 6500, 250)):
        # print(parameters[count])
        x = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/sliced/PS/MeV{E}/x.npy')
        yg = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/sliced/PS/MeV{E}/yGamma.npy')
        yn = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/sliced/PS/MeV{E}/yNeutron.npy')
            
        popt_g, pcov_g = promath.gaussFit(x, yg, start=start_g, stop=stop_g, error=True)
        popt_n, pcov_n = promath.gaussFit(x, yn, start=start_n, stop=stop_n, error=True)
        #fitted FOM
        FOM_fit = prodata.PSFOM(popt_n[1], popt_g[1], popt_n[2]*2*np.sqrt(2*np.log(2)), popt_g[2]*2*np.sqrt(2*np.log(2)))
        FOM_fit_err = promath.errorPropMulti(FOM_fit, (popt_n[1]-popt_g[1], popt_n[2]+popt_g[2]), (promath.errorPropAdd((pcov_n[1], pcov_g[1])), promath.errorPropAdd((pcov_n[2], pcov_g[2]))))
        Nthr_FOM_fit = np.append(Nthr_FOM_fit, FOM_fit)
        Nthr_FOM_fit_err = np.append(Nthr_FOM_fit_err, FOM_fit_err)

        #numeric FOM
        FOM_numeric, FOM_numeric_err = prodata.PSFOMnumeric(x, x, yn, yg, fitLim_n=lim_n, fitLim_g=lim_g, stdFactor=3, error=True, plot=False) #numeric method
        Nthr_FOM_numeric = np.append(Nthr_FOM_numeric, FOM_numeric)
        Nthr_FOM_numeric_err = np.append(Nthr_FOM_numeric_err, FOM_numeric_err)

        if plot:
            if count == 0: #initialize figure and set style once.
                fs.set_style(textwidth=width)
                plt.figure()
                plt.suptitle('NE213A neutron detector [Tn]')
            ax = plt.subplot(20, 2, count+1)
            yg, yn = prodata.PSscaler(x, yg, yn, plot=False) #Visually scale data.
            plt.step(x, yg, color='black',alpha=0.5)
            plt.fill_between(x, y1=yg, y2=0, step='pre', color='white', alpha=0.85)
            plt.step(x, yn, color='black',alpha=0.5)
            plt.vlines(popt_g[1], ymin=0, ymax=np.max(yg)*1.1, alpha=0.4, color='black')
            plt.vlines(popt_n[1], ymin=0, ymax=np.max(yg)*1.1, alpha=0.4, color='black')
            plt.text(0.07, np.max(yg)*0.35, f'FOM: {round(FOM_numeric,1)}$\pm${round(FOM_numeric_err,1)}', **titleParamters)
            if count+1 >= 15:
                plt.xlabel('PS')
            if count+1 <= 2:
                chi2 = promath.chi2red(yg, promath.gaussFunc(x, popt_g[0],popt_g[1],popt_g[2]), np.sqrt(np.where(yg<=0, 1, yg)), 3)
                plt.text(0.07, np.max(yg)*1.0, f'$\chi^2$ = {round(chi2, 1)}', **titleParamters) 
            plt.text(0.253, np.max(yg)*1.05, f'{round(E/1000,3)} MeVee', **titleParamters)
            plt.xlim(xlim)
            plt.ylim([0, np.max(yg)*1.4])
            plt.subplots_adjust(hspace = .001 ,wspace = .001)
            if (count % 2) != 0: #check if even
                plt.gca().yaxis.set_label_position("right")
                ax.yaxis.tick_right()
                
    return pd.DataFrame({'FOM_numeric': Nthr_FOM_numeric, 
                        'FOM_numeric_err': Nthr_FOM_numeric_err,
                        'FOM_fit':Nthr_FOM_fit,
                        'FOM_fit_err':Nthr_FOM_fit_err})

def NE213A_EJ305_PS_TOF(plot=True, width=345):
    """
    Makes figure for NE213A and EJ305 TOF slice PS data
    """
    stdFactor = 3

    #fit limits for NE213A
    NE213A_start_g = 0.159
    NE213A_stop_g = 0.300
    NE213A_start_n = 0.359
    NE213A_stop_n = 0.520
    NE213A_lim_g = [NE213A_start_g, NE213A_stop_g]
    NE213A_lim_n = [NE213A_start_n, NE213A_stop_n]

    #fit limits for EJ305
    EJ305_start_g = 0.146
    EJ305_stop_g = 0.196
    EJ305_start_n = 0.200
    EJ305_stop_n = 0.300
    EJ305_lim_g = [EJ305_start_g, EJ305_stop_g]
    EJ305_lim_n = [EJ305_start_n, EJ305_stop_n]


    #Define containers:
    NE213A_FOM = np.empty(0)
    NE213A_FOM_err = np.empty(0)

    NE213A_mean_n = np.empty(0)
    NE213A_err_mean_n = np.empty(0)
    NE213A_std_n = np.empty(0)
    NE213A_err_std_n = np.empty(0)

    NE213A_mean_g = np.empty(0)
    NE213A_err_mean_g = np.empty(0)
    NE213A_std_g = np.empty(0)
    NE213A_err_std_g = np.empty(0)
    
    EJ305_FOM = np.empty(0)
    EJ305_FOM_err = np.empty(0)

    EJ305_mean_n = np.empty(0)
    EJ305_err_mean_n = np.empty(0)
    EJ305_std_n = np.empty(0)
    EJ305_err_std_n = np.empty(0)

    EJ305_mean_g = np.empty(0)
    EJ305_err_mean_g = np.empty(0)
    EJ305_std_g = np.empty(0)
    EJ305_err_std_g = np.empty(0)

    EJ305_FOM_DG = np.empty(0)
    EJ305_FOM_err_DG = np.empty(0)

    EJ305_mean_n_DG = np.empty(0)
    EJ305_err_mean_n_DG = np.empty(0)
    EJ305_std_n_DG = np.empty(0)
    EJ305_err_std_n_DG = np.empty(0)

    EJ305_mean_g_DG = np.empty(0)
    EJ305_err_mean_g_DG = np.empty(0)
    EJ305_std_g_DG = np.empty(0)
    EJ305_err_std_g_DG = np.empty(0)

    energy = np.empty(0)

    count = np.arange(1, 41, 1)
    idx = 0
    for E in np.arange(1500, 6250, 250):
        if idx == 0: #initialize figure and set style once.
            fs.set_style(textwidth=width)
            plt.figure()

        print(f'NE213A, idx={idx},count={count[idx]}')
        print(f'NE213A, ENERGY={E} MeV')
        x = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/sliced/PS/MeV{E}/x.npy')
        yg = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/sliced/PS/MeV{E}/yGamma.npy')
        yn = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/sliced/PS/MeV{E}/yNeutron.npy')
        
        NPGthr = 0.3 
        #Slice away NPG data
        x_new, yn_new = prodata.binDataSlicer(x, yn, 0.3, 1)

        #Calculate FOM
        NE213A_FOM_current, NE213A_FOM_err_current, NE213A_param = prodata.PSFOMnumeric(x_new, x, yn_new, yg, fitLim_n=NE213A_lim_n, fitLim_g=NE213A_lim_g, stdFactor=stdFactor, error=True, plot=plot, return_param=True)
        NE213A_FOM = np.append(NE213A_FOM, NE213A_FOM_current)
        NE213A_FOM_err = np.append(NE213A_FOM_err, NE213A_FOM_err_current)

        NE213A_mean_n = np.append(NE213A_mean_n, NE213A_param['mean_n'])
        NE213A_err_mean_n = np.append(NE213A_err_mean_n, NE213A_param['err_mean_n'])
        NE213A_std_n = np.append(NE213A_std_n, NE213A_param['std_n'])
        NE213A_err_std_n = np.append(NE213A_err_std_n, NE213A_param['err_std_n'])

        NE213A_mean_g = np.append(NE213A_mean_g, NE213A_param['mean_g'])
        NE213A_err_mean_g = np.append(NE213A_err_mean_g, NE213A_param['err_mean_g'])
        NE213A_std_g = np.append(NE213A_std_g, NE213A_param['std_g'])
        NE213A_err_std_g = np.append(NE213A_err_std_g, NE213A_param['err_std_g'])

        yg, yn = prodata.PSscaler(x, yg, yn, plot=False) #Visually scale data.
        
        #Slice away NPG data for visulization purposes
        x_new, yn_new = prodata.binDataSlicer(x, yn, 0.3, 1)

        if not plot:
            ax = plt.subplot(19, 2, count[idx])
            plt.step(x, yg, color='black', lw=0.5, zorder=1)
            plt.fill_between(x, y1=yg, y2=0, step='pre', color='tomato', alpha=0.8, zorder=1)
            plt.step(x_new, yn_new, color='black', lw=0.5, zorder=3)
            plt.fill_between(x_new, y1=yn_new, y2=0, step='pre', color='royalblue', alpha=0.6, zorder=2)
            # plt.yscale('log')
            if E == 6000:
                plt.xlabel('PS')
            else:
                plt.tick_params(axis = 'x',
                                which = 'both',
                                top = False,
                                bottom = False,
                                labelbottom = False)
            plt.tick_params(axis = 'y',
                            which = 'both',
                            left = False,
                            top = False,
                            labelleft = False)
            plt.xlim([0.1, 0.625])
            # plt.ylim([0, np.max(yg)*1.4])
            plt.ylim([10, np.max(yg)*1.4])
            plt.subplots_adjust(hspace = .001 ,wspace = .001)
    


        print(f'EJ305, idx={idx},count={count[idx]}')
        print(f'EJ305, ENERGY={E} MeV')
        x = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/EJ305/sliced/PS/MeV{E}/x.npy')
        yg = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/EJ305/sliced/PS/MeV{E}/yGamma.npy')
        yn = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/EJ305/sliced/PS/MeV{E}/yNeutron.npy')

        NPGthr = 0.0 
        #Slice away NPG data
        x_new, yn_new = prodata.binDataSlicer(x, yn, 0, 1)
        
        #Visually scale data
        yg, yn_new = prodata.PSscaler(x, yg, yn_new, plot=False) #Visually scale data.

        #Calculate FOM double Gaussian method
        doubleGaussParameters = pd.read_pickle('/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/EJ305/doubleGaussParameters/fitParams_TOF')
        query = f'energy=={E}'
        param = [   doubleGaussParameters.query(query).amp1.item(),  #amp 1
                    doubleGaussParameters.query(query).mean1.item(), #mean 1
                    doubleGaussParameters.query(query).std1.item(),  #std 1
                    doubleGaussParameters.query(query).amp2.item(),  #amp 2
                    doubleGaussParameters.query(query).mean2.item(), #mean 2
                    doubleGaussParameters.query(query).std2.item()]  #std 2

        EJ305_FOM_DG_current, EJ305_FOM_err_DG_current, EJ305_param_DG = prodata.PSFOMdoubleGauss(x_new, x, yn_new, yg, fitLim_n=[0.100, 0.350], fitLim_g=EJ305_lim_g, param=param, error=True, plot=False, return_param=True)

        EJ305_FOM_DG = np.append(EJ305_FOM_DG, EJ305_FOM_DG_current)
        EJ305_FOM_err_DG = np.append(EJ305_FOM_err_DG, EJ305_FOM_err_DG_current)
        
        EJ305_mean_n_DG = np.append(EJ305_mean_n_DG, EJ305_param_DG['mean_n2'])
        EJ305_err_mean_n_DG = np.append(EJ305_err_mean_n_DG, EJ305_param_DG['err_mean_n2'])
        EJ305_std_n_DG = np.append(EJ305_std_n_DG, EJ305_param_DG['std_n2'])
        EJ305_err_std_n_DG = np.append(EJ305_err_std_n_DG, EJ305_param_DG['err_std_n2'])

        EJ305_mean_g_DG = np.append(EJ305_mean_g_DG, EJ305_param_DG['mean_g'])
        EJ305_err_mean_g_DG = np.append(EJ305_err_mean_g_DG, EJ305_param_DG['err_mean_g'])
        EJ305_std_g_DG = np.append(EJ305_std_g_DG, EJ305_param_DG['std_g'])
        EJ305_err_std_g_DG = np.append(EJ305_err_std_g_DG, EJ305_param_DG['err_std_g'])
        
        #Calculate FOM numeric method
        g = promath.gaussFunc(x_new, EJ305_param_DG['amp_n1'].item(), EJ305_param_DG['mean_n1'].item(), EJ305_param_DG['std_n1'].item()) #calculate NPG gaussin y-values for subtraction
        EJ305_FOM_current, EJ305_FOM_err_current, EJ305_param = prodata.PSFOMnumeric(x_new, x, yn_new-g, yg, fitLim_n=EJ305_lim_n, fitLim_g=EJ305_lim_g, stdFactor=stdFactor, error=True, plot=plot, return_param=True)
        EJ305_FOM = np.append(EJ305_FOM, EJ305_FOM_current)
        EJ305_FOM_err = np.append(EJ305_FOM_err, EJ305_FOM_err_current)
        
        EJ305_mean_n = np.append(EJ305_mean_n, EJ305_param['mean_n'])
        EJ305_err_mean_n = np.append(EJ305_err_mean_n, EJ305_param['err_mean_n'])
        EJ305_std_n = np.append(EJ305_std_n, EJ305_param['std_n'])
        EJ305_err_std_n = np.append(EJ305_err_std_n, EJ305_param['err_std_n'])

        EJ305_mean_g = np.append(EJ305_mean_g, EJ305_param['mean_g'])
        EJ305_err_mean_g = np.append(EJ305_err_mean_g, EJ305_param['err_mean_g'])
        EJ305_std_g = np.append(EJ305_std_g, EJ305_param['std_g'])
        EJ305_err_std_g = np.append(EJ305_err_std_g, EJ305_param['err_std_g'])
        
        energy = np.append(energy, E)
        
        if not plot:
            ax = plt.subplot(19, 2, count[idx]+1) 
            plt.step(x, yg, color='black', lw=0.5, zorder=1)
            plt.fill_between(x, y1=yg, y2=0, step='pre', color='tomato', alpha=0.8, zorder=1)
            plt.step(x_new, yn_new-g, color='black', lw=0.5, zorder=3)
            plt.fill_between(x_new, y1=yn_new-g, y2=0, step='pre', color='royalblue', alpha=0.6, zorder=2)
            # plt.yscale('log')
            if E == 6000:
                plt.xlabel('PS')
            else:
                plt.tick_params(axis = 'x',
                                which = 'both',
                                top = False,
                                bottom = False,
                                labelbottom = False)
            plt.tick_params(axis = 'y',
                            which = 'both',
                            left = False,
                            top = False,
                            labelleft = False)
            plt.xlim([0.01, 0.437])
            # plt.ylim([0, np.max(yg)*1.4])
            plt.ylim([6, np.max(yg)*1.4])
            plt.subplots_adjust(hspace = .001 ,wspace = .001)
            idx += 2
    

    return pd.DataFrame({'energy': energy,
                        'NE213A_mean_n': NE213A_mean_n,
                        'NE213A_err_mean_n': NE213A_err_mean_n,
                        'NE213A_std_n': NE213A_std_n,
                        'NE213A_err_std_n': NE213A_err_std_n,
                        'NE213A_mean_g': NE213A_mean_g,
                        'NE213A_err_mean_g': NE213A_err_mean_g,
                        'NE213A_std_g': NE213A_std_g,
                        'NE213A_err_std_g': NE213A_err_std_g,
                        'NE213A_FOM': NE213A_FOM, 
                        'NE213A_FOM_err': NE213A_FOM_err,
                        'EJ305_mean_n': EJ305_mean_n,
                        'EJ305_err_mean_n': EJ305_err_mean_n,
                        'EJ305_std_n': EJ305_std_n,
                        'EJ305_err_std_n': EJ305_err_std_n,
                        'EJ305_mean_g': EJ305_mean_g,
                        'EJ305_err_mean_g': EJ305_err_mean_g,
                        'EJ305_std_g': EJ305_std_g,
                        'EJ305_err_std_g': EJ305_err_std_g,
                        'EJ305_FOM': EJ305_FOM, 
                        'EJ305_FOM_err': EJ305_FOM_err,
                        'EJ305_mean_n': EJ305_mean_n_DG,
                        'EJ305_err_mean_n_DG': EJ305_err_mean_n_DG,
                        'EJ305_std_n_DG': EJ305_std_n_DG,
                        'EJ305_err_std_n_DG': EJ305_err_std_n_DG,
                        'EJ305_mean_g_DG': EJ305_mean_g_DG,
                        'EJ305_err_mean_g_DG': EJ305_err_mean_g_DG,
                        'EJ305_std_g_DG': EJ305_std_g_DG,
                        'EJ305_err_std_g_DG': EJ305_err_std_g_DG,
                        'EJ305_FOM_DG': EJ305_FOM_DG, 
                        'EJ305_FOM_err_DG': EJ305_FOM_err_DG})

 