#!/usr/bin/env python3
from re import I
import sys
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
from numpy.core.arrayprint import SubArrayFormat
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit

# from matplotlib.colors import LinearSegmentedColormap
"""
Script will loop through all data for NE 213A and look at QDC values around NPG bump with two differnt maximum neutron kinetic energies.
"""
# IMPORT FROM LIBRARY DIRECTORY
sys.path.insert(0, "../library/")
sys.path.insert(0, "../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "../")
sys.path.insert(0, "data_analysis/")

import processing_math as promath
import processing_data as prodata
import processing_pd as propd
import figure_style as fs

########################################
######## Setting parameters ############
########################################
distance = 0.959
att_factor_12dB = 3.98
att_factor_16dB = 6.31
gTOFRange = [1e-9, 6e-9]
randTOFRange = [-710e-9,-10e-9]
nTOFRange = [28.1e-9, 60e-9]

###############################################################
################## threshold processing #######################
###############################################################

numBins = np.arange(0, 25, 0.1)
yNeutronFin_thr1 = np.zeros(len(numBins)-1)
yNeutronFin_thr2 = np.zeros(len(numBins)-1)
yNeutronRaw_thr1 = np.zeros(len(numBins)-1)
yNeutronRaw_thr2 = np.zeros(len(numBins)-1)
yRandFin_thr1 = np.zeros(len(numBins)-1)
yRandFin_thr2 = np.zeros(len(numBins)-1)

Rn = (nTOFRange[1]-nTOFRange[0])/(randTOFRange[1]-randTOFRange[0])
Nthr = 0 #Threshold for Neutron detector [MeVee]
YAPthr = 3 #Threshold for YAP detector [MeVee]
tofEmax1 = 2.5 #maximum neutron kinetic energy to include in first cut [MeV]
tofEmax2 = 6 #maximum neutron kinetic energy to include in second cut [MeV]

path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/'

for tofCh in np.arange(2,6,1):
    for i in range(20, 161, 20): #161
        print('---------------------------------------')
        print(f'Now reading for YAP channel: {tofCh}')
        print(f'Runs: {np.arange(2121+i-20,2121+i,1)[0]}-{np.arange(2121+i-20,2121+i,1)[-1]}')
        
        df = propd.load_parquet_merge(path, np.arange(2121+i-20,2121+i,1), keep_col=[f'qdc_ps_ch1', f'qdc_lg_ch1', f'tof_ch{tofCh}', f'amplitude_ch{tofCh}', f'qdc_lg_ch{tofCh}'], full=False)
        flashrange = {f'tof_ch{tofCh}':[60, 76]}
        prodata.tof_calibration(df, flashrange, distance, phcut=125, calUnit=1e-9, energyCal=True)
        df.qdc_lg_ch1 =             prodata.calibrateMyDetector('/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/Ecal',         df.qdc_lg_ch1*att_factor_12dB)
        df[f'qdc_lg_ch{tofCh}'] =   prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/YAP/Ecal/ch{tofCh}', df[f'qdc_lg_ch{tofCh}'])
        
        #threshold 1
        yNeutron1, xNeutron1 =    np.histogram(df.query(f'qdc_lg_ch{tofCh}>{YAPthr} and qdc_lg_ch1>{Nthr} and tofE_ch{tofCh}<{tofEmax1} and tof_ch{tofCh}>{nTOFRange[0]} and tof_ch{tofCh}<{nTOFRange[1]} and qdc_ps_ch1>0 and qdc_ps_ch1<0.3').qdc_lg_ch1, bins=numBins)
        yRand1, xRand1 =          np.histogram(df.query(f'qdc_lg_ch{tofCh}>{YAPthr} and qdc_lg_ch1>{Nthr} and tofE_ch{tofCh}<{tofEmax1} and tof_ch{tofCh}>{randTOFRange[0]} and tof_ch{tofCh}<{randTOFRange[1]} and qdc_ps_ch1>0 and qdc_ps_ch1<0.3').qdc_lg_ch1, bins=numBins)
        yNeutronFin_thr1 += yNeutron1-Rn*yRand1
        yNeutronRaw_thr1 += yNeutron1
        yRandFin_thr1 += yRand1 

        #threshold 2
        yNeutron2, xNeutron2 =    np.histogram(df.query(f'qdc_lg_ch{tofCh}>{YAPthr} and qdc_lg_ch1>{Nthr} and tofE_ch{tofCh}<{tofEmax2} and tof_ch{tofCh}>{nTOFRange[0]} and tof_ch{tofCh}<{nTOFRange[1]} and qdc_ps_ch1>0 and qdc_ps_ch1<0.3').qdc_lg_ch1, bins=numBins)
        yRand2, xRand2 =          np.histogram(df.query(f'qdc_lg_ch{tofCh}>{YAPthr} and qdc_lg_ch1>{Nthr} and tofE_ch{tofCh}<{tofEmax2} and tof_ch{tofCh}>{randTOFRange[0]} and tof_ch{tofCh}<{randTOFRange[1]} and qdc_ps_ch1>0 and qdc_ps_ch1<0.3').qdc_lg_ch1, bins=numBins)
        yNeutronFin_thr2 += yNeutron2-Rn*yRand2
        yNeutronRaw_thr2 += yNeutron2
        yRandFin_thr2 += yRand2

# np.save(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/NPG/QDC/yNPGthr1', yNeutronFin_thr1)
# np.save(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/NPG/QDC/yNPGthr2', yNeutronFin_thr2)
np.save(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/NPG/QDC/yNPGRawthr1', yNeutronRaw_thr1)
np.save(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/NPG/QDC/yNPGRawthr2', yNeutronRaw_thr2)
# np.save(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/NPG/QDC/yNPGRandthr1', yRandFin_thr1)
# np.save(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/NPG/QDC/NPGRandthr2', yRandFin_thr2)
np.save(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/NPG/QDC/xNPG', prodata.getBinCenters(xNeutron1))

# plt.step(prodata.getBinCenters(xNeutron), yRandFin_thr1, label='Random Nthr > 1 MeVee', lw=2)
# plt.hist(df.query('qdc_lg_ch1>1').qdc_lg_ch1, bins=500, range=[0,20], histtype='step')
# plt.step(prodata.getBinCenters(xNeutron), yRandFin_thr2, label='Random Nthr > 3 MeVee')
# plt.step(prodata.getBinCenters(xNeutron2), yNeutronFin_thr1, label='tofE < 2.5 MeV', lw=2)
# plt.step(prodata.getBinCenters(xNeutron2), yNeutronFin_thr2, label='tofE < 6.0 MeV', lw=2)
plt.step(prodata.getBinCenters(xNeutron2), yNeutronRaw_thr1, label='tofE < 2.5 MeV', lw=1)
plt.step(prodata.getBinCenters(xNeutron2), yNeutronRaw_thr2, label='tofE < 6.0 MeV', lw=1)

plt.xlabel('Energy [MeVee]')
plt.ylabel('Counts')
plt.legend()
plt.show()