#!/usr/bin/env python3
"""
Work with Hanno 2022-01-14

"""


from re import I
import sys
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
from numpy.core.arrayprint import SubArrayFormat
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit

# from matplotlib.colors import LinearSegmentedColormap


# IMPORT FROM LIBRARY DIRECTORY
sys.path.insert(0, "../library/")
sys.path.insert(0, "../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "../")
sys.path.insert(0, "data_analysis/")

import processing_math as promath
import processing_data as prodata
import processing_pd as propd
import processing_simulation as prosim
import figure_style as fs
import script_FOM_figures_import as FOMfigs
########################################
######## Setting parameters ############
########################################

PMT_gain = 4e6 #Nominal gain is 7'000'000 at nominal A/lm (model: 9821B p1)
att_factor_16dB = 6.31
att_factor_12dB = 3.98
q = 1.60217662E-19 #charge of a single electron

distance = 0.959
gTOFRange = [1e-9, 6e-9] #Define range of gamma-flash
randTOFRange = [-710e-9,-10e-9] #Define range of random events
nTOFRange = [30e-9, 40e-9]
gLength = np.abs(gTOFRange[1]-gTOFRange[0]) #Calculate the lenght of cut for gammas
nLength = np.abs(nTOFRange[1]-nTOFRange[0])
randLength = np.abs(randTOFRange[1]-randTOFRange[0]) #Calculate lenght of cut for random events
dE = 0.25

################################################################
############ Figure 8 ##########################################
############# PS investigation "non prompt gamma-rays" #########
################################################################
path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop
df = propd.load_parquet_merge(path, np.arange(2121,2121+40,1), keep_col=['qdc_lg_ch2','amplitude_ch2','tof_ch2','qdc_lg_ch1','qdc_ps_ch1'], full=False)
flashrange = {'tof_ch2':[60, 76]} #gamma flash fitting ranges
prodata.tof_calibration(df, flashrange, distance, phcut=125, calUnit=1e-9, energyCal=True)

#TODO: make 2x1

tofCh = 2
numBins = np.arange(0.0, 0.75, 0.01)
titleParamters = dict(ha='left', va='center', fontsize=10, color='black')

xlimit = [0.05, 0.75]
start_g = 0.18
stop_g = 0.256
start_n = 0.35
stop_n = 0.54
lim_g = [start_g, stop_g]
lim_n = [start_n, stop_n]
###############################################
fs.set_style(textwidth=345)
f, (a0, a1) = plt.subplots(2, 1, gridspec_kw={'height_ratios': [1, 3.5]})
# query = f'qdc_lg_ch{tofCh}>{6450} and qdc_lg_ch1>{500}'
query = f'qdc_lg_ch{tofCh}>{0} and qdc_lg_ch1>{0}'
y, x = np.histogram(df.query(query).qdc_ps_ch1, bins=numBins)
x = prodata.getBinCenters(x)
a0.text(0.065, np.max(y)*1.01, f'Singles', **titleParamters)
a0.step(x, y, color='black')
a0.set_ylabel('counts')
a0.set_xlim([xlimit[0], xlimit[1]])
a0.set_ylim([0, np.max(y)*1.3])
#############################################

# plt.subplot(2, 1, 2)
x, yg, yn = prodata.YAPmergerThreshold(path = '/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/sliced/thresholds', type='N', threshold=1000)
yg, yn = prodata.PSscaler(x, yg, yn, plot=False) #scale data to be roughly same height
a1.text(0.065, np.max(yg)*1.05, f'Correlated', **titleParamters)
a1.step(x, yg, color='tomato')
a1.step(x, yn, color='royalblue')
a1.set_xlim([xlimit[0], xlimit[1]])
a1.set_ylim([0, np.max(yg)*1.2])
a1.set_xlabel('PS [arb. units]')
a1.set_ylabel('counts')
plt.subplots_adjust(hspace = .001, wspace = .001)


###############################################################
############# Figure 8 insert #################################
############# NPG QDC spectra #################################
###############################################################
from mpl_toolkits.axes_grid.inset_locator import (inset_axes, InsetPosition, mark_inset) 

x = np.load('/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/NPG/QDC/xNPG.npy')
y1 = np.load('/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/NPG/QDC/yNPGRawthr1.npy')
y2 = np.load('/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/NPG/QDC/yNPGRawthr2.npy')

a2 = plt.axes([0,0,1,1]) #create axis for inset
# Manually set the position and relative size of the inset axes within ax1
ip = InsetPosition(a1, [0.5, 0.5, 0.9, 0.9]) #x0,y0 w,h
a2.set_axes_locator(ip) 
a2.step(x, y1)
a2.step(x, y2)
a2.set_xlim([-0.1, 6])
a2.set_ylim([10,100000])
a2.set_yscale('log')
a2.set_xlabel('Energy [MeV$_e$$_e$]')
a2.set_ylabel('counts')
plt.show()