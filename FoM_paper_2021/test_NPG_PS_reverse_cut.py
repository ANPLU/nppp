distance = 0.959
att_factor_12dB = 3.98
att_factor_16dB = 6.31
gTOFRange = [1e-9, 6e-9]
randTOFRange = [-710e-9,-10e-9]
nTOFRange = [28.1e-9, 60e-9]

nLength = np.abs(nTOFRange[1]-nTOFRange[0])
gLength = np.abs(gTOFRange[1]-gTOFRange[0])
randLength = np.abs(randTOFRange[1]-randTOFRange[0])



# numBins = np.arange(0.02, 0.75, 0.01)
# query = 
# yRand,xRamd = np.histogram(dfNE213A.query(query), bins=numBins)
# Nthr = 1500
# Ythr = 6450
# x1, yg1, yn1 = prodata.randomTOFSubtraction(   dfNE213A.query(f"qdc_lg_ch{tofCh}>{Ythr} and qdc_lg_ch1>{Nthr} and tof_ch{tofCh}>{gTOFRange[0]} and tof_ch{tofCh}<{gTOFRange[1]}"), 
#                                             dfNE213A.query(f"qdc_lg_ch{tofCh}>{Ythr} and qdc_lg_ch1>{Nthr} and tof_ch{tofCh}>{nTOFRange[0]} and tof_ch{tofCh}<{nTOFRange[1]}"), 
#                                             dfNE213A.query(f"qdc_lg_ch{tofCh}>{Ythr} and qdc_lg_ch1>{Nthr} and tof_ch{tofCh}>{randTOFRange[0]} and tof_ch{tofCh}<{randTOFRange[1]}"),
#                                             gLength, nLength, randLength, numBins, plot=False)
# plt.title("0-6.5 MeV")
# plt.step(x1,yg1, color='tomato', lw=2)
# plt.step(x1,yn1, color='royalblue', lw=2)
# plt.show()






nTOFRange2 = [45e-9, 60e-9]
nLength2 = np.abs(nTOFRange2[1]-nTOFRange2[0])
# numBins = np.arange(0.02, 0.75, 0.01)
# Nthr = 1500
# Ythr = 6450
# x2, yg2, yn2 = prodata.randomTOFSubtraction(   dfNE213A.query(f"qdc_lg_ch{tofCh}>{Ythr} and qdc_lg_ch1>{Nthr} and tof_ch{tofCh}>{gTOFRange[0]} and tof_ch{tofCh}<{gTOFRange[1]}"), 
#                                             dfNE213A.query(f"qdc_lg_ch{tofCh}>{Ythr} and qdc_lg_ch1>{Nthr} and tof_ch{tofCh}>{nTOFRange2[0]} and tof_ch{tofCh}<{nTOFRange2[1]}"), 
#                                             dfNE213A.query(f"qdc_lg_ch{tofCh}>{Ythr} and qdc_lg_ch1>{Nthr} and tof_ch{tofCh}>{randTOFRange[0]} and tof_ch{tofCh}<{randTOFRange[1]}"),
#                                             gLength, nLength2, randLength, numBins, plot=False)
# plt.step(x1,yn1, color='royalblue', lw=2,alpha=.6)
# plt.step(x2,yn2, color='royalblue', lw=2)

# plt.show()


#LOOP OVER MORE DATA!
path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop
tofCh = 2
numBins = np.arange(0.02, 0.75, 0.01)
Nthr = 1500
Ythr = 6450
yn1Fin = np.zeros(len(numBins)-1)
yn2Fin = np.zeros(len(numBins)-1)
yg1Fin = np.zeros(len(numBins)-1)
yg2Fin = np.zeros(len(numBins)-1)

for tofCh in np.arange(2, 6, 1):
    if tofCh == 2: #baseline threshold to use in units of QDC for the YAP detector (ch2=6450, ch3=6985, ch4=7850, ch5=8750)
        Ythr = 6450 
    elif tofCh == 3:
        Ythr = 6985
    elif tofCh == 4:
        Ythr = 7850
    elif tofCh == 5:
        Ythr = 8750
    for i in np.arange(20,160,20):
        print(2121+i-20, 2121+i)
        dfNE213A = propd.load_parquet_merge(path, np.arange(2121+i-20,2121+i,1), keep_col=[f'amplitude_ch{tofCh}',f'tof_ch{tofCh}','qdc_lg_ch1', f'qdc_lg_ch{tofCh}', 'qdc_ps_ch1'], full=False)
        flashrange = {f"tof_ch{tofCh}":[60, 76]} #gamma flash fitting ranges
        prodata.tof_calibration(dfNE213A, flashrange, distance, phcut=125, calUnit=1e-9, energyCal=True)
        #processing full neutron energies

        x1, yg1, yn1 = prodata.randomTOFSubtraction(   dfNE213A.query(f"qdc_lg_ch{tofCh}>{Ythr} and qdc_lg_ch1>{Nthr} and tof_ch{tofCh}>{gTOFRange[0]} and tof_ch{tofCh}<{gTOFRange[1]}"), 
                                                    dfNE213A.query(f"qdc_lg_ch{tofCh}>{Ythr} and qdc_lg_ch1>{Nthr} and tof_ch{tofCh}>{nTOFRange[0]} and tof_ch{tofCh}<{nTOFRange[1]}"), 
                                                    dfNE213A.query(f"qdc_lg_ch{tofCh}>{Ythr} and qdc_lg_ch1>{Nthr} and tof_ch{tofCh}>{randTOFRange[0]} and tof_ch{tofCh}<{randTOFRange[1]}"),
                                                    gLength, nLength, randLength, numBins, plot=False)
                                        
        #processing neutron energiee <2.4
        x2, yg2, yn2 = prodata.randomTOFSubtraction(   dfNE213A.query(f"qdc_lg_ch{tofCh}>{Ythr} and qdc_lg_ch1>{Nthr} and tof_ch{tofCh}>{gTOFRange[0]} and tof_ch{tofCh}<{gTOFRange[1]}"), 
                                                dfNE213A.query(f"qdc_lg_ch{tofCh}>{Ythr} and qdc_lg_ch1>{Nthr} and tof_ch{tofCh}>{nTOFRange2[0]} and tof_ch{tofCh}<{nTOFRange2[1]}"), 
                                                dfNE213A.query(f"qdc_lg_ch{tofCh}>{Ythr} and qdc_lg_ch1>{Nthr} and tof_ch{tofCh}>{randTOFRange[0]} and tof_ch{tofCh}<{randTOFRange[1]}"),
                                                gLength, nLength2, randLength, numBins, plot=False)

        yn1Fin += yn1
        yn2Fin += yn2
        yg1Fin += yg1
        yg2Fin += yg2

plt.step(x1, yn1Fin/7, color='royalblue', lw=2, alpha=0.5, label="T$_n$ = 0 to 6.5 MeV")
plt.step(x1, yn2Fin, color='royalblue', lw=2, alpha=1.00, label="T$_n$ = 0 to 2.5 MeV")
plt.step(x1, yg2Fin, color='tomato', lw=2, alpha=1.00)
plt.ylabel('counts')
plt.xlabel('PS')
plt.legend()
plt.show()