#!/usr/bin/env python3
from re import I
import sys
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
from numpy.core.arrayprint import SubArrayFormat
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit

# from matplotlib.colors import LinearSegmentedColormap


# IMPORT FROM LIBRARY DIRECTORY
sys.path.insert(0, "../library/")
sys.path.insert(0, "../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "../")
sys.path.insert(0, "data_analysis/")

import processing_math as promath
import processing_data as prodata
import processing_pd as propd
import figure_style as fs

########################################
######## DETECTOR SETTINGS #############
########################################

detector = "NE213A"
run1 = 2121
numRuns = 141
runList = np.arange(run1, run1+numRuns)

gainOffsets = np.array([1.        , 1.00449786, 1.02776289, 0.99543515, 0.99513401,
                        0.99373714, 1.01271988, 1.00555114, 1.00767454, 1.03011807,
                        1.03737901, 1.02339094, 1.01367455, 1.103476  , 1.01138804,
                        1.09498013, 0.99517056, 0.98747555, 1.01479879, 1.03541272,
                        1.01354815, 0.99957959, 0.97865142, 0.99600754, 1.00958113,
                        1.02217856, 1.03640639, 1.01903062, 0.99881911, 1.00631287,
                        0.9909422 , 1.00325513, 1.00190923, 0.99810678, 0.97982168,
                        1.0108797 , 1.01108049, 1.01447416, 1.04953386, 0.99252985,
                        0.98535389, 0.98752831, 0.96838927, 0.9648048 , 0.97339146,
                        0.98834493, 0.97955184, 0.9898073 , 0.98643835, 0.99461799,
                        1.00308614, 0.97061575, 1.01527638, 0.98378853, 0.98731022,
                        1.00510757, 0.98845017, 0.9743922 , 0.98245988, 0.98122753,
                        0.9691665 , 1.01991096, 0.98006913, 0.97535042, 0.99039761,
                        0.97771579, 0.97718466, 0.98845839, 0.97691246, 0.9784261 ,
                        0.99225482, 0.97087123, 0.97269272, 0.9912359 , 0.97617339,
                        1.00866114, 1.00550252, 0.99468125, 0.98253334, 0.97681113,
                        0.97632393, 0.96692358, 1.00644259, 1.00780908, 0.99361568,
                        0.97946456, 1.00178607, 0.98710232, 0.98580734, 1.02220332,
                        0.97701412, 0.98474256, 1.02551657, 0.97654263, 0.97509727,
                        1.00753811, 0.99528997, 0.97050454, 0.98562478, 0.9745818 ,
                        0.98730201, 0.97500751, 0.98760836, 0.97151791, 0.98084076,
                        0.98470394, 0.97723897, 0.99826923, 0.97042334, 0.98152384,
                        0.98179468, 1.00139174, 0.9907689 , 0.9982554 , 0.99021647,
                        0.98391455, 0.97733494, 0.96542459, 0.96496404, 0.97209604,
                        1.03033098, 0.98416693, 0.97510294, 0.97231365, 0.97591612,
                        0.96953299, 0.97641848, 0.96724081, 0.9753956 , 0.96514133,
                        0.976637  , 0.98310287, 0.98319185, 0.98085286, 0.96911062,
                        0.96717393, 0.98366547, 0.96810717, 0.97483992, 0.97766298,
                        0.98462132])

distance = 0.959
att_factor_11dB = 3.55
att_factor_12dB = 3.98
att_factor_19dB = 8.91
gTOFRange = [0.5e-9, 6e-9]
randTOFRange = [-710e-9,-10e-9]
nTOFRange = [28.1e-9, 80e-9]#[28.1e-9, 60e-9]

nLength = np.abs(nTOFRange[1]-nTOFRange[0])
gLength = np.abs(gTOFRange[1]-gTOFRange[0])
randLength = np.abs(randTOFRange[1]-randTOFRange[0])

###############################################################
################## threshold processing #######################
###############################################################
plot = False
path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop

y2ThrQDC = 6450 #QDC threshold in channels
y3ThrQDC = 6985 #QDC threshold in channels
y4ThrQDC = 7850 #QDC threshold in channels
y5ThrQDC = 8750 #QDC threshold in channels

numBins = np.arange(0, 1, 0.01)

NeutronThresholds = np.arange(0.25, 5.25, 0.25) #Array of neutron thresholds in units of MeV

gammaPS_final = pd.DataFrame()
neutronPS_final = pd.DataFrame()
for i in NeutronThresholds*1000:
    gammaPS_final[f'keVee{int(i)}'] = np.zeros(len(numBins)-1)
    neutronPS_final[f'keVee{int(i)}'] = np.zeros(len(numBins)-1)

dataSize = 5

for i in np.arange(0, numRuns, dataSize):
    print('-------------------------------------------------')
    currentRunList = np.split(runList, [i, i+dataSize])[1] #split runlist into section of 20 events
    print(f'---> Now reading in runs {currentRunList[0]}-{currentRunList[-1]} of {runList[-1]}')
    PuBe_tof_data =   propd.load_parquet_merge_gainadjust(path, currentRunList, keep_col=[ 'qdc_lg_ch1',
                                                                                        'qdc_lg_ch2',
                                                                                        'qdc_lg_ch3',
                                                                                        'qdc_lg_ch4',
                                                                                        'qdc_lg_ch5',
                                                                                        'qdc_ps_ch1', 
                                                                                        'tof_ch2', 
                                                                                        'tof_ch3', 
                                                                                        'tof_ch4', 
                                                                                        'tof_ch5'], 
                                                                                        gainOffsets = gainOffsets[i: i + dataSize],
                                                                                        full=False)

    #~~~~~~~~~~~~~~~~~~~~~~~~
    #      RESET INDEX
    #~~~~~~~~~~~~~~~~~~~~~~~~
    PuBe_tof_data = PuBe_tof_data.reset_index(drop=True)

    #~~~~~~~~~~~~~~~~~~~~~~~~
    #    CALIBRATING ToF
    #~~~~~~~~~~~~~~~~~~~~~~~~
    flashrange = {'tof_ch2':[55, 72], 'tof_ch3':[55, 72], 'tof_ch4':[55, 72], 'tof_ch5':[55, 72]} #gamma flash fitting ranges
    prodata.tof_calibration(PuBe_tof_data, flashrange, distance, phcut=0, qdccut=2000, calUnit=1e-9, numBins=1000, energyCal=True)
    
    #~~~~~~~~~~~~~~~~~~~~~~~~
    #    CALIBRATING QDC
    #~~~~~~~~~~~~~~~~~~~~~~~~
    PuBe_tof_data.qdc_lg_ch1 = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/{detector}/Ecal', PuBe_tof_data.qdc_lg_ch1*att_factor_12dB)
    
    ###################
    ###################
    ###################

    # nThrQDC = NeutronThresholds[0] #QDC threshold in MeVee
    for nThrQDC in NeutronThresholds:
        print(f'Processing threshold = {nThrQDC} MeVee')
    
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        #   Get random data
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        y2_random_ps = PuBe_tof_data.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch2>{y2ThrQDC} and tof_ch2>{randTOFRange[0]} and tof_ch2<={randTOFRange[1]}').qdc_ps_ch1
        y3_random_ps = PuBe_tof_data.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch3>{y3ThrQDC} and tof_ch3>{randTOFRange[0]} and tof_ch3<={randTOFRange[1]}').qdc_ps_ch1
        y4_random_ps = PuBe_tof_data.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch4>{y4ThrQDC} and tof_ch4>{randTOFRange[0]} and tof_ch4<={randTOFRange[1]}').qdc_ps_ch1
        y5_random_ps = PuBe_tof_data.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch5>{y5ThrQDC} and tof_ch5>{randTOFRange[0]} and tof_ch5<={randTOFRange[1]}').qdc_ps_ch1
        #check for random multiplicity
        mpIdxRandom = prodata.uniqueEventHelper(y2_random_ps, y3_random_ps, y4_random_ps, y5_random_ps)


        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        #   Get correlated gamma PS
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        y2_gamma_ps = PuBe_tof_data.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch2>{y2ThrQDC} and tof_ch2>{gTOFRange[0]} and tof_ch2<={gTOFRange[1]}').qdc_ps_ch1
        y3_gamma_ps = PuBe_tof_data.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch3>{y3ThrQDC} and tof_ch3>{gTOFRange[0]} and tof_ch3<={gTOFRange[1]}').qdc_ps_ch1
        y4_gamma_ps = PuBe_tof_data.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch4>{y4ThrQDC} and tof_ch4>{gTOFRange[0]} and tof_ch4<={gTOFRange[1]}').qdc_ps_ch1
        y5_gamma_ps = PuBe_tof_data.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch5>{y5ThrQDC} and tof_ch5>{gTOFRange[0]} and tof_ch5<={gTOFRange[1]}').qdc_ps_ch1
        #check for random multiplicity
        mpIdxGamma = prodata.uniqueEventHelper(y2_gamma_ps, y3_gamma_ps, y4_gamma_ps, y5_gamma_ps)
        
        x_gamma_ps, y_gamma_ps = prodata.sliceRandomSubtraction(y2_gamma_ps.drop(mpIdxGamma, errors='ignore'),
                                                                y3_gamma_ps.drop(mpIdxGamma, errors='ignore'),
                                                                y4_gamma_ps.drop(mpIdxGamma, errors='ignore'),
                                                                y5_gamma_ps.drop(mpIdxGamma, errors='ignore'),
                                                                y2_random_ps.drop(mpIdxRandom, errors='ignore'), 
                                                                y3_random_ps.drop(mpIdxRandom, errors='ignore'),
                                                                y4_random_ps.drop(mpIdxRandom, errors='ignore'),
                                                                y5_random_ps.drop(mpIdxRandom, errors='ignore'),
                                                                numBins = numBins, 
                                                                dataLength = gLength, 
                                                                randLength = randLength)

        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        #   Get correlated neutron PS
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        y2_neutron_ps = PuBe_tof_data.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch2>{y2ThrQDC} and tof_ch2>{nTOFRange[0]} and tof_ch2<={nTOFRange[1]}').qdc_ps_ch1
        y3_neutron_ps = PuBe_tof_data.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch3>{y3ThrQDC} and tof_ch3>{nTOFRange[0]} and tof_ch3<={nTOFRange[1]}').qdc_ps_ch1
        y4_neutron_ps = PuBe_tof_data.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch4>{y4ThrQDC} and tof_ch4>{nTOFRange[0]} and tof_ch4<={nTOFRange[1]}').qdc_ps_ch1
        y5_neutron_ps = PuBe_tof_data.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch5>{y5ThrQDC} and tof_ch5>{nTOFRange[0]} and tof_ch5<={nTOFRange[1]}').qdc_ps_ch1
        #check for random multiplicity
        mpIdxNeutron = prodata.uniqueEventHelper(y2_neutron_ps, y3_neutron_ps, y4_neutron_ps, y5_neutron_ps)

        x_neutron_ps, y_neutron_ps = prodata.sliceRandomSubtraction(y2_neutron_ps.drop(mpIdxNeutron, errors='ignore'),
                                                                    y3_neutron_ps.drop(mpIdxNeutron, errors='ignore'),
                                                                    y4_neutron_ps.drop(mpIdxNeutron, errors='ignore'),
                                                                    y5_neutron_ps.drop(mpIdxNeutron, errors='ignore'),
                                                                    y2_random_ps.drop(mpIdxRandom, errors='ignore'),
                                                                    y3_random_ps.drop(mpIdxRandom, errors='ignore'),
                                                                    y4_random_ps.drop(mpIdxRandom, errors='ignore'),
                                                                    y5_random_ps.drop(mpIdxRandom, errors='ignore'),
                                                                    numBins = numBins, 
                                                                    dataLength = nLength, 
                                                                    randLength = randLength)
        #Merge statisics from all YAPs
        gammaPS_final[f'keVee{int(nThrQDC*1000)}']   += y_gamma_ps
        neutronPS_final[f'keVee{int(nThrQDC*1000)}'] += y_neutron_ps
        print(f'>>> Current sum: {np.round(sum(y_neutron_ps),1)}')
        print(f'>>> Total sum: {np.round(sum(neutronPS_final[f"keVee{int(nThrQDC*1000)}"]),1)}')

for col in np.arange(250, 5250, 250):
    print(f'Saving to... keV{col}')
    np.save(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/{detector}/sliced/thresholds/keV{col}/yGamma', gammaPS_final[f'keVee{col}'])
    np.save(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/{detector}/sliced/thresholds/keV{col}/yNeutron', neutronPS_final[f'keVee{col}'])
    np.save(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/{detector}/sliced/thresholds/keV{col}/x', x_neutron_ps)