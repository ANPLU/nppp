#!/usr/bin/env python3
from re import I
import sys
from tkinter import X
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
from numpy.core.arrayprint import SubArrayFormat
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit

# from matplotlib.colors import LinearSegmentedColormap


# IMPORT FROM LIBRARY DIRECTORY
sys.path.insert(0, "../library/")
sys.path.insert(0, "../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "../")
sys.path.insert(0, "data_analysis/")

import processing_math as promath
import processing_data as prodata
import processing_pd as propd
import processing_simulation as prosim
import figure_style as fs
import script_FOM_figures_import as FOMfigs
########################################
######## Setting parameters ############
########################################

PMT_gain = 4e6 #Nominal gain is 7'000'000 at nominal A/lm (model: 9821B p1)
att_factor_19dB = 8.91
att_factor_16_5dB = 6.68
att_factor_16dB = 6.31

att_factor_12dB = 3.98
att_factor_11dB = 3.55
q = 1.60217662E-19 #charge of a single electron

distance = 0.959


##########################################
###### Loading data ######################
##########################################
# numFiles = 10

# NE213A_raw = pd.concat([ prodata.sliceLoad("/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/sliced/PS/",  numFiles, 'raw_PS',     merge=True), 
#                          prodata.sliceLoad("/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/sliced/QDC/", numFiles, 'raw_QDC_lg', merge=True),
#                          prodata.sliceLoad("/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/sliced/QDC/", numFiles, 'raw_QDC_sg', merge=True)])
# NE213A_sliced_PS =       prodata.sliceLoad("/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/sliced/PS/",  numFiles, 'PS',         merge=False)
# NE213A_sliced_QDC_lg =   prodata.sliceLoad("/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/sliced/QDC/", numFiles, 'QDC_lg',     merge=False)
# NE213A_sliced_QDC_sg =   prodata.sliceLoad("/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/sliced/QDC/", numFiles, 'QDC_sg',     merge=False)

# EJ305_raw = pd.concat([ prodata.sliceLoad("/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/EJ305/sliced/PS/",  numFiles, 'raw_PS',     merge=True), 
#                         prodata.sliceLoad("/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/EJ305/sliced/QDC/", numFiles, 'raw_QDC_lg', merge=True),
#                         prodata.sliceLoad("/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/EJ305/sliced/QDC/", numFiles, 'raw_QDC_sg', merge=True)])
# EJ305_sliced_PS =       prodata.sliceLoad("/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/EJ305/sliced/PS/",  numFiles, 'PS',         merge=False)
# EJ305_sliced_QDC_lg =   prodata.sliceLoad("/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/EJ305/sliced/QDC/", numFiles, 'QDC_lg',     merge=False)
# EJ305_sliced_QDC_sg =   prodata.sliceLoad("/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/EJ305/sliced/QDC/", numFiles, 'QDC_sg',     merge=False)

# EJ331_raw = pd.concat([ prodata.sliceLoad("/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/EJ331/sliced/PS/",  numFiles, 'raw_PS',     merge=True), 
#                         prodata.sliceLoad("/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/EJ331/sliced/QDC/", numFiles, 'raw_QDC_lg', merge=True),
#                         prodata.sliceLoad("/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/EJ331/sliced/QDC/", numFiles, 'raw_QDC_sg', merge=True)])
# EJ331_sliced_PS =       prodata.sliceLoa1d("/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/EJ331/sliced/PS/",  numFiles, 'PS',         merge=False)
# EJ331_sliced_QDC_lg =   prodata.sliceLoad("/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/EJ331/sliced/QDC/", numFiles, 'QDC_lg',     merge=False)
# EJ331_sliced_QDC_sg =   prodata.sliceLoad("/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/EJ331/sliced/QDC/", numFiles, 'QDC_sg',     merge=False)

# EJ321P_raw = pd.concat([ prodata.sliceLoad("/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/EJ321P/sliced/PS/",  numFiles, 'raw_PS',     merge=True), 
#                          prodata.sliceLoad("/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/EJ321P/sliced/QDC/", numFiles, 'raw_QDC_lg', merge=True),
#                          prodata.sliceLoad("/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/EJ321P/sliced/QDC/", numFiles, 'raw_QDC_sg', merge=True)])
# EJ321P_sliced_PS =       prodata.sliceLoad("/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/EJ321P/sliced/PS/",  numFiles, 'PS',         merge=False)
# EJ321P_sliced_QDC_lg =   prodata.sliceLoad("/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/EJ321P/sliced/QDC/", numFiles, 'QDC_lg',     merge=False)
# EJ321P_sliced_QDC_sg =   prodata.sliceLoad("/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/EJ321P/sliced/QDC/", numFiles, 'QDC_sg',     merge=False)


#LOAD SIMULATION DATA

path_sim = '/media/gheed/Seagate_Expansion_Drive1/data/simulation/'
names = ['evtNum', 'optPhotonSum', 'optPhotonSumQE', 'optPhotonSumCompton', 'optPhotonSumComptonQE', 'xLoc', 'yLoc', 'zLoc', 'CsMin', 'optPhotonParentID', 'optPhotonParentCreatorProcess', 'optPhotonParentStartingEnergy','edep']
#pencilbeam
Cs137_sim_pen = pd.read_csv(path_sim + "NE213A/Cs137/pencilbeam/CSV_optphoton_data_sum.csv", names=names)
Cs137_sim_pen = pd.concat([Cs137_sim_pen, pd.read_csv(path_sim + "NE213A/Cs137/pencilbeam/part2/CSV_optphoton_data_sum.csv", names=names)])
Cs137_sim_pen = pd.concat([Cs137_sim_pen, pd.read_csv(path_sim + "NE213A/Cs137/pencilbeam/part3/CSV_optphoton_data_sum.csv", names=names)])
Cs137_sim_pen = pd.concat([Cs137_sim_pen, pd.read_csv(path_sim + "NE213A/Cs137/pencilbeam/part4/CSV_optphoton_data_sum.csv", names=names)])
Th232_sim_pen = pd.read_csv(path_sim + "NE213A/2_62MeV/pencilbeam/CSV_optphoton_data_sum.csv", names=names)
Th232_sim_pen = pd.concat([Th232_sim_pen, pd.read_csv(path_sim + 'NE213A/2_62MeV/pencilbeam/part2/CSV_optphoton_data_sum.csv', names=names)])
Th232_sim_pen = pd.concat([Th232_sim_pen, pd.read_csv(path_sim + 'NE213A/2_62MeV/pencilbeam/part3/CSV_optphoton_data_sum.csv', names=names)])
Th232_sim_pen = pd.concat([Th232_sim_pen, pd.read_csv(path_sim + 'NE213A/2_62MeV/pencilbeam/part4/CSV_optphoton_data_sum.csv', names=names)])
Th232_sim_pen = pd.concat([Th232_sim_pen, pd.read_csv(path_sim + 'NE213A/2_62MeV/pencilbeam/part5/CSV_optphoton_data_sum.csv', names=names)])
Th232_sim_pen = pd.concat([Th232_sim_pen, pd.read_csv(path_sim + 'NE213A/2_62MeV/pencilbeam/part6/CSV_optphoton_data_sum.csv', names=names)])
AmBe_sim_pen = pd.read_csv(path_sim + "NE213A/4_44MeV/pencilbeam/CSV_optphoton_data_sum.csv", names=names)
AmBe_sim_pen = pd.concat([AmBe_sim_pen, pd.read_csv(path_sim + 'NE213A/4_44MeV/pencilbeam/part2/CSV_optphoton_data_sum.csv', names=names)])
AmBe_sim_pen = pd.concat([AmBe_sim_pen, pd.read_csv(path_sim + 'NE213A/4_44MeV/pencilbeam/part3/CSV_optphoton_data_sum.csv', names=names)])
AmBe_sim_pen = pd.concat([AmBe_sim_pen, pd.read_csv(path_sim + 'NE213A/4_44MeV/pencilbeam/part4/CSV_optphoton_data_sum.csv', names=names)])
AmBe_sim_pen = pd.concat([AmBe_sim_pen, pd.read_csv(path_sim + 'NE213A/4_44MeV/pencilbeam/part5/CSV_optphoton_data_sum.csv', names=names)])
AmBe_sim_pen = pd.concat([AmBe_sim_pen, pd.read_csv(path_sim + 'NE213A/4_44MeV/pencilbeam/part6/CSV_optphoton_data_sum.csv', names=names)])
#isotropic
Cs137_sim_iso = np.genfromtxt(path_sim + 'NE213A/Cs137/isotropic/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
Cs137_sim_iso = np.append(Cs137_sim_iso, np.genfromtxt(path_sim + 'NE213A/Cs137/isotropic/part2/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])
Cs137_sim_iso = np.append(Cs137_sim_iso, np.genfromtxt(path_sim + 'NE213A/Cs137/isotropic/part3/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])
Th232_sim_iso = np.genfromtxt(path_sim + 'NE213A/2_62MeV/isotropic/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]     
Th232_sim_iso = np.append(Th232_sim_iso, np.genfromtxt(path_sim + 'NE213A/2_62MeV/isotropic/part2/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])
Th232_sim_iso = np.append(Th232_sim_iso, np.genfromtxt(path_sim + 'NE213A/2_62MeV/isotropic/part3/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])
AmBe_sim_iso =  np.genfromtxt(path_sim + 'NE213A/4_44MeV/isotropic/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
AmBe_sim_iso = np.append(AmBe_sim_iso, np.genfromtxt(path_sim + 'NE213A/4_44MeV/isotropic/part2/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])
AmBe_sim_iso = np.append(AmBe_sim_iso, np.genfromtxt(path_sim + 'NE213A/4_44MeV/isotropic/part3/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])
#randomize binning pencilbeam
y, x = np.histogram(Cs137_sim_pen.optPhotonSumComptonQE, bins=4096, range=[0, 4096])
Cs137_sim_pen.optPhotonSumComptonQE = prodata.getRandDist(x, y)
y, x = np.histogram(Th232_sim_pen.optPhotonSumComptonQE, bins=4096, range=[0, 4096])
Th232_sim_pen.optPhotonSumComptonQE = prodata.getRandDist(x, y)
y, x = np.histogram(AmBe_sim_pen.optPhotonSumComptonQE, bins=4096, range=[0, 4096])
AmBe_sim_pen.optPhotonSumComptonQE = prodata.getRandDist(x, y)
#randomize binning isotropic
y, x = np.histogram(Cs137_sim_iso, bins=4096, range=[0, 4096])
Cs137_sim_iso = prodata.getRandDist(x, y)
y, x = np.histogram(Th232_sim_iso, bins=4096, range=[0, 4096])
Th232_sim_iso = prodata.getRandDist(x, y)
y, x = np.histogram(AmBe_sim_iso, bins=4096, range=[0, 4096])
AmBe_sim_iso = prodata.getRandDist(x, y)
#reset index pencilbeam 
Cs137_sim_pen =  Cs137_sim_pen.reset_index()
Th232_sim_pen =  Th232_sim_pen.reset_index()
AmBe_sim_pen =   AmBe_sim_pen.reset_index()

#QDC calibrate simulations pencilbeam
Cs137_sim_pen.optPhotonSumComptonQE =      prosim.chargeCalibration(Cs137_sim_pen.optPhotonSumComptonQE * q * PMT_gain)# * PMTgainCorrection['Cs137_662']
Th232_sim_pen.optPhotonSumComptonQE =      prosim.chargeCalibration(Th232_sim_pen.optPhotonSumComptonQE * q * PMT_gain)# * PMTgainCorrection['Cs137_662']
AmBe_sim_pen.optPhotonSumComptonQE =      prosim.chargeCalibration(AmBe_sim_pen.optPhotonSumComptonQE * q * PMT_gain)# * PMTgainCorrection['Cs137_662']

#QDC calibrate simulations isotropic
Cs137_sim_iso =      prosim.chargeCalibration(Cs137_sim_iso * q * PMT_gain)# * PMTgainCorrection['Cs137_662']
Th232_sim_iso =      prosim.chargeCalibration(Th232_sim_iso * q * PMT_gain)# * PMTgainCorrection['Cs137_662']
AmBe_sim_iso =      prosim.chargeCalibration(AmBe_sim_iso * q * PMT_gain)# * PMTgainCorrection['Cs137_662']

#Apply smearing on isotropic data
for i in range(len(Cs137_sim_iso)):
    Cs137_sim_iso[i] = prosim.gaussSmear(Cs137_sim_iso[i], 0.12)
for i in range(len(Th232_sim_iso)):
    Th232_sim_iso[i] = prosim.gaussSmear(Th232_sim_iso[i], 0.065)
for i in range(len(AmBe_sim_iso)):
    AmBe_sim_iso[i] = prosim.gaussSmear(AmBe_sim_iso[i], 0.12)
    
#Apply smearing on pencilbeam data
Cs137_sim_pen.optPhotonSumComptonQE = Cs137_sim_pen.optPhotonSumComptonQE.apply(lambda x: prosim.gaussSmear(x, 0.12)) 
Th232_sim_pen.optPhotonSumComptonQE = Th232_sim_pen.optPhotonSumComptonQE.apply(lambda x: prosim.gaussSmear(x, 0.065)) 
AmBe_sim_pen.optPhotonSumComptonQE = AmBe_sim_pen.optPhotonSumComptonQE.apply(lambda x: prosim.gaussSmear(x, 0.12)) 



##########################################
###### Figure 3 ##########################
# Figure showing digitized waveforms######
##########################################

path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop
df_waveforms = propd.load_parquet(path+'run01606/', num_events=1, keep_col=['samples_ch1', 'baseline_ch1', 'CFD_rise_ch1', 'amplitude_ch1','CFD_drop_ch1', 'qdc_lg_ch1']) 
df_waveforms = df_waveforms.reset_index()

evtn = 12 #event number to use

fs.set_style(textwidth=345)
plt.plot(np.arange(0, 1001, 1), (df_waveforms.samples_ch1[evtn]-df_waveforms.baseline_ch1[evtn])*2.5, lw=1, zorder=1)
plt.hlines(-25, xmin=0, xmax=1, alpha=1)
plt.hlines(0, xmin=0, xmax=1, alpha=1)

plt.vlines(df_waveforms.CFD_rise_ch1[evtn], ymin=-500, ymax=50, alpha=1, color='black', lw=0.5)
plt.vlines(df_waveforms.CFD_rise_ch1[evtn]-25, ymin=-500, ymax=50, alpha=0.5, color='grey')
plt.vlines(df_waveforms.CFD_rise_ch1[evtn]-25+500, ymin=-500, ymax=50, alpha=0.5, color='grey')
plt.vlines(df_waveforms.CFD_rise_ch1[evtn]-25+60, ymin=-500, ymax=50, alpha=0.5, color='grey')
plt.hlines(-25, xmin=0, xmax=1000, color='black', ls='dotted')
plt.ylabel('Amplitude [mV]')
plt.xlabel('Time [ns]')
plt.xlim([160, 730])
plt.ylim([-250, 20])
plt.show()


### ZOOM IN VERSION ###
fs.set_style(textwidth=345*0.55)
plt.plot(np.arange(0, 1001, 1), (df_waveforms.samples_ch1[evtn]-df_waveforms.baseline_ch1[evtn])*2.5, lw=2, zorder=1)
plt.hlines(-25, xmin=0, xmax=1, alpha=1, zorder=2)
plt.hlines(0, xmin=0, xmax=1, alpha=1, zorder=2)
plt.hlines(-25, xmin=0, xmax=1000, color='black', zorder=2)
plt.hlines(0, xmin=0, xmax=1000, color='black', ls='dashed', zorder=2)
plt.vlines(df_waveforms.CFD_rise_ch1[evtn], ymin=-500, ymax=60, alpha=1, color='black', zorder=2)

plt.xlim([199, 265])
plt.ylim([-265, 60])
plt.show()




##########################################
###### Figure 4 ##########################
# Calibration of NE213A ##################
##########################################

#CALIBRATION DATA
path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop
Cs137_data = propd.load_parquet_merge(path, np.arange(1656,1656+10,1), keep_col=['qdc_lg_ch1', 'amplitude_ch1'], full=False)
Th232_data = propd.load_parquet_merge(path, np.arange(1671,1671+5,1), keep_col=['qdc_lg_ch1'], full=False)
AmBe_data = propd.load_parquet_merge(path, np.arange(1693, 1693+10,1), keep_col=['qdc_lg_ch1'], full=False)
BG_data = propd.load_parquet_merge(path, np.arange(1676, 1676+10,1), keep_col=['qdc_lg_ch1'], full=False)


#MAKING FIGURE
QDCcal = np.empty(0)
numBins = 1000
titleParamters = dict(ha='left', va='center', fontsize=10, color='black')

fs.set_style(textwidth=345)

#Cs137
plt.subplot(3,1,1)
binRange = np.arange(2000, 13200, 600)
w = np.empty(len(BG_data))
w.fill(1/10)
ybg, xbg = np.histogram(BG_data.qdc_lg_ch1*att_factor_16dB*1.05, bins=binRange, weights=w)
w = np.empty(len(Cs137_data))
w.fill(1/10)
y, x = np.histogram(Cs137_data.qdc_lg_ch1*att_factor_16dB*1.05, bins=binRange, weights=w)
x = prodata.getBinCenters(x)
plt.scatter(x, (y-ybg)*0.024, label='Cs137', color='black', s=6, zorder=9)

offset = 0.115*att_factor_16dB*1.05
y, x = np. histogram(Cs137_sim_pen.optPhotonSumComptonQE*offset, bins=np.arange(0, 13200, 600))
x = prodata.getBinCenters(x)
plt.step(x, y*0.25, color='black', lw=0.5)
plt.fill_between(x, y1=y*0.25, color='black', alpha=0.5, step='pre')
QDCcal = np.append(QDCcal, np.average(x, weights=(y)))
plt.vlines(QDCcal[-1], ymin=0, ymax=3000, color='black', ls='dashed', zorder=10)

y, x = np. histogram(Cs137_sim_iso*offset, bins=binRange)
x = prodata.getBinCenters(x)
plt.step(x, y*0.0355*2, color='tomato', lw=0.5)
plt.text(1000, 2300, '$^{137}$Cs (0.48 MeV$_{ee}$)', **titleParamters)
plt.xlim(0,53000)
plt.ylim([0, 2800])

#Th232
plt.subplot(3,1,2)
binRange = np.arange(15000, 30000, numBins)
w = np.empty(len(BG_data))
w.fill(1/10)
ybg, xbg = np.histogram(BG_data.qdc_lg_ch1*att_factor_16dB*1.05, bins=binRange, weights=w)
w = np.empty(len(Th232_data))
w.fill(1/5)
y, x = np.histogram(Th232_data.qdc_lg_ch1*att_factor_16dB*1.05, bins=binRange, weights=w)
x = prodata.getBinCenters(x)
plt.scatter(x, (y-ybg)*1.1, label='Th232', color='black', s=6, zorder=9)

offset = 0.109*att_factor_16dB*1.05
y, x = np. histogram(Th232_sim_pen.optPhotonSumComptonQE*offset, bins=np.arange(11000, 30000, numBins))
x = prodata.getBinCenters(x)
plt.step(x, y*1.1, color='black', lw=0.5)
plt.fill_between(x, y1=y*1.1, color='black', alpha=0.5, step='pre')
QDCcal = np.append(QDCcal, np.average(x, weights=(y)))
plt.vlines(QDCcal[-1], ymin=0, ymax=3000, color='black', ls='dashed', zorder=10)

y, x = np. histogram(Th232_sim_iso*offset, bins=binRange)
x = prodata.getBinCenters(x)
plt.step(x, y*0.24*1.1, color='tomato', lw=0.5)
# plt.vlines(x=2380, ymin=0, ymax=2500, color='black', lw=1) #dividing lines
plt.text(1000, 2300, '$^{232}$Th (2.38 MeV$_{ee}$)', **titleParamters)
plt.xlim(0, 53000)
plt.ylim([0, 2800])

#AmBe
plt.subplot(3,1,3)
binRange = np.arange(30000, 55000, numBins)
w = np.empty(len(BG_data))
w.fill(1/10)
ybg, xbg = np.histogram(BG_data.qdc_lg_ch1*att_factor_16dB*1.05, bins=binRange, weights=w)
w = np.empty(len(AmBe_data))
w.fill(1/10)
y, x = np.histogram(AmBe_data.qdc_lg_ch1*att_factor_16dB*1.05, bins=binRange, weights=w)
x = prodata.getBinCenters(x)
plt.scatter(x, (y-ybg)*0.90, label='AmBe', color='black', s=6, zorder=9)

offset = 0.107*att_factor_16dB*1.05
y, x = np. histogram(AmBe_sim_pen.optPhotonSumComptonQE*offset, bins=np.arange(17000, 55000, numBins))
x = prodata.getBinCenters(x)
plt.step(x, y*5, color='black', lw=0.5)
plt.fill_between(x, y1=y*5, color='black', alpha=0.5, step='pre')
QDCcal = np.append(QDCcal, np.average(x, weights=(y)))
plt.vlines(QDCcal[-1], ymin=0, ymax=3000, color='black', ls='dashed', zorder=10)

y, x = np. histogram(AmBe_sim_iso*offset, bins=binRange)
x = prodata.getBinCenters(x)
plt.step(x, y*0.69, color='tomato', lw=0.5)
# plt.vlines(x=4600, ymin=0, ymax=2500, color='black', lw=1) #dividing lines
plt.text(1000, 2300, 'Am/Be (4.20 MeV$_{ee}$)', **titleParamters)

plt.xlabel('QDC [channels]')
plt.ylabel('counts')
plt.xlim(0, 53000)
plt.ylim([0, 2800])
plt.subplots_adjust(hspace = .001, wspace = .001)
plt.show()



#INSET FIGURE FOR CALIBRATION FIGURE
fs.set_style(textwidth=345/2.5*1.1)
plt.figure()
Ecal = [0.48, 2.38, 4.20] ##energies of compton edges
popt, pcov = curve_fit(promath.linearFunc, QDCcal, Ecal)
pcov = np.diag(np.sqrt(pcov))
plt.plot(np.arange(0, 7, 1), promath.linearFunc(np.arange(0, 7, 1), 1/popt[0], 1/popt[1]), color='grey', lw=0.5, zorder=2)
plt.scatter(Ecal, QDCcal, color='black', s=15, marker='s', zorder=2)
plt.title(f'Calibration constant: {round(popt[0]*1000,3)}$\pm${round(pcov[0]*1000,3)} keVee / QDC channel')
plt.xlim([0,6])
plt.ylim([0,60000])
plt.show()



######################################################################
####### Figure 5 ######################################################
######## EDEP vs EDEP #################################################
######################################################################
#3x1 Gamma-flash cut Edep
path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop
tofCh = 4
data = propd.load_parquet_merge(path, np.arange(2121,2121+50,1), keep_col=['qdc_lg_ch1', f'qdc_lg_ch{tofCh}', f'tof_ch{tofCh}', f'amplitude_ch{tofCh}', ], full=False)

#Energy calibrate data NE213A
data.qdc_lg_ch1 = prodata.calibrateMyDetector('/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/Ecal', data.qdc_lg_ch1*att_factor_12dB)

#Energy calibrate data YAP
YAP_cal = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/YAP/Ecal/ch{tofCh}/popt.npy')
data[f'qdc_lg_ch{tofCh}'] = promath.linearFunc(data[f'qdc_lg_ch{tofCh}'], YAP_cal[0], YAP_cal[1])

#Make figure
numBins = 150
titleParamters = dict(ha='left', va='center', fontsize=10, color='black')
fs.set_style(textwidth=345)
query = f'amplitude_ch{tofCh}>20 and qdc_lg_ch{tofCh}>0.1 and tof_ch{tofCh}>85 and tof_ch{tofCh}<140 and qdc_lg_ch1>0.1'
plt.hist2d(data.query(query).qdc_lg_ch1, data.query(query)[f'qdc_lg_ch{tofCh}'], bins=numBins, range=[[0, 6], [0, 6]], cmap='turbo', norm=LogNorm())

plt.vlines(0.1, ymin=0, ymax=7, ls='dashed', lw=1, color='black')
plt.text(0.5, 5, 'NE 213A detector threshold', **titleParamters)

plt.hlines(3.0, xmin=0, xmax=7, ls='dashed', lw=1, color='black')
plt.text(2, 2, 'YAP:Ce detector threshold', **titleParamters)

plt.xlabel('NE 213A E$_{dep}$ [MeV$_{ee}$]')
plt.ylabel('YAP:Ce E$_{dep}$ [MeV$_{ee}$]')
plt.colorbar()
plt.show()


#################################################################
######### Figure 6 ##############################################
######### Time-of-flight random subtraction explained ###########
#################################################################
#TOF DATA
path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop
TOF = propd.load_parquet_merge(path, np.arange(2121,2121+10,1), keep_col=['amplitude_ch2','tof_ch2','amplitude_ch3','tof_ch3','amplitude_ch4','tof_ch4','amplitude_ch5','tof_ch5'], full=False)
flashrange = {'tof_ch2':[60, 76], 'tof_ch3':[60, 76], 'tof_ch4':[60, 76], 'tof_ch5':[60, 76]} #gamma flash fitting ranges
prodata.tof_calibration(TOF, flashrange, distance, phcut=125, calUnit=1e-9, energyCal=True)

numBins = 800
binRange = [-870, 200]

# binRange = np.arange(-870, 200, 2)

fs.set_style(textwidth=345)
# plt.figure()

y, x = np.histogram(TOF.query('amplitude_ch2>35').tof_ch2*1e9, bins=numBins, range=binRange)
x = prodata.getBinCenters(x)
plt.step(x, y, color='black', lw=1)

y, x = np.histogram(TOF.query('amplitude_ch2>35 and tof_ch2>-710e-9 and tof_ch2<-10e-9').tof_ch2*1e9, bins=numBins, range=binRange)
x = prodata.getBinCenters(x)
plt.fill_between(x, y, where=(y > 0), color='grey', step='pre')

y, x = np.histogram(TOF.query('amplitude_ch2>35 and tof_ch2>-1e-9 and tof_ch2<5.8e-9').tof_ch2*1e9, bins=numBins, range=binRange)
x = prodata.getBinCenters(x)
plt.fill_between(x, y, where=(y > 0), color='tomato', step='pre')
# popt,pcov = curve_fit(promath.gaussFunc, x, y)
# print(popt)
y, x = np.histogram(TOF.query('amplitude_ch2>35 and tof_ch2>26.2e-9 and tof_ch2<90e-9').tof_ch2*1e9, bins=numBins, range=binRange)
x = prodata.getBinCenters(x)
plt.fill_between(x, y, where=(y > 0), color='royalblue', step='pre')

plt.xlabel('Time-of-Flight [ns]')
plt.ylabel('counts')
plt.xlim([-900,200])
plt.ylim([-200,2300])
# plt.show()

fs.set_style(textwidth=345/1.5)
plt.figure()
y, x = np.histogram(TOF.query('amplitude_ch2>35').tof_ch2*1e9, bins=numBins, range=binRange)
x = prodata.getBinCenters(x)
plt.step(x, y, color='black', lw=1)

y, x = np.histogram(TOF.query(f'amplitude_ch2>35 and tof_ch2>33.6e-9 and tof_ch2<36.81e-9').tof_ch2*1e9, bins=numBins, range=binRange)
x = prodata.getBinCenters(x)
plt.fill_between(x, y, where=(y > 0), color='royalblue', step='pre')

y, x = np.histogram(TOF.query(f'amplitude_ch2>35 and tof_ch2>46.0e-9 and tof_ch2<52.8e-9').tof_ch2*1e9, bins=numBins, range=binRange)
x = prodata.getBinCenters(x)
plt.fill_between(x, y, where=(y > 0), color='royalblue', step='pre')

y, x = np.histogram(TOF.query('amplitude_ch2>35 and tof_ch2>-710e-9 and tof_ch2<-10e-9').tof_ch2*1e9, bins=numBins, range=binRange)
x = prodata.getBinCenters(x)
plt.fill_between(x, y, where=(y > 0), color='grey', step='pre')

y, x = np.histogram(TOF.query('amplitude_ch2>35 and tof_ch2>-1e-9 and tof_ch2<5.8e-9').tof_ch2*1e9, bins=numBins, range=binRange)
x = prodata.getBinCenters(x)
plt.fill_between(x, y, where=(y > 0), color='tomato', step='pre')


plt.vlines(prodata.EnergytoTOF(3.75 ,distance)*1e9, ymax = 2000, ymin= 50, color='purple')
plt.vlines(prodata.EnergytoTOF(4.25 ,distance)*1e9, ymax = 2000, ymin= 50, color='purple')

plt.vlines(prodata.EnergytoTOF(1.75 ,distance)*1e9, ymax = 2000, ymin= 50, color='purple')
plt.vlines(prodata.EnergytoTOF(2.25 ,distance)*1e9, ymax = 2000, ymin= 50, color='purple')

plt.xlabel('Time-of-Flight [ns]')
plt.ylabel('counts')
plt.xlim([-20,80])
plt.ylim([-250, 2200])
plt.show()


# ##########################################################
# ######### Figure 7 #######################################
# ######### Making 2D raw figures NE213A and EJ331 #########
# ##########################################################
gainOffsetsNE213A = np.array([1.        , 1.00449786, 1.02776289, 0.99543515, 0.99513401,
                            0.99373714, 1.01271988, 1.00555114, 1.00767454, 1.03011807,
                            1.03737901, 1.02339094, 1.01367455, 1.103476  , 1.01138804,
                            1.09498013, 0.99517056, 0.98747555, 1.01479879, 1.03541272,
                            1.01354815, 0.99957959, 0.97865142, 0.99600754, 1.00958113,
                            1.02217856, 1.03640639, 1.01903062, 0.99881911, 1.00631287,
                            0.9909422 , 1.00325513, 1.00190923, 0.99810678, 0.97982168,
                            1.0108797 , 1.01108049, 1.01447416, 1.04953386, 0.99252985,
                            0.98535389, 0.98752831, 0.96838927, 0.9648048 , 0.97339146,
                            0.98834493, 0.97955184, 0.9898073 , 0.98643835, 0.99461799,
                            1.00308614, 0.97061575, 1.01527638, 0.98378853, 0.98731022,
                            1.00510757, 0.98845017, 0.9743922 , 0.98245988, 0.98122753,
                            0.9691665 , 1.01991096, 0.98006913, 0.97535042, 0.99039761,
                            0.97771579, 0.97718466, 0.98845839, 0.97691246, 0.9784261 ,
                            0.99225482, 0.97087123, 0.97269272, 0.9912359 , 0.97617339,
                            1.00866114, 1.00550252, 0.99468125, 0.98253334, 0.97681113,
                            0.97632393, 0.96692358, 1.00644259, 1.00780908, 0.99361568,
                            0.97946456, 1.00178607, 0.98710232, 0.98580734, 1.02220332,
                            0.97701412, 0.98474256, 1.02551657, 0.97654263, 0.97509727,
                            1.00753811, 0.99528997, 0.97050454, 0.98562478, 0.9745818 ,
                            0.98730201, 0.97500751, 0.98760836, 0.97151791, 0.98084076,
                            0.98470394, 0.97723897, 0.99826923, 0.97042334, 0.98152384,
                            0.98179468, 1.00139174, 0.9907689 , 0.9982554 , 0.99021647,
                            0.98391455, 0.97733494, 0.96542459, 0.96496404, 0.97209604,
                            1.03033098, 0.98416693, 0.97510294, 0.97231365, 0.97591612,
                            0.96953299, 0.97641848, 0.96724081, 0.9753956 , 0.96514133,
                            0.976637  , 0.98310287, 0.98319185, 0.98085286, 0.96911062,
                            0.96717393, 0.98366547, 0.96810717, 0.97483992, 0.97766298,
                            0.98462132])

gainOffsetsEJ305 = np.array([1.    , 1.0203, 1.0296, 1.0318, 1.0304, 1.0199, 1.0197, 1.0237,
                            1.0197, 1.0318, 1.0207, 1.0203, 1.0142, 1.0124, 1.0169, 1.0049,
                            0.9928, 1.0132, 1.011 , 1.0059, 0.9999, 1.0098, 1.0144, 1.0116,
                            1.0172, 1.0009, 1.0057, 1.0007, 1.0043, 1.0051, 1.01  , 0.9999,
                            1.0009, 0.9999, 1.011 , 0.991 , 0.9999, 1.0033, 1.0013, 0.994 ,
                            1.0007, 1.0197, 1.0023, 1.0098, 0.9926, 1.0023, 1.0146, 1.0098,
                            1.0114, 1.0136, 1.0001, 0.99  , 0.9873, 0.9922, 1.0138, 0.9934,
                            0.9809, 1.0011, 0.9801, 0.9803, 1.0041, 0.9823, 1.0098, 0.9803,
                            0.9922, 0.9857, 0.9744, 0.9807, 0.9999, 0.9906, 1.0017, 0.9906,
                            0.9845, 0.9999, 0.9811, 0.9908, 0.99  , 1.0013, 0.9724, 0.9831,
                            0.9999, 0.9801, 0.99  , 0.9736, 0.9936, 0.9801, 0.9938, 1.0063,
                            0.9988, 0.9999, 0.9918, 0.9908, 0.9954, 0.9774, 0.9853, 0.9902,
                            0.9938, 0.9999, 0.9942, 1.0029, 0.9817, 0.9843, 0.9702, 0.9801,
                            0.9839, 0.9908, 0.993 , 0.9928, 0.9831, 1.0006, 0.99  , 0.9904,
                            0.9924, 0.9831, 0.9904, 0.9825, 0.9851, 0.9855, 0.9922, 0.9738,
                            0.9918, 0.9902, 0.9936, 0.9902, 0.99  , 0.99  , 0.9849, 1.0013,
                            0.9803, 0.9706, 0.9819, 0.9827, 0.975 , 0.9912, 1.0033, 0.9732,
                            0.9999, 0.993 , 0.9861, 0.9817, 0.9942, 0.9815, 0.9805, 0.99  ,
                            0.9908, 0.9809, 1.0001, 0.9805, 0.9809, 0.9801, 0.99  , 0.971 ,
                            0.9999, 1.0017, 0.9908, 0.99  , 0.9922, 0.9722, 0.9837, 0.9999,
                            0.9811, 0.9918, 0.9956, 0.9999, 0.9999, 0.9944, 1.0001, 1.0003,
                            0.9732, 0.9814, 0.9999, 0.9912, 1.0023, 0.9999, 0.9958, 0.9837,
                            0.9819, 0.9631, 0.9738, 0.9801, 0.9801, 0.99  , 0.9958, 0.9734,
                            0.9734, 0.9904, 0.9833, 0.9617, 1.0007, 0.9821, 0.9801, 0.9732,
                            0.9722, 0.99  , 0.9944, 0.9718, 0.9764, 0.9861, 0.9849, 0.9809,
                            0.9948])

path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop
tofCh = 3
numRuns = 30
dfNE213A = propd.load_parquet_merge_gainadjust(path, np.arange(2121, 2121+numRuns,1), keep_col=[f'amplitude_ch{tofCh}',f'tof_ch{tofCh}','qdc_lg_ch1', f'qdc_lg_ch{tofCh}', 'qdc_ps_ch1'], full=False, gainOffsets=gainOffsetsNE213A[0:numRuns])
dfEJ305 =  propd.load_parquet_merge_gainadjust(path, np.arange(3038, 3038+numRuns,1), keep_col=[f'amplitude_ch{tofCh}',f'tof_ch{tofCh}','qdc_lg_ch1', f'qdc_lg_ch{tofCh}', 'qdc_ps_ch1'], full=False, gainOffsets=gainOffsetsEJ305[0:numRuns])

#energy calibrate
dfNE213A.qdc_lg_ch1 = prodata.calibrateMyDetector('/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/Ecal', dfNE213A.qdc_lg_ch1*att_factor_12dB)
dfEJ305.qdc_lg_ch1 = prodata.calibrateMyDetector('/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/EJ305/Ecal', dfEJ305.qdc_lg_ch1*att_factor_19dB)



#SINGLES data
numBins=150
fs.set_style(textwidth=345)
plt.subplot(2,2,1)
query = f'qdc_lg_ch{tofCh}>{6450} and qdc_lg_ch1>0.1'
plt.hist2d(dfNE213A.query(query).qdc_lg_ch1, dfNE213A.query(query).qdc_ps_ch1, bins=numBins, range=[[0, 7], [0.1, 0.65]], norm=LogNorm(), cmap='plasma')
plt.ylabel('PS')
plt.xticks([])
plt.vlines([1,3], ymax=7, ymin=0, linestyle='dashed', color='black', lw=0.5)
# plt.colorbar()

ax = plt.subplot(2,2,2)
plt.hist2d(dfEJ305.query(query).qdc_lg_ch1, dfEJ305.query(query).qdc_ps_ch1, bins=numBins, range=[[0, 7], [0.0, 0.55]], norm=LogNorm(), cmap='viridis')
plt.gca().invert_xaxis()
plt.gca().yaxis.set_label_position("right")
ax.yaxis.tick_right()
plt.xticks([])
# plt.yticks([])
plt.vlines([1,3], ymax=7, ymin=0, linestyle='dashed', color='black', lw=0.5)
# plt.colorbar()


#TAGGED data
plt.subplot(2,2,3)
query = f'qdc_lg_ch{tofCh}>{6450} and tof_ch{tofCh}>60 and tof_ch{tofCh}<160 and qdc_lg_ch1>0.1'
plt.hist2d(dfNE213A.query(query).qdc_lg_ch1, dfNE213A.query(query).qdc_ps_ch1, bins=numBins, range=[[0, 7], [0.1, 0.65]], norm=LogNorm(), cmap='inferno')
plt.xlabel('Energy [MeV$_{ee}$]')
plt.ylabel('PS')
plt.vlines([1,3], ymax=7, ymin=0, linestyle='dashed', color='black', lw=0.5)
# plt.colorbar()

ax = plt.subplot(2,2,4)
plt.hist2d(dfEJ305.query(query).qdc_lg_ch1, dfEJ305.query(query).qdc_ps_ch1, bins=numBins, range=[[0, 7], [0.0, 0.55]], norm=LogNorm(), cmap='cividis')
plt.gca().invert_xaxis()
ax.yaxis.tick_right()
# plt.yticks([])
plt.gca().yaxis.tick_right()
plt.xlabel('Energy [MeV$_{ee}$]')
plt.vlines([1,3], ymax=7, ymin=0, linestyle='dashed', color='black', lw=0.5)
plt.subplots_adjust(hspace = .001, wspace = .001)
# plt.colorbar()
plt.show()


################################################################
############ Figure 8 ##########################################
############# PS investigation "non prompt gamma-rays" #########
################################################################
path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop
tofCh = 2
df = propd.load_parquet_merge(path, np.arange(2121,2121+5,1), keep_col=[f'qdc_lg_ch{tofCh}',f'amplitude_ch{tofCh}',f'tof_ch{tofCh}','qdc_lg_ch1','qdc_ps_ch1'], full=False)
# flashrange = {'tof_ch2':[60, 76]} #gamma flash fitting ranges
# prodata.tof_calibration(df, flashrange, distance, phcut=125, calUnit=1e-9, energyCal=True)
df.qdc_lg_ch1 = prodata.calibrateMyDetector('/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/Ecal', df.qdc_lg_ch1*att_factor_12dB)

#set paramters for figure
titleParamters = dict(ha='left', va='center', fontsize=10, color='black')
numBins = np.arange(0.0, 1, 0.01)
xlimit = [0.11, 0.55]
#Makes 'singles' figure
fs.set_style(textwidth=345)
f, (a0, a1, a2) = plt.subplots(3, 1, gridspec_kw={'height_ratios': [1, 1, 1]})
query = f'qdc_lg_ch1>{2}'
y, x = np.histogram(df.query(query).qdc_ps_ch1, bins=numBins)
x = prodata.getBinCenters(x)
#fit gaussian to peaks
popt_g, pcov_g = promath.gaussFit(x, y/np.max(y), start=0.2, stop=0.24, error=True)
popt_n, pcov_n = promath.gaussFit(x, y/np.max(y), start=0.33, stop=0.49, error=True)
a0.plot(np.arange(0,1,0.001), promath.gaussFunc(np.arange(0,1,0.001), popt_g[0], popt_g[1], popt_g[2]), color='tomato', zorder=2)
a0.plot(np.arange(0,1,0.001), promath.gaussFunc(np.arange(0,1,0.001), popt_n[0], popt_n[1], popt_n[2]), color='royalblue', zorder=2)


#Make singles figure
a0.text(0.11, 1.07, f'Singles NE 213A', **titleParamters)
a0.step(x, y/np.max(y), color='black', where='mid', lw=0.5)
a0.fill_between(x, y/np.max(y), color='grey', alpha=0.8, step='mid', zorder=1)
a0.vlines(0.3, ymin=0, ymax=1.5, linestyle='dashed', color='black', lw=0.5)
a0.set_ylabel('Yield [arb. units]')
a0.set_xlim([xlimit[0], xlimit[1]])
a0.set_ylim([0.01, 4])
a0.set_yscale('log')
a0.set_xticks([])

#Make NE213A correlated figure
x = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/sliced/thresholds/keV{2000}/x.npy')
yg = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/sliced/thresholds/keV{2000}/yGamma.npy')
yn = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/sliced/thresholds/keV{2000}/yNeutron.npy')

yg, yn = prodata.PSscaler(x, yg, yn, plot=False) #scale data to be roughly same height

a1.text(0.11, 1.5, f'Correlated NE 213A', **titleParamters)

a1.fill_between(x, yg/np.max(yg), color='tomato', step='mid', alpha=0.8, zorder=1)
a1.step(x, yg/np.max(yg), color='black', lw=0.5, where='mid', zorder=2)

a1.fill_between(x, yn/np.max(yg), color='royalblue', step='mid', alpha=0.6, zorder=3)
a1.fill_between(x[:29], yn[:29]/np.max(yg), hatch='///', edgecolor='black', step='mid', facecolor='none', lw=0.5, zorder=4)
a1.step(x, yn/np.max(yg), color='black', lw=0.5, where='mid', zorder=5)

a1.vlines(0.3, ymin=0, ymax=4, linestyle='dashed', color='black', lw=0.5)
a1.set_yscale('log')
a1.set_xlim([xlimit[0], xlimit[1]])

a1.set_ylim([0.01, 4])
a1.set_xticks([])


#make EJ305 correlated figure
EJ305_start_g = 0.146
EJ305_stop_g = 0.196
EJ305_start_n = 0.210
EJ305_stop_n = 0.280
EJ305_lim_g = [EJ305_start_g, EJ305_stop_g]
EJ305_lim_n = [EJ305_start_n, EJ305_stop_n]

x = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/EJ305/sliced/thresholds/keV{2000}/x.npy')
yg = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/EJ305/sliced/thresholds/keV{2000}/yGamma.npy')
yn = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/EJ305/sliced/thresholds/keV{2000}/yNeutron.npy')

yg, yn = prodata.PSscaler(x, yg, yn, plot=False) #scale data to be roughly same height
a2.text(0.11, 1.5, f'Correlated EJ 305', **titleParamters)

doubleGaussParameters = pd.read_pickle('/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/EJ305/doubleGaussParameters/fitParams_thr')
query = f'energy=={2000}'
param = [   doubleGaussParameters.query(query).amp1.item(), 
            doubleGaussParameters.query(query).mean1.item(), 
            doubleGaussParameters.query(query).std1.item(), 
            doubleGaussParameters.query(query).amp2.item(), 
            doubleGaussParameters.query(query).mean2.item(), 
            doubleGaussParameters.query(query).std2.item()]
EJ305_FOM_dg, EJ305_FOM_err_dg, EJ305_param_dg = prodata.PSFOMdoubleGauss(x, x, yn, yg, fitLim_n=[0.100, 0.350], fitLim_g=EJ305_lim_g, param=param, error=True, plot=False, return_param=True)



a2.fill_between(x, yg/np.max(yg), color='tomato', step='mid', alpha=0.8, zorder=1)
a2.step(x, yg/np.max(yg), color='black', lw=0.5, where='mid', zorder=2)

a2.fill_between(x, yn/np.max(yg), color='royalblue', step='mid', alpha=0.6, zorder=3)
a2.fill_between(x[:29], yn[:29]/np.max(yg), hatch='///', edgecolor='black', step='mid', facecolor='none', lw=0.5, zorder=4)
a2.step(x, yn/np.max(yg), color='black', lw=0.5, where='mid', zorder=5)



a2.plot(np.arange(0, 1, 0.001), promath.gaussFunc(np.arange(0, 1, 0.001), EJ305_param_dg['amp_n1'].item()/np.max(yg), 
                                                                            EJ305_param_dg['mean_n1'].item(), 
                                                                            EJ305_param_dg['std_n1'].item()), color='black', lw=1, zorder=4)

a2.fill_between(np.arange(0, 1, 0.001),promath.gaussFunc(np.arange(0, 1, 0.001), EJ305_param_dg['amp_n1'].item()/np.max(yg), EJ305_param_dg['mean_n1'].item(), EJ305_param_dg['std_n1'].item()), hatch='///', edgecolor='black', step='mid', facecolor='none', lw=0.5, zorder=4)

a2.plot(np.arange(0, 1, 0.001), promath.doubleGaussFunc(np.arange(0, 1, 0.001), EJ305_param_dg['amp_n1'].abs().item()/np.max(yg), 
                                                                                EJ305_param_dg['mean_n1'].item(),
                                                                                EJ305_param_dg['std_n1'].item(), 
                                                                                EJ305_param_dg['amp_n2'].abs().item()/np.max(yg), 
                                                                                EJ305_param_dg['mean_n2'].item(), 
                                                                                EJ305_param_dg['std_n2'].item()), color='black', lw=1)



a2.set_xlim([xlimit[0], xlimit[1]])
a2.set_ylim([0.01, 4])
a2.set_yscale('log')
a2.set_xlabel('PS')

plt.subplots_adjust(hspace = .001, wspace = .001)
plt.show()


###############################################################
############# Figure 9 ########################################
############# PS vs Threshold NE213A vs EJ305 ################
###############################################################
#get intrinsic FOM data
FOM_vs_thr_intrinsic = FOMfigs.NE213A_EJ305_PS_correlated(plot=True, width=335)
plt.show()

#get singles FOM data
FOM_vs_thr_singles = FOMfigs.NE213A_EJ305_PS_singles(plot=False)

#fitting the mean postions
#neutron positions
popt_int_n_NE213A, pcov_int_n_NE213A = curve_fit(promath.linearFunc, FOM_vs_thr_intrinsic.threshold, FOM_vs_thr_intrinsic.NE213A_mean_n)
popt_int_n_EJ305, pcov_int_n_EJ305   = curve_fit(promath.linearFunc, FOM_vs_thr_intrinsic.threshold, FOM_vs_thr_intrinsic.EJ305_mean_n)

popt_sing_n_NE213A, pcov_sing_n_NE213A = curve_fit(promath.linearFunc, FOM_vs_thr_singles.threshold, FOM_vs_thr_singles.NE213A_mean_n)
popt_sing_n_EJ305, pcov_sing_n_EJ305   = curve_fit(promath.linearFunc, FOM_vs_thr_singles.threshold, FOM_vs_thr_singles.EJ305_mean_n)

#gamma positions
popt_int_g_NE213A, pcov_int_g_NE213A = curve_fit(promath.linearFunc, FOM_vs_thr_intrinsic.threshold, FOM_vs_thr_intrinsic.NE213A_mean_g)
popt_int_g_EJ305, pcov_int_g_EJ305   = curve_fit(promath.linearFunc, FOM_vs_thr_intrinsic.threshold, FOM_vs_thr_intrinsic.EJ305_mean_g)

popt_sing_g_NE213A, pcov_sing_g_NE213A = curve_fit(promath.linearFunc, FOM_vs_thr_singles.threshold, FOM_vs_thr_singles.NE213A_mean_g)
popt_sing_g_EJ305, pcov_sing_g_EJ305   = curve_fit(promath.linearFunc, FOM_vs_thr_singles.threshold, FOM_vs_thr_singles.EJ305_mean_g)


#making plots of fits
x = np.arange(0,4100)
plt.figure()
plt.suptitle('NE213A')
plt.subplot(2,2,1)
plt.title('intrinsic neutron pos')
plt.scatter(FOM_vs_thr_intrinsic.threshold, FOM_vs_thr_intrinsic.NE213A_mean_n)
plt.plot(x, promath.linearFunc(x, popt_int_n_NE213A[0], popt_int_n_NE213A[1]), label=popt_int_n_NE213A[0])
plt.legend()

plt.subplot(2,2,2)
plt.title('intrinsic gamma pos')
plt.scatter(FOM_vs_thr_intrinsic.threshold, FOM_vs_thr_intrinsic.NE213A_mean_g)
plt.plot(x, promath.linearFunc(x, popt_int_g_NE213A[0], popt_int_g_NE213A[1]), label=popt_int_g_NE213A[0])
plt.legend()
# plt.ylim([0.2, 0.3])

plt.subplot(2,2,3)
plt.title('singles neutron pos')
plt.scatter(FOM_vs_thr_singles.threshold, FOM_vs_thr_singles.NE213A_mean_n)
plt.plot(x, promath.linearFunc(x, popt_sing_n_NE213A[0], popt_sing_n_NE213A[1]), label=popt_sing_n_NE213A[0])
plt.legend()
# plt.ylim([0.2, 0.3])

plt.subplot(2,2,4)
plt.title('singles gamma pos')
plt.scatter(FOM_vs_thr_singles.threshold, FOM_vs_thr_singles.NE213A_mean_g)
plt.plot(x, promath.linearFunc(x, popt_sing_g_NE213A[0], popt_sing_g_NE213A[1]), label=popt_sing_g_NE213A[0])
plt.legend()
# plt.ylim([0.2, 0.3])



plt.figure()
plt.suptitle('EJ305')
plt.subplot(2,2,1)
plt.title('intrinsic neutron pos')
plt.scatter(FOM_vs_thr_intrinsic.threshold, FOM_vs_thr_intrinsic.EJ305_mean_n)
plt.plot(x, promath.linearFunc(x, popt_int_n_EJ305[0], popt_int_n_EJ305[1]), label=popt_int_n_EJ305[0])
plt.legend()

plt.subplot(2,2,2)
plt.title('intrinsic gamma pos')
plt.scatter(FOM_vs_thr_intrinsic.threshold, FOM_vs_thr_intrinsic.EJ305_mean_g)
plt.plot(x, promath.linearFunc(x, popt_int_g_EJ305[0], popt_int_g_EJ305[1]), label=popt_int_g_EJ305[0])
plt.legend()
# plt.ylim([0.2, 0.3])

plt.subplot(2,2,3)
plt.title('singles neutron pos')
plt.scatter(FOM_vs_thr_singles.threshold, FOM_vs_thr_singles.EJ305_mean_n)
plt.plot(x, promath.linearFunc(x, popt_sing_n_EJ305[0], popt_sing_n_EJ305[1]), label=popt_sing_n_EJ305[0])
plt.legend()
# plt.ylim([0.2, 0.3])

plt.subplot(2,2,4)
plt.title('singles gamma pos')
plt.scatter(FOM_vs_thr_singles.threshold, FOM_vs_thr_singles.EJ305_mean_g)
plt.plot(x, promath.linearFunc(x, popt_sing_g_EJ305[0], popt_sing_g_EJ305[1]), label=popt_sing_g_EJ305[0])
plt.legend()
# plt.ylim([0.2, 0.3])

plt.show()




#####################################################################
########## Figure 10 ################################################
########## FOM vs Threshold #########################################
#####################################################################
offset = 0

plt.figure()
fs.set_style(textwidth=345)
thresholds = FOM_vs_thr_intrinsic.threshold/1000

plt.scatter(thresholds-offset, FOM_vs_thr_intrinsic.NE213A_FOM, s=40, marker='o', facecolors='black', edgecolors='black', label='NE 213A, PS $>0.3$', zorder=2)
plt.errorbar(thresholds-offset, FOM_vs_thr_intrinsic.NE213A_FOM, yerr=FOM_vs_thr_intrinsic.NE213A_FOM_err, ls='none', color='black', zorder=1)

plt.scatter(thresholds[:-4]-offset, FOM_vs_thr_intrinsic.EJ305_FOM[:-4], s=40, marker='v', facecolors='white', edgecolors='black', label='EJ 305', zorder=2)
plt.errorbar(thresholds[:-4]-offset, FOM_vs_thr_intrinsic.EJ305_FOM[:-4], yerr=FOM_vs_thr_intrinsic.EJ305_FOM_err[:-4], ls='none', color='black', zorder=1)

plt.subplots_adjust(hspace = .001, wspace = .001)
plt.legend()
plt.xlim([0, np.max(thresholds)*1.05])
plt.ylim([0.43, np.max(FOM_vs_thr_intrinsic.NE213A_FOM)*1.15])
plt.ylabel('FOM')
plt.xlabel('Neutron Detector Threshold [MeV$_{ee}$]')
plt.grid(alpha=0.5)
plt.show()



###############################################################
############# Figure 11 #######################################
############# PS vs Neutron kenetic energy NE213A vs EJ305 ####
###############################################################
FOM_vs_Tn_intrinsic = FOMfigs.NE213A_EJ305_PS_TOF(plot=False, width=335)
plt.show()

#fitting the mean postions
#neutron positions
popt_int_n_NE213A, pcov_int_n_NE213A = curve_fit(promath.linearFunc, FOM_vs_Tn_intrinsic.energy, FOM_vs_Tn_intrinsic.NE213A_mean_n)
popt_int_n_EJ305, pcov_int_n_EJ305   = curve_fit(promath.linearFunc, FOM_vs_Tn_intrinsic.energy, FOM_vs_Tn_intrinsic.EJ305_mean_n)

popt_int_g_NE213A, pcov_int_g_NE213A = curve_fit(promath.linearFunc, FOM_vs_Tn_intrinsic.energy, FOM_vs_Tn_intrinsic.NE213A_mean_g)
popt_int_g_EJ305, pcov_int_g_EJ305   = curve_fit(promath.linearFunc, FOM_vs_Tn_intrinsic.energy, FOM_vs_Tn_intrinsic.EJ305_mean_g)


#making plots of fits
x = np.arange(0,6250)
plt.figure()
plt.suptitle('NE213A')
plt.subplot(2,1,1)
plt.title('intrinsic neutron pos')
plt.scatter(FOM_vs_Tn_intrinsic.energy, FOM_vs_Tn_intrinsic.NE213A_mean_n)
plt.plot(x, promath.linearFunc(x, popt_int_n_NE213A[0], popt_int_n_NE213A[1]), label=popt_int_n_NE213A[0])
plt.legend()

plt.subplot(2,1,2)
plt.title('intrinsic gamma pos')
plt.scatter(FOM_vs_Tn_intrinsic.energy, FOM_vs_Tn_intrinsic.NE213A_mean_g)
plt.plot(x, promath.linearFunc(x, popt_int_g_NE213A[0], popt_int_g_NE213A[1]), label=popt_int_g_NE213A[0])
plt.legend()
# plt.ylim([0.2, 0.3])


plt.figure()
plt.suptitle('EJ305')
plt.subplot(2,1,1)
plt.title('intrinsic neutron pos')
plt.scatter(FOM_vs_Tn_intrinsic.energy, FOM_vs_Tn_intrinsic.EJ305_mean_n)
plt.plot(x, promath.linearFunc(x, popt_int_n_EJ305[0], popt_int_n_EJ305[1]), label=popt_int_n_EJ305[0])
plt.legend()

plt.subplot(2,1,2)
plt.title('intrinsic gamma pos')
plt.scatter(FOM_vs_Tn_intrinsic.energy, FOM_vs_Tn_intrinsic.EJ305_mean_g)
plt.plot(x, promath.linearFunc(x, popt_int_g_EJ305[0], popt_int_g_EJ305[1]), label=popt_int_g_EJ305[0])
plt.legend()
# plt.ylim([0.2, 0.3])
plt.show()




###############################################################
############# Figure 12 #######################################
############# PS vs Neutron kinetic energy ####################
###############################################################
def p0(x,A):
    return A


plt.figure()
fs.set_style(textwidth=345)
energies = FOM_vs_Tn_intrinsic.energy/1000

#correlated
# plt.subplot(2,1,1)
plt.scatter(energies, FOM_vs_Tn_intrinsic.NE213A_FOM, s=40, marker='o', facecolors='black', edgecolors='black', label='NE 213A, PS $>0.3$', zorder=2)
plt.errorbar(energies, FOM_vs_Tn_intrinsic.NE213A_FOM, yerr=FOM_vs_Tn_intrinsic.NE213A_FOM_err, ls='none', color='black', zorder=1)
plt.legend()
plt.xlim([1.25, 6.25])
plt.ylim([0.54, 1.005])
# plt.ylabel('FOM')
# plt.xlabel('Neutron Energy [MeV]')
# plt.grid(alpha=0.5)




# plt.subplot(2,1,2)
plt.scatter(energies, FOM_vs_Tn_intrinsic.EJ305_FOM, s=40, marker='v', facecolors='white', edgecolors='black', label='EJ 305', zorder=2)
plt.errorbar(energies, FOM_vs_Tn_intrinsic.EJ305_FOM, yerr=FOM_vs_Tn_intrinsic.EJ305_FOM_err, ls='none', color='black', zorder=1)
plt.legend()
# plt.xlim([1.25, 6.25])
# plt.ylim([0.48, 1.005])
plt.ylabel('FOM')
plt.xlabel('Neutron Energy [MeV]')
plt.grid(alpha=0.5)
# plt.subplots_adjust(hspace = .001, wspace = .001)
plt.show()



# plt.scatter(energies, FOM_vs_Tn_intrinsic.EJ305_FOM_DG, s=40, marker='v', facecolors='blue', edgecolors='black', label='EJ 305 (double Gaussian)', zorder=2)
# plt.errorbar(energies, FOM_vs_Tn_intrinsic.EJ305_FOM_DG, yerr=FOM_vs_Tn_intrinsic.EJ305_FOM_err_DG, ls='none', color='black', zorder=1)

# popt1, pcov1 = curve_fit(p0, energies, FOM_vs_Tn_intrinsic.NE213A_FOM, sigma=FOM_vs_Tn_intrinsic.NE213A_FOM_err, absolute_sigma=True)
# popt2, pcov2 = curve_fit(p0, energies, FOM_vs_Tn_intrinsic.EJ305_FOM, sigma=FOM_vs_Tn_intrinsic.EJ305_FOM_err, absolute_sigma=True)

# print(f'NE213A intrinsic mean: {np.round(popt1[0], 4)}+/-{np.round(pcov1[0], 4)}')
# print(f'EJ305 intrinsic mean: {np.round(popt2[0], 4)}+/-{np.round(pcov2[0], 4)}')



###############################################################
############# Figure XXX ######################################
############# EJ305 numeric method vs double gaussian ###############
###############################################################
#fit limits for EJ305
EJ305_start_g = 0.146
EJ305_stop_g = 0.196
EJ305_start_n = 0.210
EJ305_stop_n = 0.280
EJ305_lim_g = [EJ305_start_g, EJ305_stop_g]
EJ305_lim_n = [EJ305_start_n, EJ305_stop_n]

stdFactor = 3

E = 2000

x = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/EJ305/sliced/PS/MeV{E}/x.npy')
yg = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/EJ305/sliced/PS/MeV{E}/yGamma.npy')
yn = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/EJ305/sliced/PS/MeV{E}/yNeutron.npy')

yg, yn = prodata.PSscaler(x, yg, yn, plot=False) #Visually scale data.

doubleGaussParmters = pd.read_pickle('/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/EJ305/doubleGaussParameters/fitParams_thr')
query = f'energy=={E}'
param = [doubleGaussParmters.query(query).amp1.item(), doubleGaussParmters.query(query).mean1.item(), doubleGaussParmters.query(query).std1.item(), doubleGaussParmters.query(query).amp2.item(), doubleGaussParmters.query(query).mean2.item(), doubleGaussParmters.query(query).std2.item()]

EJ305_FOM_numeric, EJ305_FOM_err_numeric, EJ305_param_numeric = prodata.PSFOMnumeric(x, x, yn, yg, fitLim_n=EJ305_lim_n, fitLim_g=EJ305_lim_g, stdFactor=stdFactor, error=True, plot=False, return_param=True)

EJ305_FOM_dg, EJ305_FOM_err_dg, EJ305_param_dg = prodata.PSFOMdoubleGauss(x, x, yn, yg, fitLim_n=[0.100, 0.350], fitLim_g=EJ305_lim_g, param=param, error=True, plot=False, return_param=True)

g = promath.gaussFunc(x, EJ305_param_dg['amp_n1'].item(), EJ305_param_dg['mean_n1'].item(), EJ305_param_dg['std_n1'].item())
EJ305_FOM_dg2, EJ305_FOM_err_dg2, EJ305_param_dg2 = prodata.PSFOMdoubleGauss(x, x, yn-g, yg, fitLim_n=[0.100, 0.350], fitLim_g=EJ305_lim_g, param=param, error=True, plot=False, return_param=True)

print(f' FOM_num = {round(EJ305_FOM_numeric,3)}, FOM_dg = {round(EJ305_FOM_dg,3)}, FOM_dg2 = {round(EJ305_FOM_dg2,3)}')
# print(EJ305_param_dg['amp_n1'].abs().item(), EJ305_param_dg['amp_n2'].abs().item())
print(param)
print('-----------------------------------------------')



plt.figure()
fs.set_style(textwidth=345)
plt.subplot(2,1,1)
plt.title(f'EJ305, Tn = {E} keV')
plt.step(x, yg, color='black', where='mid', lw=0.5, zorder=2)
plt.fill_between(x,yg, color='tomato', step='mid', alpha=0.8, zorder=1)

plt.step(x, yn, color='black', where='mid', lw=0.5, zorder=4)
plt.fill_between(x,yn, color='royalblue', step='mid', alpha=0.6, zorder=3)

plt.plot(np.arange(0, 1, 0.001), promath.gaussFunc(np.arange(0, 1, 0.001), EJ305_param_dg['amp_g'].item(), 
                                                                            EJ305_param_dg['mean_g'].item(), 
                                                                            EJ305_param_dg['std_g'].item()), color='black', lw=2, ls='dashed')

plt.plot(np.arange(0, 1, 0.001), promath.doubleGaussFunc(np.arange(0, 1, 0.001), EJ305_param_dg['amp_n1'].abs().item(), 
                                                                                EJ305_param_dg['mean_n1'].item(),
                                                                                EJ305_param_dg['std_n1'].item(), 
                                                                                EJ305_param_dg['amp_n2'].abs().item(), 
                                                                                EJ305_param_dg['mean_n2'].item(), 
                                                                                EJ305_param_dg['std_n2'].item()), color='black', lw=2)

plt.plot(np.arange(0, 1, 0.001), promath.gaussFunc(np.arange(0, 1, 0.001), EJ305_param_dg['amp_n2'].item(), 
                                                                            EJ305_param_dg['mean_n2'].item(), 
                                                                            EJ305_param_dg['std_n2'].item())+promath.gaussFunc(np.arange(0, 1, 0.001), EJ305_param_dg['amp_n1'].item(), 
                                                                            EJ305_param_dg['mean_n1'].item(), 
                                                                            EJ305_param_dg['std_n1'].item()), color='red', lw=2)

plt.xlabel('PS')
plt.ylabel('Yield [arb. units]')
plt.xlim([0.06, 0.4])
plt.ylim([0, max(yn)*1.1])

plt.subplot(2,1,2)
plt.step(x, yg, color='black', where='mid', lw=0.5, zorder=2)
plt.fill_between(x,yg, color='tomato', step='mid', alpha=0.8, zorder=1)

plt.step(x, yn-g, color='black', where='mid', lw=0.5, zorder=4)
plt.fill_between(x,yn-g, color='royalblue', step='mid', alpha=0.6, zorder=3)

plt.xlabel('PS')
plt.ylabel('Yield [arb. units]')
plt.xlim([0.06, 0.4])
plt.ylim([0, max(yn)*1.1])
plt.show()