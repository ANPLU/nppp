#!/usr/bin/env python3
from re import I
import sys
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
from numpy.core.arrayprint import SubArrayFormat
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit

# IMPORT FROM LIBRARY DIRECTORY
sys.path.insert(0, "../library/")
sys.path.insert(0, "../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "../")
sys.path.insert(0, "data_analysis/")

import processing_math as promath
import processing_data as prodata
import processing_pd as propd
import processing_simulation as prosim
# import gaussEdgeFit_parametersEJ321P as geEJ321P
# import gaussEdgeFit_parametersNE213A as geNE213A
# import simFit_parametersNE213A as simNE213A


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#    DEFINE PARAMTERS FOR SLICE
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
gTOFRange = [1e-9, 6e-9] #Define range of gamma-flash
randTOFRange = [-710e-9,-10e-9] #Define range of random events

gLenght = np.abs(gTOFRange[1]-gTOFRange[0]) #Calculate the lenght of cut for gammas
randLenght = np.abs(randTOFRange[1]-randTOFRange[0]) #Calculate lenght of cut for random events

y2Thr = 0 #Amplitude threhold in mV
y3Thr = 0 #Amplitude threhold in mV
y4Thr = 0 #Amplitude threhold in mV
y5Thr = 0 #Amplitude threhold in mV
nThr = 0 #Amplitude threshold in mV

y2ThrQDC = 7500 #QDC threhold in channels
y3ThrQDC = 7500 #QDC threhold in channels
y4ThrQDC = 7500 #QDC threhold in channels
y5ThrQDC = 7500 #QDC threhold in channels
nThrQDC = 0 #QDC threshold in channels

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# LOADING DATA & SLICING LOOP
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop
run1 = 2121
num = 0
for i in np.arange(0, 140, 5):
    print('-------------------------------------------------')
    print(f'Now working on runs: {run1+i}-{run1+i+4}')
    PuBe_tof_data =   propd.load_parquet_merge(path, np.arange(run1+i,run1+i+5), keep_col=[ 
                                                                                'qdc_lg_ch1',
                                                                                'qdc_sg_ch1',
                                                                                'qdc_lg_ch2',
                                                                                'qdc_lg_ch3',
                                                                                'qdc_lg_ch4',
                                                                                'qdc_lg_ch5',
                                                                                'amplitude_ch1', 
                                                                                'amplitude_ch2', 
                                                                                'amplitude_ch3', 
                                                                                'amplitude_ch4', 
                                                                                'amplitude_ch5', 
                                                                                'qdc_ps_ch1', 
                                                                                'tof_ch2', 
                                                                                'tof_ch3', 
                                                                                'tof_ch4', 
                                                                                'tof_ch5'], 
                                                                                full=False)

    #~~~~~~~~~~~~~~~~~~~~~~~~
    #      RESET INDEX
    #~~~~~~~~~~~~~~~~~~~~~~~~
    PuBe_tof_data = PuBe_tof_data.reset_index(drop=True)

    #~~~~~~~~~~~~~~~~~~~~~~~~
    #    CALIBRATING ToF
    #~~~~~~~~~~~~~~~~~~~~~~~~
    flashrange = {'tof_ch2':[60, 76], 'tof_ch3':[60, 76], 'tof_ch4':[60, 76], 'tof_ch5':[60, 76]} #gamma flash fitting ranges
    distance = 0.959 #distance between source and center of detector
    prodata.tof_calibration(PuBe_tof_data, flashrange, distance, phcut=125, calUnit=1e-9, energyCal=True)

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    #   SLICE Neutron ToF ENERGY for QDC LG
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    y2QDC_lg, y3QDC_lg, y4QDC_lg, y5QDC_lg =    prodata.tofEnergySlicer(PuBe_tof_data, 
                                                                        minE = 1.25, 
                                                                        maxE = 6.25, 
                                                                        dE = 0.5, 
                                                                        colKeep = 'qdc_lg_ch1', 
                                                                        n_thr = nThr, 
                                                                        y_thr = [[y2Thr, 1000], [y3Thr, 1000], [y4Thr, 1000], [y5Thr, 1000]]
                                                                        )

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    #   SLICE Neutron ToF ENERGY for QDC SG
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    y2QDC_sg, y3QDC_sg, y4QDC_sg, y5QDC_sg =    prodata.tofEnergySlicer(PuBe_tof_data, 
                                                                        minE = 1.25, 
                                                                        maxE = 6.25, 
                                                                        dE = 0.5, 
                                                                        colKeep = 'qdc_sg_ch1', 
                                                                        n_thr = nThr, 
                                                                        y_thr = [[y2Thr, 1000], [y3Thr, 1000], [y4Thr, 1000], [y5Thr, 1000]]
                                                                        )                                                                
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    #   SLICE Neutron ToF ENERGY for PS
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    y2PS, y3PS, y4PS, y5PS =                    prodata.tofEnergySlicer(PuBe_tof_data, 
                                                                        minE = 1.25, 
                                                                        maxE = 6.25, 
                                                                        dE = 0.5, 
                                                                        colKeep = 'qdc_ps_ch1', 
                                                                        n_thr = nThr, 
                                                                        y_thr = [[y2Thr, 1000], [y3Thr, 1000], [y4Thr, 1000], [y5Thr, 1000]]
                                                                        )

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    #   Get QDC LG-data from Raw and Gamma-flash and random ToF cuts
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    #Get raw data
    y2RawQDC_lg = pd.DataFrame(PuBe_tof_data.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch2>{y2ThrQDC} and tof_ch2>0 and tof_ch2<100 and amplitude_ch2>{y2Thr} and amplitude_ch1>{nThr}').qdc_lg_ch1)
    y3RawQDC_lg = pd.DataFrame(PuBe_tof_data.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch3>{y3ThrQDC} and tof_ch3>0 and tof_ch3<100 and amplitude_ch3>{y3Thr} and amplitude_ch1>{nThr}').qdc_lg_ch1)
    y4RawQDC_lg = pd.DataFrame(PuBe_tof_data.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch4>{y4ThrQDC} and tof_ch4>0 and tof_ch4<100 and amplitude_ch4>{y4Thr} and amplitude_ch1>{nThr}').qdc_lg_ch1)
    y5RawQDC_lg = pd.DataFrame(PuBe_tof_data.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch5>{y5ThrQDC} and tof_ch5>0 and tof_ch5<100 and amplitude_ch5>{y5Thr} and amplitude_ch1>{nThr}').qdc_lg_ch1)
    #remove multiplicity events
    mpIdx = prodata.uniqueEventHelper(y2RawQDC_lg, y3RawQDC_lg, y4RawQDC_lg, y5RawQDC_lg)
    y2RawQDC_lg = y2RawQDC_lg.drop(mpIdx, errors='ignore')
    y3RawQDC_lg = y3RawQDC_lg.drop(mpIdx, errors='ignore')
    y4RawQDC_lg = y4RawQDC_lg.drop(mpIdx, errors='ignore')
    y5RawQDC_lg = y5RawQDC_lg.drop(mpIdx, errors='ignore')

    #Get random ToF cut for each YAP
    y2RandQDC_lg = PuBe_tof_data.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch2>{y2ThrQDC} and tof_ch2>{randTOFRange[0]} and tof_ch2<={randTOFRange[1]} and amplitude_ch2>{y2Thr} and amplitude_ch1>{nThr}').qdc_lg_ch1 #YAP2
    y3RandQDC_lg = PuBe_tof_data.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch3>{y3ThrQDC} and tof_ch3>{randTOFRange[0]} and tof_ch3<={randTOFRange[1]} and amplitude_ch3>{y3Thr} and amplitude_ch1>{nThr}').qdc_lg_ch1 #YAP3
    y4RandQDC_lg = PuBe_tof_data.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch4>{y4ThrQDC} and tof_ch4>{randTOFRange[0]} and tof_ch4<={randTOFRange[1]} and amplitude_ch4>{y4Thr} and amplitude_ch1>{nThr}').qdc_lg_ch1 #YAP4
    y5RandQDC_lg = PuBe_tof_data.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch5>{y5ThrQDC} and tof_ch5>{randTOFRange[0]} and tof_ch5<={randTOFRange[1]} and amplitude_ch5>{y5Thr} and amplitude_ch1>{nThr}').qdc_lg_ch1 #YAP5
    #remove multiplicity events
    mpIdx = prodata.uniqueEventHelper(y2RandQDC_lg, y3RandQDC_lg, y4RandQDC_lg, y5RandQDC_lg)
    y2RandQDC_lg = y2RandQDC_lg.drop(mpIdx, errors='ignore')
    y3RandQDC_lg = y3RandQDC_lg.drop(mpIdx, errors='ignore')
    y4RandQDC_lg = y4RandQDC_lg.drop(mpIdx, errors='ignore')
    y5RandQDC_lg = y5RandQDC_lg.drop(mpIdx, errors='ignore')

    #Get gamma-flash ToF cut values for each YAP
    y2GammaQDC_lg = PuBe_tof_data.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch2>{y2ThrQDC} and tof_ch2>{gTOFRange[0]} and tof_ch2<={gTOFRange[1]} and amplitude_ch2>{y2Thr} and amplitude_ch1>{nThr}').qdc_lg_ch1 #YAP2
    y3GammaQDC_lg = PuBe_tof_data.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch3>{y3ThrQDC} and tof_ch3>{gTOFRange[0]} and tof_ch3<={gTOFRange[1]} and amplitude_ch3>{y3Thr} and amplitude_ch1>{nThr}').qdc_lg_ch1 #YAP3
    y4GammaQDC_lg = PuBe_tof_data.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch4>{y4ThrQDC} and tof_ch4>{gTOFRange[0]} and tof_ch4<={gTOFRange[1]} and amplitude_ch4>{y4Thr} and amplitude_ch1>{nThr}').qdc_lg_ch1 #YAP4
    y5GammaQDC_lg = PuBe_tof_data.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch5>{y5ThrQDC} and tof_ch5>{gTOFRange[0]} and tof_ch5<={gTOFRange[1]} and amplitude_ch5>{y5Thr} and amplitude_ch1>{nThr}').qdc_lg_ch1 #YAP5
    #remove multiplicity events
    mpIdx = prodata.uniqueEventHelper(y2GammaQDC_lg, y3GammaQDC_lg, y4GammaQDC_lg, y5GammaQDC_lg)
    y2GammaQDC_lg = y2GammaQDC_lg.drop(mpIdx, errors='ignore')
    y3GammaQDC_lg = y3GammaQDC_lg.drop(mpIdx, errors='ignore')
    y4GammaQDC_lg = y4GammaQDC_lg.drop(mpIdx, errors='ignore')
    y5GammaQDC_lg = y5GammaQDC_lg.drop(mpIdx, errors='ignore')

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    #   Get QDC SG-data from Raw and Gamma-flash and random ToF cuts
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    #Get raw data
    y2RawQDC_sg = pd.DataFrame(PuBe_tof_data.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch2>{y2ThrQDC} and tof_ch2>0 and tof_ch2<100 and amplitude_ch2>{y2Thr} and amplitude_ch1>{nThr}').qdc_sg_ch1)
    y3RawQDC_sg = pd.DataFrame(PuBe_tof_data.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch3>{y3ThrQDC} and tof_ch3>0 and tof_ch3<100 and amplitude_ch3>{y3Thr} and amplitude_ch1>{nThr}').qdc_sg_ch1)
    y4RawQDC_sg = pd.DataFrame(PuBe_tof_data.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch4>{y4ThrQDC} and tof_ch4>0 and tof_ch4<100 and amplitude_ch4>{y4Thr} and amplitude_ch1>{nThr}').qdc_sg_ch1)
    y5RawQDC_sg = pd.DataFrame(PuBe_tof_data.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch5>{y5ThrQDC} and tof_ch5>0 and tof_ch5<100 and amplitude_ch5>{y5Thr} and amplitude_ch1>{nThr}').qdc_sg_ch1)
    #remove multiplicity events
    mpIdx = prodata.uniqueEventHelper(y2RawQDC_sg, y3RawQDC_sg, y4RawQDC_sg, y5RawQDC_sg)
    y2RawQDC_sg = y2RawQDC_sg.drop(mpIdx, errors='ignore')
    y3RawQDC_sg = y3RawQDC_sg.drop(mpIdx, errors='ignore')
    y4RawQDC_sg = y4RawQDC_sg.drop(mpIdx, errors='ignore')
    y5RawQDC_sg = y5RawQDC_sg.drop(mpIdx, errors='ignore')

    #Get random ToF cut for each YAP
    y2RandQDC_sg = PuBe_tof_data.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch2>{y2ThrQDC} and tof_ch2>{randTOFRange[0]} and tof_ch2<={randTOFRange[1]} and amplitude_ch2>{y2Thr} and amplitude_ch1>{nThr}').qdc_sg_ch1 #YAP2
    y3RandQDC_sg = PuBe_tof_data.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch3>{y3ThrQDC} and tof_ch3>{randTOFRange[0]} and tof_ch3<={randTOFRange[1]} and amplitude_ch3>{y3Thr} and amplitude_ch1>{nThr}').qdc_sg_ch1 #YAP3
    y4RandQDC_sg = PuBe_tof_data.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch4>{y4ThrQDC} and tof_ch4>{randTOFRange[0]} and tof_ch4<={randTOFRange[1]} and amplitude_ch4>{y4Thr} and amplitude_ch1>{nThr}').qdc_sg_ch1 #YAP4
    y5RandQDC_sg = PuBe_tof_data.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch5>{y5ThrQDC} and tof_ch5>{randTOFRange[0]} and tof_ch5<={randTOFRange[1]} and amplitude_ch5>{y5Thr} and amplitude_ch1>{nThr}').qdc_sg_ch1 #YAP5
    #remove multiplicity events
    mpIdx = prodata.uniqueEventHelper(y2RandQDC_sg, y3RandQDC_sg, y4RandQDC_sg, y5RandQDC_sg)
    y2RandQDC_sg = y2RandQDC_sg.drop(mpIdx, errors='ignore')
    y3RandQDC_sg = y3RandQDC_sg.drop(mpIdx, errors='ignore')
    y4RandQDC_sg = y4RandQDC_sg.drop(mpIdx, errors='ignore')
    y5RandQDC_sg = y5RandQDC_sg.drop(mpIdx, errors='ignore')
    
    #Get gamma-flash ToF cut values for each YAP
    y2GammaQDC_sg = PuBe_tof_data.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch2>{y2ThrQDC} and tof_ch2>{gTOFRange[0]} and tof_ch2<={gTOFRange[1]} and amplitude_ch2>{y2Thr} and amplitude_ch1>{nThr}').qdc_sg_ch1 #YAP2
    y3GammaQDC_sg = PuBe_tof_data.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch3>{y3ThrQDC} and tof_ch3>{gTOFRange[0]} and tof_ch3<={gTOFRange[1]} and amplitude_ch3>{y3Thr} and amplitude_ch1>{nThr}').qdc_sg_ch1 #YAP3
    y4GammaQDC_sg = PuBe_tof_data.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch4>{y4ThrQDC} and tof_ch4>{gTOFRange[0]} and tof_ch4<={gTOFRange[1]} and amplitude_ch4>{y4Thr} and amplitude_ch1>{nThr}').qdc_sg_ch1 #YAP4
    y5GammaQDC_sg = PuBe_tof_data.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch5>{y5ThrQDC} and tof_ch5>{gTOFRange[0]} and tof_ch5<={gTOFRange[1]} and amplitude_ch5>{y5Thr} and amplitude_ch1>{nThr}').qdc_sg_ch1 #YAP5
    #remove multiplicity events
    mpIdx = prodata.uniqueEventHelper(y2GammaQDC_sg, y3GammaQDC_sg, y4GammaQDC_sg, y5GammaQDC_sg)
    y2GammaQDC_sg = y2GammaQDC_sg.drop(mpIdx, errors='ignore')
    y3GammaQDC_sg = y3GammaQDC_sg.drop(mpIdx, errors='ignore')
    y4GammaQDC_sg = y4GammaQDC_sg.drop(mpIdx, errors='ignore')
    y5GammaQDC_sg = y5GammaQDC_sg.drop(mpIdx, errors='ignore')

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    #   Get PS-data from Raw and Gamma-flash and random ToF cuts
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    #Get raw data
    y2RawPS = pd.DataFrame(PuBe_tof_data.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch2>{y2ThrQDC} and tof_ch2>0 and tof_ch2<100 and amplitude_ch2>{y2Thr} and amplitude_ch1>{nThr}').qdc_ps_ch1)
    y3RawPS = pd.DataFrame(PuBe_tof_data.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch3>{y3ThrQDC} and tof_ch3>0 and tof_ch3<100 and amplitude_ch3>{y3Thr} and amplitude_ch1>{nThr}').qdc_ps_ch1)
    y4RawPS = pd.DataFrame(PuBe_tof_data.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch4>{y4ThrQDC} and tof_ch4>0 and tof_ch4<100 and amplitude_ch4>{y4Thr} and amplitude_ch1>{nThr}').qdc_ps_ch1)
    y5RawPS = pd.DataFrame(PuBe_tof_data.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch5>{y5ThrQDC} and tof_ch5>0 and tof_ch5<100 and amplitude_ch5>{y5Thr} and amplitude_ch1>{nThr}').qdc_ps_ch1)
    #remove multiplicity events
    mpIdx = prodata.uniqueEventHelper(y2RawPS, y3RawPS, y4RawPS, y5RawPS)
    y2RawPS = y2RawPS.drop(mpIdx, errors='ignore')
    y3RawPS = y3RawPS.drop(mpIdx, errors='ignore')
    y4RawPS = y4RawPS.drop(mpIdx, errors='ignore')
    y5RawPS = y5RawPS.drop(mpIdx, errors='ignore')

    #Get random ToF cut for each YAP
    y2RandPS = PuBe_tof_data.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch2>{y2ThrQDC} and tof_ch2>{randTOFRange[0]} and tof_ch2<={randTOFRange[1]} and amplitude_ch2>{y2Thr} and amplitude_ch1>{nThr}').qdc_ps_ch1 #YAP2
    y3RandPS = PuBe_tof_data.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch3>{y3ThrQDC} and tof_ch3>{randTOFRange[0]} and tof_ch3<={randTOFRange[1]} and amplitude_ch3>{y3Thr} and amplitude_ch1>{nThr}').qdc_ps_ch1 #YAP3
    y4RandPS = PuBe_tof_data.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch4>{y4ThrQDC} and tof_ch4>{randTOFRange[0]} and tof_ch4<={randTOFRange[1]} and amplitude_ch4>{y4Thr} and amplitude_ch1>{nThr}').qdc_ps_ch1 #YAP4
    y5RandPS = PuBe_tof_data.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch5>{y5ThrQDC} and tof_ch5>{randTOFRange[0]} and tof_ch5<={randTOFRange[1]} and amplitude_ch5>{y5Thr} and amplitude_ch1>{nThr}').qdc_ps_ch1 #YAP5
    #remove multiplicity events
    mpIdx = prodata.uniqueEventHelper(y2RandPS, y3RandPS, y4RandPS, y5RandPS)
    y2RandPS = y2RandPS.drop(mpIdx, errors='ignore')
    y3RandPS = y3RandPS.drop(mpIdx, errors='ignore')
    y4RandPS = y4RandPS.drop(mpIdx, errors='ignore')
    y5RandPS = y5RandPS.drop(mpIdx, errors='ignore')

    #Get gamma-flash ToF cut for each YAP
    y2GammaPS = PuBe_tof_data.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch2>{y2ThrQDC} and tof_ch2>{gTOFRange[0]} and tof_ch2<={gTOFRange[1]} and amplitude_ch2>{y2Thr} and amplitude_ch1>{nThr}').qdc_ps_ch1 #YAP2
    y3GammaPS = PuBe_tof_data.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch3>{y3ThrQDC} and tof_ch3>{gTOFRange[0]} and tof_ch3<={gTOFRange[1]} and amplitude_ch3>{y3Thr} and amplitude_ch1>{nThr}').qdc_ps_ch1 #YAP3
    y4GammaPS = PuBe_tof_data.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch4>{y4ThrQDC} and tof_ch4>{gTOFRange[0]} and tof_ch4<={gTOFRange[1]} and amplitude_ch4>{y4Thr} and amplitude_ch1>{nThr}').qdc_ps_ch1 #YAP4
    y5GammaPS = PuBe_tof_data.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch5>{y5ThrQDC} and tof_ch5>{gTOFRange[0]} and tof_ch5<={gTOFRange[1]} and amplitude_ch5>{y5Thr} and amplitude_ch1>{nThr}').qdc_ps_ch1 #YAP5
    #remove multiplicity events
    mpIdx = prodata.uniqueEventHelper(y2GammaPS, y3GammaPS, y4GammaPS, y5GammaPS)
    y2GammaPS = y2GammaPS.drop(mpIdx, errors='ignore')
    y3GammaPS = y3GammaPS.drop(mpIdx, errors='ignore')
    y4GammaPS = y4GammaPS.drop(mpIdx, errors='ignore')
    y5GammaPS = y5GammaPS.drop(mpIdx, errors='ignore')

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    #   Merge Random and Gamma-flash QDC LG data with repective energy sliced data
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    y2QDC_lgFin = pd.concat([pd.DataFrame({'random':y2RandQDC_lg, 'gamma':y2GammaQDC_lg}), y2QDC_lg]) #YAP2
    y3QDC_lgFin = pd.concat([pd.DataFrame({'random':y3RandQDC_lg, 'gamma':y3GammaQDC_lg}), y3QDC_lg]) #YAP3
    y4QDC_lgFin = pd.concat([pd.DataFrame({'random':y4RandQDC_lg, 'gamma':y4GammaQDC_lg}), y4QDC_lg]) #YAP4
    y5QDC_lgFin = pd.concat([pd.DataFrame({'random':y5RandQDC_lg, 'gamma':y5GammaQDC_lg}), y5QDC_lg]) #YAP5

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    #   Merge Random and Gamma-flash QDC SG data with repective energy sliced data
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    y2QDC_sgFin = pd.concat([pd.DataFrame({'random':y2RandQDC_sg, 'gamma':y2GammaQDC_sg}), y2QDC_sg]) #YAP2
    y3QDC_sgFin = pd.concat([pd.DataFrame({'random':y3RandQDC_sg, 'gamma':y3GammaQDC_sg}), y3QDC_sg]) #YAP3
    y4QDC_sgFin = pd.concat([pd.DataFrame({'random':y4RandQDC_sg, 'gamma':y4GammaQDC_sg}), y4QDC_sg]) #YAP4
    y5QDC_sgFin = pd.concat([pd.DataFrame({'random':y5RandQDC_sg, 'gamma':y5GammaQDC_sg}), y5QDC_sg]) #YAP5

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    #   Merge Random and Gamma-flash PS data with repective energy sliced data
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    y2PSFin = pd.concat([pd.DataFrame({'random':y2RandPS, 'gamma':y2GammaPS}), y2PS]) #YAP2
    y3PSFin = pd.concat([pd.DataFrame({'random':y3RandPS, 'gamma':y3GammaPS}), y3PS]) #YAP3
    y4PSFin = pd.concat([pd.DataFrame({'random':y4RandPS, 'gamma':y4GammaPS}), y4PS]) #YAP4
    y5PSFin = pd.concat([pd.DataFrame({'random':y5RandPS, 'gamma':y5GammaPS}), y5PS]) #YAP5

    #~~~~~~~~~~~~~~~~~~~~~~~~~
    #   Saving data to disk
    #~~~~~~~~~~~~~~~~~~~~~~~~~
    print(f'Saving to disk... part{num}')

    y2QDC_lgFin.to_parquet(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/sliced/YAP_thresholds/YAP_7500/yap2_QDC_lg_part{num}.pkl')
    y3QDC_lgFin.to_parquet(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/sliced/YAP_thresholds/YAP_7500/yap3_QDC_lg_part{num}.pkl')
    y4QDC_lgFin.to_parquet(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/sliced/YAP_thresholds/YAP_7500/yap4_QDC_lg_part{num}.pkl')
    y5QDC_lgFin.to_parquet(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/sliced/YAP_thresholds/YAP_7500/yap5_QDC_lg_part{num}.pkl')
    
    y2QDC_sgFin.to_parquet(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/sliced/YAP_thresholds/YAP_7500/yap2_QDC_sg_part{num}.pkl')
    y3QDC_sgFin.to_parquet(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/sliced/YAP_thresholds/YAP_7500/yap3_QDC_sg_part{num}.pkl')
    y4QDC_sgFin.to_parquet(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/sliced/YAP_thresholds/YAP_7500/yap4_QDC_sg_part{num}.pkl')
    y5QDC_sgFin.to_parquet(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/sliced/YAP_thresholds/YAP_7500/yap5_QDC_sg_part{num}.pkl')
    
    y2PSFin.to_parquet(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/sliced/YAP_thresholds/YAP_7500/yap2_PS_part{num}.pkl')
    y3PSFin.to_parquet(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/sliced/YAP_thresholds/YAP_7500/yap3_PS_part{num}.pkl')
    y4PSFin.to_parquet(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/sliced/YAP_thresholds/YAP_7500/yap4_PS_part{num}.pkl')
    y5PSFin.to_parquet(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/sliced/YAP_thresholds/YAP_7500/yap5_PS_part{num}.pkl')

    y2RawQDC_lg.to_parquet(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/sliced/YAP_thresholds/YAP_7500/yap2_raw_QDC_lg_part{num}.pkl')
    y3RawQDC_lg.to_parquet(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/sliced/YAP_thresholds/YAP_7500/yap3_raw_QDC_lg_part{num}.pkl')
    y4RawQDC_lg.to_parquet(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/sliced/YAP_thresholds/YAP_7500/yap4_raw_QDC_lg_part{num}.pkl')
    y5RawQDC_lg.to_parquet(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/sliced/YAP_thresholds/YAP_7500/yap5_raw_QDC_lg_part{num}.pkl')

    y2RawQDC_sg.to_parquet(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/sliced/YAP_thresholds/YAP_7500/yap2_raw_QDC_sg_part{num}.pkl')
    y3RawQDC_sg.to_parquet(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/sliced/YAP_thresholds/YAP_7500/yap3_raw_QDC_sg_part{num}.pkl')
    y4RawQDC_sg.to_parquet(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/sliced/YAP_thresholds/YAP_7500/yap4_raw_QDC_sg_part{num}.pkl')
    y5RawQDC_sg.to_parquet(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/sliced/YAP_thresholds/YAP_7500/yap5_raw_QDC_sg_part{num}.pkl')

    y2RawPS.to_parquet(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/sliced/YAP_thresholds/YAP_7500/yap2_raw_PS_part{num}.pkl')
    y3RawPS.to_parquet(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/sliced/YAP_thresholds/YAP_7500/yap3_raw_PS_part{num}.pkl')
    y4RawPS.to_parquet(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/sliced/YAP_thresholds/YAP_7500/yap4_raw_PS_part{num}.pkl')
    y5RawPS.to_parquet(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/sliced/YAP_thresholds/YAP_7500/yap5_raw_PS_part{num}.pkl')

    num+= 1

print('DONE!')