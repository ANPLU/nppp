#!/usr/bin/env python3
from re import I
import sys
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
from numpy.core.arrayprint import SubArrayFormat
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit
import random
# from matplotlib.colors import LinearSegmentedColormap


# IMPORT FROM LIBRARY DIRECTORY
sys.path.insert(0, "../library/")
sys.path.insert(0, "../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "../")
sys.path.insert(0, "data_analysis/")

import processing_simulation as prosim
import processing_math as promath
import processing_data as prodata
import processing_pd as propd
import figure_style as fs

########################################
######## DETECTOR SETTINGS #############
########################################

detector = "EJ305"
run1 = 3038
numRuns = 201
runList = np.arange(run1, run1+numRuns)

# detector = "NE213A"
# run1 = 2121
# numRuns = 141
# runList = np.arange(run1, run1+numRuns,8)
# runList = np.arange(3038, 3048, 1)
# runList = [run1, 2950, 2970, 2980, 3000]

gainOffsets = prodata.gainAlignCalculator('/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/', runList, 12500, 14200, 200, 'qdc_lg_ch1', verbose=True)
plt.scatter(runList, (1-gainOffsets)*100, color='blue')
plt.ylabel('Offset [%]')
plt.xlabel('Run number')
plt.show()





#QDC gain align test
for i, run in enumerate(runList):
    dfCurrent = propd.load_parquet_merge('/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/', [run], keep_col=[f'qdc_lg_ch1'], full=False)
    plt.subplot(1, 2, 1)
    plt.hist(dfCurrent.qdc_lg_ch1, bins=np.arange(0, 45000, 200), histtype='step')
    plt.yscale('log')
    plt.subplot(1, 2, 2)
    plt.hist(dfCurrent.qdc_lg_ch1*gainOffsets[i], bins=np.arange(0, 45000, 200), histtype='step')
    plt.yscale('log')
plt.show()




#Number of events vs runs
for i, run in enumerate(runList):
    print(f'run {i+1}/{len(runList)}')
    dfCurrent = propd.load_parquet_merge('/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/', [run], keep_col=[f'qdc_lg_ch1'], full=False)
    plt.subplot(1,2,1)
    plt.scatter(run, len(dfCurrent.query('qdc_lg_ch1>700'))/1e6, color='blue')
    plt.ylabel('Number of events [1e6]')
    plt.ylabel('run number')
    plt.subplot(1,2,2)
    plt.scatter(run, len(dfCurrent.query('qdc_lg_ch1>2000'))/1e6, color='blue')
    plt.ylabel('Number of events [1e6]')
    plt.ylabel('run number')
plt.show()




