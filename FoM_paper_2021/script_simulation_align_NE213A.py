#!/usr/bin/env python3
import sys
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit

# IMPORT FROM LIBRARY DIRECTORY
sys.path.insert(0, "../library/")
sys.path.insert(0, "../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "sim_analysis/")
sys.path.insert(0, "data_analysis/")

import processing_pd as propd
import processing_simulation as prosim
import processing_data as prodata

"""
Script for running both data and simulation and calibrating data.
"""

PMT_gain = 4e6 #Nominal gain is 7'000'000 at nominal A/lm (model: 9821B p1)
att_factor_A = 6.14102564 #used when attenuating the signal before the digitizer.
q = 1.60217662E-19 #charge of a single electron
plot = False


##############################
# LOADING DATA
##############################
path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop
bg_runs =       [1667, 1668, 1676, 1677, 1678, 1679, 1680, 1681, 1682, 1683, 1684, 1685] #12st
Cs137_runs =    [1657, 1658, 1659, 1660, 1661, 1662, 1663 ,1664 ,1665, 1666] #10st
Na22_runs =     [1669, 1670] #2st
Th232_runs =    [1671, 1672, 1673, 1674, 1675] #5st
AmBe_runs =     [1693, 1694, 1695, 1696, 1697, 1698, 1699, 1700, 1701, 1702] #10st

bg_data =         propd.load_parquet_merge(path, bg_runs,     keep_col=['qdc_lg_ch1', 'amplitude_ch1'], full=False)
Cs137_data =      propd.load_parquet_merge(path, Cs137_runs,  keep_col=['qdc_lg_ch1', 'amplitude_ch1'], full=False)
Na22_data =       propd.load_parquet_merge(path, Na22_runs,   keep_col=['qdc_lg_ch1', 'amplitude_ch1'], full=False)
Th232_data =      propd.load_parquet_merge(path, Th232_runs,  keep_col=['qdc_lg_ch1', 'amplitude_ch1'], full=False)
AmBe_data =       propd.load_parquet_merge(path, AmBe_runs,   keep_col=['qdc_lg_ch1', 'amplitude_ch1'], full=False)


###############################
# LOADING SIMULATION DATA
###############################
path_sim = '/media/gheed/Seagate_Expansion_Drive1/data/simulation/'
Cs137_sim =         np.genfromtxt(path_sim + 'NE213A/Cs137/isotropic/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
Na22_sim =          np.genfromtxt(path_sim + 'NE213A/Na22/isotropic/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
Na22_511_sim =      np.genfromtxt(path_sim + 'NE213A/Na22_511/isotropic/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
Na22_1275_sim =     np.genfromtxt(path_sim + 'NE213A/Na22_1275/isotropic/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
Th232_sim =         np.genfromtxt(path_sim + 'NE213A/2_62MeV/isotropic/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
AmBe_sim =          np.genfromtxt(path_sim + 'NE213A/4_44MeV/isotropic/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]

#append extra data
Cs137_sim =         np.append(Cs137_sim, np.genfromtxt(path_sim + 'NE213A/Cs137/isotropic/part2/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])
Cs137_sim =         np.append(Cs137_sim, np.genfromtxt(path_sim + 'NE213A/Cs137/isotropic/part3/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])

Na22_511_sim =      np.append(Na22_511_sim, np.genfromtxt(path_sim + 'NE213A/Na22_511/isotropic/part2/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])
Na22_511_sim =      np.append(Na22_511_sim, np.genfromtxt(path_sim + 'NE213A/Na22_511/isotropic/part3/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])

Na22_1275_sim =     np.append(Na22_1275_sim, np.genfromtxt(path_sim + 'NE213A/Na22_1275/isotropic/part2/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])
Na22_1275_sim =     np.append(Na22_1275_sim, np.genfromtxt(path_sim + 'NE213A/Na22_1275/isotropic/part3/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])

Th232_sim =         np.append(Th232_sim, np.genfromtxt(path_sim + 'NE213A/2_62MeV/isotropic/part2/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])
Th232_sim =         np.append(Th232_sim, np.genfromtxt(path_sim + 'NE213A/2_62MeV/isotropic/part3/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])

AmBe_sim =          np.append(AmBe_sim, np.genfromtxt(path_sim + 'NE213A/4_44MeV/isotropic/part2/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])
AmBe_sim =          np.append(AmBe_sim, np.genfromtxt(path_sim + 'NE213A/4_44MeV/isotropic/part3/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])

#############################
# CONVERT INTO FLOAT 
#############################
Cs137_sim =         pd.to_numeric(Cs137_sim, downcast='float')
Na22_511_sim =      pd.to_numeric(Na22_511_sim, downcast='float')
Na22_1275_sim =     pd.to_numeric(Na22_1275_sim, downcast='float')
Th232_sim =         pd.to_numeric(Th232_sim, downcast='float')
AmBe_sim =          pd.to_numeric(AmBe_sim, downcast='float')

###############################
# CALIBRATING SIMULATION DATA
###############################
Cs137_sim =         prosim.chargeCalibration(Cs137_sim      * q * PMT_gain)
Na22_511_sim =      prosim.chargeCalibration(Na22_511_sim   * q * PMT_gain)
Na22_1275_sim =     prosim.chargeCalibration(Na22_1275_sim  * q * PMT_gain)
Th232_sim =         prosim.chargeCalibration(Th232_sim      * q * PMT_gain)
AmBe_sim =          prosim.chargeCalibration(AmBe_sim       * q * PMT_gain)

plot = True 

###############################
# PMT ALIGN Na22 511 keV
##############################
res = 0.23
Na22_511_sim_smear = np.zeros(len(Na22_511_sim))
for i in range(len(Na22_511_sim)):
    Na22_511_sim_smear[i] = prosim.gaussSmear(Na22_511_sim[i], res)

# w = np.empty(len(bg_data))
# w.fill(1/12)
# countsBG, binsBG = np.histogram(bg_data.qdc_lg_ch1*att_factor_A, bins=300, range=[0, 24000], weights=w)

# w = np.empty(len(Na22_data))
# w.fill(1/2)
# counts, bins = np.histogram(Na22_data.qdc_lg_ch1*att_factor_A, bins=300, range=[0, 24000], weights=w)
# bins = prodata.getBinCenters(bins)

# countsSim, binsSim = np.histogram(Na22_511_sim_smear*0.778, bins=300, range=[0, 24000])
# binsSim = prodata.getBinCenters(binsSim)

# plt.plot(binsSim, countsSim*10.606, label='sim')
# plt.plot(bins, counts-countsBG, label='data-BG')
# plt.legend()
# plt.show()


Na22_511_results = prosim.PMTGainAlign( model = Na22_511_sim_smear, 
                                    data = Na22_data.qdc_lg_ch1*att_factor_A, 
                                    bg = bg_data.qdc_lg_ch1*att_factor_A,
                                    scale = 10,
                                    gain = .7,
                                    data_weight = 2,
                                    bg_weight = 12,
                                    scaleBound = [8 , 15],
                                    gainBound = [.6, 1],
                                    stepSize = 1e-2,
                                    # binRange = list(range(2500, 4200, 75)),
                                    binRange = list(range(1850, 3500, 75)),
                                    plot = plot)
Na22_511_results['smear'] = res


###############################
# PMT ALIGN Cs-137
###############################
res = 0.12
Cs137_sim_smear = np.zeros(len(Cs137_sim))
for i in range(len(Cs137_sim)):
    Cs137_sim_smear[i] = prosim.gaussSmear(Cs137_sim[i], res)

# numBins = 300
# binRange = [0,24000]

# w = np.empty(len(bg_data))
# w.fill(1/12)
# countsBG, binsBG = np.histogram(bg_data.qdc_lg_ch1*att_factor_A, bins=numBins, range=binRange, weights=w)

# w = np.empty(len(Cs137_data))
# w.fill(1/10)
# counts, bins = np.histogram(Cs137_data.qdc_lg_ch1*att_factor_A, bins=numBins, range=binRange, weights=w)
# bins = prodata.getBinCenters(bins)


# countsSim, binsSim = np.histogram(Cs137_sim_smear*0.717, bins=numBins, range=binRange)
# binsSim = prodata.getBinCenters(binsSim)

# plt.plot(binsSim, countsSim*2.95, label='sim')
# plt.plot(bins, counts-countsBG, label='data-BG')
# plt.legend()
# plt.show()

Cs137_662_results = prosim.PMTGainAlign( model = Cs137_sim_smear, 
                                    data = Cs137_data.qdc_lg_ch1*att_factor_A, 
                                    bg = bg_data.qdc_lg_ch1*att_factor_A,
                                    scale = 3,
                                    gain = 0.7,
                                    data_weight = 10,
                                    bg_weight = 12,
                                    scaleBound = [2 , 5],
                                    gainBound = [.5, 1.0],
                                    stepSize = 1e-2,
                                    # binRange = list(range(3850, 7150, 200)),
                                    binRange = list(range(3000, 7000, 100)),
                                    plot = plot)
Cs137_662_results['smear'] = res


###############################
# PMT ALIGN Na22 1275 keV
########## #####################
res = 0.09
Na22_1275_sim_smear = np.zeros(len(Na22_1275_sim))
for i in range(len(Na22_1275_sim)):
    Na22_1275_sim_smear[i] = prosim.gaussSmear(Na22_1275_sim[i], res)

# numBins = 300
# binRange = [0,24000]

# w = np.empty(len(bg_data))
# w.fill(1/12)
# countsBG, binsBG = np.histogram(bg_data.qdc_lg_ch1*att_factor_A, bins=numBins, range=binRange, weights=w)

# w = np.empty(len(Na22_data))
# w.fill(1/2)
# counts, bins = np.histogram(Na22_data.qdc_lg_ch1*att_factor_A, bins=numBins, range=binRange, weights=w)
# bins = prodata.getBinCenters(bins)

# countsSim, binsSim = np.histogram(Na22_1275_sim_smear*0.65, bins=numBins, range=binRange)
# binsSim = prodata.getBinCenters(binsSim)

# plt.plot(binsSim, countsSim*3.82, label='sim')
# plt.plot(bins, counts-countsBG, label='data-BG')
# plt.legend()
# plt.show()


Na22_1275_results = prosim.PMTGainAlign( model = Na22_1275_sim_smear, 
                                    data = Na22_data.qdc_lg_ch1*att_factor_A, 
                                    bg = bg_data.qdc_lg_ch1*att_factor_A,
                                    scale = 3.3,
                                    gain = 0.6,
                                    data_weight = 2,
                                    bg_weight = 12,
                                    scaleBound = [1 , 6],
                                    gainBound = [.3, 1.0],
                                    stepSize = 1e-2,
                                    # binRange = list(range(8190, 13750, 130)),
                                    binRange = list(range(8000, 12500, 130)),
                                    plot = plot)
Na22_1275_results['smear'] = res


###############################
# PMT ALIGN Th232 2615 keV
###############################
res = 0.065
Th232_sim_smear = np.zeros(len(Th232_sim))
for i in range(len(Th232_sim)):
    Th232_sim_smear[i] = prosim.gaussSmear(Th232_sim[i], res)

# numBins = 125
# binRange = [0, 35000]

# w = np.empty(len(bg_data))
# w.fill(1/12)
# countsBG, binsBG = np.histogram(bg_data.qdc_lg_ch1*att_factor_A, bins=numBins, range=binRange, weights=w)

# w = np.empty(len(Th232_data))
# w.fill(1/5)
# counts, bins = np.histogram(Th232_data.qdc_lg_ch1*att_factor_A, bins=numBins, range=binRange, weights=w)
# bins = prodata.getBinCenters(bins)

# countsSim, binsSim = np.histogram(Th232_sim_smear*0.659, bins=numBins, range=binRange)
# binsSim = prodata.getBinCenters(binsSim)

# plt.plot(binsSim, countsSim*0.251, label='sim')
# plt.plot(bins, counts-countsBG, label='data-BG')
# plt.legend()
# plt.ylim([0, 2000])
# plt.show()

Th232_2620_results = prosim.PMTGainAlign( model = Th232_sim_smear, 
                                    data = Th232_data.qdc_lg_ch1*att_factor_A, 
                                    bg = bg_data.qdc_lg_ch1*att_factor_A,
                                    scale = 0.25,
                                    gain = 0.65,
                                    data_weight = 5,
                                    bg_weight = 12,
                                    scaleBound = [0.1, 0.5],
                                    gainBound = [0.5, 0.8],
                                    stepSize = 1e-2,
                                    # binRange = list(range(20700, 33000, 500)),
                                    binRange = list(range(15000, 30000, 300)),
                                    plot = plot)
Th232_2620_results['smear'] = res

###############################
# PMT ALIGN AmBe 4439 keV
###############################
res = 0.12
AmBe_sim_smear = np.zeros(len(AmBe_sim))
for i in range(len(AmBe_sim)):
    AmBe_sim_smear[i] = prosim.gaussSmear(AmBe_sim[i], res)

# numBins = 100
# binRange = [0, 100000]

# w = np.empty(len(bg_data))
# w.fill(1/12)
# countsBG, binsBG = np.histogram(bg_data.qdc_lg_ch1*att_factor_A, bins=numBins, range=binRange, weights=w)

# w = np.empty(len(AmBe_data))
# w.fill(1/10)
# counts, bins = np.histogram(AmBe_data.qdc_lg_ch1*att_factor_A, bins=numBins, range=binRange, weights=w)
# bins = prodata.getBinCenters(bins)

# countsSim, binsSim = np.histogram(AmBe_sim_smear*0.664, bins=numBins, range=binRange)
# binsSim = prodata.getBinCenters(binsSim)

# plt.plot(binsSim, countsSim*0.747, label='sim')
# plt.plot(bins, counts-countsBG, label='data-BG')
# plt.legend()
# plt.ylim([0,6000])
# plt.show()

AmBe_4439_results = prosim.PMTGainAlign( model = AmBe_sim_smear, 
                                    data = AmBe_data.qdc_lg_ch1*att_factor_A, 
                                    bg = bg_data.qdc_lg_ch1*att_factor_A,
                                    scale = 0.75,
                                    gain = 0.68,
                                    data_weight = 10,
                                    bg_weight = 12,
                                    scaleBound = [.5 , 1.1],
                                    gainBound = [.5, 1.0],
                                    stepSize = 1e-2,
                                    # binRange = list(range(33300, 58300, 1000)),
                                    binRange = list(range(31000, 53000, 500)),
                                    plot = plot)
AmBe_4439_results['smear'] = res



#Copmpile results into one DataFrame
results = pd.DataFrame(pd.Series(  Na22_511_results, 
                                index = Na22_511_results.keys()), 
                                columns=['Na22_511'])
results['Cs137_662'] = pd.Series(  Cs137_662_results, 
                                index = Cs137_662_results.keys())
results['Na22_1275'] = pd.Series(Na22_1275_results, 
                                 index = Na22_1275_results.keys())
results['Th232_2620'] = pd.Series(Th232_2620_results,
                                 index = Th232_2620_results.keys())
results['AmBe_4439'] = pd.Series(AmBe_4439_results, 
                                 index = AmBe_4439_results.keys())
#Save DataFrame to disk
results.to_parquet('data/simulation_align.pkl')

#Code for loading from disk
# my_df = pd.read_parquet('data/simulation_align.pkl')