#!/usr/bin/env python3
from re import I
import sys
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
from numpy.core.arrayprint import SubArrayFormat
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit

# from matplotlib.colors import LinearSegmentedColormap


# IMPORT FROM LIBRARY DIRECTORY
sys.path.insert(0, "../library/")
sys.path.insert(0, "../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "../")
sys.path.insert(0, "data_analysis/")

import processing_math as promath
import processing_data as prodata
import processing_pd as propd
import figure_style as fs

########################################
######## Setting parameters ############
########################################
distance = 0.959
att_factor_11dB = 3.55
att_factor_12dB = 3.98
att_factor_19dB = 8.91

detector = "EJ305"

distance = 0.959

gTOFRange = [0.5e-9, 6e-9] #Define range of gamma-flash
randTOFRange = [-710e-9,-10e-9] #Define range of random events
gLength = np.abs(gTOFRange[1]-gTOFRange[0]) #Calculate the lenght of cut for gammas
randLength = np.abs(randTOFRange[1]-randTOFRange[0]) #Calculate lenght of cut for random events

minE = 1.625
maxE = 6.625
dE = 0.25
df_nLength = pd.DataFrame()
for i in np.arange(minE, maxE, dE):
    E1 = round(i-dE,3)
    E2 = round(i,3)
    t1 = prodata.EnergytoTOF(E1, distance)
    t2 = prodata.EnergytoTOF(E2, distance)
    print(f'dE = {round(E1, 3)}-{round(E2, 3)} MeV -> center = {E1-dE/2} -> dt = {np.round((t1-t2)*1e9, 4)} ns')
    df_nLength[f'keV{int((i-dE/2)*1000)}'] = [np.abs(t1-t2)]

y2ThrQDC = 6450 #QDC threshold in channels (NOT IN USE)
y3ThrQDC = 6000 #QDC threshold in channels
y4ThrQDC = 6000 #QDC threshold in channels
y5ThrQDC = 7700 #QDC threshold in channels
nThrQDC = 0.5 #threshold in units of MeVee

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# LOADING DATA & SLICING LOOP
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop
numBins = np.arange(0, 1, 0.0075)

#predefine holder for final data sets
y_neutron_ps_Fin = pd.DataFrame() #for neutrons PS
for col in df_nLength.columns:
    y_neutron_ps_Fin[col] = np.zeros(len(numBins)-1)
y_gamma_ps_Fin = np.zeros(len(numBins)-1) #for gammas PS

run1 = 3038
numRuns = 201
runList = np.arange(run1, run1+numRuns)

gainOffsets = np.array([1.    , 1.0203, 1.0296, 1.0318, 1.0304, 1.0199, 1.0197, 1.0237,
                        1.0197, 1.0318, 1.0207, 1.0203, 1.0142, 1.0124, 1.0169, 1.0049,
                        0.9928, 1.0132, 1.011 , 1.0059, 0.9999, 1.0098, 1.0144, 1.0116,
                        1.0172, 1.0009, 1.0057, 1.0007, 1.0043, 1.0051, 1.01  , 0.9999,
                        1.0009, 0.9999, 1.011 , 0.991 , 0.9999, 1.0033, 1.0013, 0.994 ,
                        1.0007, 1.0197, 1.0023, 1.0098, 0.9926, 1.0023, 1.0146, 1.0098,
                        1.0114, 1.0136, 1.0001, 0.99  , 0.9873, 0.9922, 1.0138, 0.9934,
                        0.9809, 1.0011, 0.9801, 0.9803, 1.0041, 0.9823, 1.0098, 0.9803,
                        0.9922, 0.9857, 0.9744, 0.9807, 0.9999, 0.9906, 1.0017, 0.9906,
                        0.9845, 0.9999, 0.9811, 0.9908, 0.99  , 1.0013, 0.9724, 0.9831,
                        0.9999, 0.9801, 0.99  , 0.9736, 0.9936, 0.9801, 0.9938, 1.0063,
                        0.9988, 0.9999, 0.9918, 0.9908, 0.9954, 0.9774, 0.9853, 0.9902,
                        0.9938, 0.9999, 0.9942, 1.0029, 0.9817, 0.9843, 0.9702, 0.9801,
                        0.9839, 0.9908, 0.993 , 0.9928, 0.9831, 1.0006, 0.99  , 0.9904,
                        0.9924, 0.9831, 0.9904, 0.9825, 0.9851, 0.9855, 0.9922, 0.9738,
                        0.9918, 0.9902, 0.9936, 0.9902, 0.99  , 0.99  , 0.9849, 1.0013,
                        0.9803, 0.9706, 0.9819, 0.9827, 0.975 , 0.9912, 1.0033, 0.9732,
                        0.9999, 0.993 , 0.9861, 0.9817, 0.9942, 0.9815, 0.9805, 0.99  ,
                        0.9908, 0.9809, 1.0001, 0.9805, 0.9809, 0.9801, 0.99  , 0.971 ,
                        0.9999, 1.0017, 0.9908, 0.99  , 0.9922, 0.9722, 0.9837, 0.9999,
                        0.9811, 0.9918, 0.9956, 0.9999, 0.9999, 0.9944, 1.0001, 1.0003,
                        0.9732, 0.9814, 0.9999, 0.9912, 1.0023, 0.9999, 0.9958, 0.9837,
                        0.9819, 0.9631, 0.9738, 0.9801, 0.9801, 0.99  , 0.9958, 0.9734,
                        0.9734, 0.9904, 0.9833, 0.9617, 1.0007, 0.9821, 0.9801, 0.9732,
                        0.9722, 0.99  , 0.9944, 0.9718, 0.9764, 0.9861, 0.9849, 0.9809,
                        0.9948])
                        
dataSize = 5

for i in np.arange(0, numRuns, dataSize):
    print('-------------------------------------------------')
    currentRunList = np.split(runList, [i, i+dataSize])[1] #split runlist into section of 20 events
    print(f'---> Now reading in runs {currentRunList[0]}-{currentRunList[-1]} of {runList[-1]}')
    PuBe_tof_data =   propd.load_parquet_merge_gainadjust(path, currentRunList, keep_col=[ 'qdc_lg_ch1',
                                                                                            'qdc_lg_ch2',
                                                                                            'qdc_lg_ch3',
                                                                                            'qdc_lg_ch4',
                                                                                            'qdc_lg_ch5',
                                                                                            'qdc_ps_ch1', 
                                                                                            'tof_ch2', 
                                                                                            'tof_ch3', 
                                                                                            'tof_ch4', 
                                                                                            'tof_ch5'], 
                                                                                            gainOffsets = gainOffsets[i: i + dataSize],
                                                                                            full=False)

    #~~~~~~~~~~~~~~~~~~~~~~~~
    #      RESET INDEX
    #~~~~~~~~~~~~~~~~~~~~~~~~
    PuBe_tof_data = PuBe_tof_data.reset_index(drop=True)

    #~~~~~~~~~~~~~~~~~~~~~~~~
    #    CALIBRATING ToF
    #~~~~~~~~~~~~~~~~~~~~~~~~
    flashrange = {'tof_ch3':[55, 72], 'tof_ch4':[55, 72], 'tof_ch5':[55, 72]} #gamma flash fitting ranges
    prodata.tof_calibration(PuBe_tof_data, flashrange, distance, phcut=0, qdccut=2000, calUnit=1e-9, numBins=1000, energyCal=True)
    PuBe_tof_data['tofE_ch2'] = np.zeros(len(PuBe_tof_data)) #adding due to empty ch2 

    #~~~~~~~~~~~~~~~~~~~~~~~~
    #    CALIBRATING QDC
    #~~~~~~~~~~~~~~~~~~~~~~~~
    PuBe_tof_data.qdc_lg_ch1 = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/{detector}/Ecal', PuBe_tof_data.qdc_lg_ch1*att_factor_19dB)
    
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    #   Get correlated gamma PS
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    #get random data
    y2_random_ps = PuBe_tof_data.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch2>{y2ThrQDC} and tof_ch2>{randTOFRange[0]} and tof_ch2<={randTOFRange[1]}').qdc_ps_ch1
    y3_random_ps = PuBe_tof_data.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch3>{y3ThrQDC} and tof_ch3>{randTOFRange[0]} and tof_ch3<={randTOFRange[1]}').qdc_ps_ch1
    y4_random_ps = PuBe_tof_data.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch4>{y4ThrQDC} and tof_ch4>{randTOFRange[0]} and tof_ch4<={randTOFRange[1]}').qdc_ps_ch1
    y5_random_ps = PuBe_tof_data.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch5>{y5ThrQDC} and tof_ch5>{randTOFRange[0]} and tof_ch5<={randTOFRange[1]}').qdc_ps_ch1
    #check for random multiplicity
    mpIdxRandom = prodata.uniqueEventHelper(y2_random_ps, y3_random_ps, y4_random_ps, y5_random_ps)
    
    #get gamma data
    y2_gamma_ps = PuBe_tof_data.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch2>{y2ThrQDC} and tof_ch2>{gTOFRange[0]} and tof_ch2<={gTOFRange[1]}').qdc_ps_ch1
    y3_gamma_ps = PuBe_tof_data.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch3>{y3ThrQDC} and tof_ch3>{gTOFRange[0]} and tof_ch3<={gTOFRange[1]}').qdc_ps_ch1
    y4_gamma_ps = PuBe_tof_data.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch4>{y4ThrQDC} and tof_ch4>{gTOFRange[0]} and tof_ch4<={gTOFRange[1]}').qdc_ps_ch1
    y5_gamma_ps = PuBe_tof_data.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch5>{y5ThrQDC} and tof_ch5>{gTOFRange[0]} and tof_ch5<={gTOFRange[1]}').qdc_ps_ch1
    #check for random multiplicity
    mpIdxGamma = prodata.uniqueEventHelper(y2_gamma_ps, y3_gamma_ps, y4_gamma_ps, y5_gamma_ps)
    
    x_gamma_ps, y_gamma_ps = prodata.sliceRandomSubtraction(y2_gamma_ps.drop(mpIdxGamma, errors='ignore'),
                                                            y3_gamma_ps.drop(mpIdxGamma, errors='ignore'),
                                                            y4_gamma_ps.drop(mpIdxGamma, errors='ignore'),
                                                            y5_gamma_ps.drop(mpIdxGamma, errors='ignore'),
                                                            y2_random_ps.drop(mpIdxRandom, errors='ignore'), 
                                                            y3_random_ps.drop(mpIdxRandom, errors='ignore'),
                                                            y4_random_ps.drop(mpIdxRandom, errors='ignore'),
                                                            y5_random_ps.drop(mpIdxRandom, errors='ignore'),
                                                            numBins = numBins, 
                                                            dataLength = gLength, 
                                                            randLength = randLength)
    #merge statisics from all events
    y_gamma_ps_Fin += y_gamma_ps

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    #   SLICE Neutron ToF ENERGY for PS
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    y2_neutron_ps_TMP, y3_neutron_ps_TMP, y4_neutron_ps_TMP, y5_neutron_ps_TMP =    prodata.tofEnergySlicer(PuBe_tof_data, 
                                                                                                            minE = minE, 
                                                                                                            maxE = maxE, 
                                                                                                            dE = dE, 
                                                                                                            colKeep = 'qdc_ps_ch1', 
                                                                                                            n_thr = nThrQDC, 
                                                                                                            y_thr = [y2ThrQDC, y3ThrQDC, y4ThrQDC, y5ThrQDC]
                                                                                                            )

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    #   Get correlated neutron PS
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    for col in df_nLength.columns:
        x_neutron_ps, y_neutron_ps = prodata.sliceRandomSubtraction(y2_neutron_ps_TMP[f'{col}'],
                                                                    y3_neutron_ps_TMP[f'{col}'],
                                                                    y4_neutron_ps_TMP[f'{col}'],
                                                                    y5_neutron_ps_TMP[f'{col}'],
                                                                    y2_random_ps.drop(mpIdxRandom, errors='ignore'),
                                                                    y3_random_ps.drop(mpIdxRandom, errors='ignore'),
                                                                    y4_random_ps.drop(mpIdxRandom, errors='ignore'),
                                                                    y5_random_ps.drop(mpIdxRandom, errors='ignore'),
                                                                    numBins = numBins, 
                                                                    dataLength = df_nLength[f'{col}'].item(), 
                                                                    randLength = randLength)
        #merge statisics from all events
        y_neutron_ps_Fin[f'{col}'] += y_neutron_ps
    
for col in np.arange(1500, 6500, 250):
    print(f'Saving to... MeV{col}')
    np.save(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/{detector}/sliced/PS/MeV{col}/yGamma', y_gamma_ps_Fin)
    np.save(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/{detector}/sliced/PS/MeV{col}/yNeutron', y_neutron_ps_Fin[f'keV{col}'])
    np.save(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/{detector}/sliced/PS/MeV{col}/x', x_neutron_ps)
