#!/usr/bin/env python3
import sys
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit

# IMPORT FROM LIBRARY DIRECTORY
sys.path.insert(0, "../library/")
sys.path.insert(0, "../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "sim_analysis/")
sys.path.insert(0, "data_analysis/")

import digidata
import nicholai_plot_helper as nplt
import processing_utility as prout
import processing_math as promath
import processing_data as prodata
import processing_pd as propd
import processing_simulation as prosim


"""
Script for running both data and simulation and calibrating data of NE213A detector.
"""
#TODO fit gaussian to pencilbeam data on the right side of the distyribution.


PMT_gain = 4e6 #Nominal gain is 7'000'000 at nominal A/lm (model: 9821B p1)
att_factor_12dB = 3.98
att_factor_16dB = 6.31
q = 1.60217662E-19 #charge of a single electron
plot = False


##############################
# LOADING DATA
##############################
path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop
bg_runs =       list(range(1676,1686))
Cs137_runs =    list(range(1656,1666))
Th232_runs =    list(range(1671,1676))
AmBe_runs =     list(range(1693,1703))
PuBe_runs =     list(range(2135,2145))
CableLengthCorrections = 1.05 #correction constant between inside cave and outside due to different cable lengthsc

df_bg =         propd.load_parquet_merge(path, bg_runs,     keep_col=['qdc_lg_ch1'], full=False)*CableLengthCorrections
df_Cs137 =      propd.load_parquet_merge(path, Cs137_runs,  keep_col=['qdc_lg_ch1'], full=False)*CableLengthCorrections
df_Th232 =      propd.load_parquet_merge(path, Th232_runs,  keep_col=['qdc_lg_ch1'], full=False)*CableLengthCorrections
df_AmBe =       propd.load_parquet_merge(path, AmBe_runs,   keep_col=['qdc_lg_ch1'], full=False)*CableLengthCorrections
df_PuBe =       propd.load_parquet_merge(path, PuBe_runs,   keep_col=['qdc_lg_ch1'], full=False)

###############################
# LOADING SIMULATION DATA
###############################
path_sim = '/media/gheed/Seagate_Expansion_Drive1/data/simulation/'
Cs137_sim =         np.genfromtxt(path_sim + 'NE213A/Cs137/isotropic/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
Th232_sim =         np.genfromtxt(path_sim + 'NE213A/2_62MeV/isotropic/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
AmBe_sim =          np.genfromtxt(path_sim + 'NE213A/4_44MeV/isotropic/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]

#append extra data
Cs137_sim =         np.append(Cs137_sim, np.genfromtxt(path_sim + 'NE213A/Cs137/isotropic/part2/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])
Cs137_sim =         np.append(Cs137_sim, np.genfromtxt(path_sim + 'NE213A/Cs137/isotropic/part3/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])

Th232_sim =         np.append(Th232_sim, np.genfromtxt(path_sim + 'NE213A/2_62MeV/isotropic/part2/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])
Th232_sim =         np.append(Th232_sim, np.genfromtxt(path_sim + 'NE213A/2_62MeV/isotropic/part3/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])

AmBe_sim =          np.append(AmBe_sim, np.genfromtxt(path_sim + 'NE213A/4_44MeV/isotropic/part2/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])
AmBe_sim =          np.append(AmBe_sim, np.genfromtxt(path_sim + 'NE213A/4_44MeV/isotropic/part3/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])

#############################
# CONVERT INTO FLOAT 
#############################
Cs137_sim =         pd.to_numeric(Cs137_sim, downcast='float')
Th232_sim =         pd.to_numeric(Th232_sim, downcast='float')
AmBe_sim =          pd.to_numeric(AmBe_sim, downcast='float')

###############################
# CALIBRATING SIMULATION DATA
###############################
Cs137_sim =         prosim.chargeCalibration(Cs137_sim      * q * PMT_gain)
Th232_sim =         prosim.chargeCalibration(Th232_sim      * q * PMT_gain)
AmBe_sim =          prosim.chargeCalibration(AmBe_sim       * q * PMT_gain)



plot = True 
###############################
# PMT ALIGN Cs-137
###############################
res = 0.11
numBins = np.arange(0, 24000, 50)
Cs137_sim_smear = np.zeros(len(Cs137_sim))
for i in range(len(Cs137_sim)):
    Cs137_sim_smear[i] = prosim.gaussSmear(Cs137_sim[i], res)


# countsSim, binsSim = np.histogram(Cs137_sim_smear*.80, bins=numBins)
# binsSim = prodata.getBinCenters(binsSim)

# w = np.empty(len(df_bg))
# w.fill(1/len(bg_runs))
# countsBG, binsBG = np.histogram(df_bg.qdc_lg_ch1*att_factor_16dB, bins=numBins, weights=w)

# w = np.empty(len(df_Cs137))
# w.fill(1/len(Cs137_runs))
# counts, bins = np.histogram(df_Cs137.qdc_lg_ch1*att_factor_16dB, bins=numBins, weights=w)
# bins = prodata.getBinCenters(bins)

# plt.plot(binsSim, countsSim*3, label='sim')
# plt.plot(bins, counts-countsBG, label='data-BG')
# plt.legend()
# plt.show()

Cs137_results = prosim.PMTGainAlign( model = Cs137_sim_smear, 
                                    data = df_Cs137.qdc_lg_ch1*att_factor_16dB, 
                                    bg = df_bg.qdc_lg_ch1*att_factor_16dB,
                                    scale = 3.2,
                                    gain = 0.8,
                                    data_weight = len(Cs137_runs),
                                    bg_weight = len(bg_runs),
                                    scaleBound = [2 , 5],
                                    gainBound = [.7, 1.1],
                                    stepSize = 1e-2,
                                    binRange = list(range(3200, 8000, 100)),
                                    plot = plot)
Cs137_results['smear'] = res



###############################
# PMT ALIGN Th232 2615 keV
###############################
res = 0.065
numBins = np.arange(15000, 45000, 150)
Th232_sim_smear = np.zeros(len(Th232_sim))
for i in range(len(Th232_sim)):
    Th232_sim_smear[i] = prosim.gaussSmear(Th232_sim[i], res)

# countsSim, binsSim = np.histogram(Th232_sim_smear*0.7, bins=numBins)
# binsSim = prodata.getBinCenters(binsSim)

# w = np.empty(len(df_bg))
# w.fill(1/len(bg_runs))
# countsBG, binsBG = np.histogram(df_bg.qdc_lg_ch1*att_factor_16dB, bins=numBins, weights=w)

# w = np.empty(len(df_Th232))
# w.fill(1/len(Th232_runs))
# counts, bins = np.histogram(df_Th232.qdc_lg_ch1*att_factor_16dB, bins=numBins, weights=w)
# bins = prodata.getBinCenters(bins)

# plt.plot(binsSim, countsSim*0.25, label='sim')
# plt.plot(bins, counts-countsBG, label='data-BG')
# plt.legend()
# plt.show()

Th232_results = prosim.PMTGainAlign(model = Th232_sim_smear, 
                                    data = df_Th232.qdc_lg_ch1*att_factor_16dB, 
                                    bg = df_bg.qdc_lg_ch1*att_factor_16dB,
                                    scale = 0.25,
                                    gain = 0.7,
                                    data_weight = len(Th232_runs),
                                    bg_weight = len(bg_runs),
                                    scaleBound = [0.1, 0.4],
                                    gainBound = [0.55, 0.85],
                                    stepSize = 1e-2,
                                    binRange = list(range(20000, 30000, 200)),
                                    plot = plot)
Th232_results['smear'] = res

###############################
# PMT ALIGN AmBe 4439 keV
###############################
res = 0.12
numBins = np.arange(20000, 70000, 300)
AmBe_sim_smear = np.zeros(len(AmBe_sim))
for i in range(len(AmBe_sim)):
    AmBe_sim_smear[i] = prosim.gaussSmear(AmBe_sim[i], res)

# countsSim, binsSim = np.histogram(AmBe_sim_smear*0.75, bins=numBins)
# binsSim = prodata.getBinCenters(binsSim)

# w = np.empty(len(df_bg))
# w.fill(1/len(bg_runs))
# countsBG, binsBG = np.histogram(df_bg.qdc_lg_ch1*att_factor_16dB, bins=numBins, weights=w)

# w = np.empty(len(df_AmBe))
# w.fill(1/len(AmBe_runs))
# counts, bins = np.histogram(df_AmBe.qdc_lg_ch1*att_factor_16dB, bins=numBins, weights=w)
# bins = prodata.getBinCenters(bins)

# plt.plot(binsSim, countsSim*.65, label='sim')
# plt.plot(bins, counts-countsBG, label='data-BG')
# plt.legend()
# plt.show()

AmBe_results = prosim.PMTGainAlign( model = AmBe_sim_smear, 
                                    data = df_AmBe.qdc_lg_ch1*att_factor_16dB, 
                                    bg = df_bg.qdc_lg_ch1*att_factor_16dB,
                                    scale = 0.65,
                                    gain = 0.7,
                                    data_weight = len(AmBe_runs),
                                    bg_weight = len(bg_runs),
                                    scaleBound = [0.5 , 0.8],
                                    gainBound = [0.55, 0.85],
                                    stepSize = 1e-2,
                                    binRange = list(range(35000, 52000, 300)),
                                    plot = plot)
AmBe_results['smear'] = res


############################################
# Deriving QDC locations for each energy
############################################
path_sim = '/media/gheed/Seagate_Expansion_Drive1/data/simulation/'
names = ['evtNum', 'optPhotonSum', 'optPhotonSumQE', 'optPhotonSumCompton', 'optPhotonSumComptonQE', 'xLoc', 'yLoc', 'zLoc', 'CsMin', 'optPhotonParentID', 'optPhotonParentCreatorProcess', 'optPhotonParentStartingEnergy','edep']
#pencilbeam
Cs137_sim_pen = pd.read_csv(path_sim + "NE213A/Cs137/pencilbeam/CSV_optphoton_data_sum.csv", names=names)
Cs137_sim_pen = pd.concat([Cs137_sim_pen, pd.read_csv(path_sim + "NE213A/Cs137/pencilbeam/part2/CSV_optphoton_data_sum.csv", names=names)])
Cs137_sim_pen = pd.concat([Cs137_sim_pen, pd.read_csv(path_sim + "NE213A/Cs137/pencilbeam/part3/CSV_optphoton_data_sum.csv", names=names)])
Cs137_sim_pen = pd.concat([Cs137_sim_pen, pd.read_csv(path_sim + "NE213A/Cs137/pencilbeam/part4/CSV_optphoton_data_sum.csv", names=names)])
Th232_sim_pen = pd.read_csv(path_sim + "NE213A/2_62MeV/pencilbeam/CSV_optphoton_data_sum.csv", names=names)
Th232_sim_pen = pd.concat([Th232_sim_pen, pd.read_csv(path_sim + 'NE213A/2_62MeV/pencilbeam/part2/CSV_optphoton_data_sum.csv', names=names)])
Th232_sim_pen = pd.concat([Th232_sim_pen, pd.read_csv(path_sim + 'NE213A/2_62MeV/pencilbeam/part3/CSV_optphoton_data_sum.csv', names=names)])
Th232_sim_pen = pd.concat([Th232_sim_pen, pd.read_csv(path_sim + 'NE213A/2_62MeV/pencilbeam/part4/CSV_optphoton_data_sum.csv', names=names)])
Th232_sim_pen = pd.concat([Th232_sim_pen, pd.read_csv(path_sim + 'NE213A/2_62MeV/pencilbeam/part5/CSV_optphoton_data_sum.csv', names=names)])
Th232_sim_pen = pd.concat([Th232_sim_pen, pd.read_csv(path_sim + 'NE213A/2_62MeV/pencilbeam/part6/CSV_optphoton_data_sum.csv', names=names)])
AmBe_sim_pen = pd.read_csv(path_sim + "NE213A/4_44MeV/pencilbeam/CSV_optphoton_data_sum.csv", names=names)
AmBe_sim_pen = pd.concat([AmBe_sim_pen, pd.read_csv(path_sim + 'NE213A/4_44MeV/pencilbeam/part2/CSV_optphoton_data_sum.csv', names=names)])
AmBe_sim_pen = pd.concat([AmBe_sim_pen, pd.read_csv(path_sim + 'NE213A/4_44MeV/pencilbeam/part3/CSV_optphoton_data_sum.csv', names=names)])
AmBe_sim_pen = pd.concat([AmBe_sim_pen, pd.read_csv(path_sim + 'NE213A/4_44MeV/pencilbeam/part4/CSV_optphoton_data_sum.csv', names=names)])
AmBe_sim_pen = pd.concat([AmBe_sim_pen, pd.read_csv(path_sim + 'NE213A/4_44MeV/pencilbeam/part5/CSV_optphoton_data_sum.csv', names=names)])
AmBe_sim_pen = pd.concat([AmBe_sim_pen, pd.read_csv(path_sim + 'NE213A/4_44MeV/pencilbeam/part6/CSV_optphoton_data_sum.csv', names=names)])
#randomize binning pencilbeam
y, x = np.histogram(Cs137_sim_pen.optPhotonSumComptonQE, bins=4096, range=[0, 4096])
Cs137_sim_pen.optPhotonSumComptonQE = prodata.getRandDist(x, y)
y, x = np.histogram(Th232_sim_pen.optPhotonSumComptonQE, bins=4096, range=[0, 4096])
Th232_sim_pen.optPhotonSumComptonQE = prodata.getRandDist(x, y)
y, x = np.histogram(AmBe_sim_pen.optPhotonSumComptonQE, bins=4096, range=[0, 4096])
AmBe_sim_pen.optPhotonSumComptonQE = prodata.getRandDist(x, y)

#reset index pencilbeam 
Cs137_sim_pen =  Cs137_sim_pen.reset_index()
Th232_sim_pen =  Th232_sim_pen.reset_index()
AmBe_sim_pen =   AmBe_sim_pen.reset_index()

#QDC calibrate simulations pencilbeam
Cs137_sim_pen.optPhotonSumComptonQE =      prosim.chargeCalibration(Cs137_sim_pen.optPhotonSumComptonQE * q * PMT_gain * Cs137_results['gain']) 
Th232_sim_pen.optPhotonSumComptonQE =      prosim.chargeCalibration(Th232_sim_pen.optPhotonSumComptonQE * q * PMT_gain * Th232_results['gain']) 
AmBe_sim_pen.optPhotonSumComptonQE =      prosim.chargeCalibration(AmBe_sim_pen.optPhotonSumComptonQE * q * PMT_gain * AmBe_results['gain']) 

#Apply smearing on pencilbeam data
Cs137_sim_pen.optPhotonSumComptonQE = Cs137_sim_pen.optPhotonSumComptonQE.apply(lambda x: prosim.gaussSmear(x, Cs137_results['smear'])) 
Th232_sim_pen.optPhotonSumComptonQE = Th232_sim_pen.optPhotonSumComptonQE.apply(lambda x: prosim.gaussSmear(x, Th232_results['smear'])) 
AmBe_sim_pen.optPhotonSumComptonQE = AmBe_sim_pen.optPhotonSumComptonQE.apply(lambda x: prosim.gaussSmear(x, AmBe_results['smear'])) 

#############################
## FITTING PENCILBEAM DATA ##
#############################

start = 3400
stop = 6500
counts, bin_edges = np.histogram(Cs137_sim_pen.optPhotonSumComptonQE, bins = np.arange(start*0.8, stop*1.2, 200))
bin_centers = prodata.getBinCenters(bin_edges)
Ef = 0.66166
Cs137_sim_fit  =  prosim.calibrationFitBinned(counts, bin_centers, start, stop, Ef=Ef, plot=plot, compton=True)
Cs137_sim_fit['CE'] = promath.comptonMax(Ef)
mean = Cs137_sim_fit['gauss_mean']
std = Cs137_sim_fit['gauss_std']
Cs137_sim_results = {}
Cs137_sim_results['mean'] = np.mean(Cs137_sim_pen.query(f'optPhotonSumComptonQE>{mean-3*std} and optPhotonSumComptonQE<{mean+3*std}').optPhotonSumComptonQE)
Cs137_sim_results['mean_err']  = np.std(Cs137_sim_pen.query(f'optPhotonSumComptonQE>{mean-3*std} and optPhotonSumComptonQE<{mean+3*std}').optPhotonSumComptonQE)/np.sqrt(len(Cs137_sim_pen.query(f'optPhotonSumComptonQE>{mean-3*std} and optPhotonSumComptonQE<{mean+3*std}').optPhotonSumComptonQE))

start = 20000
stop = 26000
counts, bin_edges = np.histogram(Th232_sim_pen.optPhotonSumComptonQE, bins = np.arange(start*0.8, stop*1.2, 300))
bin_centers = prodata.getBinCenters(bin_edges)
Ef = 2.614533
Th232_sim_fit = prosim.calibrationFitBinned(counts, bin_centers, start, stop, Ef=Ef, plot=plot, compton=True)
Th232_sim_fit['CE'] = promath.comptonMax(Ef)
mean = Th232_sim_fit['gauss_mean']
std = Th232_sim_fit['gauss_std']
Th232_sim_results = {}
Th232_sim_results['mean'] = np.mean(Th232_sim_pen.query(f'optPhotonSumComptonQE>{mean-3*std} and optPhotonSumComptonQE<{mean+3*std}').optPhotonSumComptonQE)
Th232_sim_results['mean_err']  = np.std(Th232_sim_pen.query(f'optPhotonSumComptonQE>{mean-3*std} and optPhotonSumComptonQE<{mean+3*std}').optPhotonSumComptonQE)/np.sqrt(len(Th232_sim_pen.query(f'optPhotonSumComptonQE>{mean-3*std} and optPhotonSumComptonQE<{mean+3*std}').optPhotonSumComptonQE))

start = 29000
stop = 55000
counts, bin_edges = np.histogram(AmBe_sim_pen.optPhotonSumComptonQE, bins = np.arange(start*0.8, stop*1.2, 800))
bin_centers = prodata.getBinCenters(bin_edges)
Ef = 4.439
AmBe_sim_fit = prosim.calibrationFitBinned(counts, bin_centers, start, stop, Ef=Ef, plot=plot, compton=True)
AmBe_sim_fit['CE'] = promath.comptonMax(Ef)
mean = AmBe_sim_fit['gauss_mean']
std = AmBe_sim_fit['gauss_std']
AmBe_sim_results = {}
AmBe_sim_results['mean'] = np.mean(AmBe_sim_pen.query(f'optPhotonSumComptonQE>{mean-3*std} and optPhotonSumComptonQE<{mean+3*std}').optPhotonSumComptonQE)
AmBe_sim_results['mean_err']  = np.std(AmBe_sim_pen.query(f'optPhotonSumComptonQE>{mean-3*std} and optPhotonSumComptonQE<{mean+3*std}').optPhotonSumComptonQE)/np.sqrt(len(AmBe_sim_pen.query(f'optPhotonSumComptonQE>{mean-3*std} and optPhotonSumComptonQE<{mean+3*std}').optPhotonSumComptonQE))











##################################################
#Calculate calibration constant
CE_energy = np.array([Cs137_sim_fit['CE'], Th232_sim_fit['CE'], AmBe_sim_fit['CE']]) #energies in MeVee of Compton edges
QDC_loc = np.array([Cs137_sim_results['mean'], Th232_sim_results['mean'], AmBe_sim_results['mean']]) #relevant QDC channels
QDC_loc_err = np.array([Cs137_sim_results['mean_err'], Th232_sim_results['mean_err'], AmBe_sim_results['mean_err']]) #relevant QDC channels errors from gain-align above.

popt, pcov = curve_fit(promath.linearFunc, CE_energy, QDC_loc, sigma=QDC_loc_err, absolute_sigma=True)
pcov = np.diag(np.sqrt(pcov))
E_err = promath.errorPropLinearFuncInv(promath.linearFuncInv(QDC_loc, popt[0], popt[1]), popt[0], pcov[0], popt[1], pcov[1])

plt.scatter(QDC_loc, CE_energy)
plt.errorbar(QDC_loc, CE_energy, yerr = E_err, ls='none')
plt.plot(np.arange(0, 40000, 1000), promath.linearFuncInv(np.arange(0,40000,1000), popt[0], popt[1]))
plt.ylabel('MeV$_{ee}$')
plt.xlabel('QDC [channels]')
plt.show()


np.save('/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/Ecal/popt', popt)
np.save('/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/Ecal/pcov', pcov)







##### REMOVE ME ###############
# numBins = np.arange(0,100000,50)

# w = np.empty(len(df_bg))
# w.fill(1/len(bg_runs))
# countsBG, binsBG = np.histogram(df_bg.qdc_lg_ch1*att_factor_16dB, bins=numBins, weights=w)

# w = np.empty(len(df_AmBe))
# w.fill(1/len(AmBe_runs))
# countsAmBe, bins = np.histogram(df_AmBe.qdc_lg_ch1*att_factor_16dB, bins=numBins, weights=w)
# bins = prodata.getBinCenters(bins)

# w = np.empty(len(df_Cs137))
# w.fill(1/len(Cs137_runs))
# countsCs137, bins = np.histogram(df_Cs137.qdc_lg_ch1*att_factor_16dB, bins=numBins, weights=w)
# bins = prodata.getBinCenters(bins)

# w = np.empty(len(df_Th232))
# w.fill(1/len(Th232_runs))
# countsTh232, bins = np.histogram(df_Th232.qdc_lg_ch1*att_factor_16dB, bins=numBins, weights=w)
# bins = prodata.getBinCenters(bins)



# plt.hist(prodata.calibrateMyDetector('/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/Ecal', df_PuBe.qdc_lg_ch1*att_factor_12dB), prodata.calibrateMyDetector('/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/Ecal', numBins), color='black')
# plt.plot(prodata.calibrateMyDetector('/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/Ecal', bins), countsAmBe-countsBG, label='AmBe')
# plt.plot(prodata.calibrateMyDetector('/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/Ecal', bins), countsCs137-countsBG, label='Cs137')
# plt.plot(prodata.calibrateMyDetector('/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/Ecal', bins), countsTh232-countsBG, label='Th232')
# plt.legend()
# plt.yscale('log')
# plt.show()

