#!/usr/bin/env python3
import random
import numpy as np
import pandas as pd 


"""
CREATE RANDOM RANGE OF SEED NUMBERS FOR SIMULATIONS
Save result as CSV file
"""
###

randNums = random.sample(range(1000000, 10000000, 1), 500) #create 400 random number between 1'000'000 and 9'999'999
pd.DataFrame(randNums).to_csv("data/randomNumberSet.csv", header=None, index=None)
