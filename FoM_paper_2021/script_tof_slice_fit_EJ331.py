#!/usr/bin/env python3
from re import I
import sys
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
from numpy.core.arrayprint import SubArrayFormat
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit

# IMPORT FROM LIBRARY DIRECTORY
sys.path.insert(0, "../library/")
sys.path.insert(0, "../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "../")
sys.path.insert(0, "data_analysis/")

import processing_math as promath
import processing_data as prodata
import processing_pd as propd
import processing_simulation as prosim

"""
Script for calibrating and slicing ToF spectra. Will save to disk in groups of 10 runs
"""

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# LOADING DATA & SLICING LOOP
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop
# PuBe_tof_runs = np.arange(1728,1795)#[1728, 1729, 1730, 1731, 1732, 1733, 1734, 1735, 1736, 1737] #10st
run1 = 1920
num = 0
for i in np.arange(0,40,10):
    print('-------------------------------------------------')
    print(f'Now working on runs: {run1+i}-{run1+i+10}, fileidx = {num}')
    PuBe_tof_data =   propd.load_parquet_merge(path, np.arange(run1+i,run1+i+10), keep_col=[  'qdc_lg_ch1'], 
                                                                            full=False)
    
#~~~~~~~~~~~~~~~~~~~~~~~~
#    CALIBRATING ToF
#~~~~~~~~~~~~~~~~~~~~~~~~
    flashrange = {'tof_ch2':[40,60], 'tof_ch3':[40,60], 'tof_ch4':[40,60], 'tof_ch5':[40,60]} #gamma flash fitting ranges
    distance = 0.959 #distance between source and center of detector
    prodata.tof_calibration(PuBe_tof_data, flashrange, distance, phcut=125, calUnit=1e-9, energyCal=True)

#~~~~~~~~~~~~~~~~~~~~~~~~
#   SLICE ToF ENERGY
#~~~~~~~~~~~~~~~~~~~~~~~~
    df_sliced = prodata.tof_slicer(PuBe_tof_data, 
                                    qdc_col = 'qdc_lg_ch1', 
                                    tof_energy_cols = ['tofE_ch2', 'tofE_ch3','tofE_ch4','tofE_ch5'], 
                                    n_thr=0, 
                                    y_thr=((125, 1000),(125, 1000),(125, 1000),(125, 1000)),
                                    PS_thr=(0.205, 0.386)) #-2, 2

    df_sliced.to_parquet(f'data/EJ331/sliced/with_PS_cut/df_slice_{num}.pkl')
    print(f'"data/EJ331/with_PS_cut/with_df_slice_{num}.pkl" saved to disk...')
    num+= 1


###TESTING SLICED DATA ####
df_slice_merge = pd.read_parquet("data/EJ331/sliced/with_PS_cut/df_slice_0.pkl")
df_slice_merge = pd.concat([df_slice_merge, pd.read_parquet("data/EJ331/sliced/with_PS_cut/df_slice_1.pkl")])
df_slice_merge = pd.concat([df_slice_merge, pd.read_parquet("data/EJ331/sliced/with_PS_cut/df_slice_2.pkl")])
df_slice_merge = pd.concat([df_slice_merge, pd.read_parquet("data/EJ331/sliced/with_PS_cut/df_slice_3.pkl")])
df_slice_merge = pd.concat([df_slice_merge, pd.read_parquet("data/EJ331/sliced/with_PS_cut/df_slice_4.pkl")])
# df_slice_merge = pd.concat([df_slice_merge, pd.read_parquet("data/EJ331/sliced/with_PS_cut/df_slice_5.pkl")])
# df_slice_merge = pd.concat([df_slice_merge, pd.read_parquet("data/EJ331/sliced/with_PS_cut/df_slice_6.pkl")])
# df_slice_merge = pd.concat([df_slice_merge, pd.read_parquet("data/EJ331/sliced/with_PS_cut/df_slice_7.pkl")])

numBins = 50*2
binRange=[0, 6000]
plt.figure()
plt.suptitle('EJ 331)')
for index, energy in enumerate(np.arange(1.2, 6.2, 0.2)):
    plt.subplot(5, 5, index+1)
    print(f'{round(energy,1)}_MeV')
    plt.hist(df_slice_merge[f'{round(energy,1)}_MeV'], bins=numBins, range=binRange, label=f'{round(energy,1)} MeV, len={len(df_slice_merge[f"{round(energy,1)}_MeV"].dropna())}')
    plt.ylim([0, 2500])
    plt.legend()
# plt.show()



###TESTING SLICED DATA ####
df_slice_merge = pd.read_parquet("data/NE213A/sliced/with_PS_cut/df_slice_0.pkl")
df_slice_merge = pd.concat([df_slice_merge, pd.read_parquet("data/NE213A/sliced/with_PS_cut/df_slice_1.pkl")])
df_slice_merge = pd.concat([df_slice_merge, pd.read_parquet("data/NE213A/sliced/with_PS_cut/df_slice_2.pkl")])
df_slice_merge = pd.concat([df_slice_merge, pd.read_parquet("data/NE213A/sliced/with_PS_cut/df_slice_3.pkl")])
df_slice_merge = pd.concat([df_slice_merge, pd.read_parquet("data/NE213A/sliced/with_PS_cut/df_slice_4.pkl")])
# df_slice_merge = pd.concat([df_slice_merge, pd.read_parquet("data/EJ331/sliced/with_PS_cut/df_slice_5.pkl")])
# df_slice_merge = pd.concat([df_slice_merge, pd.read_parquet("data/EJ331/sliced/with_PS_cut/df_slice_6.pkl")])
# df_slice_merge = pd.concat([df_slice_merge, pd.read_parquet("data/EJ331/sliced/with_PS_cut/df_slice_7.pkl")])

# numBins = 50
binRange=[0, 6000]
plt.figure()
plt.suptitle('NE 213A')
for index, energy in enumerate(np.arange(1.2, 6.2, 0.2)):
    plt.subplot(5, 5, index+1)
    print(f'{round(energy,1)}_MeV')
    plt.hist(df_slice_merge[f'{round(energy,1)}_MeV'], bins=numBins, range=binRange, label=f'{round(energy,1)} MeV, len={len(df_slice_merge[f"{round(energy,1)}_MeV"].dropna())}')
    plt.ylim([0, 2500])
    plt.legend()
plt.show()
