#!/usr/bin/env python3
import sys
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit

# IMPORT FROM LIBRARY DIRECTORY
sys.path.insert(0, "../library/")
sys.path.insert(0, "../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "sim_analysis/")
sys.path.insert(0, "data_analysis/")

import digidata
import nicholai_plot_helper as nplt
import processing_utility as prout
import processing_math as promath
import processing_data as prodata
import processing_pd as propd
import processing_simulation as prosim

"""
Script for running both data and simulation and calibrating data of EJ305 detector.
"""
#TODO fit gaussian to pencilbeam data on the right side of the distyribution.


PMT_gain = 4e6 #Nominal gain is 7'000'000 at nominal A/lm (model: 9821B p1)
att_factor_11dB = 3.55
att_factor_12dB = 3.98
att_factor_16_5dB = 6.68
att_factor_19dB = 8.91
q = 1.60217662E-19 #charge of a single electron
plot = False

path_sim = '/media/gheed/Seagate_Expansion_Drive1/data/simulation/'

##############################
# LOADING DATA
##############################
path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop
bg_runs =       list(range(3243, 3255))
Cs137_runs =    [3240]
Th232_runs =    [3241]
AmBe_runs =     [3242]
PuBe_runs =     [3038]#list(range(2930, 2933))

CableLengthCorrections = 0.97 #correction constant between inside cave and outside due to different cable lengthsc

df_bg =         propd.load_parquet_merge(path, bg_runs,     keep_col=['qdc_lg_ch1'], full=False)*CableLengthCorrections
df_Cs137 =      propd.load_parquet_merge(path, Cs137_runs,  keep_col=['qdc_lg_ch1'], full=False)*CableLengthCorrections
df_Th232 =      propd.load_parquet_merge(path, Th232_runs,  keep_col=['qdc_lg_ch1'], full=False)*CableLengthCorrections
df_AmBe =       propd.load_parquet_merge(path, AmBe_runs,   keep_col=['qdc_lg_ch1'], full=False)*CableLengthCorrections
df_PuBe =       propd.load_parquet_merge(path, PuBe_runs,   keep_col=['qdc_lg_ch1'], full=False)


# y,x = np.histogram(df_AmBe, bins=np.arange(0,25000,100))
# plt.step(prodata.getBinCenters(x)*0.97, y)
# y,x = np.histogram(df_PuBe, bins=np.arange(0,25000,100))
# plt.step(prodata.getBinCenters(x), y*0.55)
# plt.yscale('log')

# plt.show()

###############################
# LOADING SIMULATION DATA
###############################
path_sim = '/media/gheed/Seagate_Expansion_Drive1/data/simulation/'
Cs137_sim =         np.genfromtxt(path_sim + 'EJ305/Cs137/isotropic/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
Th232_sim =         np.genfromtxt(path_sim + 'EJ305/Th232/isotropic/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]
AmBe_sim =          np.genfromtxt(path_sim + 'EJ305/4.44/isotropic/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1]

#append extra data
Cs137_sim =         np.append(Cs137_sim, np.genfromtxt(path_sim + 'EJ305/Cs137/isotropic/part2/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])
Cs137_sim =         np.append(Cs137_sim, np.genfromtxt(path_sim + 'EJ305/Cs137/isotropic/part3/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])
Cs137_sim =         np.append(Cs137_sim, np.genfromtxt(path_sim + 'EJ305/Cs137/isotropic/part4/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])
Cs137_sim =         np.append(Cs137_sim, np.genfromtxt(path_sim + 'EJ305/Cs137/isotropic/part5/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])
Cs137_sim =         np.append(Cs137_sim, np.genfromtxt(path_sim + 'EJ305/Cs137/isotropic/part6/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])
Cs137_sim =         np.append(Cs137_sim, np.genfromtxt(path_sim + 'EJ305/Cs137/isotropic/part7/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])
Cs137_sim =         np.append(Cs137_sim, np.genfromtxt(path_sim + 'EJ305/Cs137/isotropic/part8/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])

Th232_sim =         np.append(Th232_sim, np.genfromtxt(path_sim + 'EJ305/Th232/isotropic/part2/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])
Th232_sim =         np.append(Th232_sim, np.genfromtxt(path_sim + 'EJ305/Th232/isotropic/part3/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])
Th232_sim =         np.append(Th232_sim, np.genfromtxt(path_sim + 'EJ305/Th232/isotropic/part4/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])
Th232_sim =         np.append(Th232_sim, np.genfromtxt(path_sim + 'EJ305/Th232/isotropic/part5/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])
Th232_sim =         np.append(Th232_sim, np.genfromtxt(path_sim + 'EJ305/Th232/isotropic/part6/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])
Th232_sim =         np.append(Th232_sim, np.genfromtxt(path_sim + 'EJ305/Th232/isotropic/part7/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])
Th232_sim =         np.append(Th232_sim, np.genfromtxt(path_sim + 'EJ305/Th232/isotropic/part8/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])
# Th232_sim =         np.append(Th232_sim, np.genfromtxt(path_sim + 'EJ305/Th232/isotropic/part9/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])

AmBe_sim =          np.append(AmBe_sim, np.genfromtxt(path_sim + 'EJ305/4.44/isotropic/part2/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])
AmBe_sim =          np.append(AmBe_sim, np.genfromtxt(path_sim + 'EJ305/4.44/isotropic/part3/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])
AmBe_sim =          np.append(AmBe_sim, np.genfromtxt(path_sim + 'EJ305/4.44/isotropic/part4/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])
AmBe_sim =          np.append(AmBe_sim, np.genfromtxt(path_sim + 'EJ305/4.44/isotropic/part5/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])
AmBe_sim =          np.append(AmBe_sim, np.genfromtxt(path_sim + 'EJ305/4.44/isotropic/part6/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])
AmBe_sim =          np.append(AmBe_sim, np.genfromtxt(path_sim + 'EJ305/4.44/isotropic/part7/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])
AmBe_sim =          np.append(AmBe_sim, np.genfromtxt(path_sim + 'EJ305/4.44/isotropic/part8/CSV_optphoton_sum_photocathode_no_cut_QE.csv', delimiter=',')[:-1])


###############################
# RANDOMIZE BINNING
###############################
y, x = np.histogram(Cs137_sim, bins=4096, range=[0, 4096])
Cs137_sim = prodata.getRandDist(x, y)
y, x = np.histogram(Th232_sim, bins=4096, range=[0, 4096])
Th232_sim = prodata.getRandDist(x, y)
y, x = np.histogram(AmBe_sim, bins=4096, range=[0, 4096])
AmBe_sim = prodata.getRandDist(x, y)


# #############################
# # CONVERT INTO FLOAT 
# #############################
# Cs137_sim =         pd.to_numeric(Cs137_sim, downcast='float')
# Th232_sim =         pd.to_numeric(Th232_sim, downcast='float')
# AmBe_sim =          pd.to_numeric(AmBe_sim, downcast='float')

###############################
# CALIBRATING SIMULATION DATA
###############################
Cs137_sim =         prosim.chargeCalibration(Cs137_sim      * q * PMT_gain)
Th232_sim =         prosim.chargeCalibration(Th232_sim      * q * PMT_gain)
AmBe_sim =          prosim.chargeCalibration(AmBe_sim       * q * PMT_gain)


plot = True 
###############################
# PMT ALIGN Cs-137
###############################
res = 0
numBins = np.arange(0, 40000, 200)
Cs137_sim_smear = np.zeros(len(Cs137_sim))
for i in range(len(Cs137_sim)):
    Cs137_sim_smear[i] = prosim.gaussSmear(Cs137_sim[i], res)


countsSim, binsSim = np.histogram(Cs137_sim_smear*2.569, bins=numBins)
binsSim = prodata.getBinCenters(binsSim)

w = np.empty(len(df_bg))
w.fill(1/len(bg_runs))
countsBG, binsBG = np.histogram(df_bg.qdc_lg_ch1*att_factor_19dB, bins=numBins, weights=w)

w = np.empty(len(df_Cs137))
w.fill(1/len(Cs137_runs))
counts, bins = np.histogram(df_Cs137.qdc_lg_ch1*att_factor_19dB, bins=numBins, weights=w)
bins = prodata.getBinCenters(bins)

plt.plot(binsSim, countsSim*5.754, label='sim')
plt.plot(bins, counts-countsBG, label='data-BG')
plt.legend()
plt.show()

Cs137_results = prosim.PMTGainAlign( model = Cs137_sim_smear, 
                                    data = df_Cs137.qdc_lg_ch1*att_factor_19dB, 
                                    bg = df_bg.qdc_lg_ch1*att_factor_19dB,
                                    scale = 6,
                                    gain = 2.5,
                                    data_weight = len(Cs137_runs),
                                    bg_weight = len(bg_runs),
                                    scaleBound = [4, 7],
                                    gainBound = [2, 3],
                                    stepSize = 1e-2,
                                    binRange = list(range(11200, 18000, 200)),
                                    plot = plot)
Cs137_results['smear'] = res



###############################
# PMT ALIGN Th232 2615 keV
###############################
res = 0
numBins = np.arange(43000, 110000, 800)
Th232_sim_smear = np.zeros(len(Th232_sim))
for i in range(len(Th232_sim)):
    Th232_sim_smear[i] = prosim.gaussSmear(Th232_sim[i], res)

countsSim, binsSim = np.histogram(Th232_sim_smear*2.404, bins=numBins)
binsSim = prodata.getBinCenters(binsSim)

w = np.empty(len(df_bg))
w.fill(1/len(bg_runs))
countsBG, binsBG = np.histogram(df_bg.qdc_lg_ch1*att_factor_19dB, bins=numBins, weights=w)

w = np.empty(len(df_Th232))
w.fill(1/len(Th232_runs))
counts, bins = np.histogram(df_Th232.qdc_lg_ch1*att_factor_19dB, bins=numBins, weights=w)
bins = prodata.getBinCenters(bins)

plt.plot(binsSim, countsSim*3.1, label='sim')
plt.plot(bins, counts-countsBG, label='data-BG')
plt.legend()
plt.show()

Th232_results = prosim.PMTGainAlign(model = Th232_sim_smear, 
                                    data = df_Th232.qdc_lg_ch1*att_factor_19dB, 
                                    bg = df_bg.qdc_lg_ch1*att_factor_19dB,
                                    scale = 3.0,
                                    gain = 2.4,
                                    data_weight = len(Th232_runs),
                                    bg_weight = len(bg_runs),
                                    scaleBound = [2.0, 5.0],
                                    gainBound = [2.0, 3.5],
                                    stepSize = 1e-2,
                                    binRange = list(range(72000, 87000, 800)),
                                    plot = plot)
Th232_results['smear'] = res

###############################
# PMT ALIGN AmBe 4439 keV
###############################
res = 0.0
numBins = np.arange(80000, 180000, 1000)
AmBe_sim_smear = np.zeros(len(AmBe_sim))
for i in range(len(AmBe_sim)):
    AmBe_sim_smear[i] = prosim.gaussSmear(AmBe_sim[i], res)

countsSim, binsSim = np.histogram(AmBe_sim_smear*2.1, bins=numBins)
binsSim = prodata.getBinCenters(binsSim)

w = np.empty(len(df_bg))
w.fill(1/len(bg_runs))
countsBG, binsBG = np.histogram(df_bg.qdc_lg_ch1*att_factor_19dB, bins=numBins, weights=w)

w = np.empty(len(df_AmBe))
w.fill(1/len(AmBe_runs))
counts, bins = np.histogram(df_AmBe.qdc_lg_ch1*att_factor_19dB, bins=numBins, weights=w)
bins = prodata.getBinCenters(bins)

plt.plot(binsSim, countsSim*6.351, label='sim')
plt.plot(bins, counts-countsBG, label='data-BG')
plt.legend()
plt.show()

AmBe_results = prosim.PMTGainAlign( model = AmBe_sim_smear, 
                                    data = df_AmBe.qdc_lg_ch1*att_factor_19dB, 
                                    bg = df_bg.qdc_lg_ch1*att_factor_19dB,
                                    scale = 6.1,
                                    gain = 2.0,
                                    data_weight = len(AmBe_runs),
                                    bg_weight = len(bg_runs),
                                    scaleBound = [5.0 , 7.0],
                                    gainBound = [1.5, 3.0],
                                    stepSize = 1e-2,
                                    binRange = list(range(110000, 126000, 1000)),
                                    plot = plot)
AmBe_results['smear'] = res






############################################
# Deriving QDC locations for each energy
############################################
path_sim = '/media/gheed/Seagate_Expansion_Drive1/data/simulation/'
names = [   'evtNum', 
            'optPhotonSum', 
            'optPhotonSumQE', 
            'optPhotonSumCompton', 
            'optPhotonSumComptonQE', 
            'xLoc', 
            'yLoc', 
            'zLoc', 
            'CsMin', 
            'optPhotonParentID', 
            'optPhotonParentCreatorProcess', 
            'optPhotonParentStartingEnergy',
            'edep']

#pencilbeam
Cs137_sim_pen_TMP = pd.read_csv(path_sim + "EJ305/Cs137/pencilbeam/CSV_optphoton_data_sum.csv", names=names)
Cs137_sim_pen_TMP = pd.concat([Cs137_sim_pen_TMP, pd.read_csv(path_sim + "EJ305/Cs137/pencilbeam/part2/CSV_optphoton_data_sum.csv", names=names)])
Cs137_sim_pen_TMP = pd.concat([Cs137_sim_pen_TMP, pd.read_csv(path_sim + "EJ305/Cs137/pencilbeam/part3/CSV_optphoton_data_sum.csv", names=names)])
Cs137_sim_pen_TMP = pd.concat([Cs137_sim_pen_TMP, pd.read_csv(path_sim + "EJ305/Cs137/pencilbeam/part4/CSV_optphoton_data_sum.csv", names=names)])
Cs137_sim_pen_TMP = pd.concat([Cs137_sim_pen_TMP, pd.read_csv(path_sim + "EJ305/Cs137/pencilbeam/part5/CSV_optphoton_data_sum.csv", names=names)])

Th232_sim_pen_TMP = pd.read_csv(path_sim + "EJ305/Th232/pencilbeam/CSV_optphoton_data_sum.csv", names=names)
Th232_sim_pen_TMP = pd.concat([Th232_sim_pen_TMP, pd.read_csv(path_sim + 'EJ305/Th232/pencilbeam/part2/CSV_optphoton_data_sum.csv', names=names)])
Th232_sim_pen_TMP = pd.concat([Th232_sim_pen_TMP, pd.read_csv(path_sim + 'EJ305/Th232/pencilbeam/part3/CSV_optphoton_data_sum.csv', names=names)])
Th232_sim_pen_TMP = pd.concat([Th232_sim_pen_TMP, pd.read_csv(path_sim + 'EJ305/Th232/pencilbeam/part4/CSV_optphoton_data_sum.csv', names=names)])
Th232_sim_pen_TMP = pd.concat([Th232_sim_pen_TMP, pd.read_csv(path_sim + 'EJ305/Th232/pencilbeam/part5/CSV_optphoton_data_sum.csv', names=names)])
Th232_sim_pen_TMP = pd.concat([Th232_sim_pen_TMP, pd.read_csv(path_sim + 'EJ305/Th232/pencilbeam/part6/CSV_optphoton_data_sum.csv', names=names)])
Th232_sim_pen_TMP = pd.concat([Th232_sim_pen_TMP, pd.read_csv(path_sim + 'EJ305/Th232/pencilbeam/part7/CSV_optphoton_data_sum.csv', names=names)])
Th232_sim_pen_TMP = pd.concat([Th232_sim_pen_TMP, pd.read_csv(path_sim + 'EJ305/Th232/pencilbeam/part8/CSV_optphoton_data_sum.csv', names=names)])
Th232_sim_pen_TMP = pd.concat([Th232_sim_pen_TMP, pd.read_csv(path_sim + 'EJ305/Th232/pencilbeam/part9/CSV_optphoton_data_sum.csv', names=names)])
Th232_sim_pen_TMP = pd.concat([Th232_sim_pen_TMP, pd.read_csv(path_sim + 'EJ305/Th232/pencilbeam/part10/CSV_optphoton_data_sum.csv', names=names)])

AmBe_sim_pen_TMP = pd.read_csv(path_sim + "EJ305/4.44/pencilbeam/CSV_optphoton_data_sum.csv", names=names)
AmBe_sim_pen_TMP = pd.concat([AmBe_sim_pen_TMP, pd.read_csv(path_sim + 'EJ305/4.44/pencilbeam/part2/CSV_optphoton_data_sum.csv', names=names)])
AmBe_sim_pen_TMP = pd.concat([AmBe_sim_pen_TMP, pd.read_csv(path_sim + 'EJ305/4.44/pencilbeam/part3/CSV_optphoton_data_sum.csv', names=names)])
AmBe_sim_pen_TMP = pd.concat([AmBe_sim_pen_TMP, pd.read_csv(path_sim + 'EJ305/4.44/pencilbeam/part4/CSV_optphoton_data_sum.csv', names=names)])
AmBe_sim_pen_TMP = pd.concat([AmBe_sim_pen_TMP, pd.read_csv(path_sim + 'EJ305/4.44/pencilbeam/part5/CSV_optphoton_data_sum.csv', names=names)])
AmBe_sim_pen_TMP = pd.concat([AmBe_sim_pen_TMP, pd.read_csv(path_sim + 'EJ305/4.44/pencilbeam/part6/CSV_optphoton_data_sum.csv', names=names)])
AmBe_sim_pen_TMP = pd.concat([AmBe_sim_pen_TMP, pd.read_csv(path_sim + 'EJ305/4.44/pencilbeam/part7/CSV_optphoton_data_sum.csv', names=names)])
AmBe_sim_pen_TMP = pd.concat([AmBe_sim_pen_TMP, pd.read_csv(path_sim + 'EJ305/4.44/pencilbeam/part8/CSV_optphoton_data_sum.csv', names=names)])
AmBe_sim_pen_TMP = pd.concat([AmBe_sim_pen_TMP, pd.read_csv(path_sim + 'EJ305/4.44/pencilbeam/part9/CSV_optphoton_data_sum.csv', names=names)])
AmBe_sim_pen_TMP = pd.concat([AmBe_sim_pen_TMP, pd.read_csv(path_sim + 'EJ305/4.44/pencilbeam/part10/CSV_optphoton_data_sum.csv', names=names)])

#randomize binning pencilbeam
y, x = np.histogram(Cs137_sim_pen_TMP.optPhotonSumComptonQE, bins=4096, range=[0, 4096])
Cs137_sim_pen = pd.DataFrame({'optPhotonSumComptonQE':prodata.getRandDist(x, y)})

y, x = np.histogram(Th232_sim_pen_TMP.optPhotonSumComptonQE, bins=4096, range=[0, 4096])
Th232_sim_pen = pd.DataFrame({'optPhotonSumComptonQE':prodata.getRandDist(x, y)})

y, x = np.histogram(AmBe_sim_pen_TMP.optPhotonSumComptonQE, bins=4096, range=[0, 4096])
AmBe_sim_pen = pd.DataFrame({'optPhotonSumComptonQE':prodata.getRandDist(x, y)})

#reset index pencilbeam 
# Cs137_sim_pen =  Cs137_sim_pen.reset_index()
# Th232_sim_pen =  Th232_sim_pen.reset_index()
# AmBe_sim_pen =   AmBe_sim_pen.reset_index()

#QDC calibrate simulations pencilbeam
Cs137_sim_pen.optPhotonSumComptonQE =      prosim.chargeCalibration(Cs137_sim_pen.optPhotonSumComptonQE * q * PMT_gain * Cs137_results['gain']) 
Th232_sim_pen.optPhotonSumComptonQE =      prosim.chargeCalibration(Th232_sim_pen.optPhotonSumComptonQE * q * PMT_gain * Th232_results['gain']) 
AmBe_sim_pen.optPhotonSumComptonQE =      prosim.chargeCalibration(AmBe_sim_pen.optPhotonSumComptonQE * q * PMT_gain * AmBe_results['gain']) 

#Apply smearing on pencilbeam data
# Cs137_sim_pen.optPhotonSumComptonQE = Cs137_sim_pen.optPhotonSumComptonQE.apply(lambda x: prosim.gaussSmear(x, Cs137_results['smear'])) 
# Th232_sim_pen.optPhotonSumComptonQE = Th232_sim_pen.optPhotonSumComptonQE.apply(lambda x: prosim.gaussSmear(x, Th232_results['smear'])) 
# AmBe_sim_pen.optPhotonSumComptonQE = AmBe_sim_pen.optPhotonSumComptonQE.apply(lambda x: prosim.gaussSmear(x, AmBe_results['smear'])) 


#####REOMVE ME######
plt.hist(Cs137_sim_pen, bins=200, range=[0,150000],label='Cs137', alpha=0.5)
plt.hist(Th232_sim_pen, bins=200, range=[0,150000],label='Th232', alpha=0.5)
plt.hist(AmBe_sim_pen, bins=100, range=[0,150000],label='AmBe', alpha=0.5)

# plt.hist(Cs137_sim, bins=150, range=[0,100000],label='Cs137',alpha=0.5)
# plt.hist(Th232_sim, bins=150, range=[0,100000],label='Th232',alpha=0.5)
# plt.hist(AmBe_sim*AmBe_results['gain'], bins=100, range=[0,150000],label='AmBe',alpha=0.5)

plt.legend()
plt.show()
#####REOMVE ME######



#############################
## FITTING PENCILBEAM DATA ##
#############################

start = 12000
stop = 21000
counts, bin_edges = np.histogram(Cs137_sim_pen.optPhotonSumComptonQE, bins = np.arange(start*0.8, stop*1.2, 400))
bin_centers = prodata.getBinCenters(bin_edges)
Ef = 0.66166
Cs137_sim_fit  =  prosim.calibrationFitBinned(counts, bin_centers, start, stop, Ef=Ef, plot=plot, compton=True)
Cs137_sim_fit['CE'] = promath.comptonMax(Ef)
mean = Cs137_sim_fit['gauss_mean']
std = Cs137_sim_fit['gauss_std']
Cs137_sim_results = {}
Cs137_sim_results['mean'] = np.mean(Cs137_sim_pen.query(f'optPhotonSumComptonQE>{mean-3*std} and optPhotonSumComptonQE<{mean+3*std}').optPhotonSumComptonQE)
Cs137_sim_results['mean_err']  = np.std(Cs137_sim_pen.query(f'optPhotonSumComptonQE>{mean-3*std} and optPhotonSumComptonQE<{mean+3*std}').optPhotonSumComptonQE)/np.sqrt(len(Cs137_sim_pen.query(f'optPhotonSumComptonQE>{mean-3*std} and optPhotonSumComptonQE<{mean+3*std}').optPhotonSumComptonQE))

start = 65000
stop = 90000
counts, bin_edges = np.histogram(Th232_sim_pen.optPhotonSumComptonQE, bins = np.arange(start*0.8, stop*1.2, 1000))
bin_centers = prodata.getBinCenters(bin_edges)
Ef = 2.614533
Th232_sim_fit = prosim.calibrationFitBinned(counts, bin_centers, start, stop, Ef=Ef, plot=plot, compton=True)
Th232_sim_fit['CE'] = promath.comptonMax(Ef)
mean = Th232_sim_fit['gauss_mean']
std = Th232_sim_fit['gauss_std']
Th232_sim_results = {}
Th232_sim_results['mean'] = np.mean(Th232_sim_pen.query(f'optPhotonSumComptonQE>{mean-3*std} and optPhotonSumComptonQE<{mean+3*std}').optPhotonSumComptonQE)
Th232_sim_results['mean_err']  = np.std(Th232_sim_pen.query(f'optPhotonSumComptonQE>{mean-3*std} and optPhotonSumComptonQE<{mean+3*std}').optPhotonSumComptonQE)/np.sqrt(len(Th232_sim_pen.query(f'optPhotonSumComptonQE>{mean-3*std} and optPhotonSumComptonQE<{mean+3*std}').optPhotonSumComptonQE))

start = 90000
stop = 130000
counts, bin_edges = np.histogram(AmBe_sim_pen.optPhotonSumComptonQE, bins = np.arange(start*0.8, stop*1.2, 2500))
bin_centers = prodata.getBinCenters(bin_edges)
Ef = 4.439
AmBe_sim_fit = prosim.calibrationFitBinned(counts, bin_centers, start, stop, Ef=Ef, plot=plot, compton=True)
AmBe_sim_fit['CE'] = promath.comptonMax(Ef)
mean = AmBe_sim_fit['gauss_mean']
std = AmBe_sim_fit['gauss_std']
AmBe_sim_results = {}
AmBe_sim_results['mean'] = np.mean(AmBe_sim_pen.query(f'optPhotonSumComptonQE>{mean-3*std} and optPhotonSumComptonQE<{mean+3*std}').optPhotonSumComptonQE)
AmBe_sim_results['mean_err']  = np.std(AmBe_sim_pen.query(f'optPhotonSumComptonQE>{mean-3*std} and optPhotonSumComptonQE<{mean+3*std}').optPhotonSumComptonQE)/np.sqrt(len(AmBe_sim_pen.query(f'optPhotonSumComptonQE>{mean-3*std} and optPhotonSumComptonQE<{mean+3*std}').optPhotonSumComptonQE))











##################################################
#Calculate calibration constant
CE_energy = np.array([Cs137_sim_fit['CE'], Th232_sim_fit['CE'], AmBe_sim_fit['CE']]) #energies in MeVee of Compton edges
QDC_loc = np.array([Cs137_sim_results['mean'], Th232_sim_results['mean'], AmBe_sim_results['mean']]) #relevant QDC channels
QDC_loc_err = np.array([Cs137_sim_results['mean_err'], Th232_sim_results['mean_err'], AmBe_sim_results['mean_err']]) #relevant QDC channels errors from gain-align above.

popt, pcov = curve_fit(promath.linearFunc, CE_energy, QDC_loc, sigma=QDC_loc_err, absolute_sigma=True)
pcov = np.diag(np.sqrt(pcov))
E_err = promath.errorPropLinearFuncInv(promath.linearFuncInv(QDC_loc, popt[0], popt[1]), popt[0], pcov[0], popt[1], pcov[1])

plt.scatter(QDC_loc, CE_energy)
plt.errorbar(QDC_loc, CE_energy, yerr = E_err, ls='none')
plt.plot(np.arange(0, 150000, 1000), promath.linearFuncInv(np.arange(0,150000,1000), popt[0], popt[1]))
plt.ylabel('MeV$_{ee}$')
plt.xlabel('QDC [channels]')
plt.show()


np.save('/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/EJ305/Ecal/popt', popt)
np.save('/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/EJ305/Ecal/pcov', pcov)


np.load('/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/EJ305/Ecal/popt.npy')


###REMOVE ME
# numBins=np.arange(0,60000,100)
# # plt.hist(df_bg.qdc_lg_ch1, bins=numBins, histtype='step', label='BG')
# plt.hist(df_Cs137.qdc_lg_ch1, bins=numBins, histtype='step', label='Cs137')
# plt.hist(df_Th232.qdc_lg_ch1, bins=numBins, histtype='step', label='Th232')

# y, x = np.histogram(df_AmBe.qdc_lg_ch1, bins=numBins)
# x = prodata.getBinCenters(x)
# plt.step(x,y, label='AmBe')

# y, x = np.histogram(df_PuBe.qdc_lg_ch1*.995, bins=numBins)
# x = prodata.getBinCenters(x)
# plt.step(x,y*5.8, label='PuBe')

# plt.yscale('log')
# plt.legend()
# plt.show()

