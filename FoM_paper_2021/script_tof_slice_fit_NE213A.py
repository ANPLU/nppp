#!/usr/bin/env python3
from re import I
import sys
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
from numpy.core.arrayprint import SubArrayFormat
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit

# IMPORT FROM LIBRARY DIRECTORY
sys.path.insert(0, "../library/")
sys.path.insert(0, "../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "../")
sys.path.insert(0, "data_analysis/")

import processing_math as promath
import processing_data as prodata
import processing_pd as propd
import processing_simulation as prosim

"""
Script for calibrating and slicing ToF spectra. Will save to disk in groups of 10 runs
"""

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# LOADING DATA & SLICING LOOP
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop
PuBe_tof_runs = np.arange(1728,1795)#[1728, 1729, 1730, 1731, 1732, 1733, 1734, 1735, 1736, 1737] #10st
run1 = 1728
num = 0
for i in np.arange(0,80,10):
    print('-------------------------------------------------')
    print(f'Now working on runs: {run1+i}-{run1+i+10}')
PuBe_tof_data =   propd.load_parquet_merge(path, np.arange(1837,1847), keep_col=[  'qdc_lg_ch1', 
                                                                            'qdc_sg_ch1',   
                                                                            'amplitude_ch1', 
                                                                            'amplitude_ch2', 
                                                                            'amplitude_ch3', 
                                                                            'amplitude_ch4', 
                                                                            'amplitude_ch5', 
                                                                            'qdc_ps_ch1', 
                                                                            'tof_ch2', 
                                                                            'tof_ch3', 
                                                                            'tof_ch4', 
                                                                            'tof_ch5'], 
                                                                            full=False)
    
#~~~~~~~~~~~~~~~~~~~~~~~~
#    CALIBRATING ToF
#~~~~~~~~~~~~~~~~~~~~~~~~
flashrange = {'tof_ch2':[40,60], 'tof_ch3':[40,60], 'tof_ch4':[40,60], 'tof_ch5':[40,60]} #gamma flash fitting ranges
distance = 0.959 #distance between source and center of detector
prodata.tof_calibration(PuBe_tof_data, flashrange, distance, phcut=125, calUnit=1e-9, energyCal=True)

#~~~~~~~~~~~~~~~~~~~~~~~~
#   SLICE ToF ENERGY
#~~~~~~~~~~~~~~~~~~~~~~~~
df_sliced = prodata.tof_slicer(PuBe_tof_data, 
                                qdc_col = 'qdc_lg_ch1', 
                                tof_energy_cols = ['tofE_ch2', 'tofE_ch3','tofE_ch4','tofE_ch5'], 
                                n_thr=0, 
                                y_thr=((125, 1000),(125, 1000),(125, 1000),(125, 1000)),
                                PS_thr=(0.345, 0.48)) #-2, 2

df_sliced.to_parquet(f'data/sliced/with_PS_cut/df_slice_{num}.pkl')
print(f'"data/no_PS_cut/df_slice_{num}.pkl" saved to disk...')
num+= 1

# df_sliced2 = prodata.tof_slicer(PuBe_tof_data, 
#                                 qdc_col = 'qdc_lg_ch1', 
#                                 tof_energy_cols = ['tofE_ch2', 'tofE_ch3','tofE_ch4','tofE_ch5'], 
#                                 n_thr=0, 
#                                 y_thr=((125, 1000),(125, 1000),(125, 1000),(125, 1000)),
#                                 PS_thr=(0.345, .48))

# #### WIP BELOW ####

# numBins = 64
# binRange=[0, 7500]
# plt.figure()
# for index, energy in enumerate(np.arange(1, 6, 0.2)):
#     plt.subplot(5, 5, index+1)
#     print(f'{round(energy,1)}_MeV')
#     plt.hist(df_sliced1[f'{round(energy,1)}_MeV'], bins=numBins, range=binRange, label=f'{round(energy,1)} MeV, len={len(df_sliced1[f"{round(energy,1)}_MeV"].dropna())}')
#     plt.legend()

# plt.suptitle('ToF Energy slices for QDC, PS_thr = (-2, 2)')


############
PuBe_tof_data =   propd.load_parquet_merge(path, np.arange(1728,1738), keep_col=[  'qdc_lg_ch1', 
                                                                                'qdc_sg_ch1', 
                                                                                'qdc_ps_ch1',
                                                                                'amplitude_ch2' ,
                                                                                'tof_ch2'], 
                                                                                full=False)
    
plt.figure()
plt.title('raw ToF ch2')
plt.hist(PuBe_tof_data.query('amplitude_ch2>125').tof_ch2, bins=200, range=[-750,250])

plt.figure()
plt.title('Raw QDC')
plt.hist(PuBe_tof_data.qdc_lg_ch1, bins=200, range=[-100,10000])

plt.figure()
plt.subplot(2,2,1)
plt.title('Cut QDC tof BG+YAP amplitude')
plt.hist(PuBe_tof_data.query('tof_ch2>-750 and tof_ch2<0 and amplitude_ch2>125').qdc_lg_ch1, bins=200, range=[-100,10000])

plt.subplot(2,2,2)
plt.title('Cut QDC tof Gamma+YAP amplitude')
plt.hist(PuBe_tof_data.query('tof_ch2>40 and tof_ch2<60 and amplitude_ch2>125').qdc_lg_ch1, bins=200, range=[-100,10000])

plt.subplot(2,2,3)
plt.title('Cut QDC tof neutron+YAP amplitude')
plt.hist(PuBe_tof_data.query('tof_ch2>70 and tof_ch2<120 and amplitude_ch2>125').qdc_lg_ch1, bins=200, range=[-100,10000])

#PS spectra

plt.figure()
plt.subplot(2,2,1)
plt.title('Cut PS tof BG+YAP amplitude')
plt.hist(PuBe_tof_data.query('tof_ch2>-750 and tof_ch2<0 and amplitude_ch2>125').qdc_ps_ch1, bins=200, range=[-1,1])

plt.subplot(2,2,2)
plt.title('Cut PS tof Gamma+YAP amplitude')
plt.hist(PuBe_tof_data.query('tof_ch2>40 and tof_ch2<60 and amplitude_ch2>125').qdc_ps_ch1, bins=200, range=[-1,1])

plt.subplot(2,2,3)
plt.title('Cut PS tof neutron+YAP amplitude')
plt.hist(PuBe_tof_data.query('tof_ch2>70 and tof_ch2<120 and amplitude_ch2>125').qdc_ps_ch1, bins=200, range=[-1,1])

plt.subplot(2,2,4)
plt.title('Cut PS YAP amplitude only')
plt.hist(PuBe_tof_data.query('tof_ch2>-750 and tof_ch2<250 and amplitude_ch2>125').qdc_ps_ch1, bins=200, range=[-1,1])
plt.show()

#hist2d
plt.figure()
plt.subplot(2,2,1)
plt.title('Cut PS tof BG+YAP amplitude')
plt.hist2d(PuBe_tof_data.query('tof_ch2>-750 and tof_ch2<0 and amplitude_ch2>125').qdc_lg_ch1, PuBe_tof_data.query('tof_ch2>-750 and tof_ch2<0 and amplitude_ch2>125').qdc_ps_ch1, bins=100, range=[[0,10000], [-1,1]], norm=LogNorm())

plt.subplot(2,2,2)
plt.title('Cut PS tof Gamma+YAP amplitude')
plt.hist2d(PuBe_tof_data.query('tof_ch2>40 and tof_ch2<60 and amplitude_ch2>125').qdc_lg_ch1, PuBe_tof_data.query('tof_ch2>40 and tof_ch2<60 and amplitude_ch2>125').qdc_ps_ch1, bins=100, range=[[0,10000], [-1,1]], norm=LogNorm())

plt.subplot(2,2,3)
plt.title('Cut PS tof neutron+YAP amplitude')
plt.hist2d(PuBe_tof_data.query('tof_ch2>70 and tof_ch2<120 and amplitude_ch2>125').qdc_lg_ch1, PuBe_tof_data.query('tof_ch2>70 and tof_ch2<120 and amplitude_ch2>125').qdc_ps_ch1, bins=100, range=[[0,10000], [-1,1]], norm=LogNorm())

plt.subplot(2,2,4)
plt.title('Cut PS YAP amplitude only')
plt.hist2d(PuBe_tof_data.query('tof_ch2>-750 and tof_ch2<250 and amplitude_ch2>125').qdc_lg_ch1, PuBe_tof_data.query('tof_ch2>-750 and tof_ch2<250 and amplitude_ch2>125').qdc_ps_ch1, bins=100, range=[[0,10000], [-1,1]], norm=LogNorm())
plt.show()

# plt.title('Cut QDC tof neutron+YAP amplitude')


###TESTING SLICED DATA ####
df_slice_merge = pd.read_parquet("data/sliced/no_PS_cut/df_slice_0.pkl")

# numBins = 32
# binRange=[0, 6000]
# # plt.figure()
# # for index, energy in enumerate(np.arange(5.2, 6.2, 0.2)):
# #     plt.subplot(2, 5, index+1)
# #     print(f'{round(energy,1)}_MeV')
# #     plt.hist(df_slice_merge[f'{round(energy,1)}_MeV'], bins=numBins, range=binRange, label=f'{round(energy,1)} MeV, len={len(df_slice_merge[f"{round(energy,1)}_MeV"].dropna())}')
# #     plt.legend()
# df_slice_merge = pd.concat([df_slice_merge, pd.read_parquet("data/sliced/no_PS_cut/df_slice_1.pkl")])
# df_slice_merge = pd.concat([df_slice_merge, pd.read_parquet("data/sliced/no_PS_cut/df_slice_2.pkl")])
# df_slice_merge = pd.concat([df_slice_merge, pd.read_parquet("data/sliced/no_PS_cut/df_slice_3.pkl")])
# df_slice_merge = pd.concat([df_slice_merge, pd.read_parquet("data/sliced/no_PS_cut/df_slice_4.pkl")])
# df_slice_merge = pd.concat([df_slice_merge, pd.read_parquet("data/sliced/no_PS_cut/df_slice_5.pkl")])
# df_slice_merge = pd.concat([df_slice_merge, pd.read_parquet("data/sliced/no_PS_cut/df_slice_6.pkl")])
# df_slice_merge = pd.concat([df_slice_merge, pd.read_parquet("data/sliced/no_PS_cut/df_slice_7.pkl")])

# for index, energy in enumerate(np.arange(1.2, 6.2, 0.2)):
#     plt.subplot(5, 5, index+1)
#     print(f'{round(energy,1)}_MeV')
#     plt.hist(df_slice_merge[f'{round(energy,1)}_MeV'], bins=numBins, range=binRange, label=f'{round(energy,1)} MeV, len={len(df_slice_merge[f"{round(energy,1)}_MeV"].dropna())}')
#     plt.legend()
#     # plt.ylim([0,1500])

# # plt.suptitle('ToF Energy slices (10h data-set)')


# plt.show()
