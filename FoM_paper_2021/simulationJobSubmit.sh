#!/bin/bash
# More safety, by turning some bugs into errors.
# Without `errexit` you don’t need ! and can replace
# PIPESTATUS with a simple $?, but I don’t do that.
set -o errexit -o pipefail -o noclobber -o nounset

# -allow a command to fail with !’s side effect on errexit
# -use return value from ${PIPESTATUS[0]}, because ! hosed $?
! getopt --test > /dev/null 
if [[ ${PIPESTATUS[0]} -ne 4 ]]; then
    echo 'I’m sorry, `getopt --test` failed in this environment.'
    exit 1
fi

OPTIONS=fo:vn:b:j:e:m:d:c:q:s:
LONGOPTS=force,output:,verbose,queue:,numevts:,numjobs:,numcores:,evtgen:,detmat:,distance:,pencilbeam,coneopening:

# -regarding ! and PIPESTATUS see above
# -temporarily store output to be able to check for errors
# -activate quoting/enhanced mode (e.g. by writing out “--options”)
# -pass arguments only via   -- "$@"   to separate them correctly
! PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTS --name "$0" -- "$@")
if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
    # e.g. return value is 1
    #  then getopt has complained about wrong arguments to stdout
    echo "Known options: verbose, force, out, queue, numevts, numjobs, numcores, evtgen, detmat, distance, pencilbeam, coneopening"
    exit 2
fi
# read getopt’s output this way to handle the quoting right:
eval set -- "$PARSED"

d=n f=n v=n queue=short numevts=1000 numjobs=1 numcores=4 evtgen=Cs137 detmat=NE213A distance=500 pencilbeam=False coneopening=15
# now enjoy the options in order and nicely split until we see --
while true; do
    case "$1" in
        -f|--force)
            f=y
            shift
            ;;
        -v|--verbose)
            v=y
            shift
            ;;
        -n|--numevts)
            numevts="$2"
            shift 2
            ;;
        -b|--numjobs)
            numjobs="$2"
            shift 2
            ;;
        -j|--numcores)
            numjobs="$2"
            shift 2
            ;;
        -e|--evtgen)
            evtgen="$2"
            shift 2
            ;;
        -m|--detmat)
            detmat="$2"
            shift 2
            ;;            
        -d|--distance)
            distance="$2"
            shift 2
            ;;   
        -c|--coneopening)
            coneopening="$2"
            shift 2
            ;;            
        -p|--pencilbeam)
            pencilbeam=True
            shift
            ;;
        -q|--queue)
            queue="$2"
            shift 2
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Programming error."
            exit 3
            ;;
    esac
done

# handle non-option arguments
if [[ $# -ne 1 ]]; then
    echo "$0: A single random-seed input file is required."
    exit 4
fi

echo "verbose: $v, force: $f, in: $1, queue: $queue, numevts: $numevts, numjobs: $numjobs, numcores: $numcores, evtgen:$evtgen, detmat: $detmat, distance: $distance, pencilbeam: $pencilbeam, coneopening: $coneopening"
runNum=0
runDate="$(date +%Y%m%d)"
for seed in $(head -n$numjobs $1)
    do 
    path="/mnt/groupdata/detector/workspaces/nmauritzson/$detmat/$evtgen"
    runNum=$(($runNum+1))
    mode="isotropic"
    if [ "$pencilbeam" = "True" ]
        then mode="pencilbeam"
    fi

    echo ess_dmscutils_submit $queue --email="nicholai.mauritzson@nuclear.lu.se" $path/$mode/${runDate}_run$runNum/ ess_ej321pchar_simanachain data -n$numevts -j$numcores -s$seed event_gen=$evtgen detector_mat=$detmat distance=$distance pencil_beam=$pencilbeam -l ESS_EJ321Pchar

done