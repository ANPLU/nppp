
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy.optimize import curve_fit

########################################

##### EJ305 TOF ######
#
energy = np.array([1500,  1750,  2000,  2250,  2500,  2750,  3000,  3250,  3500,  3750,  4000,  4250,  4500,  4750,  5000,  5250,  5500,  5750,  6000,  6250]) #energies

amp1 =   np.array([50,    50,    43,    60,    60,    155,   90,    230,   235,   194,   258,   265,   200,   248,   248,   282,   240,   221,   171,   154]) #amplitude 1
mean1 =  np.array([0.16,  0.17,  0.17,  0.17,  0.17,  0.17,  0.17,  0.17,  0.17,  0.169, 0.17,  0.17,  0.169, 0.169, 0.17,  0.17,  0.17,  0.17,  0.172, 0.17]) #mean 1
std1 =   np.array([0.005, 0.02,  0.02,  0.02,  0.02,  0.02,  0.02,  0.02,  0.02,  0.02,  0.02,  0.02,  0.02,  0.02,  0.02,  0.02,  0.02,  0.02,  0.02,  0.02]) #standard diviation 1

amp2 =   np.array([210,   215,   370,   640,   1060,  1890,  2200,  2700,  3150,  3600,  2840,  2900,  3000,  3200,  3140,  3100,  2600,  2250,  1650,  1100]) #amplitude 2
mean2 =  np.array([0.248, 0.248, 0.247, 0.246, 0.245, 0.244, 0.243, 0.243, 0.242, 0.243, 0.238, 0.237, 0.236, 0.236, 0.233, 0.232, 0.231, 0.230, 0.229, 0.228]) #mean 2
std2 =   np.array([0.036, 0.02,  0.03,  0.02,  0.02,  0.03,  0.02,  0.02,  0.016, 0.02,  0.02,  0.02,  0.026, 0.026, 0.02,  0.02,  0.02,  0.02,  0.016, 0.02]) #standard diviation 2

df = pd.DataFrame({'energy':energy,
                    'amp1' :amp1,
                    'mean1':mean1,
                    'std1' :std1,
                    'amp2' :amp2,
                    'mean2':mean2,
                    'std2' :std2})

#save EJ305 double gaussian fitting values to disk
df.to_pickle('/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/EJ305/doubleGaussParameters/fitParams_TOF') 




##### EJ305 THR ######
#
energy = np.array([250,   500,   750,   1000,  1250,  1500,  1750,  2000,  2250,  2500,  2750,  3000,  3250,  3500,  3750]) #energies

amp1 =   np.array([1300,  820,   500,   460,   430,   366,   314,   280,   230,   194,   285,   265,   200,   248,   248]) #amplitude 1
mean1 =  np.array([0.175, 0.172, 0.17,  0.17,  0.17,  0.17,  0.17,  0.17,  0.17,  0.173, 0.174, 0.17,  0.169, 0.169, 0.17]) #mean 1
std1 =   np.array([0.02,  0.01,  0.02,  0.02,  0.02,  0.02,  0.02,  0.02,  0.02,  0.02,  0.02,  0.02,  0.02,  0.02,  0.02]) #standard diviation 1

amp2 =   np.array([10000, 7580,  6300,  5100,  4100,  3000,  2220,  1470,  873,   528,   280,   2900,  3000,  3200,  3140]) #amplitude 2
mean2 =  np.array([0.238, 0.236, 0.236, 0.232, 0.231, 0.228, 0.226, 0.225, 0.224, 0.224, 0.220, 0.237, 0.236, 0.236, 0.233]) #mean 2
std2 =   np.array([0.017, 0.024, 0.017, 0.017, 0.017, 0.017, 0.017, 0.017, 0.017, 0.017, 0.017, 0.017, 0.017, 0.017, 0.017]) #standard diviation 2

df = pd.DataFrame({'energy':energy,
                    'amp1' :amp1,
                    'mean1':mean1,
                    'std1' :std1,
                    'amp2' :amp2,
                    'mean2':mean2,
                    'std2' :std2})

#save EJ305 double gaussian fitting values to disk
df.to_pickle('/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/EJ305/doubleGaussParameters/fitParams_thr') 





