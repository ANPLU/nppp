#!/usr/bin/env python3
from re import I
import sys
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
from numpy.core.arrayprint import SubArrayFormat
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit
import random
# from matplotlib.colors import LinearSegmentedColormap


# IMPORT FROM LIBRARY DIRECTORY
sys.path.insert(0, "../library/")
sys.path.insert(0, "../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "../")
sys.path.insert(0, "data_analysis/")

import processing_math as promath
import processing_data as prodata
import processing_pd as propd
import figure_style as fs

########################################
######## DETECTOR SETTINGS #############
########################################

detector = "EJ305"
run1 = 3038
numRuns = 16
runList = np.arange(run1, run1+numRuns)

detector = "NE213A"
run1 = 2121
numRuns = 141
runList = np.arange(run1, run1+numRuns)

if detector=='NE213A':
    gainOffsets = np.array([1.        , 1.00449786, 1.02776289, 0.99543515, 0.99513401,
                        0.99373714, 1.01271988, 1.00555114, 1.00767454, 1.03011807,
                        1.03737901, 1.02339094, 1.01367455, 1.103476  , 1.01138804,
                        1.09498013, 0.99517056, 0.98747555, 1.01479879, 1.03541272,
                        1.01354815, 0.99957959, 0.97865142, 0.99600754, 1.00958113,
                        1.02217856, 1.03640639, 1.01903062, 0.99881911, 1.00631287,
                        0.9909422 , 1.00325513, 1.00190923, 0.99810678, 0.97982168,
                        1.0108797 , 1.01108049, 1.01447416, 1.04953386, 0.99252985,
                        0.98535389, 0.98752831, 0.96838927, 0.9648048 , 0.97339146,
                        0.98834493, 0.97955184, 0.9898073 , 0.98643835, 0.99461799,
                        1.00308614, 0.97061575, 1.01527638, 0.98378853, 0.98731022,
                        1.00510757, 0.98845017, 0.9743922 , 0.98245988, 0.98122753,
                        0.9691665 , 1.01991096, 0.98006913, 0.97535042, 0.99039761,
                        0.97771579, 0.97718466, 0.98845839, 0.97691246, 0.9784261 ,
                        0.99225482, 0.97087123, 0.97269272, 0.9912359 , 0.97617339,
                        1.00866114, 1.00550252, 0.99468125, 0.98253334, 0.97681113,
                        0.97632393, 0.96692358, 1.00644259, 1.00780908, 0.99361568,
                        0.97946456, 1.00178607, 0.98710232, 0.98580734, 1.02220332,
                        0.97701412, 0.98474256, 1.02551657, 0.97654263, 0.97509727,
                        1.00753811, 0.99528997, 0.97050454, 0.98562478, 0.9745818 ,
                        0.98730201, 0.97500751, 0.98760836, 0.97151791, 0.98084076,
                        0.98470394, 0.97723897, 0.99826923, 0.97042334, 0.98152384,
                        0.98179468, 1.00139174, 0.9907689 , 0.9982554 , 0.99021647,
                        0.98391455, 0.97733494, 0.96542459, 0.96496404, 0.97209604,
                        1.03033098, 0.98416693, 0.97510294, 0.97231365, 0.97591612,
                        0.96953299, 0.97641848, 0.96724081, 0.9753956 , 0.96514133,
                        0.976637  , 0.98310287, 0.98319185, 0.98085286, 0.96911062,
                        0.96717393, 0.98366547, 0.96810717, 0.97483992, 0.97766298,
                        0.98462132])
elif detector=='EJ305':
    gainOffsets = np.array([1.        , 0.99877128, 0.99767598, 0.99635701, 0.99197593,
                        0.98649201, 0.98895558, 0.98536092, 0.98939812, 0.97964808,
                        0.98002578, 0.98483199, 0.98064943, 0.9780858 , 0.97518736,
                        0.9713169 , 0.97133828, 0.97188016, 0.97165055, 0.97049724,
                        0.97302327, 0.96747695, 0.96874507, 0.96799071, 0.97165963,
                        0.97007535, 0.96348486, 0.96251294, 0.96772028, 0.96338966,
                        0.96013259, 0.9580445 , 0.95698137, 0.95311693, 0.95530385,
                        0.9567158 , 0.95529382, 0.95745533, 0.95649106, 0.95607423,
                        0.9529155 , 0.94892151, 0.95169893, 0.95193443, 0.95304247,
                        0.94862629, 0.94952805, 0.9463232 , 0.95027995, 0.94780845,
                        0.9455832 , 0.94513377, 0.94461188, 0.94690829, 0.94261956,
                        0.94475942, 0.94521732, 0.94332888, 0.94155264, 0.93613025,
                        0.93933973, 0.93888518, 0.9389024 , 0.93635384, 0.93804591,
                        0.93836761, 0.93407987, 0.9373619 , 0.93235643, 0.93125469,
                        0.933287  , 0.93024508, 0.93226542, 0.93203507, 0.93080803,
                        0.93102497, 0.93169489, 0.92854987, 0.92814411, 0.92695583,
                        0.93225842, 0.92950159, 0.9264453 , 0.92657767, 0.92688452,
                        0.92630767, 0.92585836, 0.92756427, 0.9251519 , 0.92495182,
                        0.92491318, 0.9229281 , 0.92426364, 0.92523386, 0.92114852,
                        0.91985767, 0.92246126, 0.92153287, 0.91871527, 0.92111036,
                        0.91776668, 0.91876388, 0.915461  , 0.91919211, 0.91693373,
                        0.91435636, 0.91669831, 0.91646481, 0.91614625, 0.91393512,
                        0.91459724, 0.92727997, 0.92576366, 0.9214748 , 0.92149341,
                        0.91979474, 0.92117764, 0.92076268, 0.91717538, 0.91734671,
                        0.91535393, 0.91667111, 0.91707768, 0.91591174, 0.91699673,
                        0.91586549, 0.91891214, 0.91342721, 0.9133015 , 0.9117215 ,
                        0.91434592, 0.91710669, 0.90954241, 0.91015051, 0.91426073,
                        0.90913286, 0.90502546, 0.90859501, 0.90537212, 0.90837459,
                        0.90863808, 0.90734634, 0.90886763, 0.9058143 , 0.90724037,
                        0.90688086, 0.90548787, 0.90190405, 0.90456622, 0.9033378 ,
                        0.90490383, 0.90353686, 0.90249701, 0.90379935, 0.90467293,
                        0.90240614, 0.90317903, 0.90953171, 0.90681221, 0.90720422,
                        0.8977046 , 0.90281663, 0.90158718, 0.89931385, 0.90497544,
                        0.89774941, 0.89908483, 0.89818075, 0.90339851, 0.90637168,
                        0.90342995, 0.89999479])

# ---------------------

distance = 0.959
att_factor_11dB = 3.55
att_factor_12dB = 3.98
att_factor_16_5dB = 6.68
gTOFRange = [0.5e-9, 6e-9]
randTOFRange = [-710e-9,-10e-9]
nTOFRange = [28.1e-9, 80e-9]#[28.1e-9, 60e-9]

nLength = np.abs(nTOFRange[1]-nTOFRange[0])
gLength = np.abs(gTOFRange[1]-gTOFRange[0])
randLength = np.abs(randTOFRange[1]-randTOFRange[0])

path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop

#Loading data from Neutron detector
randIndex = random.sample(range(0,len(runList)-1), 10)
plt.figure()
for i in randIndex:
    print(f'Reading run: {runList[i]}')
    PuBe_tof_data =   propd.load_parquet_merge_gainadjust(path, [runList[i]], keep_col=[ 'qdc_lg_ch1'], full=False, gainOffsets=[gainOffsets[i]])
    
    PuBe_tof_data.qdc_lg_ch1 = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/{detector}/Ecal', PuBe_tof_data.qdc_lg_ch1*att_factor_16_5dB)

    plt.hist(PuBe_tof_data.qdc_lg_ch1, bins=300, range= [0, 10], histtype='step')

plt.title('Neutron Detector')
plt.show()

#Loading data from YAPS
numRuns = 10
randIndex = random.sample(range(0,len(runList)-1), numRuns)
plt.figure()
for i in randIndex:
    print(f'Reading run: {runList[i]}')
    PuBe_tof_data =   propd.load_parquet_merge(path, [runList[i]], keep_col=[ 'qdc_lg_ch2', 'qdc_lg_ch3', 'qdc_lg_ch4', 'qdc_lg_ch5'], full=False)

    plt.subplot(2,2,1)
    plt.hist(PuBe_tof_data.qdc_lg_ch2, bins=200, range= [0, 16000], histtype='step')
    plt.yscale('log')
    plt.subplot(2,2,2)
    plt.hist(PuBe_tof_data.qdc_lg_ch3, bins=200, range= [0, 16000], histtype='step')
    plt.yscale('log')
    plt.subplot(2,2,3)
    plt.hist(PuBe_tof_data.qdc_lg_ch4, bins=200, range= [0, 16000], histtype='step')
    plt.yscale('log')
    plt.subplot(2,2,4)
    plt.hist(PuBe_tof_data.qdc_lg_ch5, bins=200, range= [0, 16000], histtype='step')
    plt.yscale('log')

plt.suptitle('YAP Detectors EJ305')
plt.show()



#Checking counts with YAP cuts applied across data sets
y2ThrQDC = 6450 #EJ305 QDC threshold in channels (NOT IN USE FOR THIS DATA SET)
y3ThrQDC = 6000 #EJ305 QDC threshold in channels
y4ThrQDC = 6000 #EJ305 QDC threshold in channels
y5ThrQDC = 7700 #EJ305 QDC threshold in channels

y2ThrQDC = 6450 #NE213A QDC threshold in channels (NOT IN USE FOR THIS DATA SET)
y3ThrQDC = 6985 #NE213A QDC threshold in channels
y4ThrQDC = 7850 #NE213A QDC threshold in channels
y5ThrQDC = 8750 #NE213A QDC threshold in channels

numRuns = 50
randIndex = random.sample(range(0,len(runList)-1), numRuns)
YAP2counts = np.empty(numRuns)
YAP3counts = np.empty(numRuns)
YAP4counts = np.empty(numRuns)
YAP5counts = np.empty(numRuns)
runs = np.empty(numRuns)

for j, i in enumerate(randIndex):
    print(f'Reading run: {runList[i]}')
    PuBe_tof_data =   propd.load_parquet_merge(path, [runList[i]], keep_col=[ 'qdc_lg_ch2', 'qdc_lg_ch3', 'qdc_lg_ch4', 'qdc_lg_ch5'], full=False)

    YAP2counts[j] = sum(PuBe_tof_data.query(f'qdc_lg_ch2>0').qdc_lg_ch2)
    YAP3counts[j] = sum(PuBe_tof_data.query(f'qdc_lg_ch3>0').qdc_lg_ch3)
    YAP4counts[j] = sum(PuBe_tof_data.query(f'qdc_lg_ch4>0').qdc_lg_ch4)
    YAP5counts[j] = sum(PuBe_tof_data.query(f'qdc_lg_ch5>0').qdc_lg_ch5)
    runs[j] = runList[i]

plt.scatter(runs, YAP2counts, label=f'YAP2>{y2ThrQDC}')
plt.scatter(runs, YAP3counts, label=f'YAP3>{y3ThrQDC}')
plt.scatter(runs, YAP4counts, label=f'YAP4>{y4ThrQDC}')
plt.scatter(runs, YAP5counts, label=f'YAP5>{y5ThrQDC}')
plt.legend()
plt.title('NE213A')
plt.ylabel('counts')
plt.xlabel('run number')
plt.show()






# PuBe_tof_data.qdc_lg_ch1 = 
# df1 = propd.load_parquet_merge(path, [2121], keep_col=[ 'qdc_lg_ch1'], full=False)
# df2 = propd.load_parquet_merge(path, [2121+140], keep_col=[ 'qdc_lg_ch1'], full=False)
# df0 = propd.load_parquet_merge(path, [2121], keep_col=[ 'qdc_lg_ch1', 'amplitude_ch1'], full=False)
# df1 = propd.load_parquet_merge(path, [2830], keep_col=[ 'qdc_lg_ch1', 'amplitude_ch1'], full=False)
# df2 = propd.load_parquet_merge(path, [2830+171], keep_col=[ 'qdc_lg_ch1', 'amplitude_ch1'], full=False)

# plt.hist(df0.amplitude_ch1, bins=np.arange(0,1000,1), label='raw', histtype='step')
# plt.hist(df1.amplitude_ch1, bins=np.arange(0,1000,1), label='raw', histtype='step')
# plt.hist(df1.amplitude_ch1*.7, bins=np.arange(0,1000,1), label='raw', histtype='step')
# plt.hist(df2.qdc_lg_ch1*0.899999, bins=np.arange(0,40000,100), label='raw', histtype='step')
# plt.hist(prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/{detector}/Ecal', df1.qdc_lg_ch1*att_factor_16_5dB), bins=np.arange(0,10, .001), label='calibrated')
# plt.show()