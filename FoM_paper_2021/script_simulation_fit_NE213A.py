#!/usr/bin/env python3
import sys
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit

# IMPORT FROM LIBRARY DIRECTORY
sys.path.insert(0, "../library/")
sys.path.insert(0, "../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "sim_analysis/")
sys.path.insert(0, "data_analysis/")

import processing_math as promath
import processing_simulation as prosim
import processing_data as prodata

"""
Script for fitting of simulated data
"""

PMT_gain = 4e6 #Nominal gain is 7'000'000 at nominal A/lm (model: 9821B p1)
att_factor_A = 6.14102564 #used when attenuating the signal before the digitizer.
q = 1.60217662E-19 #charge of a single electron
plot = False

##################################################
##################################################
# LOADING AND PROCESSING SIMULATED DATA
##################################################
##################################################
path_sim = '/media/gheed/Seagate_Expansion_Drive1/data/simulation/'
names = names = ['evtNum', 
                'optPhotonSum', 
                'optPhotonSumQE', 
                'optPhotonSumCompton', 
                'optPhotonSumComptonQE', 
                'xLoc', 
                'yLoc', 
                'zLoc', 
                'CsMin', 
                'optPhotonParentID', 
                'optPhotonParentCreatorProcess', 
                'optPhotonParentStartingEnergy',
                'edep']

df_Cs137_662_sim_pen =      pd.read_csv(path_sim + "NE213A/Cs137/pencilbeam/CSV_optphoton_data_sum.csv", names=names)
df_Na22_511_sim_pen =       pd.read_csv(path_sim + "NE213A/Na22_511/pencilbeam/CSV_optphoton_data_sum.csv", names=names)
df_Na22_1275_sim_pen =      pd.read_csv(path_sim + "NE213A/Na22_1275/pencilbeam/CSV_optphoton_data_sum.csv", names=names)
df_2615_sim_pen =           pd.read_csv(path_sim + "NE213A/2_62MeV/pencilbeam/CSV_optphoton_data_sum.csv", names=names)
df_4439_sim_pen =           pd.read_csv(path_sim + "NE213A/4_44MeV/pencilbeam/CSV_optphoton_data_sum.csv", names=names)

#INCREASE STATISTICS
df_Cs137_662_sim_pen =  pd.concat([df_Cs137_662_sim_pen, pd.read_csv(path_sim + "NE213A/Cs137/pencilbeam/part2/CSV_optphoton_data_sum.csv", names=names)])
df_Cs137_662_sim_pen =  pd.concat([df_Cs137_662_sim_pen, pd.read_csv(path_sim + "NE213A/Cs137/pencilbeam/part3/CSV_optphoton_data_sum.csv", names=names)])
df_Cs137_662_sim_pen =  pd.concat([df_Cs137_662_sim_pen, pd.read_csv(path_sim + "NE213A/Cs137/pencilbeam/part4/CSV_optphoton_data_sum.csv", names=names)])

df_Na22_511_sim_pen =   pd.concat([df_Na22_511_sim_pen, pd.read_csv(path_sim + "NE213A/Na22_511/pencilbeam/part2/CSV_optphoton_data_sum.csv", names=names)])
df_Na22_511_sim_pen =   pd.concat([df_Na22_511_sim_pen, pd.read_csv(path_sim + "NE213A/Na22_511/pencilbeam/part3/CSV_optphoton_data_sum.csv", names=names)])
df_Na22_511_sim_pen =   pd.concat([df_Na22_511_sim_pen, pd.read_csv(path_sim + "NE213A/Na22_511/pencilbeam/part4/CSV_optphoton_data_sum.csv", names=names)])
df_Na22_511_sim_pen =   pd.concat([df_Na22_511_sim_pen, pd.read_csv(path_sim + "NE213A/Na22_511/pencilbeam/part5/CSV_optphoton_data_sum.csv", names=names)])
df_Na22_511_sim_pen =   pd.concat([df_Na22_511_sim_pen, pd.read_csv(path_sim + "NE213A/Na22_511/pencilbeam/part6/CSV_optphoton_data_sum.csv", names=names)])
df_Na22_511_sim_pen =   pd.concat([df_Na22_511_sim_pen, pd.read_csv(path_sim + "NE213A/Na22_511/pencilbeam/part7/CSV_optphoton_data_sum.csv", names=names)])

df_Na22_1275_sim_pen =  pd.concat([df_Na22_1275_sim_pen, pd.read_csv(path_sim + 'NE213A/Na22_1275/pencilbeam/part2/CSV_optphoton_data_sum.csv', names=names)])
df_Na22_1275_sim_pen =  pd.concat([df_Na22_1275_sim_pen, pd.read_csv(path_sim + 'NE213A/Na22_1275/pencilbeam/part3/CSV_optphoton_data_sum.csv', names=names)])
df_Na22_1275_sim_pen =  pd.concat([df_Na22_1275_sim_pen, pd.read_csv(path_sim + 'NE213A/Na22_1275/pencilbeam/part4/CSV_optphoton_data_sum.csv', names=names)])
df_Na22_1275_sim_pen =  pd.concat([df_Na22_1275_sim_pen, pd.read_csv(path_sim + 'NE213A/Na22_1275/pencilbeam/part5/CSV_optphoton_data_sum.csv', names=names)])
df_Na22_1275_sim_pen =  pd.concat([df_Na22_1275_sim_pen, pd.read_csv(path_sim + 'NE213A/Na22_1275/pencilbeam/part6/CSV_optphoton_data_sum.csv', names=names)])
df_Na22_1275_sim_pen =  pd.concat([df_Na22_1275_sim_pen, pd.read_csv(path_sim + 'NE213A/Na22_1275/pencilbeam/part7/CSV_optphoton_data_sum.csv', names=names)])

df_2615_sim_pen =       pd.concat([df_2615_sim_pen, pd.read_csv(path_sim + 'NE213A/2_62MeV/pencilbeam/part2/CSV_optphoton_data_sum.csv', names=names)])
df_2615_sim_pen =       pd.concat([df_2615_sim_pen, pd.read_csv(path_sim + 'NE213A/2_62MeV/pencilbeam/part3/CSV_optphoton_data_sum.csv', names=names)])
df_2615_sim_pen =       pd.concat([df_2615_sim_pen, pd.read_csv(path_sim + 'NE213A/2_62MeV/pencilbeam/part4/CSV_optphoton_data_sum.csv', names=names)])
df_2615_sim_pen =       pd.concat([df_2615_sim_pen, pd.read_csv(path_sim + 'NE213A/2_62MeV/pencilbeam/part5/CSV_optphoton_data_sum.csv', names=names)])
df_2615_sim_pen =       pd.concat([df_2615_sim_pen, pd.read_csv(path_sim + 'NE213A/2_62MeV/pencilbeam/part6/CSV_optphoton_data_sum.csv', names=names)])
# df_2615_sim_pen =       pd.concat([df_2615_sim_pen, pd.read_csv(path_sim + 'NE213A/2_62MeV/pencilbeam/part7/CSV_optphoton_data_sum.csv', names=names)])
# df_2615_sim_pen =       pd.concat([df_2615_sim_pen, pd.read_csv(path_sim + 'NE213A/2_62MeV/pencilbeam/part8/CSV_optphoton_data_sum.csv', names=names)])

df_4439_sim_pen =       pd.concat([df_4439_sim_pen, pd.read_csv(path_sim + 'NE213A/4_44MeV/pencilbeam/part2/CSV_optphoton_data_sum.csv', names=names)])
df_4439_sim_pen =       pd.concat([df_4439_sim_pen, pd.read_csv(path_sim + 'NE213A/4_44MeV/pencilbeam/part3/CSV_optphoton_data_sum.csv', names=names)])
df_4439_sim_pen =       pd.concat([df_4439_sim_pen, pd.read_csv(path_sim + 'NE213A/4_44MeV/pencilbeam/part4/CSV_optphoton_data_sum.csv', names=names)])
df_4439_sim_pen =       pd.concat([df_4439_sim_pen, pd.read_csv(path_sim + 'NE213A/4_44MeV/pencilbeam/part5/CSV_optphoton_data_sum.csv', names=names)])
df_4439_sim_pen =       pd.concat([df_4439_sim_pen, pd.read_csv(path_sim + 'NE213A/4_44MeV/pencilbeam/part6/CSV_optphoton_data_sum.csv', names=names)])
# df_4439_sim_pen =       pd.concat([df_4439_sim_pen, pd.read_csv(path_sim + 'NE213A/4_44MeV/pencilbeam/part7/CSV_optphoton_data_sum.csv', names=names)])
# df_4439_sim_pen =       pd.concat([df_4439_sim_pen, pd.read_csv(path_sim + 'NE213A/4_44MeV/pencilbeam/part8/CSV_optphoton_data_sum.csv', names=names)])
# df_4439_sim_pen =       pd.concat([df_4439_sim_pen, pd.read_csv(path_sim + 'NE213A/4_44MeV/pencilbeam/part9/CSV_optphoton_data_sum.csv', names=names)])
# df_4439_sim_pen =       pd.concat([df_4439_sim_pen, pd.read_csv(path_sim + 'NE213A/4_44MeV/pencilbeam/part10/CSV_optphoton_data_sum.csv', names=names)])
# df_4439_sim_pen =       pd.concat([df_4439_sim_pen, pd.read_csv(path_sim + 'NE213A/4_44MeV/pencilbeam/part11/CSV_optphoton_data_sum.csv', names=names)])
# df_4439_sim_pen =       pd.concat([df_4439_sim_pen, pd.read_csv(path_sim + 'NE213A/4_44MeV/pencilbeam/part12/CSV_optphoton_data_sum.csv', names=names)])
# df_4439_sim_pen =       pd.concat([df_4439_sim_pen, pd.read_csv(path_sim + 'NE213A/4_44MeV/pencilbeam/part13/CSV_optphoton_data_sum.csv', names=names)])
# df_4439_sim_pen =       pd.concat([df_4439_sim_pen, pd.read_csv(path_sim + 'NE213A/4_44MeV/pencilbeam/part14/CSV_optphoton_data_sum.csv', names=names)])
# df_4439_sim_pen =       pd.concat([df_4439_sim_pen, pd.read_csv(path_sim + 'NE213A/4_44MeV/pencilbeam/part15/CSV_optphoton_data_sum.csv', names=names)])

#randomize and rebin data to minimize binning effects when plotting:
y, x = np.histogram(df_Cs137_662_sim_pen.optPhotonSumComptonQE, bins=4096, range=[0, 4096])
df_Cs137_662_sim_pen.optPhotonSumComptonQE = prodata.getRandDist(x, y)

y, x = np.histogram(df_Na22_511_sim_pen.optPhotonSumComptonQE, bins=4096, range=[0, 4096])
df_Na22_511_sim_pen.optPhotonSumComptonQE = prodata.getRandDist(x, y)

y, x = np.histogram(df_Na22_1275_sim_pen.optPhotonSumComptonQE, bins=4096, range=[0, 4096])
df_Na22_1275_sim_pen.optPhotonSumComptonQE = prodata.getRandDist(x, y)

y, x = np.histogram(df_2615_sim_pen.optPhotonSumComptonQE, bins=4096, range=[0, 4096])
df_2615_sim_pen.optPhotonSumComptonQE = prodata.getRandDist(x, y)

y, x = np.histogram(df_4439_sim_pen.optPhotonSumComptonQE, bins=4096, range=[0, 4096])
df_4439_sim_pen.optPhotonSumComptonQE = prodata.getRandDist(x, y)

#convert integers to floats
df_Cs137_662_sim_pen.optPhotonSumComptonQE =    pd.to_numeric(df_Cs137_662_sim_pen.optPhotonSumComptonQE, downcast='float')
df_Na22_511_sim_pen.optPhotonSumComptonQE =     pd.to_numeric(df_Na22_511_sim_pen.optPhotonSumComptonQE, downcast='float')
df_Na22_1275_sim_pen.optPhotonSumComptonQE =    pd.to_numeric(df_Na22_1275_sim_pen.optPhotonSumComptonQE, downcast='float')
df_2615_sim_pen.optPhotonSumComptonQE =         pd.to_numeric(df_2615_sim_pen.optPhotonSumComptonQE, downcast='float')
df_4439_sim_pen.optPhotonSumComptonQE =         pd.to_numeric(df_4439_sim_pen.optPhotonSumComptonQE, downcast='float')

# Reset index of all Pandas data sets:
df_Cs137_662_sim_pen =  df_Cs137_662_sim_pen.reset_index()
df_Na22_511_sim_pen =   df_Na22_511_sim_pen.reset_index()
df_Na22_1275_sim_pen =  df_Na22_1275_sim_pen.reset_index()
df_2615_sim_pen =       df_2615_sim_pen.reset_index()
df_4439_sim_pen =       df_4439_sim_pen.reset_index()

#calibrate simulated spectra to QDC channels
df_Cs137_662_sim_pen.optPhotonSumComptonQE =      prosim.chargeCalibration(df_Cs137_662_sim_pen.optPhotonSumComptonQE * q * PMT_gain)# * PMTgainCorrection['Cs137_662']
df_Na22_511_sim_pen.optPhotonSumComptonQE =       prosim.chargeCalibration(df_Na22_511_sim_pen.optPhotonSumComptonQE    * q * PMT_gain)# * PMTgainCorrection['Na22_511']
df_Na22_1275_sim_pen.optPhotonSumComptonQE =      prosim.chargeCalibration(df_Na22_1275_sim_pen.optPhotonSumComptonQE   * q * PMT_gain)# * PMTgainCorrection['Na22_1275']
df_2615_sim_pen.optPhotonSumComptonQE =           prosim.chargeCalibration(df_2615_sim_pen.optPhotonSumComptonQE        * q * PMT_gain)# * PMTgainCorrection['Th232_2615']
df_4439_sim_pen.optPhotonSumComptonQE =           prosim.chargeCalibration(df_4439_sim_pen.optPhotonSumComptonQE        * q * PMT_gain)# * PMTgainCorrection['AmBe_4439']

# Load simulation align data and calculating average gain
simulation_align =      pd.read_parquet('data/simulation_align.pkl')

gain = [simulation_align.Na22_511.gain, simulation_align.Cs137_662.gain, simulation_align.Na22_1275.gain, simulation_align.Th232_2620.gain, simulation_align.AmBe_4439.gain]
averageGain = np.average(gain)

# Applying gain and smearing
df_Na22_511_sim_pen.optPhotonSumComptonQE = df_Na22_511_sim_pen.optPhotonSumComptonQE.apply(lambda x: prosim.gaussSmear(x, simulation_align.Na22_511.smear)) 
df_Na22_511_sim_pen.optPhotonSumComptonQE = df_Na22_511_sim_pen.optPhotonSumComptonQE * averageGain

df_Cs137_662_sim_pen.optPhotonSumComptonQE = df_Cs137_662_sim_pen.optPhotonSumComptonQE.apply(lambda x: prosim.gaussSmear(x, simulation_align.Cs137_662.smear)) 
df_Cs137_662_sim_pen.optPhotonSumComptonQE = df_Cs137_662_sim_pen.optPhotonSumComptonQE * averageGain

df_Na22_1275_sim_pen.optPhotonSumComptonQE = df_Na22_1275_sim_pen.optPhotonSumComptonQE.apply(lambda x: prosim.gaussSmear(x, simulation_align.Na22_1275.smear)) 
df_Na22_1275_sim_pen.optPhotonSumComptonQE = df_Na22_1275_sim_pen.optPhotonSumComptonQE * averageGain

df_2615_sim_pen.optPhotonSumComptonQE = df_2615_sim_pen.optPhotonSumComptonQE.apply(lambda x: prosim.gaussSmear(x, simulation_align.Th232_2620.smear))
df_2615_sim_pen.optPhotonSumComptonQE = df_2615_sim_pen.optPhotonSumComptonQE * averageGain

df_4439_sim_pen.optPhotonSumComptonQE = df_4439_sim_pen.optPhotonSumComptonQE.apply(lambda x: prosim.gaussSmear(x, simulation_align.AmBe_4439.smear)) 
df_4439_sim_pen.optPhotonSumComptonQE = df_4439_sim_pen.optPhotonSumComptonQE * averageGain



##################################################
##################################################
# FITTING SIMULATED DATA
##################################################
##################################################

plot=True

start = 500
stop = 6000
counts, bin_edges = np.histogram(df_Na22_511_sim_pen.optPhotonSumComptonQE, bins = 30, range=[0, stop*1.2])
bin_centers = prodata.getBinCenters(bin_edges)
Ef = 0.510999
sim_res_Na22_511  =  prosim.calibrationFitBinned(counts, bin_centers, start, stop, Ef=0.510999, plot=plot, compton=True)

start = 1000
stop = 6500
counts, bin_edges = np.histogram(df_Cs137_662_sim_pen.optPhotonSumComptonQE, bins = 50, range=[0, stop*1.2])
bin_centers = prodata.getBinCenters(bin_edges)
Ef = 0.66166
sim_res_Cs137_662  =  prosim.calibrationFitBinned(counts, bin_centers, start, stop, Ef=Ef, plot=plot, compton=True)

start = 3000
stop = 13000
counts, bin_edges = np.histogram(df_Na22_1275_sim_pen.optPhotonSumComptonQE, bins = 50, range=[0, stop*1.2])
bin_centers = prodata.getBinCenters(bin_edges)
Ef = 1.2745
sim_res_Na22_1275 = prosim.calibrationFitBinned(counts, bin_centers, start, stop, Ef=Ef, plot=plot, compton=True)

start = 13000#13000
stop = 35000
counts, bin_edges = np.histogram(df_2615_sim_pen.optPhotonSumComptonQE, bins = 64, range=[0, stop*1.2])
bin_centers = prodata.getBinCenters(bin_edges)
Ef = 2.614533
sim_res_Th232_2620 = prosim.calibrationFitBinned(counts, bin_centers, start, stop, Ef=Ef, plot=plot, compton=True)

start = 34000#20000
stop = 60000
counts, bin_edges = np.histogram(df_4439_sim_pen.optPhotonSumComptonQE, bins = 50, range=[0, stop*1.2])
bin_centers = prodata.getBinCenters(bin_edges)
Ef = 4.439
sim_res_AmBe_4439 = prosim.calibrationFitBinned(counts, bin_centers, start, stop, Ef=Ef, plot=plot, compton=True)

##################################################
##################################################
# CALCULATE AVERAGE VALUE LOCATION
##################################################
##################################################
energies = np.array([   sim_res_Na22_511['recoil_max'],
                        sim_res_Cs137_662['recoil_max'],
                        sim_res_Na22_1275['recoil_max'],
                        sim_res_Th232_2620['recoil_max'],
                        sim_res_AmBe_4439['recoil_max']])

averageValues = np.array([  np.average(df_Na22_511_sim_pen.query(f'optPhotonSumComptonQE>{sim_res_Na22_511["gauss_mean"] - 3 * sim_res_Na22_511["gauss_std"]} and optPhotonSumComptonQE<{sim_res_Na22_511["gauss_mean"] + 3 * sim_res_Na22_511["gauss_std"]}').optPhotonSumComptonQE),
                            np.average(df_Cs137_662_sim_pen.query(f'optPhotonSumComptonQE>{sim_res_Cs137_662["gauss_mean"] - 3 * sim_res_Cs137_662["gauss_std"]} and optPhotonSumComptonQE<{sim_res_Cs137_662["gauss_mean"] + 3 * sim_res_Cs137_662["gauss_std"]}').optPhotonSumComptonQE),
                            np.average(df_Na22_1275_sim_pen.query(f'optPhotonSumComptonQE>{sim_res_Na22_1275["gauss_mean"] - 3 * sim_res_Na22_1275["gauss_std"]} and optPhotonSumComptonQE<{sim_res_Na22_1275["gauss_mean"] + 3 * sim_res_Na22_1275["gauss_std"]}').optPhotonSumComptonQE),
                            np.average(df_2615_sim_pen.query(f'optPhotonSumComptonQE>{sim_res_Th232_2620["gauss_mean"] - 3 * sim_res_Th232_2620["gauss_std"]} and optPhotonSumComptonQE<{sim_res_Th232_2620["gauss_mean"] + 3 * sim_res_Th232_2620["gauss_std"]}').optPhotonSumComptonQE),
                            np.average(df_4439_sim_pen.query(f'optPhotonSumComptonQE>{sim_res_AmBe_4439["gauss_mean"] - 3 * sim_res_AmBe_4439["gauss_std"]} and optPhotonSumComptonQE<{sim_res_AmBe_4439["gauss_mean"] + 3 * sim_res_AmBe_4439["gauss_std"]}').optPhotonSumComptonQE) ])

plt.scatter(energies, averageValues)
popt, pcov = curve_fit(promath.linearFunc, energies, averageValues)
pcov = np.diag(np.sqrt(pcov))
plt.plot(np.arange(0, 5, 0.1), promath.linearFunc(np.arange(0, 5, 0.1), popt[0], popt[1]))

print(f'k = {popt[0]} +- {pcov[0]}')
print(f'm = {popt[1]} +- {pcov[1]}')

plt.show()
# #Copmpile results into one DataFrame
# results = pd.DataFrame(pd.Series(  Na22_511_results, 
#                                 index = Na22_511_results.keys()), 
#                                 columns=['Na22_511'])
# results['Cs137_662'] = pd.Series(  Cs137_662_results, 
#                                 index = Cs137_662_results.keys())
# results['Na22_1275'] = pd.Series(Na22_1275_results, 
#                                  index = Na22_1275_results.keys())
# results['Th232_2620'] = pd.Series(Th232_2620_results,
#                                  index = Th232_2620_results.keys())
# results['AmBe_4439'] = pd.Series(AmBe_4439_results, 
#                                  index = AmBe_4439_results.keys())
# #Save DataFrame to disk
# results.to_parquet('data/simulation_align.pkl')

# #Code for loading from disk
# # my_df = pd.read_parquet('data/simulation_align.pkl')