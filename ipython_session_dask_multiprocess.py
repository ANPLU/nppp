# coding: utf-8
import digviz as dv
import dask
df = dv.load_h5_da("/drive2/SKB/gamma_tests/jadaq-00166.h5")
df['baseline'] = np.float64(0)
import numpy as np
df['baseline'] = np.float64(0)
df['baseline'] = df.map_partitions(lambda df: np.mean(df.samples[:20]), meta=df['baseline'])
df.head(100)
res = df.map_partitions(lambda df: np.mean(df.samples[:20]), meta=df['baseline'])
res.head(100)
res = df.map_partitions(lambda df: np.mean(df.samples[:20]), meta={None: 'f8'})
res.head(100)
res = df.map_partitions(lambda df: df.assign(baseline=np.mean(df.samples[:20])), meta={None: 'f8'})
res.head()
df.index
df.head(100).index
df = dv.load_h5_da("/drive2/SKB/gamma_tests/jadaq-00166.h5")
res = df.map_partitions(lambda df: df.assign(baseline=np.mean(df.samples[:20])), meta={None: 'f8'})
res.head()
df = dv.load_h5_da("/drive2/SKB/gamma_tests/jadaq-00166.h5")
df.head(100).index
df.head(100)
res = df.head(100).map_partitions(lambda df: df.assign(baseline=np.mean(df.samples[:20])), meta={None: 'f8'})
res = df.head(100, compute=False).map_partitions(lambda df: df.assign(baseline=np.mean(df.samples[:20])), meta={None: 'f8'})
res.head(100)
res = df.head(100, compute=False).map_partitions(lambda df: df.assign(baseline=np.mean([df.evtno, df.ts])), meta={None: 'f8'})
res.head(100)
res = df.head(100, compute=False).map_partitions(lambda df: df.assign(baseline=np.mean([df.evtno, df.ts])), meta={None: 'i8'})
res.head(100)
res = df.head(100, compute=False).map_partitions(lambda df: df.assign(baseline=np.mean([df.evtno, df.ts])), meta={'baseline': 'i8'})
res.head(100)
res = df.head(100, compute=False).map_partitions(lambda df: df.assign(baseline=np.mean([df.evtno, df.ts])))
res.head(100)
res = df.head(100, compute=False).map_partitions(lambda df: df.assign(baseline=np.mean([df.evtno, df.ts])))
res.head(100)
res.head(100).dtypes
res = df.head(100, compute=False).map_partitions(lambda df: df.assign(baseline=np.mean([df.evtno, df.channel])))
res.head(100).dtypes
res.head(100)
res = df.head(100, compute=False).map_partitions(lambda df: df.assign(baseline=np.mean([evtno, channel])))
res = df.head(100, compute=False).map_partitions(lambda df: df.assign(baseline=np.mean(df.channel)))
res.head(100)
res = df.head(100, compute=False).map_partitions(lambda df: df.assign(baseline=df.evtno*df.channel))
res.head(100)
res = df.head(100, compute=False).map_partitions(lambda df: df.assign(baseline=np.mean([evtno, channel], axis=1)))
res = df.head(100, compute=False).map_partitions(lambda df: df.assign(baseline=np.mean([df.evtno, df.channel], axis=1)))
res.head(100)
res = df.head(100, compute=False).map_partitions(lambda df: df.assign(baseline=np.mean([df.evtno, df.channel])))
res.head(100)
res = df.head(100, compute=False).map_partitions(lambda df: df.assign(baseline=np.mean([df.evtno, df.channel], axis=0)))
res.head(100)
res = df.head(100, compute=False).map_partitions(lambda df: df.assign(baseline=np.mean([df.samples[:20]], axis=0)))
res = df.head(100, compute=False).map_partitions(lambda df: df.assign(baseline=np.mean(df.samples[:20], axis=0)))
res = df.head(100, compute=False).map_partitions(lambda df: df.assign(baseline=np.mean(df.samples, axis=0)))
res = df.head(100, compute=False).map_partitions(lambda df: df.assign(baseline=np.mean(df.samples, axis=1)))
res = df.head(100, compute=False).map_partitions(lambda df: df.assign(baseline=np.mean(df.samples, axis=2)))
res = df.head(100, compute=False).map_partitions(lambda df: df.assign(baseline=np.mean(df.samples)))
res = df.head(100, compute=False).map_partitions(lambda df: df.assign(baseline=np.mean(df.samples[0])))
res = df.head(100, compute=False).map_partitions(lambda df: df.assign(baseline=np.mean(df.samples[0], axis=0)))
res = df.head(100, compute=False).map_partitions(lambda df: df.assign(baseline=np.mean(df.samples[0], axis=1)))
res = df.head(100, compute=False).map_partitions(lambda df: df.assign(baseline=np.mean(df.samples[0])))
res = df.head(100, compute=False).map_partitions(lambda df: df.assign(baseline=np.mean(df.samples[0])), meta=df)
res.head(100)
res = df.head(100, compute=False).map_partitions(lambda df: np.mean(df.samples[0]), meta={None: 'f8'})
res.head(100)
res
res.calculate()
res.compute()
res = df.head(100, compute=False).map_partitions(lambda df: np.mean(df.samples[0], axis=0), meta={None: 'f8'})
res.compute()
res = df.head(100, compute=False).map_partitions(lambda df: np.mean(df.samples[:20], axis=0), meta={None: 'f8'})
res.compute()
len(res.compute())
res = df.head(100, compute=False).map_partitions(lambda df: np.mean(df.samples[:20], axis=1), meta={None: 'f8'})
len(res.compute())
res.compute()
res = df.head(100, compute=False).map_partitions(lambda df: np.mean(df.samples[:20]), meta={None: 'f8'})
res.compute()
df.head(10)
df.head(10).samples
type(df.head(10).samples)
np.mean(df.head(10).samples)
np.mean(df.head(10).samples, axis=0)
np.mean(df.head(10).samples, axis=1)
np.mean(df.head(10).samples[:20])
(df.head(10).samples[:20])
(df.head(10).samples[:5])
(df.head(10).samples[:3])
(df.head(10).samples[:][:20])
(df.head(10).samples[:][:5])
(df.head(10).samples[:][:3])
(df.head(10).samples[:,:3])
(df.head(10).samples)
df.samples.to_dask_array
df.head(100).samples.to_dask_array.compute()
df.head(100).samples
df.head(100).to_dask_array().samples
df.head(100, compute=False).to_dask_array()
df.head(100, compute=False).samples.to_dask_array()
df.head(100, compute=False).samples.to_dask_array().compute()
df.head(100, compute=False).samples.to_dask_array().compute()[:20]
df.head(100, compute=False).samples.to_dask_array().compute()[:20,4]
df.head(100, compute=False).samples.to_dask_array().compute()[:20,:4]
df.head(100, compute=False).samples.to_dask_array().compute()[:20]
df.head(100, compute=False).samples.to_dask_array().compute()[:20][:3]
df.head(100, compute=False).samples.to_dask_array().compute()[:20]
df.head(100, compute=False).samples.to_dask_array().compute()[:1]
df.head(100, compute=False).samples.to_dask_array().compute()[:1][:20]
df.head(100, compute=False).samples.to_dask_array().compute()[:1, 1]
df.head(100, compute=False).samples.to_dask_array().compute()[:1]
df.head(100, compute=False).samples.to_dask_array().compute()[:1][0]
df.head(100, compute=False).samples.to_dask_array().compute()[:1][0][:20]
df.head(100, compute=False).samples.to_dask_array().compute()[0][:20]
df.head(100, compute=False).samples.compute()[0][:20]
df.head(100, compute=False).samples.compute()
df.head(10, compute=False).samples.compute()
np.mean(df.head(10, compute=False).samples.compute())
len(np.mean(df.head(10, compute=False).samples.compute()))
len(np.mean(df.head(10, compute=False).samples.compute(), axis=1))
len(np.mean(df.head(10, compute=False).samples.compute()[,1]))
len(np.mean(df.head(10, compute=False).samples.compute()[0,:20]))
np.mean(df.head(10, compute=False).samples.compute().reshape())
df.head(10, compute=False).samples.compute().shape
df.head(10, compute=False).samples.compute()[(10,20)]
df.head(10, compute=False).samples.compute()[[10,20]]
df.head(10, compute=False).samples.compute()[[1,2]]
df.head(10, compute=False).samples.compute()[(1,2),]
df.head(10, compute=False).samples.compute()[(1,:20),]
df.head(10, compute=False).compute().samples
df.head(10, compute=False).compute().samples[:20]
df.head(10, compute=False).compute().samples[:10]
df.head(10, compute=False).compute().samples[:5]
res = df.head(100, compute=False).map_partitions(lambda df: df.samples.apply(lambda row: np.mean(row[:20])), meta={None: 'f8'})
res.head(10)
res = df.head(100, compute=False).map_partitions(lambda df: df.samples.apply(lambda row: np.mean(row[:20])), meta={None: 'f8'})
res.head(10)
res = df.head(100, compute=False).map_partitions(lambda df: df.samples.apply(lambda row: np.mean(row[:20])))
res = df.head(100, compute=False).map_partitions(lambda df: df.samples.apply(lambda row: np.mean(row[:20])), meta={'samples': 'f8'})
res.head(10)
res = df.head(100, compute=False).map_partitions(lambda df: df.samples.apply(lambda row: np.mean(row[:20])), meta=df)
res.head(10)
res = df.head(100, compute=False).map_partitions(lambda df: df.baseline = df.samples.apply(lambda row: np.mean(row[:20])), meta=df)
res = df.head(100, compute=False).map_partitions(lambda df: df.assign(df.baseline=df.samples.apply(lambda row: np.mean(row[:20])), meta=df))
res = df.head(100, compute=False).map_partitions(lambda df: df.assign(baseline=df.samples.apply(lambda row: np.mean(row[:20])), meta=df))
res = df.head(100, compute=False).map_partitions(lambda df: df.assign(baseline=df.samples.apply(lambda row: np.mean(row[:20]), meta={None: 'f8'}), meta=df))
res = df.head(100, compute=False).map_partitions(lambda df: df.assign(baseline=df.samples.apply(lambda row: np.mean(row[:20])), meta={None: 'f8'}), meta=df)
res.head(5)
res = df.head(100, compute=False).map_partitions(lambda df: df.assign(baseline=df.samples.apply(lambda row: np.mean(row)), meta={None: 'f8'}), meta=df)
res.head(5)
def myfunc(samples):
    return samples.apply(lambda x: np.mean(x[:20]))
    
res = df.head(100, compute=False).map_partitions(myfunc, df.samples, meta={None: 'f8'})
res = df.head(10000, compute=False).map_partitions(myfunc, df.samples, meta={None: 'f8'})
res = df.map_partitions(myfunc, df.samples, meta={None: 'f8'})
res.head(100)
res = df.map_partitions(myfunc, df.samples)
res = df.map_partitions(myfunc, df.samples, meta={None: 'f8'})
def myfunc(df):
    return df.samples.apply(lambda x: np.mean(x[:20]))
    
res = df.map_partitions(myfunc, meta={None: 'f8'})
res.head(100)
res = df.map_partitions(myfunc, meta={None: 'f8'})
res = myfunc(df.samples)
df = dv.load_h5_da("/drive2/SKB/gamma_tests/jadaq-00166.h5")
res = myfunc(df)
def myfunc(df):
    return df.samples.apply(lambda x: np.mean(x[:20]), meta={None: 'f8'})
        
res = myfunc(df)
res.head(100)
def myfunc(df):
    return df.samples.apply(lambda x: np.mean(x[:20]), meta={'samples': 'f8'})
    
        
res = myfunc(df)
res.head(100)
def myfunc(df):
    return df.samples.apply(lambda x: np.mean(x[:20]), meta={'samples': 'f8'})
    
        
df.samples.apply(lambda x: np.mean(x[:20]), meta={'samples': 'f8'})
df.samples.apply(lambda x: np.mean(x[:20]), meta={'samples': 'f8'}).compute()
df.samples.apply(lambda x: np.mean(x[:20])).compute()
df.samples.apply(lambda x: np.mean(x[:20]), meta=df).compute()
df.samples.apply(lambda x: np.mean(x[:20]), meta={'baseline': 'f8'})
df.samples.apply(lambda x: np.mean(x[:20]), meta={'baseline': 'f8'}).compute()
df.samples.apply(lambda x: np.mean(x), meta={'baseline': 'f8'}).compute()
df.samples.apply(lambda x: np.mean([10,20]), meta={'baseline': 'f8'}).compute()
df.samples.apply(lambda x: np.mean([10,20]), meta={'None': 'f8'}).compute()
df['baseline'] = np.float64(0)
df['baseline'] = df.samples.apply(lambda x: np.mean(x[:20]), meta=df['baseline'])
df.head(100)
df.samples.apply(lambda x: np.mean(x[:20]), meta=df['baseline'])
df.samples.apply(lambda x: np.mean(x[:20]), meta=df['baseline']).compute()
df.samples.apply(lambda x: np.mean(x[:20]), meta=df['baseline']).head(100)
df.samples.apply(lambda x: np.mean(x[:20]), meta={'baseline': 'f8'}).head(100)
df.samples.apply(lambda x: np.mean(x[:20]), meta=df['baseline']).head(100)
df.samples.apply(lambda x: np.mean(x[:20]), meta=df['baseline'])
df.samples.apply(lambda x: np.mean(x[:20]), meta={'baseline': 'f8'})
def myfunc(df):
    return df.samples.apply(lambda x: np.mean(x[:20]), meta=df['baseline'])
    
    
        
res = myfunc(df)
res.head(100)
res = df.map_partitions(myfunc, meta={None: 'f8'})
res.head()
res = df.map_partitions(myfunc, meta=df['baseline'])
res.head()
def myfunc(df):
    return df.samples.apply(lambda x: np.mean(x[:20]))
    
    
    
        
res = df.map_partitions(myfunc, meta=df['baseline'])
res.head()
res.head(100)
res = df.map_partitions(df.samples.apply(lambda x: np.mean(x[:20])), meta=df['baseline'])
res = df.map_partitions(lambda df: df.samples.apply(lambda x: np.mean(x[:20])), meta=df['baseline'])
res.head(100)
res.compute()
res = df.samples.apply(lambda x: np.mean(x[:20]), meta=df['baseline'])
res.compute()
import timeit
res = df.samples.apply(lambda x: np.mean(x[:20]), meta=df['baseline'])
from dask.diagnostics import ProgressBar
res = df.map_partitions(lambda df: df.samples.apply(lambda x: np.mean(x[:20])), meta=df['baseline'])
with ProgressBar():
    res.compute()
    
res = df.samples.apply(lambda x: np.mean(x[:20]), meta=df['baseline'])
with ProgressBar():
    res.compute()
    
df.partitions
df.partitions.compute()
df.partitions
df.partitions()
df.partitions
df.npartitions
df.memory_usage(deep=True).sum().compute()
df.head()
print("Approximately {} MB of memory used for data loaded".format(round(72158156/(1024*1024),2)))
df = dv.load_h5_da("/drive2/SKB/gamma_tests/jadaq-00166.h5")
72158156/394132
df.compute().memory_usage(deep=True).sum()
len(df)
400000*4000*2/1000000
4000/250
df.repartition(npartitions=16)
df.npartitions
df = df.repartition(npartitions=16)
df.npartitions
df
df.compute()
df
res = df.map_partitions(lambda df: df.samples.apply(lambda x: np.mean(x[:20])), meta=df['baseline'])
df['baseline'] = np.float64(0)
res = df.map_partitions(lambda df: df.samples.apply(lambda x: np.mean(x[:20])), meta=df['baseline'])
with ProgressBar():
    res.compute()
    
res = df.samples.apply(lambda x: np.mean(x[:20]), meta=df['baseline'])
with ProgressBar():
    res.compute()
    
res = df.map_partitions(lambda df: df.samples.apply(lambda x: np.mean(x[:20])), meta=df['baseline'])
import dask.multiprocessing
with ProgressBar():
    res.compute(get=dask.multiprocessing.get)
    
import dask.multiprocessing
with ProgressBar():
    res.compute(scheduler='threads')
    
import dask.multiprocessing
with ProgressBar():
    res.compute(scheduler='process')
    
import dask.multiprocessing
with ProgressBar():
    res.compute(scheduler='multiprocessing')
    
import dask.multiprocessing
with ProgressBar():
    res.compute(scheduler='multiprocessing')
    
res = df.samples.apply(lambda x: np.mean(x[:20]), meta=df['baseline'])
import dask.multiprocessing
with ProgressBar():
    res.compute(scheduler='multiprocessing')
    
__version__
help
help()
import pandas as pd
pd.DataFrame(columns='baseline')
p = pd.DataFrame()
p.columns = ['baseline']
p = pd.DataFrame(columns=['baseline'])
p = pd.DataFrame(columns=['baseline'], dtypes=['f8'])
p = pd.DataFrame(columns=['baseline'], dtype=['f8'])
p = pd.DataFrame(columns=['baseline'], dtype=[np.float64])
p = pd.DataFrame(columns=['baseline'], dtype=np.dtype([np.float64]))
help(pd.DataFrame
)
p = pd.DataFrame(columns=['baseline'], dtype=np.float64)
df.samples.apply(lambda x: np.mean(x[:20]), meta=pd.DataFrame(columns=['baseline'], dtype=np.float64)['baseline'])
df.samples.apply(lambda x: np.mean(x[:20]), meta=pd.DataFrame(columns=['baseline'], dtype=np.float64)['baseline']).compute()
df.samples.apply(lambda x: np.mean(x[:20]), meta=pd.DataFrame(columns=['baseline'], dtype=np.float64)['baseline']).compute(scheduler='multiprocessing')
df.samples.apply(lambda x: np.mean([20]), meta=pd.DataFrame(columns=['baseline'], dtype=np.float64)['baseline']).compute(scheduler='multiprocessing')
df.map_partitions(lambda df: df.samples.apply(lambda x: np.mean(x[:20])), meta=pd.DataFrame(columns=['baseline'], dtype=np.float64)['baseline']).compute()
df.map_partitions(lambda df: df.samples.apply(lambda x: np.mean(x[:20])), meta=pd.DataFrame(columns=['baseline'], dtype=np.float64)['baseline']).compute(scheduler='processes')
df.map_partitions(lambda df: df.samples.apply(lambda x: np.mean([20])), meta=pd.DataFrame(columns=['baseline'], dtype=np.float64)['baseline']).compute(scheduler='processes')
def mymean(row):
    return np.mean(row[:20])
    
df.map_partitions(lambda df: df.samples.apply(lambda row: mymean(row)), meta=pd.DataFrame(columns=['baseline'], dtype=np.float64)['baseline']).compute(scheduler='processes')
df.map_partitions(lambda df: df.timestamp.apply(lambda row: mymean(row)), meta=pd.DataFrame(columns=['baseline'], dtype=np.float64)['baseline']).compute(scheduler='processes')
df.map_partitions(lambda df: df.ts.apply(lambda row: mymean(row)), meta=pd.DataFrame(columns=['baseline'], dtype=np.float64)['baseline']).compute(scheduler='processes')
df = dv.load_h5_da("/drive2/SKB/gamma_tests/jadaq-00166.h5")
df.map_partitions(lambda df: df.ts.apply(lambda row: mymean(row)), meta=pd.DataFrame(columns=['baseline'], dtype=np.float64)['baseline']).compute(scheduler='processes')
df = dv.load_h5_da("/drive2/SKB/gamma_tests/jadaq-00166.h5")
df.compute(scheduler='processes')
get_ipython().run_line_magic('save', 'ipython_session_dask_multiprocess.py ')
get_ipython().run_line_magic('save', 'ipython_session_dask_multiprocess.py 1-280')
