#!/usr/bin/env python3

"""
A script to convert from wavedump files

"""


import os
import sys
import time
import h5py
import numpy as np
import glob

if __name__ == "__main__":
    argv = sys.argv
    progName = os.path.basename(argv.pop(0))
    fname = ''.join(argv)
    file= h5py.File("./Output.h5",'w')
    f = file.create_group("Data")
    #chunksize = 1**4
    Header = []
    y = []
    d_Ev = f.create_dataset("Event",shape = (1,),maxshape = (None,), dtype='i8',chunks=True,compression="gzip")
    d_RL = f.create_dataset("Record Length",shape=(1,),maxshape=(None,),dtype='i8',chunks=True,compression="gzip")
    d_Ch = f.create_dataset("Channel",shape = (1,),maxshape = (None,), dtype='i8',chunks=True,compression="gzip")
    #d_Ch = f.create_dataset("Channel", dtype = 'unit32')
    d_T = f.create_dataset("Timestamp",shape = (1,),maxshape = (None,), dtype='i8',chunks=True,compression="gzip")
    #d_T = f.create_dataset("Timestamp", dype='unit32')
    running = True
    i = 1
    count = 0
    files = glob.glob(os.path.join(fname,'*.txt'))
    filescache = []
    f_count = 0
    for Filename in files:
        filescache.append(open(Filename))
        f_count+=1
    #print(filescache)
    #time.sleep(100)
    #f= h5py.File("./Test.h5",'w')
    f_num = 0
    loopcount = 0
    f_current = 0
    while running:
        #print(filescache[f_current])
        if loopcount == 0:
            line = filescache[f_current].readline()
            loopcount +=1
        if i == 1:
            i+=1
            #print(Filename)
            for j in range(1,8):
                if j !=1:
                    line = filescache[f_current].readline()
                #if line =='':
                 #   running = False
                  #  i = 3
                   # break
                #else:
                a = line.split()
                if running == False:
                    break
                #print(a[-1:])
                if j not in [2,5,7]:
                    Header.append(int(''.join(a[-1:])))

        #print(Header)
       # time.sleep(1)
        Record_Length = Header[0]
        Channel = Header[1]
        Event = Header[2]
        Timestamp = Header[3]
       # if count == 0:
        #    d_Ev = f.create_dataset("Event",shape=(1,1),maxshape=(None,None), data=[Event], chunks=True)
         #   d_Ch = f.create_dataset("Channel", data=Channel)
          #  d_T = f.create_dataset("Timestamp", data=Timestamp)
        #f.create_dataset("Event", data=Event)
        #else:
        d_Ev[-1:] = Event  
        d_Ev.resize(d_Ev.shape[0]+1,axis=0)
        d_RL[-1:] = Record_Length
        d_RL.resize(d_RL.shape[0]+1,axis=0)
        d_Ch[-1:] = Channel
        d_Ch.resize(d_Ch.shape[0]+1,axis=0)
        d_T[-1:] = Timestamp
        d_T.resize(d_T.shape[0]+1,axis=0)
       #     d_Ch[count] = Channel
        #    d_T[count] = Timestamp
        #for key in f.keys():
            #data = f[key][count]
         #   print(data)
        #print(Event)
        #print("\n")
        if i == 2:
            for k in range(0,Record_Length):
                a = filescache[f_current].readline()
                y.append(int(a))
        if count == 0:
            d_sample = f.create_dataset("Samples", (Record_Length,1,),maxshape=(Record_Length,None,),dtype='i8',chunks=True,compression="gzip")
            d_sample[:,count]=np.array(y)
            #print(np.array(y).shape)
        else:
            d_sample.resize((Record_Length,d_sample.shape[1]+1))
            #print(d_sample)
            d_sample[:,count] = np.array(y)
        #data  = f['Samples'][:,count]
        #print(data)
        #data2 = f['Event'][:]
        #print(data2)
        #time.sleep(2)
        i = 1
        count+=1
        #if count%50 == 0:
            #os.system('clear')
           # print("pulses processed ",count)
        y.clear()
        if f_current == f_count-1:
            f_current = 0
        else:
            f_current+=1

        #print(f_current)
        line = filescache[f_current].readline()
        if line=='':
            running = False
            #fp.close()
        Header.clear()

    
