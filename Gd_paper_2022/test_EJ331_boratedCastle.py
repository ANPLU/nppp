#!/usr/bin/env python3
from re import I
import sys
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
from numpy.core.arrayprint import SubArrayFormat
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit

# from matplotlib.colors import LinearSegmentedColormap


# IMPORT FROM LIBRARY DIRECTORY
sys.path.insert(0, "../library/")
sys.path.insert(0, "../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "../")
sys.path.insert(0, "data_analysis/")

import processing_math as promath
import processing_data as prodata
import processing_pd as propd
import figure_style as fs



path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop
df1 = propd.load_parquet_merge(path, np.arange(2343,2343+9), keep_col=['qdc_lg_ch1','qdc_lg_ch2','qdc_lg_ch3','qdc_lg_ch4','qdc_lg_ch5','amplitude_ch1', 'amplitude_ch2', 'amplitude_ch3', 'amplitude_ch4', 'amplitude_ch5', 'qdc_ps_ch1', 'tof_ch2', 'tof_ch3', 'tof_ch4', 'tof_ch5'], full=False)
df2 = propd.load_parquet_merge(path, np.arange(2571,2571+9), keep_col=['qdc_lg_ch1','qdc_lg_ch2','qdc_lg_ch3','qdc_lg_ch4','qdc_lg_ch5','amplitude_ch1', 'amplitude_ch2', 'amplitude_ch3', 'amplitude_ch4', 'amplitude_ch5', 'qdc_ps_ch1', 'tof_ch2', 'tof_ch3', 'tof_ch4', 'tof_ch5'], full=False)
df2 = propd.load_parquet_merge(path, np.arange(2594,2594+1), keep_col=['qdc_lg_ch1','qdc_lg_ch2','qdc_lg_ch3','qdc_lg_ch4','qdc_lg_ch5','amplitude_ch1', 'amplitude_ch2', 'amplitude_ch3', 'amplitude_ch4', 'amplitude_ch5', 'qdc_ps_ch1', 'tof_ch2', 'tof_ch3', 'tof_ch4', 'tof_ch5'], full=False)

distance = 0.959
flashrange = {f'tof_ch{2}':[60, 76], f'tof_ch{3}':[60, 76], f'tof_ch{4}':[60, 76], f'tof_ch{5}':[60, 76]} #gamma flash fitting ranges
prodata.tof_calibration(df1, flashrange, distance, phcut=125, calUnit=1e-9, energyCal=True)
prodata.tof_calibration(df2, flashrange, distance, phcut=125, calUnit=1e-9, energyCal=True)


# att_factor_12dB = 3.98
# att_factor_16dB = 6.31

gTOFRange = [1e-9, 6e-9] #Define range of gamma-flash
randTOFRange = [-710e-9,-10e-9] #Define range of random events
nTOFRange = [28.1e-9, 60e-9]
# gTOFRange = [64, 70] #Define range of gamma-flash
# randTOFRange = [-750, 0] #Define range of random events
# nTOFRange = [92, 120]

gLength = np.abs(gTOFRange[1]-gTOFRange[0]) #Calculate the lenght of cut for gammas
randLength = np.abs(randTOFRange[1]-randTOFRange[0]) #Calculate lenght of cut for random events
nLength = np.abs(nTOFRange[1]-nTOFRange[0])

numBins = np.arange(0, 1, 0.01)

y2ThrQDC = 6450 #QDC threhold in channels
y3ThrQDC = 6985 #QDC threhold in channels
y4ThrQDC = 7500 #QDC threhold in channels
y5ThrQDC = 7850 #QDC threhold in channels
# y2ThrQDC = 500 #QDC threhold in channels
# y3ThrQDC = 500 #QDC threhold in channels
# y4ThrQDC = 500 #QDC threhold in channels
# y5ThrQDC = 500 #QDC threhold in channels
nThrQDC = 5500 #QDC threshold in channels

#get random data
y2_random_ps1 = df1.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch2>{y2ThrQDC} and tof_ch2>{randTOFRange[0]} and tof_ch2<={randTOFRange[1]}').qdc_ps_ch1
y3_random_ps1 = df1.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch3>{y3ThrQDC} and tof_ch3>{randTOFRange[0]} and tof_ch3<={randTOFRange[1]}').qdc_ps_ch1
y4_random_ps1 = df1.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch4>{y4ThrQDC} and tof_ch4>{randTOFRange[0]} and tof_ch4<={randTOFRange[1]}').qdc_ps_ch1
y5_random_ps1 = df1.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch5>{y5ThrQDC} and tof_ch5>{randTOFRange[0]} and tof_ch5<={randTOFRange[1]}').qdc_ps_ch1
#check for random multiplicity
mpIdxRandom1 = prodata.uniqueEventHelper(y2_random_ps1, y3_random_ps1, y4_random_ps1, y5_random_ps1)
random_ps1 = pd.concat([y2_random_ps1.drop(mpIdxRandom1, errors='ignore'), 
                        y3_random_ps1.drop(mpIdxRandom1, errors='ignore'), 
                        y4_random_ps1.drop(mpIdxRandom1, errors='ignore'), 
                        y5_random_ps1.drop(mpIdxRandom1, errors='ignore')])

#get gamma data
y2_gamma_ps1 = df1.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch2>{y2ThrQDC} and tof_ch2>{gTOFRange[0]} and tof_ch2<={gTOFRange[1]}').qdc_ps_ch1
y3_gamma_ps1 = df1.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch3>{y3ThrQDC} and tof_ch3>{gTOFRange[0]} and tof_ch3<={gTOFRange[1]}').qdc_ps_ch1
y4_gamma_ps1 = df1.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch4>{y4ThrQDC} and tof_ch4>{gTOFRange[0]} and tof_ch4<={gTOFRange[1]}').qdc_ps_ch1
y5_gamma_ps1 = df1.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch5>{y5ThrQDC} and tof_ch5>{gTOFRange[0]} and tof_ch5<={gTOFRange[1]}').qdc_ps_ch1
mpIdxGamma1 = prodata.uniqueEventHelper(y2_gamma_ps1, y3_gamma_ps1, y4_gamma_ps1, y5_gamma_ps1)
gamma_ps1 = pd.concat([ y2_gamma_ps1.drop(mpIdxGamma1, errors='ignore'), 
                        y3_gamma_ps1.drop(mpIdxGamma1, errors='ignore'), 
                        y4_gamma_ps1.drop(mpIdxGamma1, errors='ignore'), 
                        y5_gamma_ps1.drop(mpIdxGamma1, errors='ignore')])

#get neutron data
y2_neutron_ps1 = df1.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch2>{y2ThrQDC} and tof_ch2>{nTOFRange[0]} and tof_ch2<={nTOFRange[1]}').qdc_ps_ch1
y3_neutron_ps1 = df1.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch3>{y3ThrQDC} and tof_ch3>{nTOFRange[0]} and tof_ch3<={nTOFRange[1]}').qdc_ps_ch1
y4_neutron_ps1 = df1.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch4>{y4ThrQDC} and tof_ch4>{nTOFRange[0]} and tof_ch4<={nTOFRange[1]}').qdc_ps_ch1
y5_neutron_ps1 = df1.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch5>{y5ThrQDC} and tof_ch5>{nTOFRange[0]} and tof_ch5<={nTOFRange[1]}').qdc_ps_ch1
mpIdxNeutron1 = prodata.uniqueEventHelper(y2_neutron_ps1, y3_neutron_ps1, y4_neutron_ps1, y5_neutron_ps1)
neutron_ps1 = pd.concat([   y2_neutron_ps1.drop(mpIdxNeutron1, errors='ignore'), 
                            y3_neutron_ps1.drop(mpIdxNeutron1, errors='ignore'), 
                            y4_neutron_ps1.drop(mpIdxNeutron1, errors='ignore'), 
                            y5_neutron_ps1.drop(mpIdxNeutron1, errors='ignore')])


########################################################################################
#get random data
y2_random_ps2 = df2.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch2>{y2ThrQDC} and tof_ch2>{randTOFRange[0]} and tof_ch2<={randTOFRange[1]}').qdc_ps_ch1
y3_random_ps2 = df2.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch3>{y3ThrQDC} and tof_ch3>{randTOFRange[0]} and tof_ch3<={randTOFRange[1]}').qdc_ps_ch1
y4_random_ps2 = df2.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch4>{y4ThrQDC} and tof_ch4>{randTOFRange[0]} and tof_ch4<={randTOFRange[1]}').qdc_ps_ch1
y5_random_ps2 = df2.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch5>{y5ThrQDC} and tof_ch5>{randTOFRange[0]} and tof_ch5<={randTOFRange[1]}').qdc_ps_ch1
#check for random multiplicity
mpIdxRandom2 = prodata.uniqueEventHelper(y2_random_ps2, y3_random_ps2, y4_random_ps2, y5_random_ps2)
random_ps2 = pd.concat([y2_random_ps2.drop(mpIdxRandom2, errors='ignore'), 
                        y3_random_ps2.drop(mpIdxRandom2, errors='ignore'), 
                        y4_random_ps2.drop(mpIdxRandom2, errors='ignore'), 
                        y5_random_ps2.drop(mpIdxRandom2, errors='ignore')])

#get gamma data
y2_gamma_ps2 = df2.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch2>{y2ThrQDC} and tof_ch2>{gTOFRange[0]} and tof_ch2<={gTOFRange[1]}').qdc_ps_ch1
y3_gamma_ps2 = df2.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch3>{y3ThrQDC} and tof_ch3>{gTOFRange[0]} and tof_ch3<={gTOFRange[1]}').qdc_ps_ch1
y4_gamma_ps2 = df2.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch4>{y4ThrQDC} and tof_ch4>{gTOFRange[0]} and tof_ch4<={gTOFRange[1]}').qdc_ps_ch1
y5_gamma_ps2 = df2.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch5>{y5ThrQDC} and tof_ch5>{gTOFRange[0]} and tof_ch5<={gTOFRange[1]}').qdc_ps_ch1
mpIdxGamma2 = prodata.uniqueEventHelper(y2_gamma_ps2, y3_gamma_ps2, y4_gamma_ps2, y5_gamma_ps2)
gamma_ps2 = pd.concat([ y2_gamma_ps2.drop(mpIdxGamma1, errors='ignore'), 
                        y3_gamma_ps2.drop(mpIdxGamma1, errors='ignore'), 
                        y4_gamma_ps2.drop(mpIdxGamma1, errors='ignore'), 
                        y5_gamma_ps2.drop(mpIdxGamma1, errors='ignore')])

#get neutron data
y2_neutron_ps2 = df2.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch2>{y2ThrQDC} and tof_ch2>{nTOFRange[0]} and tof_ch2<={nTOFRange[1]}').qdc_ps_ch1
y3_neutron_ps2 = df2.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch3>{y3ThrQDC} and tof_ch3>{nTOFRange[0]} and tof_ch3<={nTOFRange[1]}').qdc_ps_ch1
y4_neutron_ps2 = df2.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch4>{y4ThrQDC} and tof_ch4>{nTOFRange[0]} and tof_ch4<={nTOFRange[1]}').qdc_ps_ch1
y5_neutron_ps2 = df2.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch5>{y5ThrQDC} and tof_ch5>{nTOFRange[0]} and tof_ch5<={nTOFRange[1]}').qdc_ps_ch1
mpIdxNeutron2 = prodata.uniqueEventHelper(y2_neutron_ps2, y3_neutron_ps2, y4_neutron_ps2, y5_neutron_ps2)
neutron_ps2 = pd.concat([   y2_neutron_ps2.drop(mpIdxNeutron2, errors='ignore'), 
                            y3_neutron_ps2.drop(mpIdxNeutron2, errors='ignore'), 
                            y4_neutron_ps2.drop(mpIdxNeutron2, errors='ignore'), 
                            y5_neutron_ps2.drop(mpIdxNeutron2, errors='ignore')])

x1, yGamma1, yNeutron1 = prodata.randomTOFSubtraction(  gamma_ps1,
                                                        neutron_ps1,
                                                        random_ps1,
                                                        gLength, 
                                                        nLength, 
                                                        randLength, 
                                                        numBins, 
                                                        plot=False)

x2, yGamma2, yNeutron2 = prodata.randomTOFSubtraction(  gamma_ps2,
                                                        neutron_ps2,
                                                        random_ps2,
                                                        gLength, 
                                                        nLength, 
                                                        randLength, 
                                                        numBins, 
                                                        plot=False)


#### MAKE PLOT ####

plt.title(f'Neutron threshold = {nThrQDC} (QDC)')
plt.step(x1, yGamma1, lw=2, color='tomato',alpha=0.5, label='g-flash cut')
plt.step(x1, yNeutron1, lw=2, color='grey', label='lead')
plt.step(x2, yNeutron2, lw=2, color='black', label='lead+bor')
plt.xlabel('PS')
plt.ylabel('counts')
plt.legend()
plt.show()




plt.hist(df2.query('amplitude_ch5>20').tof_ch5, bins=400)
plt.show()