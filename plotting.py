import matplotlib
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import numpy as np
import pandas as pd
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters() # suppresses warning

def plot_samples(samples, channel, run):
    # plot events with certain characteristics
    fig, ax = plt.subplots()
    ax.set_title(f"run {run}, ch{channel}")
    for s in samples:
        ax.plot(range(len(s)), s)
    ax.set_xlabel("time [sample]")
    ax.set_ylabel("V [ADC]")
    #ax.set_xlim(left=50,right=200)
    ax.grid(True)
    return fig, ax

def plot_baseline(arr, channel, run):
    # plot baseline
    fig, ax = plt.subplots()
    plt.hist(arr, bins=40)
    ax.set_title(f"run {run}, channel {channel}")
    ax.set_xlabel("baseline [ADC]")
    ax.set_ylabel("counts")
    ax.set_yscale('log')
    return fig, ax

def plot_ph(arr, channel, run):
    # plot PH
    fig, ax = plt.subplots()
    plt.hist(arr, bins=150)
    ax.set_title(f"run {run}, channel {channel}")
    ax.set_xlabel("PH [ADC]")
    ax.set_ylabel("counts")
    ax.set_yscale('log')
    return fig, ax

def plot_minval(arr, channel, run):
    # plot PH
    fig, ax = plt.subplots()
    plt.hist(arr, bins=150)
    ax.set_title(f"run {run}, channel {channel}")
    ax.set_xlabel("minimum pulse value [ADC]")
    ax.set_ylabel("counts")
    ax.set_yscale('log')
    return fig, ax

def plot_maxval(arr, channel, run):
    # plot PH
    fig, ax = plt.subplots()
    plt.hist(arr, bins=150)
    ax.set_title(f"run {run}, channel {channel}")
    ax.set_xlabel("maximum pulse value [ADC]")
    ax.set_ylabel("counts")
    ax.set_yscale('log')
    return fig, ax

def plot_siglen(arr, channel, run):
    # plot signal length
    fig, ax = plt.subplots()
    plt.hist(arr, bins=150)
    #ax.set_yscale("log")
    ax.set_title(f"run {run}, channel {channel}")
    ax.set_xlabel("CFD_drop - CFD_rise [samples]")
    ax.set_ylabel("counts")
    ax.set_yscale('log')
    return fig, ax

def plot_risetime(arr, channel, run):
    # plot signal length
    fig, ax = plt.subplots()
    plt.hist(arr, bins=150)
    #ax.set_yscale("log")
    ax.set_title(f"run {run}, channel {channel}")
    ax.set_xlabel("peak_idx - CFD_rise [samples]")
    ax.set_ylabel("counts")
    ax.set_yscale('log')
    return fig, ax

def plot_qdc(arr, channel, run):
    # plot QDC
    fig, ax = plt.subplots()
    plt.hist(arr, bins=500)
    #ax.set_yscale("log")
    ax.set_title(f"run {run}, channel {channel}")
    ax.set_xlabel("QDC [a.u.]")
    ax.set_ylabel("counts")
    ax.set_yscale('log')
    return fig, ax

def plot_qdcps(arr, channel, run):
    # plot QDC
    fig, ax = plt.subplots()
    plt.hist(arr, bins=500)
    #ax.set_yscale("log")
    ax.set_title(f"run {run}, channel {channel}")
    ax.set_xlabel("PS FoM [a.u.]")
    ax.set_ylabel("counts")
    ax.set_yscale('log')
    return fig, ax

def plot_qdcps2d(psarr, qdclgarr, channel, run):
    # plot PS versus QDC
    fig, ax = plt.subplots()
    plt.hist2d(psarr, qdclgarr, bins=100)
    #ax.set_yscale("log")
    ax.set_title(f"run {run}, channel {channel}")
    ax.set_xlabel("PS [a.u.]")
    ax.set_ylabel("QDC_lg [a.u.]")
    return fig, ax


def persistantTracePlot(samples, gridsize=250):
    ''' show "persistent" trace picture '''
    fig, ax = plt.subplots()
    time = np.concatenate([range(len(s)) for s in samples])
    ampl = np.concatenate([s for s in samples])
    plt.xlabel('time')
    plt.ylabel('ampl')
    plt.hexbin(time, ampl, bins='log', gridsize=gridsize)
    plt.title("persistent trace in 2D hex binning")
    cb = plt.colorbar()
    cb.set_label('log10(N)')
    # adjust axis range
    x1,x2,y1,y2 = plt.axis()
    plt.axis((x1,x2,1.1*y1,1.1*y2))
    return fig, ax

def plot_evtrate(global_ts, counts, channel, run):
    """ plots the number of events per second as function of the global time stamp. Expected input from a DataFrame grouped by global time stamp (i.e. g = df.groupby(df.global_ts).count().compute())"""
    fig, ax = plt.subplots()
    # time in each data block bin (do not take first bin as reference is missing)
    tpassed = (global_ts - global_ts.iloc[0]).astype(np.int64).to_numpy()[1:]/1e9
    # events per second for each time bin (remove the last as reference time is missing)
    tdiff = ((global_ts - global_ts.iloc[0]).astype(np.int64)/1e9).diff().to_numpy()[1:]
    evtspers = counts.to_numpy()[:-1]/tdiff
    # -> plot the two!
    ax.plot(tpassed, evtspers, marker="o")
    plt.xlabel("time [s]")
    plt.ylabel("events/second")
    ax.set_title(f"Run {run}, ch {channel}, first data {global_ts.iloc[0].strftime('%b %d %Y %H:%M:%S')}")
    return fig, ax

def plot_evtvstime(global_ts, counts, channel, run):
    """ plots the collected number of events as function of time. Expected input from a DataFrame grouped by global time stamp (i.e. g = df.groupby(df.global_ts).count().compute())"""
    # integrated events versus time
    fig, ax = plt.subplots()
    integral = np.array([counts.to_numpy()[:idx+1].sum() for idx in range(len(counts))])
    # plot against time
    ax.plot(global_ts, integral, marker="o")
    plt.ylabel("# of collected events")
    fig.autofmt_xdate()
    # show more precise dates when hovering over plot with mouse:
    ax.fmt_xdata = mdates.DateFormatter('%Y-%m-%d %HH:%MM')
    ax.set_title(f"Run {run}, ch {channel}, first data {global_ts.iloc[0].strftime('%b %d %Y %H:%M:%S')}")
    return fig, ax

def plot_valvstime(global_ts, val, channel, run):
    """ plots the given value in bins of global time stamps. Expected input from a DataFrame grouped by global time stamp (i.e. g = df.groupby(df.global_ts).count().compute())"""
    # integrated events versus time
    fig, ax = plt.subplots()
    # plot against time
    ax.plot(global_ts, val, marker="o")
    fig.autofmt_xdate()
    # show more precise dates when hovering over plot with mouse:
    ax.fmt_xdata = mdates.DateFormatter('%Y-%m-%d %HH:%MM')
    ax.set_title(f"Run {run}, ch {channel}, first data {global_ts.iloc[0].strftime('%b %d %Y %H:%M:%S')}")
    return fig, ax
