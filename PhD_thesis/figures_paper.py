#!/usr/bin/env python3
from re import I
import sys
from tkinter import X
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
from numpy.core.arrayprint import SubArrayFormat
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit
from scipy import interpolate

# from matplotlib.colors import LinearSegmentedColormap


# IMPORT FROM LIBRARY DIRECTORY
sys.path.insert(0, "../library/")
sys.path.insert(0, "../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "../")
sys.path.insert(0, "data_analysis/")

import processing_math as promath
import processing_data as prodata
import processing_pd as propd
import processing_simulation as prosim
import figure_style as fs

# ##########################################################
# ######### Fundamental parameters #########################
# ##########################################################
pathSim = '/media/gheed/Seagate_Expansion_Drive1/data/simulation'
pathData = '/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper'

neutron_color = '#5b7fe2' #blue
gamma_color = '#ff7860' #red

NE213A_color = '#00b5c5' #aqua
EJ305_color = '#0fa10f' #green
EJ321P_color = '#eb4402' #red
EJ331_color = '#b72cab' #purple

edepHigh = 1.0004
edepLow = 0.9996



# ##########################################################
# ######### Figure 1.1 #####################################
# ######### Neutron interaction cross sections #############
# ##########################################################
#Data take from: https://www.oecd-nea.org/janisweb/book/neutrons/ (NDF)



# hydrogen_abs = pd.read_csv('/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/PhD_thesis/data/ENDFB_VIII.0_H1_absorption.csv', names=['energy','cross_section'], sep=',')
helium_abs = pd.read_csv('/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/PhD_thesis/data/ENDFB_VIII.0_He3_absorption.csv', names=['energy','cross_section'], sep=',')
lithium_abs = pd.read_csv('/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/PhD_thesis/data/ENDFB_VIII.0_Li6_absorption.csv', names=['energy','cross_section'], sep=',')
boron_abs = pd.read_csv('/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/PhD_thesis/data/ENDFB_VIII.0_B10_absorption.csv', names=['energy','cross_section'], sep=',')
# cadmium_abs = pd.read_csv('/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/PhD_thesis/data/ENDFB_VIII.0_Cd113_absorption.csv', names=['energy','cross_section'], sep=',')
gadolinium_abs = pd.read_csv('/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/PhD_thesis/data/ENDFB_VIII.0_Gd157_absorption.csv', names=['energy','cross_section'], sep=',')

helium_el = pd.read_csv('/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/PhD_thesis/data/ENDFB_VIII.0_He3_elastic.csv', names=['energy','cross_section'], sep=',')
lithium_el = pd.read_csv('/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/PhD_thesis/data/ENDFB_VIII.0_Li6_elastic.csv', names=['energy','cross_section'], sep=',')
boron_el = pd.read_csv('/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/PhD_thesis/data/ENDFB_VIII.0_B10_elastic.csv', names=['energy','cross_section'], sep=',')

hydrogen_scatter = pd.read_csv('/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/PhD_thesis/data/ENDFB_VIII.0_H1_total.csv', names=['energy','cross_section'], sep=',')
helium_scatter = pd.read_csv('/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/PhD_thesis/data/ENDFB_VIII.0_He4_total.csv', names=['energy','cross_section'], sep=',')
carbon_scatter = pd.read_csv('/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/PhD_thesis/data/ENDFB_VIII.0_C12_total.csv', names=['energy','cross_section'], sep=',')


fs.set_style(textwidth=345)
# plt.plot(hydrogen_abs.energy, hydrogen_abs.cross_section, lw=1, label='$^1$H')
# plt.plot(cadmium_abs.energy, cadmium_abs.cross_section, lw=1, label='$^{113}$Cd')
# plt.plot(gadolinium_abs.energy, gadolinium_abs.cross_section, lw=1, label='$^{157}$Gd')
# plt.plot(hydrogen_scatter.energy, hydrogen_scatter.cross_section, lw=1.5, ls='dashed', label=r'$^{1}\textrm{H}$')
# plt.plot(helium_scatter.energy, helium_scatter.cross_section, lw=1.5, ls='dashed', label=r'$^{4}\textrm{He}$')
# plt.plot(carbon_scatter.energy, carbon_scatter.cross_section, lw=1.5, ls='dashed', label=r'$^{12}\textrm{C}$')

plt.plot(helium_abs.energy, helium_abs.cross_section, lw=1., color='black', label=r'$^3\textrm{He}(n,p)$')
plt.plot(lithium_abs.energy, lithium_abs.cross_section, lw=1., color='blue', label=r'$^6\textrm{Li}(n,\alpha)$')
plt.plot(boron_abs.energy, boron_abs.cross_section, lw=1., color='green', label=r'$^{10}\textrm{B}(n,\alpha)$')

plt.plot(helium_el.energy, helium_el.cross_section, lw=1., ls='dashed', color='black', label=r'$^3\textrm{He}(n,n)$')
plt.plot(lithium_el.energy, lithium_el.cross_section, lw=1., ls='dashed', color='blue', label=r'$^6\textrm{Li}(n,n)$')
plt.plot(boron_el.energy, boron_el.cross_section, lw=1., ls='dashed', color='green', label=r'$^{10}\textrm{B}(n,n)$')

# plt.vlines(.5, 0, 10000)
# plt.vlines(100E3, 0, 10000)

plt.yscale('log')
plt.xscale('log')
plt.ylim(0.005, 70000)
plt.grid(which='both', alpha=.45)
plt.xlim(0.5E-3, 30E6)

plt.xlabel('Neutron Energy [eV]')
plt.ylabel('Cross Section [b]')
plt.legend()

# plt.savefig('/home/gheed/Documents/writing/mauritzson_PhD_thesis_2023/figures/cross_sections_absorption.pdf')

plt.show()



# ##########################################################
# ######### Figure 1.2 #####################################
# ######### Gamma-ray interaction cross sections ###########
# ########## CARBON ########################################
# ##########################################################
#Data take from: https://iopscience.iop.org/article/10.1088/0031-9155/44/1/001/meta (with plot stealer)



# hydrogen_abs = pd.read_csv('/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/PhD_thesis/data/ENDFB_VIII.0_H1_absorption.csv', names=['energy','cross_section'], sep=',')
carbon_PE       = pd.read_csv('/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/PhD_thesis/data/gamma_rays/Carbon_photelectric.csv', names=['energy','cross_section'], sep=',')
carbon_CE       = pd.read_csv('/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/PhD_thesis/data/gamma_rays/Carbon_compton_scattering.csv', names=['energy','cross_section'], sep=',')
carbon_coherent = pd.read_csv('/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/PhD_thesis/data/gamma_rays/Carbon_coherent_scattering.csv', names=['energy','cross_section'], sep=',')
carbon_PP1      = pd.read_csv('/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/PhD_thesis/data/gamma_rays/Carbon_pairproduction_1.csv', names=['energy','cross_section'], sep=',')
carbon_PP2      = pd.read_csv('/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/PhD_thesis/data/gamma_rays/Carbon_pairproduction_2.csv', names=['energy','cross_section'], sep=',')
carbon_tot      = pd.read_csv('/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/PhD_thesis/data/gamma_rays/Carbon_total.csv', names=['energy','cross_section'], sep=',')

#interpolate data to make more pretty...
xFine = np.linspace(10e-3, 10e9, 10000)
smoothin=10
PE_interpol = interpolate.splrep(carbon_PE.energy, carbon_PE.cross_section, s=smoothin)
PE = interpolate.splev(xFine, PE_interpol, der=0)

CE_interpol = interpolate.splrep(carbon_CE.energy, carbon_CE.cross_section, s=smoothin)
CE = interpolate.splev(xFine, CE_interpol, der=0)

coh_interpol = interpolate.splrep(carbon_coherent.energy, carbon_coherent.cross_section, s=smoothin)
coh = interpolate.splev(xFine, coh_interpol, der=0)

PP1_interpol = interpolate.splrep(carbon_PP1.energy, carbon_PP1.cross_section, s=smoothin)
PP1 = interpolate.splev(xFine, PP1_interpol, der=0)

PP2_interpol = interpolate.splrep(carbon_PP2.energy, carbon_PP2.cross_section, s=smoothin)
PP2 = interpolate.splev(xFine, PP2_interpol, der=0)



fs.set_style(textwidth=345)

plt.plot(carbon_PE.energy, carbon_PE.cross_section)
plt.plot(carbon_CE.energy, carbon_CE.cross_section)
plt.plot(carbon_coherent.energy, carbon_coherent.cross_section)
plt.plot(carbon_PP1.energy, carbon_PP1.cross_section)
plt.plot(carbon_PP2.energy, carbon_PP2.cross_section)
plt.plot(carbon_PP2.energy, carbon_PP2.cross_section)
plt.plot(carbon_tot.energy, carbon_tot.cross_section)

# plt.plot(xFine, PE, lw=1., color='black')
# plt.plot(xFine, CE, lw=1., color='green')
# plt.plot(xFine, coh, lw=1., color='blue')
# plt.plot(xFine, PP1, lw=1., color='red')
# plt.plot(xFine, PP2, lw=1., color='purple')


plt.yscale('log')
plt.xscale('log')
plt.grid(which='both', alpha=.45)
plt.xlim(350, 10e9)
plt.ylim(10E-3, 5E5)

plt.xlabel('Photon Energy [eV]')
plt.ylabel('Cross Section [b]')
plt.legend()

# plt.savefig('/home/gheed/Documents/writing/mauritzson_PhD_thesis_2023/figures/cross_sections_photon.pdf')

plt.show()


# ##########################################################
# ######### Figure 2.4 / 2.11 ##############################
# ######### Actinide/Be neutron spectra ####################
# # ########################################################
#1960s Pu239Be spectra https://doi.org/10.1007/BF02710435
Pu239Be_E = np.array([2.0387238490604798,2.3326381284798208,2.5247200820330686,2.633324443104468,2.8473724404009557,2.9948530544562413,3.1551952343955776,3.2531975834038755,3.4531250258838186,3.6527542867744875,3.7530161896037333,3.96080899978299,4.058572803519868,4.246175406976455,4.459978232743984,4.656400384985563,4.85699045321945,5.043817784543923,5.140740053573293,5.346200421098667,5.544279137725106,5.770817630483435,6.006818419008082,6.219083953026461,6.440043136936581,6.53060419872809,6.704331418897093,6.916881881989667,7.11925441350166,7.301980091409224,7.589142214395878,7.745197205707197,7.917552790565535,8.014773241184177,8.260183315414832,8.497734648203709,8.638840802506046,8.883243685590708,9.098119965079624,9.252505139491007,9.497332103058184,9.704767095330313,9.907497444749438,10.076877839972566,10.258761983172617])
Pu239Be_I = np.array([0.9982456983164338,2.2751420918101117,2.6763205717135006,2.905564234112305,3.1471907152879357,3.265999512970071,3.4339213032523332,3.4913046935438716,3.3196597748066377,3.127556285916387,3.0621923967007865,3.15242131733313,3.1934378515022552,3.009505366440325,2.956532578823475,3.0999413576207764,3.2515377058902457,3.0144129384304716,2.7199130635010826,2.3723327242698278,2.073844670583889,1.7835690348358928,1.3091786919436306,1.7062861648712264,2.1443190609267817,2.246703022733033,1.9440984065507183,1.8051996242911978,1.801314980808702,1.8383267705774289,2.096394653272792,2.2479454460216775,1.851231407135486,1.577190102359113,1.1928257509620508,0.8248199728654759,0.7840477819431166,0.6083567046958631,0.6290347696298735,0.6660175695218662,0.7972009031589034,0.8628795396076265,0.8835451803087508,0.5604778525624567,0.26197323323266986])

#1960s Pu238Be spectra Kozlov et.al. https://link.springer.com/content/pdf/10.1007/BF01162326.pdf?pdf=button
# Pu238Be_E = np.array([1.5617695124976292,2.5811032641209346,3.026487239723929,3.519902820539011,3.9994309310842953,4.521212408620665,5.025133877110298,5.514051624764713,6.014821326951981,6.538244349437498,6.996169728451988,7.535811215035093,8.03635110092948,8.532984109844895,8.980140953992969,9.554222053609212,10.011884785431837,10.548144689419699,10.999471057738612])
# Pu238Be_I = np.array([3.1054922446120843,4.939119840077042,6.875521281718296,5.973691506281645,5.706839041045919,6.248459866049932,6.607167350035748,4.9644200604088535,2.9849663665679307,3.2860461383567037,3.384713933433528,4.109248099455735,3.563470153064946,2.1901799133264257,1.0667970174952206,0.9447798870617072,1.081934250652969,0.9019829862985711,0.5676258153006577])

# Pu238Be_E_smooth = np.array([1.5522157208935843,1.6476223133381,1.7523857119927628,1.7711649862110224,1.8757970612697534,1.9518990851122817,2.0372265915690253,2.0939583850116,2.141136386850131,2.2827360541636876,2.3486933302204775,2.3955430230691785,2.4613689755300365,2.4984678913807947,2.544857951643734,2.572304583193498,2.5944490245575134,2.6166919586184756,2.6349459384530087,2.634092335079451,2.6713882363241064,2.68898559817898,2.7163993988297612,2.7436818758846107,2.7904659069353457,2.8374469233799777,2.85570090321451,2.921789502867232,2.9690988283016946,3.035844045934076,3.0836130039542997,3.131775932762319,3.1607656165642837,3.2383778617600276,3.3350976901638623,3.4608728641676283,3.548104562765383,3.625388498971298,3.7410845869872915,3.799063954591219,3.856649351407351,3.9044183094275757,3.9615440736579464,4.028026644098464,4.132133424773468,4.359717216523427,4.549348489049071,4.720069163760525,4.843677498431413,4.967614142092131,5.0441101367224555,5.101958180730452,5.13121051172428,5.209151065909854,5.405545503625991,5.640352093152205,5.718555294529644,5.768097121094948,5.837009178060206,5.915540688427474,5.983533480221209,6.08993842382502,6.185870310653263,6.271854435089666,6.386171625348373,6.443034742386879,6.500029183021318,6.595304451869903,6.719503742722484,6.881721214597347,6.976996483445932,7.062717960690471,7.109830300731035,7.185407030189836,7.270274904060818,7.384001138137831,7.488239242408767,7.602293785475611,7.6787897801059355,7.745928968526112,7.823212904732028,7.919670085943999,7.997216669341777,8.142493397341429,8.27893861351466,8.454386937679661,8.649665124830376,8.940743875213402,9.037529365415203,9.1528314826434,9.27722775888988,9.353723753520203,9.506387433791023,9.591911925641664,9.677567741088238,9.830297083157022,10.011753461835905,10.145572206090499,10.269771496943081,10.432776910393535,10.614889907052081,10.758787737294444,10.835809026308496,10.989326309952872])
# Pu238Be_I_smooth = np.array([3.1054411743247767,3.125195161455065,3.173865145258488,3.2220754964761493,3.2899887645367922,3.3385055374782944,3.4351815913501516,3.5220827922314797,3.608932922825499,3.859861672478951,3.9949221542906335,4.129880495527702,4.284184261596603,4.447956458932193,4.650266295069528,4.828419885310726,5.083521077437147,5.3241898063706525,5.449373294617191,5.5744546422891155,5.709361913238878,5.930761822771511,6.1137262340770135,6.315933929639735,6.46051391300541,6.576228969985262,6.7014124582318,6.817229655786263,6.884836502123065,6.904437278391431,6.904692629827966,6.847218128492843,6.799263128711715,6.626482132695197,6.453803277253294,6.2235477799016525,6.0411962120438325,5.916523426670363,5.763189996060292,5.667279996498037,5.62909984970744,5.629355201143974,5.658526549253644,5.716613894036449,5.861500299125968,6.112888681365181,6.325586213940729,6.509316679555834,6.596575372448309,6.6357258546977365,6.626512774867582,6.549846059562546,6.463404491266981,6.242515284607415,5.464183677936176,4.657191426028336,4.397815650854333,4.138286664818408,3.8403732508426587,3.5328892650256076,3.3696788408503924,3.1778077714385757,3.120588621539987,3.121048254125749,3.1697693082164795,3.2374272248405873,3.285841857207476,3.3248391285949825,3.325503042329972,3.355236163600017,3.3942334349875236,3.4331796360877234,3.5296514088103534,3.6551413187807325,3.819168867552857,3.9544847008010735,4.080127821633373,4.167335444238541,4.158122364408387,4.119993287905096,3.9953205025316265,3.861128215604161,3.697968861716253,3.410085652167568,3.016313308916871,2.5072855412720862,1.8925218507872108,1.2397822946609658,1.0574817970904524,0.9618782192520383,0.9336772066011996,0.9244641267710438,0.9541461777537821,1.0219573052398099,1.0705251484686205,1.0905855573227479,1.101177534910188,1.092270876803874,1.0929347905388633,1.0072082062655943,0.9215837625669376,0.8357550377190535,0.7495688208600235,0.6541695241708361])

Pu238Be_E = np.array([2.6127539683114143,3.06592318883589,3.5783239358121564,4.078950035104991,4.578794415049547,5.091097447107279,5.593905846247386,6.097105105061635,6.587585138646614,7.075606013448471,7.587615900750598,8.068585015598199,8.559602481235121,9.080781285059754,9.571098460447176,10.541814746990747])
Pu238Be_I = np.array([6.562830692617821,12.897066381001327,11.374751188864845,11.329556229507013,12.311887924607515,10.918013564278322,8.004306694557643,4.576836497607795,3.8680812409070846,6.391753584689882,5.3832017197825675,3.1760207589915836,1.7608409273507704,2.7002576778592484,2.205570474170692,0.252940495233684])

Pu238Be_E_smooth = np.array([2.6021356138306424,2.6338766765346673,2.6762523795392203,2.7186117967240175,2.728953292268941,2.7498805706551246,2.7707427057622853,2.823687905788343,2.8339805438739987,2.8656076058397337,2.8652493178051066,2.8756070991697857,2.8965832350152367,2.9601305037023105,3.0236289149301165,3.066004617934669,3.119145247797796,3.151016597059867,3.22554050826234,3.289478636623552,3.3321149127441974,3.374865189603133,3.4175828948225577,3.460430886600028,3.4925465231584365,3.524727302995868,3.567542723133827,3.6103255716322744,3.6530758484912105,3.717013976852422,3.876826726115942,3.972652489558981,4.025809405241863,4.110870241826329,4.174645511989984,4.259641205295426,4.323400189639324,4.440381232945128,4.5042379322075625,4.621381833710925,4.717272740432987,4.7812760120732225,4.845295569533214,4.920063768032023,4.984164754590793,5.048330884428586,5.1338477239662135,5.219397135143353,5.347721251909061,5.444043732854652,5.529593144031792,5.615207698487953,5.668804331304242,5.733100747700083,5.818731587976,5.904411285711186,6.000725623746899,6.065005754322981,6.118569815499759,6.182687087878286,6.225486222196489,6.278903710995464,6.396112755777849,6.481287593100606,6.534509652062511,6.587617710286126,6.630091128209214,6.683166614793317,6.693687254355554,6.767771448424621,6.820602647712387,6.894849699979012,6.979812821644943,7.075540870169448,7.139446426891148,7.224702693312683,7.2993243194336905,7.363327591073926,7.406029010473594,7.470129997032365,7.5769812604500695,7.67314902610798,7.780049146984955,7.929699544720864,8.057868946198896,8.196803274535496,8.314272892433973,8.410294085714082,8.452995505113751,8.570237121535648,8.676697525279215,8.719236086481326,8.804248065606524,8.857339838010382,8.953019029075621,9.080764999239998,9.176639620142307,9.315166802985015,9.432408419406912,9.592481741786523,9.720537142526261,9.816590907445882,9.8913916775842,9.9661273044435,10.072815709663647,10.222075247725419,10.339316864147316,10.488527544749818,10.573767525351597])
Pu238Be_I_smooth = np.array([6.520067749010186,6.798204216941597,7.097696823179428,7.418596234718475,7.825274868446768,8.317530056385127,8.895412465528349,9.301888431277462,9.772787480909402,10.200771585949319,10.671721302576056,11.056993131003136,11.48502790303785,11.955673617695812,12.490539748257419,12.79003235449525,12.939626656629779,13.046508682151467,13.088967623790328,13.045850011219123,13.002833732637509,12.809969816947389,12.659919511859698,12.338614764362285,12.124394710365745,11.824547435164344,11.546056298269363,11.310378771976811,11.11751485628669,11.074397243715485,11.009416822889902,11.05177443053917,11.179961927372485,11.372217839125057,11.543168279566004,11.821051412523435,12.013408658265599,12.248326179636212,12.312242593571085,12.333092061929547,12.289822448373954,12.161077614597888,12.010925975520607,11.732282837641236,11.475097172057879,11.132284285269659,10.725149648588202,10.275201401304315,9.60027903037848,8.989729076340687,8.5397808290568,8.00420536056805,7.554409114268552,7.040341785070609,6.483359511280645,5.862156821587035,5.262310270199848,4.76964974630312,4.362667110606052,4.084074639721477,3.826990308127712,3.6126689201415783,3.5478911672951803,3.590299441939244,3.6328597175676975,3.825267630304655,3.9963194047351944,4.231540928074583,4.402744703489509,5.0231873882611815,5.5795109911188,5.985885622878321,6.3065823664381835,6.477380805894743,6.47707680392597,6.4124510520639575,6.326469161895527,6.19772432811946,6.069080828332986,5.811895162749627,5.361845581476148,4.954660277799896,4.440390280622772,3.7332563677555264,3.2616986471912455,2.640242622523653,2.232955984857808,2.0184319288924932,1.8897884291060176,1.7821970656571882,1.8459108116128782,1.9313353648385547,2.1878116924947726,2.4016264105329466,2.6366452658931525,2.721664483160465,2.6998016749060856,2.6135157827688804,2.505924419320051,2.098435113675025,1.7767250302192448,1.5193873636515,1.1979306151697013,0.962101087892762,0.7261195596314369,0.5327489739933551,0.4251576105445256,0.29600744081009545,0.2527884942492964])




#ISO STANDARD (AmBe)
ISO_E = np.array([0.10146287985199853,0.2980574874096402,0.4898831751687278,0.7186268799890372,0.93544828531296,1.164685326664154,1.3796156086196851,1.571770187399363,1.7830004453732564,1.9924218027339067,2.230374456130734,2.442139162013086,2.6370482030902047,2.8481962383089523,3.0482442015827877,3.277645688444276,3.4993182363219018,3.6934461612251197,3.9132275857343513,4.147726883414984,4.364753845626776,4.5802185754907665,4.77068758778992,4.980766727191751,5.203384836753571,5.414080646819006,5.655609990064752,5.854219055123506,6.100023981636918,6.283956284901844,6.505628832779474,6.721463565041626,6.936846072150466,7.356552125800816,7.561369008873205,7.786741580732469,8.025310904792903,8.241515639453217,8.43042241940457,8.665291719483367,8.886882044605844,9.094782280996267,9.325499331940113,9.501743807598753,9.725225256089624,9.945088903354003,10.159279180513195,10.391270684161828,10.569981842474904,10.79642331015107,11.010860255575722])
ISO_I = np.array([0.014375895028949266,0.03333536606255782,0.03132258727602864,0.028129466579875986,0.025005721333379017,0.021395936825516465,0.019869402857240745,0.01757884819623831,0.019177292815786773,0.022303504744938165,0.021332659563534216,0.022479718387063627,0.02286279077734764,0.02953067936551441,0.035573503717153725,0.03682483127205455,0.034603891877076985,0.030646681969234993,0.030022953852478676,0.026968755353043963,0.02867140018500121,0.03169348042070644,0.030826527116379464,0.033397187296584335,0.030377642262496153,0.02742747267806366,0.023435814861762997,0.020693908664222826,0.01809116447976978,0.01774500668059887,0.0205240672856213,0.018233649662544114,0.01632517386686766,0.016848436054678145,0.01886351022645518,0.01851759224365344,0.017025917297612124,0.014423001815752519,0.009875483915173545,0.006508787556956386,0.00435729213059715,0.0037682174791873677,0.003908441536195149,0.005055294802836688,0.006306588098256197,0.005613416012881574,0.004711877762170691,0.0037757203055945737,0.002839254513686676,0.0015905649388468293,0.0004806947822810076])

#Schertzinger AmBe
Julius_AmBe_E = np.array([1.009966777408638,1.2093023255813955,1.408637873754153,1.6079734219269104,1.8006644518272432,2.013289036544851,2.1993355481727583,2.4186046511627906,2.6378737541528245,2.8039867109634553,3.0232558139534884,3.2093023255813966,3.4219269102990033,3.8006644518272417,4.006644518272426,4.205980066445183,4.39202657807309,4.624584717607974,4.803986710963455,5.009966777408639,5.215946843853821,5.408637873754154,5.607973421926912,5.807308970099673,6.019933554817282,6.212624584717612,6.411960132890372,6.591362126245846,6.81063122923588,7.02990033222591])
Julius_AmBe_I = np.array([0.000735845055488793,0.0011596098112674064,0.002221672439386445,0.004560330812186333,0.007271388515821965,0.010833333333333337,0.013650832920996208,0.014925484790650551,0.016200136660304898,0.021145472538347358,0.021303103131405956,0.02470570910204755,0.02231020711104828,0.021721743125751043,0.019911406423034333,0.020707511604344857,0.01793990480902901,0.017671885676586323,0.02064901863763813,0.018625915977474615,0.014528345232204712,0.015430892297542471,0.012024869819278527,0.01069331542612097,0.007659515562781279,0.004998232840884992,0.00340072100091892,0.0018033858768643496,0.0008971866826889136,0.0006824768502155937])

#Schertzinger PuBe
Julius_PuBe_E = np.array([0.8125372625841067,1.2632654799420833,1.463589132101184,1.6608466059109102,1.915339408908952,2.1606336768588705,2.385486755812963,2.623626607614343,2.8372370326207306,3.0549356954262836,3.2746784771314204,3.5199727450813385,3.6732816625500386,3.8531641257133127,4.0529767481475165,4.2323481815858965,4.450557874116344,4.618686653607018,4.832808108338302,5.046929563069586,5.2206796695341104,5.3913635976492635,5.594753428157738,5.758282940124353,5.924367600715442,6.0950515288305915,6.260625159696788,6.4476620390086055,6.626522442722086,6.8370666893791014,7.033302103739036])
Julius_PuBe_I = np.array([0.00040976360469793804,0.002940241828664064,0.005903979276863591,0.01098984108150336,0.016922485014200156,0.02646288132656266,0.03727721013013635,0.0398152069523538,0.04553709517781779,0.04934902449124641,0.05634390538314013,0.06036706031619231,0.053571187321763705,0.049532995192208856,0.04825271888372311,0.0441084440319658,0.048026456067596895,0.042927671672212606,0.045997021930223855,0.049066372188235216,0.04237587704742292,0.035048885573395916,0.035890498665155146,0.027078349080293357,0.02155523379609911,0.015607552666899643,0.008599044315675403,0.0060460102969553675,0.0031749630678230834,0.0013638032359341856,0.002099568561686277])


#without tagged neutron data
fs.set_style(textwidth=345, ratio=.45)
plt.scatter(Pu238Be_E, Pu238Be_I/np.sum(Pu238Be_I), label='Kozlov et al.', s=10, color='black', zorder=1, marker='s')
# plt.errorbar(Pu238Be_E, Pu238Be_I/np.sum(Pu238Be_I), yerr=Pu238Be_I/np.sum(Pu238Be_I)*0.04, color='black', zorder=1, ls='none')
plt.plot(Pu238Be_E_smooth, Pu238Be_I_smooth/np.sum(Pu238Be_I_smooth)*7.5, color='black', zorder=1, ls='solid')
# plt.step(Julius_AmBe_E, Julius_AmBe_I*1.49, label='Scherzinger et al.', lw=1, color='black', zorder=1)
# plt.fill_between(Julius_AmBe_E, Julius_AmBe_I*1.49, color='tomato', step='pre', zorder=0)
# plt.step(Julius_PuBe_E, Julius_PuBe_I/np.sum(Julius_PuBe_I)*0.64, lw=1, color='black', zorder=1)
# plt.fill_between(Julius_PuBe_E, Julius_PuBe_I/np.sum(Julius_PuBe_I)*0.64, color=neutron_color, step='pre', zorder=0, label='Scherzinger et al.')

plt.xlabel('Energy [MeV]')
plt.ylabel('Intensity [arb. units]')
# plt.yticks([])
plt.ylim([0, 0.155])
plt.xlim([0, 12])
plt.legend()
# 
plt.savefig('/home/gheed/Documents/writing/mauritzson_PhD_thesis_2023/figures/PuBe_source_spectra_1.pdf')
# 
plt.show()


#with tagged neutron data

fs.set_style(textwidth=345, ratio=.45)
plt.scatter(Pu238Be_E, Pu238Be_I/np.sum(Pu238Be_I), label='Kozlov et al.', s=10, color='black', zorder=1, marker='s')
plt.plot(Pu238Be_E_smooth, Pu238Be_I_smooth/np.sum(Pu238Be_I_smooth)*7.5, color='black', zorder=1, ls='solid')
# plt.step(Julius_AmBe_E, Julius_AmBe_I*1.49, label='Scherzinger et al.', lw=1, color='black', zorder=1)
# plt.fill_between(Julius_AmBe_E, Julius_AmBe_I*1.49, color='tomato', step='pre', zorder=0)
plt.step(Julius_PuBe_E, Julius_PuBe_I/np.sum(Julius_PuBe_I)*2.15, lw=1, color='black', zorder=1)
plt.fill_between(Julius_PuBe_E, Julius_PuBe_I/np.sum(Julius_PuBe_I)*2.15, color=neutron_color, step='pre', zorder=0, label='Scherzinger et al.')

plt.xlabel('Energy [MeV]')
plt.ylabel('Intensity [arb. units]')
# plt.yticks([])
plt.ylim([0, 0.155])
plt.xlim([0, 12])
plt.legend()

plt.savefig('/home/gheed/Documents/writing/mauritzson_PhD_thesis_2023/figures/PuBe_source_spectra_2.pdf')
# 
plt.show()



# ##########################################################
# ######### Figure 2.10 ####################################
# ######### TOF calibration ################################
# ##########################################################
path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop
path = '/media/gheed/backup_data_8TB/EJ321P_char/cooked/'
#Load data
TOF_raw = propd.load_parquet_merge(path, np.arange(2121, 2121+10, 1), keep_col=['amplitude_ch2','tof_ch2', 'amplitude_ch3','tof_ch3', 'amplitude_ch4','tof_ch4', 'amplitude_ch5','tof_ch5', 'qdc_lg_ch1'], full=False)


#REMOVE ME!
# numBins = np.arange(-850, 300, 1)
# thr = 5

# plt.subplot(2,2,1)
# y,x = np.histogram(TOF_raw.tof_ch2, bins=numBins)
# x=prodata.getBinCenters(x)
# plt.step(x,y, label='ch2')
# y,x = np.histogram(TOF_raw.query(f'amplitude_ch2>{thr}').tof_ch2, bins=numBins)
# x=prodata.getBinCenters(x)
# plt.step(x,y, label=f'ch2 thr={thr}')
# plt.legend()

# plt.subplot(2,2,2)
# y,x = np.histogram(TOF_raw.tof_ch3, bins=numBins)
# x=prodata.getBinCenters(x)
# plt.step(x,y, label='ch3')
# y,x = np.histogram(TOF_raw.query(f'amplitude_ch3>{thr}').tof_ch3, bins=numBins)
# x=prodata.getBinCenters(x)
# plt.step(x,y, label=f'ch3 thr={thr}')
# plt.legend()

# plt.subplot(2,2,3)
# y,x = np.histogram(TOF_raw.tof_ch4, bins=numBins)
# x=prodata.getBinCenters(x)
# plt.step(x,y, label='ch4')
# y,x = np.histogram(TOF_raw.query(f'amplitude_ch4>{thr}').tof_ch4, bins=numBins)
# x=prodata.getBinCenters(x)
# plt.step(x,y*5.74, label=f'ch4 thr={thr}')
# plt.legend()

# plt.subplot(2,2,4)
# y,x = np.histogram(TOF_raw.tof_ch5, bins=numBins)
# x=prodata.getBinCenters(x)
# plt.step(x,y, label='ch5')
# y,x = np.histogram(TOF_raw.query(f'amplitude_ch5>{thr}').tof_ch5, bins=numBins)
# x=prodata.getBinCenters(x)
# plt.step(x,y, label=f'ch5 thr={thr}')
# plt.legend()

# plt.show()



#Make figures
numBins = np.arange(-870, 200, 1)

fs.set_style(textwidth=345)

y, x = np.histogram(TOF_raw.query('amplitude_ch2>35').tof_ch2, bins=numBins)
x = prodata.getBinCenters(x)
plt.step(x, y, color='black', lw=1, zorder=3)

#random fill
y, x = np.histogram(TOF_raw.query('amplitude_ch2>35 and tof_ch2>-710 and tof_ch2<200').tof_ch2, bins=numBins)
x = prodata.getBinCenters(x)
plt.fill_between(x, y, where=(y > 0), color='grey', alpha=0.60, step='pre', zorder=1)

#gamma fill
y, x = np.histogram(TOF_raw.query('amplitude_ch2>35 and tof_ch2>61 and tof_ch2<71').tof_ch2, bins=numBins)
x = prodata.getBinCenters(x)
plt.fill_between(x, y1=y, y2=103, where=y>103, color=gamma_color, step='pre', zorder=2)

#neutron fill
y, x = np.histogram(TOF_raw.query('amplitude_ch2>35 and tof_ch2>89 and tof_ch2<150').tof_ch2, bins=numBins)
x = prodata.getBinCenters(x)
plt.fill_between(x, y1=y, y2=103, where=y>103, color=neutron_color, step='pre', zorder=2)

plt.vlines(61.9, 0, 1200, color='black', ls='dashed', lw=1)

plt.xlabel('Pulse Time Differences [arb. units]')
plt.ylabel('Counts')
plt.xlim([45,155])
plt.ylim([0, 1400])

plt.savefig('/home/gheed/Documents/writing/mauritzson_PhD_thesis_2023/figures/TOF_uncalibrated.pdf')

plt.show()



# ##########################################################
# ######### Figure 2.11 ####################################
# ######### TOF energy slice ###############################
# ##########################################################

path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop
path = '/media/gheed/backup_data_8TB/EJ321P_char/cooked/' #path on Nicholais laptop
distance = 0.959

#Load data
TOF_cal = propd.load_parquet_merge(path, np.arange(2121, 2121+10, 1), keep_col=['amplitude_ch2','tof_ch2'], full=False)

#calibrate TOF
flashrange = {'tof_ch2':[60, 73]} #gamma flash fitting ranges
prodata.tof_calibration(TOF_cal, flashrange, distance, phcut=125, calUnit=1e-9, energyCal=True)

#define Neutron energy bin cuts in time (s)
t1_start = prodata.EnergytoTOF(5+0.125 ,distance)
t1_stop = prodata.EnergytoTOF(5-0.125 ,distance)

t2_start = prodata.EnergytoTOF(3+0.125 ,distance)
t2_stop = prodata.EnergytoTOF(3-0.125 ,distance)

#Make figures
numBins = np.arange(-870, 200, 1)

fs.set_style(textwidth=345)

y, x = np.histogram(TOF_cal.query('amplitude_ch2>35').tof_ch2*1e9, bins=numBins)
x = prodata.getBinCenters(x)
plt.step(x, y, color='black', lw=1)

#random fill
y, x = np.histogram(TOF_cal.query('amplitude_ch2>35 and tof_ch2>-710e-9 and tof_ch2<200e-9').tof_ch2*1e9, bins=numBins)
x = prodata.getBinCenters(x)
plt.fill_between(x, y, color='grey', alpha=0.6, step='pre')

#gamma fill
y, x = np.histogram(TOF_cal.query('amplitude_ch2>35 and tof_ch2>-2e-9 and tof_ch2<5.8e-9').tof_ch2*1e9, bins=numBins)
x = prodata.getBinCenters(x)
plt.fill_between(x, y1=y, y2=97, where=y>97, color=gamma_color, step='pre')

#neutron fill
y, x = np.histogram(TOF_cal.query('amplitude_ch2>35 and tof_ch2>25e-9 and tof_ch2<92e-9').tof_ch2*1e9, bins=numBins)
x = prodata.getBinCenters(x)
plt.fill_between(x, y1=y, y2=97, where=y>97, color=neutron_color, step='pre')

plt.vlines(t1_start*1e9, ymin=97, ymax = 2000, lw=0.5, color='purple')
plt.vlines(t1_stop*1e9, ymin=97, ymax = 2000, lw=0.5, color='purple')

plt.vlines(t2_start*1e9, ymin=97, ymax = 2000, lw=0.5, color='purple')
plt.vlines(t2_stop*1e9, ymin=97, ymax = 2000, lw=0.5, color='purple')

plt.xlabel('Time-of-Flight [ns]')
plt.ylabel('Counts')
plt.xlim([-17,93])
plt.ylim([0, 1400])
# plt.savefig('/home/gheed/Documents/writing/mauritzson_PhD_thesis_2023/figures/TOF_calibrated.pdf')
plt.show()



#Make inset figure
numBins = np.arange(-870, 200, 4)

fs.set_style(textwidth=345/3, ratio=.7)

y, x = np.histogram(TOF_cal.query('amplitude_ch2>35').tof_ch2*1e9, bins=numBins)
x = prodata.getBinCenters(x)
plt.step(x, y/4, color='black', lw=1)

#random fill
y, x = np.histogram(TOF_cal.query('amplitude_ch2>35 and tof_ch2>-710e-9 and tof_ch2<200e-9').tof_ch2*1e9, bins=numBins)
x = prodata.getBinCenters(x)
plt.fill_between(x, y/4, color='grey', alpha=0.6, step='pre')

#gamma fill
y, x = np.histogram(TOF_cal.query('amplitude_ch2>35 and tof_ch2>-2e-9 and tof_ch2<5.8e-9').tof_ch2*1e9, bins=numBins)
x = prodata.getBinCenters(x)
plt.fill_between(x, y1=y/4, y2=97, where=y>97, color=gamma_color, step='pre')

#neutron fill
y, x = np.histogram(TOF_cal.query('amplitude_ch2>35 and tof_ch2>25e-9 and tof_ch2<92e-9').tof_ch2*1e9, bins=numBins)
x = prodata.getBinCenters(x)
plt.fill_between(x, y1=y/4, y2=97, where=y>97, color=neutron_color, step='pre')

# plt.vlines(t1_start*1e9, ymin=97, ymax = 2000, lw=0.5, color='purple')
# plt.vlines(t1_stop*1e9, ymin=97, ymax = 2000, lw=0.5, color='purple')

# plt.vlines(t2_start*1e9, ymin=97, ymax = 2000, lw=0.5, color='purple')
# plt.vlines(t2_stop*1e9, ymin=97, ymax = 2000, lw=0.5, color='purple')

plt.xlabel('Time-of-Flight [ns]')
plt.ylabel('Counts')
plt.xlim([-250,93])
plt.ylim([0, 300])
plt.savefig('/home/gheed/Documents/writing/mauritzson_PhD_thesis_2023/figures/TOF_calibrated_inset.pdf')
# 
plt.show()

# ##########################################################
# ######### Figure 2.15 ####################################
# ######### How I obtain kB optimal ########################
# ##########################################################


names = [ 'evtNum', 
        'optPhotonSum', 
        'optPhotonSumQE', 
        'optPhotonSumCompton', 
        'optPhotonSumComptonQE', 
        'optPhotonSumNPQE', 
        'xLoc', 
        'yLoc', 
        'zLoc', 
        'CsMin', 
        'optPhotonParentID', 
        'optPhotonParentCreatorProcess', 
        'optPhotonParentStartingEnergy',
        'total_edep',
        'gamma_edep',
        'proton_edep',
        'nScatters',
        'nScattersProton']


energy = '4000'
# ampCoeff =      [3.6,   3.4,  3.4,  3.5,  3.6,  3.5] #NE213A
# gainCoeff =     [1.29,  1.20, 1.05, 1.00, 0.95, 0.78] #NE213A
# smearCoeff =    [0.06,  0.08, 0.08, 0.08, 0.09, 0.12] #NE213A
ampCoeff =      [5.00,  5.10,   5.00,   5.00,   5.00,   5.00] #EJ305
gainCoeff =     [1.05,  0.95,   0.86,   0.81,   0.76,   0.63] #EJ305
smearCoeff =    [0.12,  0.12,   0.12,   0.12,   0.12,   0.13] #EJ305

#EJ305 initial parameters
detector =          'EJ305'
density =           0.893 #g/cm3
numBins =           np.arange(0, 4, 0.06)
birksFolder =       ['1516', '1260', '1100', '1000', '0911', '0683']
birksVal =          [0.1516, 0.126,  0.11,   0.1,    0.0911,  0.0683]

#LOAD SIMULATION DATA kB=0.1516
i = 0
sim1 = pd.read_csv(f'{pathSim}/{detector}/neutrons/neutron_range/5MeV_optimized/{int(energy)}keV/isotropic/birks{birksFolder[i]}/CSV_optphoton_data_sum.csv', names=names)
#Randomize binning
y, x = np.histogram(sim1['optPhotonSumQE'], bins=4096, range=[0, 4096]) 
sim1['optPhotonSumQE'] = prodata.getRandDist(x, y)
#Apply smearing
sim1['optPhotonSumQEsmear'] = sim1['optPhotonSumQE'].apply(lambda x: prosim.gaussSmear(x, smearCoeff[i]))
#Calibrate simulations
sim1['optPhotonSumQEsmearcal'] = prodata.calibrateMyDetector(f'{pathData}/{detector}/Simcal/popt.npy', sim1['optPhotonSumQEsmear'], inverse=False)
#Bin simulations
y_sim1, x_sim1 = np.histogram(sim1['optPhotonSumQEsmearcal'], bins = numBins)
x_sim1 = prodata.getBinCenters(x_sim1)

#LOAD SIMULATION DATA kB=0.0911
i = 5
sim2 = pd.read_csv(f'{pathSim}/{detector}/neutrons/neutron_range/5MeV_optimized/{int(energy)}keV/isotropic/birks{birksFolder[i]}/CSV_optphoton_data_sum.csv', names=names)
#Randomize binning
y, x = np.histogram(sim2['optPhotonSumQE'], bins=4096, range=[0, 4096]) 
sim2['optPhotonSumQE'] = prodata.getRandDist(x, y)
#Apply smearing
sim2['optPhotonSumQEsmear'] = sim2['optPhotonSumQE'].apply(lambda x: prosim.gaussSmear(x, smearCoeff[i]))
#Calibrate simulations
sim2['optPhotonSumQEsmearcal'] = prodata.calibrateMyDetector(f'{pathData}/{detector}/Simcal/popt.npy', sim2['optPhotonSumQEsmear'], inverse=False)
#Bin simulations
y_sim2, x_sim2 = np.histogram(sim2['optPhotonSumQEsmearcal'], bins = numBins)
x_sim2 = prodata.getBinCenters(x_sim2)

#LOAD DATA
energy = 4000
gate = 'LG'
detector = 'EJ305'
# numBins = np.arange(0.35, 2.5, 0.04)

nQDC       = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/nQDC_{gate}.npy')
randQDC    = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/randQDC_{gate}.npy')
nLength    = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/nLength.npy')
randLength = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/randLength.npy')
nQDC = prodata.calibrateMyDetector(f'{pathData}/{detector}/Ecal/popt_{gate}.npy', nQDC)
randQDC = prodata.calibrateMyDetector(f'{pathData}/{detector}/Ecal/popt_{gate}.npy', randQDC)

x_data, y_data = prodata.randomTOFSubtractionQDC( nQDC,
                                                randQDC,
                                                nLength, 
                                                randLength,
                                                numBins,#np.arange(0,3,0.03), 
                                                plot=False)
###################################
#Optimal kB data:
kBs = np.array(birksVal)*0.1*density#np.array([0.145, 0.126, 0.0917, 0.085, 0.060])*0.1*density
offsets = np.array([1.060259, 0.950380, 0.866949, 0.816808, 0.771099, 0.642894])#
offsets_err = np.array([0.00215, 0.002528, 0.001970, 0.002177, 0.001271, 0.002068])#
smear = np.array([0.12, 0.12, 0.12, 0.12, 0.12, 0.13])#
optimalVal = 0.13738*0.1*density
#fit optimal kB vs offsets
popt, pcov = curve_fit(promath.linearFunc, kBs, offsets)


###################################
#make figure
fs.set_style(textwidth=345)
scale1 = 4.7
scale2 = 6.5
plt.subplot(1,2,1)

plt.step(x_sim1*0.90, y_sim1*scale1, lw=1, color='royalblue', zorder=0, label='$k_B = 0.1516$', where='mid' )
# plt.errorbar(x_sim1*0.90, y_sim1*scale1, yerr=np.sqrt(y_sim1*scale1), ls='none', color='royalblue', lw=1)

plt.step(x_sim2, y_sim2*scale2, lw=1, color='tomato', zorder=0, label='$k_B = 0.0683$', where='mid' )
# plt.errorbar(x_sim2, y_sim2*scale2, yerr=np.sqrt(y_sim2*scale2), ls='none', color='tomato', lw=1)

plt.scatter(x_data, y_data, label='data', color='black', s=20, zorder=1)
# plt.errorbar(x_data, y_data, yerr=np.sqrt(y_data), ls='none', color='black', lw=1)

# plt.legend()
plt.xlim([0.46, 2.1])
plt.ylim([0, 2970])
plt.xlabel('Energy [MeV$_{ee}$]')
plt.ylabel('Counts')

#new subfigure
plt.subplot(1,2,2)

plt.scatter(kBs, offsets, color='royalblue', s=50, alpha=0.85)
# plt.errorbar(kBs, offsets, yerr=offsets_err, ls='none', color='royalblue', lw=1)

plt.plot(kBs, promath.linearFunc(kBs, popt[0], popt[1]), lw=1, ls='dashed', label='Linear fit', color='black')

plt.scatter(optimalVal, 1, color='black', marker='x', s=85, alpha=1)
# plt.legend()
plt.xlim([0.0056, 0.014])
plt.ylim([0.62, 1.1])
plt.xlabel('$k_B$ [gcm$^{-2}$MeV$^{-1}$]')
plt.ylabel('Scale factor $\Delta$')

plt.gca().yaxis.set_label_position('right')
plt.gca().yaxis.tick_right()

plt.tight_layout()
plt.subplots_adjust(hspace = .001, wspace = .001)

# plt.savefig('/home/gheed/Documents/writing/mauritzson_PhD_thesis_2023/figures/kB_optimization_explained.pdf')

plt.show()



# ##########################################################
# ######### Figure 2.9 #####################################
# ######### Pulse processing and waveform ##################
# ##########################################################

path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop
# df = propd.load_parquet(path+'run01606/', num_events=1, keep_col=['samples_ch1', 'baseline_ch1', 'CFD_rise_ch1', 'amplitude_ch1','CFD_drop_ch1', 'qdc_lg_ch1']) 
# df = df.reset_index()
# df.to_pickle(path+'run01606/data.pkl')

df_waveforms = pd.read_pickle(path+'run01606/data.pkl')

evtn = 3 #event number to use

fs.set_style(textwidth=345*1.25, ratio=0.6)
plt.plot(np.arange(0, 1001, 1), (df_waveforms.samples_ch1[evtn]-df_waveforms.baseline_ch1[evtn])*1.97, lw=1, zorder=3, color='black')
plt.hlines(-25, xmin=0, xmax=1, alpha=1)
plt.hlines(0, xmin=0, xmax=1, alpha=1)
plt.fill_between(np.arange(0, 1001, 1)[0:350], y2=(df_waveforms.samples_ch1[evtn]-df_waveforms.baseline_ch1[evtn])[0:350]*1.97, color=neutron_color, alpha=0.4, y1=0)

plt.vlines(df_waveforms.CFD_rise_ch1[evtn], ymin=-500, ymax=50, alpha=1, color='black', lw=0.5)
plt.vlines(df_waveforms.CFD_rise_ch1[evtn]-25, ymin=-500, ymax=50, alpha=0.5, color='grey')
plt.vlines(df_waveforms.CFD_rise_ch1[evtn]-25+500, ymin=-500, ymax=50, alpha=0.5, color='grey')
plt.vlines(df_waveforms.CFD_rise_ch1[evtn]-25+60, ymin=-500, ymax=50, alpha=0.5, color='grey')
plt.hlines(-25, xmin=0, xmax=1000, color='black', ls='dotted')
plt.ylabel('Amplitude [mV]')
plt.xlabel('Time [ns]')
plt.xlim([160, 730])
plt.ylim([-250, 20])
plt.savefig('/home/gheed/Documents/writing/mauritzson_PhD_thesis_2023/figures/pulse_processing_schematic_waveform.pdf') 
plt.show()


### ZOOM IN VERSION ###
fs.set_style(textwidth=345*0.55, ratio=0.7)
plt.plot(np.arange(0, 1001, 1), (df_waveforms.samples_ch1[evtn]-df_waveforms.baseline_ch1[evtn])*1.97, lw=1.75, zorder=3, color='black')
plt.fill_between(np.arange(0, 1001, 1), y2=(df_waveforms.samples_ch1[evtn]-df_waveforms.baseline_ch1[evtn])*1.97, color=neutron_color, alpha=0.4, y1=0)

plt.hlines(-25, xmin=0, xmax=1, alpha=1, zorder=2)
plt.hlines(0, xmin=0, xmax=1, alpha=1, zorder=2)
plt.hlines(-25, xmin=0, xmax=1000, color='black', zorder=2)
plt.hlines(0, xmin=0, xmax=1000, color='black', ls='dashed', zorder=2)
plt.vlines(df_waveforms.CFD_rise_ch1[evtn], ymin=-500, ymax=60, alpha=1, color='black', zorder=2)

plt.xlim([199, 265])
plt.ylim([-265, 60])
plt.savefig('/home/gheed/Documents/writing/mauritzson_PhD_thesis_2023/figures/pulse_processing_schematic_waveform_inset.pdf') 
plt.show()





# ##########################################################
# ######### Figure 3.4 ####################################
# ######### Before and after kB optimization ###############
# ##########################################################
detector = 'EJ305'
gate = 'LG'

#define energies to plot
Tn = np.arange(2000, 7000, 1000)

#load fitted parameters for reciprocal smear function
poptReciprocal_smear = np.load(f'{pathData}/{detector}/birks/smear_reciprocal_popt.npy')
pcovReciprocal_smear = np.load(f'{pathData}/{detector}/birks/smear_reciprocal_pcov.npy')

#create smear values to use for each energy based on function
smearFactorList = promath.reciprocalConstantFunc(Tn/1000, poptReciprocal_smear[0], poptReciprocal_smear[1])

smearFactorListBefore = np.array([0.21, 0.15, 0.08, 0.065 , 0.032])
scalingBefore = np.array([2.173, 1.774, 1.30, 1.020, 1.490])
scalingAfter = np.array([1, 1, 0.9, 0.75, 1])

#number of bins to use for each energy
numBinsList = np.array([np.arange(0, 1.12, 0.050),
                        np.arange(0, 1.30, 0.055),
                        np.arange(0, 1.80, 0.060),
                        np.arange(0, 2.40, 0.065),
                        np.arange(0, 3.00, 0.070)], dtype=object)

#ranges over which to optmizise scaling between simulation and data
# start = [0.3, 0.7, 1.15, 1.58, 2.12] #For NE 213A
# stop =  [1.0,  1.3,  1.7,  2.8, 3.1] #For NE 213A

start = [0.22,  0.47, 0.82, 1.18, 1.08] #For EJ 305
stop =  [0.70,  1.15, 1.75, 2.20,  2.80] #For EJ 305

names = [ 'evtNum', 
        'optPhotonSum', 
        'optPhotonSumQE', 
        'optPhotonSumCompton', 
        'optPhotonSumComptonQE', 
        'optPhotonSumNPQE', 
        'xLoc', 
        'yLoc', 
        'zLoc', 
        'CsMin', 
        'optPhotonParentID', 
        'optPhotonParentCreatorProcess', 
        'optPhotonParentStartingEnergy',
        'total_edep',
        'gamma_edep',
        'proton_edep',
        'nScatters',
        'nScattersProton']

########
#BEFORE#
########
fs.set_style(textwidth=345)
titleParamters = dict(ha='left', va='center', fontsize=10, color='black')
ratioTotalBefore = 0
ratioTotalAfter = 0
j = 0
gainCorrection = 0.85

for i, energy in enumerate(Tn):
        #import simulations
        simCurrent = pd.read_csv(f"{pathSim}/{detector}/neutrons/neutron_range/5MeV_optimized/{int(energy)}keV/isotropic/CSV_optphoton_data_sum.csv", names=names)

        #randomize binning
        y, x = np.histogram(simCurrent.optPhotonSumQE, bins=4096, range=[0, 4096]) 
        simCurrent.optPhotonSumQE = prodata.getRandDist(x, y)

        #calibrate simulation
        simCurrent.optPhotonSumQE = prodata.calibrateMyDetector(f'{pathData}/{detector}/Simcal/popt.npy', simCurrent.optPhotonSumQE, inverse=False)

        #smear simulation
        simCurrent.optPhotonSumQE = simCurrent.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, smearFactorListBefore[i])) 

        #binning simulations and applying gain offset
        ySimCurrent, xSimCurrent = np.histogram(simCurrent.optPhotonSumQE*gainCorrection, bins=numBinsList[i])
        xSimCurrent = prodata.getBinCenters(xSimCurrent)

        #import data
        nQDC       = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/nQDC_{gate}.npy')
        randQDC    = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/randQDC_{gate}.npy')
        nLength    = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/nLength.npy')
        randLength = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/randLength.npy')
        nQDC = prodata.calibrateMyDetector(f'{pathData}/{detector}/Ecal/popt_LG.npy', nQDC)
        randQDC = prodata.calibrateMyDetector(f'{pathData}/{detector}/Ecal/popt_LG.npy', randQDC)

        #random subtract data
        xDataCurrent, yDataCurrent = prodata.randomTOFSubtractionQDC(   nQDC,
                                                                        randQDC,
                                                                        nLength, 
                                                                        randLength,
                                                                        numBinsList[i], 
                                                                        plot=False)
        #normalize data and simulation to 1
        yDataCurrent = prodata.integralNorm(yDataCurrent, forcePositive=True)
        ySimCurrent = prodata.integralNorm(ySimCurrent, forcePositive=True)

        #fitting data to get range for Chi2
        poptCurrent = promath.gaussFit(xDataCurrent, yDataCurrent, start[i], stop[i], y_error=False, error=False)

        scaleCurrent = scalingBefore[i]

        #calculate the average ratio by first slicing the data sets and then taking the ratio
        xDataCurrentRatio, yDataCurrentRatio = prodata.binDataSlicer(xDataCurrent, yDataCurrent, start[i], stop[i])
        xSimCurrentRatio, ySimCurrentRatio = prodata.binDataSlicer(xSimCurrent, ySimCurrent * scaleCurrent, start[i], stop[i])
        ratioCurrent = np.average(np.abs((yDataCurrentRatio/ySimCurrentRatio)-1))
        ratioStd = np.std((yDataCurrentRatio/ySimCurrentRatio)-1)

        ratioTotalBefore += ratioCurrent/5


        #make plot
        plt.subplot(5, 1, j+1)
        if j==3:
                plt.ylabel('Counts')
        if j==4:
                plt.xlabel('Scintillation Light Yield [MeV$_{ee}$]')       
        
        plt.scatter(xDataCurrent, yDataCurrent*scalingAfter[i], s=5, color='black', zorder=3)
        plt.errorbar(xDataCurrent, yDataCurrent*scalingAfter[i], yerr=yDataCurrent*0.065*scalingAfter[i], linestyle='none',color='black')
        # plt.fill_between(xSimCurrent, y1=0, y2=yDataCurrent*scalingAfter[i], color='black', edgecolor='none', alpha=0.4, step='pre', zorder=1)

        plt.step(xSimCurrent, ySimCurrent * scaleCurrent, color='black', ls='solid', lw=0.75)
        plt.ylim([np.min(yDataCurrent[-15:-1])*0.8, np.max(yDataCurrent)*1.25])
        # plt.xlim(0.294, 3.05) #NE213A
        plt.xlim(0.13, 3.05)#EJ305
        plt.yticks([])

        ########
        #AFTER##
        ########
        simCurrent = pd.read_csv(f"{pathSim}/{detector}/neutrons/neutron_range/{int(energy)}keV/isotropic/1M/CSV_optphoton_data_sum.csv", names=names)

        xSimCurrent, ySimCurrent = prosim.simProcessing(simCurrent, pathData, detector, smearFactorList[i], numBinsList[i])

        #import data
        nQDC       = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/nQDC_{gate}.npy')
        randQDC    = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/randQDC_{gate}.npy')
        nLength    = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/nLength.npy')
        randLength = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/randLength.npy')
        nQDC = prodata.calibrateMyDetector(f'{pathData}/{detector}/Ecal/popt_LG.npy', nQDC)
        randQDC = prodata.calibrateMyDetector(f'{pathData}/{detector}/Ecal/popt_LG.npy', randQDC)

        #random subtract data
        xDataCurrent, yDataCurrent = prodata.randomTOFSubtractionQDC(   nQDC,
                                                                        randQDC,
                                                                        nLength, 
                                                                        randLength,
                                                                        numBinsList[i], 
                                                                        plot=False)
        #normalize data and simulation to 1
        yDataCurrent = prodata.integralNorm(yDataCurrent, forcePositive=True)
        ySimCurrent = prodata.integralNorm(ySimCurrent, forcePositive=True)

        #fitting data to get range for Chi2
        poptCurrent = promath.gaussFit(xDataCurrent, yDataCurrent, start[i], stop[i], y_error=False, error=False)

        ###############################################################################################
        # THIS PART WILL SLICE DATA SETS TO BE USED FOR CALCULATING SCALING PRIOR TO Chi2 CALCULATION #
        ###############################################################################################
        #slicing data and simulation for determining optimal scaling
        xDataCurrentChi2, yDataCurrentScale = prodata.binDataSlicer(xDataCurrent, yDataCurrent, poptCurrent[1]*1, poptCurrent[1]*1.5)
        xSimCurrentChi2, ySimCurrentScale = prodata.binDataSlicer(xSimCurrent, ySimCurrent, poptCurrent[1]*1, poptCurrent[1]*1.5)
        ###############################################################################################

        #Determine optimal scaling for minimal Chi2 value
        scaleCurrent = prodata.chi2Minimize(yDataCurrentScale, ySimCurrentScale, verbose=True)
        
        #calculate the average ratio by first slicing the data sets and then taking the ratio
        xDataCurrentRatio, yDataCurrentRatio = prodata.binDataSlicer(xDataCurrent, yDataCurrent, start[i], stop[i])
        xSimCurrentRatio, ySimCurrentRatio = prodata.binDataSlicer(xSimCurrent, ySimCurrent * scaleCurrent, start[i], stop[i])
        ratioCurrent = np.average(np.abs((yDataCurrentRatio/ySimCurrentRatio)-1))
        ratioStd = np.std((yDataCurrentRatio/ySimCurrentRatio)-1)
        # print(f'E={energy}keV, ratio value={np.round(np.average(ratioCurrent)*100, 2)}%, std={np.round(ratioStd,2)}')

        ratioTotalAfter += ratioCurrent/5

        #make plot
        plt.subplot(5, 1, j+1)
        
        plt.text(0.95, np.max(yDataCurrent)*0.88, f'{np.int32(energy/1000)} MeV', **titleParamters)
        # plt.scatter(xDataCurrent, yDataCurrent, s=2, color='black', zorder=3)
        # plt.errorbar(xDataCurrent, yDataCurrent, yerr=yDataCurrent*0.065, linestyle='none', color='black')

        plt.step(xSimCurrent, ySimCurrent * scaleCurrent * scalingAfter[i], color='black', ls='solid', lw=0.75)
        
        # plt.step(xSimCurrentRatio, ySimCurrentRatio, color='black', ls='solid', lw=2)
        plt.fill_between(xSimCurrent, y1=0, y2= ySimCurrent * scaleCurrent * scalingAfter[i], color='black', edgecolor='none', alpha=0.2, step='pre', zorder=1)

        # plt.ylim([, np.max(yDataCurrent[-15:-1])*1.25])
        plt.ylim([np.min(yDataCurrent[-15:-1])*0.8, np.max(yDataCurrent)*1.25])
        # plt.xlim(0.294, 3.05)#NE213A
        plt.xlim(0.13, 3.05)#EJ305
        plt.yticks([])

        j+=1

print(f'Ratio total before = {np.round(ratioTotalBefore*100, 2)}%')
print(f'Ratio total after = {np.round(ratioTotalAfter*100, 2)}%')

plt.subplots_adjust(hspace = .001, wspace = .001)

plt.savefig(f'/home/gheed/Documents/writing/mauritzson_PhD_thesis_2023/figures/kB_optimization_result.pdf')

plt.show()

# ##########################################################
# ######### Figure 3.4 #####################################
# ######### Optimal kB values and Optimal smear values #####
# ##########################################################
detector = 'EJ305'
density = 0.893 #g/cm3 EJ305

# import optimal kB and smearing values
E_2000 = pd.read_csv(f'{pathData}/{detector}/birks/{2000}keV.csv')
E_2500 = pd.read_csv(f'{pathData}/{detector}/birks/{2500}keV.csv')
E_3000 = pd.read_csv(f'{pathData}/{detector}/birks/{3000}keV.csv')
E_3500 = pd.read_csv(f'{pathData}/{detector}/birks/{3500}keV.csv')
E_4000 = pd.read_csv(f'{pathData}/{detector}/birks/{4000}keV.csv')
E_4500 = pd.read_csv(f'{pathData}/{detector}/birks/{4500}keV.csv')
E_5000 = pd.read_csv(f'{pathData}/{detector}/birks/{5000}keV.csv')
E_5500 = pd.read_csv(f'{pathData}/{detector}/birks/{5500}keV.csv')
E_6000 = pd.read_csv(f'{pathData}/{detector}/birks/{6000}keV.csv')
poptReciprocal_kB = np.load(f'{pathData}/{detector}/birks/kB_reciprocal_popt.npy')
pcovReciprocal_kB = np.load(f'{pathData}/{detector}/birks/kB_reciprocal_pcov.npy')

optimal_kB = np.array([E_2000.optimal_kB[0], 
                        E_2500.optimal_kB[0], 
                        E_3000.optimal_kB[0], 
                        E_3500.optimal_kB[0], 
                        E_4000.optimal_kB[0], 
                        E_4500.optimal_kB[0], 
                        E_5000.optimal_kB[0], 
                        E_5500.optimal_kB[0], 
                        E_6000.optimal_kB[0]])*(density/10)

optimal_smear = np.array([E_2000.optimal_smear[0], 
                        E_2500.optimal_smear[0], 
                        E_3000.optimal_smear[0], 
                        E_3500.optimal_smear[0], 
                        E_4000.optimal_smear[0], 
                        E_4500.optimal_smear[0], 
                        E_5000.optimal_smear[0], 
                        E_5500.optimal_smear[0], 
                        E_6000.optimal_smear[0]])

#define energies to plot
Tn = np.arange(2000, 7000, 1000)

#load fitted parameters for smearing fitted function
poptReciprocal_smear = np.load(f'{pathData}/{detector}/birks/smear_reciprocal_popt.npy')
pcovReciprocal_smear = np.load(f'{pathData}/{detector}/birks/smear_reciprocal_pcov.npy')

#load fitted parameters for kB fitted function
poptReciprocal_kB = np.load(f'{pathData}/{detector}/birks/kB_reciprocal_popt.npy')
pcovReciprocal_kB = np.load(f'{pathData}/{detector}/birks/kB_reciprocal_pcov.npy')

#create smear values to use for each energy based on function
# smearFactorList = promath.reciprocalConstantFunc(Tn/1000, poptReciprocal_smear[0], poptReciprocal_smear[1])


#############################################
optimal_kB_err = np.load(f'{pathData}/{detector}/birks/kB_error.npy')*(density/10)
optimal_smear_err = np.load(f'{pathData}/{detector}/birks/smear_error.npy')
# optimal_kB_err = np.load(f'{pathData}/{detector}/birks/total_error.npy')

#Define variables
Tn = np.array([2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5, 6.0])
Tn_fine = np.arange(1.75, 6.26, 0.01)


#Make plot
fs.set_style(textwidth=345, ratio=0.45)
##################
#optimal kB figure
##################
plt.subplot(1,2,1)

plt.scatter(Tn, optimal_kB, color='black', marker='o', lw=0.5, s=25, facecolor='white', zorder=4)
plt.errorbar(Tn, optimal_kB, yerr=optimal_kB_err, color='black', linestyle='none', lw=0.4, zorder=3)

plt.plot(       Tn_fine, 
                promath.reciprocalConstantFunc(Tn_fine, poptReciprocal_kB[0], poptReciprocal_kB[1])*(density/10), 
                lw=0.5, 
                color='black', 
                linestyle='solid', 
                zorder=2)

plt.fill_between(       Tn_fine, 
                        (promath.reciprocalConstantFunc(Tn_fine, poptReciprocal_kB[0], poptReciprocal_kB[1]) + np.sqrt((1/Tn_fine)**2 * pcovReciprocal_kB[0]**2 + pcovReciprocal_kB[1]**2))*(density/10),
                        (promath.reciprocalConstantFunc(Tn_fine, poptReciprocal_kB[0], poptReciprocal_kB[1]) - np.sqrt((1/Tn_fine)**2 * pcovReciprocal_kB[0]**2 + pcovReciprocal_kB[1]**2))*(density/10),                     
                        color='black', alpha=0.2, zorder=1)

plt.ylim([0.0099, 0.019])#kB

plt.xlim([np.min(Tn_fine)-0.40, np.max(Tn_fine)+0.125])
plt.xlabel('Neutron Energy [MeV]')
plt.ylabel('$k_B$ [g/cm$^{-2}$MeV$^{-1}$]')

########################
#optimal smearing figure
########################
plt.subplot(1,2,2)

plt.scatter(Tn, optimal_smear*100, color='black', marker='o', lw=0.5, s=25, facecolor='white', zorder=4)
plt.errorbar(Tn, optimal_smear*100, yerr=optimal_smear_err*100, color='black', linestyle='none', lw=0.4, zorder=3)

plt.plot(       Tn_fine, 
                promath.reciprocalConstantFunc(Tn_fine, poptReciprocal_smear[0], poptReciprocal_smear[1])*100,
                lw=0.5, 
                color='black', 
                linestyle='solid', 
                zorder=2)

plt.fill_between(       Tn_fine, 
                        (promath.reciprocalConstantFunc(Tn_fine, poptReciprocal_smear[0], poptReciprocal_smear[1]) + np.sqrt((1/Tn_fine)**2 * pcovReciprocal_smear[0]**2 + pcovReciprocal_smear[1]**2))*100,
                        (promath.reciprocalConstantFunc(Tn_fine, poptReciprocal_smear[0], poptReciprocal_smear[1]) - np.sqrt((1/Tn_fine)**2 * pcovReciprocal_smear[0]**2 + pcovReciprocal_smear[1]**2))*100,
                        color='black', alpha=0.2, zorder=1)

plt.ylim([0, 47])#smear

plt.xlim([np.min(Tn_fine)-0.40, np.max(Tn_fine)+0.125])
plt.xlabel('Neutron Energy [MeV]')
plt.ylabel('Smearing [\%]')
plt.subplots_adjust(hspace = .001, wspace = .001)

plt.savefig('/home/gheed/Documents/writing/mauritzson_PhD_thesis_2023/figures/optimal_kB_and_smearing.pdf')

plt.show()






# ##########################################################
# ######### Figure 3.6 #######################################
# ######### Simulations optimal kB and smear ###############
# ##########################################################
att_factor_12dB = 3.98

path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop
path = '/media/gheed/backup_data_8TB/EJ321P_char/cooked/' #path on Nicholais laptop
tofCh = 2
df = propd.load_parquet_merge(path, np.arange(2121,2121+5,1), keep_col=[f'qdc_lg_ch{tofCh}',f'amplitude_ch{tofCh}',f'tof_ch{tofCh}','qdc_lg_ch1','qdc_ps_ch1'], full=False)
# flashrange = {'tof_ch2':[60, 76]} #gamma flash fitting ranges
# prodata.tof_calibration(df, flashrange, distance, phcut=125, calUnit=1e-9, energyCal=True)
df.qdc_lg_ch1 = prodata.calibrateMyDetector('/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/Ecal/popt.npy', df.qdc_lg_ch1*att_factor_12dB)

#set paramters for figure
titleParamters = dict(ha='left', va='center', fontsize=10, color='black')
numBins = np.arange(0.0, 1, 0.01)
xlimit = [0.11, 0.55]
#Makes 'singles' figure
fs.set_style(textwidth=345)

f, (a0, a1) = plt.subplots(2, 1, gridspec_kw={'height_ratios': [1, 1]})
query = f'qdc_lg_ch1>{2}'
y, x = np.histogram(df.query(query).qdc_ps_ch1, bins=numBins)
x = prodata.getBinCenters(x)
#fit gaussian to peaks
popt_g, pcov_g = promath.gaussFit(x, y/np.max(y), start=0.2, stop=0.24, error=True)
popt_n, pcov_n = promath.gaussFit(x, y/np.max(y), start=0.33, stop=0.49, error=True)
a0.plot(np.arange(0,1,0.001), promath.gaussFunc(np.arange(0,1,0.001), popt_g[0], popt_g[1], popt_g[2]), color='tomato', zorder=2)
a0.plot(np.arange(0,1,0.001), promath.gaussFunc(np.arange(0,1,0.001), popt_n[0], popt_n[1], popt_n[2]), color='royalblue', zorder=2)


#Make singles figure
a0.text(0.11, 1.07, f'Uncorrelated NE 213A', **titleParamters)
a0.step(x, y/np.max(y), color='black', where='mid', lw=0.5)
a0.fill_between(x, y/np.max(y), color='grey', alpha=0.8, step='mid', zorder=1)
a0.vlines(0.3, ymin=0, ymax=1.5, linestyle='dashed', color='black', lw=0.5)
a0.set_ylabel('Counts')
a0.set_xlim([xlimit[0], xlimit[1]])
a0.set_ylim([0.01, 4])
a0.set_yscale('log')
a0.set_xticks([])
a0.set_yticks([])


#Make NE213A correlated figure
x = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/sliced/thresholds/keV{2000}/x.npy')
yg = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/sliced/thresholds/keV{2000}/yGamma.npy')
yn = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/NE213A/sliced/thresholds/keV{2000}/yNeutron.npy')

yg, yn = prodata.PSscaler(x, yg, yn, plot=False) #scale data to be roughly same height

a1.text(0.11, 1.5, f'Correlated NE 213A', **titleParamters)

a1.fill_between(x, yg/np.max(yg), color='tomato', step='mid', alpha=0.8, zorder=1)
a1.step(x, yg/np.max(yg), color='black', lw=0.5, where='mid', zorder=2)

a1.fill_between(x, yn/np.max(yg), color='royalblue', step='mid', alpha=0.6, zorder=3)
a1.fill_between(x[:29], yn[:29]/np.max(yg), hatch='///', edgecolor='black', step='mid', facecolor='none', lw=0.5, zorder=4)
a1.step(x, yn/np.max(yg), color='black', lw=0.5, where='mid', zorder=5)

a1.vlines(0.3, ymin=0, ymax=4, linestyle='dashed', color='black', lw=0.5)
a1.set_yscale('log')
a1.set_xlim([xlimit[0], xlimit[1]])
a1.set_ylim([0.01, 4])
a1.set_yticks([])
a1.set_xlabel('PS')


plt.subplots_adjust(hspace = .001, wspace = .001)


plt.savefig('/home/gheed/Documents/writing/mauritzson_PhD_thesis_2023/figures/NPG_gamma_rays.pdf')

plt.show()




# #########################################################
# ######### Figure 3.11 ####################################
# ######### HH, TP, FD & Sim for NE213A ###################
# #########################################################

def lightYieldPlot(detector, figureNum, HH_data_LG, TP_data_LG, FD_data_LG, HH_data_SG, TP_data_SG, FD_data_SG, LY_sim, kornilov_LG, kornilov_SG, cecil_LG, cecil_SG, kornilov_SMD):
        if detector == 'NE 213A':
                mainColor = 'black'
                detectorName = 'NE213A'
                L1 = 3.67#2.47
                p = [0.83, 2.82, 0.25, 0.93]
        elif detector == 'EJ 305':
                mainColor = EJ305_color
                detectorName = 'EJ305'
                L1 = 6.55#8.5700387
                # p = [1.0, 8.2, 0.1, 0.88] #NE224
                p = [0.817, 2.63, 0.297, 1]
        elif detector == 'EJ 331':
                mainColor = EJ331_color
                detectorName = 'EJ331'
                L1 = 5.34#7.666969
                p = [0.817, 2.63, 0.297, 1] #EJ309
        elif detector == 'EJ 321P':
                mainColor = EJ321P_color
                detectorName = 'EJ321P'
                L1 = 6.68#6.617600
                p = [0.43, 0.77, 0.26, 2.13]
    
        fs.set_style(textwidth=345, ratio=1.61)
        titleParamters = dict(ha='right', va='center', fontsize=10, color='black')
        
        plt.subplot(3,1,1)
        plt.scatter(HH_data_LG.energy, HH_data_LG.HH_loc, s=40, lw=0.5, color=mainColor, marker='o', label='Measured data', facecolor='white', alpha=1, zorder=2)
        plt.errorbar(HH_data_LG.energy, HH_data_LG.HH_loc, yerr=HH_data_LG.HH_loc_err, linestyle='none', color=mainColor, lw=0.5, alpha=1, zorder=1)
        plt.plot(np.arange(1.75, 6.25, 0.01), promath.kornilovEq(np.arange(1.75, 6.25, 0.01), kornilov_LG.query('method=="HH_data"').L0.item(), L1), lw=.75, color=mainColor, alpha=.9, label='Kornilov Eq.[3.3]')
        # plt.plot(np.arange(1.75, 6.25, 0.01), promath.cecilEq(np.arange(1.75, 6.25, 0.01), cecil_LG.query('method=="HH_data"').C.item(), p[0], p[1], p[2], p[3]), lw=.5, color='black', alpha=.9, ls='dashed')

        plt.scatter(LY_sim.energy,  LY_sim.means, s=45, lw=0.5, edgecolors='none', facecolors=NE213A_color, marker='v', alpha=1, zorder=4)
        plt.errorbar(LY_sim.energy, LY_sim.means, yerr=LY_sim.means_err, linestyle='none', color=NE213A_color, lw=0.5, alpha=1, zorder=3)
        plt.plot(np.arange(1.75, 6.25, 0.01), promath.kornilovEq(np.arange(1.75, 6.25, 0.01), kornilov_SMD.L0.item(), L1), lw=1, color=NE213A_color, alpha=.9, ls='dashed')        
        
        plt.legend(loc=2)
        plt.text(4.15, np.max(HH_data_LG.HH_loc)*1.05, 'HH', **titleParamters)
        plt.xlim([1.75, 6.25])
        plt.ylim([np.min(HH_data_LG.HH_loc)*0.29, np.max(HH_data_LG.HH_loc)*1.15])

        ############################
        plt.subplot(3,1,2)
        plt.scatter(TP_data_LG.energy, TP_data_LG.TP_loc, s=40, lw=0.5, color=mainColor, marker='o', facecolor='white', alpha=1, zorder=2)
        plt.errorbar(TP_data_LG.energy, TP_data_LG.TP_loc, yerr=TP_data_LG.TP_loc_err, linestyle='none', color=mainColor, lw=0.5, alpha=1, zorder=1)
        plt.plot(np.arange(1.75, 6.25, 0.01), promath.kornilovEq(np.arange(1.75, 6.25, 0.01), kornilov_LG.query('method=="TP_data"').L0.item(), L1), lw=0.75, color=mainColor, alpha=.9)
        # plt.plot(np.arange(1.75, 6.25, 0.01), promath.cecilEq(np.arange(1.75, 6.25, 0.01), cecil_LG.query('method=="TP_data"').C.item(), p[0], p[1], p[2], p[3]), lw=0.5, color='black', alpha=.9, ls='dashed', label='Cecil Eq.[4]')

        plt.scatter(LY_sim.energy,  LY_sim.means, s=45,  lw=0.5, edgecolors='none', facecolors=NE213A_color, marker='v', alpha=1, label='SMD', zorder=4)
        plt.errorbar(LY_sim.energy, LY_sim.means, yerr=LY_sim.means_err, linestyle='none', color=NE213A_color, lw=0.5, alpha=1, zorder=3)
        plt.plot(np.arange(1.75, 6.25, 0.01), promath.kornilovEq(np.arange(1.75, 6.25, 0.01), kornilov_SMD.L0.item(), L1), lw=1, color=NE213A_color, alpha=.9, ls='dashed', label='Kornilov Eq.[3.3]')

        plt.text(4.15, np.max(HH_data_LG.HH_loc)*1.05, 'TP', **titleParamters)
        plt.xlim([1.75, 6.25])
        plt.ylim([np.min(HH_data_LG.HH_loc)*0.29, np.max(HH_data_LG.HH_loc)*1.15])
        # plt.legend(loc=2)
        plt.legend(loc=2)
        plt.ylabel('Scintillation Light Yield [MeV$_{ee}$]')

        ############################
        plt.subplot(3,1,3)
        plt.scatter(FD_data_LG.energy, FD_data_LG.FD_loc, s=40, lw=0.5, color=mainColor, marker='o', facecolor='white', alpha=1, zorder=2)
        plt.errorbar(FD_data_LG.energy, FD_data_LG.FD_loc, yerr=FD_data_LG.FD_loc_err, linestyle='none', color=mainColor, lw=0.5, alpha=1, zorder=1)
        plt.plot(np.arange(1.75, 6.25, 0.01), promath.kornilovEq(np.arange(1.75, 6.25, 0.01), kornilov_LG.query('method=="FD_data"').L0.item(), L1), lw=0.5, color=mainColor, alpha=.9)
        # plt.plot(np.arange(1.75, 6.25, 0.01), promath.cecilEq(np.arange(1.75, 6.25, 0.01), cecil_LG.query('method=="FD_data"').C.item(), p[0], p[1], p[2], p[3]), lw=0.5, color='black', alpha=.9, ls='dashed')

        plt.scatter(LY_sim.energy,  LY_sim.means, s=45, lw=0.5, edgecolors='none', facecolors=NE213A_color, marker='v', alpha=1, zorder=4)
        plt.errorbar(LY_sim.energy, LY_sim.means, yerr=LY_sim.means_err, linestyle='none', color=NE213A_color, lw=0.5, alpha=1, zorder=3)
        plt.plot(np.arange(1.75, 6.25, 0.01), promath.kornilovEq(np.arange(1.75, 6.25, 0.01), kornilov_SMD.L0.item(), L1), lw=1, color=NE213A_color, alpha=.9, ls='dashed', label='Kornilov Eq.[5]')

        plt.text(4.15, np.max(HH_data_LG.HH_loc*1.05), 'FD', **titleParamters)
        plt.text(6.20, np.max(HH_data_LG.HH_loc*.15), f'{detector}', **titleParamters)
        plt.xlim([1.75, 6.25])
        plt.ylim([np.min(HH_data_LG.HH_loc)*0.29, np.max(HH_data_LG.HH_loc)*1.15])
        plt.xlabel('Proton Energy [MeV]')
        
        plt.subplots_adjust(hspace = .001, wspace = .001)
        plt.savefig(f'/home/gheed/Documents/writing/mauritzson_PhD_thesis_2023/figures/{detectorName}_result.pdf')
        
        plt.show()

detector = 'NE213A'
figureNum = 10
HH_data_LG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/HH_data_LG.pkl')
TP_data_LG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/TP_data_LG.pkl')
FD_data_LG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/FD_data_LG.pkl')
HH_data_SG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/HH_data_SG.pkl')
TP_data_SG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/TP_data_SG.pkl')
FD_data_SG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/FD_data_SG.pkl')
LY_sim = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/simulation/sim_LightYield.pkl')
kornilov_LG = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/HH_TP_FD/kornilov_LG_result.pkl')
kornilov_SG = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/HH_TP_FD/kornilov_SG_result.pkl')
cecil_LG = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/HH_TP_FD/cecil_LG_result.pkl')
cecil_SG = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/HH_TP_FD/cecil_SG_result.pkl')
kornilov_SMD = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/simulation/kornilov_result.pkl')
cecil_SMD = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/simulation/cecil_result.pkl')

print((1-HH_data_LG.HH_loc.to_numpy()/LY_sim.means.to_numpy())*100)
print((1-TP_data_LG.TP_loc.to_numpy()/LY_sim.means.to_numpy())*100)
print((1-FD_data_LG.FD_loc.to_numpy()/LY_sim.means.to_numpy())*100)
lightYieldPlot('NE 213A', figureNum, HH_data_LG, TP_data_LG, FD_data_LG, HH_data_SG, TP_data_SG, FD_data_SG, LY_sim, kornilov_LG, kornilov_SG, cecil_LG, cecil_SG, kornilov_SMD)




# ##########################################################
# ######### Figure X #######################################
# ######### Simulations optimal kB and smear ###############
# ##########################################################

#Load simulations results
detector = 'EJ305'
density = 0.893 #g/cm3 NE213A

data_2000 = pd.read_csv(f'{pathData}/{detector}/birks/{2000}keV.csv')
data_2500 = pd.read_csv(f'{pathData}/{detector}/birks/{2500}keV.csv')
data_3000 = pd.read_csv(f'{pathData}/{detector}/birks/{3000}keV.csv')
data_3500 = pd.read_csv(f'{pathData}/{detector}/birks/{3500}keV.csv')
data_4000 = pd.read_csv(f'{pathData}/{detector}/birks/{4000}keV.csv')
data_4500 = pd.read_csv(f'{pathData}/{detector}/birks/{4500}keV.csv')
data_5000 = pd.read_csv(f'{pathData}/{detector}/birks/{5000}keV.csv')
data_5500 = pd.read_csv(f'{pathData}/{detector}/birks/{5500}keV.csv')
data_6000 = pd.read_csv(f'{pathData}/{detector}/birks/{6000}keV.csv')
poptReciprocal_kB = np.load(f'{pathData}/{detector}/birks/kB_reciprocal_popt.npy')
pcovReciprocal_kB = np.sqrt(np.diag(np.load(f'{pathData}/{detector}/birks/kB_reciprocal_pcov.npy')))
optimalkB_err = np.load(f'{pathData}/{detector}/birks/kB_error.npy')
poptReciprocal_smear = np.load(f'{pathData}/{detector}/birks/smear_reciprocal_popt.npy')
pcovReciprocal_smear = np.sqrt(np.diag(np.load(f'{pathData}/{detector}/birks/smear_reciprocal_pcov.npy')))
optimalSmear_err = np.load(f'{pathData}/{detector}/birks/smear_error.npy')
sim_total_error = np.load(f'{pathData}/{detector}/birks/total_error.npy')


#Define variables
Tn = np.array([2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5, 6.0])
Tn_fine = np.arange(1.75, 6.26, 0.01)

optimalkB = np.array([data_2000.optimal_kB[0], 
                        data_2500.optimal_kB[0], 
                        data_3000.optimal_kB[0], 
                        data_3500.optimal_kB[0], 
                        data_4000.optimal_kB[0], 
                        data_4500.optimal_kB[0], 
                        data_5000.optimal_kB[0], 
                        data_5500.optimal_kB[0], 
                        data_6000.optimal_kB[0]])*(density/10)

optimalSmear = np.array([data_2000.optimal_smear[0], 
                        data_2500.optimal_smear[0], 
                        data_3000.optimal_smear[0], 
                        data_3500.optimal_smear[0], 
                        data_4000.optimal_smear[0], 
                        data_4500.optimal_smear[0], 
                        data_5000.optimal_smear[0], 
                        data_5500.optimal_smear[0], 
                        data_6000.optimal_smear[0]])


#Make plot
fs.set_style(textwidth=345)
fig, ax1 = plt.subplots()

# Instantiate a second axes that shares the same x-axis
ax2 = ax1.twinx()  

#kB
ax1.scatter(Tn, optimalkB, color='black', marker='o', lw=0.5, s=25, facecolor='white', label='Optimal $k_B$', zorder=4)
ax1.errorbar(Tn, optimalkB, yerr=optimalkB*optimalkB_err, color='black', linestyle='none', lw=0.5, zorder=3)

ax1.plot(Tn_fine, promath.reciprocalConstantFunc(Tn_fine, poptReciprocal_kB[0], poptReciprocal_kB[1])*(density/10), lw=0.5, color='black', linestyle='solid', zorder=2)
# ax1.fill_between(       Tn_fine, 
                        # promath.reciprocalConstantFunc(Tn_fine, poptReciprocal_kB[0]-pcovReciprocal_kB[0], poptReciprocal_kB[1]-pcovReciprocal_kB[1])*(density/10),#-0.000307836226262341, 
                        # promath.reciprocalConstantFunc(Tn_fine, poptReciprocal_kB[0]+pcovReciprocal_kB[0], poptReciprocal_kB[1]+pcovReciprocal_kB[1])*(density/10),#+0.000307836226262341, #0.02895527565858691
                        # color='black', alpha=0.1, zorder=1)

#smear
ax2.scatter(Tn, optimalSmear*100, color='black', marker='v', lw=0.5, s=25,  facecolor='white', label='Optimal smearing', zorder=4)
ax2.errorbar(Tn, optimalSmear*100, yerr=optimalSmear*optimalSmear_err*100, color='black', linestyle='none', lw=0.5, zorder=3)


ax2.plot(Tn_fine, promath.reciprocalConstantFunc(Tn_fine, poptReciprocal_smear[0], poptReciprocal_smear[1])*100, lw=0.5, color='black', linestyle='solid', zorder=2)
# ax2.fill_between(       Tn_fine, 
                        # promath.reciprocalConstantFunc(Tn_fine, poptReciprocal_smear[0]-pcovReciprocal_smear[0], poptReciprocal_smear[1]-pcovReciprocal_smear[1])*100,#-0.02895527565858691, 
                        # promath.reciprocalConstantFunc(Tn_fine, poptReciprocal_smear[0]+pcovReciprocal_smear[0], poptReciprocal_smear[1]+pcovReciprocal_smear[1])*100,#+0.02895527565858691,
                        # color='black', alpha=0.1, zorder=1)

ax1.set_ylim([0.0099, 0.022])#kB
ax2.set_ylim([-25, 52])#smear

ax1.set_xlim([np.min(Tn_fine)-0.125, np.max(Tn_fine)+0.125])
ax1.set_xlabel('Neutron Energy [MeV]')
ax1.set_ylabel('$k_B$ [g/cm$^2$MeV]')
ax2.set_ylabel('Smearing [\%]')

ax1.legend(loc=3)
ax2.legend(loc=1)
plt.tight_layout()

# plt.savefig( '/home/gheed/Documents/writing/mauritzson_PhD_thesis_2023/figures/optimal_kB_smear.pdf')

plt.show()




# ##########################################################
# ######### Figure 3.2 #####################################
# ######### Isotropic vs Pencilbeam simulations ############
# ##########################################################
energy = 4000
smear = 0#0.08
numBins_neutron = np.arange(50, 480, 8)
numBins_gamma = np.arange(50, 850, 8)
detector = 'EJ305'

names = [ 'evtNum', 
        'optPhotonSum', 
        'optPhotonSumQE', 
        'optPhotonSumCompton', 
        'optPhotonSumComptonQE', 
        'optPhotonSumNPQE', 
        'xLoc', 
        'yLoc', 
        'zLoc', 
        'CsMin', 
        'optPhotonParentID', 
        'optPhotonParentCreatorProcess', 
        'optPhotonParentStartingEnergy',
        'total_edep',
        'gamma_edep',
        'proton_edep',
        'nScatters',
        'nScattersProton']

#load sims    
isotropic = pd.read_csv(f"{pathSim}/{detector}/neutrons/neutron_range/{int(energy)}keV/isotropic/1M/CSV_optphoton_data_sum.csv", names=names)
pencilbeam = pd.read_csv(f"{pathSim}/{detector}/neutrons/neutron_range/{int(energy)}keV/pencilbeam/300k/CSV_optphoton_data_sum.csv", names=names)

isotropicG = pd.read_csv(f"{pathSim}/{detector}/Th232/isotropic/CSV_optphoton_data_sum.csv", names=names)
isotropicG = pd.concat([isotropicG, pd.read_csv(f"{pathSim}/{detector}/Th232/isotropic/part2/CSV_optphoton_data_sum.csv", names=names)])
isotropicG = pd.concat([isotropicG, pd.read_csv(f"{pathSim}/{detector}/Th232/isotropic/part3/CSV_optphoton_data_sum.csv", names=names)])

pencilbeamG = pd.read_csv(f"{pathSim}/{detector}/Th232/pencilbeam/CSV_optphoton_data_sum.csv", names=names)
pencilbeamG = pd.concat([pencilbeamG, pd.read_csv(f"{pathSim}/{detector}/Th232/pencilbeam/part2/CSV_optphoton_data_sum.csv", names=names)])
pencilbeamG = pd.concat([pencilbeamG, pd.read_csv(f"{pathSim}/{detector}/Th232/pencilbeam/part3/CSV_optphoton_data_sum.csv", names=names)])

#randomize binning
y, x = np.histogram(isotropic.optPhotonSumQE, bins=4096, range=[0, 4096]) 
isotropic.optPhotonSumQE = prodata.getRandDist(x, y)

y, x = np.histogram(pencilbeam.optPhotonSumQE, bins=4096, range=[0, 4096]) 
pencilbeam.optPhotonSumQE = prodata.getRandDist(x, y)

y, x = np.histogram(pencilbeam.optPhotonSumNPQE, bins=4096, range=[0, 4096]) 
pencilbeam.optPhotonSumNPQE = prodata.getRandDist(x, y)


y, x = np.histogram(isotropicG.optPhotonSumQE, bins=4096, range=[0, 4096]) 
isotropicG.optPhotonSumQE = prodata.getRandDist(x, y)

y, x = np.histogram(pencilbeamG.optPhotonSumQE, bins=4096, range=[0, 4096]) 
pencilbeamG.optPhotonSumQE = prodata.getRandDist(x, y)



#calibrating simulation
# isotropic.optPhotonSumQE = prodata.calibrateMyDetector(f'{pathData}/{detector}/Simcal/popt.npy', isotropic.optPhotonSumQE, inverse=False)
# pencilbeam.optPhotonSumQE = prodata.calibrateMyDetector(f'{pathData}/{detector}/Simcal/popt.npy', pencilbeam.optPhotonSumQE, inverse=False)

#smear simulation
# isotropic.optPhotonSumQE = isotropic.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, smear)) 
# pencilbeam.optPhotonSumQE = pencilbeam.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, smear)) 

# isotropicG.optPhotonSumQE = isotropicG.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, smear)) 
# pencilbeamG.optPhotonSumQE = pencilbeamG.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, smear)) 



#bin simulation
ySimIso, xSimIso = np.histogram(isotropic.optPhotonSumQE, bins=numBins_neutron)
xSimIso = prodata.getBinCenters(xSimIso)
ySimPen, xSimPen = np.histogram(pencilbeam.optPhotonSumQE, bins=numBins_neutron)
xSimPen = prodata.getBinCenters(xSimPen)

ySimPenNP, xSimPenNP = np.histogram(pencilbeam.optPhotonSumNPQE, bins=numBins_neutron)
xSimPenNP = prodata.getBinCenters(xSimPenNP)


ySimGIso, xSimGIso = np.histogram(isotropicG.optPhotonSumQE, bins=numBins_gamma)
xSimGIso = prodata.getBinCenters(xSimGIso)
ySimGPen, xSimGPen = np.histogram(pencilbeamG.optPhotonSumQE, bins=numBins_gamma)
xSimGPen = prodata.getBinCenters(xSimGPen)



#Make figure 
#NOTE 4% less gain for isotropic compared to pencilbeam

fs.set_style(textwidth=345)

#neutron
plt.step(xSimIso, prodata.dataAveraging(ySimIso, 2), color='black', ls='dashed', lw=1, label='Isotropic')
# plt.fill_between(xSimIso, prodata.dataAveraging(ySimIso, 2), color=neutron_color, alpha=1, step='pre')

plt.step(xSimPen, prodata.dataAveraging(ySimPen, 2)*0.72, color='black', ls='solid', lw=1, label='Pencil beam')

#gamma
plt.step(xSimGIso, prodata.dataAveraging(ySimGIso, 2)*0.95, color='black', ls='dashed', lw=1)
# plt.fill_between(xSimGIso, prodata.dataAveraging(ySimGIso, 2)*0.95, color=gamma_color, alpha=1, step='pre')

plt.step(xSimGIso, prodata.dataAveraging(ySimGPen, 2)*0.27, color='black', ls='solid', lw=1)
# plt.step(xSimPenNP, prodata.dataAveraging(ySimPenNP*0.68, 2), color='red', ls='solid', lw=1, label='Pen. NP')

plt.xlim([75, 850])
plt.ylim([0, np.max(ySimIso)*1.15])
plt.xlabel('Number of Photoelectrons')
plt.ylabel('Counts')
plt.legend(loc=3)
# 
# plt.savefig('/home/gheed/Documents/writing/mauritzson_PhD_thesis_2023/figures/isotropic_vs_pencilbeam_spectra.pdf')

plt.show()


# ##########################################################
# ######### Figure X #######################################
# ######### Simulation light yield explanation #############
# ##########################################################
energy = 3000
gate = 'LG'
detector = 'NE213A'
numBins = np.arange(0.35, 2.5, 0.04)

nQDC       = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/nQDC_{gate}.npy')
randQDC    = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/randQDC_{gate}.npy')
nLength    = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/nLength.npy')
randLength = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/randLength.npy')
nQDC = prodata.calibrateMyDetector(f'{pathData}/{detector}/Ecal/popt_{gate}.npy', nQDC)
randQDC = prodata.calibrateMyDetector(f'{pathData}/{detector}/Ecal/popt_{gate}.npy', randQDC)

x_data, y_data = prodata.randomTOFSubtractionQDC( nQDC,
                                                                randQDC,
                                                                nLength, 
                                                                randLength,
                                                                numBins,#np.arange(0,3,0.03), 
                                                                plot=False)
fitRange = [0.60, 1.55]
xFine = np.arange(0.60, 1.60, 0.0001)

y_gradient = prodata.firstDerivativeHelper(y_data, 5)
interpolFunc = interpolate.splrep(x_data, y_gradient, s=100)
ynew = interpolate.splev(xFine, interpolFunc, der=0)

# popt, pcov = promath.gaussFit(x_data, y_gradient*-1, 0.65, 1.55)

# plt.step(x_data, y_data)
plt.step(x_data, prodata.firstDerivativeHelper(y_data, 5))
plt.plot(xFine, ynew, label='spline')
# plt.plot(xFine, promath.gaussFunc(xFine, popt[0], popt[1], popt[2])*-1)
plt.legend()
plt.show()


names = [ 'evtNum', 
        'optPhotonSum', 
        'optPhotonSumQE', 
        'optPhotonSumCompton', 
        'optPhotonSumComptonQE', 
        'optPhotonSumNPQE', 
        'xLoc', 
        'yLoc', 
        'zLoc', 
        'CsMin', 
        'optPhotonParentID', 
        'optPhotonParentCreatorProcess', 
        'optPhotonParentStartingEnergy',
        'total_edep',
        'gamma_edep',
        'proton_edep',
        'nScatters',
        'nScattersProton']

#load sim
sim_iso = pd.read_csv(f"{pathSim}/{detector}/neutrons/neutron_range/{int(energy)}keV/isotropic/CSV_optphoton_data_sum.csv", names=names)
sim_pen = pd.read_csv(f"{pathSim}/{detector}/neutrons/neutron_range/{int(energy)}keV/pencilbeam/CSV_optphoton_data_sum.csv", names=names)

#Find edep maximum location
y_sim_edep, x_sim_edep = np.histogram(sim_pen.total_edep, bins=np.arange(0, energy*1.2, .3))
x_sim_edep = prodata.getBinCenters(x_sim_edep)
edepMaxLoc = x_sim_edep[np.argmax(y_sim_edep)]

#Cut on edep for pencilbeam
sim_pen = sim_pen.query(f'{edepMaxLoc*edepLow}<total_edep<{edepMaxLoc*edepHigh}')

#randomize binning
y, x = np.histogram(sim_iso.optPhotonSumQE, bins=4096, range=[0, 4096]) 
sim_iso.optPhotonSumQE = prodata.getRandDist(x, y)
y, x = np.histogram(sim_pen.optPhotonSumQE, bins=4096, range=[0, 4096]) 
sim_pen.optPhotonSumQE = prodata.getRandDist(x, y)

#calibrate simulation
sim_iso.optPhotonSumQE = prodata.calibrateMyDetector(f'{pathData}/{detector}/Simcal/popt.npy', sim_iso.optPhotonSumQE, inverse=False)
sim_pen.optPhotonSumQE = prodata.calibrateMyDetector(f'{pathData}/{detector}/Simcal/popt.npy', sim_pen.optPhotonSumQE, inverse=False)

#smear simulation
sim_iso.optPhotonSumQE = sim_iso.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, 0.16061025) )
sim_pen.optPhotonSumQE = sim_pen.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, 0.16061025) )

#bin simulation
y_sim_iso, x_sim_iso = np.histogram(sim_iso.optPhotonSumQE, bins=numBins)
x_sim_iso = prodata.getBinCenters(x_sim_iso)
y_sim_pen, x_sim_pen = np.histogram(sim_pen.optPhotonSumQE, bins=numBins)
x_sim_pen = prodata.getBinCenters(x_sim_pen)

#LY locations
NE213A_HH_loc_3 = 1.083929
NE213A_TP_loc_3 = 1.0227
NE213A_FD_loc_3 = 1.0581
NE213A_sim_loc_3 = 1.015851


#Make main figure
fs.set_style(textwidth=345)
plt.scatter(x_data, y_data, s=20, color='black')
plt.plot(x_sim_iso, y_sim_iso*4.3, lw=1, ls='dashed', color='black', label='isotropic')
plt.plot(x_sim_pen, y_sim_pen*0.8, color='black', lw=1, label='pencilbeam')
plt.fill_between(x_sim_pen, 0, y_sim_pen*0.8, alpha=0.5, color='black')
plt.vlines(NE213A_HH_loc_3, ymin=0, ymax=2000, lw=0.5, ls='solid', color='black')
plt.vlines(NE213A_TP_loc_3, ymin=0, ymax=2000, lw=0.5, ls='dashed', color='black')
plt.vlines(NE213A_FD_loc_3, ymin=0, ymax=2000, lw=0.5, ls='dashdot', color='black')
plt.vlines(NE213A_sim_loc_3, ymin=0, ymax=2000, lw=0.5, ls='dotted', color='black')

plt.ylim(0, np.max(y_data)*1.2)
plt.xlim(0.6, 1.75)
plt.ylabel('Counts')
plt.xlabel('Energy [MeV$_{ee}$]')

# plt.savefig('/home/gheed/Documents/writing/mauritzson_lightYield_paper_2022/figures/figure06_A.pdf')

plt.show()

#Make inset figure
fs.set_style(textwidth=345/2.2)
sim_pen_edep = pd.read_csv(f"{pathSim}/{detector}/neutrons/neutron_range/{int(energy)}keV/pencilbeam/CSV_optphoton_data_sum.csv", names=names)
y_sim_edep, x_sim_edep = np.histogram(sim_pen_edep.total_edep, bins=np.arange(energy-10, energy+4.5, .3))
x_sim_edep = prodata.getBinCenters(x_sim_edep)
plt.vlines(edepMaxLoc*edepLow, ymin=1, ymax=np.max(y_sim_edep)*1.1, lw=1, color='black', ls='dashed')
plt.vlines(edepMaxLoc*edepHigh, ymin=1, ymax=np.max(y_sim_edep)*1.1, lw=1, color='black', ls='dashed')

plt.step(x_sim_edep,y_sim_edep, lw=1, color='black')
plt.fill_between(x_sim_edep, 1, y_sim_edep, alpha=0.3, color='black', step='pre')
plt.ylim(1, np.max(y_sim_edep)*1.1)
plt.xlim(2999, 3006)
plt.ylabel('Counts')
plt.xlabel('Energy [MeV]')

# plt.savefig('/home/gheed/Documents/writing/mauritzson_lightYield_paper_2022/figures/figure06_B.pdf')

plt.show()


# ##########################################################
# ######### Figure X #######################################
# ######### Neutron Light yield method #####################
# ##########################################################

gate = 'LG'

numBins2= np.arange(0.23, 1.2, 0.047)
numBins4 = np.arange(0.23, 2.5, 0.06)
numBins6 = np.arange(0.23, 3.8, 0.075)
numBins2pen= np.arange(0.20, 1.2, 0.047)
numBins4pen = np.arange(0.60, 2.5, 0.06)
numBins6pen = np.arange(1.5, 3.8, 0.075)

#Load data
energy = 2000
detector = 'NE213A'
nQDC       = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/nQDC_{gate}.npy')
randQDC    = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/randQDC_{gate}.npy')
nLength    = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/nLength.npy')
randLength = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/randLength.npy')
nQDC = prodata.calibrateMyDetector(f'{pathData}/{detector}/Ecal/popt_{gate}.npy', nQDC)
randQDC = prodata.calibrateMyDetector(f'{pathData}/{detector}/Ecal/popt_{gate}.npy', randQDC)

x_NE213A_data_2, y_NE213A_data_2 = prodata.randomTOFSubtractionQDC( nQDC,
                                                                randQDC,
                                                                nLength, 
                                                                randLength,
                                                                numBins2, 
                                                                plot=False)
energy = 4000
detector = 'NE213A'
nQDC       = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/nQDC_{gate}.npy')
randQDC    = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/randQDC_{gate}.npy')
nLength    = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/nLength.npy')
randLength = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/randLength.npy')
nQDC = prodata.calibrateMyDetector(f'{pathData}/{detector}/Ecal/popt_{gate}.npy', nQDC)
randQDC = prodata.calibrateMyDetector(f'{pathData}/{detector}/Ecal/popt_{gate}.npy', randQDC)

x_NE213A_data_4, y_NE213A_data_4 = prodata.randomTOFSubtractionQDC( nQDC,
                                                                randQDC,
                                                                nLength, 
                                                                randLength,
                                                                numBins4, 
                                                                plot=False)
energy = 6000
detector = 'NE213A'
nQDC       = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/nQDC_{gate}.npy')
randQDC    = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/randQDC_{gate}.npy')
nLength    = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/nLength.npy')
randLength = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/randLength.npy')
nQDC = prodata.calibrateMyDetector(f'{pathData}/{detector}/Ecal/popt_{gate}.npy', nQDC)
randQDC = prodata.calibrateMyDetector(f'{pathData}/{detector}/Ecal/popt_{gate}.npy', randQDC)

x_NE213A_data_6, y_NE213A_data_6 = prodata.randomTOFSubtractionQDC( nQDC,
                                                                randQDC,
                                                                nLength, 
                                                                randLength,
                                                                numBins6, 
                                                                plot=False)

#Load simulation
names = [ 'evtNum', 
        'optPhotonSum', 
        'optPhotonSumQE', 
        'optPhotonSumCompton', 
        'optPhotonSumComptonQE', 
        'optPhotonSumNPQE', 
        'xLoc', 
        'yLoc', 
        'zLoc', 
        'CsMin', 
        'optPhotonParentID', 
        'optPhotonParentCreatorProcess', 
        'optPhotonParentStartingEnergy',
        'total_edep',
        'gamma_edep',
        'proton_edep',
        'nScatters',
        'nScattersProton']

#load sim 2 MeV
energy = 2000
detector = 'NE213A'
NE213A_sim_iso_2 = pd.read_csv(f"{pathSim}/{detector}/neutrons/neutron_range/{int(energy)}keV/isotropic/CSV_optphoton_data_sum.csv", names=names)
NE213A_sim_pen_2 = pd.read_csv(f"{pathSim}/{detector}/neutrons/neutron_range/{int(energy)}keV/pencilbeam/CSV_optphoton_data_sum.csv", names=names)

#load sim 4 MeV
energy = 4000
detector = 'NE213A'
NE213A_sim_iso_4 = pd.read_csv(f"{pathSim}/{detector}/neutrons/neutron_range/{int(energy)}keV/isotropic/CSV_optphoton_data_sum.csv", names=names)
NE213A_sim_pen_4 = pd.read_csv(f"{pathSim}/{detector}/neutrons/neutron_range/{int(energy)}keV/pencilbeam/CSV_optphoton_data_sum.csv", names=names)

#load sim 6 MeV
energy = 6000
detector = 'NE213A'
NE213A_sim_iso_6 = pd.read_csv(f"{pathSim}/{detector}/neutrons/neutron_range/{int(energy)}keV/isotropic/CSV_optphoton_data_sum.csv", names=names)
NE213A_sim_pen_6 = pd.read_csv(f"{pathSim}/{detector}/neutrons/neutron_range/{int(energy)}keV/pencilbeam/CSV_optphoton_data_sum.csv", names=names)

#Find edep maximum location NE213A used for cut
y, x = np.histogram(NE213A_sim_pen_2.total_edep, bins=np.arange(0, 2000*1.2, .3))
x = prodata.getBinCenters(x)
NE213A_edepMax_2 = x[np.argmax(y)]

y, x = np.histogram(NE213A_sim_pen_4.total_edep, bins=np.arange(0, 4000*1.2, .3))
x = prodata.getBinCenters(x)
NE213A_edepMax_4 = x[np.argmax(y)]

y, x = np.histogram(NE213A_sim_pen_6.total_edep, bins=np.arange(0, 6000*1.2, .3))
x = prodata.getBinCenters(x)
NE213A_edepMax_6 = x[np.argmax(y)]

#Bin edep speectrum before cutting on edep in next step

y_NE213A_edep_4, x_NE213A_edep_4 = np.histogram(NE213A_sim_pen_4.total_edep, bins=np.arange(0, 5000, .32))
x_NE213A_edep_4 = prodata.getBinCenters(x_NE213A_edep_4)
edepMaxLoc_4 = x_NE213A_edep_4[np.argmax(y_NE213A_edep_4)]


#Cut pencilbeam on edep
NE213A_sim_pen_2 = NE213A_sim_pen_2.query(f'{NE213A_edepMax_2*edepLow}<total_edep<{NE213A_edepMax_2*edepHigh}')
NE213A_sim_pen_4 = NE213A_sim_pen_4.query(f'{NE213A_edepMax_4*edepLow}<total_edep<{NE213A_edepMax_4*edepHigh}')
NE213A_sim_pen_6 = NE213A_sim_pen_6.query(f'{NE213A_edepMax_6*edepLow}<total_edep<{NE213A_edepMax_6*edepHigh}')

#randomize binning isotropic
y, x = np.histogram(NE213A_sim_iso_2.optPhotonSumQE, bins=4096, range=[0, 4096]) 
NE213A_sim_iso_2.optPhotonSumQE = prodata.getRandDist(x, y)

y, x = np.histogram(NE213A_sim_iso_4.optPhotonSumQE, bins=4096, range=[0, 4096]) 
NE213A_sim_iso_4.optPhotonSumQE = prodata.getRandDist(x, y)

y, x = np.histogram(NE213A_sim_iso_6.optPhotonSumQE, bins=4096, range=[0, 4096]) 
NE213A_sim_iso_6.optPhotonSumQE = prodata.getRandDist(x, y)

#randomize binning pencilbeam
y, x = np.histogram(NE213A_sim_pen_2.optPhotonSumQE, bins=4096, range=[0, 4096]) 
NE213A_sim_pen_2.optPhotonSumQE = prodata.getRandDist(x, y)

y, x = np.histogram(NE213A_sim_pen_4.optPhotonSumQE, bins=4096, range=[0, 4096]) 
NE213A_sim_pen_4.optPhotonSumQE = prodata.getRandDist(x, y)

y, x = np.histogram(NE213A_sim_pen_6.optPhotonSumQE, bins=4096, range=[0, 4096]) 
NE213A_sim_pen_6.optPhotonSumQE = prodata.getRandDist(x, y)

#smearing simulation
NE213A_sim_iso_2.optPhotonSumQE = NE213A_sim_iso_2.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, 0.2705))
NE213A_sim_iso_4.optPhotonSumQE = NE213A_sim_iso_4.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, 0.10625))
NE213A_sim_iso_6.optPhotonSumQE = NE213A_sim_iso_6.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, 0.0515))

NE213A_sim_pen_2.optPhotonSumQE = NE213A_sim_pen_2.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, 0.2705))
NE213A_sim_pen_4.optPhotonSumQE = NE213A_sim_pen_4.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, 0.10625))
NE213A_sim_pen_6.optPhotonSumQE = NE213A_sim_pen_6.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, 0.0515))

#calibrate isotropic
detector = 'NE213A'
NE213A_sim_iso_2.optPhotonSumQE = prodata.calibrateMyDetector(f'{pathData}/{detector}/Simcal/popt.npy', NE213A_sim_iso_2.optPhotonSumQE, inverse=False)
NE213A_sim_iso_4.optPhotonSumQE = prodata.calibrateMyDetector(f'{pathData}/{detector}/Simcal/popt.npy', NE213A_sim_iso_4.optPhotonSumQE, inverse=False)
NE213A_sim_iso_6.optPhotonSumQE = prodata.calibrateMyDetector(f'{pathData}/{detector}/Simcal/popt.npy', NE213A_sim_iso_6.optPhotonSumQE, inverse=False)

NE213A_sim_pen_2.optPhotonSumQE = prodata.calibrateMyDetector(f'{pathData}/{detector}/Simcal/popt.npy', NE213A_sim_pen_2.optPhotonSumQE, inverse=False)
NE213A_sim_pen_4.optPhotonSumQE = prodata.calibrateMyDetector(f'{pathData}/{detector}/Simcal/popt.npy', NE213A_sim_pen_4.optPhotonSumQE, inverse=False)
NE213A_sim_pen_6.optPhotonSumQE = prodata.calibrateMyDetector(f'{pathData}/{detector}/Simcal/popt.npy', NE213A_sim_pen_6.optPhotonSumQE, inverse=False)

#bin simulations
y_NE213A_iso_2, x_NE213A_iso_2 = np.histogram(NE213A_sim_iso_2.optPhotonSumQE, bins=numBins2)
x_NE213A_iso_2 = prodata.getBinCenters(x_NE213A_iso_2)

y_NE213A_iso_4, x_NE213A_iso_4 = np.histogram(NE213A_sim_iso_4.optPhotonSumQE, bins=numBins4)
x_NE213A_iso_4 = prodata.getBinCenters(x_NE213A_iso_4)

y_NE213A_iso_6, x_NE213A_iso_6 = np.histogram(NE213A_sim_iso_6.optPhotonSumQE, bins=numBins6)
x_NE213A_iso_6 = prodata.getBinCenters(x_NE213A_iso_6)


y_NE213A_pen_2, x_NE213A_pen_2 = np.histogram(NE213A_sim_pen_2.optPhotonSumQE, bins=numBins2pen)
x_NE213A_pen_2 = prodata.getBinCenters(x_NE213A_pen_2)

y_NE213A_pen_4, x_NE213A_pen_4 = np.histogram(NE213A_sim_pen_4.optPhotonSumQE, bins=numBins4pen)
x_NE213A_pen_4 = prodata.getBinCenters(x_NE213A_pen_4)

y_NE213A_pen_6, x_NE213A_pen_6 = np.histogram(NE213A_sim_pen_6.optPhotonSumQE, bins=numBins6pen)
x_NE213A_pen_6 = prodata.getBinCenters(x_NE213A_pen_6)

#set integral to 1
y_NE213A_iso_2 = prodata.integralNorm(y_NE213A_iso_2, forcePositive=True)
y_NE213A_iso_4 = prodata.integralNorm(y_NE213A_iso_4, forcePositive=True)
y_NE213A_iso_6 = prodata.integralNorm(y_NE213A_iso_6, forcePositive=True)

y_NE213A_pen_2 = prodata.integralNorm(y_NE213A_pen_2, forcePositive=True)
y_NE213A_pen_4 = prodata.integralNorm(y_NE213A_pen_4, forcePositive=True)
y_NE213A_pen_6 = prodata.integralNorm(y_NE213A_pen_6, forcePositive=True)

y_NE213A_data_2 = prodata.integralNorm(y_NE213A_data_2, forcePositive=True)
y_NE213A_data_4 = prodata.integralNorm(y_NE213A_data_4, forcePositive=True)
y_NE213A_data_6 = prodata.integralNorm(y_NE213A_data_6, forcePositive=True)

#LY locations
NE213A_sim_loc_2 = 0.551
NE213A_sim_loc_4 = 1.552
NE213A_sim_loc_6 = 2.886

#Make figure

xMin = 0.2
xMax = 3.7

fs.set_style(textwidth=345)

plt.subplot(3,1,1)
penScale = 0.5
plt.scatter(x_NE213A_data_2, y_NE213A_data_2, s=2, color='black', zorder=3)
plt.errorbar(x_NE213A_data_2, y_NE213A_data_2, yerr=y_NE213A_data_2*0.01, linestyle='none', color='black', zorder=2)
plt.step(x_NE213A_iso_2, y_NE213A_iso_2, lw=0.5, color='black', ls='solid')
plt.step(x_NE213A_pen_2, y_NE213A_pen_2*penScale, lw=0.5, color='black')
plt.fill_between(x_NE213A_pen_2, y1=y_NE213A_pen_2*penScale, color=neutron_color, edgecolor='none', step='pre', zorder=1)
plt.vlines(NE213A_sim_loc_2, ymin=0, ymax=np.max(y_NE213A_data_2[:-10])*1.1, lw=1, ls='dashed', color='black')
plt.xlim([xMin, xMax])
plt.xlabel('Counts')
plt.ylim([0, 0.137])
plt.xticks([])
plt.yticks([])

plt.subplot(3,1,2)
penScale = 0.25
plt.scatter(x_NE213A_data_4, y_NE213A_data_4, s=2, color='black', zorder=3)
plt.errorbar(x_NE213A_data_4, y_NE213A_data_4, yerr=y_NE213A_data_4*0.01, linestyle='none', color='black', zorder=2)
plt.step(x_NE213A_iso_4, y_NE213A_iso_4, lw=0.5, color='black', ls='solid')
plt.step(x_NE213A_pen_4, prodata.dataAveraging(y_NE213A_pen_4, 2)*penScale, lw=0.5, color='black')
plt.fill_between(x_NE213A_pen_4, y1=prodata.dataAveraging(y_NE213A_pen_4, 2)*penScale, color=neutron_color, edgecolor='none', step='pre', zorder=1)
plt.vlines(NE213A_sim_loc_4, ymin=0, ymax=np.max(y_NE213A_data_4[:-20])*1.1, lw=1, ls='dashed', color='black')
plt.xlim([xMin, xMax])
plt.xlabel('Counts')
plt.ylim([0, 0.073])
plt.xticks([])
plt.yticks([])
plt.ylabel('Counts')

plt.subplot(3,1,3)
penScale = 0.15
plt.scatter(x_NE213A_data_6, y_NE213A_data_6, s=2, color='black', zorder=3)
plt.errorbar(x_NE213A_data_6, y_NE213A_data_6, yerr=y_NE213A_data_6*0.01, linestyle='none', color='black', zorder=2)
plt.step(x_NE213A_iso_6, y_NE213A_iso_6, lw=0.5, color='black', ls='solid')
plt.step(x_NE213A_pen_6, prodata.dataAveraging(y_NE213A_pen_6, 2)*penScale, lw=0.5, color='black')
plt.fill_between(x_NE213A_pen_6, y1=prodata.dataAveraging(y_NE213A_pen_6, 2)*penScale, color=neutron_color, edgecolor='none', step='pre', zorder=1)
plt.vlines(NE213A_sim_loc_6, ymin=0, ymax=np.max(y_NE213A_data_6[:-20])*1.1, lw=1, ls='dashed', color='black')
plt.xlim([xMin, xMax])
plt.ylim([0, 0.045])
plt.yticks([])
plt.xlabel('Scintillation Light Yield [MeV$_{ee}$]')


plt.tight_layout()
plt.subplots_adjust(hspace = .001, wspace = .001)
plt.savefig('/home/gheed/Documents/writing/mauritzson_PhD_thesis_2023/figures/neutron_LY_method_A.pdf')

plt.show()

#make inset figure


fs.set_style(textwidth=345/2.2)
plt.vlines(edepMaxLoc_4*edepLow, ymin=1, ymax=np.max(y_NE213A_edep_4)*1.1, lw=1, color='black', ls='dashed')
plt.vlines(edepMaxLoc_4*edepHigh, ymin=1, ymax=np.max(y_NE213A_edep_4)*1.1, lw=1, color='black', ls='dashed')

plt.step(x_NE213A_edep_4, y_NE213A_edep_4, lw=1, color='black')
plt.fill_between(x_NE213A_edep_4, 0, y_NE213A_edep_4, color=neutron_color, step='pre')
plt.ylim(1, np.max(y_NE213A_edep_4)*1.1)
plt.xlim(4001, 4007)
plt.yticks([])
plt.xticks([])
plt.ylabel('Counts')
plt.xlabel('Deposited Neutron Energy')

plt.savefig('/home/gheed/Documents/writing/mauritzson_PhD_thesis_2023/figures/neutron_LY_method_B.pdf')

plt.show()



# ##########################################################
# ######### Appendix G4 parameters #########################
# ######### 2x2 plot of scintillation light yields #########
# ##########################################################

NE213A = np.array([     0.054171164687537576, 0.08393806725040975, 0.11559913637999286, 0.16968533845108147, 0.24621416576706268,
                        0.45356793011075136, 0.743168006077327, 0.8832363759945625, 0.9494847267202433, 0.9914162768381913,
                        0.9828875294870262, 0.8809024029427053, 0.7920015193314942, 0.7311358002478912, 0.7067190436208071,
                        0.7056595098156813, 0.7008816120906802, 0.6830044780296674, 0.6567185438407102, 0.6182829554995803,
                        0.5452650833633204, 0.48253768341929615, 0.43101535324457285, 0.39351685258486313, 0.36069879253128656,
                        0.33160409419855297, 0.30718483867098445, 0.28557434728719366, 0.2676972132261807, 0.24982757586661886,
                        0.23382211826796206, 0.21781666066930505, 0.2027457918515856, 0.189546599496222, 0.17727949702131074,
                        0.16314571588501048, 0.15274779097197277, 0.14141527727799774, 0.13101735236495982, 0.12249110391427775,
                        0.11303026668265992])

EJ305 = np.array([      0.000, 0.0201, 0.072, 0.164, 0.255,
                        0.381, 0.519, 0.726, 0.852, 0.956, 
                        0.990, 0.986, 0.956, 0.922, 0.873,
                        0.821, 0.748, 0.705, 0.642, 0.597,
                        0.556, 0.513, 0.477, 0.455, 0.430,
                        0.407, 0.391, 0.348, 0.303, 0.265,
                        0.233, 0.197, 0.174, 0.152, 0.131,
                        0.111, 0.097, 0.083, 0.072,0.059,
                        0.047])

EJ331 = np.array([      0.011, 0.026, 0.084, 0.163, 0.257,
                        0.380, 0.531, 0.714, 0.866, 0.972, 
                        0.993, 0.983, 0.940, 0.911, 0.866, 
                        0.800, 0.743, 0.683, 0.628, 0.582,
                        0.541, 0.507, 0.471, 0.446, 0.422,
                        0.403, 0.378, 0.338, 0.299, 0.255,
                        0.221, 0.193, 0.166, 0.143, 0.122,
                        0.107, 0.092, 0.081, 0.069, 0.054,
                        0.048])

EJ321P_mod = np.array([ 1.        , 1.        , 1.        , 1.        , 1.        ,
                        1.        , 1.        , 1.        , 1.        , 1.0001525 ,
                        1.        , 1.00648128, 1.02653081, 1.05506231, 1.03046661,
                        1.06146072, 1.06393152, 1.07885053, 1.10250623, 1.09796947,
                        1.13772603, 1.11435867, 1.15155508, 1.13487906, 1.1644532 ,
                        1.16585829, 1.19213957, 1.16463344, 1.17060455, 1.15915749,
                        1.16815522, 1.20589867, 1.18096969, 1.24033491, 1.20878113,
                        1.22496907, 1.22274455, 1.26268104, 1.27502275, 1.05033746,
                        1.27202393])

EJ321P = np.array([     0.000, 0.043, 0.105, 0.147, 0.239,
                        0.429, 0.534, 0.648, 0.917, 1.000, 
                        1.000, 0.979, 0.955, 0.913, 0.850, 
                        0.819, 0.734, 0.696, 0.628, 0.591, 
                        0.544, 0.507, 0.472, 0.451, 0.433, 
                        0.412, 0.375, 0.345, 0.287, 0.257, 
                        0.220, 0.193, 0.174, 0.154, 0.120, 
                        0.110, 0.099, 0.088, 0.083, 0.060,  
                        0.055]) * EJ321P_mod


#FIGURE: Emissions spectra
wavelength = np.arange(400, 502.5, 2.5)

fs.set_style(textwidth=345)
plt.subplot(2,2,1)
plt.step(wavelength, NE213A, color=NE213A_color, label='NE 213A', lw=1)
plt.legend()
plt.xlim([390, 510])
plt.ylim([0, 1.1])
plt.xticks([])


plt.subplot(2,2,2)
plt.step(wavelength, EJ305, color=EJ305_color, label='EJ 305', lw=1)
plt.legend()
plt.xlim([390, 510])
plt.ylim([0, 1.1])
plt.xticks([])
plt.yticks([])


plt.subplot(2,2,3)
plt.step(wavelength, EJ331, color=EJ331_color, label='EJ 331', lw=1)
plt.legend()
plt.xlim([390, 510])
plt.ylim([0, 1.1])
plt.ylabel('Intensity')
plt.xlabel('Wavelength [nm]')

plt.subplot(2,2,4)
plt.step(wavelength, EJ321P, color=EJ321P_color, label='EJ 321P', lw=1)
plt.legend()
plt.xlim([390, 510])
plt.ylim([0, 1.1])
plt.yticks([])

plt.legend()
plt.tight_layout()
plt.subplots_adjust(hspace = .001, wspace = .001)

plt.savefig('/home/gheed/Documents/writing/mauritzson_PhD_thesis_2023/figures/appendix_simulation_ligh_yield.pdf')

plt.show()


#MAKE TABLE
for i in range(len(wavelength)):
        print(f'{wavelength[i]} & {np.round(NE213A[i],3)} & {np.round(EJ305[i],3)} & {np.round(EJ331[i],3)} & {np.round(EJ321P[i],3)}')



#FIGURE: PMT efficiency curve
PMT_eff = np.array([0.1251771909405015,1.3638154587490412,3.1777072284846,5.256989135308206,7.424855190279288,9.814000461941873,12.15885365953072,14.194069934377076,16.229240920804138,18.485600547084097,20.43232325062158,22.954163590828184,24.94499721476221,26.891448187783897,28.395431304261184,29.36854356972378,29.94329889903852,29.8985539407718,29.63248447739428,29.056461072339186,28.480709397799885,27.28518570516333,25.77993451294569,22.637189944159374,19.98152232492629,17.63567278211288,13.298083846979484,9.978397423994707,7.499898101056573,3.8261921044169753,2.0997522723464357,0.9039568491940848,0.2172259031642909,-0.0711933951369339])
PMT_E = np.array([278.55593346225436,283.97605148387504,288.81285466493364,295.3959792941347,300.37770541694783,304.6311937574443,309.6853813511347,312.9751321289633,317.0836975277052,322.10165438595703,325.35517442834697,330.48182349291466,336.2099027657638,344.37631053363344,352.36156462430995,358.49180506052795,366.915451050012,375.9042965123389,386.4401943778957,400.9433577739836,410.5336334445919,425.60199631352265,440.5435516084182,461.3653552650052,475.8359109991984,488.7956450656003,512.4035922774189,532.3341198422153,543.6018785636325,564.2062978075877,578.2384616429732,598.2197122373839,614.3152164560001,628.9361297422637])

PMT_interpol = interpolate.splrep(PMT_E, PMT_eff, s=5)

wavelengthFine = np.arange(250,700,1)

fs.set_style(textwidth=345)
plt.plot(wavelengthFine, interpolate.splev(wavelengthFine, PMT_interpol, der=0), color='black', label='PMT efficiency', lw=1)
# plt.scatter(PMT_E, PMT_eff, color='black', lw=1)
plt.legend()
plt.xlim([300, 620])
plt.ylim([0, 32])
plt.vlines(400, 0, 30)
plt.vlines(500, 0, 30)
plt.ylabel('Photoelectric efficiency [\%]')
plt.xlabel('Wavelength [nm]')
plt.legend()

plt.savefig('/home/gheed/Documents/writing/mauritzson_PhD_thesis_2023/figures/appendix_simulation_PMT_QE.pdf')
plt.show()


# ##########################################################
# ######### Mutiple neutron scattering #####################
# ######### For NE 213 #####################################
# ##########################################################
from scipy import interpolate

total    = pd.read_csv('/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/PhD_thesis/data/multiple_scattering/total.csv', names=['energy','efficiency'], sep=',')
nH       = pd.read_csv('/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/PhD_thesis/data/multiple_scattering/nH.csv', names=['energy','efficiency'], sep=',')
nHnH     = pd.read_csv('/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/PhD_thesis/data/multiple_scattering/nHnH.csv', names=['energy','efficiency'], sep=',')
nCnH     = pd.read_csv('/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/PhD_thesis/data/multiple_scattering/nCnH.csv', names=['energy','efficiency'], sep=',')
nCnHnH   = pd.read_csv('/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/PhD_thesis/data/multiple_scattering/nCnHnH.csv', names=['energy','efficiency'], sep=',')
nalphann = pd.read_csv('/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/PhD_thesis/data/multiple_scattering/nalpha_nn.csv', names=['energy','efficiency'], sep=',')

energyFine = np.arange(0.9, 15, 0.001)
total_func              = interpolate.splrep(total.energy,      total.efficiency)
nH_func                 = interpolate.splrep(nH.energy,         nH.efficiency)
nHnH_func               = interpolate.splrep(nHnH.energy,       nHnH.efficiency)
nCnH_func               = interpolate.splrep(nCnH.energy,       nCnH.efficiency)
nCnHnH_func             = interpolate.splrep(nCnHnH.energy,     nCnHnH.efficiency)
nalphann_func           = interpolate.splrep(nalphann.energy,   nalphann.efficiency)


total_values = interpolate.splev(energyFine, total_func, der=0)
nH_values = interpolate.splev(energyFine, nH_func, der=0)
nHnH_values = interpolate.splev(energyFine, nHnH_func, der=0)
nCnH_values = interpolate.splev(energyFine, nCnH_func, der=0)
alphann_values = interpolate.splev(energyFine, nalphann_func, der=0)


fs.set_style(textwidth=345)

plt.plot(energyFine, nH_values/total_values*100, label='nH')
plt.plot(energyFine, nHnH_values/total_values*100, label='nHnH')
plt.plot(energyFine, nCnH_values/total_values*100, label='nCnH')
# plt.plot(energyFine, alphann_values/total_values*100, label='alpha')
plt.xlabel('Neutron Energy [MeV]')
plt.ylabel('Light Contribution [\%]')
plt.ylim([0,100])
plt.xlim([1, 7])
plt.legend()
plt.show()





#STUDY OF MUTIPLE SCATTERING EVENTS
#Load simulation
names = [ 'evtNum', 
        'optPhotonSum', 
        'optPhotonSumQE', 
        'optPhotonSumCompton', 
        'optPhotonSumComptonQE', 
        'optPhotonSumNPQE', 
        'xLoc', 
        'yLoc', 
        'zLoc', 
        'CsMin', 
        'optPhotonParentID', 
        'optPhotonParentCreatorProcess', 
        'optPhotonParentStartingEnergy',
        'total_edep',
        'gamma_edep',
        'proton_edep',
        'nScatters',
        'nScattersProton']

#load sim
energy = 4000
detector = 'EJ305'
sim_iso = pd.read_csv(f"{pathSim}/{detector}/neutrons/neutron_range/{int(energy)}keV/isotropic/1M/CSV_optphoton_data_sum.csv", names=names)
sim_pen = pd.read_csv(f"{pathSim}/{detector}/neutrons/neutron_range/{int(energy)}keV/pencilbeam/300k/CSV_optphoton_data_sum.csv", names=names)

# sim_iso = pd.read_csv(f"{pathSim}/{detector}/neutrons/neutron_range/{int(energy)}keV/isotropic/CSV_optphoton_data_sum.csv", names=names)
# sim_pen = pd.read_csv(f"{pathSim}/{detector}/neutrons/neutron_range/{int(energy)}keV/pencilbeam/CSV_optphoton_data_sum.csv", names=names)

#find maximum edep
y, x = np.histogram(sim_pen.total_edep, bins=np.arange(0, energy*1.2, .3))
x = prodata.getBinCenters(x)
edepMax = x[np.argmax(y)]


#Cut pencilbeam on edep
sim_pen = sim_pen.query(f'{edepMax*edepLow}<total_edep<{edepMax*edepHigh}')

#randomize binning isotropic
y, x = np.histogram(sim_iso.optPhotonSumQE, bins=4096, range=[0, 4096]) 
sim_iso.optPhotonSumQE = prodata.getRandDist(x, y)

#randomize binning pencilbeam

y, x = np.histogram(sim_pen.optPhotonSumQE, bins=4096, range=[0, 4096]) 
sim_pen.optPhotonSumQE = prodata.getRandDist(x, y)

#make plot
numBins = np.arange(25,600,7)

y,x = np.histogram(sim_iso.optPhotonSumQE, bins=numBins)
x = prodata.getBinCenters(x)
plt.step(x, y, color='blue', ls='solid', label='iso')

y,x = np.histogram(sim_iso.query('nScattersProton==1 and nScattersProton==1').optPhotonSumQE, bins=numBins)
x = prodata.getBinCenters(x)
plt.step(x, y*2.45, color='blue', ls='dashed', label='iso nH')

y,x = np.histogram(sim_pen.optPhotonSumQE, bins=numBins)
x = prodata.getBinCenters(x)
plt.step(x, y, color='red', ls='solid', label='pen')

y,x = np.histogram(sim_pen.query('nScattersProton==1 and nScattersProton==1').optPhotonSumQE, bins=numBins)
x = prodata.getBinCenters(x)
plt.step(x, y*4.2, color='red', ls='dashed', label='pen nH')

plt.legend()
plt.show()

