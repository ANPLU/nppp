#!/usr/bin/env python3
from re import I
import sys
from tkinter import X
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
from numpy.core.arrayprint import SubArrayFormat
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit

# from matplotlib.colors import LinearSegmentedColormap


# IMPORT FROM LIBRARY DIRECTORY
sys.path.insert(0, "../library/")
sys.path.insert(0, "../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "../")
sys.path.insert(0, "data_analysis/")

import processing_math as promath
import processing_data as prodata
import figure_style as fs


#########################################
####### MIRRORBOR GAMMA SPECTRUM ########
#########################################

mirrobor = pd.read_csv('/media/gheed/Seagate_Expansion_Drive1/data/MG_activation_analysis/activated_mirrobor/Mirrobor.txt', sep=' ')
mirrobor_bg = pd.read_csv('/media/gheed/Seagate_Expansion_Drive1/data/MG_activation_analysis/background/Big_castle_Background.txt', sep=' ')

liveTime = 61030.829
liveTime_bg = 73422.104
# plt.step(mirrobor.energy, (mirrobor.counts/liveTime), label='mirrobor')

# plt.step(mirrobor_bg.energy, (mirrobor_bg.counts/liveTime_bg), label='background')
# E = np.array([  140, 
#                 182.2, 
#                 195.8, 
#                 237.6, 
#                 509.60, #
#                 655.83, 
#                 1457.67, 
#                 1709, #
#                 1764,#
#                 2217.69,
#                 2615.2])#

# dy = np.array([ (7023-6002), 
#                 (7810-6343), 
#                 (7062-6121), 
#                 (7545-5797), 
#                 (11470-11010), #
#                 (8554-7815), 
#                 (3628-3317),
#                 (1529-1457),#
#                 (1243-1181),#
#                 (3872-3689),
#                 (771-749)])#


# E = np.array([  655.83, 
#                 1457.67, 
#                 2217.69,
#                 2615.2])#


# dy = np.array([  740/7815,
#                  315/3316,
#                  182/3688,
#                 0])


# def oneOverX(x, A):
#     return A*1/(x*x)

# def expFunc(x, A, B, C):
#     return A+B*np.exp(-C*x)

# def expFuncLn(x, B, C, D):
#     return np.log(B)-C*x-D*x*x

# def linearFuncMod(x, A, B):
#     yVal = np.empty(0)
#     for val in x:
#         if val<586:
#             yVal = np.append(yVal, 0.055)
#         elif val<1450 and val>=586:
#             yVal = np.append(yVal, 0.07)
#         else:
#             yVal = np.append(yVal, (A*val + B))
#     return yVal

# popt, pcov = curve_fit(promath.linearFunc, E, dy)
# A = popt[0]
# B = popt[1]

binCoeff = 32
R = liveTime/liveTime_bg
x, y = prodata.reBinning(mirrobor.energy, mirrobor.counts, binCoeff)
xbg, ybg = prodata.reBinning(mirrobor_bg.energy, (mirrobor_bg.counts*R), binCoeff)
xbg_raw, ybg_raw = prodata.reBinning(mirrobor_bg.energy, (mirrobor_bg.counts), binCoeff)
# xbg_mod, ybg_mod = prodata.reBinning(mirrobor_bg.energy, (mirrobor_bg.counts*R*(linearFuncMod(mirrobor_bg.energy, A, B)+1)),binCoeff)

fs.set_style(textwidth=345)
plt.subplot(3,1,1)

plt.yscale('log')
plt.step(x,y, label='Mirrobor plus Background', lw=1, color='royalblue')
plt.hlines(1, 0, 5000, color='black')
plt.legend()
plt.ylabel('counts')
plt.xlim([0, 5000])
plt.ylim([0.1, np.max(y)*1.2])


plt.subplot(3,1,2)
plt.step(xbg, ybg, label='Corrected Background', lw=1, color='orange')
plt.step(xbg_raw, ybg_raw, label='Background', lw=1, color='tomato')
plt.hlines(1, 0, 5000, color='black')
plt.legend()
plt.ylabel('counts')
plt.xlim([0,5000])
plt.ylim([0.1, np.max(ybg_raw)*1.2])
plt.yscale('log')

plt.subplot(3,1,3)
plt.step(x, y - ybg, label='Mirrobor', lw=1, color='black')
plt.xlim([0,5000])
plt.ylim([np.min(y - ybg)*1.2, np.max(y - ybg)*1.2])
plt.ylabel('counts')
plt.xlabel('Energy [keV]')
plt.legend()
plt.show()


############################################
####### HP ALUMINIUM GAMMA SPECTRUM ########
############################################



al = pd.read_csv('/media/gheed/Seagate_Expansion_Drive1/data/MG_activation_analysis/activated_hp_al/Aluminium.txt', sep=' ')
al_bg = pd.read_csv('/media/gheed/Seagate_Expansion_Drive1/data/MG_activation_analysis/activated_bor/Mirrobor_Background.txt', sep=' ')

liveTimeAl = 155284.604
liveTime_bg = 232179.91999999998

binCoeff = 40
R = liveTimeAl/liveTime_bg
x, y = prodata.reBinning(al.energy, al.counts, binCoeff)
xbg_raw, ybg_raw = prodata.reBinning(al_bg.energy, (al_bg.counts), binCoeff)
xbg, ybg = prodata.reBinning(al_bg.energy, (al_bg.counts*R), binCoeff)

fs.set_style(textwidth=345)
plt.subplot(3,1,1)
plt.step(x,y, lw=1, color='royalblue', label='Aluminium plus Background')
plt.ylim([5, 40000])
plt.xlim([430, 5000])
plt.yscale('log')
plt.legend()
plt.ylabel('counts')

plt.subplot(3,1,2)
plt.step(xbg, ybg, label='Corrected Background', lw=1, color='orange')
plt.step(xbg_raw, ybg_raw, label='Background', lw=1, color='tomato')
plt.ylim([5, 40000])
plt.xlim([430, 5000])
plt.yscale('log')
plt.legend()
plt.ylabel('counts')

plt.subplot(3,1,3)
plt.step(x, y-ybg, lw=1, color='black', label='Aluminium')
plt.ylim([-1000, 2000])
plt.xlim([430, 5000])
plt.legend()
plt.ylabel('counts')
plt.xlabel('Energy [keV]')

# plt.step(xbg_raw, ybg_raw)
plt.show()




#####################################
####### Gd2O3 GAMMA SPECTRUM ########
#####################################

gd = pd.read_csv('/media/gheed/Seagate_Expansion_Drive1/data/MG_activation_analysis/activated_gd/Gd2O3.txt', sep=' ')
gd_bg = pd.read_csv('/media/gheed/Seagate_Expansion_Drive1/data/MG_activation_analysis/activated_bor/Mirrobor_Background.txt', sep=' ')

liveTimeGd = 397362.789
liveTime_bg = 232179.91999999998

binCoeff = 2
R = liveTimeGd/liveTime_bg
x, y = prodata.reBinning(gd.energy, gd.counts, binCoeff)
xbg_raw, ybg_raw = prodata.reBinning(gd_bg.energy, (gd_bg.counts), binCoeff)
xbg, ybg = prodata.reBinning(gd_bg.energy, (gd_bg.counts*R), binCoeff)

fs.set_style(textwidth=345)
plt.subplot(3,1,1)
plt.step(x,y, lw=1, color='royalblue', label='Gd203 plus Background')
plt.ylim([5, 40000])
plt.xlim([-20, 5000])
plt.yscale('log')
plt.legend()
plt.ylabel('counts')

plt.subplot(3,1,2)
plt.step(xbg, ybg, label='Corrected Background', lw=1, color='red',alpha=0.7)
plt.step(xbg_raw, ybg_raw, label='Background', lw=1, color='tomato')
plt.ylim([5, 40000])
plt.xlim([430, 5000])
plt.yscale('log')
plt.legend()
plt.ylabel('counts')

plt.subplot(3,1,3)
plt.step(x, y-ybg, lw=1, color='black', label='Gd203')
plt.ylim([-1000, 2000])
plt.xlim([430, 5000])
plt.legend()
plt.ylabel('counts')
plt.xlabel('Energy [keV]')

# plt.step(xbg_raw, ybg_raw)
plt.show()




##############################################
####### Activate EJ331 GAMMA SPECTRUM ########
##############################################

EJ331 = pd.read_csv('/media/gheed/Seagate_Expansion_Drive1/data/MG_activation_analysis/activated_EJ331/EJ331.txt', sep=' ')
EJ331_bg = pd.read_csv('/media/gheed/Seagate_Expansion_Drive1/data/MG_activation_analysis/activated_EJ331/Big_castle_Background.txt', sep=' ')

liveTimeEJ331 = 224544.735
liveTime_bg = 73422.104

binCoeff = 8
R = liveTimeEJ331/liveTime_bg
x, y = prodata.reBinning(EJ331.energy, EJ331.counts, binCoeff)
xbg_raw, ybg_raw = prodata.reBinning(EJ331_bg.energy, (EJ331_bg.counts), binCoeff)
xbg, ybg = prodata.reBinning(EJ331_bg.energy, (EJ331_bg.counts*R), binCoeff)

fs.set_style(textwidth=345)
plt.subplot(3,1,1)
plt.step(x, y, lw=1, color='royalblue', label='EJ331 plus Background')
plt.step(xbg, ybg, label='Corrected Background', lw=1, color='red',alpha=1)
plt.ylim([5, np.max(y)*2])
plt.xlim([-20, 5000])
plt.yscale('log')
plt.legend()
plt.ylabel('counts')

plt.subplot(3,1,2)
plt.step(xbg, ybg, label='Corrected Background', lw=1, color='red',alpha=0.7)
plt.step(xbg_raw, ybg_raw, label='Background', lw=1, color='tomato')
plt.ylim([5, np.max(ybg)*2])
plt.xlim([-20, 5000])
plt.yscale('log')
plt.legend()
plt.ylabel('counts')

plt.subplot(3,1,3)
plt.step(x, y-ybg, lw=1, color='black', label='EJ331')
plt.ylim([-20, np.max(y-ybg)*1.2])
plt.xlim([-20, 5000])
plt.legend()
plt.ylabel('counts')
plt.xlabel('Energy [keV]')

# plt.step(xbg_raw, ybg_raw)
plt.show()




# #RAW on RAW
binCoeff = 4

x, y = prodata.reBinning(EJ331.energy, EJ331.counts, binCoeff)

plt.step(x,y, lw=1, color='royalblue', label='EJ331 plus Background')
plt.ylim([5, 50000])
plt.xlim([-20, 5000])
plt.yscale('log')
plt.legend()
plt.ylabel('counts')

x, y = prodata.reBinning(gd.energy, gd.counts, binCoeff)

plt.step(x,y*0.16, lw=1, color='tomato', label='Gd2O3 plus Background')
plt.ylim([5, 50000])
plt.xlim([-20, 5000])
plt.yscale('log')
plt.legend()
plt.ylabel('counts')
plt.show()


##############################################
####### Activate MG 'grids' ##################
##############################################

MG = pd.read_csv('/media/gheed/Seagate_Expansion_Drive1/data/MG_activation_analysis/activated_MG/Detector_modules.txt', sep=' ')
MG_bg = pd.read_csv('/media/gheed/Seagate_Expansion_Drive1/data/MG_activation_analysis/background/Big_castle_Background.txt', sep=' ')

binCoeff = 12
x, y = prodata.reBinning(EJ331.energy, EJ331.counts, binCoeff)
xbg, ybg = prodata.reBinning(EJ331_bg.energy, (EJ331_bg.counts), binCoeff)



liveTimeMG = 70974.889
liveTime_bg = 73422.104

binCoeff = 8
R = liveTimeMG/liveTime_bg
x, y = prodata.reBinning(MG.energy, MG.counts, binCoeff)
xbg_raw, ybg_raw = prodata.reBinning(MG_bg.energy, (MG_bg.counts), binCoeff)
xbg, ybg = prodata.reBinning(MG_bg.energy, (MG_bg.counts*R), binCoeff)

fs.set_style(textwidth=345)
plt.subplot(3,1,1)
plt.step(x,y, lw=1, color='royalblue', label='Gd203 plus Background')
plt.ylim([5, 40000])
plt.xlim([430, 5000])
plt.yscale('log')
plt.legend()
plt.ylabel('counts')

plt.subplot(3,1,2)
plt.step(xbg, ybg, label='Corrected Background', lw=1, color='red',alpha=0.7)
plt.step(xbg_raw, ybg_raw, label='Background', lw=1, color='tomato')
plt.ylim([5, 40000])
plt.xlim([430, 5000])
plt.yscale('log')
plt.legend()
plt.ylabel('counts')

plt.subplot(3,1,3)
plt.step(x, y-ybg, lw=1, color='black', label='Gd203')
plt.ylim([-1000, 2000])
plt.xlim([430, 5000])
plt.legend()
plt.ylabel('counts')
plt.xlabel('Energy [keV]')
# plt.step(xbg_raw, ybg_raw)
plt.show()


##############################################
####### Activate EJ331 EXP FITTING TEST ######
##############################################

EJ331 = pd.read_csv('/media/gheed/Seagate_Expansion_Drive1/data/MG_activation_analysis/activated_EJ331/EJ331.txt', sep=' ')
EJ331_bg = pd.read_csv('/media/gheed/Seagate_Expansion_Drive1/data/MG_activation_analysis/background/Big_castle_Background.txt', sep=' ')

binCoeff = 12
x, y = prodata.reBinning(EJ331.energy, EJ331.counts, binCoeff)
xbg, ybg = prodata.reBinning(EJ331_bg.energy, (EJ331_bg.counts), binCoeff)

min = 50
max = -20

def expFunc(x, A, B):
    return A*np.exp(-B*x)

#fitting
popt1, pcov1 = curve_fit(expFunc, x, y)
popt2, pcov2 = curve_fit(expFunc, xbg, ybg)

plt.subplot(2, 1, 1)
# plt.step(x[min:max], y[min:max], lw=1, color='royalblue', label='EJ331 plus Background')
yFit1 = y/expFunc(x, popt1[0], popt1[1])
plt.step(x, yFit1, color='royalblue',alpha=0.5, label='EJ331 plus background')

# plt.step(xbg[min:max], ybg[min:max], label='Corrected Background', lw=1, color='red',alpha=1)
yFit2 = ybg/expFunc(xbg, popt2[0], popt2[1])
plt.step(xbg, yFit2, color='tomato',alpha=0.5, label='Corrected background')

plt.step(x, yFit1-yFit2, color='black', label='EJ331')

plt.xlim([-20, 5000])
plt.yscale('log')
plt.legend()
plt.ylabel('Intensity [arb. units]')





liveTimeEJ331 = 224544.735
liveTime_bg = 73422.104
binCoeff = 50
R = liveTimeEJ331/liveTime_bg
x, y = prodata.reBinning(EJ331.energy, EJ331.counts, binCoeff)
xbg, ybg = prodata.reBinning(EJ331_bg.energy, (EJ331_bg.counts*R), binCoeff)

plt.step(x, y, color='royalblue', label='EJ331 plus background', alpha=0.5)
plt.step(xbg, ybg, color='tomato', label='Corrected background', alpha=0.5)
plt.step(x, y-ybg, color='black', label='EJ331')

plt.xlim([-20, 5000])
plt.yscale('log')
plt.legend()
plt.ylabel('Intensity [arb. units]')


plt.show()





#Comparison #2

EJ331 = pd.read_csv('/media/gheed/Seagate_Expansion_Drive1/data/MG_activation_analysis/activated_EJ331/EJ331.txt', sep=' ')
EJ331_bg = pd.read_csv('/media/gheed/Seagate_Expansion_Drive1/data/MG_activation_analysis/background/Big_castle_Background.txt', sep=' ')

binCoeff = 16
x, y = prodata.reBinning(EJ331.energy, EJ331.counts, binCoeff)
xbg, ybg = prodata.reBinning(EJ331_bg.energy, (EJ331_bg.counts), binCoeff)

# plt.step(x,y)
# plt.step(xbg,ybg)
# plt.show()

def expFunc(x, A, B):
    return A*np.exp(-B*x)

def func1(x, A):
    # return A*1/(x)
    return A*1/(x**1.5)

def func2(x, A, B, C, D):
    return A*(np.exp(-B*x + C) + D*x)
#fitting

start = 150
stop = 5700

xFit, yFit = prodata.binDataSlicer(x, y, start, stop)
xbgFit, ybgFit = prodata.binDataSlicer(xbg, ybg, start, stop)
popt1, pcov1 = curve_fit(func1, xFit, yFit)#, p0=[10e3, 2.41e-3, 1,1])
popt2, pcov2 = curve_fit(func1, xbgFit, ybgFit)#, p0=[7e3, 2.13e-3,1,1 ])

plt.step(xFit, yFit)
plt.step(xbgFit, ybgFit)
plt.plot(xFit, func1(xFit, popt1[0]))
plt.plot(xFit, func1(xFit, popt2[0]))
# popt3, pcov3 = curve_fit(promath.linearFunc, xFit, (yFit/ybgFit))
# popt4, pcov4 = curve_fit(promath.quadraticFunc, xFit, (yFit/ybgFit))
# plt.plot(xFit, promath.linearFunc(xFit, popt3[0], popt3[1]), color='black')
# plt.plot(xFit, promath.quadraticFunc(xFit, popt4[0], popt4[1], popt4[2]), color='black', linestyle='dashed')
# plt.hlines(np.mean(yFit/ybgFit), xmin=0, xmax=5700)
print(popt1)
print(popt2)
plt.show()

g = func1(xbg, popt1[0], popt1[1], popt1[2]) / func1(x, popt2[0], popt2[1], popt2[2])
plt.plot(x, g)
plt.plot(x, func2(x, popt1[0], popt1[1], popt1[2], popt1[3]))
plt.plot(x, func2(x, popt2[0], popt2[1], popt2[2], popt2[3], ))

plt.step(x, y, lw=1, color='royalblue', label='EJ331 plus Background', alpha=0.5)
plt.step(xbg, ybg*g, label='Corrected Background', lw=1, color='tomato',alpha=0.5)
plt.step(x, y-(ybg*g), color='black', label='EJ331')

plt.xlim([-20, 5700])
plt.yscale('log')
plt.legend()
plt.ylabel('Intensity [arb. units]')
plt.show()




