import numpy as np
import math

#Dataframe stuff
import pandas as pd
import dask.dataframe as dd
from dask.diagnostics import ProgressBar
#Neural network stuff
# import keras

#Logging
import logging

import h5py

import helpers as hlp # helper routines

#                        _   _
# _____  _____ ___ _ __ | |_(_) ___  _ __  ___
#/ _ \ \/ / __/ _ \ '_ \| __| |/ _ \| '_ \/ __|
#  __/>  < (_|  __/ |_) | |_| | (_) | | | \__ \
#\___/_/\_\___\___| .__/ \__|_|\___/|_| |_|___/
#                 |_|
#
class Error(Exception):
    """Base class for exceptions in this module."""
    pass

class InputError(Error):
    """Exception raised for errors in the input.

    Attributes:
        expression -- input expression in which the error occurred
        message -- explanation of the error
    """

    def __init__(self, expression, message):
        self.expression = expression
        self.message = message


def get_tof(df, Nchannel, YchannelMask):
    """
    calculates time difference between events located shift rows apart.
    Arguments:
    df: dataframe
    Nchannel: integer giving the digitizer channel the neutron detector was connected tofdummy
    YchannelMask: list of integers giving the channels which the gamma-ray detectors were connected to
    """
    #for N active channels we have <=N waveforms per trigger (after throwing away empty windows).
    #Channel order is not preserved for each event number, so we must shift down and up by N rows.
    shiftMask = [x for x in range(-len(YchannelMask),0)] + [x for x in range(1, len(YchannelMask)+1)]

    #loop through shiftMask
    for shift in shiftMask:
        df['tofdummy'] =  ( ( 8*1000*df['ts'].astype(np.int64) + df['cfd_trig_rise'] )
                            - ( 8*1000*df['ts'].astype(np.int64).shift(shift) + df['cfd_trig_rise'].shift(shift) ) ).fillna(-999999).astype(np.int64, )
        df['tof_ch_shift'] = df['channel'].shift(shift).fillna(-1).astype(np.int8)
        #loop throgh Ychannel
        for y in YchannelMask:
            df['tof_ch%s_S%s'%(y, shift)] = df['tofdummy'].where( ((df['channel']==Nchannel) & (df['tof_ch_shift']==y)), -999999)

            #Collect all tof values across different shifts into channel specific tof columns df['tof_ch%s'%y]
            if shift == shiftMask[0]:
                #Initially set it equal to the tof corresponding to the first shift value
                df['tof_ch%s'%y] = df['tof_ch%s_S%s'%(y, shift)]
            else:
                #Afterwards overwrite entries which have no tof value with tof values corresponding to the next shift.
                df['tof_ch%s'%y] = df['tof_ch%s'%y].where(df['tof_ch%s'%y]!= -999999, df['tof_ch%s_S%s'%(y, shift)])

    #Throw away helper columns
    for shift in shiftMask:
        for y in YchannelMask:
            df = df.drop(['tof_ch%s_S%s'%(y, shift)], axis=1)
    df = df.drop(['tof_ch_shift', 'tofdummy'], axis=1)

    return df

def qdc(samples, baseline, trigger, gate, pre_trig):
    """
    @brief      Calculates QDC (pulse charge integral value)

    @details    Determines the QDC value by integrating the pulse over a variable gate length using the composite trapezoidal rule. Integration starts at trigger point minus a pre-trigger window. Returns value in units of ADC*gate sampling time (positive values for negative pulses)

    @param      samples: array of int. samples to integrate
    @param      baseline: float. value for the baseline of the samples (is subtracted)
    @param      trigger: int or float. sample index position (int) or fraction of index position (float) where the trigger occurred
    @param      gate: int. integration length. In units of sampling time.
    @param      pre_trig: int. shift integration window by this amount before the trigger.

    @return     return float
    """
    # Integrate along the given axis using the composite trapezoidal rule, subtracting the baseline.
    # Defined to be a positive value for negative pulses (therefore multiplied by -1)
    try:
        result = (-1) * np.trapz(samples[int(np.round(trigger))-pre_trig:int(np.round(trigger))+gate-pre_trig] - baseline)
    except ValueError:
        return np.nan
    return result

def pulse_integration(df, lg, sg, pre_trig=25, lg_offset=0, sg_offset=0, suffix=""):
    """
    @brief      Calculate PSD from pulse integrals.

    @details    Calculates long gate, short gate and PSD FoM for all samples in the column 'samples{suffix}' and stores the output in new columns called 'qdc_*{suffix}' where '*' is either lg, sg or ps.

    @param      col: string. What column of the df to use.
    @param      lg and sg: Gatelengths in units of sampling time. Single value will be used for all channels, or can be dictionaries mapping channel number to individual gatelenghts. Ex: lg = {1:500, 2:500, 3:400, ...}.
    @param      lg_offset, sg_offset: offset values that can be used to linearize the pulse shape parameter: PS = 1 - (Q_sg+sg_offset)/(Q_lg+lg_offset)
    @param      pretrig: number of samples before cfd trigger to start pulse integration.
    @return     return type
    """
    # Long gate
    if isinstance(lg, dict):
            # different integration length for each channel
        df[f'qdc_lg{suffix}'] = df.apply(lambda row:
                                         qdc(row[f"samples{suffix}"],
                                             row[f'baseline{suffix}'],
                                             row[f'CFD_rise{suffix}'],
                                             lg[int(suffix.strip('_ch'))],
                                             pre_trig),
                                             axis=1,
                                         meta=(f'qdc_lg{suffix}', np.float64))
    else:
        # same integration length for each channel
        df[f'qdc_lg{suffix}'] = df.apply(lambda row:
                                         qdc(row[f"samples{suffix}"],
                                             row[f'baseline{suffix}'],
                                             row[f'CFD_rise{suffix}'],
                                             lg,
                                             pre_trig),
                                             axis=1,
                                         meta=(f'qdc_lg{suffix}', np.float64))

    #Short gate
    if isinstance(sg, dict):
            # different integration length for each channel
        df[f'qdc_sg{suffix}'] = df.apply(lambda row:
                                        qdc(row[f"samples{suffix}"],
                                            row[f'baseline{suffix}'],
                                            row[f'CFD_rise{suffix}'],
                                            sg[int(suffix.strip('_ch'))],
                                            pre_trig),
                                            axis=1,
                                        meta=(f'qdc_lg{suffix}', np.float64))
    else:
        # same integration length for each channel
        df[f'qdc_sg{suffix}'] = df.apply(lambda row:
                                         qdc(row[f"samples{suffix}"],
                                             row[f'baseline{suffix}'],
                                             row[f'CFD_rise{suffix}'],
                                             sg,
                                             pre_trig),
                                             axis=1,
                                         meta=(f'qdc_lg{suffix}', np.float64))

    #Pulse-shape calulation. Get the Tail/total ratio
    df[f'qdc_ps{suffix}'] = (1 - (df[f'qdc_sg{suffix}'] + sg_offset)/(df[f'qdc_lg{suffix}'] + lg_offset)).astype(np.float32)
    return df


def tof_processing(df, Nchannel=None, Ychannel=None):
    """
    Method for calulating the time of flight of JADAQ data set.

    NOTE: Method only accepts data with one Neutron detector (Nchannel)
    NOTE: Method only works with singlerow=True data from dv.load_h5.da()

    Parameters:
    df............The DataFrame containing the data.
    Nchannel......int. Channel number of the neutron detector.
    Ychannel......List of int. Channel number(s) of the gamma detectors.
    """

    log = hlp.init_logging('tof_processing')  # set up logging
    log.info(f'Processing....')
    #~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~#
    #         CALCULATE Time-of-Flight  (singlerow=True)
    #~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~#
    try:
        #Loop through all active YAP channels.
        for thridx, Ych in enumerate(Ych for Ych in Ychannel):
            log.info(f"Preparing TOF calculations between channels '{Nchannel}' and '{Ych}'")
            df[f'tof_ch{Ych}'] = df[f'CFD_rise_ch{Nchannel}']-df[f'CFD_rise_ch{Ych}']
    except TypeError:
        #Handle expections were Ychannel is not an itterable.
        log.info(f"Preparing TOF calculations between channels '{Nchannel}' and '{Ychannel}'")
        df[f'tof_ch{Ych}'] = df[f'CFD_rise_ch{Nchannel}']-df[f'CFD_rise_ch{Ych}']
    return df

def cfd(samples, baseline, peakidx, CFD_frac, polarity='-', backwards=True):
    """
    Method for calulating the CFD fraction crossing point of a set of samples and determine time relative the events time-stamp.
    Returns CFD fraction crossing point + event time-stamp, in ps.

    Parameters:
    samples........The sample array to process
    baseline.......The baseline to subtract
    peakidx........The index value of the peak
    frac...........The fraction (baseline/peak amplitude) at which the CFD trigger determines the pulses' time-stamp.
    polarity.......The polarity of the pulse. 0: Negative, 1: Positive.
    backwards......Direction to search for crossing point (backwards or forwards in time)
    """
    # :::: CFD COARSE ::::
    # slice samples to search window before or after the peak position
    if backwards:
        start = 0
        stop = peakidx+1 # slice so that peak is included
    else:
        start = peakidx
        stop = -1
    peakamp = samples[peakidx] - baseline
    s = samples[start:stop]
    s = s - baseline
    try:
        if polarity == '-':
            candidates = np.where(s > CFD_frac * peakamp)
        else:
            candidates = np.where(s < CFD_frac * peakamp)
        if backwards:
            rise_index = candidates[0][-1] # last sample below/above frac*peakamp value (backward search)
        else:
            rise_index = candidates[0][0] # first sample below/above frac*peakamp value (forward search)
    except IndexError:
        # could not find crossing point
        return np.nan

    # :::: CFD FINE ::::
    # rise_index is the first bin after (forward) or before (backward) we crossed the CFD fraction threshold.
    # We now extrapolate between two index points to find a better resolution (in ps) of the CFD crossing point
    if rise_index-1 < 0 or rise_index+1>=len(s):
        # can't do the following calculations if index would go out of range
        return float(start+rise_index)
    if backwards:
        adj_index = rise_index+1
    else:
        adj_index = rise_index-1
    # Getting the slope of the pulse between the two samples points.
    slope_rise = s[adj_index] - s[rise_index] #divided by deltaT between samples
    # if slope is 0 we cannot perform linear interpolation
    if math.isclose(slope_rise, 0.0):
        return np.nan
    # Calculating the point between the two index that the CFD crossing occures.
    tfine_rise = (start + adj_index) + ((peakamp * CFD_frac - s[adj_index]) / slope_rise)
    return tfine_rise

def cfdX(samples, baseline, peakidx, CFDX_frac, CFDX_delay, polarity='-'):
    """
    CFD algorithm using the zero crossing point method.
    Return the zero crossing point in ps

    TODO: implement polarity parameter for negativ and positive pulses

    Parameters:
    samples........The sample array to process
    baseline.......The baseline to subtract
    peakidx........The index value of the peak
    CFDX_frac.......The fraction (baseline/peak amplitude) at which the CFD trigger determines the pulses' time-stamp.
    CFDX_delay......The delay needed for zero crossing method. Should be rise-time of pulse + 1ns
    polarity.......The polarity of the pulse. 0: Negative, 1: Positive.
    
    Process:
    1. Signal is copied
    2. Signal copy is delayed by 'delay'
    3. Signal copy is inverted and attenuated by 'time_frac'
    4. Signal and signal copy is added together 
    5. Zero crossing points extracted and returned
    """
    # ========== SIGNAL PROCESSING ==========
    #Subtract baseline from signal
    signal = samples-baseline
    #Copy original signal and offset by the delay
    signal_copy = signal[CFDX_delay:-1]
    #Invert copy of signal and apply time_fraction
    signal_copy = signal_copy*(-CFDX_frac)
    #Calculate the sum of both signals
    sum = signal[0:len(signal_copy)]+signal_copy

    # ========== CFD COARSE ==========
    #Find first index after zero-crossing
    rise_index = np.nan #set default value for rise_index
    if (peakidx-20)>=0 and (peakidx+20)<=len(sum): #Filter out events with index out-of-range.
        for i in range(15): #Try and find the zero-crossing points within 15 samples.
            max_idx = np.argmax(sum[peakidx-20:peakidx+20])+peakidx-20 #find index of maximum value (for negative polarity)
            try:
                if sum[max_idx+i]<0: #find coarse zero crossing (when using negative polarity)
                    rise_index = max_idx+i
                    break
            except IndexError:
                #in cases where only noise was present sum array may go out of range. Set CFD value to nan!
                rise_index = np.nan
                break
            
    # ========== CFD FINE ==========
    if rise_index is np.nan: #check if rise_index was not found.
        return np.nan
    else:
        # Calculting the slope between the two samples points.
        slope_rise = (sum[rise_index] - sum[rise_index-1]) / (rise_index-(rise_index-1)) #calculate the slope of straight line between points
        # Getting the stright line constant
        if slope_rise!=0: #remove bad events with no slope
            const = sum[rise_index]-(slope_rise*(rise_index))
            # Calculating the zero crossing point between the two index.
            return -const/slope_rise
        else:
            return np.nan
    return df

def get_chinfo(col, chidx, polarity):
    """ helper function to determine channel properties """
    try:
        chSuffix = "_"+col.split('_')[1] # save channel number suffix as string.
        chNum = int(chSuffix.strip('_ch')) # string suffix string and save channel number as integer. Used for polarity dictionary.
    except IndexError:
        chSuffix = "" #when singlerow == False, chSuffix is empty.
        chNum = None

# determine polarity from argument
    if isinstance(polarity, dict):
        if chNum:
            try:
                chpol = polarity[chNum] # go by channel _number_
            except KeyError:
                raise InputError("polarity",f"Could not find information for channel {chNum}")
        else:
            raise InputError("polarity","Dictionary with settings for each channel not supported for one-row-per-channel data format")
    elif isinstance(polarity, list):
        if chNum:
            try:
                chpol = polarity[chidx] # go by channel _idx_
            except IndexError:
                raise InputError("polarity",f"Could not find information for channel {chNum}, index {chidx}")
        else:
            raise InputError("polarity","List with settings for each channel not supported for one-row-per-channel data format")
    else:
        # not a dictionary nor list
        chpol = polarity
    # check value
    if not chpol in ['+','-']:
        raise InputError("polarity",f"Value for polarity must always be either '-' or '+' (is: '{chpol}')!")
    return chNum, chSuffix, chpol


def sample_processing(df, baseline_samples=20, CFD_type='fractional_amplitude', CFD_param = {}, polarity='-'):
    """
    A function for processing waveform sample data stored in a dask DataFrame.

    =================================================
    Parameters:
    df.....................A dask DataFrame.
    baseline_samples.......The number of samples to use when averaging the baseline, ahead of the signal. Starting from the first samples point. Default: 20
    CFD_type...............String, selecting which CFD algorithm to use: "fractional_amplitude" or "zero_crossing"
    CFD_param..............Dictionary containing settings for the algorithm:
       CFD_frac............The fraction (baseline/peak amplitude) at which the CFD trigger determines the pulses' time-stamp. Default: 0.3.
       CFD_frac............Fraction of pulse amplitude to use with the zero crossing CFD method.
       CFD_delay...........The delay needed for zero crossing method. Should be rise-time of pulse + 1ns
    
    polarity...............String, either '-' or '+' for negative of positive polarity, respectively. Can also be a dictionary mapping channel number to polarity. Ex polarity = {1:'-', 2:'+', 3:'-', ...} or a list with one entry for each channel. 
                            NOTE: the latter only works when the dataframe is in one-row-per-event format (not one-row-per-channel).
    """
    # TODO: Add dictionary to polarity parameter, mapping it to channel numbers, like: polarity = {10: '-', 2: '+', 99:} to polarity
    # TODO support channel-by-channel polarity for data in one-row-per-channel format

    log = hlp.init_logging('sample_processing')  # set up logging

    #~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~#
    #            START OF CHANNEL LOOP
    #~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~#
    #Loop going through all active channels and processing their samples set.
    for chidx, col in enumerate([col for col in df.columns if col.startswith('sample')]):
        chNum, chSuffix, chpol = get_chinfo(col, chidx, polarity)

        log.info(f"Preparing channel #{chidx} ('{chSuffix}') with polarity '{chpol}'")
        log.info(f"- Baseline and amplitude calculations")

        #~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~#
        #            BASELINE CALCULATION
        #~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~#
        df[f'baseline{chSuffix}'] = df[f'samples{chSuffix}'].apply(lambda x: np.mean(x[:baseline_samples]), meta = (f'baseline{chSuffix}', np.float64))

        #~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~#
        #     GET MAX AMPLITUDE AND ITS POSITION
        #~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~#
        if chpol == '-':
            df[f'amplitude{chSuffix}'] = df[f'samples{chSuffix}'].apply(lambda x: np.min(x), meta = (f'amplitude{chSuffix}', np.float64))
        else:
            df[f'amplitude{chSuffix}'] = df[f'samples{chSuffix}'].apply(lambda x: np.max(x), meta = (f'amplitude{chSuffix}', np.float64))
        # Subtracting the baseline from the amplitude.
        df[f'amplitude{chSuffix}'] = np.abs(df[f'amplitude{chSuffix}'] - df[f'baseline{chSuffix}'])

        # Find the index value of max amplitude.
        if chpol == '-':
            df[f'peak_index{chSuffix}'] = df[f'samples{chSuffix}'].apply(np.argmin, meta = (f'peak_index{chSuffix}', np.float64))
        else:
            df[f'peak_index{chSuffix}'] = df[f'samples{chSuffix}'].apply(np.argmax, meta = (f'peak_index{chSuffix}', np.float64))
        # The max amplitude and its position will be used by the cfd determination in the next step.
        
        #~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~#
        #                     CFD
        #~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~#
        # Call CFD function and operate on one row (event) at a time.
        #
        # IMPORTANT NOTE
        # we need a helper routine that makes sure that the df.apply call only sees strictly local variables
        # or they will be overwritten by later iterations in the loops
        def cfd_apply(df, chSuffix, CFD_frac, polarity):
            df[f'CFD_rise{chSuffix}'] = df.apply(lambda x: cfd(
                                                samples = x[f'samples{chSuffix}'],
                                                baseline = x[f'baseline{chSuffix}'],
                                                peakidx = x[f'peak_index{chSuffix}'],
                                                CFD_frac = CFD_frac, 
                                                polarity = polarity),
                                                axis = 1,
                                                meta = (f'CFD_rise{chSuffix}', np.float64)
            )
        #Helper routine for zero crossing CFD method
        def cfdX_apply(df, chSuffix, CFDX_frac, CFDX_delay, polarity):
            df[f'CFD_rise{chSuffix}'] = df.apply(lambda x: cfdX(
                                                samples = x[f'samples{chSuffix}'],
                                                baseline = x[f'baseline{chSuffix}'],
                                                peakidx = x[f'peak_index{chSuffix}'],
                                                CFDX_frac = CFDX_frac, 
                                                CFDX_delay = CFDX_delay, 
                                                polarity = polarity),
                                                axis = 1,
                                                meta = (f'CFD_rise{chSuffix}', np.float64))

        # execute the apply statement, passing all necessary variables to selected CFD algorithm.
        if CFD_type=='fractional_amplitude': #calculate CFD based on fractional amplitude
            log.info(f"- CFD determination: ({CFD_type})")

            cfd_apply(
                df = df,
                chSuffix = chSuffix,
                CFD_frac = CFD_param['CFD_frac'],
                polarity = polarity)

        elif CFD_type == 'zero_crossing':
            log.info(f"- CFD determination: ({CFD_type})")

            cfdX_apply( #calculate CFD based on zero crossing
                df = df, 
                chSuffix = chSuffix, 
                CFDX_frac = CFD_param['CFD_frac'],
                CFDX_delay = CFD_param['CFD_delay'],
                polarity = polarity)
        else:
            log.error('No valid CFD algorithm specified!')
    log.info('Sample processing prepared!')
    return df

def sample_stats(df, CFD_drop_frac = 0.1, polarity='-'):
    """
    Calculates additional statistics from waveform sample data stored in a dask DataFrame.

    =================================================

    Parameters:
    df.....................A dask DataFrame.
    baseline_samples.......The number of samples to use when averaging the baseline, ahead of the signal. Starting from the first samples point. Default: 20
    CFD_drop_frac...............The fraction of the peak amplitude at which the Signal is considered to end.
    polarity...............String, either '-' or '+' for negative of positive polarity, respectively. Can also be a dictionary mapping channel number to polarity. Ex polarity = {1:'-', 2:'+', 3:'-', ...} or a list with one entry for each channel. NOTE: the latter only works when the dataframe is in one-row-per-event format (not one-row-per-channel).

    """

    # TODO: Add dictionary to polarity parameter, mapping it to channel numbers, like: polarity = {10: '-', 2: '+', 99:} to polarity
    # TODO support channel-by-channel polarity for data in one-row-per-channel format

    log = hlp.init_logging('sample_stats')  # set up logging

    #~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~#
    #            START OF CHANNEL LOOP
    #~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~#
    #Loop going through all active channels and processing their samples set.

    for chidx, col in enumerate([col for col in df.columns if col.startswith('sample')]):
        chNum, chSuffix, chpol = get_chinfo(col, chidx, polarity)

        log.info(f"Preparing stats determination for channel {chSuffix} with polarity '{chpol}'")

        #~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~#
        #            MIN/MAX VALUE CALCULATION
        #~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~#
        df[f'min_val{chSuffix}'] = df[f'samples{chSuffix}'].apply(lambda x: np.min(x), meta = (f'min_val{chSuffix}', np.float64))
        df[f'max_val{chSuffix}'] = df[f'samples{chSuffix}'].apply(lambda x: np.max(x), meta = (f'max_val{chSuffix}', np.float64))


        #~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~#
        #                     SIGNAL END
        #~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~#
        # Call CFD function and operate on one row (event) at a time.
        #
        # IMPORTANT NOTE
        # we need a helper routine that makes sure that the df.apply call only sees strictly local variables
        # or they will be overwritten by later iterations in the loops
        def sigendapply(df, chSuffix, CFD_frac, polarity, nsamples):
            df[f'CFD_drop{chSuffix}'] = df.apply(lambda x: cfd(
                                                samples = x[f'samples{chSuffix}'],
                                                baseline = x[f'baseline{chSuffix}'],
                                                peakidx = x[f'peak_index{chSuffix}'],
                                                CFD_frac = CFD_frac, 
                                                polarity = polarity,
                                                backwards=False # run algorithm in forward direction
                                                ),
                                                 axis=1,
                                                 meta = (f'CFD_drop{chSuffix}', np.float64)
            )
        # execute the apply statement, passing all necessary variables
        sigendapply(
            df = df,
            chSuffix = chSuffix,
            CFD_frac = CFD_drop_frac,
            polarity = chpol,
            nsamples = 20)

        # calculate rise time
        df[f'risetime{chSuffix}'] = df[f'peak_index{chSuffix}'] - df[f'CFD_rise{chSuffix}']
        # calculate signal length
        df[f'siglen{chSuffix}']   = df[f'CFD_drop{chSuffix}']   - df[f'CFD_rise{chSuffix}']
    log.info('Sample statistics prepared!')
    return df
