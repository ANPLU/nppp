from tabulate import tabulate
import pandas as pd

def read_orgtbl(s):
    """Parse an org-table (input as a filename) into a Pandas data frame."""
    # cannot open file directly: boundary separators ('|') mess up column count
    # read first line to determine # of rows
    df = pd.read_csv(s, keep_default_na=False, sep='|',skiprows=[1],skipinitialspace=True, nrows=1)
    ncols = len(df.columns)
    # now load df and filter first and last column using 'usecols'
    df = pd.read_csv(s, keep_default_na=False, sep='|',skiprows=[1],skipinitialspace=True,
                       usecols = [i for i in range(ncols -1) if i != 0 ] )
    # filter out spaces in col names
    df.rename(columns=lambda x: x.strip(), inplace=True)
    # filter out spaces in fields (all columns of dtype 'object')
    df_obj = df.select_dtypes(['object'])
    df[df_obj.columns] = df_obj.apply(lambda x: x.str.strip())
    # replace string indicators for NaN and None
    df = df.replace(['nan', 'None'], '')
    # convert numbers
    df = df.apply(pd.to_numeric, errors='ignore')
    return df

def write_orgtbl(df, fn):
    """Write a DataFrame as an org-table to file"""
    with open(fn, "w") as orgfile:
        print(tabulate(df, headers='keys', tablefmt='orgtbl', showindex=False), file=orgfile)

# Convert an Org table, stored in a var in a python src block to a Pandas Dataframe
def org2df(orgTblVar):
    if isinstance(orgTblVar, pd.DataFrame):
        return orgTblVar # no conversion if already DataFrame
    # filter rows marked not to be exported (requires: empty title in first col, '/' in first col for that row)
    if orgTblVar[0][0] == '':
        orgTblVar = [r for r in orgTblVar if r[0] != '/']
    return pd.DataFrame(orgTblVar[1:],columns=orgTblVar[0])

# Convert a Pandas DataFrame to an Org table
def df2org(df):
    return tabulate(df, tablefmt="orgtbl", headers="keys")
    #return tabulate(df, list(df), showindex=False, floatfmt=".8f", tablefmt="orgtbl")
