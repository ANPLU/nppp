#!/usr/bin/env python3
import sys
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit

# IMPORT FROM LIBRARY DIRECTORY
sys.path.insert(0, "../library/")
sys.path.insert(0, "../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "sim_analysis/")
sys.path.insert(0, "data_analysis/")

import digidata
import nicholai_plot_helper as nplt
import processing_utility as prout
import processing_math as promath
import processing_data as prodata
import processing_pd as propd
import processing_simulation as prosim


"""
Script for running both data and simulation and calibrating data of NE213A detector.
"""
#TODO fit gaussian to pencilbeam data on the right side of the distyribution.


PMT_gain = 4e6 #Nominal gain is 7'000'000 at nominal A/lm (model: 9821B p1)
att_factor_12dB = 3.98
q = 1.60217662E-19 #charge of a single electron
plot = False

##############################
# LOADING DATA
##############################
path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop
bg_runs =       list(range(3260,3270))
Cs137_runs =    [3259]#list(range(1656,1666))
Th232_runs =    [3258]#list(range(1671,1676))
AmBe_runs =     [3257]#list(range(1693,1703))
PuBe_runs =     [2121]

CableLengthCorrections = 1.192 #correction constant between inside cave and outside due to different cable lengthsc

df_bg =         propd.load_parquet_merge(path, bg_runs,     keep_col=['qdc_lg_ch1'], full=False)*CableLengthCorrections
df_Cs137 =      propd.load_parquet_merge(path, Cs137_runs,  keep_col=['qdc_lg_ch1'], full=False)*CableLengthCorrections
df_Th232 =      propd.load_parquet_merge(path, Th232_runs,  keep_col=['qdc_lg_ch1'], full=False)*CableLengthCorrections
df_AmBe =       propd.load_parquet_merge(path, AmBe_runs,   keep_col=['qdc_lg_ch1'], full=False)*CableLengthCorrections
df_PuBe =       propd.load_parquet_merge(path, PuBe_runs,   keep_col=['qdc_lg_ch1'], full=False)


#Check cablelenghtcorrection value
y,x = np.histogram(df_AmBe, bins=np.arange(0,25000,100))
plt.step(prodata.getBinCenters(x), y*1.1, label='AmBe')

# y,x = np.histogram(df_bg, bins=np.arange(0,25000,100))
# plt.step(prodata.getBinCenters(x), y, label='BG')

# y,x = np.histogram(df_Cs137, bins=np.arange(0,25000,100))
# plt.step(prodata.getBinCenters(x), y, label='Cs137')

# y,x = np.histogram(df_Th232, bins=np.arange(0,25000,100))
# plt.step(prodata.getBinCenters(x), y, label='Th232')

y,x = np.histogram(df_PuBe, bins=np.arange(0,25000,100))
plt.step(prodata.getBinCenters(x), y*1, label='PuBe')

# plt.yscale('log')

plt.legend()
plt.show()


###############################
# LOADING SIMULATION DATA
###############################
detector = 'NE213A'

path_sim = '/media/gheed/Seagate_Expansion_Drive1/data/simulation/'
names = ['evtNum', 
        'optPhotonSum', 
        'optPhotonSumQE', 
        'optPhotonSumCompton', 
        'optPhotonSumComptonQE', 
        'optPhotonSumNPQE', 
        'xLoc', 
        'yLoc', 
        'zLoc', 
        'CsMin', 
        'optPhotonParentID', 
        'optPhotonParentCreatorProcess', 
        'optPhotonParentStartingEnergy',
        'total_edep',
        'gamma_edep',
        'proton_edep',
        'nScatters',
        'nScattersProton']

Cs137_sim =         pd.read_csv(path_sim + f"{detector}/Cs137/isotropic/CSV_optphoton_data_sum.csv", names=names)
Th232_sim =         pd.read_csv(path_sim + f"{detector}/Th232/isotropic/CSV_optphoton_data_sum.csv", names=names)
AmBe_sim =          pd.read_csv(path_sim + f"{detector}/4.44/isotropic/CSV_optphoton_data_sum.csv", names=names)

#append extra data
Cs137_sim =         pd.concat([Cs137_sim, pd.read_csv(path_sim + f'{detector}/Cs137/isotropic/part2/CSV_optphoton_data_sum.csv', names=names)])
Cs137_sim =         pd.concat([Cs137_sim, pd.read_csv(path_sim + f'{detector}/Cs137/isotropic/part3/CSV_optphoton_data_sum.csv', names=names)])
Cs137_sim =         pd.concat([Cs137_sim, pd.read_csv(path_sim + f'{detector}/Cs137/isotropic/part4/CSV_optphoton_data_sum.csv', names=names)])
Cs137_sim =         pd.concat([Cs137_sim, pd.read_csv(path_sim + f'{detector}/Cs137/isotropic/part5/CSV_optphoton_data_sum.csv', names=names)])

Th232_sim =         pd.concat([Th232_sim, pd.read_csv(path_sim + f'{detector}/Th232/isotropic/part2/CSV_optphoton_data_sum.csv', names=names)])

AmBe_sim =          pd.concat([AmBe_sim, pd.read_csv(path_sim + f'{detector}/4.44/isotropic/part2/CSV_optphoton_data_sum.csv', names=names)])

###############################
# RANDOMIZE BINNING
###############################
y, x = np.histogram(Cs137_sim.optPhotonSumQE, bins=4096, range=[0, 4096])
Cs137_sim.optPhotonSumQE = prodata.getRandDist(x, y)

y, x = np.histogram(Th232_sim.optPhotonSumQE, bins=4096, range=[0, 4096])
Th232_sim.optPhotonSumQE = prodata.getRandDist(x, y)

y, x = np.histogram(AmBe_sim.optPhotonSumQE, bins=4096, range=[0, 4096])
AmBe_sim.optPhotonSumQE = prodata.getRandDist(x, y)

###############################
# RESET INDEX
###############################
Cs137_sim =  Cs137_sim.reset_index()
Th232_sim =  Th232_sim.reset_index()
AmBe_sim =   AmBe_sim.reset_index()

###############################
# CALIBRATING SIMULATION DATA
###############################
Cs137_sim.optPhotonSumQE =         prosim.chargeCalibration(Cs137_sim.optPhotonSumQE  * q * PMT_gain)
Th232_sim.optPhotonSumQE =         prosim.chargeCalibration(Th232_sim.optPhotonSumQE  * q * PMT_gain)
AmBe_sim.optPhotonSumQE =          prosim.chargeCalibration(AmBe_sim.optPhotonSumQE   * q * PMT_gain)

#### TEST PLOT ####
# numBins = np.arange(4500, 16000, 150)
# y1,x = np.histogram(df_PuBe.qdc_lg_ch1, bins=numBins)
# y2,x = np.histogram(df_AmBe.qdc_lg_ch1*1.192, bins=numBins)
# plt.step(prodata.getBinCenters(x), y1, label='PuBe')
# plt.step(prodata.getBinCenters(x), y2*1.15, label='AmBe')
# plt.legend()
# plt.show()
###################


############################################
# LOAD PENCILBEAM SIMULATIONS
############################################
Cs137_sim_pen = pd.read_csv(path_sim + f"{detector}/Cs137/pencilbeam/CSV_optphoton_data_sum.csv", names=names)

Th232_sim_pen = pd.read_csv(path_sim + f"{detector}/Th232/pencilbeam/CSV_optphoton_data_sum.csv", names=names)
Th232_sim_pen = pd.concat([Th232_sim_pen, pd.read_csv(path_sim + f'{detector}/Th232/pencilbeam/part2/CSV_optphoton_data_sum.csv', names=names)])
Th232_sim_pen = pd.concat([Th232_sim_pen, pd.read_csv(path_sim + f'{detector}/Th232/pencilbeam/part3/CSV_optphoton_data_sum.csv', names=names)])
Th232_sim_pen = pd.concat([Th232_sim_pen, pd.read_csv(path_sim + f'{detector}/Th232/pencilbeam/part4/CSV_optphoton_data_sum.csv', names=names)])
Th232_sim_pen = pd.concat([Th232_sim_pen, pd.read_csv(path_sim + f'{detector}/Th232/pencilbeam/part5/CSV_optphoton_data_sum.csv', names=names)])

AmBe_sim_pen = pd.read_csv(path_sim + f"{detector}/4.44/pencilbeam/CSV_optphoton_data_sum.csv", names=names)
AmBe_sim_pen = pd.concat([AmBe_sim_pen, pd.read_csv(path_sim + f'{detector}/4.44/pencilbeam/part2/CSV_optphoton_data_sum.csv', names=names)])
AmBe_sim_pen = pd.concat([AmBe_sim_pen, pd.read_csv(path_sim + f'{detector}/4.44/pencilbeam/part3/CSV_optphoton_data_sum.csv', names=names)])
AmBe_sim_pen = pd.concat([AmBe_sim_pen, pd.read_csv(path_sim + f'{detector}/4.44/pencilbeam/part4/CSV_optphoton_data_sum.csv', names=names)])

#randomize binning pencilbeam
y, x = np.histogram(Cs137_sim_pen.optPhotonSumComptonQE, bins=4096, range=[0, 4096])
Cs137_sim_pen.optPhotonSumComptonQE = prodata.getRandDist(x, y)
y, x = np.histogram(Th232_sim_pen.optPhotonSumComptonQE, bins=4096, range=[0, 4096])
Th232_sim_pen.optPhotonSumComptonQE = prodata.getRandDist(x, y)
y, x = np.histogram(AmBe_sim_pen.optPhotonSumComptonQE, bins=4096, range=[0, 4096])
AmBe_sim_pen.optPhotonSumComptonQE = prodata.getRandDist(x, y)

#reset index pencilbeam 
Cs137_sim_pen =  Cs137_sim_pen.reset_index()
Th232_sim_pen =  Th232_sim_pen.reset_index()
AmBe_sim_pen =   AmBe_sim_pen.reset_index()

#QDC calibrate simulations pencilbeam
Cs137_sim_pen.optPhotonSumComptonQE =  prosim.chargeCalibration(Cs137_sim_pen.optPhotonSumComptonQE * q * PMT_gain) 
Th232_sim_pen.optPhotonSumComptonQE =  prosim.chargeCalibration(Th232_sim_pen.optPhotonSumComptonQE * q * PMT_gain) 
AmBe_sim_pen.optPhotonSumComptonQE =   prosim.chargeCalibration(AmBe_sim_pen.optPhotonSumComptonQE * q * PMT_gain)

#####REOMVE ME######
# numBins = np.arange(0,4000,10)
# plt.hist(Cs137_sim_pen.optPhotonSumComptonQE, bins=numBins,label='Cs137', alpha=0.5)
# plt.hist(Th232_sim_pen.optPhotonSumComptonQE, bins=numBins,label='Th232', alpha=0.5)
# plt.hist(AmBe_sim_pen.optPhotonSumComptonQE, bins=numBins,label='AmBe', alpha=0.5)

# plt.hist(Cs137_sim.optPhotonSumQE, bins=numBins,label='Cs137',alpha=0.5)
# plt.hist(Th232_sim.optPhotonSumQE, bins=numBins,label='Th232',alpha=0.5)
# plt.hist(AmBe_sim.optPhotonSumQE, bins=numBins,label='AmBe',alpha=0.5)

# plt.legend()
# plt.show()
#####REOMVE ME######

###############################
# PMT ALIGN Cs-137
###############################
res = 0.16
numBins = np.arange(0, 24000, 50)
Cs137_sim_smear = np.zeros(len(Cs137_sim.optPhotonSumQE))
for i in range(len(Cs137_sim.optPhotonSumQE)):
    Cs137_sim_smear[i] = prosim.gaussSmear(Cs137_sim.optPhotonSumQE[i], res)

gain = 0.38

countsSimPen, binsSimPen = np.histogram(Cs137_sim_pen.optPhotonSumComptonQE*gain, bins=numBins)

countsSim, binsSim = np.histogram(Cs137_sim_smear*gain, bins=numBins)
binsSim = prodata.getBinCenters(binsSim)

w = np.empty(len(df_bg))
w.fill(1/len(bg_runs))
countsBG, binsBG = np.histogram(df_bg.qdc_lg_ch1*att_factor_12dB, bins=numBins, weights=w)

w = np.empty(len(df_Cs137))
w.fill(1/len(Cs137_runs))
counts, bins = np.histogram(df_Cs137.qdc_lg_ch1*att_factor_12dB, bins=numBins, weights=w)
bins = prodata.getBinCenters(bins)

plt.plot(bins, counts-countsBG, label='data-BG')
plt.plot(binsSim, countsSimPen*1.15, label='sim pencil')
plt.plot(binsSim, countsSim*1.15, label='sim')

plt.legend()
plt.show()

Cs137_results = prosim.PMTGainAlign( model = Cs137_sim_smear, 
                                    data = df_Cs137.qdc_lg_ch1*att_factor_12dB, 
                                    bg = df_bg.qdc_lg_ch1*att_factor_12dB,
                                    scale = 1.15,
                                    gain = gain,
                                    data_weight = len(Cs137_runs),
                                    bg_weight = len(bg_runs),
                                    scaleBound = [1 , 1.5],
                                    gainBound = [.2, 0.5],
                                    stepSize = 1e-2,
                                    binRange = list(range(3300, 8000, 100)),
                                    plot = False)
Cs137_results['smear'] = res



###############################
# PMT ALIGN Th232 2615 keV
###############################
res = 0.07
numBins = np.arange(0, 45000, 250)
Th232_sim_smear = np.zeros(len(Th232_sim.optPhotonSumQE))
for i in range(len(Th232_sim.optPhotonSumQE)):
    Th232_sim_smear[i] = prosim.gaussSmear(Th232_sim.optPhotonSumQE[i], res)

gain = 0.38

# countsSimPen, binsSimPen = np.histogram(Th232_sim_pen.optPhotonSumComptonQE*gain, bins=numBins)

# countsSim, binsSim = np.histogram(Th232_sim_smear*gain, bins=numBins)
# binsSim = prodata.getBinCenters(binsSim)

# w = np.empty(len(df_bg))
# w.fill(1/len(bg_runs))
# countsBG, binsBG = np.histogram(df_bg.qdc_lg_ch1*att_factor_12dB, bins=numBins, weights=w)

# w = np.empty(len(df_Th232))
# w.fill(1/len(Th232_runs))
# counts, bins = np.histogram(df_Th232.qdc_lg_ch1*att_factor_12dB, bins=numBins, weights=w)
# bins = prodata.getBinCenters(bins)

# plt.plot(bins, counts-countsBG, label='data-BG')
# plt.plot(binsSim, countsSimPen*1.15, label='sim pencil')
# plt.plot(binsSim, countsSim*0.38, label='sim')

# plt.legend()
# plt.show()

Th232_results = prosim.PMTGainAlign(model = Th232_sim_smear, 
                                    data = df_Th232.qdc_lg_ch1*att_factor_12dB, 
                                    bg = df_bg.qdc_lg_ch1*att_factor_12dB,
                                    scale = 0.38,
                                    gain = gain,
                                    data_weight = len(Th232_runs),
                                    bg_weight = len(bg_runs),
                                    scaleBound = [0.1, 0.5],
                                    gainBound = [0.25, 0.5],
                                    stepSize = 1e-2,
                                    binRange = list(range(22000, 30000, 250)),
                                    plot = False)
Th232_results['smear'] = res

###############################
# PMT ALIGN AmBe 4439 keV
###############################
res = 0.11
numBins = np.arange(100, 100000, 450)
AmBe_sim_smear = np.zeros(len(AmBe_sim.optPhotonSumQE))
for i in range(len(AmBe_sim.optPhotonSumQE)):
    AmBe_sim_smear[i] = prosim.gaussSmear(AmBe_sim.optPhotonSumQE[i], res)

gain = 0.38

# countsSimPen, binsSimPen = np.histogram(AmBe_sim_pen.optPhotonSumComptonQE*gain, bins=numBins)

# countsSim, binsSim = np.histogram(AmBe_sim_smear*gain, bins=numBins)
# binsSim = prodata.getBinCenters(binsSim)

# w = np.empty(len(df_bg))
# w.fill(1/len(bg_runs))
# countsBG, binsBG = np.histogram(df_bg.qdc_lg_ch1*att_factor_12dB, bins=numBins, weights=w)

# w = np.empty(len(df_AmBe))
# w.fill(1/len(AmBe_runs))
# counts, bins = np.histogram(df_AmBe.qdc_lg_ch1*att_factor_12dB, bins=numBins, weights=w)
# bins = prodata.getBinCenters(bins)

# plt.plot(bins, counts-countsBG, label='data-BG')
# plt.plot(binsSim, countsSimPen*1.15, label='sim pencil')
# plt.plot(binsSim, countsSim*1.65, label='sim')

# plt.legend()
# plt.show()

AmBe_results = prosim.PMTGainAlign( model = AmBe_sim_smear, 
                                    data = df_AmBe.qdc_lg_ch1*att_factor_12dB, 
                                    bg = df_bg.qdc_lg_ch1*att_factor_12dB,
                                    scale = 1.65,
                                    gain = gain,
                                    data_weight = len(AmBe_runs),
                                    bg_weight = len(bg_runs),
                                    scaleBound = [1 , 2],
                                    gainBound = [0.2, 0.5],
                                    stepSize = 1e-2,
                                    binRange = list(range(36000, 53000, 450)),
                                    plot = False)
AmBe_results['smear'] = res


#############################
## FITTING PENCILBEAM DATA ##
#############################
#apply gain corrections on pencilbeam simulation
Cs137_sim_pen.optPhotonSumComptonQE = Cs137_sim_pen.optPhotonSumComptonQE * Cs137_results['gain']
Th232_sim_pen.optPhotonSumComptonQE = Th232_sim_pen.optPhotonSumComptonQE * Th232_results['gain'] 
AmBe_sim_pen.optPhotonSumComptonQE =  AmBe_sim_pen.optPhotonSumComptonQE * AmBe_results['gain']

#Apply smearing on pencilbeam simulations
Cs137_sim_pen.optPhotonSumComptonQE = Cs137_sim_pen.optPhotonSumComptonQE.apply(lambda x: prosim.gaussSmear(x, Cs137_results['smear'])) 
Th232_sim_pen.optPhotonSumComptonQE = Th232_sim_pen.optPhotonSumComptonQE.apply(lambda x: prosim.gaussSmear(x, Th232_results['smear'])) 
AmBe_sim_pen.optPhotonSumComptonQE = AmBe_sim_pen.optPhotonSumComptonQE.apply(lambda x: prosim.gaussSmear(x, AmBe_results['smear'])) 

start = 3000
stop = 5500
counts, bin_edges = np.histogram(Cs137_sim_pen.optPhotonSumComptonQE, bins = np.arange(start*0.8, stop*1.2, 200))
bin_centers = prodata.getBinCenters(bin_edges)
Ef = 0.66166
Cs137_sim_fit  =  prosim.calibrationFitBinned(bin_centers, counts, start, stop, Ef=Ef, plot=False, compton=True)
Cs137_sim_fit['CE'] = promath.comptonMax(Ef)
mean = Cs137_sim_fit['gauss_mean']
std = Cs137_sim_fit['gauss_std']
Cs137_sim_results = {}
Cs137_sim_results['mean'] = np.mean(Cs137_sim_pen.query(f'optPhotonSumComptonQE>{mean-3*std} and optPhotonSumComptonQE<{mean+3*std}').optPhotonSumComptonQE)
Cs137_sim_results['mean_err']  = np.std(Cs137_sim_pen.query(f'optPhotonSumComptonQE>{mean-3*std} and optPhotonSumComptonQE<{mean+3*std}').optPhotonSumComptonQE)/np.sqrt(len(Cs137_sim_pen.query(f'optPhotonSumComptonQE>{mean-3*std} and optPhotonSumComptonQE<{mean+3*std}').optPhotonSumComptonQE))

start = 21000
stop = 35000
counts, bin_edges = np.histogram(Th232_sim_pen.optPhotonSumComptonQE, bins = np.arange(start*0.8, stop*1.2, 750))
bin_centers = prodata.getBinCenters(bin_edges)
Ef = 2.614533
Th232_sim_fit = prosim.calibrationFitBinned(bin_centers, counts, start, stop, Ef=Ef, plot=False, compton=True)
Th232_sim_fit['CE'] = promath.comptonMax(Ef)
mean = Th232_sim_fit['gauss_mean']
std = Th232_sim_fit['gauss_std']
Th232_sim_results = {}
Th232_sim_results['mean'] = np.mean(Th232_sim_pen.query(f'optPhotonSumComptonQE>{mean-3*std} and optPhotonSumComptonQE<{mean+3*std}').optPhotonSumComptonQE)
Th232_sim_results['mean_err']  = np.std(Th232_sim_pen.query(f'optPhotonSumComptonQE>{mean-3*std} and optPhotonSumComptonQE<{mean+3*std}').optPhotonSumComptonQE)/np.sqrt(len(Th232_sim_pen.query(f'optPhotonSumComptonQE>{mean-3*std} and optPhotonSumComptonQE<{mean+3*std}').optPhotonSumComptonQE))

start = 35000
stop = 55000
counts, bin_edges = np.histogram(AmBe_sim_pen.optPhotonSumComptonQE, bins = np.arange(start*0.8, stop*1.2, 2000))
bin_centers = prodata.getBinCenters(bin_edges)
Ef = 4.439
AmBe_sim_fit = prosim.calibrationFitBinned(bin_centers, counts, start, stop, Ef=Ef, plot=False, compton=True)
AmBe_sim_fit['CE'] = promath.comptonMax(Ef)
mean = AmBe_sim_fit['gauss_mean']
std = AmBe_sim_fit['gauss_std']
AmBe_sim_results = {}
AmBe_sim_results['mean'] = np.mean(AmBe_sim_pen.query(f'optPhotonSumComptonQE>{mean-3*std} and optPhotonSumComptonQE<{mean+3*std}').optPhotonSumComptonQE)
AmBe_sim_results['mean_err']  = np.std(AmBe_sim_pen.query(f'optPhotonSumComptonQE>{mean-3*std} and optPhotonSumComptonQE<{mean+3*std}').optPhotonSumComptonQE)/np.sqrt(len(AmBe_sim_pen.query(f'optPhotonSumComptonQE>{mean-3*std} and optPhotonSumComptonQE<{mean+3*std}').optPhotonSumComptonQE))






##################################################
#Calculate calibration constant
CE_energy = np.array([0, Cs137_sim_fit['CE'], Th232_sim_fit['CE'], AmBe_sim_fit['CE']]) #energies in MeVee of Compton edges
QDC_loc = np.array([0, Cs137_sim_results['mean'], Th232_sim_results['mean'], AmBe_sim_results['mean']]) #relevant QDC channels
QDC_loc_err = np.array([0, Cs137_sim_results['mean_err'], Th232_sim_results['mean_err'], AmBe_sim_results['mean_err']]) #relevant QDC channels errors from gain-align above.

# CE_energy = np.array([Cs137_sim_fit['CE'], Th232_sim_fit['CE'], AmBe_sim_fit['CE']]) #energies in MeVee of Compton edges
# QDC_loc = np.array([Cs137_sim_results['mean'], Th232_sim_results['mean'], AmBe_sim_results['mean']]) #relevant QDC channels
# QDC_loc_err = np.array([Cs137_sim_results['mean_err'], Th232_sim_results['mean_err'], AmBe_sim_results['mean_err']]) #relevant QDC channels errors from gain-align above.

popt, pcov = curve_fit(promath.linearFunc, CE_energy, QDC_loc)
pcov = np.diag(np.sqrt(pcov))
E_err = promath.errorPropLinearFuncInv(promath.linearFuncInv(QDC_loc, popt[0], popt[1]), popt[0], pcov[0], popt[1], pcov[1])

# popt, pcov = curve_fit(promath.quadraticFunc, CE_energy, QDC_loc)

plt.scatter(QDC_loc, CE_energy)
plt.errorbar(QDC_loc, CE_energy, yerr = E_err, ls='none')
plt.plot(np.arange(0, 100000, 1000), promath.linearFuncInv(np.arange(0,100000,1000), popt[0], popt[1]))
# plt.plot(np.arange(0, 100000, 1000), promath.quadraticFunc(np.arange(0,100000,1000), popt[0], popt[1], popt[2]))

plt.ylabel('MeV$_{ee}$')
plt.xlabel('QDC [channels]')
plt.show()

np.save(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal/popt_LG', popt)
np.save(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal/pcov_LG', pcov)




