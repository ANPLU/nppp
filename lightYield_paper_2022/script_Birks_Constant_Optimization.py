
#!/usr/bin/env python3
import sys
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit

# IMPORT FROM LIBRARY DIRECTORY
sys.path.insert(0, "../library/")
sys.path.insert(0, "../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "sim_analysis/")
sys.path.insert(0, "data_analysis/")

import digidata
import nicholai_plot_helper as nplt
import processing_utility as prout
import processing_math as promath
import processing_data as prodata
import processing_pd as propd
import processing_simulation as prosim

"""
Script will derive optimla kB between 2-6 MeV in 500 keV bins and save the resulting optimal kB values for each energy to disk.
- also fit optimal kB for each energy and save fitted values to disk (for each detector)
- also fit optimal smearing values for at each energy and save fitted values to disk (for each detector)
"""

pathData = '/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper'
pathSim = '/media/gheed/Seagate_Expansion_Drive1/data/simulation'


# ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#   _   _ _____ ____  _ _____   _    
#  | \ | | ____|___ \/ |___ /  / \   
#  |  \| |  _|   __) | | |_ \ / _ \  
#  | |\  | |___ / __/| |___) / ___ \ 
#  |_| \_|_____|_____|_|____/_/   \_\
#NE213A initial parameters
detector =          'NE213A'
density =           0.874 #g/cm3
numBins =           np.arange(0.15, 4.2, 0.05)
lightYieldFactor =  1
birksFolder =       ['145', '126', '100',   '0917', '085', '060']
birksVal =          [0.145, 0.126,  0.100,  0.0917, 0.085,  0.060]

#2000 keV
energy =        '2000'
optRangeBins =  np.arange(0.32, 1.0, 0.05)
ampCoeff =      [3.20, 3.20,  3.15,   3.20,   3.30,   3.40]
gainCoeff =     [1.07, 0.97,  0.82,   0.79,   0.79,   0.63]
smearCoeff =    [0.22, 0.23,  0.24,   0.26,   0.27,   0.29]
NE213A_2000 = prosim.birksOptimization( pathData, 
                                        pathSim, 
                                        energy, 
                                        detector, 
                                        numBins, 
                                        birksFolder, 
                                        birksVal, 
                                        ampCoeff, 
                                        gainCoeff, 
                                        smearCoeff, 
                                        optRangeBins, 
                                        lightYieldFactor, 
                                        plotTest=False, 
                                        plotFinal=False,
                                        optimizeOff=False)
#2500 keV
energy =        '2500'
optRangeBins =  np.arange(0.45, 1.2, 0.05)
ampCoeff =      [4.00, 4.00,  4.00,   4.17,   4.10,   4.10]
gainCoeff =     [1.15, 1.05,  0.90,   0.88,   0.84,   0.69]
smearCoeff =    [0.22, 0.22,  0.22,   0.23,   0.25,   0.26]
NE213A_2500 = prosim.birksOptimization( pathData, 
                                        pathSim, 
                                        energy, 
                                        detector, 
                                        numBins, 
                                        birksFolder, 
                                        birksVal, 
                                        ampCoeff, 
                                        gainCoeff, 
                                        smearCoeff, 
                                        optRangeBins, 
                                        lightYieldFactor, 
                                        plotTest=False, 
                                        plotFinal=False,
                                        optimizeOff=False)
#3000 keV
energy =        '3000'
optRangeBins =  np.arange(0.61, 1.5, 0.05)
ampCoeff =      [4.40, 4.40,  4.40,   4.40,   4.40,   4.40]
gainCoeff =     [1.20, 1.10,  0.95,   0.94,   0.90,   0.73]
smearCoeff =    [0.15, 0.15,  0.15,   0.16,   0.17,   0.19]
NE213A_3000 = prosim.birksOptimization( pathData, 
                                        pathSim, 
                                        energy, 
                                        detector, 
                                        numBins, 
                                        birksFolder, 
                                        birksVal, 
                                        ampCoeff, 
                                        gainCoeff, 
                                        smearCoeff, 
                                        optRangeBins, 
                                        lightYieldFactor, 
                                        plotTest=False, 
                                        plotFinal=False,
                                        optimizeOff=False)
#3500 keV
energy =        '3500'
optRangeBins =  np.arange(0.83, 1.8, 0.05)
ampCoeff =      [3.50,  3.60,   3.50,   3.60,   3.50,   3.60]
gainCoeff =     [1.30,  1.18,   1.00,   0.94,   0.90,   0.77]
smearCoeff =    [0.10,  0.10,   0.11,   0.12,   0.13,   0.18]
NE213A_3500 = prosim.birksOptimization( pathData, 
                                        pathSim, 
                                        energy, 
                                        detector, 
                                        numBins, 
                                        birksFolder, 
                                        birksVal, 
                                        ampCoeff, 
                                        gainCoeff, 
                                        smearCoeff, 
                                        optRangeBins, 
                                        lightYieldFactor, 
                                        plotTest=False, 
                                        plotFinal=False,
                                        optimizeOff=False)
energy =        '4000'
optRangeBins =  np.arange(1.00, 2.1, 0.05)
ampCoeff =      [3.50,  3.50,   3.30,   3.20,   3.20,   3.10]
gainCoeff =     [1.30,  1.20,   1.05,   0.99,   0.93,   0.78]
smearCoeff =    [0.12,  0.11,   0.11,   0.11,   0.12,   0.12]

NE213A_4000 = prosim.birksOptimization( pathData, 
                                        pathSim, 
                                        energy, 
                                        detector, 
                                        numBins, 
                                        birksFolder, 
                                        birksVal, 
                                        ampCoeff, 
                                        gainCoeff, 
                                        smearCoeff, 
                                        optRangeBins, 
                                        lightYieldFactor, 
                                        plotTest=False, 
                                        plotFinal=False,
                                        optimizeOff=False)

energy = '4500'
optRangeBins = np.arange(1.40, 2.5, 0.05)
ampCoeff =      [3.6,   3.4,  3.4,  3.5,  3.6,  3.5]
gainCoeff =     [1.29,  1.20, 1.05, 1.00, 0.95, 0.78]
smearCoeff =    [0.06,  0.08, 0.08, 0.08, 0.09, 0.12]
NE213A_4500 = prosim.birksOptimization( pathData, 
                                        pathSim, 
                                        energy, 
                                        detector, 
                                        numBins, 
                                        birksFolder, 
                                        birksVal, 
                                        ampCoeff, 
                                        gainCoeff, 
                                        smearCoeff, 
                                        optRangeBins, 
                                        lightYieldFactor, 
                                        plotTest=False, 
                                        plotFinal=False,
                                        optimizeOff=False)
#5000 keV
energy =        '5000'
optRangeBins =  np.arange(1.50, 3.0, 0.05)
ampCoeff =      [3.4,   0.65,   3.2,    3.1,    1.3,    3.2]
gainCoeff =     [1.30,  1.20,   1.05,   1.03,   0.97,   0.82]
# smearCoeff =    [0.05*1.1,  0.05*1.1,   0.05*1.1,   0.05*1.1,   0.06*1.1,   0.10*1.1]
smearCoeff =    [0.05,  0.05,   0.05,   0.05,   0.06,   0.10]
NE213A_5000 = prosim.birksOptimization( pathData, 
                                        pathSim, 
                                        energy, 
                                        detector, 
                                        numBins, 
                                        birksFolder, 
                                        birksVal, 
                                        ampCoeff, 
                                        gainCoeff, 
                                        smearCoeff, 
                                        optRangeBins, 
                                        lightYieldFactor, 
                                        plotTest=False, 
                                        plotFinal=False,
                                        optimizeOff=False)
#5500 keV
energy =        '5500'
optRangeBins =  np.arange(2.00, 3.2, 0.05)
ampCoeff =      [2.8,   2.8,    2.7,    2.7,    2.7,    2.6]
gainCoeff =     [1.28,  1.20,   1.05,   1.03,   0.97,   0.82]
smearCoeff =    [0.06,  0.05,   0.05,   0.05,   0.06,   0.10]
NE213A_5500 = prosim.birksOptimization( pathData, 
                                        pathSim, 
                                        energy, 
                                        detector, 
                                        numBins, 
                                        birksFolder, 
                                        birksVal, 
                                        ampCoeff, 
                                        gainCoeff, 
                                        smearCoeff, 
                                        optRangeBins, 
                                        lightYieldFactor, 
                                        plotTest=False, 
                                        plotFinal=False,
                                        optimizeOff=False)
#6000 keV
energy =        '6000'
optRangeBins =  np.arange(2.3, 3.6, 0.05)
ampCoeff =      [1.3,   1.3,    1.2,    1.2,    1.2,    1.2]
gainCoeff =     [1.25,  1.19,   1.05,   1.03,   0.97,   0.82]
smearCoeff =    [0.04,  0.04,   0.05,   0.04,   0.06,   0.09]
NE213A_6000 = prosim.birksOptimization( pathData, 
                                        pathSim, 
                                        energy, 
                                        detector, 
                                        numBins, 
                                        birksFolder, 
                                        birksVal, 
                                        ampCoeff, 
                                        gainCoeff, 
                                        smearCoeff, 
                                        optRangeBins, 
                                        lightYieldFactor, 
                                        plotTest=False, 
                                        plotFinal=False,
                                        optimizeOff=False)

#Load from disk.
detector = 'NE213A'
E_2000 = pd.read_csv(f'{pathData}/{detector}/birks/{2000}keV.csv')
E_2500 = pd.read_csv(f'{pathData}/{detector}/birks/{2500}keV.csv')
E_3000 = pd.read_csv(f'{pathData}/{detector}/birks/{3000}keV.csv')
E_3500 = pd.read_csv(f'{pathData}/{detector}/birks/{3500}keV.csv')
E_4000 = pd.read_csv(f'{pathData}/{detector}/birks/{4000}keV.csv')
E_4500 = pd.read_csv(f'{pathData}/{detector}/birks/{4500}keV.csv')
E_5000 = pd.read_csv(f'{pathData}/{detector}/birks/{5000}keV.csv')
E_5500 = pd.read_csv(f'{pathData}/{detector}/birks/{5500}keV.csv')
E_6000 = pd.read_csv(f'{pathData}/{detector}/birks/{6000}keV.csv')

#print to screen for reference
print('-----------------------------------')
print(f'Detector: {detector}')
print(f'2.0 MeV, optimal k_B = {E_2000.optimal_kB[0]}')
print(f'2.5 MeV, optimal k_B = {E_2500.optimal_kB[0]}')
print(f'3.0 MeV, optimal k_B = {E_3000.optimal_kB[0]}')
print(f'3.5 MeV, optimal k_B = {E_3500.optimal_kB[0]}')
print(f'4.0 MeV, optimal k_B = {E_4000.optimal_kB[0]}')
print(f'4.5 MeV, optimal k_B = {E_4500.optimal_kB[0]}')
print(f'5.0 MeV, optimal k_B = {E_5000.optimal_kB[0]}')
print(f'5.5 MeV, optimal k_B = {E_5500.optimal_kB[0]}')
print(f'6.0 MeV, optimal k_B = {E_6000.optimal_kB[0]}')
print('-----------------------------------')

#Organize values for plotting
Tn = np.array([2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5, 6.0])

offset = np.array([E_2000.offset[0], E_2500.offset[0], E_3000.offset[0], E_3500.offset[0], E_4000.offset[0], E_4500.offset[0], E_5000.offset[0], E_5500.offset[0], E_6000.offset[0]])
offset_err = np.array([E_2000.offset_err[0], E_2500.offset_err[0], E_3000.offset_err[0], E_3500.offset_err[0], E_4000.offset_err[0], E_4500.offset_err[0], E_5000.offset_err[0], E_5500.offset_err[0], E_6000.offset_err[0]])

scale = np.array([E_2000.scale[0], E_2500.scale[0], E_3000.scale[0], E_3500.scale[0], E_4000.scale[0], E_4500.scale[0], E_5000.scale[0], E_5500.scale[0], E_6000.scale[0]])
scale_err = np.array([E_2000.scale_err.mean(), E_2500.scale_err.mean(), E_3000.scale_err.mean(), E_3500.scale_err.mean(), E_4000.scale_err.mean(), E_4500.scale_err.mean(), E_5000.scale_err.mean(), E_5500.scale_err.mean(), E_6000.scale_err.mean()])

smear = np.array([E_2000.optimal_smear[0], E_2500.optimal_smear[0], E_3000.optimal_smear[0], E_3500.optimal_smear[0], E_4000.optimal_smear[0], E_4500.optimal_smear[0], E_5000.optimal_smear[0], E_5500.optimal_smear[0], E_6000.optimal_smear[0]])
smear_err = np.array([E_2000.smear_err.mean(), E_2500.smear_err.mean(), E_3000.smear_err.mean(), E_3500.smear_err.mean(), E_4000.smear_err.mean(), E_4500.smear_err.mean(), E_5000.smear_err.mean(), E_5500.smear_err.mean(), E_6000.smear_err.mean()])

kB = np.array([E_2000.optimal_kB[0], E_2500.optimal_kB[0], E_3000.optimal_kB[0], E_3500.optimal_kB[0], E_4000.optimal_kB[0], E_4500.optimal_kB[0], E_5000.optimal_kB[0], E_5500.optimal_kB[0], E_6000.optimal_kB[0]])
kB_err = np.sqrt((offset_err/offset)**2 + (scale_err/scale)**2 + (smear_err/smear)**2)*kB



Tn_full = np.arange(2, 6.25, 0.25)

#kB fit
poptReci, pcovReci = curve_fit(promath.reciprocalConstantFunc, Tn, kB, sigma=kB_err, absolute_sigma=True)
pcovReci = np.diag(np.sqrt(pcovReci))

kB_fitted = promath.reciprocalConstantFunc(Tn_full, poptReci[0], poptReci[1])
total_err = np.sqrt((1/Tn_full)**2 * pcovReci[0]**2 + pcovReci[1]**2)/kB_fitted #relative value


#smear fit
poptReciSmear, pcovReciSmear = curve_fit(promath.reciprocalConstantFunc, Tn, smear)
pcovReciSmear = np.diag(np.sqrt(pcovReciSmear))


print(detector)
print('----- kB fit -----')
print(f'A = {np.round(poptReci[0], 2)} +/- {np.round(pcovReci[0], 2)}')
print(f'B = {np.round(poptReci[1], 2)} +/- {np.round(pcovReci[1], 2)}')
print('----- smear fit -----')
print(f'A = {np.round(poptReciSmear[0], 2)} +/- {np.round(pcovReciSmear[0], 2)}')
print(f'B = {np.round(poptReciSmear[1], 2)} +/- {np.round(pcovReciSmear[1], 2)}')

#Plot kB vs Tn
plt.figure()
TnVals = np.arange(1.5, 6.5, 0.01)
plt.scatter(Tn, kB, label='Optimal k$_B$ values', color='black')
plt.errorbar(Tn, kB, yerr=kB_err, linestyle='none', color='black')
plt.plot(Tn_full, promath.reciprocalConstantFunc(Tn_full, poptReci[0], poptReci[1]), color='green', label=f'${np.round(poptReci[0],3)}/T_n + {np.round(poptReci[1],3)}$')
plt.fill_between(Tn_full, promath.reciprocalConstantFunc(Tn_full, poptReci[0]+pcovReci[0], poptReci[1]+pcovReci[1]), promath.reciprocalConstantFunc(Tn_full, poptReci[0]-pcovReci[0], poptReci[1]-pcovReci[1]), color='darkgreen', alpha=0.3)
plt.ylabel('Optimal $k_B$ [mm/MeV]')
plt.xlabel('$T_n$ [MeV]')
plt.title(f'{detector}')
plt.legend()
# plt.show()

#Plot smear vs Tn
plt.figure()
TnVals = np.arange(1.5, 6.5, 0.01)
plt.scatter(Tn, smear, label='Optimal k$_B$ values', color='black')
plt.errorbar(Tn, smear, yerr=smear_err, linestyle='none', color='black')
plt.plot(Tn_full, promath.reciprocalConstantFunc(Tn_full, poptReciSmear[0], poptReciSmear[1]), color='green', label=f'${np.round(poptReciSmear[0],3)}/T_n + {np.round(poptReciSmear[1],3)}$')
plt.fill_between(Tn_full, 
promath.reciprocalConstantFunc(Tn_full, poptReciSmear[0]+pcovReciSmear[0], poptReciSmear[1]+pcovReciSmear[1]), 
promath.reciprocalConstantFunc(Tn_full, poptReciSmear[0]-pcovReciSmear[0], poptReciSmear[1]-pcovReciSmear[1]), color='darkgreen', alpha=0.3)
plt.ylabel('Optimal smearing [\%]')
plt.xlabel('$T_n$ [MeV]')
plt.title(f'{detector}')
plt.legend()
plt.show()

#Save to disk
np.save(f'{pathData}/{detector}/birks/kB_reciprocal_popt.npy', poptReci)
np.save(f'{pathData}/{detector}/birks/kB_reciprocal_pcov.npy', pcovReci)

np.save(f'{pathData}/{detector}/birks/smear_reciprocal_popt.npy', poptReciSmear)
np.save(f'{pathData}/{detector}/birks/smear_reciprocal_pcov.npy', pcovReciSmear)

np.save(f'{pathData}/{detector}/birks/kB_error.npy', kB_err)
np.save(f'{pathData}/{detector}/birks/smear_error.npy', smear_err)
np.save(f'{pathData}/{detector}/birks/total_error.npy', total_err)



# ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#   _____    _ _____  ___  ____  
#  | ____|  | |___ / / _ \| ___| 
#  |  _| _  | | |_ \| | | |___ \ 
#  | |__| |_| |___) | |_| |___) |
#  |_____\___/|____/ \___/|____/ 
#EJ305 intial parameters
detector =          'EJ305'
density =           0.893 #g/cm3
numBins =           np.arange(-1, 4, 0.05)
lightYieldFactor =  1
birksFolder =       ['1516', '1260', '1100', '1000', '0911', '0683']
birksVal =          [0.1516, 0.126,  0.11,   0.1,    0.0911,  0.0683]

#2000 keV
energy =        '2000' #DONE!
optRangeBins =  np.arange(0.15, 0.65, 0.02) 
ampCoeff =      [4.50,  4.50,   4.50,   4.50,   4.60,   4.65]
gainCoeff =     [0.85,  0.75,   0.66,   0.65,   0.61,   0.50]
smearCoeff =    [0.30,  0.30,   0.33,   0.33,   0.33,   0.31]
EJ305_2000 = prosim.birksOptimization(  pathData, 
                                        pathSim, 
                                        energy, 
                                        detector, 
                                        numBins, 
                                        birksFolder, 
                                        birksVal, 
                                        ampCoeff, 
                                        gainCoeff, 
                                        smearCoeff, 
                                        optRangeBins, 
                                        lightYieldFactor, 
                                        plotTest=False, 
                                        plotFinal=False,
                                        optimizeOff=False)
#2500 keV
energy =        '2500' #DONE!
optRangeBins =  np.arange(0.22, 0.95, 0.03)
ampCoeff =      [5.20,  5.20,   5.20,   5.20,   5.20,   5.20]
gainCoeff =     [0.90,  0.80,   0.72,   0.69,   0.64,   0.52]
smearCoeff =    [0.28,  0.27,   0.29,   0.29,   0.29,   0.29]
EJ305_2500 = prosim.birksOptimization(  pathData, 
                                        pathSim, 
                                        energy, 
                                        detector, 
                                        numBins, 
                                        birksFolder, 
                                        birksVal, 
                                        ampCoeff, 
                                        gainCoeff, 
                                        smearCoeff, 
                                        optRangeBins, 
                                        lightYieldFactor, 
                                        plotTest=False, 
                                        plotFinal=False,
                                        optimizeOff=False)
#3000 keV
energy =        '3000' #DONE!
optRangeBins =  np.arange(0.40, 1.10, 0.03)
ampCoeff =      [7.00,  6.50,   6.40,   6.40,   6.50,   6.50]
gainCoeff =     [0.97,  0.85,   0.77,   0.70,   0.68,   0.58]
smearCoeff =    [0.22,  0.19,   0.18,   0.19,   0.18,   0.18]
EJ305_3000 = prosim.birksOptimization(  pathData, 
                                        pathSim, 
                                        energy, 
                                        detector, 
                                        numBins, 
                                        birksFolder, 
                                        birksVal, 
                                        ampCoeff, 
                                        gainCoeff, 
                                        smearCoeff, 
                                        optRangeBins, 
                                        lightYieldFactor, 
                                        plotTest=False, 
                                        plotFinal=False,
                                        optimizeOff=False)
#3500 keV
energy =        '3500' #DONE!
optRangeBins =  np.arange(0.57, 1.40, 0.03)
ampCoeff =      [6.00,  5.70,   5.70,   5.60,   5.60,   5.50]
gainCoeff =     [1.05,  0.89,   0.82,   0.77,   0.74,   0.62]
smearCoeff =    [0.17,  0.21,   0.20,   0.20,   0.21,   0.23]
EJ305_3500 = prosim.birksOptimization(  pathData, 
                                        pathSim, 
                                        energy, 
                                        detector, 
                                        numBins, 
                                        birksFolder, 
                                        birksVal, 
                                        ampCoeff, 
                                        gainCoeff, 
                                        smearCoeff, 
                                        optRangeBins, 
                                        lightYieldFactor, 
                                        plotTest=False, 
                                        plotFinal=False,
                                        optimizeOff=False)
#4000 keV
energy =        '4000' #DONE!
optRangeBins =  np.arange(0.74, 1.60, 0.03)
ampCoeff =      [5.00,  5.10,   5.00,   5.00,   5.00,   5.00]
gainCoeff =     [1.05,  0.95,   0.86,   0.81,   0.76,   0.63]
smearCoeff =    [0.12,  0.12,   0.12,   0.12,   0.12,   0.13]
EJ305_4000 = prosim.birksOptimization(  pathData, 
                                        pathSim, 
                                        energy, 
                                        detector, 
                                        numBins, 
                                        birksFolder, 
                                        birksVal, 
                                        ampCoeff, 
                                        gainCoeff, 
                                        smearCoeff, 
                                        optRangeBins, 
                                        lightYieldFactor, 
                                        plotTest=False, 
                                        plotFinal=False,
                                        optimizeOff=False)
#4500 keV
energy =        '4500' #DONE!
optRangeBins =  np.arange(1.00, 2.00, 0.04)
ampCoeff =      [5.00,  4.75,   4.85,   4.85,   4.70,   4.60]
gainCoeff =     [1.08,  0.95,   0.88,   0.85,   0.79,   0.67]
smearCoeff =    [0.10,  0.10,   0.10,   0.10,   0.10,   0.11]
EJ305_4500 = prosim.birksOptimization(  pathData, 
                                        pathSim, 
                                        energy, 
                                        detector, 
                                        numBins, 
                                        birksFolder, 
                                        birksVal, 
                                        ampCoeff, 
                                        gainCoeff, 
                                        smearCoeff, 
                                        optRangeBins, 
                                        lightYieldFactor, 
                                        plotTest=False, 
                                        plotFinal=False,
                                        optimizeOff=False)
#5000 keV
energy =        '5000' #DONE!
optRangeBins =  np.arange(1.17, 2.20, 0.04)
ampCoeff =      [4.55,  0.47,   4.50,   4.50,   4.50,   4.50]
gainCoeff =     [1.10,  0.99,   0.90,   0.87,   0.81,   0.68]
smearCoeff =    [0.04,  0.05,   0.06,   0.05,   0.06,   0.06]
EJ305_5000 = prosim.birksOptimization(  pathData, 
                                        pathSim, 
                                        energy, 
                                        detector, 
                                        numBins, 
                                        birksFolder, 
                                        birksVal, 
                                        ampCoeff, 
                                        gainCoeff, 
                                        smearCoeff, 
                                        optRangeBins, 
                                        lightYieldFactor, 
                                        plotTest=False, 
                                        plotFinal=False,
                                        optimizeOff=False)
#5500 keV
energy =        '5500' #DONE!
optRangeBins =  np.arange(1.42, 2.46, 0.05)
ampCoeff =      [3.40,  3.30,   3.25,   3.20,   3.10,   3.10]
gainCoeff =     [1.08,  1.01,   0.91,   0.87,   0.83,   0.71]
smearCoeff =    [0.04,  0.05,   0.04,   0.04,   0.04,   0.06]
EJ305_5500 = prosim.birksOptimization(  pathData, 
                                        pathSim, 
                                        energy, 
                                        detector, 
                                        numBins, 
                                        birksFolder, 
                                        birksVal, 
                                        ampCoeff, 
                                        gainCoeff, 
                                        smearCoeff, 
                                        optRangeBins, 
                                        lightYieldFactor, 
                                        plotTest=False, 
                                        plotFinal=False,
                                        optimizeOff=False)
#6000 keV
energy =        '6000'
optRangeBins =  np.arange(1.71, 2.90, 0.05)
ampCoeff =      [1.70,  1.70,   1.70,   1.70,   1.70,   1.60]
gainCoeff =     [1.10,  1.00,   0.90,   0.88,   0.83,   0.75]
smearCoeff =    [0.04,  0.04,   0.04,   0.04,   0.04,   0.04]
EJ305_6000 = prosim.birksOptimization(  pathData, 
                                        pathSim, 
                                        energy, 
                                        detector, 
                                        numBins, 
                                        birksFolder, 
                                        birksVal, 
                                        ampCoeff, 
                                        gainCoeff, 
                                        smearCoeff, 
                                        optRangeBins, 
                                        lightYieldFactor, 
                                        plotTest=False, 
                                        plotFinal=False,
                                        optimizeOff=False)


#Load from disk.
detector = 'EJ305'
E_2000 = pd.read_csv(f'{pathData}/{detector}/birks/{2000}keV.csv')
E_2500 = pd.read_csv(f'{pathData}/{detector}/birks/{2500}keV.csv')
E_3000 = pd.read_csv(f'{pathData}/{detector}/birks/{3000}keV.csv')
E_3500 = pd.read_csv(f'{pathData}/{detector}/birks/{3500}keV.csv')
E_4000 = pd.read_csv(f'{pathData}/{detector}/birks/{4000}keV.csv')
E_4500 = pd.read_csv(f'{pathData}/{detector}/birks/{4500}keV.csv')
E_5000 = pd.read_csv(f'{pathData}/{detector}/birks/{5000}keV.csv')
E_5500 = pd.read_csv(f'{pathData}/{detector}/birks/{5500}keV.csv')
E_6000 = pd.read_csv(f'{pathData}/{detector}/birks/{6000}keV.csv')

#print to screen for reference
print('-----------------------------------')
print(f'Detector: {detector}')
print(f'2.0 MeV, optimal k_B = {E_2000.optimal_kB[0]}')
print(f'2.5 MeV, optimal k_B = {E_2500.optimal_kB[0]}')
print(f'3.0 MeV, optimal k_B = {E_3000.optimal_kB[0]}')
print(f'3.5 MeV, optimal k_B = {E_3500.optimal_kB[0]}')
print(f'4.0 MeV, optimal k_B = {E_4000.optimal_kB[0]}')
print(f'4.5 MeV, optimal k_B = {E_4500.optimal_kB[0]}')
print(f'5.0 MeV, optimal k_B = {E_5000.optimal_kB[0]}')
print(f'5.5 MeV, optimal k_B = {E_5500.optimal_kB[0]}')
print(f'6.0 MeV, optimal k_B = {E_6000.optimal_kB[0]}')
print('-----------------------------------')

#Organize values for plotting
Tn = np.array([2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5, 6.0])

offset = np.array([E_2000.offset[0], E_2500.offset[0], E_3000.offset[0], E_3500.offset[0], E_4000.offset[0], E_4500.offset[0], E_5000.offset[0], E_5500.offset[0], E_6000.offset[0]])
offset_err = np.array([E_2000.offset_err[0], E_2500.offset_err[0], E_3000.offset_err[0], E_3500.offset_err[0], E_4000.offset_err[0], E_4500.offset_err[0], E_5000.offset_err[0], E_5500.offset_err[0], E_6000.offset_err[0]])

scale = np.array([E_2000.scale[0], E_2500.scale[0], E_3000.scale[0], E_3500.scale[0], E_4000.scale[0], E_4500.scale[0], E_5000.scale[0], E_5500.scale[0], E_6000.scale[0]])
scale_err = np.array([E_2000.scale_err.mean(), E_2500.scale_err.mean(), E_3000.scale_err.mean(), E_3500.scale_err.mean(), E_4000.scale_err.mean(), E_4500.scale_err.mean(), E_5000.scale_err.mean(), E_5500.scale_err.mean(), E_6000.scale_err.mean()])

smear = np.array([E_2000.optimal_smear[0], E_2500.optimal_smear[0], E_3000.optimal_smear[0], E_3500.optimal_smear[0], E_4000.optimal_smear[0], E_4500.optimal_smear[0], E_5000.optimal_smear[0], E_5500.optimal_smear[0], E_6000.optimal_smear[0]])
smear_err = np.array([E_2000.smear_err.mean(), E_2500.smear_err.mean(), E_3000.smear_err.mean(), E_3500.smear_err.mean(), E_4000.smear_err.mean(), E_4500.smear_err.mean(), E_5000.smear_err.mean(), E_5500.smear_err.mean(), E_6000.smear_err.mean()])

kB = np.array([E_2000.optimal_kB[0], E_2500.optimal_kB[0], E_3000.optimal_kB[0], E_3500.optimal_kB[0], E_4000.optimal_kB[0], E_4500.optimal_kB[0], E_5000.optimal_kB[0], E_5500.optimal_kB[0], E_6000.optimal_kB[0]])
kB_err = np.sqrt((offset_err/offset)**2 + (scale_err/scale)**2 + (smear_err/smear)**2)*kB


Tn_full = np.arange(2, 6.25, 0.25)

#kB fit
poptReci, pcovReci = curve_fit(promath.reciprocalConstantFunc, Tn, kB, sigma=kB_err, absolute_sigma=True)
pcovReci = np.diag(np.sqrt(pcovReci))

kB_fitted = promath.reciprocalConstantFunc(Tn_full, poptReci[0], poptReci[1])
total_err = np.sqrt((1/Tn_full)**2 * pcovReci[0]**2 + pcovReci[1]**2)/kB_fitted #relative value


#smear fit
poptReciSmear, pcovReciSmear = curve_fit(promath.reciprocalConstantFunc, Tn, smear)
pcovReciSmear = np.diag(np.sqrt(pcovReciSmear))

# print(detector)
# print('----- kB fit -----')
# print(f'A = {np.round(poptReci[0], 2)} +/- {np.round(pcovReci[0], 2)}')
# print(f'B = {np.round(poptReci[1], 2)} +/- {np.round(pcovReci[1], 2)}')
# print('----- smear fit -----')
# print(f'A = {np.round(poptReciSmear[0], 2)} +/- {np.round(pcovReciSmear[0], 2)}')
# print(f'B = {np.round(poptReciSmear[1], 2)} +/- {np.round(pcovReciSmear[1], 2)}')

#Plot kB vs Tn
plt.figure()
TnVals = np.arange(1.5, 6.5, 0.01)
plt.scatter(Tn, kB, label='Optimal k$_B$ values', color='black')
plt.errorbar(Tn, kB, yerr=kB_err, linestyle='none', color='black')
plt.plot(Tn_full, promath.reciprocalConstantFunc(Tn_full, poptReci[0], poptReci[1]), color='green', label=f'${np.round(poptReci[0],3)}/T_n + {np.round(poptReci[1],3)}$')
plt.fill_between(Tn_full, promath.reciprocalConstantFunc(Tn_full, poptReci[0]+pcovReci[0], poptReci[1]+pcovReci[1]), promath.reciprocalConstantFunc(Tn_full, poptReci[0]-pcovReci[0], poptReci[1]-pcovReci[1]), color='darkgreen', alpha=0.3)
plt.ylabel('Optimal $k_B$ [mm/MeV]')
plt.xlabel('$T_n$ [MeV]')
plt.title(f'{detector}')
plt.legend()
# plt.show()

#Plot smear vs Tn
plt.figure()
TnVals = np.arange(1.5, 6.5, 0.01)
plt.scatter(Tn, smear, label='Optimal k$_B$ values', color='black')
plt.errorbar(Tn, smear, yerr=smear_err, linestyle='none', color='black')
plt.plot(Tn_full, promath.reciprocalConstantFunc(Tn_full, poptReciSmear[0], poptReciSmear[1]), color='green', label=f'${np.round(poptReciSmear[0],3)}/T_n + {np.round(poptReciSmear[1],3)}$')
plt.fill_between(Tn_full, 
promath.reciprocalConstantFunc(Tn_full, poptReciSmear[0]+pcovReciSmear[0], poptReciSmear[1]+pcovReciSmear[1]), 
promath.reciprocalConstantFunc(Tn_full, poptReciSmear[0]-pcovReciSmear[0], poptReciSmear[1]-pcovReciSmear[1]), color='darkgreen', alpha=0.3)
plt.ylabel('Optimal smearing [\%]')
plt.xlabel('$T_n$ [MeV]')
plt.title(f'{detector}')
plt.legend()
plt.show()

#Save to disk
np.save(f'{pathData}/{detector}/birks/kB_reciprocal_popt.npy', poptReci)
np.save(f'{pathData}/{detector}/birks/kB_reciprocal_pcov.npy', pcovReci)

np.save(f'{pathData}/{detector}/birks/smear_reciprocal_popt.npy', poptReciSmear)
np.save(f'{pathData}/{detector}/birks/smear_reciprocal_pcov.npy', pcovReciSmear)

np.save(f'{pathData}/{detector}/birks/kB_error.npy', kB_err)
np.save(f'{pathData}/{detector}/birks/smear_error.npy', smear_err)
np.save(f'{pathData}/{detector}/birks/total_error.npy', total_err)




#   _____    _ __________ _ 
#  | ____|  | |___ /___ // |
#  |  _| _  | | |_ \ |_ \| |
#  | |__| |_| |___) |__) | |
#  |_____\___/|____/____/|_|
#EJ331 intial parameters
detector =          'EJ331'
density =           0.890 #g/cm3
numBins =           np.arange(0.15, 4, 0.045)
lightYieldFactor =  1
birksFolder =       ['165', '145', '126', '086']
birksVal =          [0.165, 0.145, 0.126, 0.086]

#2000 keV
energy =        '2000' #DONE!
optRangeBins =  np.arange(0.25, 0.90, 0.025)
ampCoeff =      [2.70, 2.60, 2.70, 3.00]
gainCoeff =     [1.05, 0.95, 0.87, 0.72]
smearCoeff =    [0.28, 0.28, 0.28, 0.28]
EJ331_2000 = prosim.birksOptimization( pathData, 
                                        pathSim, 
                                        energy, 
                                        detector, 
                                        numBins, 
                                        birksFolder, 
                                        birksVal, 
                                        ampCoeff, 
                                        gainCoeff, 
                                        smearCoeff, 
                                        optRangeBins, 
                                        lightYieldFactor, 
                                        plotTest=False, 
                                        plotFinal=False,
                                        optimizeOff=False)
#2500 keV
energy =        '2500' #DONE!
optRangeBins =  np.arange(0.32, 1.00, 0.025)
ampCoeff =      [3.60, 3.50, 3.50, 3.80]
gainCoeff =     [1.11, 1.00, 0.92, 0.72]
smearCoeff =    [0.24, 0.24, 0.24, 0.24]
EJ331_2500 = prosim.birksOptimization(  pathData, 
                                        pathSim, 
                                        energy, 
                                        detector, 
                                        numBins, 
                                        birksFolder, 
                                        birksVal, 
                                        ampCoeff, 
                                        gainCoeff, 
                                        smearCoeff, 
                                        optRangeBins, 
                                        lightYieldFactor, 
                                        plotTest=False, 
                                        plotFinal=True,
                                        optimizeOff=False)
#3000 keV 
energy =        '3000' #DONE!
optRangeBins =  np.arange(0.43, 1.20, 0.025)
ampCoeff =      [4.10, 4.20, 4.20, 4.20]
gainCoeff =     [1.12, 1.00, 0.95, 0.71]
smearCoeff =    [0.22, 0.24, 0.24, 0.24]
EJ331_3000 = prosim.birksOptimization(  pathData, 
                                        pathSim, 
                                        energy, 
                                        detector, 
                                        numBins, 
                                        birksFolder, 
                                        birksVal, 
                                        ampCoeff, 
                                        gainCoeff, 
                                        smearCoeff, 
                                        optRangeBins, 
                                        lightYieldFactor, 
                                        plotTest=False, 
                                        plotFinal=False,
                                        optimizeOff=False)
#3500 keV
energy =        '3500' #DONE!
optRangeBins =  np.arange(0.49, 1.40, 0.025)
ampCoeff =      [3.70, 3.80, 3.80, 3.70]
gainCoeff =     [1.15, 1.03, 0.95, 0.73]
smearCoeff =    [0.20, 0.22, 0.22, 0.24]
EJ331_3500 = prosim.birksOptimization(  pathData, 
                                        pathSim, 
                                        energy, 
                                        detector, 
                                        numBins, 
                                        birksFolder, 
                                        birksVal, 
                                        ampCoeff, 
                                        gainCoeff, 
                                        smearCoeff, 
                                        optRangeBins, 
                                        lightYieldFactor, 
                                        plotTest=False, 
                                        plotFinal=False,
                                        optimizeOff=False)
#4000 keV
energy =        '4000' #DONE!
optRangeBins =  np.arange(0.77, 1.70, 0.035)
ampCoeff =      [3.30, 3.20, 3.20, 3.20]
gainCoeff =     [1.20, 1.07, 1.00, 0.78]
smearCoeff =    [0.18, 0.19, 0.21, 0.21]
EJ331_4000 = prosim.birksOptimization(  pathData, 
                                        pathSim, 
                                        energy, 
                                        detector, 
                                        numBins, 
                                        birksFolder, 
                                        birksVal, 
                                        ampCoeff, 
                                        gainCoeff, 
                                        smearCoeff, 
                                        optRangeBins, 
                                        lightYieldFactor, 
                                        plotTest=False, 
                                        plotFinal=False,
                                        optimizeOff=False)
#4500 keV
energy =        '4500' #DONE!
optRangeBins =  np.arange(1.07, 2.10, 0.035)
ampCoeff =      [3.20, 3.20, 3.30, 3.35]
gainCoeff =     [1.16, 1.09, 1.00, 0.78]
smearCoeff =    [0.17, 0.17, 0.16, 0.17]
EJ331_4500 = prosim.birksOptimization(  pathData, 
                                        pathSim, 
                                        energy, 
                                        detector, 
                                        numBins, 
                                        birksFolder, 
                                        birksVal, 
                                        ampCoeff, 
                                        gainCoeff, 
                                        smearCoeff, 
                                        optRangeBins, 
                                        lightYieldFactor, 
                                        plotTest=False, 
                                        plotFinal=False,
                                        optimizeOff=False)
#5000 keV
energy =        '5000' #DONE!
optRangeBins =  np.arange(1.31, 2.60, 0.045)
ampCoeff =      [3.18, 3.20, 0.30, 3.15]
gainCoeff =     [1.18, 1.09, 1.05, 0.81]
smearCoeff =    [0.17, 0.17, 0.15, 0.17]
EJ331_5000 = prosim.birksOptimization(  pathData, 
                                        pathSim, 
                                        energy, 
                                        detector, 
                                        numBins, 
                                        birksFolder, 
                                        birksVal, 
                                        ampCoeff, 
                                        gainCoeff, 
                                        smearCoeff, 
                                        optRangeBins, 
                                        lightYieldFactor, 
                                        plotTest=False, 
                                        plotFinal=False,
                                        optimizeOff=False)
#5500 keV
energy =        '5500' #DONE!
optRangeBins =  np.arange(1.56, 2.70, 0.045)
ampCoeff =      [2.50, 2.50, 2.50, 2.50]
gainCoeff =     [1.16, 1.09, 1.00, 0.81]
smearCoeff =    [0.14, 0.14, 0.14, 0.14]
EJ331_5500 = prosim.birksOptimization(  pathData, 
                                        pathSim, 
                                        energy, 
                                        detector, 
                                        numBins, 
                                        birksFolder, 
                                        birksVal, 
                                        ampCoeff, 
                                        gainCoeff, 
                                        smearCoeff, 
                                        optRangeBins, 
                                        lightYieldFactor, 
                                        plotTest=False, 
                                        plotFinal=False,
                                        optimizeOff=False)
#6000 keV
energy =        '6000' #DONE!
optRangeBins =  np.arange(1.65, 2.90, 0.045)
ampCoeff =      [1.30, 1.30, 1.30, 1.30]
gainCoeff =     [1.16, 1.09, 1.00, 0.81]
smearCoeff =    [0.11, 0.11, 0.11, 0.11]
EJ331_6000 = prosim.birksOptimization(  pathData, 
                                        pathSim, 
                                        energy, 
                                        detector, 
                                        numBins, 
                                        birksFolder, 
                                        birksVal, 
                                        ampCoeff, 
                                        gainCoeff, 
                                        smearCoeff, 
                                        optRangeBins, 
                                        lightYieldFactor, 
                                        plotTest=False, 
                                        plotFinal=False,
                                        optimizeOff=False)


#Load from disk.
detector = 'EJ331'
E_2000 = pd.read_csv(f'{pathData}/{detector}/birks/{2000}keV.csv')
E_2500 = pd.read_csv(f'{pathData}/{detector}/birks/{2500}keV.csv')
E_3000 = pd.read_csv(f'{pathData}/{detector}/birks/{3000}keV.csv')
E_3500 = pd.read_csv(f'{pathData}/{detector}/birks/{3500}keV.csv')
E_4000 = pd.read_csv(f'{pathData}/{detector}/birks/{4000}keV.csv')
E_4500 = pd.read_csv(f'{pathData}/{detector}/birks/{4500}keV.csv')
E_5000 = pd.read_csv(f'{pathData}/{detector}/birks/{5000}keV.csv')
E_5500 = pd.read_csv(f'{pathData}/{detector}/birks/{5500}keV.csv')
E_6000 = pd.read_csv(f'{pathData}/{detector}/birks/{6000}keV.csv')

#print to screen for reference
print('-----------------------------------')
print(f'Detector: {detector}')
print(f'2.0 MeV, optimal k_B = {E_2000.optimal_kB[0]}')
print(f'2.5 MeV, optimal k_B = {E_2500.optimal_kB[0]}')
print(f'3.0 MeV, optimal k_B = {E_3000.optimal_kB[0]}')
print(f'3.5 MeV, optimal k_B = {E_3500.optimal_kB[0]}')
print(f'4.0 MeV, optimal k_B = {E_4000.optimal_kB[0]}')
print(f'4.5 MeV, optimal k_B = {E_4500.optimal_kB[0]}')
print(f'5.0 MeV, optimal k_B = {E_5000.optimal_kB[0]}')
print(f'5.5 MeV, optimal k_B = {E_5500.optimal_kB[0]}')
print(f'6.0 MeV, optimal k_B = {E_6000.optimal_kB[0]}')
print('-----------------------------------')

#Organize values for plotting
Tn = np.array([2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5, 6.0])

offset = np.array([E_2000.offset[0], E_2500.offset[0], E_3000.offset[0], E_3500.offset[0], E_4000.offset[0], E_4500.offset[0], E_5000.offset[0], E_5500.offset[0], E_6000.offset[0]])
offset_err = np.array([E_2000.offset_err[0], E_2500.offset_err[0], E_3000.offset_err[0], E_3500.offset_err[0], E_4000.offset_err[0], E_4500.offset_err[0], E_5000.offset_err[0], E_5500.offset_err[0], E_6000.offset_err[0]])

scale = np.array([E_2000.scale[0], E_2500.scale[0], E_3000.scale[0], E_3500.scale[0], E_4000.scale[0], E_4500.scale[0], E_5000.scale[0], E_5500.scale[0], E_6000.scale[0]])
scale_err = np.array([E_2000.scale_err.mean(), E_2500.scale_err.mean(), E_3000.scale_err.mean(), E_3500.scale_err.mean(), E_4000.scale_err.mean(), E_4500.scale_err.mean(), E_5000.scale_err.mean(), E_5500.scale_err.mean(), E_6000.scale_err.mean()])

smear = np.array([E_2000.optimal_smear[0], E_2500.optimal_smear[0], E_3000.optimal_smear[0], E_3500.optimal_smear[0], E_4000.optimal_smear[0], E_4500.optimal_smear[0], E_5000.optimal_smear[0], E_5500.optimal_smear[0], E_6000.optimal_smear[0]])
smear_err = np.array([E_2000.smear_err.mean(), E_2500.smear_err.mean(), E_3000.smear_err.mean(), E_3500.smear_err.mean(), E_4000.smear_err.mean(), E_4500.smear_err.mean(), E_5000.smear_err.mean(), E_5500.smear_err.mean(), E_6000.smear_err.mean()])

kB = np.array([E_2000.optimal_kB[0], E_2500.optimal_kB[0], E_3000.optimal_kB[0], E_3500.optimal_kB[0], E_4000.optimal_kB[0], E_4500.optimal_kB[0], E_5000.optimal_kB[0], E_5500.optimal_kB[0], E_6000.optimal_kB[0]])
kB_err = np.sqrt((offset_err/offset)**2 + (scale_err/scale)**2 + (smear_err/smear)**2)*kB


Tn_full = np.arange(2, 6.25, 0.25)

#kB fit
poptReci, pcovReci = curve_fit(promath.reciprocalConstantFunc, Tn, kB, sigma=kB_err, absolute_sigma=True)
pcovReci = np.diag(np.sqrt(pcovReci))

kB_fitted = promath.reciprocalConstantFunc(Tn_full, poptReci[0], poptReci[1])
total_err = np.sqrt((1/Tn_full)**2 * pcovReci[0]**2 + pcovReci[1]**2)/kB_fitted #relative value


#smear fit
poptReciSmear, pcovReciSmear = curve_fit(promath.reciprocalConstantFunc, Tn, smear)
pcovReciSmear = np.diag(np.sqrt(pcovReciSmear))

# print(detector)
# print('----- kB fit -----')
# print(f'A = {np.round(poptReci[0], 2)} +/- {np.round(pcovReci[0], 2)}')
# print(f'B = {np.round(poptReci[1], 2)} +/- {np.round(pcovReci[1], 2)}')
# print('----- smear fit -----')
# print(f'A = {np.round(poptReciSmear[0], 2)} +/- {np.round(pcovReciSmear[0], 2)}')
# print(f'B = {np.round(poptReciSmear[1], 2)} +/- {np.round(pcovReciSmear[1], 2)}')


#Plot kB vs Tn
plt.figure()
TnVals = np.arange(1.5, 6.5, 0.01)
plt.scatter(Tn, kB, label='Optimal k$_B$ values', color='black')
plt.errorbar(Tn, kB, yerr=kB_err, linestyle='none', color='black')
plt.plot(Tn_full, promath.reciprocalConstantFunc(Tn_full, poptReci[0], poptReci[1]), color='green', label=f'${np.round(poptReci[0],3)}/T_n + {np.round(poptReci[1],3)}$')
plt.fill_between(Tn_full, promath.reciprocalConstantFunc(Tn_full, poptReci[0]+pcovReci[0], poptReci[1]+pcovReci[1]), promath.reciprocalConstantFunc(Tn_full, poptReci[0]-pcovReci[0], poptReci[1]-pcovReci[1]), color='darkgreen', alpha=0.3)
plt.ylabel('Optimal $k_B$ [mm/MeV]')
plt.xlabel('$T_n$ [MeV]')
plt.title(f'{detector}')
plt.legend()
# plt.show()

#Plot smear vs Tn
plt.figure()
TnVals = np.arange(1.5, 6.5, 0.01)
plt.scatter(Tn, smear, label='Optimal k$_B$ values', color='black')
plt.errorbar(Tn, smear, yerr=smear_err, linestyle='none', color='black')
plt.plot(Tn_full, promath.reciprocalConstantFunc(Tn_full, poptReciSmear[0], poptReciSmear[1]), color='green', label=f'${np.round(poptReciSmear[0],3)}/T_n + {np.round(poptReciSmear[1],3)}$')
plt.fill_between(Tn_full, 
promath.reciprocalConstantFunc(Tn_full, poptReciSmear[0]+pcovReciSmear[0], poptReciSmear[1]+pcovReciSmear[1]), 
promath.reciprocalConstantFunc(Tn_full, poptReciSmear[0]-pcovReciSmear[0], poptReciSmear[1]-pcovReciSmear[1]), color='darkgreen', alpha=0.3)
plt.ylabel('Optimal smearing [\%]')
plt.xlabel('$T_n$ [MeV]')
plt.title(f'{detector}')
plt.legend()
plt.show()

#Save to disk
np.save(f'{pathData}/{detector}/birks/kB_reciprocal_popt.npy', poptReci)
np.save(f'{pathData}/{detector}/birks/kB_reciprocal_pcov.npy', pcovReci)

np.save(f'{pathData}/{detector}/birks/smear_reciprocal_popt.npy', poptReciSmear)
np.save(f'{pathData}/{detector}/birks/smear_reciprocal_pcov.npy', pcovReciSmear)

np.save(f'{pathData}/{detector}/birks/kB_error.npy', kB_err)
np.save(f'{pathData}/{detector}/birks/smear_error.npy', smear_err)
np.save(f'{pathData}/{detector}/birks/total_error.npy', total_err)




#   _____    _ _________  _ ____  
#  | ____|  | |___ /___ \/ |  _ \ 
#  |  _| _  | | |_ \ __) | | |_) |
#  | |__| |_| |___) / __/| |  __/ 
#  |_____\___/|____/_____|_|_|    
#EJ321P intial parameters
detector =          'EJ321P'
density =           0.850 #g/cm3
numBins =           np.arange(-0.25, 4, 0.025)
lightYieldFactor =  1
birksFolder =       ['195', '145', '135', '126', '086'] #add 0.24 to test and resim (remove 0.086)
birksVal =          [0.195, 0.145, 0.135, 0.126, 0.086]

#2000 keV
energy =        '2000' #DONE!
optRangeBins =  np.arange(0.240, 0.60, 0.015)
ampCoeff =      [1.40, 1.35, 1.35, 1.35, 1.35]
gainCoeff =     [0.92, 0.75, 0.73, 0.69, 0.51]
smearCoeff =    [0.25, 0.28, 0.28, 0.28, 0.33]
EJ321P_2000 = prosim.birksOptimization( pathData, 
                                        pathSim, 
                                        energy, 
                                        detector, 
                                        numBins, 
                                        ['240','195', '145', '135', '126'], 
                                        [0.240, 0.195, 0.145, 0.135, 0.126], 
                                        ampCoeff, 
                                        gainCoeff, 
                                        smearCoeff, 
                                        optRangeBins, 
                                        lightYieldFactor, 
                                        plotTest=False, 
                                        plotFinal=True,
                                        optimizeOff=False)
#2500 keV
energy =        '2500' #DONE!
optRangeBins =  np.arange(0.260, 0.80, 0.025) 
ampCoeff =      [2.00, 2.02, 2.20, 2.20, 2.0]
gainCoeff =     [0.92, 0.80, 0.70, 0.65, 0.62]
smearCoeff =    [0.28, 0.26, 0.26, 0.26, 0.27]
EJ321P_2500 = prosim.birksOptimization( pathData, 
                                        pathSim, 
                                        energy, 
                                        detector, 
                                        numBins, 
                                        ['240','195', '145', '135', '126'], 
                                        [0.240, 0.195, 0.145, 0.135, 0.126], 
                                        ampCoeff, 
                                        gainCoeff, 
                                        smearCoeff, 
                                        optRangeBins, 
                                        lightYieldFactor, 
                                        plotTest=False, 
                                        plotFinal=False,
                                        optimizeOff=False)
#3000 keV
energy =        '3000' #DONE!
optRangeBins =  np.arange(0.33, 1.00, 0.025)
ampCoeff =      [2.25, 2.40, 2.40, 2.40, 2.40,]
gainCoeff =     [0.90, 0.76, 0.70, 0.68, 0.52]
smearCoeff =    [0.20, 0.24, 0.24, 0.25, 0.25]
EJ321P_3000 = prosim.birksOptimization( pathData, 
                                        pathSim, 
                                        energy, 
                                        detector, 
                                        numBins, 
                                        birksFolder, 
                                        birksVal, 
                                        ampCoeff, 
                                        gainCoeff, 
                                        smearCoeff, 
                                        optRangeBins, 
                                        lightYieldFactor, 
                                        plotTest=False, 
                                        plotFinal=False,
                                        optimizeOff=False)
#3500 keV
energy =        '3500' #DONE!
optRangeBins =  np.arange(0.38, 1.20, 0.025)
ampCoeff =      [2.00, 2.10, 2.10, 2.20, 2.15,]
gainCoeff =     [0.94, 0.80, 0.74, 0.69, 0.55]
smearCoeff =    [0.20, 0.24, 0.24, 0.25, 0.26]
EJ321P_3500 = prosim.birksOptimization( pathData, 
                                        pathSim, 
                                        energy, 
                                        detector, 
                                        numBins, 
                                        birksFolder, 
                                        birksVal, 
                                        ampCoeff, 
                                        gainCoeff, 
                                        smearCoeff, 
                                        optRangeBins, 
                                        lightYieldFactor, 
                                        plotTest=False, 
                                        plotFinal=False,
                                        optimizeOff=False)
#4000 keV
energy =        '4000' #DONE!
optRangeBins =  np.arange(0.55, 1.40, 0.025)
ampCoeff =      [1.85, 1.80, 1.80, 1.80, 1.88]
gainCoeff =     [1.00, 0.80, 0.78, 0.74, 0.57]
smearCoeff =    [0.21, 0.23, 0.23, 0.25, 0.24]
EJ321P_4000 = prosim.birksOptimization( pathData, 
                                        pathSim, 
                                        energy, 
                                        detector, 
                                        numBins, 
                                        birksFolder, 
                                        birksVal, 
                                        ampCoeff, 
                                        gainCoeff, 
                                        smearCoeff, 
                                        optRangeBins, 
                                        lightYieldFactor, 
                                        plotTest=False, 
                                        plotFinal=False,
                                        optimizeOff=False)
#4500 keV
energy =        '4500' #DONE!
optRangeBins =  np.arange(0.75, 1.70, 0.025)
ampCoeff =      [1.83, 1.81, 1.89, 1.80, 1.70]
gainCoeff =     [1.04, 0.85, 0.81, 0.75, 0.62]
smearCoeff =    [0.17, 0.18, 0.19, 0.17, 0.22]
EJ321P_4500 = prosim.birksOptimization( pathData, 
                                        pathSim, 
                                        energy, 
                                        detector, 
                                        numBins, 
                                        birksFolder, 
                                        birksVal, 
                                        ampCoeff, 
                                        gainCoeff, 
                                        smearCoeff, 
                                        optRangeBins, 
                                        lightYieldFactor, 
                                        plotTest=False, 
                                        plotFinal=False,
                                        optimizeOff=False)
#5000 keV
energy =        '5000' #DONE!
optRangeBins =  np.arange(0.88, 1.85, 0.035)
ampCoeff =      [1.80, 1.70, 1.70, 0.17, 1.64]
gainCoeff =     [1.05, 0.85, 0.83, 0.80, 0.62]
smearCoeff =    [0.14, 0.14, 0.14, 0.14, 0.15]
EJ321P_5000 = prosim.birksOptimization( pathData, 
                                        pathSim, 
                                        energy, 
                                        detector, 
                                        numBins, 
                                        birksFolder, 
                                        birksVal, 
                                        ampCoeff, 
                                        gainCoeff, 
                                        smearCoeff, 
                                        optRangeBins, 
                                        lightYieldFactor, 
                                        plotTest=False, 
                                        plotFinal=False,
                                        optimizeOff=False)
#5500 keV
energy =        '5500' #DONE!
optRangeBins =  np.arange(1.16, 2.10, 0.045)
ampCoeff =      [1.40, 1.40, 1.40, 1.40, 1.40]
gainCoeff =     [1.05, 0.85, 0.83, 0.80, 0.63]
smearCoeff =    [0.12, 0.13, 0.12, 0.12, 0.14]
EJ321P_5500 = prosim.birksOptimization( pathData, 
                                        pathSim, 
                                        energy, 
                                        detector, 
                                        numBins, 
                                        birksFolder, 
                                        birksVal, 
                                        ampCoeff, 
                                        gainCoeff, 
                                        smearCoeff, 
                                        optRangeBins, 
                                        lightYieldFactor, 
                                        plotTest=False, 
                                        plotFinal=False,
                                        optimizeOff=False)

#6000 keV
energy =        '6000'#DONE!
optRangeBins =  np.arange(1.34, 2.40, 0.05)
ampCoeff =      [0.80, 0.80, 0.80, 0.80, 0.75]
gainCoeff =     [1.05, 0.85, 0.83, 0.80, 0.63]
smearCoeff =    [0.11, 0.12, 0.11, 0.11, 0.13]
EJ321P_6000 = prosim.birksOptimization( pathData, 
                                        pathSim, 
                                        energy, 
                                        detector, 
                                        numBins, 
                                        birksFolder, 
                                        birksVal, 
                                        ampCoeff, 
                                        gainCoeff, 
                                        smearCoeff, 
                                        optRangeBins, 
                                        lightYieldFactor, 
                                        plotTest=False, 
                                        plotFinal=False,
                                        optimizeOff=False)


#Load from disk.
detector = 'EJ321P'
E_2000 = pd.read_csv(f'{pathData}/{detector}/birks/{2000}keV.csv')
E_2500 = pd.read_csv(f'{pathData}/{detector}/birks/{2500}keV.csv')
E_3000 = pd.read_csv(f'{pathData}/{detector}/birks/{3000}keV.csv')
E_3500 = pd.read_csv(f'{pathData}/{detector}/birks/{3500}keV.csv')
E_4000 = pd.read_csv(f'{pathData}/{detector}/birks/{4000}keV.csv')
E_4500 = pd.read_csv(f'{pathData}/{detector}/birks/{4500}keV.csv')
E_5000 = pd.read_csv(f'{pathData}/{detector}/birks/{5000}keV.csv')
E_5500 = pd.read_csv(f'{pathData}/{detector}/birks/{5500}keV.csv')
E_6000 = pd.read_csv(f'{pathData}/{detector}/birks/{6000}keV.csv')

#print to screen for reference
print('-----------------------------------')
print(f'Detector: {detector}')
print(f'2.0 MeV, optimal k_B = {E_2000.optimal_kB[0]}')
print(f'2.5 MeV, optimal k_B = {E_2500.optimal_kB[0]}')
print(f'3.0 MeV, optimal k_B = {E_3000.optimal_kB[0]}')
print(f'3.5 MeV, optimal k_B = {E_3500.optimal_kB[0]}')
print(f'4.0 MeV, optimal k_B = {E_4000.optimal_kB[0]}')
print(f'4.5 MeV, optimal k_B = {E_4500.optimal_kB[0]}')
print(f'5.0 MeV, optimal k_B = {E_5000.optimal_kB[0]}')
print(f'5.5 MeV, optimal k_B = {E_5500.optimal_kB[0]}')
print(f'6.0 MeV, optimal k_B = {E_6000.optimal_kB[0]}')
print('-----------------------------------')

#Organize values for plotting
Tn = np.array([2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5, 6.0])

offset = np.array([E_2000.offset[0], E_2500.offset[0], E_3000.offset[0], E_3500.offset[0], E_4000.offset[0], E_4500.offset[0], E_5000.offset[0], E_5500.offset[0], E_6000.offset[0]])
offset_err = np.array([E_2000.offset_err[0], E_2500.offset_err[0], E_3000.offset_err[0], E_3500.offset_err[0], E_4000.offset_err[0], E_4500.offset_err[0], E_5000.offset_err[0], E_5500.offset_err[0], E_6000.offset_err[0]])

scale = np.array([E_2000.scale[0], E_2500.scale[0], E_3000.scale[0], E_3500.scale[0], E_4000.scale[0], E_4500.scale[0], E_5000.scale[0], E_5500.scale[0], E_6000.scale[0]])
scale_err = np.array([E_2000.scale_err.mean(), E_2500.scale_err.mean(), E_3000.scale_err.mean(), E_3500.scale_err.mean(), E_4000.scale_err.mean(), E_4500.scale_err.mean(), E_5000.scale_err.mean(), E_5500.scale_err.mean(), E_6000.scale_err.mean()])

smear = np.array([E_2000.optimal_smear[0], E_2500.optimal_smear[0], E_3000.optimal_smear[0], E_3500.optimal_smear[0], E_4000.optimal_smear[0], E_4500.optimal_smear[0], E_5000.optimal_smear[0], E_5500.optimal_smear[0], E_6000.optimal_smear[0]])
smear_err = np.array([E_2000.smear_err.mean(), E_2500.smear_err.mean(), E_3000.smear_err.mean(), E_3500.smear_err.mean(), E_4000.smear_err.mean(), E_4500.smear_err.mean(), E_5000.smear_err.mean(), E_5500.smear_err.mean(), E_6000.smear_err.mean()])

kB = np.array([E_2000.optimal_kB[0], E_2500.optimal_kB[0], E_3000.optimal_kB[0], E_3500.optimal_kB[0], E_4000.optimal_kB[0], E_4500.optimal_kB[0], E_5000.optimal_kB[0], E_5500.optimal_kB[0], E_6000.optimal_kB[0]])
kB_err = np.sqrt((offset_err/offset)**2 + (scale_err/scale)**2 + (smear_err/smear)**2)*kB


Tn_full = np.arange(2, 6.25, 0.25)

#kB fit
poptReci, pcovReci = curve_fit(promath.reciprocalConstantFunc, Tn, kB, sigma=kB_err, absolute_sigma=True)
pcovReci = np.diag(np.sqrt(pcovReci))

kB_fitted = promath.reciprocalConstantFunc(Tn_full, poptReci[0], poptReci[1])
total_err = np.sqrt((1/Tn_full)**2 * pcovReci[0]**2 + pcovReci[1]**2)/kB_fitted #relative value

#smear fit
poptReciSmear, pcovReciSmear = curve_fit(promath.reciprocalConstantFunc, Tn, smear)
pcovReciSmear = np.diag(np.sqrt(pcovReciSmear))

print(detector)
print('----- kB fit -----')
print(f'A = {np.round(poptReci[0], 2)} +/- {np.round(pcovReci[0], 2)}')
print(f'B = {np.round(poptReci[1], 2)} +/- {np.round(pcovReci[1], 2)}')
print('----- smear fit -----')
print(f'A = {np.round(poptReciSmear[0], 2)} +/- {np.round(pcovReciSmear[0], 2)}')
print(f'B = {np.round(poptReciSmear[1], 2)} +/- {np.round(pcovReciSmear[1], 2)}')


#Plot kB vs Tn
plt.figure()
TnVals = np.arange(1.5, 6.5, 0.01)
plt.scatter(Tn, kB, label='Optimal k$_B$ values', color='black')
plt.errorbar(Tn, kB, yerr=kB_err, linestyle='none', color='black')
plt.plot(Tn_full, promath.reciprocalConstantFunc(Tn_full, poptReci[0], poptReci[1]), color='green', label=f'${np.round(poptReci[0],3)}/T_n + {np.round(poptReci[1],3)}$')
plt.fill_between(Tn_full, promath.reciprocalConstantFunc(Tn_full, poptReci[0]+pcovReci[0], poptReci[1]+pcovReci[1]), promath.reciprocalConstantFunc(Tn_full, poptReci[0]-pcovReci[0], poptReci[1]-pcovReci[1]), color='darkgreen', alpha=0.3)
plt.ylabel('Optimal $k_B$ [mm/MeV]')
plt.xlabel('$T_n$ [MeV]')
plt.title(f'{detector}')
plt.legend()
# plt.show()

#Plot smear vs Tn
plt.figure()
TnVals = np.arange(1.5, 6.5, 0.01)
plt.scatter(Tn, smear, label='Optimal smearkB values', color='black')
plt.errorbar(Tn, smear, yerr=smear_err, linestyle='none', color='black')
plt.plot(Tn_full, promath.reciprocalConstantFunc(Tn_full, poptReciSmear[0], poptReciSmear[1]), color='green', label=f'${np.round(poptReciSmear[0],3)}/T_n + {np.round(poptReciSmear[1],3)}$')
plt.fill_between(Tn_full, 
promath.reciprocalConstantFunc(Tn_full, poptReciSmear[0]+pcovReciSmear[0], poptReciSmear[1]+pcovReciSmear[1]), 
promath.reciprocalConstantFunc(Tn_full, poptReciSmear[0]-pcovReciSmear[0], poptReciSmear[1]-pcovReciSmear[1]), color='darkgreen', alpha=0.3)
plt.ylabel('Optimal smearing [\%]')
plt.xlabel('$T_n$ [MeV]')
plt.title(f'{detector}')
plt.legend()
plt.show()

#Save to disk
np.save(f'{pathData}/{detector}/birks/kB_reciprocal_popt.npy', poptReci)
np.save(f'{pathData}/{detector}/birks/kB_reciprocal_pcov.npy', pcovReci)

np.save(f'{pathData}/{detector}/birks/smear_reciprocal_popt.npy', poptReciSmear)
np.save(f'{pathData}/{detector}/birks/smear_reciprocal_pcov.npy', pcovReciSmear)

np.save(f'{pathData}/{detector}/birks/kB_error.npy', kB_err)
np.save(f'{pathData}/{detector}/birks/smear_error.npy', smear_err)
np.save(f'{pathData}/{detector}/birks/total_error.npy', total_err)




#REMOVE ME!

print('-----------------------------------')
print(f'Detector: {detector}')
E20 = E_2000.smear.to_numpy()
E25 = E_2500.smear.to_numpy()
E30 = E_3000.smear.to_numpy()
E35 = E_3500.smear.to_numpy()
E40 = E_4000.smear.to_numpy()
E45 = E_4500.smear.to_numpy()
E50 = E_5000.smear.to_numpy()
E55 = E_5500.smear.to_numpy()
E60 = E_6000.smear.to_numpy()

print(1-np.min(E20)/np.max(E20))
print(1-np.min(E25)/np.max(E25))
print(1-np.min(E30)/np.max(E30))
print(1-np.min(E35)/np.max(E35))
print(1-np.min(E40)/np.max(E40))
print(1-np.min(E45)/np.max(E45))
print(1-np.min(E50)/np.max(E50))
print(1-np.min(E55)/np.max(E55))
print(1-np.min(E60)/np.max(E60))

plt.plot(E20)
plt.plot(E25)
plt.plot(E30)
plt.plot(E35)
plt.plot(E40)
plt.plot(E45)
plt.plot(E50)
plt.plot(E55)
plt.plot(E60)
plt.show()


print(E20)
print(E25)
print(E30)
print(E35)
print(E40)
print(E45)
print(E50)
print(E55)
print(E60)
print('-----------------------------------')