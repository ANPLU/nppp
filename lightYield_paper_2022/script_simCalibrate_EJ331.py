#!/usr/bin/env python3
import sys
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit

# IMPORT FROM LIBRARY DIRECTORY
sys.path.insert(0, "../library/")
sys.path.insert(0, "../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "sim_analysis/")
sys.path.insert(0, "data_analysis/")

import digidata
import nicholai_plot_helper as nplt
import processing_utility as prout
import processing_math as promath
import processing_data as prodata
import processing_pd as propd
import processing_simulation as prosim


"""
Script for calibrating simulations light yield to MeVee
"""

path_sim = '/media/gheed/Seagate_Expansion_Drive1/data/simulation/'
names = [ 'evtNum', 
        'optPhotonSum', 
        'optPhotonSumQE', 
        'optPhotonSumCompton', 
        'optPhotonSumComptonQE', 
        'optPhotonSumNPQE', 
        'xLoc', 
        'yLoc', 
        'zLoc', 
        'CsMin', 
        'optPhotonParentID', 
        'optPhotonParentCreatorProcess', 
        'optPhotonParentStartingEnergy',
        'total_edep',
        'gamma_edep',
        'proton_edep',
        'nScatters',
        'nScattersProton']

detector = 'EJ331'

#pencilbeam
Cs137_sim_pen = pd.read_csv(path_sim + f"{detector}/Cs137/pencilbeam/CSV_optphoton_data_sum.csv", names=names)
Cs137_sim_pen = pd.concat([Cs137_sim_pen, pd.read_csv(path_sim + f"{detector}/Cs137/pencilbeam/part2/CSV_optphoton_data_sum.csv", names=names)])
Cs137_sim_pen = pd.concat([Cs137_sim_pen, pd.read_csv(path_sim + f"{detector}/Cs137/pencilbeam/part3/CSV_optphoton_data_sum.csv", names=names)])

Th232_sim_pen = pd.read_csv(path_sim + f"{detector}/Th232/pencilbeam/CSV_optphoton_data_sum.csv", names=names)
Th232_sim_pen = pd.concat([Th232_sim_pen, pd.read_csv(path_sim + f'{detector}/Th232/pencilbeam/part2/CSV_optphoton_data_sum.csv', names=names)])
Th232_sim_pen = pd.concat([Th232_sim_pen, pd.read_csv(path_sim + f'{detector}/Th232/pencilbeam/part3/CSV_optphoton_data_sum.csv', names=names)])

AmBe_sim_pen = pd.read_csv(path_sim + f"{detector}/4.44/pencilbeam/CSV_optphoton_data_sum.csv", names=names)
AmBe_sim_pen = pd.concat([AmBe_sim_pen, pd.read_csv(path_sim + f'{detector}/4.44/pencilbeam/part2/CSV_optphoton_data_sum.csv', names=names)])
AmBe_sim_pen = pd.concat([AmBe_sim_pen, pd.read_csv(path_sim + f'{detector}/4.44/pencilbeam/part3/CSV_optphoton_data_sum.csv', names=names)])

#reset index pencilbeam 
Cs137_sim_pen =  Cs137_sim_pen.reset_index()
Th232_sim_pen =  Th232_sim_pen.reset_index()
AmBe_sim_pen =   AmBe_sim_pen.reset_index()



simLightYieldFactor = 1
#randomize binning pencilbeam
y, x = np.histogram(Cs137_sim_pen.optPhotonSumComptonQE, bins=4096, range=[0, 4096])
Cs137_sim_pen = pd.DataFrame({'optPhotonSumComptonQE':prodata.getRandDist(x, y)}) * simLightYieldFactor

y, x = np.histogram(Th232_sim_pen.optPhotonSumComptonQE, bins=4096, range=[0, 4096])
Th232_sim_pen = pd.DataFrame({'optPhotonSumComptonQE':prodata.getRandDist(x, y)}) * simLightYieldFactor

y, x = np.histogram(AmBe_sim_pen.optPhotonSumComptonQE, bins=4096, range=[0, 4096])
AmBe_sim_pen = pd.DataFrame({'optPhotonSumComptonQE':prodata.getRandDist(x, y)}) * simLightYieldFactor


start = 109     
stop = 155
counts, bin_edges = np.histogram(Cs137_sim_pen.optPhotonSumComptonQE, bins = np.arange(start*0.8, stop*1.2, 2))
bin_centers = prodata.getBinCenters(bin_edges)
Ef = 0.66166
Cs137_sim_fit  =  prosim.calibrationFitBinned(bin_centers, counts, start, stop, Ef=Ef, plot=True, compton=False)
mean = Cs137_sim_fit['gauss_mean']
std = Cs137_sim_fit['gauss_std']
Cs137_sim_results = {}
Cs137_sim_results['mean'] = np.mean(Cs137_sim_pen.query(f'optPhotonSumComptonQE>{mean-3*std} and optPhotonSumComptonQE<{mean+3*std}').optPhotonSumComptonQE)
Cs137_sim_results['mean_err']  = np.std(Cs137_sim_pen.query(f'optPhotonSumComptonQE>{mean-3*std} and optPhotonSumComptonQE<{mean+3*std}').optPhotonSumComptonQE)/np.sqrt(len(Cs137_sim_pen.query(f'optPhotonSumComptonQE>{mean-3*std} and optPhotonSumComptonQE<{mean+3*std}').optPhotonSumComptonQE))

start = 602
stop = 725
counts, bin_edges = np.histogram(Th232_sim_pen.optPhotonSumComptonQE, bins = np.arange(start*0.8, stop*1.2, 15))
bin_centers = prodata.getBinCenters(bin_edges)
Ef = 2.614533
Th232_sim_fit = prosim.calibrationFitBinned(bin_centers, counts, start, stop, Ef=Ef, plot=True, compton=False)
mean = Th232_sim_fit['gauss_mean']
std = Th232_sim_fit['gauss_std']
Th232_sim_results = {}
Th232_sim_results['mean'] = np.mean(Th232_sim_pen.query(f'optPhotonSumComptonQE>{mean-3*std} and optPhotonSumComptonQE<{mean+3*std}').optPhotonSumComptonQE)
Th232_sim_results['mean_err']  = np.std(Th232_sim_pen.query(f'optPhotonSumComptonQE>{mean-3*std} and optPhotonSumComptonQE<{mean+3*std}').optPhotonSumComptonQE)/np.sqrt(len(Th232_sim_pen.query(f'optPhotonSumComptonQE>{mean-3*std} and optPhotonSumComptonQE<{mean+3*std}').optPhotonSumComptonQE))

start = 1050
stop = 1300
counts, bin_edges = np.histogram(AmBe_sim_pen.optPhotonSumComptonQE, bins = np.arange(start*0.8, stop*1.2, 20))
bin_centers = prodata.getBinCenters(bin_edges)
Ef = 4.439
AmBe_sim_fit = prosim.calibrationFitBinned(bin_centers, counts, start, stop, Ef=Ef, plot=True, compton=False)
mean = AmBe_sim_fit['gauss_mean']
std = AmBe_sim_fit['gauss_std']
AmBe_sim_results = {}
AmBe_sim_results['mean'] = np.mean(AmBe_sim_pen.query(f'optPhotonSumComptonQE>{mean-3*std} and optPhotonSumComptonQE<{mean+3*std}').optPhotonSumComptonQE)
AmBe_sim_results['mean_err']  = np.std(AmBe_sim_pen.query(f'optPhotonSumComptonQE>{mean-3*std} and optPhotonSumComptonQE<{mean+3*std}').optPhotonSumComptonQE)/np.sqrt(len(AmBe_sim_pen.query(f'optPhotonSumComptonQE>{mean-3*std} and optPhotonSumComptonQE<{mean+3*std}').optPhotonSumComptonQE))


# FITTING SCINT. PHOTONS TO MeVee
optPhoton =     np.array([0, Cs137_sim_results['mean'],    Th232_sim_results['mean'],      AmBe_sim_results['mean']])
CE_energy =     np.array([0, Cs137_sim_fit['recoil_max'],  Th232_sim_fit['recoil_max'],    AmBe_sim_fit['recoil_max']])

# optPhoton =     np.array([Cs137_sim_results['mean'],    Th232_sim_results['mean'],      AmBe_sim_results['mean']])
# CE_energy =     np.array([Cs137_sim_fit['recoil_max'],  Th232_sim_fit['recoil_max'],    AmBe_sim_fit['recoil_max']])

popt, pcov = curve_fit(promath.linearFunc, optPhoton, CE_energy)
plt.plot(np.arange(0,2000,1), promath.linearFunc(np.arange(0,2000,1), popt[0], popt[1]))
plt.scatter(optPhoton, CE_energy)
plt.xlabel('#scint photons')
plt.ylabel('Energy [MeVee]')
plt.show()


np.save(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Simcal/popt', popt)
np.save(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Simcal/pcov', pcov)


