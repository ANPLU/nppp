from re import I
import sys
from tkinter import X
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
from numpy.core.arrayprint import SubArrayFormat
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit
from scipy import interpolate

# from matplotlib.colors import LinearSegmentedColormap


# IMPORT FROM LIBRARY DIRECTORY
sys.path.insert(0, "../library/")
sys.path.insert(0, "../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "../")
sys.path.insert(0, "data_analysis/")

import processing_math as promath
import processing_data as prodata
import processing_pd as propd
import processing_simulation as prosim
import figure_style as fs


path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop
pathSim = '/media/gheed/Seagate_Expansion_Drive1/data/simulation'
pathData = '/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper'

#Load data from NE213A
NE213A = propd.load_parquet_merge(path, [2121], keep_col=['qdc_lg_ch1', 'qdc_sg_ch1'], full=False)

#Load data from EJ305
EJ305 = propd.load_parquet_merge(path, [3439], keep_col=['qdc_lg_ch1', 'qdc_sg_ch1'], full=False)

#Load data from EJ321P
EJ321P = propd.load_parquet_merge(path, [2423], keep_col=['qdc_lg_ch1', 'qdc_sg_ch1'], full=False)

#Load data from EJ331
EJ331 = propd.load_parquet_merge(path, [1881], keep_col=['qdc_lg_ch1', 'qdc_sg_ch1'], full=False)

ratioNE213A = (NE213A.qdc_lg_ch1-NE213A.qdc_sg_ch1)/NE213A.qdc_lg_ch1

plt.subplot(4,1,1)
plt.hist(ratioNE213A, bins=np.arange(-0.5,1.5,0.001), histtype='step', label='NE213A')
plt.legend()
plt.yscale('log')
plt.subplot(4,1,2)
plt.hist(EJ305.qdc_lg_ch1/(EJ305.qdc_lg_ch1+EJ305.qdc_sg_ch1), bins=np.arange(-0.5,1.5,0.001), histtype='step', label='EJ305')
plt.legend()
plt.yscale('log')
plt.subplot(4,1,3)
plt.hist(EJ321P.qdc_lg_ch1/(EJ321P.qdc_lg_ch1+EJ321P.qdc_sg_ch1), bins=np.arange(-0.5,1.5,0.001), histtype='step', label='EJ321P')
plt.legend()
plt.yscale('log')
plt.subplot(4,1,4)
plt.hist(EJ331.qdc_lg_ch1/(EJ331.qdc_lg_ch1+EJ331.qdc_sg_ch1), bins=np.arange(-0.5,1.5,0.001), histtype='step', label='EJ331')
plt.legend()
plt.yscale('log')

plt.show()