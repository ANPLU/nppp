#!/usr/bin/env python3
from re import I
import sys
from tkinter import X
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
from numpy.core.arrayprint import SubArrayFormat
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit
from scipy import interpolate

# from matplotlib.colors import LinearSegmentedColormap
"""
Script to studythe relative location of HH TP FD and SMD.
"""

# IMPORT FROM LIBRARY DIRECTORY
sys.path.insert(0, "../library/")
sys.path.insert(0, "../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "../")
sys.path.insert(0, "data_analysis/")

import processing_math as promath
import processing_data as prodata
import processing_pd as propd
import processing_simulation as prosim
import figure_style as fs


for detector in ['NE213A', 'EJ305', 'EJ321P', 'EJ331']:
# detector = 'NE213A'

    gate = 'LG'
    pathData = '/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper'

    HH_data = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/HH_TP_FD/HH_data_{gate}.pkl')
    TP_data = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/HH_TP_FD/TP_data_{gate}.pkl')
    FD_data = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/HH_TP_FD/FD_data_{gate}.pkl')

    energy = HH_data.energy

    LY_sim = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/simulation/sim_LightYield.pkl')

    SMD_loc = LY_sim.means
    SMD_loc_err = LY_sim.means_err

    HH_loc = HH_data.HH_loc
    HH_loc_err = HH_data.HH_loc_err

    TP_loc = TP_data.TP_loc
    TP_loc_err = TP_data.TP_loc_err

    FD_loc = FD_data.FD_loc
    FD_loc_err = FD_data.FD_loc_err


    SMD_variation = (SMD_loc.to_numpy()-HH_loc.to_numpy())/HH_loc.to_numpy()
    TP_variation = (TP_loc.to_numpy()-HH_loc.to_numpy())/HH_loc.to_numpy()
    FD_variation = (FD_loc.to_numpy()-HH_loc.to_numpy())/HH_loc.to_numpy()

    plt.suptitle(detector)
    # plt.subplot(3,1,1)
    plt.scatter(energy.to_numpy(), TP_variation*100, label='TP', color='red')
    # plt.scatter(energy.to_numpy(), HH_loc, label='HH', color='blue')
    # plt.scatter(energy.to_numpy(), TP_loc, label='TP', color='red')
    # plt.ylim([-15,15])
    # plt.hlines(0,2,6, color='black', ls='dashed', label='HH location')

    # plt.subplot(3,1,2)
    plt.scatter(energy.to_numpy(), FD_variation*100, label='FD', color='green')
    # plt.scatter(energy.to_numpy(), FD_loc, label='FD', color='green')
    # plt.ylim([-15,15])

    # plt.hlines(0,2,6, color='black', ls='dashed', label='HH location')

    # plt.subplot(3,1,3)
    plt.scatter(energy.to_numpy(), SMD_variation*100, label='SMD', marker='v', color='black')
    # plt.scatter(energy.to_numpy(), SMD_loc, label='SMD', marker='v', color='black')
    plt.ylim([-15,15])
    plt.hlines(0,2,6, color='black', ls='dashed', label='HH location')
    plt.xlabel('Proton Energy [MeV]')
    plt.ylabel('Percent of HH locations [%]')
    plt.legend()


    plt.savefig(f'/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/lightYield_paper_2022/figures/HH_TD_FD_SMD_loc_relative_{detector}.pdf')

    plt.show()



