#!/usr/bin/env python3
from faulthandler import cancel_dump_traceback_later
from glob import escape
import sys
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit

# IMPORT FROM LIBRARY DIRECTORY
sys.path.insert(0, "../library/")
sys.path.insert(0, "../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "sim_analysis/")
sys.path.insert(0, "data_analysis/")

import digidata
import nicholai_plot_helper as nplt
import processing_utility as prout
import processing_math as promath
import processing_data as prodata
import processing_pd as propd
import processing_simulation as prosim


"""
Script for running both data and simulation and calibrating data of NE213A detector.
"""
#TODO fit gaussian to pencilbeam data on the right side of the distyribution.


PMT_gain = 4e6 #Nominal gain is 7'000'000 at nominal A/lm (model: 9821B p1)
att_factor_12dB = 3.98
att_factor_16dB = 6.31
plot = False


##############################
# LOADING DATA
##############################
path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop
df1 = propd.load_parquet_merge(path, [2518], keep_col=[f'qdc_lg_ch2', f'qdc_lg_ch3', f'qdc_lg_ch4', f'qdc_lg_ch5'], full=False)
df2 = propd.load_parquet_merge(path, np.arange(2121, 2121+20), keep_col=[f'qdc_lg_ch2', f'qdc_lg_ch3', f'qdc_lg_ch4', f'qdc_lg_ch5'], full=False)
# numBins = np.arange(5000, 40000, 75)
for tofCh in np.arange(2,6,1):

    #Na22 1.274 MeV
    y,x = np.histogram(df1[f'qdc_lg_ch{tofCh}'], bins=np.arange(500, 7500, 10))
    x = prodata.getBinCenters(x)
    if tofCh == 2:
        start = 1311
        stop = 1630
    elif tofCh == 3:
        start = 1770
        stop = 2080
    elif tofCh == 4:
        start = 2377
        stop = 2748
    elif tofCh == 5:
        start = 2070
        stop = 2400
    popt1, pcov1 = promath.gaussFit(x, y, start=start, stop=stop, error=True)
    # plt.plot(x,y)
    # plt.plot(x, promath.gaussFunc(x, popt1[0],popt1[1],popt1[2]))
    # plt.show()  

    #4.44 MeV CE
    y,x = np.histogram(df2[f'qdc_lg_ch{tofCh}'], bins=np.arange(5000, 40000, 75))
    x = prodata.getBinCenters(x)
    if tofCh == 2:
        start = 9815
        stop = 12100
    elif tofCh == 3:
        start = 10270
        stop = 12300
    elif tofCh == 4:
        start = 11550
        stop = 13980
    elif tofCh == 5:
        start = 12970
        stop = 15990

    popt2, pcov2 = promath.gaussFit(x, y, start=start, stop=stop, error=True) 

    #Find 89% of CE positions
    for xVal in np.arange(5000,40000,.1):
        yVal = promath.gaussFunc(xVal, popt2[0], popt2[1], popt2[2])
        if xVal>popt2[1] and yVal <= popt2[0]*0.89: #look past mean values of Gaussian
            xLoc = xVal
            break

    # plt.plot(x,y)
    # plt.scatter(xLoc, popt2[0]*0.89)
    # plt.plot(x, promath.gaussFunc(x, popt2[0],popt2[1],popt2[2]))
    # plt.show()    

    #fitting linear calibration function
    E = [0, 1.274, 4.1974]
    QDC_loc = [0, popt1[1], xLoc] #Using Knox and Miller
    QDC_loc_err = [0, pcov1[1], pcov2[1]]
    
    popt, pcov = curve_fit(promath.linearFunc, QDC_loc, E)
    # pcov = np.diag(np.sqrt(pcov))

    # print(f'RESULTS YAP{tofCh}: {popt}')
    # plt.scatter(QDC_loc, E)
    # plt.plot(np.arange(-500,20000,100), promath.linearFunc(np.arange(-500,20000,100), popt[0], popt[1]))
    # plt.show()

    np.save(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/YAP/Ecal/ch{tofCh}/popt', popt)
    np.save(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/YAP/Ecal/ch{tofCh}/pcov', pcov)