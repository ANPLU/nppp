#!/usr/bin/env python3
from re import I
import sys
from tkinter import X
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
from numpy.core.arrayprint import SubArrayFormat
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit
from scipy import interpolate

# from matplotlib.colors import LinearSegmentedColormap
"""
The purpose of this script is tro try and find the optimal functional format for EJ321P light yield
"""

# IMPORT FROM LIBRARY DIRECTORY
sys.path.insert(0, "../library/")
sys.path.insert(0, "../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "../")
sys.path.insert(0, "data_analysis/")

import processing_math as promath
import processing_data as prodata
import processing_pd as propd
import processing_simulation as prosim
import figure_style as fs

def cecilEq(Ep, p1, p2, p3, p4):
    """
    Used to convert proton recoil energy (Ep) to electron recoil energy (Ee).
    Returns electron recoil energy Ee
    """
    return p1*Ep-p2*(1-np.exp(-p3*np.power(Ep,p4)))

def cecilEqEJ321P(Ep, K):
    """
    Used to convert proton recoil energy (Ep) to electron recoil energy (Ee).
    Returns electron recoil energy Ee
    """
    p1 = 0.44 #values derived from averageing HH, TP and FD fits for EJ321P
    p2 = 0.75
    p3 = 0.38
    p4 = 2.71

    return K*(p1*Ep-p2*(1-np.exp(-p3*np.power(Ep,p4))))

def kornilovEq(Ep, L0, L1):
    """
    Energy calibration equation for EJ321P
    Fitting parameter L1 determined for each detector using average if all fitted methods
    """

    return L0*Ep**2/(Ep+L1)

def kornilovEq_EJ321P(Ep, L0):
    """
    Energy calibration equation for EJ321P
    Fitting parameter L1 determined for each detector using average if all fitted methods
    """
    L1 = 6.50#6.617600503137628 # +/- 1.1217571647413835 Dervied from average of all L1 from fitting kornilovEq(Ep, L0, L1)
    
    return L0*Ep**2/(Ep+L1)

def func1(Ep, L0, L1):
    """
    Energy calibration equation for EJ321P
    Fitting parameter L1 determined for each detector using average if all fitted methods
    """
    #Chi2 = 3.2, 9.3, 9.8, 10.6
    return L0*Ep**2/(Ep**2+L1)

def func2(Ep, L0, L1):
    """
    Energy calibration equation for EJ321P
    Fitting parameter L1 determined for each detector using average if all fitted methods
    """
    #Chi2 = 3.7, 9.8, 4.5, 0.75
    return L0*Ep**2/(Ep+L1*Ep**2)

def func3(Ep, A, B):
    """
    Energy calibration equation for EJ321P
    Fitting parameter L1 determined for each detector using average if all fitted methods
    """
    #Chi2 = 3.7, 9.8, 4.5, 0.75
    return A*Ep**2 + B*Ep

def func4(Ep, A, B, C):
    """
    Energy calibration equation for EJ321P
    Fitting parameter L1 determined for each detector using average if all fitted methods
    """
    #Chi2 = 3.7, 9.8, 4.5, 0.75
    return A*Ep**2 + B*Ep + C

def polyEJ321P(Ep, S):
    """
    Energy calibration equation for EJ321P
    Fitting parameter L1 determined for each detector using average if all fitted methods
    """
    #Chi2 = 3.7, 9.8, 4.5, 0.75
    A = 0.03463543958969215 #+/- 0.001824665639541493 (5%)
    B = 0.10759911522298186 #+/- 0.008767418704202378 (8%)
    return S*(A*Ep**2 + B*Ep)

currentFitFunc = func3
detector = 'EJ321P'
gate = 'LG'
pathData = '/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper'

HH_data = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/HH_TP_FD/HH_data_{gate}.pkl')
TP_data = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/HH_TP_FD/TP_data_{gate}.pkl')
FD_data = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/HH_TP_FD/FD_data_{gate}.pkl')

energy = HH_data.energy

LY_sim = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/simulation/sim_LightYield.pkl')

SMD_loc = LY_sim.means
SMD_loc_err = LY_sim.means_err

HH_loc = HH_data.HH_loc
HH_loc_err = HH_data.HH_loc_err

TP_loc = TP_data.TP_loc
TP_loc_err = TP_data.TP_loc_err

FD_loc = FD_data.FD_loc
FD_loc_err = FD_data.FD_loc_err

popt_SMD, pcov_SMD = curve_fit(currentFitFunc, energy, SMD_loc)
pcov_SMD = np.diag(np.sqrt(pcov_SMD))



popt_HH, pcov_HH = curve_fit(currentFitFunc, energy, HH_loc)
pcov_HH = np.diag(np.sqrt(pcov_HH))

popt_TP, pcov_TP = curve_fit(currentFitFunc, energy, TP_loc)
pcov_TP = np.diag(np.sqrt(pcov_TP))

popt_FD, pcov_FD = curve_fit(currentFitFunc, energy, FD_loc)
pcov_FD = np.diag(np.sqrt(pcov_FD))

#calculate Cchi2/dof
redChi2_HH = promath.chi2red(HH_loc, currentFitFunc(energy, popt_HH[0], popt_HH[1]), HH_loc_err, 2)
redChi2_TP = promath.chi2red(TP_loc, currentFitFunc(energy, popt_TP[0], popt_TP[1]), TP_loc_err, 2)
redChi2_FD = promath.chi2red(FD_loc, currentFitFunc(energy, popt_FD[0], popt_FD[1]), FD_loc_err, 2)    
redChi2_SMD = promath.chi2red(SMD_loc, currentFitFunc(energy, popt_SMD[0], popt_SMD[1]), SMD_loc_err, 2)
# redChi2_HH = promath.chi2red(HH_loc, currentFitFunc(energy, popt_HH[0], popt_HH[1], popt_HH[2]), HH_loc_err, 3)
# redChi2_TP = promath.chi2red(TP_loc, currentFitFunc(energy, popt_TP[0], popt_TP[1], popt_TP[2]), TP_loc_err, 3)
# redChi2_FD = promath.chi2red(FD_loc, currentFitFunc(energy, popt_FD[0], popt_FD[1], popt_FD[2]), FD_loc_err, 3)    
# redChi2_SMD = promath.chi2red(SMD_loc, currentFitFunc(energy, popt_SMD[0], popt_SMD[1], popt_SMD[2]), SMD_loc_err, 3)

p1_avg =         np.average([popt_HH[0], popt_TP[0], popt_FD[0]])
p1_avg_err  =    np.average([pcov_HH[0], pcov_TP[0], pcov_FD[0]])
p2_avg =         np.average([popt_HH[1], popt_TP[1], popt_FD[1]])
p2_avg_err  =    np.average([pcov_HH[1], pcov_TP[1], pcov_FD[1]])
# p3_avg =         np.average([popt_HH[2], popt_TP[2], popt_FD[2]])
# p3_avg_err  =    np.average([pcov_HH[2], pcov_TP[2], pcov_FD[2]])
# p4_avg =         np.average([popt_HH[3], popt_TP[3], popt_FD[3]])
# p4_avg_err  =    np.average([pcov_HH[3], pcov_TP[3], pcov_FD[3]])
    
#fit SMD with new average p* paramters for cecil:
# redChi2_SMD = promath.chi2red(SMD_loc, cecilEqNE213A(energy, SMD_popt[0]), SMD_loc_err, 1)

print('-----------------------------------------------------------------------')    
print(f'Scintillator: {detector}, func = {currentFitFunc}')
print(f'SMD....p = {np.round(popt_SMD, 2)}+/-{np.round(pcov_SMD, 2)}, Chi2/dof = {np.round(redChi2_SMD, 2)}')
print(f'HH.....p = {np.round(popt_HH, 2)}+/-{np.round(pcov_HH, 2)}, Chi2/dof = {np.round(redChi2_HH, 2)}')
print(f'TP.....p = {np.round(popt_TP, 2)}+/-{np.round(pcov_TP, 2)}, Chi2/dof = {np.round(redChi2_TP, 2)}')
print(f'FD.....p = {np.round(popt_FD, 2)}+/-{np.round(pcov_FD, 2)}, Chi2/dof = {np.round(redChi2_FD, 2)}')

print('-----------------------------------------------------------------------')

# print(f'Avg.....p1 = {np.round(popt_HH, 2)}+/-{np.round(pcov_HH,2)}')

plt.suptitle(detector)
plt.subplot(4,1,1)
plt.scatter(energy, HH_loc, label='HH',color='blue', s=20)
plt.errorbar(energy, HH_loc, yerr=HH_loc_err, ls='none',color='blue', lw=0.5)
# plt.plot(energy, currentFitFunc(energy, popt_HH[0], popt_HH[1], popt_HH[2]),color='blue', lw=0.5)
plt.plot(energy, currentFitFunc(energy, popt_HH[0], popt_HH[1]),color='blue', lw=0.5)
plt.legend()

plt.subplot(4,1,2)
plt.scatter(energy, TP_loc, label='TP',color='red', s=20)
plt.errorbar(energy, TP_loc, yerr=TP_loc_err, ls='none',color='red', lw=0.5)
# plt.plot(energy, currentFitFunc(energy, popt_TP[0], popt_TP[1], popt_TP[2]),color='red', lw=0.5)
plt.plot(energy, currentFitFunc(energy, popt_TP[0], popt_TP[1]),color='red', lw=0.5)
plt.legend()

plt.subplot(4,1,3)
plt.scatter(energy, FD_loc, label='FD',color='green', s=20)
plt.errorbar(energy, FD_loc, yerr=FD_loc_err, ls='none',color='green', lw=0.5)
# plt.plot(energy, currentFitFunc(energy, popt_FD[0], popt_FD[1], popt_FD[2]),color='green', lw=0.5)
plt.plot(energy, currentFitFunc(energy, popt_FD[0], popt_FD[1]),color='green', lw=0.5)
plt.legend()

plt.subplot(4,1,4)
plt.scatter(energy, SMD_loc, marker='v', s=30, color='black', label= 'SMD')
plt.errorbar(energy, SMD_loc, yerr=SMD_loc_err, ls='none',color='green', lw=0.5)
# plt.plot(energy, currentFitFunc(energy, popt_SMD[0], popt_SMD[1], popt_SMD[2]),color='green', lw=0.5)
plt.plot(energy, currentFitFunc(energy, popt_SMD[0], popt_SMD[1]),color='green', lw=0.5)
plt.legend()

plt.show()










##### COMPARE FUNCTION #####

