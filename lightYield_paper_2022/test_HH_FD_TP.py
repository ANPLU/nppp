#!/usr/bin/env python3
import sys
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit

# IMPORT FROM LIBRARY DIRECTORY
sys.path.insert(0, "../library/")
sys.path.insert(0, "../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "sim_analysis/")
sys.path.insert(0, "data_analysis/")

import digidata
import nicholai_plot_helper as nplt
import processing_utility as prout
import processing_math as promath
import processing_data as prodata
import processing_pd as propd
import processing_simulation as prosim
import figure_style as fs

"""
Script for comparing HH, FD and TP between all detectors
"""

detector = 'NE213A'
NE213A_HH_data = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/HH_data_LG.pkl')
NE213A_TP_data = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/TP_data_LG.pkl')
NE213A_FD_data = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/FD_data_LG.pkl')

NE213A_HH_sim = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/HH_sim.pkl')
NE213A_TP_sim = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/TP_sim.pkl')
NE213A_FD_sim = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/FD_sim.pkl')


detector = 'EJ305'
EJ305_HH_data = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/HH_data_LG.pkl')
EJ305_TP_data = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/TP_data_LG.pkl')
EJ305_FD_data = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/FD_data_LG.pkl')

EJ305_HH_sim = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/HH_sim.pkl')
EJ305_TP_sim = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/TP_sim.pkl')
EJ305_FD_sim = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/FD_sim.pkl')


detector = 'EJ331'
EJ331_HH_data = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/HH_data_LG.pkl')
EJ331_TP_data = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/TP_data_LG.pkl')
EJ331_FD_data = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/FD_data_LG.pkl')

EJ331_HH_sim = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/HH_sim.pkl')
EJ331_TP_sim = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/TP_sim.pkl')
EJ331_FD_sim = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/FD_sim.pkl')


detector = 'EJ321P'
EJ321P_HH_data = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/HH_data_LG.pkl')
EJ321P_TP_data = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/TP_data_LG.pkl')
EJ321P_FD_data = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/FD_data_LG.pkl')

EJ321P_HH_sim = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/HH_sim.pkl')
EJ321P_TP_sim = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/TP_sim.pkl')
EJ321P_FD_sim = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/FD_sim.pkl')



fs.set_style(textwidth=345*2.5)

plt.figure()
plt.title('HH')
plt.scatter(NE213A_HH_data.energy[2:-1], NE213A_HH_data.HH_loc[2:-1], label='NE213A', color='royalblue', marker='o')
plt.scatter(EJ305_HH_data.energy[2:-1], EJ305_HH_data.HH_loc[2:-1], label='EJ305', color='orange', marker='o')
plt.scatter(EJ331_HH_data.energy[2:-1], EJ331_HH_data.HH_loc[2:-1], label='EJ331', color='tomato', marker='o')
plt.scatter(EJ321P_HH_data.energy[2:-1], EJ321P_HH_data.HH_loc[2:-1], label='EJ321P', color='forestgreen', marker='o')

plt.scatter(NE213A_HH_sim.energy[4:-4], NE213A_HH_sim.HH_loc[4:-4], color='royalblue', marker='v',  alpha=0.5)
plt.scatter(EJ305_HH_sim.energy[4:-4], EJ305_HH_sim.HH_loc[4:-4], color='orange', marker='v', alpha=0.5)
plt.scatter(EJ331_HH_sim.energy[4:-4], EJ331_HH_sim.HH_loc[4:-4], color='tomato', marker='v', alpha=0.5)
plt.scatter(EJ321P_HH_sim.energy[4:-4], EJ321P_HH_sim.HH_loc[4:-4], color='forestgreen', marker='v', alpha=0.5)

plt.legend()
plt.xlabel('Neutron Energy [MeV]')
plt.ylabel('Light Yield [MeV$_{ee}$]')

plt.figure()
plt.title('TP')
plt.scatter(NE213A_TP_data.energy[2:-1], NE213A_TP_data.TP_loc[2:-1], label='NE213A', color='royalblue', marker='o')
plt.scatter(EJ305_TP_data.energy[2:-1], EJ305_TP_data.TP_loc[2:-1], label='EJ305', color='orange', marker='o')
plt.scatter(EJ331_TP_data.energy[2:-1], EJ331_TP_data.TP_loc[2:-1], label='EJ331', color='tomato', marker='o')
plt.scatter(EJ321P_TP_data.energy[2:-1], EJ321P_TP_data.TP_loc[2:-1], label='EJ321P', color='forestgreen', marker='o')

plt.scatter(NE213A_TP_sim.energy[4:-4], NE213A_TP_sim.TP_loc[4:-4], color='royalblue', marker='v',  alpha=0.5)
plt.scatter(EJ305_TP_sim.energy[4:-4], EJ305_TP_sim.TP_loc[4:-4], color='orange', marker='v', alpha=0.5)
plt.scatter(EJ331_TP_sim.energy[4:-4], EJ331_TP_sim.TP_loc[4:-4], color='tomato', marker='v', alpha=0.5)
plt.scatter(EJ321P_TP_sim.energy[4:-4], EJ321P_TP_sim.TP_loc[4:-4], color='forestgreen', marker='v', alpha=0.5)
plt.legend()
plt.xlabel('Neutron Energy [MeV]')
plt.ylabel('Light Yield [MeV$_{ee}$]')



plt.figure()
plt.title('FD')
plt.scatter(NE213A_FD_data.energy[2:-1], NE213A_FD_data.FD_loc[2:-1], label='NE213A', color='royalblue', marker='o')
plt.scatter(EJ305_FD_data.energy[2:-1], EJ305_FD_data.FD_loc[2:-1], label='EJ305', color='orange', marker='o')
plt.scatter(EJ331_FD_data.energy[2:-1], EJ331_FD_data.FD_loc[2:-1], label='EJ331', color='tomato', marker='o')
plt.scatter(EJ321P_FD_data.energy[2:-1], EJ321P_FD_data.FD_loc[2:-1], label='EJ321P', color='forestgreen', marker='o')

plt.scatter(NE213A_FD_sim.energy[4:-4], NE213A_FD_sim.FD_loc[4:-4], color='royalblue', marker='v', alpha=0.5)
plt.scatter(EJ305_FD_sim.energy[4:-4], EJ305_FD_sim.FD_loc[4:-4], color='orange', marker='v', alpha=0.5)
plt.scatter(EJ331_FD_sim.energy[4:-4], EJ331_FD_sim.FD_loc[4:-4], color='tomato', marker='v', alpha=0.5)
plt.scatter(EJ321P_FD_sim.energy[4:-4], EJ321P_FD_sim.FD_loc[4:-4], color='forestgreen', marker='v', alpha=0.5)
plt.legend()
plt.xlabel('Neutron Energy [MeV]')
plt.ylabel('Light Yield [MeV$_{ee}$]')
plt.show()




