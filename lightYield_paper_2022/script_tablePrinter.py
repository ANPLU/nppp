#!/usr/bin/env python3
import sys
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit

# IMPORT FROM LIBRARY DIRECTORY
sys.path.insert(0, "../library/")
sys.path.insert(0, "../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "sim_analysis/")
sys.path.insert(0, "data_analysis/")

import digidata
import nicholai_plot_helper as nplt
import processing_utility as prout
import processing_math as promath
import processing_data as prodata
import processing_pd as propd
import processing_simulation as prosim
import figure_style as fs

"""
Script for making LATEX tables
"""

detectors = ['NE213A', 'EJ305', 'EJ331', 'EJ321P']
for detector in detectors:
    kornilov_LG =   pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/kornilov_LG_result.pkl')
    kornilov_SMD =  pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/simulation/kornilov_result.pkl')
    cecil_LG =      pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/cecil_LG_result.pkl')
    cecil_SMD =     pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/simulation/cecil_result.pkl')
    # ~~~~~~~~~~~~~~~~~~~~
    #Kornilov 
    # ~~~~~~~~~~~~~~~~~~~~

    #SMD method
    #Kornilov SMD
    L0_SMD =            round(kornilov_SMD.L0.item(), 2)
    L0_err_SMD =        round(kornilov_SMD.L0_err.item(), 2)
    kornilov_chi2_SMD = round(kornilov_SMD.redchi2.item(), 1)
    
    currentMethod= "HH_data"
    #Kornilov HH LG
    L0_HH_LG =            round(kornilov_LG.query("method == @currentMethod").L0.item(), 2)
    L0_err_HH_LG =        round(kornilov_LG.query("method == @currentMethod").L0_err.item(), 2)
    kornilov_chi2_HH_LG =  round(kornilov_LG.query("method == @currentMethod").redchi2.item(), 1)
    
    currentMethod= "TP_data"
    #Kornilov TP LG
    L0_TP_LG =            round(kornilov_LG.query("method == @currentMethod").L0.item(), 2)
    L0_err_TP_LG =        round(kornilov_LG.query("method == @currentMethod").L0_err.item(), 2)
    kornilov_chi2_TP_LG =  round(kornilov_LG.query("method == @currentMethod").redchi2.item(), 1)
    
    currentMethod= "FD_data"
    #Kornilov FD LG
    L0_FD_LG =            round(kornilov_LG.query("method == @currentMethod").L0.item(), 2)
    L0_err_FD_LG =        round(kornilov_LG.query("method == @currentMethod").L0_err.item(), 2)
    kornilov_chi2_FD_LG =  round(kornilov_LG.query("method == @currentMethod").redchi2.item(), 1)

    # ~~~~~~~~~~~~~~~~~~~~
    #Cecil
    # ~~~~~~~~~~~~~~~~~~~~~~

    #Cecil SMD
    C_SMD =            round(cecil_SMD.C.item(), 2)
    C_err_SMD =        round(cecil_SMD.C_err.item(), 2)
    cecil_chi2_SMD =   round(cecil_SMD.redchi2.item(), 1)
    
    currentMethod= "HH_data"
    #Cecil HH LG
    C_HH_LG =             round(cecil_LG.query("method == @currentMethod").C.item(), 2)
    C_err_HH_LG =         round(cecil_LG.query("method == @currentMethod").C_err.item(), 2)
    cecil_chi2_HH_LG =    round(cecil_LG.query("method == @currentMethod").redchi2.item(), 1)

    currentMethod= "TP_data"
    #Cecil TP LG
    C_TP_LG =             round(cecil_LG.query("method == @currentMethod").C.item(), 2)
    C_err_TP_LG =         round(cecil_LG.query("method == @currentMethod").C_err.item(), 2)
    cecil_chi2_TP_LG =    round(cecil_LG.query("method == @currentMethod").redchi2.item(), 1)

    currentMethod= "FD_data"
    #Cecil FD LG
    C_FD_LG =             round(cecil_LG.query("method == @currentMethod").C.item(), 2)
    C_err_FD_LG =         round(cecil_LG.query("method == @currentMethod").C_err.item(), 2)
    cecil_chi2_FD_LG =    round(cecil_LG.query("method == @currentMethod").redchi2.item(), 1)


    print(f'{detector} & HH & {C_HH_LG} $\pm$ {C_err_HH_LG} & {cecil_chi2_HH_LG} & {L0_HH_LG} $\pm$ {L0_err_HH_LG} & {kornilov_chi2_HH_LG}')
    print(           f'& TP & {C_TP_LG} $\pm$ {C_err_TP_LG} & {cecil_chi2_TP_LG} & {L0_TP_LG} $\pm$ {L0_err_TP_LG} & {kornilov_chi2_TP_LG}')
    print(           f'& FD & {C_FD_LG} $\pm$ {C_err_FD_LG} & {cecil_chi2_FD_LG} & {L0_FD_LG} $\pm$ {L0_err_FD_LG} & {kornilov_chi2_FD_LG}')
    print(           f'& SMD & {C_SMD} $\pm$ {C_err_SMD} & {cecil_chi2_SMD} & {L0_SMD} $\pm$ {L0_err_SMD} & {kornilov_chi2_SMD}')





# print('-----------------------------------------------')

# detector = 'EJ321P'
# kornilov_LG =   pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/kornilov_LG_result.pkl')
# kornilov_SMD =  pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/simulation/kornilov_result.pkl')
# cecil_LG =      pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/cecil_LG_result.pkl')
# cecil_SMD =     pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/simulation/cecil_result.pkl')
# poly2_LG =      pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/poly2_LG_result.pkl')
# poly2_SMD =     pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/simulation/poly2_result.pkl')

# # ~~~~~~~~~~~~~~~~~~~~
# #Kornilov 
# # ~~~~~~~~~~~~~~~~~~~~

# #SMD method
# #Kornilov SMD
# L0_SMD =            round(kornilov_SMD.L0.item(), 2)
# L0_err_SMD =        round(kornilov_SMD.L0_err.item(), 2)
# kornilov_chi2_SMD = round(kornilov_SMD.redchi2.item(), 2)

# currentMethod= "HH_data"
# #Kornilov HH LG
# L0_HH_LG =            round(kornilov_LG.query("method == @currentMethod").L0.item(), 2)
# L0_err_HH_LG =        round(kornilov_LG.query("method == @currentMethod").L0_err.item(), 2)
# kornilov_chi2_HH_LG =  round(kornilov_LG.query("method == @currentMethod").redchi2.item(), 2)

# currentMethod= "TP_data"
# #Kornilov TP LG
# L0_TP_LG =            round(kornilov_LG.query("method == @currentMethod").L0.item(), 2)
# L0_err_TP_LG =        round(kornilov_LG.query("method == @currentMethod").L0_err.item(), 2)
# kornilov_chi2_TP_LG =  round(kornilov_LG.query("method == @currentMethod").redchi2.item(), 2)

# currentMethod= "FD_data"
# #Kornilov FD LG
# L0_FD_LG =            round(kornilov_LG.query("method == @currentMethod").L0.item(), 2)
# L0_err_FD_LG =        round(kornilov_LG.query("method == @currentMethod").L0_err.item(), 2)
# kornilov_chi2_FD_LG =  round(kornilov_LG.query("method == @currentMethod").redchi2.item(), 2)

# # ~~~~~~~~~~~~~~~~~~~~
# # Cecil
# # ~~~~~~~~~~~~~~~~~~~~~~

# #Cecil SMD
# C_SMD =            round(cecil_SMD.C.item(), 2)
# C_err_SMD =        round(cecil_SMD.C_err.item(), 2)
# cecil_chi2_SMD =   round(cecil_SMD.redchi2.item(), 2)

# currentMethod= "HH_data"
# #Cecil HH LG
# C_HH_LG =             round(cecil_LG.query("method == @currentMethod").C.item(), 2)
# C_err_HH_LG =         round(cecil_LG.query("method == @currentMethod").C_err.item(), 2)
# cecil_chi2_HH_LG =    round(cecil_LG.query("method == @currentMethod").redchi2.item(), 2)

# currentMethod= "TP_data"
# #Cecil TP LG
# C_TP_LG =             round(cecil_LG.query("method == @currentMethod").C.item(), 2)
# C_err_TP_LG =         round(cecil_LG.query("method == @currentMethod").C_err.item(), 2)
# cecil_chi2_TP_LG =    round(cecil_LG.query("method == @currentMethod").redchi2.item(), 2)

# currentMethod= "FD_data"
# #Cecil FD LG
# C_FD_LG =             round(cecil_LG.query("method == @currentMethod").C.item(), 2)
# C_err_FD_LG =         round(cecil_LG.query("method == @currentMethod").C_err.item(), 2)
# cecil_chi2_FD_LG =    round(cecil_LG.query("method == @currentMethod").redchi2.item(), 2)


# # ~~~~~~~~~~~~~~~~~~~~
# # Poly2
# # ~~~~~~~~~~~~~~~~~~~~~~
# #SMD
# N_SMD =            round(poly2_SMD.N.item(), 2)
# N_err_SMD =        round(poly2_SMD.N_err.item(), 2)
# poly2_chi2_SMD =   round(poly2_SMD.redchi2.item(), 2)

# currentMethod= "HH_data"
# #HH LG
# N_HH_LG =             round(poly2_LG.query("method == @currentMethod").N.item(), 2)
# N_err_HH_LG =         round(poly2_LG.query("method == @currentMethod").N_err.item(), 2)
# poly2_chi2_HH_LG =    round(poly2_LG.query("method == @currentMethod").redchi2.item(), 2)

# currentMethod= "TP_data"
# # TP LG
# N_TP_LG =             round(poly2_LG.query("method == @currentMethod").N.item(), 2)
# N_err_TP_LG =         round(poly2_LG.query("method == @currentMethod").N_err.item(), 2)
# poly2_chi2_TP_LG =    round(poly2_LG.query("method == @currentMethod").redchi2.item(), 2)

# currentMethod= "FD_data"
# # FD LG
# N_FD_LG =             round(poly2_LG.query("method == @currentMethod").N.item(), 2)
# N_err_FD_LG =         round(poly2_LG.query("method == @currentMethod").N_err.item(), 2)
# poly2_chi2_FD_LG =    round(poly2_LG.query("method == @currentMethod").redchi2.item(), 2)


# print(f'{detector} & SMD & {C_SMD} $\pm$ {C_err_SMD} & {cecil_chi2_SMD} &{L0_SMD} $\pm$ {L0_err_SMD} & {kornilov_chi2_SMD} & {N_SMD} $\pm$ {N_err_SMD} & {poly2_chi2_SMD}')
# print(           f'& HH & {C_HH_LG} $\pm$ {C_err_HH_LG} & {cecil_chi2_HH_LG} & {L0_HH_LG} $\pm$ {L0_err_HH_LG} & {kornilov_chi2_HH_LG} & {N_HH_LG} $\pm$ {N_err_HH_LG} & {poly2_chi2_HH_LG}')
# print(           f'& TP & {C_TP_LG} $\pm$ {C_err_TP_LG} & {cecil_chi2_TP_LG} & {L0_TP_LG} $\pm$ {L0_err_TP_LG} & {kornilov_chi2_TP_LG} & {N_TP_LG} $\pm$ {N_err_TP_LG} & {poly2_chi2_TP_LG}')
# print(           f'& FD & {C_FD_LG} $\pm$ {C_err_FD_LG} & {cecil_chi2_FD_LG} & {L0_FD_LG} $\pm$ {L0_err_FD_LG} & {kornilov_chi2_FD_LG} & {N_FD_LG} $\pm$ {N_err_FD_LG} & {poly2_chi2_FD_LG}')
