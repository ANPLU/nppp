#!/usr/bin/env python3
import sys
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit

# IMPORT FROM LIBRARY DIRECTORY
sys.path.insert(0, "../library/")
sys.path.insert(0, "../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "sim_analysis/")
sys.path.insert(0, "data_analysis/")

import digidata
import nicholai_plot_helper as nplt
import processing_utility as prout
import processing_math as promath
import processing_data as prodata
import processing_pd as propd
import processing_simulation as prosim

"""
calibration comparison
"""






##################################################
E = 2000
##################################################

############
# NE213A
############
detector = 'NE213A'
nQDC       = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nQDC.npy')
randQDC    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randQDC.npy')
nLength    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nLength.npy')
randLength = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randLength.npy')
# nQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', nQDC)
# randQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', randQDC)
x_neutron_QDC, y_neutron_QDC = prodata.randomTOFSubtractionQDC( nQDC,
                                                                randQDC,
                                                                nLength, 
                                                                randLength,
                                                                np.arange(0,40000,500), 
                                                                plot=False)
plt.figure(0)
plt.step(x_neutron_QDC, y_neutron_QDC, label=detector)

############
# EJ305
############
detector = 'EJ305'
nQDC       = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nQDC.npy')
randQDC    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randQDC.npy')
nLength    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nLength.npy')
randLength = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randLength.npy')
# nQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', nQDC)
# randQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', randQDC)
x_neutron_QDC, y_neutron_QDC = prodata.randomTOFSubtractionQDC( nQDC,
                                                                randQDC,
                                                                nLength, 
                                                                randLength,
                                                                np.arange(0,40000,500), 
                                                                plot=False)
plt.figure(0)
plt.step(x_neutron_QDC, y_neutron_QDC, label=detector)


############
# EJ331
############
detector = 'EJ331'
nQDC       = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nQDC.npy')
randQDC    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randQDC.npy')
nLength    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nLength.npy')
randLength = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randLength.npy')
# nQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', nQDC)
# randQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', randQDC)
x_neutron_QDC, y_neutron_QDC = prodata.randomTOFSubtractionQDC( nQDC,
                                                                randQDC,
                                                                nLength, 
                                                                randLength,
                                                                np.arange(0,40000,500), 
                                                                plot=False)
plt.figure(0)
plt.step(x_neutron_QDC, y_neutron_QDC, label=detector)

############
# EJ321P
############
detector = 'EJ321P'
nQDC       = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nQDC.npy')
randQDC    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randQDC.npy')
nLength    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nLength.npy')
randLength = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randLength.npy')
# nQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', nQDC)
# randQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', randQDC)
x_neutron_QDC, y_neutron_QDC = prodata.randomTOFSubtractionQDC( nQDC,
                                                                randQDC,
                                                                nLength, 
                                                                randLength,
                                                                np.arange(0,40000,500), 
                                                                plot=False)
plt.figure(0)
plt.step(x_neutron_QDC, y_neutron_QDC, label=detector)
plt.legend()
plt.title(f'Tn = {E} keV')
plt.xlabel('QDC [arb. units]')
plt.ylabel('counts')




##################################################
E = 5000
##################################################

############
# NE213A
############
detector = 'NE213A'
nQDC       = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nQDC.npy')
randQDC    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randQDC.npy')
nLength    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nLength.npy')
randLength = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randLength.npy')
# nQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', nQDC)
# randQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', randQDC)
x_neutron_QDC, y_neutron_QDC = prodata.randomTOFSubtractionQDC( nQDC,
                                                                randQDC,
                                                                nLength, 
                                                                randLength,
                                                                np.arange(0,100000,800), 
                                                                plot=False)
plt.figure(1)
plt.step(x_neutron_QDC, y_neutron_QDC, label=detector)

############
# EJ305
############
detector = 'EJ305'
nQDC       = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nQDC.npy')
randQDC    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randQDC.npy')
nLength    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nLength.npy')
randLength = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randLength.npy')
# nQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', nQDC)
# randQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', randQDC)
x_neutron_QDC, y_neutron_QDC = prodata.randomTOFSubtractionQDC( nQDC,
                                                                randQDC,
                                                                nLength, 
                                                                randLength,
                                                                np.arange(0,100000,800), 
                                                                plot=False)
plt.figure(1)
plt.step(x_neutron_QDC, y_neutron_QDC, label=detector)


############
# EJ331
############
detector = 'EJ331'
nQDC       = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nQDC.npy')
randQDC    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randQDC.npy')
nLength    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nLength.npy')
randLength = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randLength.npy')
# nQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', nQDC)
# randQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', randQDC)
x_neutron_QDC, y_neutron_QDC = prodata.randomTOFSubtractionQDC( nQDC,
                                                                randQDC,
                                                                nLength, 
                                                                randLength,
                                                                np.arange(0,100000,800), 
                                                                plot=False)
plt.figure(1)
plt.step(x_neutron_QDC, y_neutron_QDC, label=detector)

############
# EJ321P
############
detector = 'EJ321P'
nQDC       = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nQDC.npy')
randQDC    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randQDC.npy')
nLength    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nLength.npy')
randLength = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randLength.npy')
# nQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', nQDC)
# randQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', randQDC)
x_neutron_QDC, y_neutron_QDC = prodata.randomTOFSubtractionQDC( nQDC,
                                                                randQDC,
                                                                nLength, 
                                                                randLength,
                                                                np.arange(0,100000,800), 
                                                                plot=False)
plt.figure(1)
plt.step(x_neutron_QDC, y_neutron_QDC, label=detector)
plt.legend()
plt.title(f'Tn = {E} keV')
plt.xlabel('QDC [arb. units]')
plt.ylabel('counts')




plt.show()







#CALIBRATED COMPARISONS
##################################################
E = 2000
##################################################

############
# NE213A
############
detector = 'NE213A'
nQDC       = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nQDC.npy')
randQDC    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randQDC.npy')
nLength    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nLength.npy')
randLength = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randLength.npy')
nQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', nQDC)
randQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', randQDC)
x_neutron_QDC, y_neutron_QDC = prodata.randomTOFSubtractionQDC( nQDC,
                                                                randQDC,
                                                                nLength, 
                                                                randLength,
                                                                np.arange(0,4,0.0500), 
                                                                plot=False)
plt.figure(0)
plt.step(x_neutron_QDC, y_neutron_QDC, label=detector)

############
# EJ305
############
detector = 'EJ305'
nQDC       = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nQDC.npy')
randQDC    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randQDC.npy')
nLength    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nLength.npy')
randLength = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randLength.npy')
nQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', nQDC)
randQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', randQDC)
x_neutron_QDC, y_neutron_QDC = prodata.randomTOFSubtractionQDC( nQDC,
                                                                randQDC,
                                                                nLength, 
                                                                randLength,
                                                                np.arange(0,4,0.0500), 
                                                                plot=False)
plt.figure(0)
plt.step(x_neutron_QDC, y_neutron_QDC, label=detector)


############
# EJ331
############
detector = 'EJ331'
nQDC       = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nQDC.npy')
randQDC    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randQDC.npy')
nLength    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nLength.npy')
randLength = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randLength.npy')
nQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', nQDC)
randQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', randQDC)
x_neutron_QDC, y_neutron_QDC = prodata.randomTOFSubtractionQDC( nQDC,
                                                                randQDC,
                                                                nLength, 
                                                                randLength,
                                                                np.arange(0,4,0.0500), 
                                                                plot=False)
plt.figure(0)
plt.step(x_neutron_QDC, y_neutron_QDC, label=detector)

############
# EJ321P
############
# detector = 'EJ321P'
# nQDC       = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nQDC.npy')
# randQDC    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randQDC.npy')
# nLength    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nLength.npy')
# randLength = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randLength.npy')
# nQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', nQDC)
# randQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', randQDC)
# x_neutron_QDC, y_neutron_QDC = prodata.randomTOFSubtractionQDC( nQDC,
#                                                                 randQDC,
#                                                                 nLength, 
#                                                                 randLength,
#                                                                 np.arange(0,4,0.0500), 
#                                                                 plot=False)
# plt.figure(0)
# plt.step(x_neutron_QDC, y_neutron_QDC, label=detector)
plt.legend()
plt.title(f'Tn = {E} keV')
plt.xlabel('Energy [MeVee]')
plt.ylabel('counts')




##################################################
E = 5000
##################################################

############
# NE213A
############
detector = 'NE213A'
nQDC       = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nQDC.npy')
randQDC    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randQDC.npy')
nLength    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nLength.npy')
randLength = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randLength.npy')
nQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', nQDC)
randQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', randQDC)
x_neutron_QDC, y_neutron_QDC = prodata.randomTOFSubtractionQDC( nQDC,
                                                                randQDC,
                                                                nLength, 
                                                                randLength,
                                                                np.arange(0,10,0.0800), 
                                                                plot=False)
plt.figure(1)
plt.step(x_neutron_QDC, y_neutron_QDC, label=detector)

############
# EJ305
############
detector = 'EJ305'
nQDC       = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nQDC.npy')
randQDC    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randQDC.npy')
nLength    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nLength.npy')
randLength = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randLength.npy')
nQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', nQDC)
randQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', randQDC)
x_neutron_QDC, y_neutron_QDC = prodata.randomTOFSubtractionQDC( nQDC,
                                                                randQDC,
                                                                nLength, 
                                                                randLength,
                                                                np.arange(0,10,0.0800), 
                                                                plot=False)
plt.figure(1)
plt.step(x_neutron_QDC, y_neutron_QDC, label=detector)


############
# EJ331
############
detector = 'EJ331'
nQDC       = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nQDC.npy')
randQDC    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randQDC.npy')
nLength    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nLength.npy')
randLength = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randLength.npy')
nQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', nQDC)
randQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', randQDC)
x_neutron_QDC, y_neutron_QDC = prodata.randomTOFSubtractionQDC( nQDC,
                                                                randQDC,
                                                                nLength, 
                                                                randLength,
                                                                np.arange(0,10,0.0800), 
                                                                plot=False)
plt.figure(1)
plt.step(x_neutron_QDC, y_neutron_QDC, label=detector)

############
# EJ321P
############
# detector = 'EJ321P'
# nQDC       = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nQDC.npy')
# randQDC    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randQDC.npy')
# nLength    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nLength.npy')
# randLength = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randLength.npy')
# nQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', nQDC)
# randQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', randQDC)
# x_neutron_QDC, y_neutron_QDC = prodata.randomTOFSubtractionQDC( nQDC,
#                                                                 randQDC,
#                                                                 nLength, 
#                                                                 randLength,
#                                                                 np.arange(0,10,0.0800), 
#                                                                 plot=False)
# plt.figure(1)
# plt.step(x_neutron_QDC, y_neutron_QDC, label=detector)
plt.legend()
plt.title(f'Tn = {E} keV')
plt.xlabel('Energy [MeVee]')
plt.ylabel('counts')




plt.show()





