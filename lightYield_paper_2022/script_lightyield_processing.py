#!/usr/bin/env python3
from re import I
import sys
from tkinter import X
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
from numpy.core.arrayprint import SubArrayFormat
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit
# from matplotlib.colors import LinearSegmentedColormap


# IMPORT FROM LIBRARY DIRECTORY
sys.path.insert(0, "../library/")
sys.path.insert(0, "../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "../")
sys.path.insert(0, "data_analysis/")

import processing_math as promath
import processing_data as prodata
import processing_pd as propd
import processing_simulation as prosim
import figure_style as fs




# ██████   █████  ████████  █████      ██████  ██████   ██████   ██████ ███████ ███████ ███████ ██ ███    ██  ██████  
# ██   ██ ██   ██    ██    ██   ██     ██   ██ ██   ██ ██    ██ ██      ██      ██      ██      ██ ████   ██ ██       
# ██   ██ ███████    ██    ███████     ██████  ██████  ██    ██ ██      █████   ███████ ███████ ██ ██ ██  ██ ██   ███ 
# ██   ██ ██   ██    ██    ██   ██     ██      ██   ██ ██    ██ ██      ██           ██      ██ ██ ██  ██ ██ ██    ██ 
# ██████  ██   ██    ██    ██   ██     ██      ██   ██  ██████   ██████ ███████ ███████ ███████ ██ ██   ████  ██████  
        

########################################
######## Get HH, TP and FD Data ########
########################################

pathData = '/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper'
pathSim =  '/media/gheed/Seagate_Expansion_Drive1/data/simulation/'

NE213AnumBinsDataLG = [np.arange(0.06, 4, 0.0200),#2000 keV
                np.arange(0.07, 4, 0.0225),#2250 keV
                np.arange(0.08, 4, 0.0250),#2500 keV
                np.arange(0.10, 4, 0.0275),#2750 keV
                np.arange(0.12, 4, 0.0300),#3000 keV
                np.arange(0.13, 4, 0.0325),#3250 keV
                np.arange(0.14, 4, 0.0350),#3500 keV
                np.arange(0.16, 4, 0.0375),#3750 keV
                np.arange(0.17, 4, 0.0400),#4000 keV
                np.arange(0.19, 4, 0.0425),#4250 keV
                np.arange(0.20, 4, 0.0450),#4500 keV
                np.arange(0.21, 4, 0.0475),#4750 keV
                np.arange(0.23, 4, 0.0500),#5000 keV
                np.arange(0.24, 4, 0.0525),#5250 keV
                np.arange(0.26, 4, 0.0550),#5500 keV
                np.arange(0.27, 4, 0.0575),#5750 keV
                np.arange(0.29, 4, 0.0600)]#6000 keV

NE213AfitRangeDataLG = [[0.14, 0.70], #2000
                [0.15, 0.80], #2250
                [0.16, 0.90], #2500
                [0.58, 1.50], #2750
                [0.65, 1.55], #3000
                [0.75, 1.80], #3250
                [0.94, 2.10], #3500
                [1.06, 2.20], #3750
                [1.15, 2.50], #4000
                [1.30, 2.60], #4250
                [1.35, 2.70], #4500
                [1.50, 3.00], #4750
                [1.65, 3.10], #5000
                [1.77, 3.20], #5250
                [1.96, 3.30], #5500
                [1.98, 3.50], #5750
                [2.20, 3.60]] #6000

EJ305numBinsDataLG = [np.arange(0.06, 4, 0.0200),#2000 keV
                np.arange(0.07, 4, 0.0225),#2250 keV
                np.arange(0.08, 4, 0.0250),#2500 keV
                np.arange(0.10, 4, 0.0275),#2750 keV
                np.arange(0.12, 4, 0.0300),#3000 keV
                np.arange(0.13, 4, 0.0325),#3250 keV
                np.arange(0.14, 4, 0.0350),#3500 keV
                np.arange(0.16, 4, 0.0375),#3750 keV
                np.arange(0.17, 4, 0.0400),#4000 keV
                np.arange(0.19, 4, 0.0425),#4250 keV
                np.arange(0.20, 4, 0.0450),#4500 keV
                np.arange(0.21, 4, 0.0475),#4750 keV
                np.arange(0.23, 4, 0.0500),#5000 keV
                np.arange(0.24, 4, 0.0525),#5250 keV
                np.arange(0.26, 4, 0.0550),#5500 keV
                np.arange(0.27, 4, 0.0575),#5750 keV
                np.arange(0.29, 4, 0.0600)]#6000 keV

EJ305fitRangeDataLG = [[0.22, 0.65], #2000
                        [0.18, 0.75], #2250
                        [0.30, 0.85], #2500
                        [0.35, 1.20], #2750
                        [0.33, 1.40], #3000
                        [0.44, 1.50], #3250
                        [0.46, 1.60], #3500
                        [0.59, 1.70], #3750
                        [0.65, 1.90], #4000
                        [0.74, 2.00], #4250
                        [0.90, 2.20], #4500
                        [0.95, 2.26], #4750
                        [1.07, 2.50], #5000
                        [1.15, 2.60], #5250
                        [1.26, 2.65], #5500
                        [1.36, 2.75], #5750
                        [1.46, 2.90]] #6000
        
EJ331numBinsDataLG = [np.arange(0.06, 4, 0.0200),#2000 keV
                np.arange(0.07, 4, 0.0225),#2250 keV
                np.arange(0.08, 4, 0.0250),#2500 keV
                np.arange(0.10, 4, 0.0275),#2750 keV
                np.arange(0.12, 4, 0.0300),#3000 keV
                np.arange(0.13, 4, 0.0325),#3250 keV
                np.arange(0.14, 4, 0.0350),#3500 keV
                np.arange(0.16, 4, 0.0375),#3750 keV
                np.arange(0.17, 4, 0.0400),#4000 keV
                np.arange(0.19, 4, 0.0425),#4250 keV
                np.arange(0.20, 4, 0.0450),#4500 keV
                np.arange(0.21, 4, 0.0475),#4750 keV
                np.arange(0.23, 4, 0.0500),#5000 keV
                np.arange(0.24, 4, 0.0525),#5250 keV
                np.arange(0.26, 4, 0.0550),#5500 keV
                np.arange(0.27, 4, 0.0575),#5750 keV
                np.arange(0.29, 4, 0.0600)]#6000 keV

EJ331fitRangeDataLG = [[0.22, 0.65], #2000
                [0.18, 0.75], #2250
                [0.30, 0.85], #2500
                [0.35, 1.20], #2750
                [0.33, 1.40], #3000
                [0.44, 1.50], #3250
                [0.46, 1.60], #3500
                [0.59, 1.70], #3750
                [0.65, 2.00], #4000
                [0.74, 2.10], #4250
                [0.84, 2.50], #4500
                [0.85, 2.70], #4750
                [1.03, 2.90], #5000
                [1.38, 3.00], #5250
                [1.40, 3.10], #5500
                [1.50, 3.20], #5750
                [1.60, 3.20]] #6000

EJ321PnumBinsDataLG =  [np.arange(0.0, 4, 0.0180),#2000 keV
                np.arange(0.0, 4, 0.0203),#2250 keV
                np.arange(0.0, 4, 0.0226),#2500 keV
                np.arange(0.0, 4, 0.0249),#2750 keV
                np.arange(0.0, 4, 0.0273),#3000 keV
                np.arange(0.0, 4, 0.0296),#3250 keV
                np.arange(0.0, 4, 0.0319),#3500 keV
                np.arange(0.0, 4, 0.0342),#3750 keV
                np.arange(0.0, 4, 0.0365),#4000 keV
                np.arange(0.0, 4, 0.0388),#4250 keV
                np.arange(0.0, 4, 0.0411),#4500 keV
                np.arange(0.0, 4, 0.0434),#4750 keV
                np.arange(0.0, 4, 0.0458),#5000 keV
                np.arange(0.0, 4, 0.0481),#5250 keV
                np.arange(0.0, 4, 0.0504),#5500 keV
                np.arange(0.0, 4, 0.0527),#5750 keV
                np.arange(0.0, 4, 0.0550)]#6000 keV

# EJ321PfitRangeDataLG = [[0.175, 0.70], #2000
#                 [0.20, 0.77], #2250
#                 [0.21, 0.85], #2500
#                 [0.25, 0.95], #2750
#                 [0.36, 1.00], #3000
#                 [0.39, 0.90], #3250
#                 [0.44, 1.30], #3500
#                 [0.49, 1.40], #3750
#                 [0.55, 1.55], #4000
#                 [0.64, 1.65], #4250
#                 [0.76, 1.80], #4500
#                 [0.84, 2.00], #4750
#                 [0.87, 2.10], #5000
#                 [1.03, 2.30], #5250
#                 [0.99, 2.35], #5500
#                 [0.98, 2.45], #5750
#                 [1.10, 2.50]] #6000

EJ321PfitRangeDataLG = [[0.17, 0.70], #2000
                [0.19, 0.77], #2250
                [0.21, 0.85], #2500
                [0.23, 0.95], #2750
                [0.24, 1.00], #3000
                [0.29, 1.10], #3250
                [0.30, 1.30], #3500
                [0.40, 1.40], #3750
                [0.41, 1.55], #4000
                [0.53, 1.70], #4250
                [0.66, 1.85], #4500
                [0.70, 2.00], #4750
                [0.77, 2.10], #5000
                [0.94, 2.30], #5250
                [0.98, 2.40], #5500
                [1.00, 2.50], #5750
                [1.10, 2.60]] #6000


def loadSysError(detector, gate):
       """
       Function will load systematic error values for specific detector and return as dictionary.
       """
       HH_sysErr = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/errors/sys_error/HH_sys_error_{gate}_5mm.npy')
       TP_sysErr = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/errors/sys_error/TP_sys_error_{gate}_5mm.npy')
       FD_sysErr = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/errors/sys_error/FD_sys_error_{gate}_5mm.npy')

       return {'HH_sysErr':HH_sysErr, 'TP_sysErr':TP_sysErr, 'FD_sysErr':FD_sysErr}

NE213ASysError = loadSysError('NE213A', 'LG')

EJ305SysError = loadSysError('EJ305', 'LG')

EJ321PSysError  = loadSysError('EJ321P', 'LG')

EJ331SysError = loadSysError('EJ331', 'LG')

# plt.subplot(4,1,1)
# plt.scatter(np.arange(2,6.25,0.25), NE213ASysError['HH_sysErr'], label='NE213A HH')
# plt.scatter(np.arange(2,6.25,0.25), NE213ASysError['TP_sysErr'], label='NE213A TP')
# plt.scatter(np.arange(2,6.25,0.25), NE213ASysError['FD_sysErr'], label='NE213A FD')
# plt.legend()
# plt.subplot(4,1,2)
# plt.scatter(np.arange(2,6.25,0.25), EJ305SysError['HH_sysErr'], label='EJ305 HH')
# plt.scatter(np.arange(2,6.25,0.25), EJ305SysError['TP_sysErr'], label='EJ305 TP')
# plt.scatter(np.arange(2,6.25,0.25), EJ305SysError['FD_sysErr'], label='EJ305 FD')
# plt.legend()
# plt.subplot(4,1,3)
# plt.scatter(np.arange(2,6.25,0.25), EJ321PSysError['HH_sysErr'], label='EJ321P HH')
# plt.scatter(np.arange(2,6.25,0.25), EJ321PSysError['TP_sysErr'], label='EJ321P TP')
# plt.scatter(np.arange(2,6.25,0.25), EJ321PSysError['FD_sysErr'], label='EJ321P FD')
# plt.legend()
# plt.subplot(4,1,4)
# plt.scatter(np.arange(2,6.25,0.25), EJ331SysError['HH_sysErr'], label='EJ331 HH')
# plt.scatter(np.arange(2,6.25,0.25), EJ331SysError['TP_sysErr'], label='EJ331 TP')
# plt.scatter(np.arange(2,6.25,0.25), EJ331SysError['FD_sysErr'], label='EJ331 FD')
# plt.legend()
# plt.show()

prodata.HH_TP_FD(   pathData=pathData, 
                    detector = 'NE213A', 
                    gate = 'LG',
                    numBinsData=NE213AnumBinsDataLG, 
                    fitRangeData=NE213AfitRangeDataLG,
                    relErrSystematic=NE213ASysError,
                    fitError=True,
                    plot=False)

prodata.HH_TP_FD(   pathData=pathData, 
                    detector = 'EJ305', 
                    gate = 'LG',
                    numBinsData=EJ305numBinsDataLG, 
                    fitRangeData=EJ305fitRangeDataLG,
                    relErrSystematic=EJ305SysError,
                    fitError=True,
                    plot=False)

prodata.HH_TP_FD(   pathData=pathData, 
                    detector = 'EJ331', 
                    gate = 'LG',
                    numBinsData=EJ331numBinsDataLG, 
                    fitRangeData=EJ331fitRangeDataLG, 
                    relErrSystematic=EJ331SysError,
                    fitError=True,
                    plot=False)

prodata.HH_TP_FD(   pathData=pathData, 
                    detector = 'EJ321P', 
                    gate = 'LG',
                    numBinsData=EJ321PnumBinsDataLG, 
                    fitRangeData=EJ321PfitRangeDataLG,
                    relErrSystematic=EJ321PSysError,
                    fitError=True,
                    plot=True)



###############################################################
######## Fit HH, TP and FD Data and Simulation values  ########
################ Kronilov and Cecil ###########################

pathData = '/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper'

#### LG ####
prodata.HH_TP_FD_fitting(   pathData=pathData, 
                detector = 'NE213A', 
                gate = 'LG',
                fitError=False,
                plot=False)

prodata.HH_TP_FD_fitting(   pathData=pathData, 
                detector = 'EJ305', 
                gate = 'LG',
                fitError=False,
                plot=False)

prodata.HH_TP_FD_fitting(   pathData=pathData, 
                detector = 'EJ331', 
                gate = 'LG',
                fitError=False,
                plot=False)
             
prodata.HH_TP_FD_fitting(   pathData=pathData, 
                detector = 'EJ321P', 
                gate = 'LG',
                fitError=False,
                plot=True)




# ███████ ██ ███    ███     ██████  ██████   ██████   ██████ ███████ ███████ ███████ ██ ███    ██  ██████  
# ██      ██ ████  ████     ██   ██ ██   ██ ██    ██ ██      ██      ██      ██      ██ ████   ██ ██       
# ███████ ██ ██ ████ ██     ██████  ██████  ██    ██ ██      █████   ███████ ███████ ██ ██ ██  ██ ██   ███ 
#      ██ ██ ██  ██  ██     ██      ██   ██ ██    ██ ██      ██           ██      ██ ██ ██  ██ ██ ██    ██ 
# ███████ ██ ██      ██     ██      ██   ██  ██████   ██████ ███████ ███████ ███████ ██ ██   ████  ██████  

pathData = '/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper'
pathSim =  '/media/gheed/Seagate_Expansion_Drive1/data/simulation/'

NE213AnumBinsSim = [np.arange(0.00, 4, 0.0380),#2000 keV
                    np.arange(0.07, 4, 0.0380),#2250 keV
                    np.arange(0.08, 4, 0.0400),#2500 keV
                    np.arange(0.10, 4, 0.0412),#2750 keV
                    np.arange(0.12, 4, 0.0454),#3000 keV
                    np.arange(0.13, 4, 0.0496),#3250 keV
                    np.arange(0.14, 4, 0.0538),#3500 keV
                    np.arange(0.16, 4, 0.0580),#3750 keV
                    np.arange(0.17, 4, 0.0622),#4000 keV
                    np.arange(0.19, 4, 0.0664),#4250 keV
                    np.arange(0.20, 4, 0.0706),#4500 keV
                    np.arange(0.21, 4, 0.0748),#4750 keV
                    np.arange(0.23, 4, 0.0790),#5000 keV
                    np.arange(0.24, 4, 0.0832),#5250 keV
                    np.arange(0.26, 4, 0.0874),#5500 keV
                    np.arange(0.27, 4, 0.0916),#5750 keV
                    np.arange(0.29, 4.5, 0.0958),#6000 keV
                    np.arange(0.30, 4.5, 0.1000),#6250 keV
                    np.arange(0.30, 4.5, 0.1000),#6500 keV
                    np.arange(0.30, 4.6, 0.1000),#6750 keV
                    np.arange(0.30, 4.8, 0.1000)]#7000 keV

NE213AfitRangeSim  = [  [0.30, 1.00], #2000
                        [0.50, 1.13], #2250
                        [0.56, 1.14], #2500
                        [0.64, 1.35], #2750
                        [0.72, 1.51], #3000
                        [0.90, 1.70], #3250
                        [1.02, 1.80], #3500
                        [1.15, 2.00], #3750
                        [1.23, 2.20], #4000
                        [1.37, 2.40], #4250
                        [1.50, 2.65], #4500
                        [1.65, 2.86], #4750
                        [1.75, 2.95], #5000
                        [2.00, 3.10], #5250
                        [2.10, 3.20], #5500
                        [2.30, 3.40], #5750
                        [2.48, 3.70], #6000
                        [2.85, 3.80], #6250
                        [3.00, 4.02], #6500
                        [3.03, 4.05], #6750
                        [3.15, 4.20]] #7000

NE213ASmearSim  = [ 0.27002977,#2000
                    0.2335566, 
                    0.20437806, 
                    0.18050471, 
                    0.16061025, #3000
                    0.14377648,
                    0.12934753, 
                    0.11684244, 
                    0.10590049, #4000
                    0.09624583, 
                    0.0876639 ,
                    0.07998534, 
                    0.07307464, #5000
                    0.06682209, 
                    0.06113796, 
                    0.0559481 ,
                    0.05119073, #6000
                    0.04681395, 
                    0.04277385, 
                    0.03903301, 
                    0.03555937] #7000

prodata.simLightYield(pathData = pathData, 
                    pathSim = pathSim, 
                    detector = 'NE213A', 
                    numBinsRange = NE213AnumBinsSim,
                    fitRange = NE213AfitRangeSim,
                    smearFactor=NE213ASmearSim, 
                    edepLow = 0.99,#0.9996,
                    edepHigh = 1.01,#1.0004,
                    edepCol = 'proton_edep',
                    fitError=False,
                    plot=False)

####################################################################

EJ305numBinsSim = [ np.arange(0.0, 4, 0.022),#2000 keV
                    np.arange(0.0, 4, 0.0400),#2250 keV
                    np.arange(0.00, 4, 0.0500),#2500 keV
                    np.arange(0.00, 4, 0.0500),#2750 keV
                    np.arange(0.00, 4, 0.0500),#3000 keV
                    np.arange(0.00, 4, 0.0496),#3250 keV
                    np.arange(0.00, 4, 0.0538),#3500 keV
                    np.arange(0.00, 4, 0.0580),#3750 keV
                    np.arange(0.00, 4, 0.0622),#4000 keV
                    np.arange(0.15, 4, 0.0664),#4250 keV
                    np.arange(0.20, 4, 0.0706),#4500 keV
                    np.arange(0.21, 4, 0.0748),#4750 keV
                    np.arange(0.23, 4, 0.0790),#5000 keV
                    np.arange(0.24, 4, 0.0832),#5250 keV
                    np.arange(0.26, 4, 0.0874),#5500 keV
                    np.arange(0.27, 4, 0.0916),#5750 keV
                    np.arange(0.29, 4.5, 0.0958),#6000 keV
                    np.arange(0.30, 4.5, 0.1000),#6250 keV
                    np.arange(0.30, 4.5, 0.1000),#6500 keV
                    np.arange(0.30, 4.6, 0.1000),#6750 keV
                    np.arange(0.30, 4.8, 0.1000)]#7000 keV

EJ305fitRangeSim  = [   [0.24, 0.69], #2000
                        [0.33, 0.78], #2250
                        [0.40, 0.90], #2500
                        [0.48, 1.15], #2750
                        [0.48, 1.37], #3000
                        [0.55, 1.30], #3250
                        [0.70, 1.40], #3500
                        [0.78, 1.76], #3750
                        [0.87, 1.88], #4000
                        [1.05, 1.85], #4250
                        [1.20, 2.25], #4500
                        [1.33, 2.32], #4750
                        [1.36, 2.30], #5000
                        [1.40, 2.65], #5250
                        [1.58, 2.68], #5500
                        [1.66, 2.85], #5750
                        [1.84, 3.05], #6000
                        [2.57, 3.58], #6250
                        [2.85, 3.81], #6500
                        [2.95, 4.06], #6750
                        [3.15, 4.22]] #7000

EJ305SmearSim = [0.34800322, #2000
              0.29768745, 
              0.25743484, 
              0.22450089, 
              0.19705593,#3000
              0.17383327, 
              0.15392813, 
              0.13667701, 
              0.12158228, #4000
              0.10826341,
              0.0964244 , 
              0.08583161, 
              0.0762981 , #5000
              0.06767254, 
              0.05983112,
              0.05267157, 
              0.04610864]#6000


prodata.simLightYield(pathData = pathData, 
                    pathSim = pathSim, 
                    detector = 'EJ305', 
                    numBinsRange = EJ305numBinsSim,
                    fitRange = EJ305fitRangeSim,
                    smearFactor=EJ305SmearSim, 
                    edepLow = 0.99,#0.9996,
                    edepHigh = 1.01,#1.0004,
                    edepCol = 'proton_edep',
                    fitError=False,
                    plot=False)

####################################################################

EJ331numBinsSim = [ np.arange(0.00, 4, 0.0380),#2000 keV
                    np.arange(0.00, 4, 0.0400),#2250 keV
                    np.arange(0.00, 4, 0.0500),#2500 keV
                    np.arange(0.00, 4, 0.0500),#2750 keV
                    np.arange(0.00, 4, 0.0500),#3000 keV
                    np.arange(0.00, 4, 0.0500),#3250 keV
                    np.arange(0.00, 4, 0.0538),#3500 keV
                    np.arange(0.00, 4, 0.0580),#3750 keV
                    np.arange(0.00, 4, 0.0622),#4000 keV
                    np.arange(0.00, 4, 0.0664),#4250 keV
                    np.arange(0.00, 4, 0.0706),#4500 keV
                    np.arange(0.00, 4, 0.0748),#4750 keV
                    np.arange(0.00, 4, 0.0790),#5000 keV
                    np.arange(0.00, 4, 0.0832),#5250 keV
                    np.arange(0.00, 4, 0.0874),#5500 keV
                    np.arange(0.00, 4, 0.0916),#5750 keV
                    np.arange(0.00, 4.5, 0.0958),#6000 keV
                    np.arange(0.00, 4.5, 0.1000),#6250 keV
                    np.arange(0.00, 4.5, 0.1000),#6500 keV
                    np.arange(0.00, 4.6, 0.1000),#6750 keV
                    np.arange(0.00, 4.8, 0.1000)]#7000 keV

EJ331fitRangeSim  = [   [0.24, 0.76], #2000
                        [0.24, 0.85], #2250
                        [0.29, 0.90], #2500
                        [0.34, 1.10], #2750
                        [0.41, 1.15], #3000
                        [0.43, 1.25], #3250
                        [0.53, 1.35], #3500
                        [0.58, 1.40], #3750
                        [0.88, 1.65], #4000
                        [0.95, 1.98], #4250
                        [1.11, 2.00], #4500
                        [1.20, 2.20], #4750
                        [1.30, 2.40], #5000
                        [1.40, 2.60], #5250
                        [1.50, 2.80], #5500
                        [1.60, 3.00], #5750
                        [1.70, 3.20], #6000
                        [1.80, 3.40], #6250
                        [1.90, 3.60], #6500
                        [2.00, 3.80], #6750
                        [2.10, 4.00]] #7000
       
EJ331SmearSim  = [    0.29496006, #2000
                     0.26946323, 
                     0.24906577, 
                     0.23237694, 
                     0.21846959, #3000
                     0.20670182, 
                     0.19661517, 
                     0.1878734 , 
                     0.18022435,  #4000
                     0.17347519,
                     0.16747594, 
                     0.16210819, 
                     0.15727721,  #5000
                     0.15290633, 
                     0.1489328 ,
                     0.14530479, 
                     0.14197912,  #6000
                     0.1389195 , 
                     0.13609523, 
                     0.13348018,
                     0.13105191] #7000

prodata.simLightYield(pathData = pathData, 
                    pathSim = pathSim, 
                    detector = 'EJ331', 
                    numBinsRange = EJ331numBinsSim,
                    fitRange = EJ331fitRangeSim,
                    smearFactor=EJ331SmearSim, 
                    edepLow = 0.99,#0.9996,
                    edepHigh = 1.01,#1.0004,
                    edepCol = 'proton_edep',
                    fitError = False,
                    plot=False)


####################################################################
EJ321PnumBinsSim =  [np.arange(0.0, 4, 0.0280),#2000 keV
                     np.arange(0.0, 4, 0.0303),#2250 keV
                     np.arange(0.0, 4, 0.0326),#2500 keV
                     np.arange(0.0, 4, 0.0349),#2750 keV
                     np.arange(0.0, 4, 0.0373),#3000 keV
                     np.arange(0.0, 4, 0.0396),#3250 keV
                     np.arange(0.0, 4, 0.0419),#3500 keV
                     np.arange(0.0, 4, 0.0442),#3750 keV
                     np.arange(0.0, 4, 0.0465),#4000 keV
                     np.arange(0.0, 4, 0.0488),#4250 keV
                     np.arange(0.0, 4, 0.0511),#4500 keV
                     np.arange(0.0, 4, 0.0534),#4750 keV
                     np.arange(0.0, 4, 0.0558),#5000 keV
                     np.arange(0.0, 4, 0.0581),#5250 keV
                     np.arange(0.0, 4, 0.0604),#5500 keV
                     np.arange(0.0, 4, 0.0627),#5750 keV
                     np.arange(0.0, 4, 0.0650)]#6000 keV

EJ321PfitRangeSim  = [  [0.20, 0.61], #2000
                        [0.24, 0.63], #2250
                        [0.29, 0.65], #2500
                        [0.34, 0.87], #2750
                        [0.40, 0.90], #3000
                        [0.20, 1.25], #3250
                        [0.22, 1.35], #3500
                        [0.44, 1.22], #3750
                        [0.55, 1.35], #4000
                        [0.59, 1.50], #4250
                        [0.70, 1.65], #4500
                        [0.82, 1.74], #4750
                        [0.90, 2.00], #5000
                        [1.09, 2.10], #5250
                        [1.20, 2.18], #5500
                        [1.21, 2.34], #5750
                        [1.35, 2.49], #6000
                        [0.62, 2.62], #6250
                        [0.75, 2.81], #6500
                        [0.92, 3.00], #6750
                        [0.95, 3.20]] #7000

EJ321PSmearSim  = [0.31394474, #2000
                     0.28561771, 
                     0.26295609, 
                     0.24441476, 
                     0.22896365,#3000
                     0.21588964, 
                     0.20468334, 
                     0.19497122, 
                     0.18647311, #4000
                     0.17897478,
                     0.17230959, 
                     0.16634601, 
                     0.16097878, #5000
                     0.15612272, 
                     0.15170812,
                     0.14767739, 
                     0.14398256, #6000
                     0.14058332, 
                     0.13744556, 
                     0.13454022,
                     0.13184241]#7000

prodata.simLightYield(pathData = pathData, 
                    pathSim = pathSim, 
                    detector = 'EJ321P', 
                    numBinsRange = EJ321PnumBinsSim,
                    fitRange = EJ321PfitRangeSim,
                    smearFactor=EJ321PSmearSim, 
                    edepLow = 0.99,#0.9996,
                    edepHigh = 1.01,#1.0004,
                    edepCol = 'proton_edep',
                    fitError = False,
                    plot=False)













   
#### SG ####
# prodata.HH_TP_FD_fitting(   pathData=pathData, 
#                 detector = 'NE213A', 
#                 gate = 'SG',
#                 fitError=True,
#                 plot=False)

# prodata.HH_TP_FD_fitting(   pathData=pathData, 
#                 detector = 'EJ305', 
#                 gate = 'SG',
#                 fitError=True,
#                 plot=False)

# prodata.HH_TP_FD_fitting(   pathData=pathData, 
#                 detector = 'EJ321P', 
#                 gate = 'SG',
#                 fitError=True,
#                 plot=False)

# prodata.HH_TP_FD_fitting(   pathData=pathData, 
#                 detector = 'EJ331', 
#                 gate = 'SG',
#                 fitError=True,
#                 plot=False)


#### SG ####
# NE213AnumBinsDataSG = [ np.arange(0.03, 4, 0.0130),#1500 keV
#                         np.arange(0.04, 4, 0.0140),#1750 keV
#                         np.arange(0.06, 4, 0.0150),#2000 keV
#                         np.arange(0.07, 4, 0.0172),#2250 keV
#                         np.arange(0.08, 4, 0.0194),#2500 keV
#                         np.arange(0.10, 4, 0.0216),#2750 keV
#                         np.arange(0.12, 4, 0.0238),#3000 keV
#                         np.arange(0.13, 4, 0.0259),#3250 keV
#                         np.arange(0.14, 4, 0.0281),#3500 keV
#                         np.arange(0.16, 4, 0.0303),#3750 keV
#                         np.arange(0.17, 4, 0.0325),#4000 keV
#                         np.arange(0.19, 4, 0.0347),#4250 keV
#                         np.arange(0.20, 4, 0.0369),#4500 keV
#                         np.arange(0.21, 4, 0.0391),#4750 keV
#                         np.arange(0.23, 4, 0.0413),#5000 keV
#                         np.arange(0.24, 4, 0.0434),#5250 keV
#                         np.arange(0.26, 4, 0.0456),#5500 keV
#                         np.arange(0.27, 4, 0.0478),#5750 keV
#                         np.arange(0.29, 4, 0.0500),#6000 keV
#                         np.arange(0.30, 4, 0.0520)]#6250 keV

# NE213AfitRangeDataSG = [[0.14, 0.40], #1500
#                 [0.14, 0.60], #1750
#                 [0.16, 0.70], #2000
#                 [0.19, 0.80], #2250
#                 [0.22, 0.90], #2500
#                 [0.30, 1.50], #2750
#                 [0.30, 2.00], #3000
#                 [0.40, 2.00], #3250
#                 [0.43, 2.10], #3500
#                 [0.46, 2.20], #3750
#                 [0.55, 2.50], #4000
#                 [0.64, 2.60], #4250
#                 [0.74, 2.70], #4500
#                 [0.84, 3.00], #4750
#                 [0.86, 3.10], #5000
#                 [0.95, 3.20], #5250
#                 [0.98, 3.30], #5500
#                 [1.06, 3.50], #5750
#                 [1.37, 3.60], #6000
#                 [1.38, 3.60]] #6250

# EJ305numBinsDataSG = [  np.arange(0, 4, 0.0090),#1500 keV
#                         np.arange(0, 4, 0.0090),#1750 keV
#                         np.arange(0, 4, 0.0100),#2000 keV
#                         np.arange(0, 4, 0.0122),#2250 keV
#                         np.arange(0.08, 4, 0.0144),#2500 keV
#                         np.arange(0.10, 4, 0.0166),#2750 keV
#                         np.arange(0.12, 4, 0.0188),#3000 keV
#                         np.arange(0.13, 4, 0.0209),#3250 keV
#                         np.arange(0.14, 4, 0.0231),#3500 keV
#                         np.arange(0.16, 4, 0.0235),#3750 keV
#                         np.arange(0.17, 4, 0.0275),#4000 keV
#                         np.arange(0.19, 4, 0.0297),#4250 keV
#                         np.arange(0.20, 4, 0.0319),#4500 keV
#                         np.arange(0.21, 4, 0.0341),#4750 keV
#                         np.arange(0.23, 4, 0.0363),#5000 keV
#                         np.arange(0.24, 4, 0.0384),#5250 keV
#                         np.arange(0.26, 4, 0.0406),#5500 keV
#                         np.arange(0.27, 4, 0.0428),#5750 keV
#                         np.arange(0.29, 4, 0.0450),#6000 keV
#                         np.arange(0.30, 4, 0.0475)]#6250 keV

# EJ305fitRangeDataSG = [[0.01, 0.42], #1500
#                 [0.01, 0.55], #1750
#                 [0.05, 0.65], #2000
#                 [0.11, 0.75], #2250
#                 [0.19, 0.85], #2500
#                 [0.23, 1.20], #2750
#                 [0.28, 1.40], #3000
#                 [0.34, 1.50], #3250
#                 [0.37, 1.60], #3500
#                 [0.37, 1.70], #3750
#                 [0.44, 2.00], #4000
#                 [0.49, 2.10], #4250
#                 [0.56, 2.50], #4500
#                 [0.66, 2.70], #4750
#                 [0.78, 2.90], #5000
#                 [0.80, 3.00], #5250
#                 [0.88, 3.10], #5500
#                 [0.95, 3.30], #5750
#                 [1.11, 3.50], #6000
#                 [1.11, 3.70]] #6250

# EJ321PnumBinsDataSG =  [ np.arange(0.03, 4, 0.0130),#1500 keV
#                         np.arange(0.04, 4, 0.0140),#1750 keV
#                         np.arange(0.06, 4, 0.0150),#2000 keV
#                         np.arange(0.07, 4, 0.0172),#2250 keV
#                         np.arange(0.08, 4, 0.0194),#2500 keV
#                         np.arange(0.10, 4, 0.0216),#2750 keV
#                         np.arange(0.12, 4, 0.0238),#3000 keV
#                         np.arange(0.13, 4, 0.0259),#3250 keV
#                         np.arange(0.14, 4, 0.0281),#3500 keV
#                         np.arange(0.16, 4, 0.0303),#3750 keV
#                         np.arange(0.17, 4, 0.0325),#4000 keV
#                         np.arange(0.19, 4, 0.0347),#4250 keV
#                         np.arange(0.20, 4, 0.0369),#4500 keV
#                         np.arange(0.21, 4, 0.0391),#4750 keV
#                         np.arange(0.23, 4, 0.0413),#5000 keV
#                         np.arange(0.24, 4, 0.0434),#5250 keV
#                         np.arange(0.26, 4, 0.0456),#5500 keV
#                         np.arange(0.27, 4, 0.0478),#5750 keV
#                         np.arange(0.29, 4, 0.0500),#6000 keV
#                         np.arange(0.30, 4, 0.0520)]#6250 keV

# EJ321PfitRangeDataSG = [[0.16, 0.30], #1500
#                 [0.17, 0.40], #1750
#                 [0.22, 0.45], #2000
#                 [0.25, 0.45], #2250
#                 [0.26, 0.50], #2500
#                 [0.27, 0.65], #2750
#                 [0.34, 0.80], #3000
#                 [0.36, 0.90], #3250
#                 [0.38, 1.00], #3500
#                 [0.44, 1.10], #3750
#                 [0.48, 1.20], #4000
#                 [0.58, 1.30], #4250
#                 [0.62, 1.45], #4500
#                 [0.68, 1.55], #4750
#                 [0.74, 1.60], #5000
#                 [0.94, 1.70], #5250
#                 [0.99, 2.00], #5500
#                 [0.98, 2.10], #5750
#                 [1.10, 2.30], #6000
#                 [1.25, 2.40]] #6250
                
# EJ331numBinsDataSG = [ np.arange(0.03, 4, 0.0130),#1500 keV
#                         np.arange(0.04, 4, 0.0140),#1750 keV
#                         np.arange(0.06, 4, 0.0150),#2000 keV
#                         np.arange(0.07, 4, 0.0172),#2250 keV
#                         np.arange(0.08, 4, 0.0194),#2500 keV
#                         np.arange(0.10, 4, 0.0216),#2750 keV
#                         np.arange(0.12, 4, 0.0238),#3000 keV
#                         np.arange(0.13, 4, 0.0259),#3250 keV
#                         np.arange(0.14, 4, 0.0281),#3500 keV
#                         np.arange(0.16, 4, 0.0303),#3750 keV
#                         np.arange(0.17, 4, 0.0325),#4000 keV
#                         np.arange(0.19, 4, 0.0347),#4250 keV
#                         np.arange(0.20, 4, 0.0369),#4500 keV
#                         np.arange(0.21, 4, 0.0391),#4750 keV
#                         np.arange(0.23, 4, 0.0413),#5000 keV
#                         np.arange(0.24, 4, 0.0434),#5250 keV
#                         np.arange(0.26, 4, 0.0456),#5500 keV
#                         np.arange(0.27, 4, 0.0478),#5750 keV
#                         np.arange(0.29, 4, 0.0500),#6000 keV
#                         np.arange(0.30, 4, 0.0520)]#6250 keV

# EJ331fitRangeDataSG = [[0.11, 0.35], #1500
#                 [0.14, 0.40], #1750
#                 [0.14, 0.55], #2000
#                 [0.14, 0.60], #2250
#                 [0.17, 0.75], #2500
#                 [0.17, 0.85], #2750
#                 [0.27, 1.00], #3000
#                 [0.32, 1.20], #3250
#                 [0.36, 1.30], #3500
#                 [0.42, 1.40], #3750
#                 [0.48, 1.50], #4000
#                 [0.55, 1.60], #4250
#                 [0.57, 1.70], #4500
#                 [0.65, 1.80], #4750
#                 [0.79, 1.90], #5000
#                 [0.84, 2.00], #5250
#                 [0.85, 2.10], #5500
#                 [1.04, 2.30], #5750
#                 [1.30, 2.60], #6000
#                 [1.30, 2.60]] #6250

# # relErrSystematic= 0.0352

# prodata.HH_TP_FD(   pathData=pathData, 
#                     detector = 'NE213A', 
#                     gate = 'SG',
#                     numBinsData=NE213AnumBinsDataSG, 
#                     fitRangeData=NE213AfitRangeDataSG, 
#                     relErrSystematic=relErrSystematic,
#                     fitError=False,
#                     plot=False)

# prodata.HH_TP_FD(   pathData=pathData,
#                     detector = 'EJ305', 
#                     gate = 'SG',
#                     numBinsData=EJ305numBinsDataSG, 
#                     fitRangeData=EJ305fitRangeDataSG,
#                     relErrSystematic=relErrSystematic,
#                     fitError=False,
#                     plot=False)

# prodata.HH_TP_FD(   pathData=pathData, 
#                     detector = 'EJ321P', 
#                     gate = 'SG',
#                     numBinsData=EJ321PnumBinsDataSG, 
#                     fitRangeData=EJ321PfitRangeDataSG,
#                     relErrSystematic=relErrSystematic,
#                     fitError=False,
#                     plot=False)

# prodata.HH_TP_FD(   pathData=pathData, 
#                     detector = 'EJ331', 
#                     gate = 'SG',
#                     numBinsData=EJ331numBinsDataSG, 
#                     fitRangeData=EJ331fitRangeDataSG,
#                     relErrSystematic=relErrSystematic,
#                     fitError=False,
#                     plot=False)