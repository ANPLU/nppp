#!/usr/bin/env python3
from re import I
import sys
from tkinter import X
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
from numpy.core.arrayprint import SubArrayFormat
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit
# from matplotlib.colors import LinearSegmentedColormap


# IMPORT FROM LIBRARY DIRECTORY
sys.path.insert(0, "../library/")
sys.path.insert(0, "../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "../")
sys.path.insert(0, "data_analysis/")

import processing_math as promath
import processing_data as prodata
import processing_pd as propd
import processing_simulation as prosim
import figure_style as fs



names = [ 'evtNum', 
        'optPhotonSum', 
        'optPhotonSumQE', 
        'optPhotonSumCompton', 
        'optPhotonSumComptonQE', 
        'optPhotonSumNPQE', 
        'xLoc', 
        'yLoc', 
        'zLoc', 
        'CsMin', 
        'optPhotonParentID', 
        'optPhotonParentCreatorProcess', 
        'optPhotonParentStartingEnergy',
        'total_edep',
        'gamma_edep',
        'proton_edep',
        'nScatters',
        'nScattersProton']

pathSim = '/media/gheed/Seagate_Expansion_Drive1/data/simulation'
pathData = '/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper'
edepHigh = 1.0004
edepLow = 0.9996


# #NE213A
# detector = 'NE213A'
# numBinsRange = [np.arange(0.00, 4, 0.0480),#2000 keV
#                     np.arange(0.12, 4, 0.0554),#3000 keV
#                     np.arange(0.17, 4, 0.0722),#4000 keV
#                     np.arange(0.23, 4, 0.0890),#5000 keV
#                     np.arange(0.29, 4.5, 0.1058),#6000 keV
#                     np.arange(0.30, 4.8, 0.1100)]#7000 keV

# fitRange  = [  [0.45, 1.00], #2000
#                 [0.90, 1.55], #3000
#                 [1.42, 2.30], #4000
#                 [2.12, 2.95], #5000
#                 [2.76, 3.70], #6000
#                 [3.15, 4.20]] #7000

# SmearSim  = [ 0.27002977,#2000
#             0.16061025, #3000
#             0.10590049, #4000
#             0.07307464, #5000
#             0.05119073, #6000
#             0.03555937] #7000

#EJ305
# detector = 'EJ305'
# numBinsRange = [ np.arange(0.00, 4, 0.0380),#2000 keV
#                     np.arange(0.00, 4, 0.0500),#3000 keV
#                     np.arange(0.00, 4, 0.0622),#4000 keV
#                     np.arange(0.23, 4, 0.0790),#5000 keV
#                     np.arange(0.29, 4.5, 0.0958),#6000 keV
#                     np.arange(0.30, 4.8, 0.1000)]#7000 keV
# fitRange  = [   [0.24, 0.69], #2000
#                         [0.48, 1.37], #3000
#                         [0.87, 1.88], #4000
#                         [1.51, 2.64], #5000
#                         [2.41, 3.43], #6000
#                         [3.15, 4.22]] #7000
# SmearSim = [0.34800322, #2000
#             0.19705593, #3000
#             0.12158228, #4000
#             0.0762981 , #5000
#             0.04610864] #6000

#EJ321P
# detector = 'EJ321P'
# numBinsRange = [np.arange(0.00, 4, 0.0220),#2000 keV
#                 np.arange(0.00, 4, 0.0300),#3000 keV
#                 np.arange(0.00, 4, 0.0422),#4000 keV
#                 np.arange(0.00, 4, 0.0590),#5000 keV
#                 np.arange(0.00, 4.5, 0.0758),#6000 keV
#                 np.arange(0.00, 4.8, 0.080)]#7000 keV

# fitRange  = [[0.20, 0.61], #2000
#             [0.15, 1.15], #3000
#             [0.30, 1.48], #4000
#             [0.58, 2.49], #6000
#             [0.95, 3.20]] #7000

# SmearSim  = [0.31394474, #2000
#             0.22896365,  #3000
#             0.18647311,  #4000
#             0.16097878,  #5000
#             0.14398256] #6000

#EJ331
detector = 'EJ331'
numBinsRange = [ np.arange(0.00, 4, 0.0280),#2000 keV
                    np.arange(0.00, 4, 0.0400),#3000 keV
                    np.arange(0.00, 4, 0.0522),#4000 keV
                    np.arange(0.00, 4, 0.0690),#5000 keV
                    np.arange(0.00, 4.5, 0.0858),#6000 keV
                    np.arange(0.00, 4.8, 0.0900)]#7000 keV

fitRange  = [   [0.24, 0.76], #2000
                [0.41, 1.15], #3000
                [0.88, 1.65], #4000
                [1.30, 2.40], #5000
                [1.70, 3.20], #6000
                [2.10, 4.00]] #7000
       

SmearSim  = [   0.29496006, #2000
                0.21846959, #3000
                0.18022435,  #4000
                0.15727721,  #5000
                0.14197912,  #6000
                0.13105191] #7000

Tn = np.arange(2000,7000,1000)
fs.set_style(textwidth=345)

for i, E in enumerate(Tn):
    print(f'Tn = {E/1000} MeV, smearing = {SmearSim[i]}')

    neutron_sim_pen_raw = pd.read_csv(f"{pathSim}/{detector}/neutrons/neutron_range/{int(E)}keV/pencilbeam/CSV_optphoton_data_sum.csv", names=names)
    
    #find maximum edep location
    y, x = np.histogram(neutron_sim_pen_raw['total_edep'], bins=np.arange(E-10, E+10, 0.3))
    x = prodata.getBinCenters(x)
    maxEdepLoc = x[np.argmax(y)]

    #apply edep cuts
    neutron_sim_pen_cut = neutron_sim_pen_raw.query(f'{maxEdepLoc*edepLow}<total_edep<{maxEdepLoc*edepHigh}')
    
    #randomize binning
    y, x = np.histogram(neutron_sim_pen_cut['optPhotonSumQE'], bins=4096, range=[0, 4096]) 
    neutron_sim_pen_cut['optPhotonSumQE'] = prodata.getRandDist(x, y)

    #apply smearing
    neutron_sim_pen_cut['optPhotonSumQEsmear'] = neutron_sim_pen_cut['optPhotonSumQE'].apply(lambda x: prosim.gaussSmear(x, SmearSim[i]))
    
    #Applying light yield factor to simulation.
    neutron_sim_pen_cut['optPhotonSumQEsmear'] = neutron_sim_pen_cut['optPhotonSumQEsmear']

    #calibrate simulations
    neutron_sim_pen_cut['optPhotonSumQEsmearcal'] = prodata.calibrateMyDetector(f'{pathData}/{detector}/Simcal/popt.npy', neutron_sim_pen_cut['optPhotonSumQEsmear'], inverse=False)
    
    #bin simulations
    y_sim, x_sim = np.histogram(neutron_sim_pen_cut['optPhotonSumQEsmearcal'], bins=numBinsRange[i])
    x_sim = prodata.getBinCenters(x_sim)
    
    #Fit gaussian
    popt, pcov = promath.gaussFit(x_sim, y_sim, fitRange[i][0], fitRange[i][1], y_error=False, error=True)
    mean = popt[1]
    std = popt[2]

    #Determine mean position
    meanPos = np.mean(neutron_sim_pen_cut.query(f'optPhotonSumQEsmearcal>{mean-3*std} and optPhotonSumQEsmearcal<{mean+3*std}')['optPhotonSumQEsmearcal'])
    #Determine mean position error (statistical error)
    meanPosErr = np.std(neutron_sim_pen_cut.query(f'optPhotonSumQEsmearcal>{mean-3*std} and optPhotonSumQEsmearcal<{mean+3*std}')['optPhotonSumQEsmearcal']) / np.sqrt(len(neutron_sim_pen_cut.query(f'optPhotonSumQEsmearcal>{mean-3*std} and optPhotonSumQEsmearcal<{mean+3*std}')))
    #Adding systematic errors
    sim_birks_error = np.load(f'{pathData}/{detector}/birks/total_error.npy')
    #Merging all errors
    totalRelError = np.sqrt((meanPosErr/meanPos)**2 + sim_birks_error**2)
    meanPosErr = meanPos*totalRelError

    
    #Make figure

    plt.subplot(1,5,i+1)
    plt.step(x_sim, y_sim, lw=1)
    plt.plot(np.arange(0, 5, 0.001), promath.gaussFunc(np.arange(0, 5, 0.001), popt[0], popt[1], popt[2]), color='black', ls='dashed', lw=0.75)
    plt.xlim([0,5.25])
    plt.yticks([])
    plt.xticks([])
    if E==4000:
        plt.xlabel('Scintillation Light Yield [MeVee]')
    # plt.legend()
plt.subplots_adjust(hspace = .001, wspace = .001)

plt.savefig(f'/home/gheed/Downloads/{detector}.pdf')

plt.show()








