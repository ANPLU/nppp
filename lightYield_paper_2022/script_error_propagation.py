#!/usr/bin/env python3
from re import I
import sys
from tkinter import X
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
from numpy.core.arrayprint import SubArrayFormat
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit

# from matplotlib.colors import LinearSegmentedColormap


# IMPORT FROM LIBRARY DIRECTORY
sys.path.insert(0, "../library/")
sys.path.insert(0, "../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "../")
sys.path.insert(0, "data_analysis/")

import processing_math as promath
import processing_data as prodata
import processing_pd as propd
import processing_simulation as prosim
import figure_style as fs

# ##########################################################
# ######### Fundamental parameters #########################
# ##########################################################
pathSim = '/media/gheed/Seagate_Expansion_Drive1/data/simulation'
pathData = '/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper'



# ██████   █████  ████████  █████      ███████ ██████  ██████   ██████  ██████  
# ██   ██ ██   ██    ██    ██   ██     ██      ██   ██ ██   ██ ██    ██ ██   ██ 
# ██   ██ ███████    ██    ███████     █████   ██████  ██████  ██    ██ ██████  
# ██   ██ ██   ██    ██    ██   ██     ██      ██   ██ ██   ██ ██    ██ ██   ██ 
# ██████  ██   ██    ██    ██   ██     ███████ ██   ██ ██   ██  ██████  ██   ██ 

#Load data
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#    DEFINE PARAMETERS FOR SLICE
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
distance = 0.959 - 0.005
gTOFRange = [1e-9, 6e-9] #Define range of gamma-flash
randTOFRange = [-710e-9,-10e-9] #Define range of random events
gLength = np.abs(gTOFRange[1]-gTOFRange[0]) #Calculate the lenght of cut for gammas
randLength = np.abs(randTOFRange[1]-randTOFRange[0]) #Calculate lenght of cut for random events

minE = 1625
maxE = 6625
dE = 250
nLength = pd.DataFrame()
for i in np.arange(minE, maxE, dE):
    E1 = round(i-dE,3)
    E2 = round(i,3)
    t1 = prodata.EnergytoTOF(E1/1000, distance)
    t2 = prodata.EnergytoTOF(E2/1000, distance)
    print(f'dE = {round(E1, 3)}-{round(E2, 3)} keV, center = {E1-dE/2} -> dt = {np.round((t1-t2)*1e9, 4)} ns')
    nLength[f'keV{int((i-dE/2))}'] = [np.abs(t1-t2)]

y2ThrQDC = 6450 #QDC threhold in channels
y3ThrQDC = 6985 #QDC threhold in channels
y4ThrQDC = 7500 #QDC threhold in channels
y5ThrQDC = 7850 #QDC threhold in channels
nThrQDC = 0 #QDC threshold in channels

#NE213A
# detector = 'NE213A'
# att_factor = 3.98 # 12dB
# run1 = 2121
# numRuns = 100#141
# flashrange = {'tof_ch2':[60, 73], 'tof_ch3':[60, 73], 'tof_ch4':[60, 73], 'tof_ch5':[60, 73]} #gamma flash fitting ranges
# gainOffsets = np.array([1.    , 1.0098, 1.0106, 1.01  , 1.0098, 1.0098, 1.0098, 1.0098,
#                         1.0098, 1.0098, 1.01  , 1.0199, 1.0104, 1.0308, 1.0199, 1.0197,
#                         0.99  , 1.0098, 1.0098, 1.0197, 1.0199, 1.0108, 0.9999, 0.9999,
#                         1.0003, 1.0011, 1.0003, 1.0001, 1.0001, 1.0001, 1.0001, 1.0098,
#                         0.9902, 0.9999, 0.9908, 0.9902, 0.9906, 0.991 , 1.0197, 0.9999,
#                         0.9807, 0.9999, 0.9813, 0.9801, 0.9702, 0.99  , 0.99  , 0.9902,
#                         0.9999, 0.99  , 1.0098, 0.99  , 1.0197, 0.99  , 0.99  , 1.0102,
#                         0.9801, 0.99  , 0.9999, 0.9801, 0.9803, 1.0199, 0.9809, 0.9801,
#                         0.9902, 0.9813, 0.9807, 1.0098, 0.99  , 0.9999, 1.0098, 0.99  ,
#                         0.9803, 1.0098, 0.9999, 1.0098, 1.0005, 1.0001, 1.01  , 0.9702,
#                         1.0017, 0.9803, 0.9912, 0.9999, 0.9904, 1.011 , 1.0098, 1.0197,
#                         0.9823, 1.01  , 1.0001, 1.0098, 1.0197, 0.99  , 1.0005, 1.0009,
#                         0.9906, 0.9704, 0.9999, 0.9999, 1.0116, 0.9902, 0.9906, 0.9914,
#                         0.99  , 0.99  , 1.0001, 0.99  , 1.0009, 0.99  , 0.99  , 0.9999,
#                         0.9904, 0.9904, 0.9902, 0.9908, 0.9999, 0.9805, 0.9906, 0.9817,
#                         0.9902, 0.9803, 0.99  , 0.99  , 0.9807, 0.99  , 0.9817, 0.9718,
#                         0.9805, 0.9801, 0.9805, 0.9809, 0.9819, 0.9801, 0.9805, 0.9803,
#                         0.9805, 0.9801, 0.99  , 0.9801, 0.9803])

#EJ305
# detector = 'EJ305'
# att_factor = 6.31 #
# run1 = 3439
# numRuns = 100
# flashrange = {'tof_ch2':[55, 66], 'tof_ch3':[55, 66], 'tof_ch4':[55, 66], 'tof_ch5':[55, 66]} #gamma flash fitting ranges
# gainOffsets = np.array([1.    , 1.0003, 1.0007, 1.0098, 1.0003, 1.0005, 1.0001, 0.9999,
#                         0.9999, 0.9934, 0.993 , 1.0007, 0.9999, 0.9934, 0.9926, 0.9914,
#                         1.0001, 0.9924, 0.9904, 0.9914, 0.991 , 0.9912, 0.9926, 0.99  ,
#                         0.99  , 0.9906, 0.99  , 0.99  , 0.99  , 0.99  , 0.99  , 0.99  ,
#                         0.9829, 0.99  , 0.9847, 0.9813, 0.9815, 0.99  , 0.9837, 0.9813,
#                         0.9841, 0.9801, 0.9801, 0.9837, 0.9809, 0.9809, 0.9801, 0.9823,
#                         0.9819, 0.9801, 0.9801, 0.9801, 0.9803, 0.9805, 0.9801, 0.9801,
#                         0.99  , 0.9801, 0.9803, 0.9803, 0.9801, 0.9807, 0.9803, 0.9801,
#                         0.9801, 0.9801, 0.9702, 0.9712, 0.9734, 0.9708, 0.9722, 0.9801,
#                         0.973 , 0.9706, 0.9714, 0.971 , 0.9827, 0.9801, 0.9734, 0.9738,
#                         0.9702, 0.9702, 0.9702, 0.9722, 0.9726, 0.9704, 0.9704, 0.9704,
#                         0.9704, 0.9702, 0.9702, 0.9702, 0.9702, 0.9702, 0.9702, 0.9702,
#                         0.9702, 0.9718, 0.9702, 0.9702, 0.9702, 0.9702, 0.9704, 0.9702,
#                         0.9702, 0.9702, 0.9702, 0.9702, 0.9617, 0.9702, 0.9702, 0.9627,
#                         0.9702, 0.9702, 0.9702, 0.9635, 0.9702, 0.9621, 0.9702, 0.9702,
#                         0.9702, 0.9625, 0.9617, 0.9637, 0.9623, 0.9702, 0.9617, 0.9603,
#                         0.9603, 0.9603, 0.9603, 0.9609, 0.9605, 0.9603, 0.9702, 0.9603,
#                         0.9607, 0.9611, 0.9603, 0.9609, 0.9617, 0.9603, 0.9609, 0.9603,
#                         0.9603, 0.9603, 0.9605, 0.9603, 0.9603, 0.9603, 0.9603, 0.9603,
#                         0.9605, 0.9603, 0.9603, 0.9603, 0.9603, 0.9603, 0.9603, 0.9603,
#                         0.9603, 0.9603, 0.9603, 0.9603, 0.9603, 0.9605, 0.9607])

#EJ321P
# detector = 'EJ321P'
# att_factor = 2.00 # 6dB
# run1 = 3607
# numRuns = 100
# flashrange = {'tof_ch2':[55, 66], 'tof_ch3':[55, 66], 'tof_ch4':[55, 66], 'tof_ch5':[55, 66]} #gamma flash fitting ranges
# gainOffsets = np.array([1.    , 0.9999, 1.01  , 0.9944, 0.9805, 1.0027, 0.9999, 0.9902,
#                         0.99  , 1.0001, 0.9801, 0.9803, 0.9801, 0.9803, 0.9807, 0.9805,
#                         0.9801, 0.9813, 0.9807, 0.9801, 0.9801, 0.9801, 0.9801, 0.9801,
#                         0.9801, 0.9706, 0.9708, 0.9706, 0.9726, 0.9704, 0.9704, 0.9702,
#                         0.9611, 0.9702, 0.9702, 0.9702, 0.9704, 0.9702, 0.9702, 0.9605,
#                         0.9605, 0.9615, 0.9702, 0.9603, 0.9605, 0.9611, 0.9635, 0.9609,
#                         0.9603, 0.9603, 0.9603, 0.9603, 0.9603, 0.9603, 0.9504, 0.9516,
#                         0.9504, 0.9506, 0.951 , 0.951 , 0.9504, 0.9508, 0.9516, 0.9504,
#                         0.9504, 0.9504, 0.9504, 0.9504, 0.9506, 0.9419, 0.9504, 0.9415,
#                         0.9504, 0.9504, 0.9405, 0.9417, 0.9504, 0.9504, 0.9411, 0.9405,
#                         0.9415, 0.9504, 0.9407, 0.9409, 0.9405, 0.9415, 0.9405, 0.9405,
#                         0.9405, 0.9405, 0.9405, 0.9405, 0.9405, 0.9405, 0.9405, 0.931 ,
#                         0.9405, 0.9308, 0.931 , 0.9306, 0.9306, 0.9306, 0.9306, 0.9306,
#                         0.9306, 0.9306, 0.931 , 0.9306, 0.9306, 0.9306, 0.9306, 0.9225,
#                         0.9306, 0.9223, 0.9306, 0.9308, 0.9209, 0.9207, 0.9215, 0.9211])

#EJ331
detector = 'EJ331'
att_factor = 5.62 # 15dB
run1 = 2342
numRuns = 79
flashrange = {'tof_ch2':[59, 76], 'tof_ch3':[59, 76], 'tof_ch4':[59, 76], 'tof_ch5':[59, 76]} #gamma flash fitting ranges
gainOffsets = np.array([1.0098, 1.0108, 1.0401, 1.0197, 1.0494, 1.0197, 1.0015, 1.0207,
                        1.0296, 0.9999, 1.0197, 1.0199, 1.0201, 1.0104, 0.9999, 1.0098,
                        1.0098, 1.01  , 1.0102, 1.0108, 1.0102, 1.0197, 1.0108, 1.0003,
                        1.0197, 1.0112, 0.9999, 0.9999, 1.0098, 0.9904, 1.0013, 1.0211,
                        0.9916, 1.0203, 0.9904, 1.0098, 1.0098, 1.0003, 1.0098, 1.0098,
                        1.0005, 0.99  , 1.0098, 1.01  , 0.9999, 0.9999, 0.9906, 0.9908,
                        1.0197, 1.0197, 0.9999, 1.01  , 1.0098, 0.9916, 1.0102, 1.0098,
                        1.0098, 0.9999, 1.0098, 1.0098, 1.0098, 1.0098, 0.99  , 1.0098,
                        1.0005, 0.99  , 0.99  , 1.0199, 1.0102, 0.99  , 1.0203, 0.9999,
                        0.9999, 1.0098, 0.9999, 0.99  , 1.0003, 1.0001, 1.0197])


# predefine holder for final UNBINNED data sets
neutron_QDC_FIN = pd.DataFrame()
random_QDC_FIN = pd.DataFrame()

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# LOADING DATA & SLICING LOOP
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop
# path = '/media/gheed/backup_data_8TB/EJ321P_char/cooked/' #path on Nicholais laptop


runList = np.arange(run1, run1+numRuns)
stepSize = 5

for i in np.arange(0, len(runList), stepSize):
    currentRunList      = np.split(runList, [i, i+stepSize])[1] #split runlist into section of stepSize events
    currentGainOffsets  = np.split(gainOffsets, [i, i+stepSize])[1]  #split gainoffsets into section of stepSize events
    print('-------------------------------------------------')
    print(f'Now working on runs: {currentRunList}')
    PuBe_tof_data = propd.load_parquet_merge_gainadjust(path, currentRunList, keep_col=[    'qdc_lg_ch1', 
                                                                                            'tof_ch2', 
                                                                                            'tof_ch3', 
                                                                                            'tof_ch4', 
                                                                                            'tof_ch5', 
                                                                                            'amplitude_ch2', 
                                                                                            'amplitude_ch3', 
                                                                                            'amplitude_ch4', 
                                                                                            'amplitude_ch5', 
                                                                                            'qdc_lg_ch2',
                                                                                            'qdc_lg_ch3',
                                                                                            'qdc_lg_ch4',
                                                                                            'qdc_lg_ch5'], 
                                                                                            full=False,
                                                                                            gainOffsets=currentGainOffsets,
                                                                                            gainOffsetCol = 'qdc_lg_ch1')
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    #      APPLY ATTENUATION COEFF. BEFORE BINNING
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    PuBe_tof_data.qdc_lg_ch1 = PuBe_tof_data.qdc_lg_ch1*att_factor

    #~~~~~~~~~~~~~~~~~~~~~~~~
    #      RESET INDEX
    #~~~~~~~~~~~~~~~~~~~~~~~~
    PuBe_tof_data = PuBe_tof_data.reset_index(drop=True)

    #~~~~~~~~~~~~~~~~~~~~~~~~
    #    CALIBRATING ToF
    #~~~~~~~~~~~~~~~~~~~~~~~~
    prodata.tof_calibration(PuBe_tof_data, flashrange, distance, phcut=125, calUnit=1e-9, energyCal=True)

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    #   Get random QDC LG
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    #get random data
    y2_random_QDC_TMP = PuBe_tof_data.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch2>{y2ThrQDC} and tof_ch2>{randTOFRange[0]} and tof_ch2<={randTOFRange[1]}').qdc_lg_ch1
    y3_random_QDC_TMP = PuBe_tof_data.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch3>{y3ThrQDC} and tof_ch3>{randTOFRange[0]} and tof_ch3<={randTOFRange[1]}').qdc_lg_ch1
    y4_random_QDC_TMP = PuBe_tof_data.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch4>{y4ThrQDC} and tof_ch4>{randTOFRange[0]} and tof_ch4<={randTOFRange[1]}').qdc_lg_ch1
    y5_random_QDC_TMP = PuBe_tof_data.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch5>{y5ThrQDC} and tof_ch5>{randTOFRange[0]} and tof_ch5<={randTOFRange[1]}').qdc_lg_ch1
    #check for random multiplicity
    mpIdxRandom = prodata.uniqueEventHelper(y2_random_QDC_TMP, y3_random_QDC_TMP, y4_random_QDC_TMP, y5_random_QDC_TMP)

    #Merge Random data
    random_QDC_FIN = pd.concat([random_QDC_FIN,
                                y2_random_QDC_TMP.drop(mpIdxRandom, errors='ignore'),
                                y3_random_QDC_TMP.drop(mpIdxRandom, errors='ignore'), 
                                y4_random_QDC_TMP.drop(mpIdxRandom, errors='ignore'), 
                                y5_random_QDC_TMP.drop(mpIdxRandom, errors='ignore')])
    
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    #   SLICE Neutron ToF ENERGY for QDC LG
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    y2_neutron_QDC_TMP, y3_neutron_QDC_TMP, y4_neutron_QDC_TMP, y5_neutron_QDC_TMP =    prodata.tofEnergySlicer(PuBe_tof_data, 
                                                                                                                minE = minE/1000, 
                                                                                                                maxE = maxE/1000, 
                                                                                                                dE = dE/1000, 
                                                                                                                colKeep = 'qdc_lg_ch1', 
                                                                                                                n_thr = nThrQDC, 
                                                                                                                y_thr = [y2ThrQDC, y3ThrQDC, y4ThrQDC, y5ThrQDC]
                                                                                                                )

    # Merge QDC data
    neutron_QDC_FIN = pd.concat([   neutron_QDC_FIN,
                                    y2_neutron_QDC_TMP,
                                    y3_neutron_QDC_TMP, 
                                    y4_neutron_QDC_TMP, 
                                    y5_neutron_QDC_TMP])

    print(f'> neutrons: {len(neutron_QDC_FIN)} events')
    print(f'> random: {len(random_QDC_FIN)} events')


#calculate HH, TP and FD positions
pathData = '/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper'
pathSim =  '/media/gheed/Seagate_Expansion_Drive1/data/simulation/'
plot = True
fitError = True
gate = 'LG'

#SAVE SLICED DATA TO DISK
# neutron_QDC_FIN.to_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/errors/sys_error/neutron_QDC_FIN_{gate}_nominal.pkl')
# random_QDC_FIN.to_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/errors/sys_error/random_QDC_FIN_{gate}_nominal.pkl')

# neutron_QDC_FIN.to_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/errors/sys_error/neutron_QDC_FIN_{gate}_plus_5mm.pkl')
# random_QDC_FIN.to_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/errors/sys_error/random_QDC_FIN_{gate}_plus_5mm.pkl')

neutron_QDC_FIN.to_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/errors/sys_error/neutron_QDC_FIN_{gate}_minus_5mm.pkl')
random_QDC_FIN.to_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/errors/sys_error/random_QDC_FIN_{gate}_minus_5mm.pkl')



#########################################################
#########################################################
#########################################################
#########################################################




#Load sliced data
detector = 'NE213A'
# neutron_QDC_FIN = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/errors/sys_error/neutron_QDC_FIN_{gate}_nominal.pkl')
# random_QDC_FIN = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/errors/sys_error/random_QDC_FIN_{gate}_nominal.pkl')

# neutron_QDC_FIN = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/errors/sys_error/neutron_QDC_FIN_{gate}_plus_5mm.pkl')
# random_QDC_FIN = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/errors/sys_error/random_QDC_FIN_{gate}_plus_5mm.pkl')

neutron_QDC_FIN = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/errors/sys_error/neutron_QDC_FIN_{gate}_minus_5mm.pkl')
random_QDC_FIN = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/errors/sys_error/random_QDC_FIN_{gate}_minus_5mm.pkl')

# #NE213A
numBinsDataLG = [np.arange(0.06, 4, 0.0200),#2000 keV
                np.arange(0.07, 4, 0.0225),#2250 keV
                np.arange(0.08, 4, 0.0250),#2500 keV
                np.arange(0.10, 4, 0.0275),#2750 keV
                np.arange(0.12, 4, 0.0300),#3000 keV
                np.arange(0.13, 4, 0.0325),#3250 keV
                np.arange(0.14, 4, 0.0350),#3500 keV
                np.arange(0.16, 4, 0.0375),#3750 keV
                np.arange(0.17, 4, 0.0400),#4000 keV
                np.arange(0.19, 4, 0.0425),#4250 keV
                np.arange(0.20, 4, 0.0450),#4500 keV
                np.arange(0.21, 4, 0.0475),#4750 keV
                np.arange(0.23, 4, 0.0500),#5000 keV
                np.arange(0.24, 4, 0.0525),#5250 keV
                np.arange(0.26, 4, 0.0550),#5500 keV
                np.arange(0.27, 4, 0.0575),#5750 keV
                np.arange(0.29, 4, 0.0600),#6000 keV
                np.arange(0.30, 4, 0.0625)]#6250 keV

fitRangeDataLG = [[0.14, 0.70], #2000
                [0.15, 0.80], #2250
                [0.16, 0.90], #2500
                [0.58, 1.50], #2750
                [0.65, 1.55], #3000
                [0.75, 1.80], #3250
                [0.94, 2.10], #3500
                [1.06, 2.20], #3750
                [1.15, 2.50], #4000
                [1.30, 2.60], #4250
                [1.35, 2.70], #4500
                [1.50, 3.00], #4750
                [1.65, 3.10], #5000
                [1.77, 3.20], #5250
                [1.96, 3.30], #5500
                [1.98, 3.50], #5750
                [2.20, 3.60], #6000
                [2.20, 3.60]] #6250

#EJ305
# numBinsDataLG = [np.arange(0.06, 4, 0.0200),#2000 keV
#                 np.arange(0.07, 4, 0.0225),#2250 keV
#                 np.arange(0.08, 4, 0.0250),#2500 keV
#                 np.arange(0.10, 4, 0.0275),#2750 keV
#                 np.arange(0.12, 4, 0.0300),#3000 keV
#                 np.arange(0.13, 4, 0.0325),#3250 keV
#                 np.arange(0.14, 4, 0.0350),#3500 keV
#                 np.arange(0.16, 4, 0.0375),#3750 keV
#                 np.arange(0.17, 4, 0.0400),#4000 keV
#                 np.arange(0.19, 4, 0.0425),#4250 keV
#                 np.arange(0.20, 4, 0.0450),#4500 keV
#                 np.arange(0.21, 4, 0.0575),#4750 keV
#                 np.arange(0.23, 4, 0.0600),#5000 keV
#                 np.arange(0.24, 4, 0.0625),#5250 keV
#                 np.arange(0.26, 4, 0.0650),#5500 keV
#                 np.arange(0.27, 4, 0.0675),#5750 keV
#                 np.arange(0.29, 4, 0.0700),#6000 keV
#                 np.arange(0.30, 4, 0.0725)]#6250 keV

# fitRangeDataLG = [[0.22, 0.65], #2000
#                 [0.18, 0.75], #2250
#                 [0.30, 0.85], #2500
#                 [0.35, 1.20], #2750
#                 [0.33, 1.40], #3000
#                 [0.44, 1.50], #3250
#                 [0.46, 1.60], #3500
#                 [0.59, 1.70], #3750
#                 [0.65, 1.90], #4000
#                 [0.74, 2.00], #4250
#                 [0.90, 2.20], #4500
#                 [0.95, 2.26], #4750
#                 [1.07, 2.50], #5000
#                 [1.15, 2.60], #5250
#                 [1.26, 2.65], #5500
#                 [1.36, 2.75], #5750
#                 [1.46, 2.90]] #6000

# EJ321P
# numBinsDataLG =  [np.arange(0.06, 4, 0.0100),#2000 keV
#                 np.arange(0.07, 4, 0.0128),#2250 keV
#                 np.arange(0.08, 4, 0.0156),#2500 keV
#                 np.arange(0.10, 4, 0.0184),#2750 keV
#                 np.arange(0.12, 4, 0.0212),#3000 keV
#                 np.arange(0.13, 4, 0.0241),#3250 keV
#                 np.arange(0.14, 4, 0.0269),#3500 keV
#                 np.arange(0.16, 4, 0.0297),#3750 keV
#                 np.arange(0.17, 4, 0.0325),#4000 keV
#                 np.arange(0.19, 4, 0.0353),#4250 keV
#                 np.arange(0.20, 4, 0.0381),#4500 keV
#                 np.arange(0.21, 4, 0.0409),#4750 keV
#                 np.arange(0.23, 4, 0.0437),#5000 keV
#                 np.arange(0.24, 4, 0.0466),#5250 keV
#                 np.arange(0.26, 4, 0.0494),#5500 keV
#                 np.arange(0.27, 4, 0.0521),#5750 keV
#                 np.arange(0.29, 4, 0.0550)]#6000 keV]#6250 keV

# fitRangeDataLG = [[0.28, 0.45], #2000
#                 [0.28, 0.45], #2250
#                 [0.28, 0.50], #2500
#                 [0.33, 0.65], #2750
#                 [0.36, 0.80], #3000
#                 [0.39, 0.90], #3250
#                 [0.42, 1.00], #3500
#                 [0.44, 1.10], #3750
#                 [0.48, 1.20], #4000
#                 [0.58, 1.30], #4250
#                 [0.62, 1.45], #4500
#                 [0.68, 1.55], #4750
#                 [0.74, 1.60], #5000
#                 [0.94, 1.70], #5250
#                 [0.99, 2.00], #5500
#                 [0.98, 2.10], #5750
#                 [1.10, 2.30]] #6000

#EJ331
# numBinsDataLG = [np.arange(0.06, 4, 0.0200),#2000 keV
#                 np.arange(0.07, 4, 0.0225),#2250 keV
#                 np.arange(0.08, 4, 0.0250),#2500 keV
#                 np.arange(0.10, 4, 0.0275),#2750 keV
#                 np.arange(0.12, 4, 0.0300),#3000 keV
#                 np.arange(0.13, 4, 0.0325),#3250 keV
#                 np.arange(0.14, 4, 0.0350),#3500 keV
#                 np.arange(0.16, 4, 0.0375),#3750 keV
#                 np.arange(0.17, 4, 0.0400),#4000 keV
#                 np.arange(0.19, 4, 0.0425),#4250 keV
#                 np.arange(0.20, 4, 0.0450),#4500 keV
#                 np.arange(0.21, 4, 0.0475),#4750 keV
#                 np.arange(0.23, 4, 0.0500),#5000 keV
#                 np.arange(0.24, 4, 0.0525),#5250 keV
#                 np.arange(0.26, 4, 0.0550),#5500 keV
#                 np.arange(0.27, 4, 0.0575),#5750 keV
#                 np.arange(0.29, 4, 0.0600),#6000 keV
#                 np.arange(0.30, 4, 0.0625)]#6250 keV

# fitRangeDataLG = [[0.22, 0.65], #2000
#                 [0.18, 0.75], #2250
#                 [0.30, 0.85], #2500
#                 [0.35, 1.20], #2750
#                 [0.33, 1.40], #3000
#                 [0.44, 1.50], #3250
#                 [0.46, 1.60], #3500
#                 [0.59, 1.70], #3750
#                 [0.65, 2.00], #4000
#                 [0.74, 2.10], #4250
#                 [0.84, 2.50], #4500
#                 [0.85, 2.70], #4750
#                 [1.03, 2.90], #5000
#                 [1.38, 3.00], #5250
#                 [1.40, 3.10], #5500
#                 [1.50, 3.20], #5750
#                 [1.60, 3.20], #6000
#                 [1.65, 3.20]] #6250

HH_data = pd.DataFrame()
TP_data = pd.DataFrame()
FD_data = pd.DataFrame()

for i, E in enumerate(np.arange(2000, 6250, 250)):
    print(f'Energy = {E}')
    nQDC = neutron_QDC_FIN[f'keV{E}']
    randQDC = random_QDC_FIN
    
    nQDC = prodata.calibrateMyDetector(f'{pathData}/{detector}/Ecal/popt_{gate}.npy', nQDC)
    randQDC = prodata.calibrateMyDetector(f'{pathData}/{detector}/Ecal/popt_{gate}.npy', randQDC)

    x_neutron_QDC, y_neutron_QDC = prodata.randomTOFSubtractionQDC( nQDC,
                                                            randQDC,
                                                            nLength[f'keV{E}'].item(), 
                                                            randLength,
                                                            numBinsDataLG[i], 
                                                            plot=False)

    #calculate HH values for data
    HHres_data = prodata.halfHeight(x_neutron_QDC, y_neutron_QDC, fitRangeDataLG[i][0], fitRangeDataLG[i][1], y_error=fitError, plot=False)
    HHres_data['energy'] = E/1000 #add current energy in MeV

    #calculate TP values
    TPres_data = prodata.turningPoint(x_neutron_QDC, y_neutron_QDC, fitStart=fitRangeDataLG[i][0], fitStop=fitRangeDataLG[i][1], relErrSystematic=0, y_error=fitError, plot=False)
    TPres_data['energy'] = E/1000 #add current energy in MeV

    #calculate FD values
    FDres_data = prodata.firstDerivative(x_neutron_QDC, y_neutron_QDC, fitRangeDataLG[i][0], 5, 0, plot=False)
    FDres_data['energy'] = E/1000 #add current energy in MeV
    
    HH_data = pd.concat([HH_data, HHres_data])
    TP_data = pd.concat([TP_data, TPres_data])
    FD_data = pd.concat([FD_data, FDres_data])









##################################
# PROCESS ERROR AND SAVE TO DISK #
##################################
# NOMINAL!
# detector = 'EJ331'

# HH_data1 = HH_data 
# TP_data1 = TP_data
# FD_data1 = FD_data
# HH_data1.to_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/errors/sys_error/HH_data_{gate}_nominal.pkl')
# TP_data1.to_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/errors/sys_error/TP_data_{gate}_nominal.pkl')
# FD_data1.to_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/errors/sys_error/FD_data_{gate}_nominal.pkl')

HH_data1 = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/errors/sys_error/HH_data_{gate}_nominal.pkl')
TP_data1 = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/errors/sys_error/TP_data_{gate}_nominal.pkl')
FD_data1 = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/errors/sys_error/FD_data_{gate}_nominal.pkl')

# ADDING!
# HH_data3 = HH_data 
# TP_data3 = TP_data
# FD_data3 = FD_data
# HH_data3.to_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/errors/sys_error/HH_data_{gate}_plus_5mm.pkl')
# TP_data3.to_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/errors/sys_error/TP_data_{gate}_plus_5mm.pkl')
# FD_data3.to_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/errors/sys_error/FD_data_{gate}_plus_5mm.pkl')


HH_data3 = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/errors/sys_error/HH_data_{gate}_plus_5mm.pkl')
TP_data3 = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/errors/sys_error/TP_data_{gate}_plus_5mm.pkl')
FD_data3 = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/errors/sys_error/FD_data_{gate}_plus_5mm.pkl')


# SUBTRACTING!
# HH_data2 = HH_data 
# TP_data2 = TP_data
# FD_data2 = FD_data
# HH_data2.to_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/errors/sys_error/HH_data_{gate}_minus_5mm.pkl')
# TP_data2.to_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/errors/sys_error/TP_data_{gate}_minus_5mm.pkl')
# FD_data2.to_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/errors/sys_error/FD_data_{gate}_minus_5mm.pkl')

HH_data2 = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/errors/sys_error/HH_data_{gate}_minus_5mm.pkl')
TP_data2 = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/errors/sys_error/TP_data_{gate}_minus_5mm.pkl')
FD_data2 = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/errors/sys_error/FD_data_{gate}_minus_5mm.pkl')




#Calculate the relative differnces for each data set
HH_errSub = np.abs(HH_data1.HH_loc.array - HH_data2.HH_loc.array) / HH_data1.HH_loc.array
HH_errAdd = np.abs(HH_data1.HH_loc.array - HH_data3.HH_loc.array) / HH_data1.HH_loc.array

TP_errSub = np.abs(TP_data1.TP_loc.array - TP_data2.TP_loc.array) / TP_data1.TP_loc.array
TP_errAdd = np.abs(TP_data1.TP_loc.array - TP_data3.TP_loc.array) / TP_data1.TP_loc.array

FD_errSub = np.abs(FD_data1.FD_loc.array - FD_data2.FD_loc.array) / FD_data1.FD_loc.array
FD_errAdd = np.abs(FD_data1.FD_loc.array - FD_data3.FD_loc.array) / FD_data1.FD_loc.array

#Average the two values for each energy bin and each method separate.
HH_error = np.zeros(len(HH_errSub))
TP_error = np.zeros(len(TP_errSub))
FD_error = np.zeros(len(FD_errSub))
avg_error = np.zeros(len(FD_errSub)) #Total average across all methods

for i in range(len(HH_errSub)): 
    HH_error[i] = np.average([HH_errSub[i], HH_errAdd[i]])
    TP_error[i] = np.average([TP_errSub[i], TP_errAdd[i]])
    FD_error[i] = np.average([FD_errSub[i], FD_errAdd[i]])
    avg_error[i] = np.average([HH_error[i], TP_error[i], FD_error[i]])

    # print(f'HH - {np.round(HH_errSub[i]*100,3)}, {np.round(HH_errAdd[i]*100,3)} = {np.round(np.average([HH_errSub[i], HH_errAdd[i]])*100,3)}')
    # print(f'TP - {np.round(TP_errSub[i]*100,3)}, {np.round(TP_errAdd[i]*100,3)} = {np.round(np.average([TP_errSub[i], TP_errAdd[i]])*100,3)}')
    # print(f'FD - {np.round(FD_errSub[i]*100,3)}, {np.round(FD_errAdd[i]*100,3)} = {np.round(np.average([FD_errSub[i], FD_errAdd[i]])*100,3)}')
    # print('----------------------')


print(np.average([HH_errSub, HH_errAdd]))
print(np.average([TP_errSub, TP_errAdd]))
print(np.average([FD_errSub, FD_errAdd]))

print(f'Total average: {np.average([np.average([HH_errSub, HH_errAdd]), np.average([TP_errSub, TP_errAdd]), np.average([FD_errSub, FD_errAdd])])*100}%')

plt.subplot(3,1,1)
plt.title('HH')
plt.scatter(HH_data1.energy, HH_data2.HH_loc.array, label='-5mm')
plt.scatter(HH_data1.energy, HH_data3.HH_loc.array, label='+5mm')
plt.scatter(HH_data1.energy, HH_data1.HH_loc.array, label='nominal')
# plt.ylim([-10,10])
plt.legend()

plt.subplot(3,1,2)
plt.title('TP')
plt.scatter(TP_data1.energy, TP_data2.TP_loc.array, label='-5mm')
plt.scatter(TP_data1.energy, TP_data3.TP_loc.array, label='+5mm')
plt.scatter(TP_data1.energy, TP_data1.TP_loc.array, label='nominal')
# plt.ylim([-10,10])
plt.ylabel('Light Yield')
plt.legend()

plt.subplot(3,1,3)
plt.title('FD')
plt.scatter(FD_data1.energy, FD_data2.FD_loc.array, label='-5mm')
plt.scatter(FD_data1.energy, FD_data3.FD_loc.array, label='+5mm')
plt.scatter(FD_data1.energy, FD_data1.FD_loc.array, label='nominal')
# plt.ylim([-10,10])
plt.legend()

plt.xlabel('Neutron Energy [MeV]')

plt.show()


#Save to disk!
np.save(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/errors/sys_error/HH_sys_error_{gate}_5mm', HH_error)
np.save(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/errors/sys_error/TP_sys_error_{gate}_5mm', TP_error)
np.save(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/errors/sys_error/FD_sys_error_{gate}_5mm', FD_error)






plt.subplot(3,1,1)
plt.title('HH')
plt.scatter(HH_data1.energy, (HH_errSub)*100, label='-10mm')
plt.scatter(HH_data1.energy, (HH_errAdd)*100, label='+10mm')
plt.ylim([-10,10])
plt.legend()

plt.subplot(3,1,2)
plt.title('TP')
plt.scatter(TP_data1.energy, (TP_errSub)*100, label='-10mm')
plt.scatter(TP_data1.energy, (TP_errAdd)*100, label='+10mm')
plt.ylim([-10,10])
plt.ylabel('Difference from nominal[%]')
plt.legend()

plt.subplot(3,1,3)
plt.title('FD')
plt.scatter(FD_data1.energy, (FD_errSub)*100, label='-10mm')
plt.scatter(FD_data1.energy, (FD_errAdd)*100, label='+10mm')
plt.ylim([-10,10])
plt.legend()

plt.xlabel('Neutron Energy [MeV]')

plt.show()





#      ██ ██    ██ ██      ██ ██    ██ ███████     ███████ ██████  ██████   ██████  ██████  ██████   █████  ██████  ███████ 
#      ██ ██    ██ ██      ██ ██    ██ ██          ██      ██   ██ ██   ██ ██    ██ ██   ██ ██   ██ ██   ██ ██   ██ ██      
#      ██ ██    ██ ██      ██ ██    ██ ███████     █████   ██████  ██████  ██    ██ ██████  ██████  ███████ ██████  ███████ 
# ██   ██ ██    ██ ██      ██ ██    ██      ██     ██      ██   ██ ██   ██ ██    ██ ██   ██ ██   ██ ██   ██ ██   ██      ██ 
#  █████   ██████  ███████ ██  ██████  ███████     ███████ ██   ██ ██   ██  ██████  ██   ██ ██████  ██   ██ ██   ██ ███████ 
                                                                                                                          
                                                                                                                          
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#    FIND ERRORBARS FOR JULIUS DATA
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
relErr = 0.1 #relative error to use

#load data
Julius_E = np.array([2, 2.2, 2.4, 2.6, 2.8, 3.0, 3.2, 3.4, 3.6, 3.8, 4.0, 4.2, 4.4, 4.6, 4.8, 5.0, 5.2, 5.4, 5.6, 5.8])

Julius_HH_LG_LY = np.array([0.655151539173894, 0.7150465373541994, 0.7918484860496036, 0.9024066820650116, 0.987647692590417, 1.115084012265827, 1.2171887330236846, 1.2855516198890884, 1.4298516497969507, 1.607907927024816, 1.7015879993802225, 1.769936472818077, 1.9142365027259391, 2.092321606808903, 2.2197435130567635, 2.338740770902172, 2.483040800810034, 2.517647440355435, 2.7041571928408503, 2.8568962845787134])
Julius_HH_SG_LY = np.array([0.4948093644038787, 0.571596899671734, 0.6146281876195858, 0.7251863836349943, 0.7935348570728489, 0.8703368057682539, 1.0483930829961192, 1.116755969861523, 1.2273141658769307, 1.3294188866347887, 1.4146454837326452, 1.4998864942580505, 1.6273228139334603, 1.6788075771388624, 1.7809122978967202, 1.8745923702521268, 1.9091845963699778, 1.9606693595753804, 2.1471791120607957, 2.384308822098667])

Julius_TP_LG_LY = np.array([0.6886097574637304, 0.7444127037472685, 0.7752618607516935, 0.8977202711028949, 0.9535652271326942, 1.1093093597714665, 1.2068139808435552, 1.2876127261524668, 1.4267770122668453, 1.5576093653727658, 1.7217274407662573, 1.7359127317537661, 1.9083627401557155, 2.039195093261636, 2.153405590096902, 2.309149722735674, 2.398280401053044, 2.570772419201254, 2.643281241247969, 2.8323951156668343])
Julius_TP_SG_LY = np.array([0.49693328852293694,0.5278244552736227, 0.6002912675740766, 0.6560942138576138, 0.745224892174984, 0.8677253122724466, 1.0234694449112185, 1.0709824679325597, 1.2101047443006774, 1.2409539013051019, 1.346748445639388, 1.4276312104408218, 1.5250938217666492, 1.5809387777964483, 1.686775331876995, 1.7758640004481034, 1.8067551671987896, 1.8542261804738693, 2.0516719879011927, 2.2991514031255247])

Julius_FD_LG_LY = np.array([0.5813888888888892, 0.6847222222222222, 0.7632638888888894, 0.8334027777777782, 0.9450000000000003, 1.0152083333333337, 1.085555555555556, 1.1886805555555555, 1.4159722222222224, 1.4696527777777784, 1.54, 1.6928472222222224, 1.8126388888888891, 1.9242361111111113, 2.0605555555555557, 2.188541666666667, 2.325, 2.37875, 2.5233333333333334, 2.701041666666667])
Julius_FD_SG_LY = np.array([0.5070138888888889, 0.5525000000000002, 0.6227777777777779, 0.6515972222222222, 0.7962500000000001, 0.8664583333333331, 0.9532638888888889, 1.0068055555555562, 1.1350000000000002, 1.2713888888888891, 1.325138888888889, 1.3870138888888892, 1.523402777777778, 1.618541666666667, 1.680416666666667, 1.8580555555555558, 1.8292361111111115, 1.9820833333333334, 2.0687500000000005, 2.3125694444444447])

#Fit data
popt_HH_Julius_LG, pcov_HH_Julius_LG = curve_fit(promath.cecilEq_NE213, Julius_E, Julius_HH_LG_LY)
popt_HH_Julius_SG, pcov_HH_Julius_SG = curve_fit(promath.cecilEq_NE213, Julius_E, Julius_HH_SG_LY)

popt_TP_Julius_LG, pcov_TP_Julius_LG = curve_fit(promath.cecilEq_NE213, Julius_E, Julius_TP_LG_LY)
popt_TP_Julius_SG, pcov_TP_Julius_SG = curve_fit(promath.cecilEq_NE213, Julius_E, Julius_TP_SG_LY)

popt_FD_Julius_LG, pcov_FD_Julius_LG = curve_fit(promath.cecilEq_NE213, Julius_E, Julius_FD_LG_LY)
popt_FD_Julius_SG, pcov_FD_Julius_SG = curve_fit(promath.cecilEq_NE213, Julius_E, Julius_FD_SG_LY)





#loop over errors to find optimal
for relErr in np.arange(0.01, 0.05, 0.0001): 
    #NOTE: Optimla results obtained at 3.17 % error
    popt_HH_Julius_LG_k, pcov_HH_Julius_LG_k = curve_fit(promath.kornilovEq_NE213, Julius_E, Julius_HH_LG_LY, sigma=Julius_HH_LG_LY*relErr, absolute_sigma=False)
    popt_HH_Julius_SG_k, pcov_HH_Julius_SG_k = curve_fit(promath.kornilovEq_NE213, Julius_E, Julius_HH_SG_LY, sigma=Julius_HH_SG_LY*relErr, absolute_sigma=False)
    
    popt_TP_Julius_LG_k, pcov_TP_Julius_LG_k = curve_fit(promath.kornilovEq_NE213, Julius_E, Julius_TP_LG_LY, sigma=Julius_TP_LG_LY*relErr, absolute_sigma=False)
    popt_TP_Julius_SG_k, pcov_TP_Julius_SG_k = curve_fit(promath.kornilovEq_NE213, Julius_E, Julius_TP_SG_LY, sigma=Julius_TP_SG_LY*relErr, absolute_sigma=False)
    
    popt_FD_Julius_LG_k, pcov_FD_Julius_LG_k = curve_fit(promath.kornilovEq_NE213, Julius_E, Julius_FD_LG_LY, sigma=Julius_FD_LG_LY*relErr, absolute_sigma=False)
    popt_FD_Julius_SG_k, pcov_FD_Julius_SG_k = curve_fit(promath.kornilovEq_NE213, Julius_E, Julius_FD_SG_LY, sigma=Julius_FD_SG_LY*relErr, absolute_sigma=False)
    
    popt_HH_Julius_LG_c, pcov_HH_Julius_LG_c = curve_fit(promath.cecilEq_NE213, Julius_E, Julius_HH_LG_LY, sigma=Julius_HH_LG_LY*relErr, absolute_sigma=False)
    popt_HH_Julius_SG_c, pcov_HH_Julius_SG_c = curve_fit(promath.cecilEq_NE213, Julius_E, Julius_HH_SG_LY, sigma=Julius_HH_SG_LY*relErr, absolute_sigma=False)
    
    popt_TP_Julius_LG_c, pcov_TP_Julius_LG_c = curve_fit(promath.cecilEq_NE213, Julius_E, Julius_TP_LG_LY, sigma=Julius_TP_LG_LY*relErr, absolute_sigma=False)
    popt_TP_Julius_SG_c, pcov_TP_Julius_SG_c = curve_fit(promath.cecilEq_NE213, Julius_E, Julius_TP_SG_LY, sigma=Julius_TP_SG_LY*relErr, absolute_sigma=False)
    
    popt_FD_Julius_LG_c, pcov_FD_Julius_LG_c = curve_fit(promath.cecilEq_NE213, Julius_E, Julius_FD_LG_LY, sigma=Julius_FD_LG_LY*relErr, absolute_sigma=False)
    popt_FD_Julius_SG_c, pcov_FD_Julius_SG_c = curve_fit(promath.cecilEq_NE213, Julius_E, Julius_FD_SG_LY, sigma=Julius_FD_SG_LY*relErr, absolute_sigma=False)
    
    chi2_HH_LG_k = promath.chi2red(Julius_HH_LG_LY, promath.kornilovEq_NE213(Julius_E, popt_HH_Julius_LG_k[0]), Julius_HH_LG_LY*relErr, 1)
    chi2_HH_SG_k = promath.chi2red(Julius_HH_SG_LY, promath.kornilovEq_NE213(Julius_E, popt_HH_Julius_SG_k[0]), Julius_HH_SG_LY*relErr, 1)

    chi2_TP_LG_k = promath.chi2red(Julius_TP_LG_LY, promath.kornilovEq_NE213(Julius_E, popt_TP_Julius_LG_k[0]), Julius_TP_LG_LY*relErr, 1)
    chi2_TP_SG_k = promath.chi2red(Julius_TP_SG_LY, promath.kornilovEq_NE213(Julius_E, popt_TP_Julius_SG_k[0]), Julius_TP_SG_LY*relErr, 1)

    chi2_FD_LG_k = promath.chi2red(Julius_FD_LG_LY, promath.kornilovEq_NE213(Julius_E, popt_FD_Julius_LG_k[0]), Julius_FD_LG_LY*relErr, 1)
    chi2_FD_SG_k = promath.chi2red(Julius_FD_SG_LY, promath.kornilovEq_NE213(Julius_E, popt_FD_Julius_SG_k[0]), Julius_FD_SG_LY*relErr, 1)
    
    chi2_HH_LG_c = promath.chi2red(Julius_HH_LG_LY, promath.cecilEq_NE213(Julius_E, popt_HH_Julius_LG_c[0]), Julius_HH_LG_LY*relErr, 1)
    chi2_HH_SG_c = promath.chi2red(Julius_HH_SG_LY, promath.cecilEq_NE213(Julius_E, popt_HH_Julius_SG_c[0]), Julius_HH_SG_LY*relErr, 1)

    chi2_TP_LG_c = promath.chi2red(Julius_TP_LG_LY, promath.cecilEq_NE213(Julius_E, popt_TP_Julius_LG_c[0]), Julius_TP_LG_LY*relErr, 1)
    chi2_TP_SG_c = promath.chi2red(Julius_TP_SG_LY, promath.cecilEq_NE213(Julius_E, popt_TP_Julius_SG_c[0]), Julius_TP_SG_LY*relErr, 1)

    chi2_FD_LG_c = promath.chi2red(Julius_FD_LG_LY, promath.cecilEq_NE213(Julius_E, popt_FD_Julius_LG_c[0]), Julius_FD_LG_LY*relErr, 1)
    chi2_FD_SG_c = promath.chi2red(Julius_FD_SG_LY, promath.cecilEq_NE213(Julius_E, popt_FD_Julius_SG_c[0]), Julius_FD_SG_LY*relErr, 1)

    # if chi2_FD_SG_c<0.82:
    #     print(f'Best at {relErr*100}%')
    #     break
    print(f'- RelErr = {round(relErr*100,3)}% -------------------------')
    print(f'HH:LG: L_0 error = {np.round(np.sqrt(np.diag(pcov_HH_Julius_LG_k))[0], 3)}, Chi2/dof = {np.round(chi2_HH_LG_k, 2)}, C_error = {np.round(np.sqrt(np.diag(pcov_HH_Julius_LG_c))[0], 3)}, Chi2/dof = {np.round(chi2_HH_LG_c, 2)}')
    print(f'HH:SG: L_0 error = {np.round(np.sqrt(np.diag(pcov_HH_Julius_SG_k))[0], 3)}, Chi2/dof = {np.round(chi2_HH_SG_k, 2)}, C_error = {np.round(np.sqrt(np.diag(pcov_HH_Julius_SG_c))[0], 3)}, Chi2/dof = {np.round(chi2_HH_SG_c, 2)}')

    print(f'TP:LG: L_0 error = {np.round(np.sqrt(np.diag(pcov_TP_Julius_LG_k))[0], 3)}, Chi2/dof = {np.round(chi2_TP_LG_k, 2)}, C_error = {np.round(np.sqrt(np.diag(pcov_TP_Julius_LG_c))[0], 3)}, Chi2/dof = {np.round(chi2_TP_LG_c, 2)}')
    print(f'TP:SG: L_0 error = {np.round(np.sqrt(np.diag(pcov_TP_Julius_SG_k))[0], 3)}, Chi2/dof = {np.round(chi2_TP_SG_k, 2)}, C_error = {np.round(np.sqrt(np.diag(pcov_TP_Julius_SG_c))[0], 3)}, Chi2/dof = {np.round(chi2_TP_SG_c, 2)}')

    print(f'FD:LG: L_0 error = {np.round(np.sqrt(np.diag(pcov_FD_Julius_LG_k))[0], 3)}, Chi2/dof = {np.round(chi2_FD_LG_k, 2)}, C_error = {np.round(np.sqrt(np.diag(pcov_FD_Julius_LG_c))[0], 3)}, Chi2/dof = {np.round(chi2_FD_LG_c, 2)}')
    print(f'FD:SG: L_0 error = {np.round(np.sqrt(np.diag(pcov_FD_Julius_SG_k))[0], 3)}, Chi2/dof = {np.round(chi2_FD_SG_k, 2)}, C_error = {np.round(np.sqrt(np.diag(pcov_FD_Julius_SG_c))[0], 3)}, Chi2/dof = {np.round(chi2_FD_SG_c, 2)}')






# ███████ ██ ███    ███        ███████ ██████  ██████   ██████  ██████  
# ██      ██ ████  ████        ██      ██   ██ ██   ██ ██    ██ ██   ██ 
# ███████ ██ ██ ████ ██        █████   ██████  ██████  ██    ██ ██████  
#      ██ ██ ██  ██  ██        ██      ██   ██ ██   ██ ██    ██ ██   ██ 
# ███████ ██ ██      ██ ██     ███████ ██   ██ ██   ██  ██████  ██   ██ 
                                                                      
# detector = 'NE213A'
# sim_birks_error =    np.load(f'{pathData}/{detector}/birks/total_error.npy')
# sim_LY_error =       pd.read_pickle(f'{pathData}/{detector}/TOF_slice/simulation/sim_LightYield.pkl')

# #calculate error for each energy bin
# sim_total_error = np.sqrt((sim_LY_error['means_err']/sim_LY_error['means'])**2 + sim_birks_error**2)

# #save to disk
# np.save(f'{pathData}/{detector}/errors/simulation_errors.npy', sim_total_error)

# print(detector)
# print(sim_total_error)
# ####################################

# detector = 'EJ305'
# sim_birks_error =    np.load(f'{pathData}/{detector}/birks/total_error.npy')
# sim_LY_error =       pd.read_pickle(f'{pathData}/{detector}/TOF_slice/simulation/sim_LightYield.pkl')

# #calculate error for each energy bin
# sim_total_error = np.sqrt((sim_LY_error['means_err']/sim_LY_error['means'])**2 + sim_birks_error**2)

# #save to disk
# np.save(f'{pathData}/{detector}/errors/simulation_errors.npy', sim_total_error)

# print(detector)
# print(sim_total_error)
# ####################################

# detector = 'EJ321P'
# sim_birks_error =    np.load(f'{pathData}/{detector}/birks/total_error.npy')
# sim_LY_error =       pd.read_pickle(f'{pathData}/{detector}/TOF_slice/simulation/sim_LightYield.pkl')

# #calculate error for each energy bin
# sim_total_error = np.sqrt((sim_LY_error['means_err']/sim_LY_error['means'])**2 + sim_birks_error**2)

# #save to disk
# np.save(f'{pathData}/{detector}/errors/simulation_errors.npy', sim_total_error)

# print(detector)
# print(sim_total_error)
# ####################################

# detector = 'EJ331'
# sim_birks_error =    np.load(f'{pathData}/{detector}/birks/total_error.npy')
# sim_LY_error =       pd.read_pickle(f'{pathData}/{detector}/TOF_slice/simulation/sim_LightYield.pkl')

# #calculate error for each energy bin
# sim_total_error = np.sqrt((sim_LY_error['means_err']/sim_LY_error['means'])**2 + sim_birks_error**2)

# #save to disk
# np.save(f'{pathData}/{detector}/errors/simulation_errors.npy', sim_total_error)

# print(detector)
# print(sim_total_error)