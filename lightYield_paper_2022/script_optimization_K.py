#!/usr/bin/env python3
from re import I
import sys
from tkinter import X
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
from numpy.core.arrayprint import SubArrayFormat
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit
from scipy import interpolate

# from matplotlib.colors import LinearSegmentedColormap
"""
Optimization script for cecil equation, fitting all paramters!
"""

# IMPORT FROM LIBRARY DIRECTORY
sys.path.insert(0, "../library/")
sys.path.insert(0, "../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "../")
sys.path.insert(0, "data_analysis/")

import processing_math as promath
import processing_data as prodata
import processing_pd as propd
import processing_simulation as prosim
import figure_style as fs

def cecilEq(Ep, p1, p2, p3, p4):
    """
    Used to convert proton recoil energy (Ep) to electron recoil energy (Ee).
    Returns electron recoil energy Ee
    """
    return p1*Ep-p2*(1-np.exp(-p3*np.power(Ep,p4)))

def cecilEqNE213A(Ep, K):
    """
    Used to convert proton recoil energy (Ep) to electron recoil energy (Ee).
    Returns electron recoil energy Ee
    """
    p1 = 0.645
    p2 = 0.88
    p3 = 0.395
    p4 = 1.88

    return K*(p1*Ep-p2*(1-np.exp(-p3*np.power(Ep,p4))))

def cecilEqEJ305(Ep, K):
    """
    Used to convert proton recoil energy (Ep) to electron recoil energy (Ee).
    Returns electron recoil energy Ee
    """
    p1 = 0.55
    p2 = 0.95
    p3 = 0.25
    p4 = 2.22

    return K*(p1*Ep-p2*(1-np.exp(-p3*np.power(Ep,p4))))


def cecilEqEJ321P(Ep, K):
    """
    Used to convert proton recoil energy (Ep) to electron recoil energy (Ee).
    Returns electron recoil energy Ee
    """
    #values derive from average of HH, TP and FD methods
    p1 = 0.43 #+/- 0.01
    p2 = 0.74 #+/- 0.06
    p3 = 0.23 #+/- 0.07
    p4 = 2.28 #+/- 0.44

    return K*(p1*Ep-p2*(1-np.exp(-p3*np.power(Ep,p4))))

def cecilEqEJ331(Ep, K):
    """
    Used to convert proton recoil energy (Ep) to electron recoil energy (Ee).
    Returns electron recoil energy Ee
    """
    p1 = 0.62
    p2 = 1.35
    p3 = 0.29
    p4 = 1.61

    return K*(p1*Ep-p2*(1-np.exp(-p3*np.power(Ep,p4))))

for detector in ['NE213A', 'EJ305', 'EJ321P', 'EJ331']:

    # detector='NE213A'
    gate = 'LG'
    pathData = '/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper'

    HH_data = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/HH_TP_FD/HH_data_{gate}.pkl')
    TP_data = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/HH_TP_FD/TP_data_{gate}.pkl')
    FD_data = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/HH_TP_FD/FD_data_{gate}.pkl')

    energy = HH_data.energy

    LY_sim = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/simulation/sim_LightYield.pkl')
    SMD_loc = LY_sim.means
    SMD_loc_err = LY_sim.means_err

    HH_loc = HH_data.HH_loc
    TP_loc = TP_data.TP_loc
    FD_loc = FD_data.FD_loc

    HH_loc_err = HH_data.HH_loc_err
    TP_loc_err = TP_data.TP_loc_err
    FD_loc_err = FD_data.FD_loc_err

    # popt_HH, pcov_HH = curve_fit(cecilEq, energy, HH_loc, p0=[0.83, 2.6, 0.25, 1])
    # pcov_HH = np.diag(np.sqrt(pcov_HH))

    # popt_TP, pcov_TP = curve_fit(cecilEq, energy, TP_loc, p0=[0.83, 2.6, 0.25, 1])
    # pcov_TP = np.diag(np.sqrt(pcov_TP))

    if detector == 'NE213A':
        popt_SMD, pcov_SMD = curve_fit(cecilEqNE213A, energy, SMD_loc)
        pcov_SMD = np.diag(np.sqrt(pcov_SMD))

        popt_HH, pcov_HH = curve_fit(cecilEqNE213A, energy, HH_loc)
        pcov_HH = np.diag(np.sqrt(pcov_HH))

        popt_TP, pcov_TP = curve_fit(cecilEqNE213A, energy, TP_loc)
        pcov_TP = np.diag(np.sqrt(pcov_TP))

        popt_FD, pcov_FD = curve_fit(cecilEqNE213A, energy, FD_loc)
        pcov_FD = np.diag(np.sqrt(pcov_FD))

        redChi2_HH = promath.chi2red(HH_loc, cecilEqNE213A(energy, popt_HH[0]), HH_loc_err, 1)
        redChi2_TP = promath.chi2red(TP_loc, cecilEqNE213A(energy, popt_TP[0]), TP_loc_err, 1)
        redChi2_FD = promath.chi2red(FD_loc, cecilEqNE213A(energy, popt_FD[0]), FD_loc_err, 1)    
        redChi2_SMD = promath.chi2red(SMD_loc, cecilEqNE213A(energy, popt_SMD[0]), SMD_loc_err, 1)

        print(f'SMD....K = {np.round(popt_SMD[0],2)}+/-{np.round(pcov_SMD[0],2)}, chi2/dof = {np.round(redChi2_SMD,2)}')
        print(f'HH.....K = {np.round(popt_HH[0],2)}+/-{np.round(pcov_HH[0],2)}, chi2/dof = {np.round(redChi2_HH,2)}')
        print(f'TP.....K = {np.round(popt_TP[0],2)}+/-{np.round(pcov_TP[0],2)}, chi2/dof = {np.round(redChi2_TP,2)}')
        print(f'FD.....K = {np.round(popt_FD[0],2)}+/-{np.round(pcov_FD[0],2)}, chi2/dof = {np.round(redChi2_FD,2)}')
        print('-------------------------------')
        plt.suptitle(detector)
        plt.subplot(3,1,1)
        plt.scatter(energy, SMD_loc, marker='v', s=30, color='black')
        plt.scatter(energy, HH_loc, label='HH',color='blue', s=30)
        plt.errorbar(energy, HH_loc, yerr=HH_loc_err, ls='none',color='blue', lw=0.5)
        plt.plot(energy, cecilEqNE213A(energy, popt_HH[0]),color='blue', lw=0.5)
        plt.legend()

        plt.subplot(3,1,2)
        plt.scatter(energy, SMD_loc, marker='v', s=30, color='black')
        plt.scatter(energy, TP_loc, label='TP',color='red', s=30)
        plt.errorbar(energy, TP_loc, yerr=TP_loc_err, ls='none',color='red', lw=0.5)
        plt.plot(energy, cecilEqNE213A(energy, popt_TP[0]),color='red', lw=0.5)
        plt.legend()

        plt.subplot(3,1,3)
        plt.scatter(energy, SMD_loc, marker='v', s=30, color='black')
        plt.scatter(energy, FD_loc, label='FD',color='green', s=30)
        plt.errorbar(energy, FD_loc, yerr=FD_loc_err, ls='none',color='green', lw=0.5)
        plt.plot(energy, cecilEqNE213A(energy, popt_FD[0]),color='green', lw=0.5)
        plt.legend()
        plt.show()

    if detector == 'EJ305':
        popt_SMD, pcov_SMD = curve_fit(cecilEqEJ305, energy, SMD_loc)
        pcov_SMD = np.diag(np.sqrt(pcov_SMD))

        popt_HH, pcov_HH = curve_fit(cecilEqEJ305, energy, HH_loc)
        pcov_HH = np.diag(np.sqrt(pcov_HH))

        popt_TP, pcov_TP = curve_fit(cecilEqEJ305, energy, TP_loc)
        pcov_TP = np.diag(np.sqrt(pcov_TP))

        popt_FD, pcov_FD = curve_fit(cecilEqEJ305, energy, FD_loc)
        pcov_FD = np.diag(np.sqrt(pcov_FD))

        redChi2_HH = promath.chi2red(HH_loc, cecilEqEJ305(energy, popt_HH[0]), HH_loc_err, 1)
        redChi2_TP = promath.chi2red(TP_loc, cecilEqEJ305(energy, popt_TP[0]), TP_loc_err, 1)
        redChi2_FD = promath.chi2red(FD_loc, cecilEqEJ305(energy, popt_FD[0]), FD_loc_err, 1)    
        redChi2_SMD = promath.chi2red(SMD_loc, cecilEqEJ305(energy, popt_SMD[0]), SMD_loc_err, 1)

        print(f'SMD....K = {np.round(popt_SMD[0],2)}+/-{np.round(pcov_SMD[0],2)}, chi2/dof = {np.round(redChi2_SMD,2)}')
        print(f'HH.....K = {np.round(popt_HH[0],2)}+/-{np.round(pcov_HH[0],2)}, chi2/dof = {np.round(redChi2_HH,2)}')
        print(f'TP.....K = {np.round(popt_TP[0],2)}+/-{np.round(pcov_TP[0],2)}, chi2/dof = {np.round(redChi2_TP,2)}')
        print(f'FD.....K = {np.round(popt_FD[0],2)}+/-{np.round(pcov_FD[0],2)}, chi2/dof = {np.round(redChi2_FD,2)}')
        print('-------------------------------')
        plt.suptitle(detector)
        plt.subplot(3,1,1)
        plt.scatter(energy, SMD_loc, marker='v', s=30, color='black')
        plt.scatter(energy, HH_loc, label='HH',color='blue', s=30)
        plt.errorbar(energy, HH_loc, yerr=HH_loc_err, ls='none',color='blue', lw=0.5)
        plt.plot(energy, cecilEqEJ305(energy, popt_HH[0]),color='blue', lw=0.5)
        plt.legend()

        plt.subplot(3,1,2)
        plt.scatter(energy, SMD_loc, marker='v', s=30, color='black')
        plt.scatter(energy, TP_loc, label='TP',color='red', s=30)
        plt.errorbar(energy, TP_loc, yerr=TP_loc_err, ls='none',color='red', lw=0.5)
        plt.plot(energy, cecilEqEJ305(energy, popt_TP[0]),color='red', lw=0.5)
        plt.legend()

        plt.subplot(3,1,3)
        plt.scatter(energy, SMD_loc, marker='v', s=30, color='black')
        plt.scatter(energy, FD_loc, label='FD',color='green', s=30)
        plt.errorbar(energy, FD_loc, yerr=FD_loc_err, ls='none',color='green', lw=0.5)
        plt.plot(energy, cecilEqEJ305(energy, popt_FD[0]),color='green', lw=0.5)
        plt.legend()
        plt.show()

    if detector == 'EJ321P':
        popt_SMD, pcov_SMD = curve_fit(cecilEqEJ321P, energy, SMD_loc)
        pcov_SMD = np.diag(np.sqrt(pcov_SMD))

        popt_HH, pcov_HH = curve_fit(cecilEqEJ321P, energy, HH_loc)
        pcov_HH = np.diag(np.sqrt(pcov_HH))

        popt_TP, pcov_TP = curve_fit(cecilEqEJ321P, energy, TP_loc)
        pcov_TP = np.diag(np.sqrt(pcov_TP))

        popt_FD, pcov_FD = curve_fit(cecilEqEJ321P, energy, FD_loc)
        pcov_FD = np.diag(np.sqrt(pcov_FD))

        redChi2_HH = promath.chi2red(HH_loc, cecilEqEJ321P(energy, popt_HH[0]), HH_loc_err, 1)
        redChi2_TP = promath.chi2red(TP_loc, cecilEqEJ321P(energy, popt_TP[0]), TP_loc_err, 1)
        redChi2_FD = promath.chi2red(FD_loc, cecilEqEJ321P(energy, popt_FD[0]), FD_loc_err, 1)    
        redChi2_SMD = promath.chi2red(SMD_loc, cecilEqEJ321P(energy, popt_SMD[0]), SMD_loc_err, 1)

        print(f'SMD....K = {np.round(popt_SMD[0],2)}+/-{np.round(pcov_SMD[0],2)}, chi2/dof = {np.round(redChi2_SMD,2)}')
        print(f'HH.....K = {np.round(popt_HH[0],2)}+/-{np.round(pcov_HH[0],2)}, chi2/dof = {np.round(redChi2_HH,2)}')
        print(f'TP.....K = {np.round(popt_TP[0],2)}+/-{np.round(pcov_TP[0],2)}, chi2/dof = {np.round(redChi2_TP,2)}')
        print(f'FD.....K = {np.round(popt_FD[0],2)}+/-{np.round(pcov_FD[0],2)}, chi2/dof = {np.round(redChi2_FD,2)}')
        print('-------------------------------')
        plt.suptitle(detector)
        plt.subplot(3,1,1)
        plt.scatter(energy, SMD_loc, marker='v', s=30, color='black')
        plt.scatter(energy, HH_loc, label='HH',color='blue', s=30)
        plt.errorbar(energy, HH_loc, yerr=HH_loc_err, ls='none',color='blue', lw=0.5)
        plt.plot(energy, cecilEqEJ321P(energy, popt_HH[0]),color='blue', lw=0.5)
        plt.legend()

        plt.subplot(3,1,2)
        plt.scatter(energy, SMD_loc, marker='v', s=30, color='black')
        plt.scatter(energy, TP_loc, label='TP',color='red', s=30)
        plt.errorbar(energy, TP_loc, yerr=TP_loc_err, ls='none',color='red', lw=0.5)
        plt.plot(energy, cecilEqEJ321P(energy, popt_TP[0]),color='red', lw=0.5)
        plt.legend()

        plt.subplot(3,1,3)
        plt.scatter(energy, SMD_loc, marker='v', s=30, color='black')
        plt.scatter(energy, FD_loc, label='FD',color='green', s=30)
        plt.errorbar(energy, FD_loc, yerr=FD_loc_err, ls='none',color='green', lw=0.5)
        plt.plot(energy, cecilEqEJ321P(energy, popt_FD[0]),color='green', lw=0.5)
        plt.legend()
        plt.show()

    if detector == 'EJ331':
        popt_SMD, pcov_SMD = curve_fit(cecilEqEJ331, energy, SMD_loc)
        pcov_SMD = np.diag(np.sqrt(pcov_SMD))

        popt_HH, pcov_HH = curve_fit(cecilEqEJ331, energy, HH_loc)
        pcov_HH = np.diag(np.sqrt(pcov_HH))

        popt_TP, pcov_TP = curve_fit(cecilEqEJ331, energy, TP_loc)
        pcov_TP = np.diag(np.sqrt(pcov_TP))

        popt_FD, pcov_FD = curve_fit(cecilEqEJ331, energy, FD_loc)
        pcov_FD = np.diag(np.sqrt(pcov_FD))

        redChi2_HH = promath.chi2red(HH_loc, cecilEqEJ331(energy, popt_HH[0]), HH_loc_err, 1)
        redChi2_TP = promath.chi2red(TP_loc, cecilEqEJ331(energy, popt_TP[0]), TP_loc_err, 1)
        redChi2_FD = promath.chi2red(FD_loc, cecilEqEJ331(energy, popt_FD[0]), FD_loc_err, 1)    
        redChi2_SMD = promath.chi2red(SMD_loc, cecilEqEJ331(energy, popt_SMD[0]), SMD_loc_err, 1)
        
        print('-------------------------------')
        print(detector)
        print(f'SMD....K = {np.round(popt_SMD[0],2)}+/-{np.round(pcov_SMD[0],2)}, chi2/dof = {np.round(redChi2_SMD,2)}')
        print(f'HH.....K = {np.round(popt_HH[0],2)}+/-{np.round(pcov_HH[0],2)}, chi2/dof = {np.round(redChi2_HH,2)}')
        print(f'TP.....K = {np.round(popt_TP[0],2)}+/-{np.round(pcov_TP[0],2)}, chi2/dof = {np.round(redChi2_TP,2)}')
        print(f'FD.....K = {np.round(popt_FD[0],2)}+/-{np.round(pcov_FD[0],2)}, chi2/dof = {np.round(redChi2_FD,2)}')
        print('-------------------------------')
        
        plt.suptitle(detector)
        plt.subplot(3,1,1)
        plt.scatter(energy, SMD_loc, marker='v', s=30, color='black')
        plt.scatter(energy, HH_loc, label='HH',color='blue', s=30)
        plt.errorbar(energy, HH_loc, yerr=HH_loc_err, ls='none',color='blue', lw=0.5)
        plt.plot(energy, cecilEqEJ331(energy, popt_HH[0]),color='blue', lw=0.5)
        plt.legend()

        plt.subplot(3,1,2)
        plt.scatter(energy, SMD_loc, marker='v', s=30, color='black')
        plt.scatter(energy, TP_loc, label='TP',color='red', s=30)
        plt.errorbar(energy, TP_loc, yerr=TP_loc_err, ls='none',color='red', lw=0.5)
        plt.plot(energy, cecilEqEJ331(energy, popt_TP[0]),color='red', lw=0.5)
        plt.legend()

        plt.subplot(3,1,3)
        plt.scatter(energy, SMD_loc, marker='v', s=30, color='black')
        plt.scatter(energy, FD_loc, label='FD',color='green', s=30)
        plt.errorbar(energy, FD_loc, yerr=FD_loc_err, ls='none',color='green', lw=0.5)
        plt.plot(energy, cecilEqEJ331(energy, popt_FD[0]),color='green', lw=0.5)
        plt.legend()

        plt.show()



















detector = 'EJ321P'
gate = 'LG'
pathData = '/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper'

HH_data = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/HH_TP_FD/HH_data_{gate}.pkl')
TP_data = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/HH_TP_FD/TP_data_{gate}.pkl')
FD_data = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/HH_TP_FD/FD_data_{gate}.pkl')

energy = HH_data.energy

LY_sim = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/simulation/sim_LightYield.pkl')
SMD_loc = LY_sim.means
SMD_loc_err = LY_sim.means_err

HH_loc = HH_data.HH_loc
TP_loc = TP_data.TP_loc
FD_loc = FD_data.FD_loc

HH_loc_err = HH_data.HH_loc_err
TP_loc_err = TP_data.TP_loc_err
FD_loc_err = FD_data.FD_loc_err

popt_HH, pcov_HH = curve_fit(cecilEq, energy, HH_loc)
pcov_HH = np.sqrt(np.diag(pcov_HH))

popt_TP, pcov_TP = curve_fit(cecilEq, energy, TP_loc)
pcov_TP = np.sqrt(np.diag(pcov_TP))

popt_FD, pcov_FD = curve_fit(cecilEq, energy, FD_loc)
pcov_FD = np.sqrt(np.diag(pcov_FD))





p1_avg =         np.average([popt_HH[0], popt_TP[0], popt_FD[0]])
p1_avg_err  =    np.average([pcov_HH[0], pcov_TP[0], pcov_FD[0]])
p2_avg =         np.average([popt_HH[1], popt_TP[1], popt_FD[1]])
p2_avg_err  =    np.average([pcov_HH[1], pcov_TP[1], pcov_FD[1]])
p3_avg =         np.average([popt_HH[2], popt_TP[2], popt_FD[2]])
p3_avg_err  =    np.average([pcov_HH[2], pcov_TP[2], pcov_FD[2]])
p4_avg =         np.average([popt_HH[3], popt_TP[3], popt_FD[3]])
p4_avg_err  =    np.average([pcov_HH[3], pcov_TP[3], pcov_FD[3]])

redChi2_HH = promath.chi2red(HH_loc, cecilEq(energy, popt_HH[0], popt_HH[1], popt_HH[2], popt_HH[3]), HH_loc_err, 4)    
redChi2_FD = promath.chi2red(FD_loc, cecilEq(energy, popt_FD[0], popt_FD[1], popt_FD[2], popt_FD[3]), FD_loc_err, 4)
redChi2_TP = promath.chi2red(TP_loc, cecilEq(energy, popt_TP[0], popt_TP[1], popt_TP[2], popt_TP[3]), TP_loc_err, 4)
redChi2_SMD = promath.chi2red(SMD_loc, cecilEq(energy, p1_avg, p2_avg, p3_avg, p4_avg), SMD_loc_err, 4)

print(f'Scintillator: {detector}')
# print(f'SMD.....p1 = {np.round(popt_SMD[0],2)}+/-{np.round(pcov_SMD[0],2)}, p2 = {np.round(popt_SMD[1],2)}+/-{np.round(pcov_SMD[1],2)}, p3 = {np.round(popt_SMD[2],2)}+/-{np.round(pcov_SMD[2],2)}, p4 = {np.round(popt_SMD[3],2)}+/-{np.round(pcov_SMD[3],2)}, chi2/dof = {np.round(redChi2_SMD,2)}')
print(f'HH.....p1 = {np.round(popt_HH[0],2)}+/-{np.round(pcov_HH[0],2)}, p2 = {np.round(popt_HH[1],2)}+/-{np.round(pcov_HH[1],2)}, p3 = {np.round(popt_HH[2],2)}+/-{np.round(pcov_HH[2],2)}, p4 = {np.round(popt_HH[3],2)}+/-{np.round(pcov_HH[3],2)}, chi2/dof = {np.round(redChi2_HH,2)}')
print(f'TP.....p1 = {np.round(popt_TP[0],2)}+/-{np.round(pcov_TP[0],2)}, p2 = {np.round(popt_TP[1],2)}+/-{np.round(pcov_TP[1],2)}, p3 = {np.round(popt_TP[2],2)}+/-{np.round(pcov_TP[2],2)}, p4 = {np.round(popt_TP[3],2)}+/-{np.round(pcov_TP[3],2)}, chi2/dof = {np.round(redChi2_TP,2)}')
print(f'FD.....p1 = {np.round(popt_FD[0],2)}+/-{np.round(pcov_FD[0],2)}, p2 = {np.round(popt_FD[1],2)}+/-{np.round(pcov_FD[1],2)}, p3 = {np.round(popt_FD[2],2)}+/-{np.round(pcov_FD[2],2)}, p4 = {np.round(popt_FD[3],2)}+/-{np.round(pcov_FD[3],2)}, chi2/dof = {np.round(redChi2_FD,2)}')
print(f'Avg.....p1 = {np.round(p1_avg,2)}+/-{np.round(p1_avg_err,2)}, p2 = {np.round(p2_avg,2)}+/-{np.round(p2_avg_err,2)}, p3 = {np.round(p3_avg,2)}+/-{np.round(p3_avg_err,2)}, p4 = {np.round(p4_avg,2)}+/-{np.round(p4_avg_err,2)}')
# print(f'SMD....chi2/dof = {np.round(redChi2_SMD,2)}')

# popt_SMD, pcov_SMD = curve_fit(cecilEq, energy, SMD_loc)
# pcov_SMD = np.sqrt(np.diag(pcov_SMD))

print('-----------------------------------------------------------------------')


plt.suptitle(detector)
plt.subplot(4,1,1)
# plt.scatter(energy, SMD_loc, marker='v', s=20, color='black')
plt.scatter(energy, HH_loc, label='HH',color='blue', s=10)
plt.errorbar(energy, HH_loc, yerr=HH_loc_err, ls='none',color='blue', lw=0.5)
plt.plot(energy, cecilEq(energy, popt_HH[0], popt_HH[1], popt_HH[2], popt_HH[3]),color='blue', lw=0.5)
plt.legend()

plt.subplot(4,1,2)
# plt.scatter(energy, SMD_loc, marker='v', s=20, color='black')
plt.scatter(energy, TP_loc, label='TP',color='red', s=10)
plt.errorbar(energy, TP_loc, yerr=TP_loc_err, ls='none',color='red', lw=0.5)
plt.plot(energy, cecilEq(energy, popt_TP[0], popt_TP[1], popt_HH[2], popt_HH[3]),color='red', lw=0.5)
plt.legend()

plt.subplot(4,1,3)
# plt.scatter(energy, SMD_loc, marker='v', s=20, color='black')
plt.scatter(energy, FD_loc, label='FD',color='green', s=10)
plt.errorbar(energy, FD_loc, yerr=FD_loc_err, ls='none',color='green', lw=0.5)
plt.plot(energy, cecilEq(energy, popt_FD[0], popt_FD[1], popt_HH[2], popt_HH[3]),color='green', lw=0.5)
plt.legend()

plt.subplot(4,1,4)
plt.scatter(energy, SMD_loc, marker='v', s=20, color='black')
plt.errorbar(energy, SMD_loc, yerr=SMD_loc_err, ls='none',color='green', lw=0.5)
plt.plot(energy, cecilEq(energy, p1_avg, p2_avg, p3_avg, p4_avg),color='black', lw=0.5)
plt.legend()


plt.show()