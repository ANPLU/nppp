#!/usr/bin/env python3
import sys
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit

# IMPORT FROM LIBRARY DIRECTORY
sys.path.insert(0, "../library/")
sys.path.insert(0, "../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "sim_analysis/")
sys.path.insert(0, "data_analysis/")

import digidata
import nicholai_plot_helper as nplt
import processing_utility as prout
import processing_math as promath
import processing_data as prodata
import processing_pd as propd
import processing_simulation as prosim

"""
Script for running both data and simulation and calibrating data of EJ321P detector.
"""

PMT_gain = 4e6 #Nominal gain is 7'000'000 at nominal A/lm (model: 9821B p1)
att_factor_6dB = 2.00
att_factor_9dB = 2.82
att_factor_11dB = 3.55
att_factor_12dB = 3.98
att_factor_16_5dB = 6.68
att_factor_19dB = 8.91
q = 1.60217662E-19 #charge of a single electron
plot = False

##############################
# LOADING DATA
##############################
path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop
bg_runs =       list(range(3753, 3753+10))#list(range(2800, 2823))
Cs137_runs =    [3749]#[2824, 2825]
Th232_runs =    [3763]
AmBe_runs =     [3751]#list(range(3025, 3034))
PuBe_runs =     [3607] 

CableLengthCorrections = 0.95#1.076 #correction constant between inside cave and outside due to different cable lengthsc

df_bg =         propd.load_parquet_merge(path, bg_runs,     keep_col=['qdc_lg_ch1'], full=False)*CableLengthCorrections
df_Cs137 =      propd.load_parquet_merge(path, Cs137_runs,  keep_col=['qdc_lg_ch1'], full=False)*CableLengthCorrections
df_Th232 =      propd.load_parquet_merge(path, Th232_runs,  keep_col=['qdc_lg_ch1'], full=False)*CableLengthCorrections*1.05 #added 5% due to gain drift between several days of running the detector
df_AmBe =       propd.load_parquet_merge(path, AmBe_runs,   keep_col=['qdc_lg_ch1'], full=False)*CableLengthCorrections
df_PuBe =       propd.load_parquet_merge(path, PuBe_runs,   keep_col=['qdc_lg_ch1'], full=False)

#check for gaindrift with Cs137
# df1 = propd.load_parquet_merge(path, [3749],  keep_col=['qdc_lg_ch1'], full=False)
# df2 = propd.load_parquet_merge(path, [3750],  keep_col=['qdc_lg_ch1'], full=False)
# df3 = propd.load_parquet_merge(path, [3752],  keep_col=['qdc_lg_ch1'], full=False)
# df4 = propd.load_parquet_merge(path, [3764],  keep_col=['qdc_lg_ch1'], full=False) #for Th calibration

# numBins = np.arange(0,4000,30)
# y,x = np.histogram(df1.qdc_lg_ch1, bins=numBins)
# plt.step(prodata.getBinCenters(x), y/6, label='1')
# y,x = np.histogram(df2.qdc_lg_ch1, bins=numBins)
# plt.step(prodata.getBinCenters(x), y, label='2')
# y,x = np.histogram(df3.qdc_lg_ch1, bins=numBins)
# plt.step(prodata.getBinCenters(x), y, label='3')
# y,x = np.histogram(df4.qdc_lg_ch1, bins=numBins)
# plt.step(prodata.getBinCenters(x)*1.05, y*0.38, label='4')
# plt.legend()
# plt.show()


# Check cablelength correction value
# y,x = np.histogram(df_AmBe, bins=np.arange(0,25000,100))
# plt.step(prodata.getBinCenters(x), y*3, label='AmBe')

# y,x = np.histogram(df_bg, bins=np.arange(0,25000,100))
# plt.step(prodata.getBinCenters(x), y, label='BG')

# y,x = np.histogram(df_Cs137, bins=np.arange(0,25000,100))
# plt.step(prodata.getBinCenters(x), y, label='Cs137')

# y,x = np.histogram(df_Th232, bins=np.arange(0,25000,100))
# plt.step(prodata.getBinCenters(x), y, label='Th232')

# y,x = np.histogram(df_PuBe, bins=np.arange(0,25000,100))
# plt.step(prodata.getBinCenters(x), y, label='ToF')

# plt.yscale('log')

# plt.legend()
# plt.show()
# 
###############################
# LOADING SIMULATION DATA
###############################
detector = 'EJ321P'

path_sim = '/media/gheed/Seagate_Expansion_Drive1/data/simulation/'
names = ['evtNum', 
        'optPhotonSum', 
        'optPhotonSumQE', 
        'optPhotonSumCompton', 
        'optPhotonSumComptonQE', 
        'optPhotonSumNPQE', 
        'xLoc', 
        'yLoc', 
        'zLoc', 
        'CsMin', 
        'optPhotonParentID', 
        'optPhotonParentCreatorProcess', 
        'optPhotonParentStartingEnergy',
        'total_edep',
        'gamma_edep',
        'proton_edep',
        'nScatters',
        'nScattersProton']

############################################
# LOAD ISOTROPIC SIMULATIONS
############################################
Cs137_sim_iso =         pd.read_csv(path_sim + f"{detector}/Cs137/isotropic/CSV_optphoton_data_sum.csv", names=names)
Th232_sim_iso =         pd.read_csv(path_sim + f"{detector}/Th232/isotropic/CSV_optphoton_data_sum.csv", names=names)
AmBe_sim_iso =          pd.read_csv(path_sim + f"{detector}/4.44/isotropic/CSV_optphoton_data_sum.csv", names=names)

Cs137_sim_iso =         pd.concat([Cs137_sim_iso, pd.read_csv(path_sim + f"{detector}/Cs137/isotropic/part2/CSV_optphoton_data_sum.csv", names=names)])
Cs137_sim_iso =         pd.concat([Cs137_sim_iso, pd.read_csv(path_sim + f"{detector}/Cs137/isotropic/part3/CSV_optphoton_data_sum.csv", names=names)])

Th232_sim_iso =         pd.concat([Th232_sim_iso, pd.read_csv(path_sim + f"{detector}/Th232/isotropic/part2/CSV_optphoton_data_sum.csv", names=names)])
Th232_sim_iso =         pd.concat([Th232_sim_iso, pd.read_csv(path_sim + f"{detector}/Th232/isotropic/part3/CSV_optphoton_data_sum.csv", names=names)])

AmBe_sim_iso =          pd.concat([AmBe_sim_iso, pd.read_csv(path_sim + f"{detector}/4.44/isotropic/part3/CSV_optphoton_data_sum.csv", names=names)])
AmBe_sim_iso =          pd.concat([AmBe_sim_iso, pd.read_csv(path_sim + f"{detector}/4.44/isotropic/part3/CSV_optphoton_data_sum.csv", names=names)])

############################################
# LOAD PENCILBEAM SIMULATIONS
############################################
Cs137_sim_pen = pd.read_csv(path_sim + f"{detector}/Cs137/pencilbeam/CSV_optphoton_data_sum.csv", names=names)
Th232_sim_pen = pd.read_csv(path_sim + f"{detector}/Th232/pencilbeam/CSV_optphoton_data_sum.csv", names=names)
AmBe_sim_pen = pd.read_csv(path_sim + f"{detector}/4.44/pencilbeam/CSV_optphoton_data_sum.csv", names=names)

Cs137_sim_pen = pd.concat([Cs137_sim_pen, pd.read_csv(path_sim + f"{detector}/Cs137/pencilbeam/part2/CSV_optphoton_data_sum.csv", names=names)])
Cs137_sim_pen = pd.concat([Cs137_sim_pen, pd.read_csv(path_sim + f"{detector}/Cs137/pencilbeam/part3/CSV_optphoton_data_sum.csv", names=names)])

Th232_sim_pen = pd.concat([Th232_sim_pen, pd.read_csv(path_sim + f"{detector}/Th232/pencilbeam/part2/CSV_optphoton_data_sum.csv", names=names)])
Th232_sim_pen = pd.concat([Th232_sim_pen, pd.read_csv(path_sim + f"{detector}/Th232/pencilbeam/part3/CSV_optphoton_data_sum.csv", names=names)])

AmBe_sim_pen = pd.concat([AmBe_sim_pen, pd.read_csv(path_sim + f"{detector}/4.44/pencilbeam/part2/CSV_optphoton_data_sum.csv", names=names)])
AmBe_sim_pen = pd.concat([AmBe_sim_pen, pd.read_csv(path_sim + f"{detector}/4.44/pencilbeam/part3/CSV_optphoton_data_sum.csv", names=names)])

###############################
# RANDOMIZE BINNING
###############################
y, x = np.histogram(Cs137_sim_iso.optPhotonSumQE, bins=4096, range=[0, 4096])
Cs137_sim_iso.optPhotonSumQE = prodata.getRandDist(x, y)
y, x = np.histogram(Th232_sim_iso.optPhotonSumQE, bins=4096, range=[0, 4096])
Th232_sim_iso.optPhotonSumQE = prodata.getRandDist(x, y)
y, x = np.histogram(AmBe_sim_iso.optPhotonSumQE, bins=4096, range=[0, 4096])
AmBe_sim_iso.optPhotonSumQE = prodata.getRandDist(x, y)

y, x = np.histogram(Cs137_sim_pen.optPhotonSumComptonQE, bins=4096, range=[0, 4096])
Cs137_sim_pen.optPhotonSumComptonQE = prodata.getRandDist(x, y)
y, x = np.histogram(Th232_sim_pen.optPhotonSumComptonQE, bins=4096, range=[0, 4096])
Th232_sim_pen.optPhotonSumComptonQE = prodata.getRandDist(x, y)
y, x = np.histogram(AmBe_sim_pen.optPhotonSumComptonQE, bins=4096, range=[0, 4096])
AmBe_sim_pen.optPhotonSumComptonQE = prodata.getRandDist(x, y)

###############################
# RESET INDEX
###############################
Cs137_sim_iso =  Cs137_sim_iso.reset_index()
Th232_sim_iso =  Th232_sim_iso.reset_index()
AmBe_sim_iso =   AmBe_sim_iso.reset_index()

Cs137_sim_pen =  Cs137_sim_pen.reset_index()
Th232_sim_pen =  Th232_sim_pen.reset_index()
AmBe_sim_pen =   AmBe_sim_pen.reset_index()

###############################
# CALIBRATING SIMULATION DATA
###############################
simLightYieldFactor = 1#/3 #due to x3 increased Light Yield in simulation
Cs137_sim_iso.optPhotonSumQE =         prosim.chargeCalibration(Cs137_sim_iso.optPhotonSumQE            * q * PMT_gain * simLightYieldFactor)
Th232_sim_iso.optPhotonSumQE =         prosim.chargeCalibration(Th232_sim_iso.optPhotonSumQE            * q * PMT_gain * simLightYieldFactor)
AmBe_sim_iso.optPhotonSumQE =          prosim.chargeCalibration(AmBe_sim_iso.optPhotonSumQE             * q * PMT_gain * simLightYieldFactor)

Cs137_sim_pen.optPhotonSumComptonQE =      prosim.chargeCalibration(Cs137_sim_pen.optPhotonSumComptonQE * q * PMT_gain * simLightYieldFactor) 
Th232_sim_pen.optPhotonSumComptonQE =      prosim.chargeCalibration(Th232_sim_pen.optPhotonSumComptonQE * q * PMT_gain * simLightYieldFactor) 
AmBe_sim_pen.optPhotonSumComptonQE =      prosim.chargeCalibration(AmBe_sim_pen.optPhotonSumComptonQE   * q * PMT_gain * simLightYieldFactor) 



###############################
# PMT ALIGN Cs-137
###############################
res = 0.115
numBins = np.arange(0, 30000, 50)
Cs137_sim_smear = np.zeros(len(Cs137_sim_iso.optPhotonSumQE))
for i in range(len(Cs137_sim_iso.optPhotonSumQE)):
    Cs137_sim_smear[i] = prosim.gaussSmear(Cs137_sim_iso.optPhotonSumQE[i], res)

gain = .27

# countsSimPen, binsSim = np.histogram(Cs137_sim_pen.optPhotonSumComptonQE*gain, bins=numBins)

# countsSim, binsSim = np.histogram(Cs137_sim_smear*gain, bins=numBins)
# binsSim = prodata.getBinCenters(binsSim)
# w = np.empty(len(df_bg))
# w.fill(1/len(bg_runs))
# countsBG, binsBG = np.histogram(df_bg.qdc_lg_ch1*att_factor_6dB, bins=numBins, weights=w)
# w = np.empty(len(df_Cs137))
# w.fill(1/len(Cs137_runs))
# counts, bins = np.histogram(df_Cs137.qdc_lg_ch1*att_factor_6dB, bins=numBins, weights=w)
# bins = prodata.getBinCenters(bins)
# plt.plot(bins, counts-countsBG, label='data-BG')
# plt.plot(binsSim, countsSimPen*2.1, label='sim pencilbeam', alpha=0.4)
# plt.plot(binsSim, countsSim*1.8, label='sim')

# plt.legend()
# plt.show()

Cs137_results = prosim.PMTGainAlign( model = Cs137_sim_smear, 
                                    data = df_Cs137.qdc_lg_ch1*att_factor_6dB, 
                                    bg = df_bg.qdc_lg_ch1*att_factor_6dB,
                                    scale = 1.8,
                                    gain = gain,
                                    data_weight = len(Cs137_runs),
                                    bg_weight = len(bg_runs),
                                    scaleBound = [1.5, 3.0],
                                    gainBound = [gain*0.5, gain*1.5],
                                    stepSize = 1e-2,
                                    binRange = list(range(2400, 5000, 50)),
                                    plot = True)
Cs137_results['smear'] = res



###############################
# PMT ALIGN Th232 2615 keV
###############################
res = 0.061
numBins = np.arange(7500, 75000, 150)
Th232_sim_smear = np.zeros(len(Th232_sim_iso.optPhotonSumQE))
for i in range(len(Th232_sim_iso.optPhotonSumQE)):
    Th232_sim_smear[i] = prosim.gaussSmear(Th232_sim_iso.optPhotonSumQE[i], res)

gain = 0.27

# countsSimPen, binsSim = np.histogram(Th232_sim_pen.optPhotonSumComptonQE*gain, bins=numBins)
# countsSim, binsSim = np.histogram(Th232_sim_smear*gain, bins=numBins)
# binsSim = prodata.getBinCenters(binsSim)
# w = np.empty(len(df_bg))
# w.fill(1/len(bg_runs))
# countsBG, binsBG = np.histogram(df_bg.qdc_lg_ch1*att_factor_6dB, bins=numBins, weights=w)
# w = np.empty(len(df_Th232))
# w.fill(1/len(Th232_runs))
# counts, bins = np.histogram(df_Th232.qdc_lg_ch1*att_factor_6dB, bins=numBins, weights=w)
# bins = prodata.getBinCenters(bins)
# plt.plot(bins, counts-countsBG, label='data-BG')
# plt.plot(binsSim, countsSimPen*1, label='sim pencilbeam', alpha=0.4)
# plt.plot(binsSim, countsSim*0.24, label='sim')
# plt.legend()
# plt.show()

Th232_results = prosim.PMTGainAlign(model = Th232_sim_smear, 
                                    data = df_Th232.qdc_lg_ch1*att_factor_6dB, 
                                    bg = df_bg.qdc_lg_ch1*att_factor_6dB,
                                    scale = 0.25,
                                    gain = gain,
                                    data_weight = len(Th232_runs),
                                    bg_weight = len(bg_runs),
                                    scaleBound = [0.1, 0.7],
                                    gainBound = [gain*0.5, gain*1.5],
                                    stepSize = 1e-2,
                                    binRange = list(range(15000, 18900, 200)),
                                    plot = True)
Th232_results['smear'] = res

###############################
# PMT ALIGN AmBe 4439 keV
###############################
res = 0.068
numBins = np.arange(15000, 120000, 300)
AmBe_sim_smear = np.zeros(len(AmBe_sim_iso.optPhotonSumQE))
for i in range(len(AmBe_sim_iso.optPhotonSumQE)):
    AmBe_sim_smear[i] = prosim.gaussSmear(AmBe_sim_iso.optPhotonSumQE[i], res)

gain = 0.27

# countsSimPen, binsSim = np.histogram(AmBe_sim_pen.optPhotonSumComptonQE*gain, bins=numBins)
# countsSim, binsSim = np.histogram(AmBe_sim_smear*gain, bins=numBins)
# binsSim = prodata.getBinCenters(binsSim)
# w = np.empty(len(df_bg))
# w.fill(1/len(bg_runs))
# countsBG, binsBG = np.histogram(df_bg.qdc_lg_ch1*att_factor_6dB, bins=numBins, weights=w)
# w = np.empty(len(df_AmBe))
# w.fill(1/len(AmBe_runs))
# counts, bins = np.histogram(df_AmBe.qdc_lg_ch1*att_factor_6dB, bins=numBins, weights=w)
# bins = prodata.getBinCenters(bins)
# plt.plot(bins, counts-countsBG, label='data-BG')
# plt.plot(binsSim, countsSimPen*0.7592, label='sim pencilbeam', alpha=0.4)
# plt.plot(binsSim, countsSim*0.7592, label='sim')
# plt.legend()
# plt.show()

AmBe_results = prosim.PMTGainAlign( model = AmBe_sim_smear, 
                                    data = df_AmBe.qdc_lg_ch1*att_factor_6dB, 
                                    bg = df_bg.qdc_lg_ch1*att_factor_6dB,
                                    scale = 0.75,
                                    gain = gain,
                                    data_weight = len(AmBe_runs),
                                    bg_weight = len(bg_runs),
                                    scaleBound = [0.5 , 1.0],
                                    gainBound = [gain*0.5, gain*1.5],
                                    stepSize = 1e-2,
                                    binRange = list(range(27000, 34200, 300)),
                                    plot = True)
AmBe_results['smear'] = res


#############################
## FITTING PENCILBEAM DATA ##
#############################

#QDC calibrate simulations pencilbeam
Cs137_sim_pen.optPhotonSumComptonQE = Cs137_sim_pen.optPhotonSumComptonQE * Cs137_results['gain']
Th232_sim_pen.optPhotonSumComptonQE = Th232_sim_pen.optPhotonSumComptonQE * Th232_results['gain']
AmBe_sim_pen.optPhotonSumComptonQE =  AmBe_sim_pen.optPhotonSumComptonQE * AmBe_results['gain']

#Apply smearing on pencilbeam data
Cs137_sim_pen.optPhotonSumComptonQE = Cs137_sim_pen.optPhotonSumComptonQE.apply(lambda x: prosim.gaussSmear(x, Cs137_results['smear'])) 
Th232_sim_pen.optPhotonSumComptonQE = Th232_sim_pen.optPhotonSumComptonQE.apply(lambda x: prosim.gaussSmear(x, Th232_results['smear'])) 
AmBe_sim_pen.optPhotonSumComptonQE = AmBe_sim_pen.optPhotonSumComptonQE.apply(lambda x: prosim.gaussSmear(x, AmBe_results['smear'])) 

start = 2000
stop = 4200
counts, bin_edges = np.histogram(Cs137_sim_pen.optPhotonSumComptonQE, bins = np.arange(start*0.8, stop*1.2, 100))
bin_centers = prodata.getBinCenters(bin_edges)
Ef = 0.66166
Cs137_sim_fit  =  prosim.calibrationFitBinned(bin_centers, counts, start, stop, Ef=Ef, plot=True, compton=True)
Cs137_sim_fit['CE'] = promath.comptonMax(Ef)
mean = Cs137_sim_fit['gauss_mean']
std = Cs137_sim_fit['gauss_std']
Cs137_sim_results = {}
Cs137_sim_results['mean'] = np.mean(Cs137_sim_pen.query(f'optPhotonSumComptonQE>{mean-3*std} and optPhotonSumComptonQE<{mean+3*std}').optPhotonSumComptonQE)
Cs137_sim_results['mean_err']  = np.std(Cs137_sim_pen.query(f'optPhotonSumComptonQE>{mean-3*std} and optPhotonSumComptonQE<{mean+3*std}').optPhotonSumComptonQE)/np.sqrt(len(Cs137_sim_pen.query(f'optPhotonSumComptonQE>{mean-3*std} and optPhotonSumComptonQE<{mean+3*std}').optPhotonSumComptonQE))


start = 15400
stop = 22000
counts, bin_edges = np.histogram(Th232_sim_pen.optPhotonSumComptonQE, bins = np.arange(start*0.8, stop*1.2, 350))
bin_centers = prodata.getBinCenters(bin_edges)
Ef = 2.614533
Th232_sim_fit = prosim.calibrationFitBinned(bin_centers, counts, start, stop, Ef=Ef, plot=True, compton=True)
Th232_sim_fit['CE'] = promath.comptonMax(Ef)
mean = Th232_sim_fit['gauss_mean']
std = Th232_sim_fit['gauss_std']
Th232_sim_results = {}
Th232_sim_results['mean'] = np.mean(Th232_sim_pen.query(f'optPhotonSumComptonQE>{mean-3*std} and optPhotonSumComptonQE<{mean+3*std}').optPhotonSumComptonQE)
Th232_sim_results['mean_err']  = np.std(Th232_sim_pen.query(f'optPhotonSumComptonQE>{mean-3*std} and optPhotonSumComptonQE<{mean+3*std}').optPhotonSumComptonQE)/np.sqrt(len(Th232_sim_pen.query(f'optPhotonSumComptonQE>{mean-3*std} and optPhotonSumComptonQE<{mean+3*std}').optPhotonSumComptonQE))


start = 24500
stop = 34500
counts, bin_edges = np.histogram(AmBe_sim_pen.optPhotonSumComptonQE, bins = np.arange(start*0.8, stop*1.2, 800))
bin_centers = prodata.getBinCenters(bin_edges)
Ef = 4.439
AmBe_sim_fit = prosim.calibrationFitBinned(bin_centers, counts, start, stop, Ef=Ef, plot=True, compton=True)
AmBe_sim_fit['CE'] = promath.comptonMax(Ef)
mean = AmBe_sim_fit['gauss_mean']
std = AmBe_sim_fit['gauss_std']
AmBe_sim_results = {}
AmBe_sim_results['mean'] = np.mean(AmBe_sim_pen.query(f'optPhotonSumComptonQE>{mean-3*std} and optPhotonSumComptonQE<{mean+3*std}').optPhotonSumComptonQE)
AmBe_sim_results['mean_err']  = np.std(AmBe_sim_pen.query(f'optPhotonSumComptonQE>{mean-3*std} and optPhotonSumComptonQE<{mean+3*std}').optPhotonSumComptonQE)/np.sqrt(len(AmBe_sim_pen.query(f'optPhotonSumComptonQE>{mean-3*std} and optPhotonSumComptonQE<{mean+3*std}').optPhotonSumComptonQE))





##################################################
#Calculate calibration constant
CE_energy =     np.array([0, Cs137_sim_fit['CE'],           Th232_sim_fit['CE'],            AmBe_sim_fit['CE']])            #energies in MeVee of Compton edges
QDC_loc =       np.array([0, Cs137_sim_results['mean'],     Th232_sim_results['mean'],      AmBe_sim_results['mean']])      #relevant QDC channels
QDC_loc_err =   np.array([0, Cs137_sim_results['mean_err'], Th232_sim_results['mean_err'],  AmBe_sim_results['mean_err']])  #relevant QDC channels errors from gain-align above.

popt, pcov = curve_fit(promath.linearFunc, CE_energy, QDC_loc)
pcov = np.diag(np.sqrt(pcov))

#Make figure
E_err = promath.errorPropLinearFuncInv(promath.linearFuncInv(QDC_loc, popt[0], popt[1]), popt[0], pcov[0], popt[1], pcov[1])
plt.scatter(QDC_loc, CE_energy)
plt.errorbar(QDC_loc, CE_energy, yerr = E_err, ls='none')
plt.plot(np.arange(0, 30000, 100), promath.linearFuncInv(np.arange(0,30000,100), popt[0], popt[1]))
plt.ylabel('MeV$_{ee}$')
plt.xlabel('QDC [channels]')
plt.show()

#save to disk
np.save(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal/popt_LG', popt)
np.save(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal/pcov_LG', pcov)

# np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal/popt_LG.npy')

# plt.plot(np.arange(0, 30000, 100), prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal/popt_LG.npy', np.arange(0, 30000, 100)), label='before')
# plt.plot(np.arange(0, 30000, 100), promath.linearFuncInv(np.arange(0,30000,100), popt[0], popt[1]), label='after')
# plt.legend()
# plt.xlabel('QDC channels')
# plt.ylabel('MeVee')
# plt.title('EJ 321P (oil)')
# plt.show()
