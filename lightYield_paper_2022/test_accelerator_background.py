#!/usr/bin/env python3
from re import I
import sys
from tkinter import X
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
from numpy.core.arrayprint import SubArrayFormat
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit

# from matplotlib.colors import LinearSegmentedColormap


# IMPORT FROM LIBRARY DIRECTORY
sys.path.insert(0, "../library/")
sys.path.insert(0, "../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "../")
sys.path.insert(0, "data_analysis/")

import processing_math as promath
import processing_data as prodata
import processing_pd as propd
import processing_simulation as prosim
import figure_style as fs

# ##########################################################
# ######### Fundamental parameters #########################
# ##########################################################
pathSim = '/media/gheed/Seagate_Expansion_Drive1/data/simulation'
pathData = '/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper'
path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/'

attenuation16dB = 6.31

# run3320 = propd.load_parquet_merge(path, [3320],             keep_col=[   'qdc_lg_ch1','amplitude_ch1'], full=False)
# run3319 = propd.load_parquet_merge(path, [3319],             keep_col=[   'qdc_lg_ch1','amplitude_ch1'], full=False)
run3317 = propd.load_parquet_merge(path, [3317],             keep_col=[   'qdc_lg_ch1','amplitude_ch1'], full=False)
# run3318 = propd.load_parquet_merge(path, [3318],             keep_col=[   'qdc_lg_ch1','amplitude_ch1'], full=False)
# run3314 = propd.load_parquet_merge(path, [3314],             keep_col=[   'qdc_lg_ch1','amplitude_ch1'], full=False)

#QDC
numBins=np.arange(0, 40000, 250)
plt.hist(run3317.qdc_lg_ch1, bins=numBins, histtype='step', lw=2, color='black', label='Good background, run3317')
for run in np.arange(3321, 3369, 1):
    plt.hist(propd.load_parquet_merge(path, [run], keep_col=['qdc_lg_ch1','amplitude_ch1'], full=False).qdc_lg_ch1, histtype='step', lw=1, bins=numBins, label=f'run: {run}')
plt.legend()
plt.show()


#RATE
rate = np.empty(0)
runs = np.arange(3321, 3422, 1)
# bg = np.append(rate, len(propd.load_parquet_merge(path, [3317], keep_col=['qdc_lg_ch1','amplitude_ch1'], full=False).query('qdc_lg_ch1>1000').qdc_lg_ch1)/3600)
for run in runs:
    rate = np.append(rate, len(propd.load_parquet_merge(path, [run], keep_col=['qdc_lg_ch1','amplitude_ch1'], full=False).query('qdc_lg_ch1>1000').qdc_lg_ch1)/(30*60))
    
plt.scatter(runs, rate, s=75)
# plt.scatter(3317, bg, color='black', s=75)

plt.ylabel('Count rate [Hz]')
plt.xlabel('Run number')
plt.show()