#!/usr/bin/env python3
from ctypes import cdll
from re import I
import sys
from tkinter import X
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
from numpy.core.arrayprint import SubArrayFormat
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit
import os

# IMPORT FROM LIBRARY DIRECTORY
sys.path.insert(0, "../library/")
sys.path.insert(0, "../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "../")
sys.path.insert(0, "data_analysis/")

import processing_math as promath
import processing_data as prodata
import processing_pd as propd
import processing_simulation as prosim
import figure_style as fs


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# LOAD SIMULATION DATA
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
path_sim = '/media/gheed/Seagate_Expansion_Drive1/data/simulation/'
columnNames = [ 'evtNum', 
                'optPhotonSum', 
                'optPhotonSumQE', 
                'optPhotonSumCompton', 
                'optPhotonSumComptonQE', 
                'optPhotonSumNPQE', 
                'xLoc', 
                'yLoc', 
                'zLoc', 
                'CsMin', 
                'optPhotonParentID', 
                'optPhotonParentCreatorProcess', 
                'optPhotonParentStartingEnergy',
                'total_edep',
                'gamma_edep',
                'proton_edep',
                'nScatters',
                'nScattersProton']

detector = 'EJ331'

############################################
#### NEUTRON SIMULATIONS ###################
############################################
numJobs = 100
numEnergies = 25
source = 'neutrons/neutron_range'
neutron_pen = pd.DataFrame()
#neutron_range
currentEnergyDir = os.listdir(path_sim + f'{detector}/{source}/')
for i, E in enumerate(np.arange(1000, 7250, 250)): #number of simulated energies
    currentJobDir = os.listdir(path_sim + f'{detector}/{source}/{currentEnergyDir[i]}/isotropic/')
    currentEnergy = pd.DataFrame()
    for j in np.arange(0, numJobs, 1): #number of jobs per energy
        # currentJobDir = os.listdir(path_sim + f'{detector}/neutrons/{source}/{currentEnergyDir[i]}/isotropic/')[j]
        try:
            currentEnergy = pd.concat([currentEnergy, pd.read_csv(path_sim + f"{detector}/{source}/{currentEnergyDir[i]}/isotropic/{currentJobDir[j]}/data/runana/CSV_optphoton_data_sum.csv", names=columnNames)])
        except OSError:
            print(f"{detector}/{source}/{currentEnergyDir[i]}/isotropic/{currentJobDir[j]}")
    #save current energy to disk
    print(f'Save to disk: {currentEnergyDir[i]}')
    currentEnergy.to_csv(path_sim + f"{detector}/{source}/{currentEnergyDir[i]}/isotropic/CSV_optphoton_data_sum.csv", header=False)
    # neutron_pen[f'keV{E}'] = currentEnergy









############################################
#### GAMMA SIMULATIONS #####################
############################################
Cs137_pen = pd.DataFrame()
Cs137_iso = pd.DataFrame()

Th232_pen = pd.DataFrame()
Th232_iso = pd.DataFrame()

AmBe_pen = pd.DataFrame()
AmBe_iso = pd.DataFrame()

for i in np.arange(0, 100, 1):
    # Cs137
    source = 'Cs137'
    print(i)
    try:
        Cs137_pen_Current = pd.read_csv(path_sim + f"{detector}/{source}/pencilbeam/{os.listdir(path_sim + f'{detector}/{source}/pencilbeam/')[i]}/data/runana/CSV_optphoton_data_sum.csv", names=columnNames)
    except OSError:
        print(f"Skipped: {detector}/{source}/pencilbeam/{os.listdir(path_sim + f'{detector}/{source}/pencilbeam/')[i]}")
    
    try:
        Cs137_iso_Current = pd.read_csv(path_sim + f"{detector}/{source}/isotropic/{os.listdir(path_sim + f'{detector}/{source}/isotropic/')[i]}/data/runana/CSV_optphoton_data_sum.csv", names=columnNames)
    except OSError:
        print(f"Skipped: {detector}/{source}/isotropic/{os.listdir(path_sim + f'{detector}/{source}/pencilbeam/')[i]}")
    
        
    #Th232
    source = 'Th232'
    try:
        Th232_pen_Current = pd.read_csv(path_sim + f"{detector}/{source}/pencilbeam/{os.listdir(path_sim + f'{detector}/{source}/pencilbeam/')[i]}/data/runana/CSV_optphoton_data_sum.csv", names=columnNames)
    except OSError:
        print(f"Skipped: {detector}/{source}/pencilbeam/{os.listdir(path_sim + f'{detector}/{source}/pencilbeam/')[i]}")

    try:
        Th232_iso_Current = pd.read_csv(path_sim + f"{detector}/{source}/isotropic/{os.listdir(path_sim + f'{detector}/{source}/isotropic/')[i]}/data/runana/CSV_optphoton_data_sum.csv", names=columnNames)
    except OSError:
        print(f"Skipped: {detector}/{source}/isotropic/{os.listdir(path_sim + f'{detector}/{source}/pencilbeam/')[i]}")
    
    #AmBe
    source = '4.44'
    try:
        AmBe_pen_Current = pd.read_csv(path_sim + f"{detector}/{source}/pencilbeam/{os.listdir(path_sim + f'{detector}/{source}/pencilbeam/')[i]}/data/runana/CSV_optphoton_data_sum.csv", names=columnNames)
    except OSError:
        print(f"Skipped: {detector}/{source}/pencilbeam/{os.listdir(path_sim + f'{detector}/{source}/pencilbeam/')[i]}")
        
    try:
        AmBe_iso_Current = pd.read_csv(path_sim + f"{detector}/{source}/isotropic/{os.listdir(path_sim + f'{detector}/{source}/isotropic/')[i]}/data/runana/CSV_optphoton_data_sum.csv", names=columnNames)
    except OSError:
        print(f"Skipped: {detector}/{source}/isotropic/{os.listdir(path_sim + f'{detector}/{source}/pencilbeam/')[i]}")
    
    # #merge data
    Cs137_pen = pd.concat([Cs137_pen, Cs137_pen_Current])
    Cs137_iso = pd.concat([Cs137_iso, Cs137_iso_Current])

    Th232_pen = pd.concat([Th232_pen, Th232_pen_Current])
    Th232_iso = pd.concat([Th232_iso, Th232_iso_Current])

    AmBe_pen = pd.concat([AmBe_pen, AmBe_pen_Current])
    AmBe_iso = pd.concat([AmBe_iso, AmBe_iso_Current])

#save to disk
Cs137_pen.to_csv(path_sim + f"{detector}/Cs137/pencilbeam/CSV_optphoton_data_sum.csv", header=False)
Cs137_iso.to_csv(path_sim + f"{detector}/Cs137/isotropic/CSV_optphoton_data_sum.csv", header=False)

Th232_pen.to_csv(path_sim + f"{detector}/Th232/pencilbeam/CSV_optphoton_data_sum.csv", header=False)
Th232_iso.to_csv(path_sim + f"{detector}/Th232/isotropic/CSV_optphoton_data_sum.csv", header=False)

AmBe_pen.to_csv(path_sim + f"{detector}/4.44/pencilbeam/CSV_optphoton_data_sum.csv", header=False)
AmBe_iso.to_csv(path_sim + f"{detector}/4.44/isotropic/CSV_optphoton_data_sum.csv", header=False)



