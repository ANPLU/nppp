#!/usr/bin/env python3
from re import I
import sys
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
from numpy.core.arrayprint import SubArrayFormat
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit
import random
# from matplotlib.colors import LinearSegmentedColormap


# IMPORT FROM LIBRARY DIRECTORY
sys.path.insert(0, "../library/")
sys.path.insert(0, "../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "../")
sys.path.insert(0, "data_analysis/")

import processing_simulation as prosim
import processing_math as promath
import processing_data as prodata
import processing_pd as propd
import figure_style as fs

"""
Script to derive gain drift over time and calculate compensation values
"""

########################################
######## NE213A ########################
########################################
# detector = 'NE213A'
# gateName = 'qdc_lg_ch1'
# minQDC = 6200
# maxQDC = 9200

# path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop
# run1 = 2121
# runList = np.arange(run1, run1+141)

# df = propd.load_parquet_merge('/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/', [2121], keep_col=[f'qdc_sg_ch1'], full=False)
# plt.hist(df.qdc_sg_ch1, bins=np.arange(6200,9200,100))
# plt.show()

# gainOffsets = prodata.gainAlignCalculator(path, runList, minQDC, maxQDC, 100, gateName, verbose=False)
# plt.scatter(runList, (1-gainOffsets)*100, color='blue')
# plt.ylabel('Offset [%]')
# plt.xlabel('Run number')
# plt.show()


#QDC gain align test
# for i, run in enumerate(runList):
#     dfCurrent = propd.load_parquet_merge('/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/', [run], keep_col=[f'qdc_lg_ch1'], full=False)
#     plt.subplot(1, 2, 1)
#     plt.hist(dfCurrent.qdc_lg_ch1, bins=np.arange(0, 20000, 100), histtype='step')
#     plt.yscale('log')
#     plt.title('before')
#     plt.subplot(1, 2, 2)
#     plt.hist(dfCurrent.qdc_lg_ch1*gainOffsets[i], bins=np.arange(0, 20000, 100), histtype='step')
#     plt.yscale('log')
#     plt.title('after')
# plt.show()



########################################
######## EJ305 #########################
########################################
# detector = 'EJ305'
# colName = 'qdc_sg_ch1'
# minQDC = 7000 #8500
# maxQDC = 9200#11000

# path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop
# run1 = 3439
# numRuns = 167
# runList = np.arange(run1, run1+numRuns)

# df = propd.load_parquet_merge('/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/', [3439], keep_col=[f'qdc_sg_ch1'], full=False)
# plt.hist(df.qdc_sg_ch1, bins=np.arange(minQDC, maxQDC, 50))
# plt.show()

# gainOffsets = prodata.gainAlignCalculator(path, runList, minQDC, maxQDC, 75, colName, verbose=False)
# plt.scatter(runList, (1-gainOffsets)*100, color='blue')
# plt.ylabel('Offset [%]')
# plt.xlabel('Run number')
# plt.show()

# #QDC gain align test
# for i, run in enumerate(runList):
#     dfCurrent = propd.load_parquet_merge('/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/', [run], keep_col=[f'qdc_lg_ch1'], full=False)
#     plt.subplot(1, 2, 1)
#     plt.hist(dfCurrent.qdc_lg_ch1, bins=np.arange(0, 20000, 50), histtype='step')
#     plt.yscale('log')
#     plt.title('before')
#     plt.subplot(1, 2, 2)
#     plt.hist(dfCurrent.qdc_lg_ch1*gainOffsets[i], bins=np.arange(0, 20000, 100), histtype='step')
#     plt.yscale('log')
#     plt.title('after')
# plt.show()


########################################
######## EJ321P ########################
########################################
detector = 'EJ321P'
gateName = 'qdc_lg_ch1'
minQDC = 13000
maxQDC = 16700

path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop
run1 = 3607
numRuns = 140
runList = np.arange(run1, run1+numRuns)

# df1 = propd.load_parquet_merge('/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/', [3607], keep_col=[f'qdc_lg_ch1', 'amplitude_ch1'], full=False)
# df2 = propd.load_parquet_merge('/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/', [3607+140], keep_col=[f'qdc_lg_ch1', 'amplitude_ch1'], full=False)
# # df3 = propd.load_parquet_merge('/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/', [3750], keep_col=[f'qdc_lg_ch1', 'amplitude_ch1'], full=False)

# plt.hist(df1.qdc_lg_ch1, bins=np.arange(0, 20000, 100), alpha=0.5, label='old')
# plt.hist(df2.qdc_lg_ch1, bins=np.arange(0, 20000, 100), alpha=0.5, label='new')
# # plt.hist(df3.qdc_lg_ch1*2.00, bins=np.arange(0, 50000, 100), alpha=0.5, label='new small')
# plt.vlines([minQDC, maxQDC], ymin=0, ymax=2000, color='black')
# plt.yscale('log')
# plt.legend()
# plt.show()


gainOffsets = prodata.gainAlignCalculator(path, runList, minQDC, maxQDC, 100, gateName, verbose=False)
plt.scatter(runList, (1-gainOffsets)*100, color='blue')
plt.ylabel('Offset [%]')
plt.xlabel('Run number')
plt.show()


#QDC gain align test
for i, run in enumerate(runList):
    print(f'run {run}, ({i+1}/{len(runList)})')
    dfCurrent = propd.load_parquet_merge('/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/', [run], keep_col=[f'qdc_lg_ch1'], full=False)
    plt.subplot(1, 2, 1)
    plt.hist(dfCurrent.qdc_lg_ch1, bins=np.arange(0, 20000, 100), histtype='step')
    plt.yscale('log')
    plt.title('before')
    plt.subplot(1, 2, 2)
    plt.hist(dfCurrent.qdc_lg_ch1*gainOffsets[i], bins=np.arange(0, 20000, 100), histtype='step')
    plt.yscale('log')
    plt.title('after')
plt.show()



########################################
######## EJ331 #########################
########################################
# detector = 'EJ331'
# gateName = 'qdc_sg_ch1'
# minQDC = 5400
# maxQDC = 8200

# path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop
# run1 = 1881
# run2 = 2342
# runList1 = np.arange(run1, run1+59)
# runList2 = np.arange(run2, run2+79)
# runList = np.append(runList1, runList2)

# df = propd.load_parquet_merge('/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/', [1882], keep_col=[f'qdc_sg_ch1'], full=False)
# plt.hist(df.qdc_sg_ch1, bins=np.arange(minQDC, maxQDC, 90))
# plt.show()

# gainOffsets = prodata.gainAlignCalculator(path, runList2, minQDC, maxQDC, 100, gateName, verbose=False)
# plt.scatter(runList2, (1-gainOffsets)*100, color='blue')
# plt.ylabel('Offset [%]')
# plt.xlabel('Run number')
# plt.show()


#QDC gain align test
# for i, run in enumerate(runList2):
#     dfCurrent = propd.load_parquet_merge('/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/', [run], keep_col=[f'qdc_lg_ch1'], full=False)
#     plt.subplot(1, 2, 1)
#     plt.hist(dfCurrent.qdc_lg_ch1, bins=np.arange(0, 20000, 100), histtype='step')
#     plt.yscale('log')
#     plt.title('before')
#     plt.subplot(1, 2, 2)
#     plt.hist(dfCurrent.qdc_lg_ch1*gainOffsets[i], bins=np.arange(0, 20000, 100), histtype='step')
#     plt.yscale('log')
#     plt.title('after')
# plt.show()



