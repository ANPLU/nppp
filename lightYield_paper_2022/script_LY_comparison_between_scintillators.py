#!/usr/bin/env python3
from re import I
import sys
from tkinter import X
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
from numpy.core.arrayprint import SubArrayFormat
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit
from scipy import interpolate

# from matplotlib.colors import LinearSegmentedColormap


# IMPORT FROM LIBRARY DIRECTORY
sys.path.insert(0, "../library/")
sys.path.insert(0, "../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "../")
sys.path.insert(0, "data_analysis/")

import processing_math as promath
import processing_data as prodata
import processing_pd as propd
import processing_simulation as prosim
import figure_style as fs

pathSim = '/media/gheed/Seagate_Expansion_Drive1/data/simulation'
pathData = '/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper'

energy = np.arange(1000, 21000, 1000)
att_factor_6dB = 2.00 # 6dB
att_factor_12dB = 3.98 # 12dB
att_factor_16dB = 6.31 #16dB
att_factor_15dB = 5.62 # 15dB

detector = 'NE213A'
NE213A_cal = prodata.calibrateMyDetector(f'{pathData}/{detector}/Simcal/popt.npy', energy*att_factor_12dB, inverse=False)
detector = 'EJ305'
EJ305_cal = prodata.calibrateMyDetector(f'{pathData}/{detector}/Simcal/popt.npy', energy*att_factor_16dB, inverse=False)
detector = 'EJ331'
EJ331_cal = prodata.calibrateMyDetector(f'{pathData}/{detector}/Simcal/popt.npy', energy*att_factor_15dB, inverse=False)
detector = 'EJ321P'
EJ321P_cal = prodata.calibrateMyDetector(f'{pathData}/{detector}/Simcal/popt.npy', energy*att_factor_6dB, inverse=False)

plt.subplot(1,2,1)
plt.scatter(energy, NE213A_cal, label='NE 213A')
plt.scatter(energy, EJ305_cal, label='NE 305')
plt.scatter(energy, EJ331_cal, label='NE 331')
plt.scatter(energy, EJ321P_cal, label='EJ 321P')
plt.legend()

plt.subplot(1,2,2)
plt.scatter(energy, EJ321P_cal/NE213A_cal, label=f'ratio NE213A average = {np.round(np.average(EJ321P_cal/NE213A_cal),2)}')
plt.scatter(energy, EJ321P_cal/EJ305_cal, label=f'ratio EJ305 average = {np.round(np.average(EJ321P_cal/EJ305_cal),2)}')
plt.scatter(energy, EJ321P_cal/EJ331_cal, label=f'ratio EJ331 average = {np.round(np.average(EJ321P_cal/EJ331_cal),2)}')
plt.ylim([0,1])
plt.xlabel('energy [MeVee]')
plt.legend()
plt.show()