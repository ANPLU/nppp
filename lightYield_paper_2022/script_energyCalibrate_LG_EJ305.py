#!/usr/bin/env python3
import sys
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit

# IMPORT FROM LIBRARY DIRECTORY
sys.path.insert(0, "../library/")
sys.path.insert(0, "../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "sim_analysis/")
sys.path.insert(0, "data_analysis/")

import digidata
import nicholai_plot_helper as nplt
import processing_utility as prout
import processing_math as promath
import processing_data as prodata
import processing_pd as propd
import processing_simulation as prosim

"""
Script for running both data and simulation and calibrating data of EJ305 detector.
"""

PMT_gain = 4e6 #Nominal gain is 7'000'000 at nominal A/lm (model: 9821B p1)
att_factor_11dB = 3.55
att_factor_12dB = 3.98
att_factor_16dB = 6.31
att_factor_16_5dB = 6.68
att_factor_19dB = 8.91
q = 1.60217662E-19 #charge of a single electron
plot = False

path_sim = '/media/gheed/Seagate_Expansion_Drive1/data/simulation/'

##############################
# LOADING DATA
##############################
path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop
bg_runs =       list(range(3422, 3432))
Cs137_runs =    [3432]#[3240]
Th232_runs =    [3433]#[3241]
AmBe_runs =     [3434]#[3242]
PuBe_runs =     [3442]#list(range(2930, 2933))

CableLengthCorrections = 1#1.074 #0.97 #correction constant between inside cave and outside due to different cable length

df_bg =         propd.load_parquet_merge(path, bg_runs,     keep_col=['qdc_lg_ch1'], full=False)*CableLengthCorrections
df_Cs137 =      propd.load_parquet_merge(path, Cs137_runs,  keep_col=['qdc_lg_ch1'], full=False)*CableLengthCorrections
df_Th232 =      propd.load_parquet_merge(path, Th232_runs,  keep_col=['qdc_lg_ch1'], full=False)*CableLengthCorrections
df_AmBe =       propd.load_parquet_merge(path, AmBe_runs,   keep_col=['qdc_lg_ch1'], full=False)*CableLengthCorrections
df_PuBe =       propd.load_parquet_merge(path, PuBe_runs,   keep_col=['qdc_lg_ch1'], full=False)


# y,x = np.histogram(df_PuBe, bins=np.arange(0,25000,100))
# plt.step(prodata.getBinCenters(x), y, label='PuBe')

# y,x = np.histogram(df_AmBe*1, bins=np.arange(0,25000,100))
# plt.step(prodata.getBinCenters(x), y*2.99, label='AmBe')


# plt.yscale('log')
# plt.legend()
# plt.show()

###############################
# LOADING SIMULATION DATA
###############################
detector = 'EJ305'

path_sim = '/media/gheed/Seagate_Expansion_Drive1/data/simulation/'
names = ['evtNum', 
        'optPhotonSum', 
        'optPhotonSumQE', 
        'optPhotonSumCompton', 
        'optPhotonSumComptonQE', 
        'optPhotonSumNPQE', 
        'xLoc', 
        'yLoc', 
        'zLoc', 
        'CsMin', 
        'optPhotonParentID', 
        'optPhotonParentCreatorProcess', 
        'optPhotonParentStartingEnergy',
        'total_edep',
        'gamma_edep',
        'proton_edep',
        'nScatters',
        'nScattersProton']

############################################
# LOAD ISOTROPIC SIMULATIONS
############################################
Cs137_sim =         pd.read_csv(path_sim + f"{detector}/Cs137/isotropic/CSV_optphoton_data_sum.csv", names=names)
Th232_sim =         pd.read_csv(path_sim + f"{detector}/Th232/isotropic/CSV_optphoton_data_sum.csv", names=names)
AmBe_sim =          pd.read_csv(path_sim + f"{detector}/4.44/isotropic/CSV_optphoton_data_sum.csv", names=names)

Cs137_sim =         pd.concat([Cs137_sim, pd.read_csv(path_sim + f"{detector}/Cs137/isotropic/part2/CSV_optphoton_data_sum.csv", names=names)])
Cs137_sim =         pd.concat([Cs137_sim, pd.read_csv(path_sim + f"{detector}/Cs137/isotropic/part3/CSV_optphoton_data_sum.csv", names=names)])

Th232_sim =         pd.concat([Th232_sim, pd.read_csv(path_sim + f"{detector}/Th232/isotropic/part2/CSV_optphoton_data_sum.csv", names=names)])
Th232_sim =         pd.concat([Th232_sim, pd.read_csv(path_sim + f"{detector}/Th232/isotropic/part3/CSV_optphoton_data_sum.csv", names=names)])

AmBe_sim =          pd.concat([AmBe_sim, pd.read_csv(path_sim + f"{detector}/4.44/isotropic/part3/CSV_optphoton_data_sum.csv", names=names)])
AmBe_sim =          pd.concat([AmBe_sim, pd.read_csv(path_sim + f"{detector}/4.44/isotropic/part3/CSV_optphoton_data_sum.csv", names=names)])

############################################
# LOAD PENCILBEAM SIMULATIONS
############################################
Cs137_sim_pen = pd.read_csv(path_sim + f"{detector}/Cs137/pencilbeam/CSV_optphoton_data_sum.csv", names=names)
Th232_sim_pen = pd.read_csv(path_sim + f"{detector}/Th232/pencilbeam/CSV_optphoton_data_sum.csv", names=names)
AmBe_sim_pen = pd.read_csv(path_sim + f"{detector}/4.44/pencilbeam/CSV_optphoton_data_sum.csv", names=names)

Cs137_sim_pen = pd.concat([Cs137_sim_pen, pd.read_csv(path_sim + f"{detector}/Cs137/pencilbeam/part2/CSV_optphoton_data_sum.csv", names=names)])
Cs137_sim_pen = pd.concat([Cs137_sim_pen, pd.read_csv(path_sim + f"{detector}/Cs137/pencilbeam/part3/CSV_optphoton_data_sum.csv", names=names)])

Th232_sim_pen = pd.concat([Th232_sim_pen, pd.read_csv(path_sim + f"{detector}/Th232/pencilbeam/part2/CSV_optphoton_data_sum.csv", names=names)])
Th232_sim_pen = pd.concat([Th232_sim_pen, pd.read_csv(path_sim + f"{detector}/Th232/pencilbeam/part3/CSV_optphoton_data_sum.csv", names=names)])

AmBe_sim_pen = pd.concat([AmBe_sim_pen, pd.read_csv(path_sim + f"{detector}/4.44/pencilbeam/part2/CSV_optphoton_data_sum.csv", names=names)])
AmBe_sim_pen = pd.concat([AmBe_sim_pen, pd.read_csv(path_sim + f"{detector}/4.44/pencilbeam/part3/CSV_optphoton_data_sum.csv", names=names)])

###############################
# RANDOMIZE BINNING
###############################
y, x = np.histogram(Cs137_sim.optPhotonSumQE, bins=4096, range=[0, 4096])
Cs137_sim.optPhotonSumQE = prodata.getRandDist(x, y)
y, x = np.histogram(Th232_sim.optPhotonSumQE, bins=4096, range=[0, 4096])
Th232_sim.optPhotonSumQE = prodata.getRandDist(x, y)
y, x = np.histogram(AmBe_sim.optPhotonSumQE, bins=4096, range=[0, 4096])
AmBe_sim.optPhotonSumQE = prodata.getRandDist(x, y)

y, x = np.histogram(Cs137_sim_pen.optPhotonSumComptonQE, bins=4096, range=[0, 4096])
Cs137_sim_pen.optPhotonSumComptonQE = prodata.getRandDist(x, y)
y, x = np.histogram(Th232_sim_pen.optPhotonSumComptonQE, bins=4096, range=[0, 4096])
Th232_sim_pen.optPhotonSumComptonQE = prodata.getRandDist(x, y)
y, x = np.histogram(AmBe_sim_pen.optPhotonSumComptonQE, bins=4096, range=[0, 4096])
AmBe_sim_pen.optPhotonSumComptonQE = prodata.getRandDist(x, y)

###############################
# RESET INDEX
###############################
Cs137_sim =  Cs137_sim.reset_index()
Th232_sim =  Th232_sim.reset_index()
AmBe_sim =   AmBe_sim.reset_index()

Cs137_sim_pen =  Cs137_sim_pen.reset_index()
Th232_sim_pen =  Th232_sim_pen.reset_index()
AmBe_sim_pen =   AmBe_sim_pen.reset_index()

###############################
# CALIBRATING SIMULATION DATA
###############################
Cs137_sim.optPhotonSumQE =         prosim.chargeCalibration(Cs137_sim.optPhotonSumQE      * q * PMT_gain)
Th232_sim.optPhotonSumQE =         prosim.chargeCalibration(Th232_sim.optPhotonSumQE      * q * PMT_gain)
AmBe_sim.optPhotonSumQE =          prosim.chargeCalibration(AmBe_sim.optPhotonSumQE       * q * PMT_gain)

Cs137_sim_pen.optPhotonSumComptonQE =      prosim.chargeCalibration(Cs137_sim_pen.optPhotonSumComptonQE * q * PMT_gain) 
Th232_sim_pen.optPhotonSumComptonQE =      prosim.chargeCalibration(Th232_sim_pen.optPhotonSumComptonQE * q * PMT_gain) 
AmBe_sim_pen.optPhotonSumComptonQE =      prosim.chargeCalibration(AmBe_sim_pen.optPhotonSumComptonQE * q * PMT_gain) 



###############################
# PMT ALIGN Cs-137
###############################
res = 0.09
numBins = np.arange(0, 30000, 75)
Cs137_sim_smear = np.zeros(len(Cs137_sim.optPhotonSumQE))
for i in range(len(Cs137_sim.optPhotonSumQE)):
    Cs137_sim_smear[i] = prosim.gaussSmear(Cs137_sim.optPhotonSumQE[i], res)

gain = 0.53

# countsSim, binsSim = np.histogram(Cs137_sim_smear*gain, bins=numBins)
# binsSim = prodata.getBinCenters(binsSim)

# countsSimPen, binsSimPen = np.histogram(Cs137_sim_pen.optPhotonSumComptonQE*gain, bins=numBins)

# w = np.empty(len(df_bg))
# w.fill(1/len(bg_runs))
# countsBG, binsBG = np.histogram(df_bg.qdc_lg_ch1*att_factor_16dB, bins=numBins, weights=w)

# w = np.empty(len(df_Cs137))
# w.fill(1/len(Cs137_runs))
# counts, bins = np.histogram(df_Cs137.qdc_lg_ch1*att_factor_16dB, bins=numBins, weights=w)
# bins = prodata.getBinCenters(bins)

# plt.plot(bins, counts-countsBG, label='data-BG')
# plt.plot(binsSim, countsSimPen*5.37, label='sim pencilbeam', alpha=0.4)
# plt.plot(binsSim, countsSim*1.95, label='sim')

# plt.legend()
# plt.show()

Cs137_results = prosim.PMTGainAlign( model = Cs137_sim_smear, 
                                    data = df_Cs137.qdc_lg_ch1*att_factor_16dB, 
                                    bg = df_bg.qdc_lg_ch1*att_factor_16dB,
                                    scale = 1.95,
                                    gain = gain,
                                    data_weight = len(Cs137_runs),
                                    bg_weight = len(bg_runs),
                                    scaleBound = [1, 7],
                                    gainBound = [0.1, 2],
                                    stepSize = 1e-2,
                                    binRange = list(range(5710, 9640, 75)),
                                    plot = True)
Cs137_results['smear'] = res



###############################
# PMT ALIGN Th232 2615 keV
###############################
res = 0.0
numBins = np.arange(25000, 60000, 350)
Th232_sim_smear = np.zeros(len(Th232_sim.optPhotonSumQE))
for i in range(len(Th232_sim.optPhotonSumQE)):
    Th232_sim_smear[i] = prosim.gaussSmear(Th232_sim.optPhotonSumQE[i], res)

gain=0.54#1.131

# countsSimPen, binsSimPen = np.histogram(Th232_sim_pen.optPhotonSumComptonQE*gain, bins=numBins)

# countsSim, binsSim = np.histogram(Th232_sim_smear*gain, bins=numBins)
# binsSim = prodata.getBinCenters(binsSim)

# w = np.empty(len(df_bg))
# w.fill(1/len(bg_runs))
# countsBG, binsBG = np.histogram(df_bg.qdc_lg_ch1*att_factor_16dB, bins=numBins, weights=w)

# w = np.empty(len(df_Th232))
# w.fill(1/len(Th232_runs))
# counts, bins = np.histogram(df_Th232.qdc_lg_ch1*att_factor_16dB, bins=numBins, weights=w)
# bins = prodata.getBinCenters(bins)

# plt.plot(bins, counts-countsBG, label='data-BG')
# plt.plot(binsSim, countsSimPen*1, label='sim pencilbeam', alpha=0.4)
# plt.plot(binsSim, countsSim*0.25, label='sim')

# plt.legend()
# plt.show()

Th232_results = prosim.PMTGainAlign(model = Th232_sim_smear, 
                                    data = df_Th232.qdc_lg_ch1*att_factor_16dB, 
                                    bg = df_bg.qdc_lg_ch1*att_factor_16dB,
                                    scale = 0.23,
                                    gain = gain,
                                    data_weight = len(Th232_runs),
                                    bg_weight = len(bg_runs),
                                    scaleBound = [0.1, 5.0],
                                    gainBound = [0.1, 3.5],
                                    stepSize = 1e-2,
                                    binRange = list(range(33000, 43000, 350)),
                                    plot = True)
Th232_results['smear'] = res

###############################
# PMT ALIGN AmBe 4439 keV
###############################
res = 0.015
numBins = np.arange(35000, 100000, 450)
AmBe_sim_smear = np.zeros(len(AmBe_sim.optPhotonSumQE))
for i in range(len(AmBe_sim.optPhotonSumQE)):
    AmBe_sim_smear[i] = prosim.gaussSmear(AmBe_sim.optPhotonSumQE[i], res)

gain=0.51

# countsSimPen, binsSimPen = np.histogram(AmBe_sim_pen.optPhotonSumComptonQE*gain, bins=numBins)

# countsSim, binsSim = np.histogram(AmBe_sim_smear*gain, bins=numBins)
# binsSim = prodata.getBinCenters(binsSim)

# w = np.empty(len(df_bg))
# w.fill(1/len(bg_runs))
# countsBG, binsBG = np.histogram(df_bg.qdc_lg_ch1*att_factor_16dB, bins=numBins, weights=w)

# w = np.empty(len(df_AmBe))
# w.fill(1/len(AmBe_runs))
# counts, bins = np.histogram(df_AmBe.qdc_lg_ch1*att_factor_16dB, bins=numBins, weights=w)
# bins = prodata.getBinCenters(bins)

# plt.plot(bins, counts-countsBG, label='data-BG')
# plt.plot(binsSim, countsSimPen*10, label='sim pencilbeam', alpha=0.4)
# plt.plot(binsSim, countsSim*0.8, label='sim')

# plt.legend()
# plt.show()

AmBe_results = prosim.PMTGainAlign( model = AmBe_sim_smear, 
                                    data = df_AmBe.qdc_lg_ch1*att_factor_16dB, 
                                    bg = df_bg.qdc_lg_ch1*att_factor_16dB,
                                    scale = 0.8,
                                    gain = gain,
                                    data_weight = len(AmBe_runs),
                                    bg_weight = len(bg_runs),
                                    scaleBound = [0.1 , 7.0],
                                    gainBound = [0.1, 2.0],
                                    stepSize = 1e-2,
                                    binRange = list(range(59300, 66800, 450)),
                                    plot = True)
AmBe_results['smear'] = res



#############################
## FITTING PENCILBEAM DATA ##
#############################

#Apply gain correction for pencilbeams
Cs137_sim_pen.optPhotonSumComptonQE = Cs137_sim_pen.optPhotonSumComptonQE * Cs137_results['gain']
Th232_sim_pen.optPhotonSumComptonQE = Th232_sim_pen.optPhotonSumComptonQE * Th232_results['gain'] 
AmBe_sim_pen.optPhotonSumComptonQE =  AmBe_sim_pen.optPhotonSumComptonQE * AmBe_results['gain'] 

#Apply smearing on pencilbeam data
Cs137_sim_pen.optPhotonSumComptonQE = Cs137_sim_pen.optPhotonSumComptonQE.apply(lambda x: prosim.gaussSmear(x, Cs137_results['smear'])) 
Th232_sim_pen.optPhotonSumComptonQE = Th232_sim_pen.optPhotonSumComptonQE.apply(lambda x: prosim.gaussSmear(x, Th232_results['smear'])) 
AmBe_sim_pen.optPhotonSumComptonQE = AmBe_sim_pen.optPhotonSumComptonQE.apply(lambda x: prosim.gaussSmear(x, AmBe_results['smear'])) 


start = 5000
stop = 9500
counts, bin_edges = np.histogram(Cs137_sim_pen.optPhotonSumComptonQE, bins = np.arange(start*0.8, stop*1.2, 250))
bin_centers = prodata.getBinCenters(bin_edges)
Ef = 0.66166
Cs137_sim_fit  =  prosim.calibrationFitBinned(bin_centers, counts, start, stop, Ef=Ef, plot=True, compton=True)
Cs137_sim_fit['CE'] = promath.comptonMax(Ef)
mean = Cs137_sim_fit['gauss_mean']
std = Cs137_sim_fit['gauss_std']
Cs137_sim_results = {}
Cs137_sim_results['mean'] = np.mean(Cs137_sim_pen.query(f'optPhotonSumComptonQE>{mean-3*std} and optPhotonSumComptonQE<{mean+3*std}').optPhotonSumComptonQE)
Cs137_sim_results['mean_err']  = np.std(Cs137_sim_pen.query(f'optPhotonSumComptonQE>{mean-3*std} and optPhotonSumComptonQE<{mean+3*std}').optPhotonSumComptonQE)/np.sqrt(len(Cs137_sim_pen.query(f'optPhotonSumComptonQE>{mean-3*std} and optPhotonSumComptonQE<{mean+3*std}').optPhotonSumComptonQE))

start = 37000
stop = 45000
counts, bin_edges = np.histogram(Th232_sim_pen.optPhotonSumComptonQE, bins = np.arange(start*0.8, stop*1.2, 1000))
bin_centers = prodata.getBinCenters(bin_edges)
Ef = 2.614533
Th232_sim_fit = prosim.calibrationFitBinned(bin_centers, counts, start, stop, Ef=Ef, plot=True, compton=True)
Th232_sim_fit['CE'] = promath.comptonMax(Ef)
mean = Th232_sim_fit['gauss_mean']
std = Th232_sim_fit['gauss_std']
Th232_sim_results = {}
Th232_sim_results['mean'] = np.mean(Th232_sim_pen.query(f'optPhotonSumComptonQE>{mean-3*std} and optPhotonSumComptonQE<{mean+3*std}').optPhotonSumComptonQE)
Th232_sim_results['mean_err']  = np.std(Th232_sim_pen.query(f'optPhotonSumComptonQE>{mean-3*std} and optPhotonSumComptonQE<{mean+3*std}').optPhotonSumComptonQE)/np.sqrt(len(Th232_sim_pen.query(f'optPhotonSumComptonQE>{mean-3*std} and optPhotonSumComptonQE<{mean+3*std}').optPhotonSumComptonQE))

start = 60000
stop = 70000
counts, bin_edges = np.histogram(AmBe_sim_pen.optPhotonSumComptonQE, bins = np.arange(start*0.8, stop*1.2, 1300))
bin_centers = prodata.getBinCenters(bin_edges)
Ef = 4.439
AmBe_sim_fit = prosim.calibrationFitBinned(bin_centers, counts, start, stop, Ef=Ef, plot=True, compton=True)
AmBe_sim_fit['CE'] = promath.comptonMax(Ef)
mean = AmBe_sim_fit['gauss_mean']
std = AmBe_sim_fit['gauss_std']
AmBe_sim_results = {}
AmBe_sim_results['mean'] = np.mean(AmBe_sim_pen.query(f'optPhotonSumComptonQE>{mean-3*std} and optPhotonSumComptonQE<{mean+3*std}').optPhotonSumComptonQE)
AmBe_sim_results['mean_err']  = np.std(AmBe_sim_pen.query(f'optPhotonSumComptonQE>{mean-3*std} and optPhotonSumComptonQE<{mean+3*std}').optPhotonSumComptonQE)/np.sqrt(len(AmBe_sim_pen.query(f'optPhotonSumComptonQE>{mean-3*std} and optPhotonSumComptonQE<{mean+3*std}').optPhotonSumComptonQE))









##################################################
#Calculate calibration constant
CE_energy = np.array([0, Cs137_sim_fit['CE'], Th232_sim_fit['CE'], AmBe_sim_fit['CE']]) #energies in MeVee of Compton edges
QDC_loc = np.array([0, Cs137_sim_results['mean'], Th232_sim_results['mean'], AmBe_sim_results['mean']]) #relevant QDC channels
QDC_loc_err = np.array([0, Cs137_sim_results['mean_err'], Th232_sim_results['mean_err'], AmBe_sim_results['mean_err']]) #relevant QDC channels errors from gain-align above.

# CE_energy = np.array([Cs137_sim_fit['CE'], Th232_sim_fit['CE'], AmBe_sim_fit['CE']]) #energies in MeVee of Compton edges
# QDC_loc = np.array([Cs137_sim_results['mean'], Th232_sim_results['mean'], AmBe_sim_results['mean']]) #relevant QDC channels
# QDC_loc_err = np.array([Cs137_sim_results['mean_err'], Th232_sim_results['mean_err'], AmBe_sim_results['mean_err']]) #relevant QDC channels errors from gain-align above.

popt, pcov = curve_fit(promath.linearFunc, CE_energy, QDC_loc)
pcov = np.sqrt(np.diag(pcov))
E_err = promath.errorPropLinearFuncInv(promath.linearFuncInv(QDC_loc, popt[0], popt[1]), popt[0], pcov[0], popt[1], pcov[1])

# popt, pcov = curve_fit(promath.quadraticFunc, QDC_loc, CE_energy)


plt.scatter(QDC_loc, CE_energy)
# plt.errorbar(QDC_loc, CE_energy, yerr = E_err, ls='none')
plt.plot(np.arange(0, 80000, 1000), promath.linearFuncInv(np.arange(0,80000,1000), popt[0], popt[1]))
# plt.plot(np.arange(0, 150000, 1000), promath.quadraticFunc(np.arange(0, 150000, 1000), popt[0], popt[1], popt[2]))
plt.ylabel('MeV$_{ee}$')
plt.xlabel('QDC [channels]')
plt.show()

np.save(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal/popt_LG', popt)
np.save(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal/pcov_LG', pcov)


