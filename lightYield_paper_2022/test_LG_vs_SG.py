#!/usr/bin/env python3
from re import I
import sys
from tkinter import X
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
from numpy.core.arrayprint import SubArrayFormat
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit

# from matplotlib.colors import LinearSegmentedColormap


# IMPORT FROM LIBRARY DIRECTORY
sys.path.insert(0, "../library/")
sys.path.insert(0, "../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "../")
sys.path.insert(0, "data_analysis/")

import processing_math as promath
import processing_data as prodata
import processing_pd as propd
import processing_simulation as prosim
import figure_style as fs

# ##########################################################
pathSim = '/media/gheed/Seagate_Expansion_Drive1/data/simulation'
pathData = '/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper'
path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop

#NE213A
detector = 'NE213A'
run1 = 2121+1

#EJ305
detector = 'EJ305'
run1 = 3439+1

#EJ321P
detector = 'EJ321P'
run1 = 2423+1

#EJ331
detector = 'EJ331'
run1 = 2342+1


PuBe_data = propd.load_parquet_merge(path, [run1], keep_col=['qdc_lg_ch1','qdc_sg_ch1'], full=False)
plt.hist(PuBe_data.qdc_lg_ch1/PuBe_data.qdc_sg_ch1, bins=200, range=[0,4])
plt.show()

np.average(PuBe_data.qdc_lg_ch1/PuBe_data.qdc_sg_ch1)