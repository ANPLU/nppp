#!/usr/bin/env python3
from re import I
import sys
from tkinter import X
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
from numpy.core.arrayprint import SubArrayFormat
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit

# IMPORT FROM LIBRARY DIRECTORY
sys.path.insert(0, "../library/")
sys.path.insert(0, "../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "../")
sys.path.insert(0, "data_analysis/")

import processing_math as promath
import processing_data as prodata
import processing_pd as propd
import processing_simulation as prosim
import figure_style as fs

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# FUNCTIONS
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def loadNeutronSimEnergies(path, columnNames, detector='NE213A', E_range=np.arange(1000,7250,250), keepCol='optPhotonSumNPQE', PMT_gain=4e6):
        """
        Routine for loading simulated neutron data and merging into one pandas DataFrame.
        For both isotropic and pencilbeam data.
        """
        
        q = 1.60217662E-19 #charge of a single electron

        # sim_pen = pd.DataFrame()
        sim_iso = pd.DataFrame()
        
        for E in E_range:
                print(f'Simulation Load: Energy = {E} keV')
                # #Load data                
                # pen_data = pd.read_csv(path + f"{detector}/neutrons/pencilbeam/{E}keV/CSV_optphoton_data_sum.csv", names=columnNames)[f'{keepCol}']
                # #Randomize data
                # y, x = np.histogram(pen_data, bins=4096, range=[0, 4096])
                # pen_data = prodata.getRandDist(x, y)
                # #QDC calibrate data
                # # pen_data = prosim.chargeCalibration(pen_data * q * PMT_gain)
                # #Save data to DataFrame
                # sim_pen[f'keV{E}'] = pd.Series(pen_data)

                #Load data                
                iso_data = pd.read_csv(path + f"{detector}/neutrons/neutron_range/{E}keV/isotropic/CSV_optphoton_data_sum.csv", names=columnNames)[f'{keepCol}']
                #Randomize data
                y, x = np.histogram(iso_data, bins=4096, range=[0, 4096])
                iso_data = prodata.getRandDist(x, y)
                #QDC calibrate data
                # iso_data = prosim.chargeCalibration(iso_data * q * PMT_gain)
                #Save data to DataFrame
                sim_iso[f'keV{E}'] = pd.Series(iso_data)
        
        #Reset index before return
        # sim_pen =  sim_pen.reset_index()
        sim_iso =  sim_iso.reset_index()
        
        return sim_iso

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# SETTING PARAMETERS
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
PMT_gain = 4e6 #Nominal gain is 7'000'000 at nominal A/lm (model: 9821B p1)
att_factor_11dB = 3.55
att_factor_12dB = 3.98
att_factor_16_5dB = 6.68
q = 1.60217662E-19 #charge of a single electron
distance = 0.959

path_sim = '/media/gheed/Seagate_Expansion_Drive1/data/simulation/'
columnNames = [ 'evtNum', 
                'optPhotonSum', 
                'optPhotonSumQE', 
                'optPhotonSumCompton', 
                'optPhotonSumComptonQE', 
                'optPhotonSumNPQE', 
                'xLoc', 
                'yLoc', 
                'zLoc', 
                'CsMin', 
                'optPhotonParentID', 
                'optPhotonParentCreatorProcess', 
                'optPhotonParentStartingEnergy',
                'total_edep',
                'gamma_edep',
                'proton_edep',
                'nScatters',
                'nScattersProton']




#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# LOAD SIMULATION DATA
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#NE213A
print('------ NE213A ------')
NE213A_sim = loadNeutronSimEnergies(     path = path_sim, 
                                        columnNames = columnNames, 
                                        detector = 'NE213A', 
                                        E_range = np.arange(1000, 6500, 250),
                                        keepCol='optPhotonSumNPQE')

#EJ305
print('------ EJ305 ------')
EJ305_sim = loadNeutronSimEnergies(     path = path_sim, 
                                        columnNames = columnNames, 
                                        detector = 'EJ305', 
                                        E_range = np.arange(1000, 6500, 250),
                                        keepCol='optPhotonSumNPQE')

#EJ331
print('------ EJ331 ------')
EJ331_sim = loadNeutronSimEnergies(     path = path_sim, 
                                        columnNames = columnNames, 
                                        detector = 'EJ331', 
                                        E_range = np.arange(1000, 6500, 250),
                                        keepCol='optPhotonSumNPQE')

#EJ321P
print('------ EJ321P ------')
EJ321P_sim = loadNeutronSimEnergies(    path = path_sim, 
                                        columnNames = columnNames, 
                                        detector = 'EJ321P', 
                                        E_range = np.arange(1000, 6500, 250),
                                        keepCol='optPhotonSumNPQE')

numBins = np.arange(15, 1000, 25)
plt.figure()
for i, E in enumerate(np.arange(1000, 6500, 250)):
    plt.subplot(5, 5, i+1)
    plt.hist(NE213A_sim[f'keV{E}'], numBins)
plt.suptitle('NE213A')

plt.figure()
for i, E in enumerate(np.arange(1000, 6500, 250)):
    plt.subplot(5, 5, i+1)
    plt.hist(EJ305_sim[f'keV{E}'], numBins)
plt.suptitle('EJ305')

plt.figure()
for i, E in enumerate(np.arange(1000, 6500, 250)):
    plt.subplot(5, 5, i+1)
    plt.hist(EJ331_sim[f'keV{E}'], numBins)
plt.suptitle('EJ331')

plt.figure()
for i, E in enumerate(np.arange(1000, 6500, 250)):
    plt.subplot(5, 5, i+1)
    plt.hist(EJ321P_sim[f'keV{E}'], numBins)
plt.suptitle('EJ321P')
plt.show()