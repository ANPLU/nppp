#!/usr/bin/env python3
from re import I
import sys
from tkinter import X
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
from numpy.core.arrayprint import SubArrayFormat
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit
# from matplotlib.colors import LinearSegmentedColormap


# IMPORT FROM LIBRARY DIRECTORY
sys.path.insert(0, "../library/")
sys.path.insert(0, "../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "../")
sys.path.insert(0, "data_analysis/")

import processing_math as promath
import processing_data as prodata
import processing_pd as propd
import processing_simulation as prosim
import figure_style as fs

pathSim = '/media/gheed/Seagate_Expansion_Drive1/data/simulation'
pathData = '/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper'

detector = 'EJ305'
HH_data_LG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/HH_data_LG.pkl')
TP_data_LG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/TP_data_LG.pkl')
FD_data_LG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/FD_data_LG.pkl')

HH_data_SG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/HH_data_SG.pkl')
TP_data_SG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/TP_data_SG.pkl')
FD_data_SG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/FD_data_SG.pkl')

LY_sim = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/simulation/sim_LightYield.pkl')
LY_sim_error = np.load(f'{pathData}/{detector}/errors/simulation_errors.npy')
LY_sim['means_err'] = LY_sim_error

# kornilov_LG = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/HH_TP_FD/kornilov_LG_result.pkl')
# cecil_SG = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/HH_TP_FD/cecil_SG_result.pkl')
# kornilov_SMD = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/simulation/kornilov_result.pkl')
# cecil_SMD = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/simulation/cecil_result.pkl')


Julius_E = np.array([2, 2.2, 2.4, 2.6, 2.8, 3.0, 3.2, 3.4, 3.6, 3.8, 4.0, 4.2, 4.4, 4.6, 4.8, 5.0, 5.2, 5.4, 5.6, 5.8])
Julius_HH_LG_LY = np.array([0.655151539173894, 0.7150465373541994, 0.7918484860496036, 0.9024066820650116, 0.987647692590417, 1.115084012265827, 1.2171887330236846, 1.2855516198890884, 1.4298516497969507, 1.607907927024816, 1.7015879993802225, 1.769936472818077, 1.9142365027259391, 2.092321606808903, 2.2197435130567635, 2.338740770902172, 2.483040800810034, 2.517647440355435, 2.7041571928408503, 2.8568962845787134])
Julius_TP_LG_LY = np.array([0.6886097574637304, 0.7444127037472685, 0.7752618607516935, 0.8977202711028949, 0.9535652271326942, 1.1093093597714665, 1.2068139808435552, 1.2876127261524668, 1.4267770122668453, 1.5576093653727658, 1.7217274407662573, 1.7359127317537661, 1.9083627401557155, 2.039195093261636, 2.153405590096902, 2.309149722735674, 2.398280401053044, 2.570772419201254, 2.643281241247969, 2.8323951156668343])
Julius_FD_LG_LY = np.array([0.5813888888888892, 0.6847222222222222, 0.7632638888888894, 0.8334027777777782, 0.9450000000000003, 1.0152083333333337, 1.085555555555556, 1.1886805555555555, 1.4159722222222224, 1.4696527777777784, 1.54, 1.6928472222222224, 1.8126388888888891, 1.9242361111111113, 2.0605555555555557, 2.188541666666667, 2.325, 2.37875, 2.5233333333333334, 2.701041666666667])

Julius_HH_SG_LY = np.array([0.4948093644038787, 0.571596899671734, 0.6146281876195858, 0.7251863836349943, 0.7935348570728489, 0.8703368057682539, 1.0483930829961192, 1.116755969861523, 1.2273141658769307, 1.3294188866347887, 1.4146454837326452, 1.4998864942580505, 1.6273228139334603, 1.6788075771388624, 1.7809122978967202, 1.8745923702521268, 1.9091845963699778, 1.9606693595753804, 2.1471791120607957, 2.384308822098667])
Julius_TP_SG_LY = np.array([0.49693328852293694,0.5278244552736227, 0.6002912675740766, 0.6560942138576138, 0.745224892174984, 0.8677253122724466, 1.0234694449112185, 1.0709824679325597, 1.2101047443006774, 1.2409539013051019, 1.346748445639388, 1.4276312104408218, 1.5250938217666492, 1.5809387777964483, 1.686775331876995, 1.7758640004481034, 1.8067551671987896, 1.8542261804738693, 2.0516719879011927, 2.2991514031255247])
Julius_FD_SG_LY = np.array([0.5070138888888889, 0.5525000000000002, 0.6227777777777779, 0.6515972222222222, 0.7962500000000001, 0.8664583333333331, 0.9532638888888889, 1.0068055555555562, 1.1350000000000002, 1.2713888888888891, 1.325138888888889, 1.3870138888888892, 1.523402777777778, 1.618541666666667, 1.680416666666667, 1.8580555555555558, 1.8292361111111115, 1.9820833333333334, 2.0687500000000005, 2.3125694444444447])


#Define functions
def kornilovFull(Ep, L0, L1):
    return L0*Ep**2/(Ep+L1)

def kornilovRestricted_NE213(Ep, L0):
    """
    Fitting parameter L1 determined for each detector using LG HH method data-set
    """
    L1 = 2.47 

    return L0*Ep**2/(Ep+L1)

def kornilovRestricted_NE213A(Ep, L0):
    """
    Fitting parameter L1 determined for each detector using LG HH method data-set
    """
    L1 = 2.47

    return L0*Ep**2/(Ep+L1)

def kornilovRestricted_EJ305(Ep, L0):
    """
    Fitting parameter L1 determined for each detector using average if all fitted methods
    """
    L1 = 8.5700387 #+/- 0.6495035

    return L0*Ep**2/(Ep+L1)

def kornilovRestricted_EJ321P(Ep, L0):
    """
    Fitting parameter L1 determined for each detector using average if all fitted methods
    """
    L1 = 6.617600503137628 # +/- 1.1217571647413835

    return L0*Ep**2/(Ep+L1)

def kornilovRestricted_EJ331(Ep, L0):
    """
    Fitting parameter L1 determined for each detector using average if all fitted methods
    """
    L1 = 7.666969223160507 # +/- 1.0443771496741985

    return L0*Ep**2/(Ep+L1)


######################################################


def cecilFull(Ep, p1, p2, p3, p4):
    return (p1*Ep-p2*(1-np.exp(-p3*np.power(Ep, p4))))

def cecilRestriced_NE213(Ep, C):
    """
    Fitting paramters p1, p2 and p3 determined for each detector using LG HH method data-set
    """
    p1 = 0.83 # +/- 0.
    p2 = 2.82 # +/- 0.
    p3 = 0.25 # +/- 0.
    p4 = 0.93 # +/- 0.    

    return C*(p1*Ep-p2*(1-np.exp(-p3*np.power(Ep,p4))))

def cecilRestriced_NE213A(Ep, C):
    """
    Fitting paramters taken from Table 1 in https://doi.org/10.1016/0029-554X(79)90417-8 
    """
    p1 = 0.83 # +/- 0.
    p2 = 2.82 # +/- 0.
    p3 = 0.25 # +/- 0.
    p4 = 0.93 # +/- 0. 
    
    return C*(p1*Ep-p2*(1-np.exp(-p3*np.power(Ep,p4))))

def cecilRestriced_EJ305(Ep, C):
    """
    Fitting paramters taken from Table 1 for NE-224 in https://doi.org/10.1016/0029-554X(79)90417-8 
    """
    p1 = 1
    p2 = 8.2
    p3 = 0.1 
    p4 = 0.88 

    return C*(p1*Ep-p2*(1-np.exp(-p3*np.power(Ep,p4))))

# def cecilRestriced_EJ321P(Ep, p1):
#     """
#     Fitting parameter p1, p2 and p3 determined for each detector using LG HH method data-set
#     """
#     # p1 = 0.46857352 # +/- 0.00840129
#     p2 = 0.86276409 # +/- 0.04428133
#     p3 = 0.21511096 # +/- 0.02732681 
#     p4 = 2.1535176 # +/- 0.1849958

#     return (p1*Ep-p2*(1-np.exp(-p3*np.power(Ep,p4))))

# def cecilRestricted_EJ331(Ep, p1):
#     """
#     Fitting parameter p1, p2 and p3 determined for each detector using LG HH method data-set
#     """
#     # p1 = 0.5773861 # +/- 0.00902452
#     p2 = 1.01531984 # +/- 0.04904072
#     p3 = 0.27606416 # +/- 0.02465754
#     p4 = 1.9036763 # +/- 0.14400218

#     return (p1*Ep-p2*(1-np.exp(-p3*np.power(Ep,p4))))


#########################################################
#########################################################
#########################################################

#plot ratio between two functions
def kornilovTest(Ep):
    L0 = 0.76955288 # +/- 0.0130656
    L1 = 3.33332457 # +/- 0.13650993
    return L0*Ep**2/(Ep+L1)

def cecilTest(Ep):
    p1 = 0.63725119 # +/- 0.00950756
    p2 = 0.86957062 # +/- 0.04950511
    p3 = 0.44885415 # +/- 0.10135812
    p4 = 1.76079883 # +/- 0.35061535
    return (p1*Ep-p2*(1-np.exp(-p3*pow(Ep,p4))))
Tn = np.arange(2, 6.01, 0.01)

plt.subplot(2,1,1)
plt.plot(Tn, cecilTest(Tn), label='Cecil Eq.')
plt.plot(Tn, kornilovTest(Tn), label='Kornilov Eq.')
plt.legend()
plt.ylabel('Light Yield [MeVee]')
plt.subplot(2,1,2)
plt.plot(Tn, ((cecilTest(Tn)/kornilovTest(Tn))-1)*100, label='ratio C/K')
plt.ylabel('Ratio [%]')
plt.xlabel('Neutron Energy [MeV]')
plt.legend()
plt.show()

#########################################################
#########################################################
#########################################################


#Fit data NE213A Cecil :::::::::::::::::::::::::::::
#::::::::::::::::::::::::::::::::::::::::::::

p0 = [0.83, 2.82, 0.25, 0.93]#initial guesses
popt_HH_data_LG, pcov_HH_data_LG = curve_fit(cecilRestriced_NE213A, HH_data_LG.energy[2:-1], HH_data_LG.HH_loc[2:-1])
popt_TP_data_LG, pcov_TP_data_LG = curve_fit(cecilRestriced_NE213A, TP_data_LG.energy[2:-1], TP_data_LG.TP_loc[2:-1])
popt_FD_data_LG, pcov_FD_data_LG = curve_fit(cecilRestriced_NE213A, FD_data_LG.energy[2:-1], FD_data_LG.FD_loc[2:-1])
popt_HH_data_SG, pcov_HH_data_SG = curve_fit(cecilRestriced_NE213A, HH_data_SG.energy[2:-1], HH_data_SG.HH_loc[2:-1])
popt_TP_data_SG, pcov_TP_data_SG = curve_fit(cecilRestriced_NE213A, TP_data_SG.energy[2:-1], TP_data_SG.TP_loc[2:-1])
popt_FD_data_SG, pcov_FD_data_SG = curve_fit(cecilRestriced_NE213A, FD_data_SG.energy[2:-1], FD_data_SG.FD_loc[2:-1])
popt_HH_Julius_LG, pcov_HH_Julius_LG = curve_fit(cecilRestriced_NE213, Julius_E, Julius_HH_LG_LY)
popt_TP_Julius_LG, pcov_TP_Julius_LG = curve_fit(cecilRestriced_NE213, Julius_E, Julius_TP_LG_LY)
popt_FD_Julius_LG, pcov_FD_Julius_LG = curve_fit(cecilRestriced_NE213, Julius_E, Julius_FD_LG_LY)
popt_HH_Julius_SG, pcov_HH_Julius_SG = curve_fit(cecilRestriced_NE213, Julius_E, Julius_HH_SG_LY)
popt_TP_Julius_SG, pcov_TP_Julius_SG = curve_fit(cecilRestriced_NE213, Julius_E, Julius_TP_SG_LY)
popt_FD_Julius_SG, pcov_FD_Julius_SG = curve_fit(cecilRestriced_NE213, Julius_E, Julius_FD_SG_LY)

popt_sim, pocov_sim = curve_fit(cecilRestriced_NE213A, LY_sim.energy[4:-4],  LY_sim.means[4:-4])

#Plot
Tn = np.arange(2, 6.25, 0.01)
plt.subplot(3,1,1)
plt.title('HH')
plt.scatter(HH_data_LG.energy[2:-1],  HH_data_LG.HH_loc[2:-1])
plt.scatter(HH_data_SG.energy[2:-1],  HH_data_SG.HH_loc[2:-1])
plt.plot(Tn, cecilRestriced_NE213A(Tn, popt_HH_data_LG[0]))
plt.plot(Tn, cecilRestriced_NE213A(Tn, popt_HH_data_SG[0]))
plt.scatter(LY_sim.energy[4:-4],  LY_sim.means[4:-4])
plt.plot(Tn, cecilRestriced_NE213A(Tn, popt_sim[0]))
plt.scatter(Julius_E,  Julius_HH_LG_LY, color='black', lw=1, alpha=0.5)
plt.plot(Tn, cecilRestriced_NE213(Tn, popt_HH_Julius_LG[0]), color='black', lw=1, alpha=0.5)



plt.subplot(3,1,2)
plt.title('TP')
plt.scatter(TP_data_LG.energy[2:-1],  TP_data_LG.TP_loc[2:-1])
plt.scatter(TP_data_SG.energy[2:-1],  TP_data_SG.TP_loc[2:-1])
plt.plot(Tn, cecilRestriced_NE213A(Tn, popt_TP_data_LG[0]))
plt.plot(Tn, cecilRestriced_NE213A(Tn, popt_TP_data_SG[0]))
plt.scatter(Julius_E,  Julius_TP_LG_LY, color='black', lw=1, alpha=0.5)
plt.plot(Tn, cecilRestriced_NE213(Tn, popt_TP_Julius_LG[0]), color='black', lw=1, alpha=0.5)

plt.subplot(3,1,3)
plt.title('FD')
plt.scatter(FD_data_LG.energy[2:-1],  FD_data_LG.FD_loc[2:-1])
plt.scatter(FD_data_SG.energy[2:-1],  FD_data_SG.FD_loc[2:-1])
plt.plot(Tn, cecilRestriced_NE213A(Tn, popt_FD_data_LG[0]))
plt.plot(Tn, cecilRestriced_NE213A(Tn, popt_FD_data_SG[0]))
plt.scatter(Julius_E,  Julius_FD_LG_LY, color='black', lw=1, alpha=0.5)
plt.plot(Tn, cecilRestriced_NE213(Tn, popt_FD_Julius_LG[0]), color='black', lw=1, alpha=0.5)



print(f'Chi2/dof HH LG: {promath.chi2red(HH_data_LG.HH_loc[2:-1], cecilRestriced_NE213A(HH_data_LG.energy[2:-1], popt_HH_data_LG[0]), HH_data_LG.HH_loc_err[2:-1], 1)}')
print(f'Chi2/dof TP LG: {promath.chi2red(TP_data_LG.TP_loc[2:-1], cecilRestriced_NE213A(TP_data_LG.energy[2:-1], popt_TP_data_LG[0]), TP_data_LG.TP_loc_err[2:-1], 1)}')
print(f'Chi2/dof FD LG: {promath.chi2red(FD_data_LG.FD_loc[2:-1], cecilRestriced_NE213A(FD_data_LG.energy[2:-1], popt_FD_data_LG[0]), FD_data_LG.FD_loc_err[2:-1], 1)}')
print(f'Chi2/dof HH SG: {promath.chi2red(HH_data_SG.HH_loc[2:-1], cecilRestriced_NE213A(HH_data_SG.energy[2:-1], popt_HH_data_SG[0]), HH_data_SG.HH_loc_err[2:-1], 1)}')
print(f'Chi2/dof TP SG: {promath.chi2red(TP_data_SG.TP_loc[2:-1], cecilRestriced_NE213A(TP_data_SG.energy[2:-1], popt_TP_data_SG[0]), TP_data_SG.TP_loc_err[2:-1], 1)}')
print(f'Chi2/dof FD SG: {promath.chi2red(FD_data_SG.FD_loc[2:-1], cecilRestriced_NE213A(FD_data_SG.energy[2:-1], popt_FD_data_SG[0]), FD_data_SG.FD_loc_err[2:-1], 1)}')

print(f'Chi2/dof simulation: {promath.chi2red(LY_sim.means[4:-4], cecilRestriced_NE213A(LY_sim.energy[4:-4], popt_sim[0]), LY_sim.means_err[4:-4], 1)}')

print(f'Chi2/dof J. HH LG: {promath.chi2red(Julius_HH_LG_LY, cecilRestriced_NE213(Julius_E, popt_HH_Julius_LG[0]), Julius_HH_LG_LY*0.05, 1)}')
print(f'Chi2/dof J. TP LG: {promath.chi2red(Julius_TP_LG_LY, cecilRestriced_NE213(Julius_E, popt_TP_Julius_LG[0]), Julius_TP_LG_LY*0.05, 1)}')
print(f'Chi2/dof J. FD LG: {promath.chi2red(Julius_FD_LG_LY, cecilRestriced_NE213(Julius_E, popt_FD_Julius_LG[0]), Julius_FD_LG_LY*0.05, 1)}')
print(f'Chi2/dof J. HH SG: {promath.chi2red(Julius_HH_SG_LY, cecilRestriced_NE213(Julius_E, popt_HH_Julius_SG[0]), Julius_HH_SG_LY*0.05, 1)}')
print(f'Chi2/dof J. TP SG: {promath.chi2red(Julius_TP_SG_LY, cecilRestriced_NE213(Julius_E, popt_TP_Julius_SG[0]), Julius_TP_SG_LY*0.05, 1)}')
print(f'Chi2/dof J. FD SG: {promath.chi2red(Julius_FD_SG_LY, cecilRestriced_NE213(Julius_E, popt_FD_Julius_SG[0]), Julius_FD_SG_LY*0.05, 1)}')

print()

# print(popt_HH_Julius_LG)
# print(popt_TP_Julius_LG)
# print(popt_FD_Julius_LG)
# print(popt_HH_Julius_SG)
# print(popt_TP_Julius_SG)
# print(popt_FD_Julius_SG)

#Fit data NE213A Kornilov ::::::::::::::::::::::::::
#::::::::::::::::::::::::::::::::::::::::::::

p0 = [0.57, 2.47]#initial guesses
popt_HH_data_LG, pcov_HH_data_LG = curve_fit(kornilovRestricted_NE213A, HH_data_LG.energy[2:-1], HH_data_LG.HH_loc[2:-1])
popt_TP_data_LG, pcov_TP_data_LG = curve_fit(kornilovRestricted_NE213A, TP_data_LG.energy[2:-1], TP_data_LG.TP_loc[2:-1])
popt_FD_data_LG, pcov_FD_data_LG = curve_fit(kornilovRestricted_NE213A, FD_data_LG.energy[2:-1], FD_data_LG.FD_loc[2:-1])
popt_HH_data_SG, pcov_HH_data_SG = curve_fit(kornilovRestricted_NE213A, HH_data_SG.energy[2:-1], HH_data_SG.HH_loc[2:-1])
popt_TP_data_SG, pcov_TP_data_SG = curve_fit(kornilovRestricted_NE213A, TP_data_SG.energy[2:-1], TP_data_SG.TP_loc[2:-1])
popt_FD_data_SG, pcov_FD_data_SG = curve_fit(kornilovRestricted_NE213A, FD_data_SG.energy[2:-1], FD_data_SG.FD_loc[2:-1])
popt_HH_Julius_LG, pcov_HH_Julius_LG = curve_fit(kornilovRestricted_NE213, Julius_E, Julius_HH_LG_LY)
popt_TP_Julius_LG, pcov_TP_Julius_LG = curve_fit(kornilovRestricted_NE213, Julius_E, Julius_TP_LG_LY)
popt_FD_Julius_LG, pcov_FD_Julius_LG = curve_fit(kornilovRestricted_NE213, Julius_E, Julius_FD_LG_LY)
popt_HH_Julius_SG, pcov_HH_Julius_SG = curve_fit(kornilovRestricted_NE213, Julius_E, Julius_HH_SG_LY)
popt_TP_Julius_SG, pcov_TP_Julius_SG = curve_fit(kornilovRestricted_NE213, Julius_E, Julius_TP_SG_LY)
popt_FD_Julius_SG, pcov_FD_Julius_SG = curve_fit(kornilovRestricted_NE213, Julius_E, Julius_FD_SG_LY)

popt_sim, pocov_sim = curve_fit(kornilovRestricted_NE213A, LY_sim.energy[4:-4],  LY_sim.means[4:-4])

# print(popt_HH_Julius_LG)
# print(popt_TP_Julius_LG)
# print(popt_FD_Julius_LG)
# print(popt_HH_Julius_SG)
# print(popt_TP_Julius_SG)
# print(popt_FD_Julius_SG)


#Plot
Tn = np.arange(2, 6.25, 0.01)
plt.subplot(3,1,1)
plt.title('HH')
plt.scatter(HH_data_LG.energy[2:-1],  HH_data_LG.HH_loc[2:-1])
plt.scatter(HH_data_SG.energy[2:-1],  HH_data_SG.HH_loc[2:-1])
plt.plot(Tn, kornilovRestricted_NE213A(Tn, popt_HH_data_LG[0], popt_HH_data_LG[1]))
plt.plot(Tn, kornilovRestricted_NE213A(Tn, popt_HH_data_SG[0]))
plt.scatter(LY_sim.energy[4:-4],  LY_sim.means[4:-4])
plt.plot(Tn, kornilovRestricted_NE213A(Tn, popt_sim[0]))
plt.scatter(Julius_E,  Julius_HH_LG_LY, color='black', lw=1, alpha=0.5)
plt.plot(Tn, kornilovRestricted_NE213(Tn, popt_HH_Julius_LG[0]), color='black', lw=1, alpha=0.5)

plt.subplot(3,1,2)
plt.title('TP')
plt.scatter(TP_data_LG.energy[2:-1],  TP_data_LG.TP_loc[2:-1])
plt.scatter(TP_data_SG.energy[2:-1],  TP_data_SG.TP_loc[2:-1])
plt.plot(Tn, kornilovRestricted_NE213A(Tn, popt_TP_data_LG[0]))
plt.plot(Tn, kornilovRestricted_NE213A(Tn, popt_TP_data_SG[0]))
plt.scatter(Julius_E, Julius_TP_LG_LY, color='black', lw=1, alpha=0.5)
plt.plot(Tn, kornilovRestricted_NE213(Tn, popt_TP_Julius_LG[0]), color='black', lw=1, alpha=0.5)

plt.subplot(3,1,3)
plt.title('FD')
plt.scatter(FD_data_LG.energy[2:-1],  FD_data_LG.FD_loc[2:-1])
plt.scatter(FD_data_SG.energy[2:-1],  FD_data_SG.FD_loc[2:-1])
plt.plot(Tn, kornilovRestricted_NE213A(Tn, popt_FD_data_LG[0]))
plt.plot(Tn, kornilovRestricted_NE213A(Tn, popt_FD_data_SG[0]))
plt.scatter(Julius_E, Julius_FD_LG_LY, color='black', lw=1, alpha=0.5)
plt.plot(Tn, kornilovRestricted_NE213(Tn, popt_FD_Julius_LG[0]), color='black', lw=1, alpha=0.5)

print(f'Chi2/dof HH LG: {promath.chi2red(HH_data_LG.HH_loc[2:-1], kornilovRestricted_NE213A(HH_data_LG.energy[2:-1], popt_HH_data_LG[0]), HH_data_LG.HH_loc_err[2:-1], 1)}')
print(f'Chi2/dof TP LG: {promath.chi2red(TP_data_LG.TP_loc[2:-1], kornilovRestricted_NE213A(TP_data_LG.energy[2:-1], popt_TP_data_LG[0]), TP_data_LG.TP_loc_err[2:-1], 1)}')
print(f'Chi2/dof FD LG: {promath.chi2red(FD_data_LG.FD_loc[2:-1], kornilovRestricted_NE213A(FD_data_LG.energy[2:-1], popt_FD_data_LG[0]), FD_data_LG.FD_loc_err[2:-1], 1)}')
print(f'Chi2/dof HH SG: {promath.chi2red(HH_data_SG.HH_loc[2:-1], kornilovRestricted_NE213A(HH_data_SG.energy[2:-1], popt_HH_data_SG[0]), HH_data_SG.HH_loc_err[2:-1], 1)}')
print(f'Chi2/dof TP SG: {promath.chi2red(TP_data_SG.TP_loc[2:-1], kornilovRestricted_NE213A(TP_data_SG.energy[2:-1], popt_TP_data_SG[0]), TP_data_SG.TP_loc_err[2:-1], 1)}')
print(f'Chi2/dof FD SG: {promath.chi2red(FD_data_SG.FD_loc[2:-1], kornilovRestricted_NE213A(FD_data_SG.energy[2:-1], popt_FD_data_SG[0]), FD_data_SG.FD_loc_err[2:-1], 1)}')

print(f'Chi2/dof simulation: {promath.chi2red(LY_sim.means[4:-4], kornilovRestricted_NE213A(LY_sim.energy[4:-4], popt_sim[0]), LY_sim.means_err[4:-4], 1)}')

print(f'Chi2/dof J. HH LG: {promath.chi2red(Julius_HH_LG_LY, kornilovRestricted_NE213(Julius_E, popt_HH_Julius_LG[0]), Julius_HH_LG_LY*0.05, 1)}')
print(f'Chi2/dof J. TP LG: {promath.chi2red(Julius_TP_LG_LY, kornilovRestricted_NE213(Julius_E, popt_TP_Julius_LG[0]), Julius_TP_LG_LY*0.05, 1)}')
print(f'Chi2/dof J. FD LG: {promath.chi2red(Julius_FD_LG_LY, kornilovRestricted_NE213(Julius_E, popt_FD_Julius_LG[0]), Julius_FD_LG_LY*0.05, 1)}')
print(f'Chi2/dof J. HH SG: {promath.chi2red(Julius_HH_SG_LY, kornilovRestricted_NE213(Julius_E, popt_HH_Julius_SG[0]), Julius_HH_SG_LY*0.05, 1)}')
print(f'Chi2/dof J. TP SG: {promath.chi2red(Julius_TP_SG_LY, kornilovRestricted_NE213(Julius_E, popt_TP_Julius_SG[0]), Julius_TP_SG_LY*0.05, 1)}')
print(f'Chi2/dof J. FD SG: {promath.chi2red(Julius_FD_SG_LY, kornilovRestricted_NE213(Julius_E, popt_FD_Julius_SG[0]), Julius_FD_SG_LY*0.05, 1)}')


plt.show()


####################################
####################################
####################################
####################################
####################################

#Fit data EJ305 Cecil :::::::::::::::::::::::::::::
#::::::::::::::::::::::::::::::::::::::::::::

p0 = [0.83, 2.82, 0.25, 0.93]#initial guesses
popt_HH_data_LG, pcov_HH_data_LG = curve_fit(cecilRestriced_EJ305, HH_data_LG.energy[2:-1], HH_data_LG.HH_loc[2:-1])
popt_TP_data_LG, pcov_TP_data_LG = curve_fit(cecilRestriced_EJ305, TP_data_LG.energy[2:-1], TP_data_LG.TP_loc[2:-1])
popt_FD_data_LG, pcov_FD_data_LG = curve_fit(cecilRestriced_EJ305, FD_data_LG.energy[2:-1], FD_data_LG.FD_loc[2:-1])
popt_HH_data_SG, pcov_HH_data_SG = curve_fit(cecilRestriced_EJ305, HH_data_SG.energy[2:-1], HH_data_SG.HH_loc[2:-1])
popt_TP_data_SG, pcov_TP_data_SG = curve_fit(cecilRestriced_EJ305, TP_data_SG.energy[2:-1], TP_data_SG.TP_loc[2:-1])
popt_FD_data_SG, pcov_FD_data_SG = curve_fit(cecilRestriced_EJ305, FD_data_SG.energy[2:-1], FD_data_SG.FD_loc[2:-1])

popt_sim, pocov_sim = curve_fit(cecilRestriced_EJ305, LY_sim.energy[4:-4],  LY_sim.means[4:-4])

#Plot
Tn = np.arange(2, 6.25, 0.01)
plt.subplot(3,1,1)
plt.title('HH')
plt.scatter(HH_data_LG.energy[2:-1],  HH_data_LG.HH_loc[2:-1])
plt.scatter(HH_data_SG.energy[2:-1],  HH_data_SG.HH_loc[2:-1])
plt.plot(Tn, cecilRestriced_EJ305(Tn, popt_HH_data_LG[0]))
plt.plot(Tn, cecilRestriced_EJ305(Tn, popt_HH_data_SG[0]))
plt.scatter(LY_sim.energy[4:-4],  LY_sim.means[4:-4])
plt.plot(Tn, cecilRestriced_EJ305(Tn, popt_sim[0]))

plt.subplot(3,1,2)
plt.title('TP')
plt.scatter(TP_data_LG.energy[2:-1],  TP_data_LG.TP_loc[2:-1])
plt.scatter(TP_data_SG.energy[2:-1],  TP_data_SG.TP_loc[2:-1])
plt.plot(Tn, cecilRestriced_EJ305(Tn, popt_TP_data_LG[0]))
plt.plot(Tn, cecilRestriced_EJ305(Tn, popt_TP_data_SG[0]))

plt.subplot(3,1,3)
plt.title('FD')
plt.scatter(FD_data_LG.energy[2:-1],  FD_data_LG.FD_loc[2:-1])
plt.scatter(FD_data_SG.energy[2:-1],  FD_data_SG.FD_loc[2:-1])
plt.plot(Tn, cecilRestriced_EJ305(Tn, popt_FD_data_LG[0]))
plt.plot(Tn, cecilRestriced_EJ305(Tn, popt_FD_data_SG[0]))

print(f'Chi2/dof HH LG: {promath.chi2red(HH_data_LG.HH_loc[2:-1], cecilRestriced_EJ305(HH_data_LG.energy[2:-1], popt_HH_data_LG[0]), HH_data_LG.HH_loc_err[2:-1], 1)}')
print(f'Chi2/dof TP LG: {promath.chi2red(TP_data_LG.TP_loc[2:-1], cecilRestriced_EJ305(TP_data_LG.energy[2:-1], popt_TP_data_LG[0]), TP_data_LG.TP_loc_err[2:-1], 1)}')
print(f'Chi2/dof FD LG: {promath.chi2red(FD_data_LG.FD_loc[2:-1], cecilRestriced_EJ305(FD_data_LG.energy[2:-1], popt_FD_data_LG[0]), FD_data_LG.FD_loc_err[2:-1], 1)}')
print(f'Chi2/dof HH SG: {promath.chi2red(HH_data_SG.HH_loc[2:-1], cecilRestriced_EJ305(HH_data_SG.energy[2:-1], popt_HH_data_SG[0]), HH_data_SG.HH_loc_err[2:-1], 1)}')
print(f'Chi2/dof TP SG: {promath.chi2red(TP_data_SG.TP_loc[2:-1], cecilRestriced_EJ305(TP_data_SG.energy[2:-1], popt_TP_data_SG[0]), TP_data_SG.TP_loc_err[2:-1], 1)}')
print(f'Chi2/dof FD SG: {promath.chi2red(FD_data_SG.FD_loc[2:-1], cecilRestriced_EJ305(FD_data_SG.energy[2:-1], popt_FD_data_SG[0]), FD_data_SG.FD_loc_err[2:-1], 1)}')
print(f'Chi2/dof simulation: {promath.chi2red(LY_sim.means[4:-4], kornilovRestricted_EJ305(LY_sim.energy[4:-4], popt_sim[0]), LY_sim.means_err[4:-4], 1)}')

print(f'HH LG: L0={popt_HH_data_LG[0]}')
print(f'TP LG: L0={popt_TP_data_LG[0]}')
print(f'FD LG: L0={popt_FD_data_LG[0]}')
print(f'HH SG: L0={popt_HH_data_SG[0]}')
print(f'TP SG: L0={popt_TP_data_SG[0]}')
print(f'FD SG: L0={popt_FD_data_SG[0]}')

plt.show()


#Fit data EJ305 Kornilov ::::::::::::::::::::::::::
#::::::::::::::::::::::::::::::::::::::::::::

p0 = [0.57, 2.47]#initial guesses
popt_HH_data_LG, pcov_HH_data_LG = curve_fit(kornilovFull, HH_data_LG.energy[2:-1], HH_data_LG.HH_loc[2:-1])
popt_TP_data_LG, pcov_TP_data_LG = curve_fit(kornilovFull, TP_data_LG.energy[2:-1], TP_data_LG.TP_loc[2:-1])
popt_FD_data_LG, pcov_FD_data_LG = curve_fit(kornilovFull, FD_data_LG.energy[2:-1], FD_data_LG.FD_loc[2:-1])
popt_HH_data_SG, pcov_HH_data_SG = curve_fit(kornilovFull, HH_data_SG.energy[2:-1], HH_data_SG.HH_loc[2:-1])
popt_TP_data_SG, pcov_TP_data_SG = curve_fit(kornilovFull, TP_data_SG.energy[2:-1], TP_data_SG.TP_loc[2:-1])
popt_FD_data_SG, pcov_FD_data_SG = curve_fit(kornilovFull, FD_data_SG.energy[2:-1], FD_data_SG.FD_loc[2:-1])
popt_sim, pocov_sim = curve_fit(kornilovRestricted_EJ305, LY_sim.energy[4:-4],  LY_sim.means[4:-4])

# print([popt_HH_data_LG[1], popt_TP_data_LG[1], popt_FD_data_LG[1], popt_HH_data_SG[1], popt_TP_data_SG[1], popt_FD_data_SG[1]])
# print(f'L1 average: {np.average([popt_HH_data_LG[1], popt_TP_data_LG[1], popt_FD_data_LG[1], popt_HH_data_SG[1], popt_TP_data_SG[1], popt_FD_data_SG[1]])}')
# print(f'L1 err: {np.average([np.sqrt(np.diag(pcov_HH_data_LG))[1], np.sqrt(np.diag(pcov_TP_data_LG))[1], np.sqrt(np.diag(pcov_FD_data_LG))[1], np.sqrt(np.diag(pcov_HH_data_SG))[1], np.sqrt(np.diag(pcov_TP_data_SG))[1], np.sqrt(np.diag(pcov_FD_data_SG))[1] ])}')
# print(f'L1 std: {np.std([popt_HH_data_LG[1], popt_TP_data_LG[1], popt_FD_data_LG[1], popt_HH_data_SG[1], popt_TP_data_SG[1], popt_FD_data_SG[1]])}')

#Plot
Tn = np.arange(2, 6.25, 0.01)
plt.subplot(3,1,1)
plt.title('HH')
plt.scatter(HH_data_LG.energy[2:-1],  HH_data_LG.HH_loc[2:-1])
plt.scatter(HH_data_SG.energy[2:-1],  HH_data_SG.HH_loc[2:-1])
plt.plot(Tn, kornilovRestricted_EJ305(Tn, popt_HH_data_LG[0]))
plt.plot(Tn, kornilovRestricted_EJ305(Tn, popt_HH_data_SG[0]))
plt.scatter(LY_sim.energy[4:-4],  LY_sim.means[4:-4])
plt.plot(Tn, kornilovRestricted_EJ305(Tn, popt_sim[0]))

plt.subplot(3,1,2)
plt.title('TP')
plt.scatter(TP_data_LG.energy[2:-1],  TP_data_LG.TP_loc[2:-1])
plt.scatter(TP_data_SG.energy[2:-1],  TP_data_SG.TP_loc[2:-1])
plt.plot(Tn, kornilovRestricted_EJ305(Tn, popt_TP_data_LG[0]))
plt.plot(Tn, kornilovRestricted_EJ305(Tn, popt_TP_data_SG[0]))

plt.subplot(3,1,3)
plt.title('FD')
plt.scatter(FD_data_LG.energy[2:-1],  FD_data_LG.FD_loc[2:-1])
plt.scatter(FD_data_SG.energy[2:-1],  FD_data_SG.FD_loc[2:-1])
plt.plot(Tn, kornilovRestricted_EJ305(Tn, popt_FD_data_LG[0]))
plt.plot(Tn, kornilovRestricted_EJ305(Tn, popt_FD_data_SG[0]))

print(f'Chi2/dof HH LG: {promath.chi2red(HH_data_LG.HH_loc[2:-1], kornilovRestricted_EJ305(HH_data_LG.energy[2:-1], popt_HH_data_LG[0]), HH_data_LG.HH_loc_err[2:-1], 1)}')
print(f'Chi2/dof TP LG: {promath.chi2red(TP_data_LG.TP_loc[2:-1], kornilovRestricted_EJ305(TP_data_LG.energy[2:-1], popt_TP_data_LG[0]), TP_data_LG.TP_loc_err[2:-1], 1)}')
print(f'Chi2/dof FD LG: {promath.chi2red(FD_data_LG.FD_loc[2:-1], kornilovRestricted_EJ305(FD_data_LG.energy[2:-1], popt_FD_data_LG[0]), FD_data_LG.FD_loc_err[2:-1], 1)}')

print(f'Chi2/dof HH SG: {promath.chi2red(HH_data_SG.HH_loc[2:-1], kornilovRestricted_EJ305(HH_data_SG.energy[2:-1], popt_HH_data_SG[0]), HH_data_SG.HH_loc_err[2:-1], 1)}')
print(f'Chi2/dof TP SG: {promath.chi2red(TP_data_SG.TP_loc[2:-1], kornilovRestricted_EJ305(TP_data_SG.energy[2:-1], popt_TP_data_SG[0]), TP_data_SG.TP_loc_err[2:-1], 1)}')
print(f'Chi2/dof FD SG: {promath.chi2red(FD_data_SG.FD_loc[2:-1], kornilovRestricted_EJ305(FD_data_SG.energy[2:-1], popt_FD_data_SG[0]), FD_data_SG.FD_loc_err[2:-1], 1)}')
print(f'Chi2/dof simulation: {promath.chi2red(LY_sim.means[4:-4], kornilovRestricted_EJ305(LY_sim.energy[4:-4], popt_sim[0]), LY_sim.means_err[4:-4], 1)}')

plt.show()



####################################
####################################
####################################
####################################
####################################

#Fit data EJ321P Cecil :::::::::::::::::::::::::::::
#::::::::::::::::::::::::::::::::::::::::::::

# p0 = [0.83, 2.82, 0.25, 0.93]#initial guesses
# popt_HH_data_LG, pcov_HH_data_LG = curve_fit(cecilFull, HH_data_LG.energy[2:-1], HH_data_LG.HH_loc[2:-1], p0=p0)
# popt_TP_data_LG, pcov_TP_data_LG = curve_fit(cecilRestriced_EJ321P, TP_data_LG.energy[2:-1], TP_data_LG.TP_loc[2:-1])
# popt_FD_data_LG, pcov_FD_data_LG = curve_fit(cecilRestriced_EJ321P, FD_data_LG.energy[2:-1], FD_data_LG.FD_loc[2:-1])
# popt_HH_data_SG, pcov_HH_data_SG = curve_fit(cecilRestriced_EJ321P, HH_data_SG.energy[2:-1], HH_data_SG.HH_loc[2:-1])
# popt_TP_data_SG, pcov_TP_data_SG = curve_fit(cecilRestriced_EJ321P, TP_data_SG.energy[2:-1], TP_data_SG.TP_loc[2:-1])
# popt_FD_data_SG, pcov_FD_data_SG = curve_fit(cecilRestriced_EJ321P, FD_data_SG.energy[2:-1], FD_data_SG.FD_loc[2:-1])
# popt_sim, pocov_sim = curve_fit(cecilRestriced_EJ321P, LY_sim.energy[4:-4],  LY_sim.means[4:-4])

# #Plot
# Tn = np.arange(2, 6.25, 0.01)
# plt.subplot(3,1,1)
# plt.title('HH')
# plt.scatter(HH_data_LG.energy[2:-1],  HH_data_LG.HH_loc[2:-1])
# plt.scatter(HH_data_SG.energy[2:-1],  HH_data_SG.HH_loc[2:-1])
# plt.plot(Tn, cecilFull(Tn, popt_HH_data_LG[0], popt_HH_data_LG[1], popt_HH_data_LG[2], popt_HH_data_LG[3]))
# plt.plot(Tn, cecilRestriced_EJ321P(Tn, popt_HH_data_SG[0]))
# plt.scatter(LY_sim.energy[4:-4],  LY_sim.means[4:-4])
# plt.plot(Tn, cecilRestriced_EJ321P(Tn, popt_sim[0]))

# plt.subplot(3,1,2)
# plt.title('TP')
# plt.scatter(TP_data_LG.energy[2:-1],  TP_data_LG.TP_loc[2:-1])
# plt.scatter(TP_data_SG.energy[2:-1],  TP_data_SG.TP_loc[2:-1])
# plt.plot(Tn, cecilRestriced_EJ321P(Tn, popt_TP_data_LG[0]))
# plt.plot(Tn, cecilRestriced_EJ321P(Tn, popt_TP_data_SG[0]))

# plt.subplot(3,1,3)
# plt.title('FD')
# plt.scatter(FD_data_LG.energy[2:-1],  FD_data_LG.FD_loc[2:-1])
# plt.scatter(FD_data_SG.energy[2:-1],  FD_data_SG.FD_loc[2:-1])
# plt.plot(Tn, cecilRestriced_EJ321P(Tn, popt_FD_data_LG[0]))
# plt.plot(Tn, cecilRestriced_EJ321P(Tn, popt_FD_data_SG[0]))

# print(f'Chi2/dof HH LG: {promath.chi2red(HH_data_LG.HH_loc[2:-1], cecilFull(HH_data_LG.energy[2:-1], popt_HH_data_LG[0], popt_HH_data_LG[1], popt_HH_data_LG[2], popt_HH_data_LG[3]), HH_data_LG.HH_loc_err[2:-1], 4)}')
# print(f'Chi2/dof TP LG: {promath.chi2red(TP_data_LG.TP_loc[2:-1], cecilRestriced_EJ321P(TP_data_LG.energy[2:-1], popt_TP_data_LG[0]), TP_data_LG.TP_loc_err[2:-1], 1)}')
# print(f'Chi2/dof FD LG: {promath.chi2red(FD_data_LG.FD_loc[2:-1], cecilRestriced_EJ321P(FD_data_LG.energy[2:-1], popt_FD_data_LG[0]), FD_data_LG.FD_loc_err[2:-1], 1)}')
# print(f'Chi2/dof HH SG: {promath.chi2red(HH_data_SG.HH_loc[2:-1], cecilRestriced_EJ321P(HH_data_SG.energy[2:-1], popt_HH_data_SG[0]), HH_data_SG.HH_loc_err[2:-1], 1)}')
# print(f'Chi2/dof TP SG: {promath.chi2red(TP_data_SG.TP_loc[2:-1], cecilRestriced_EJ321P(TP_data_SG.energy[2:-1], popt_TP_data_SG[0]), TP_data_SG.TP_loc_err[2:-1], 1)}')
# print(f'Chi2/dof FD SG: {promath.chi2red(FD_data_SG.FD_loc[2:-1], cecilRestriced_EJ321P(FD_data_SG.energy[2:-1], popt_FD_data_SG[0]), FD_data_SG.FD_loc_err[2:-1], 1)}')
# print(f'Chi2/dof simulation: {promath.chi2red(LY_sim.means[4:-4], cecilRestriced_EJ321P(LY_sim.energy[4:-4], popt_sim[0]), LY_sim.means_err[4:-4], 1)}')

# plt.show()


#Fit data EJ321P Kornilov ::::::::::::::::::::::::::
#::::::::::::::::::::::::::::::::::::::::::::

p0 = [0.57, 2.47]#initial guesses
popt_HH_data_LG, pcov_HH_data_LG = curve_fit(kornilovRestricted_EJ321P, HH_data_LG.energy[2:-1], HH_data_LG.HH_loc[2:-1])
popt_TP_data_LG, pcov_TP_data_LG = curve_fit(kornilovRestricted_EJ321P, TP_data_LG.energy[2:-1], TP_data_LG.TP_loc[2:-1])
popt_FD_data_LG, pcov_FD_data_LG = curve_fit(kornilovRestricted_EJ321P, FD_data_LG.energy[2:-1], FD_data_LG.FD_loc[2:-1])
popt_HH_data_SG, pcov_HH_data_SG = curve_fit(kornilovRestricted_EJ321P, HH_data_SG.energy[2:-1], HH_data_SG.HH_loc[2:-1])
popt_TP_data_SG, pcov_TP_data_SG = curve_fit(kornilovRestricted_EJ321P, TP_data_SG.energy[2:-1], TP_data_SG.TP_loc[2:-1])
popt_FD_data_SG, pcov_FD_data_SG = curve_fit(kornilovRestricted_EJ321P, FD_data_SG.energy[2:-1], FD_data_SG.FD_loc[2:-1])
popt_sim, pocov_sim = curve_fit(kornilovRestricted_EJ321P, LY_sim.energy[4:-4],  LY_sim.means[4:-4])

print([popt_HH_data_LG[1], popt_TP_data_LG[1], popt_FD_data_LG[1], popt_HH_data_SG[1], popt_TP_data_SG[1], popt_FD_data_SG[1]])
print(f'L1 average: {np.average([popt_HH_data_LG[1], popt_TP_data_LG[1], popt_FD_data_LG[1], popt_HH_data_SG[1], popt_TP_data_SG[1], popt_FD_data_SG[1]])}')
print(f'L1 err: {np.average([np.sqrt(np.diag(pcov_HH_data_LG))[1], np.sqrt(np.diag(pcov_TP_data_LG))[1], np.sqrt(np.diag(pcov_FD_data_LG))[1], np.sqrt(np.diag(pcov_HH_data_SG))[1], np.sqrt(np.diag(pcov_TP_data_SG))[1], np.sqrt(np.diag(pcov_FD_data_SG))[1] ])}')
print(f'L1 std: {np.std([popt_HH_data_LG[1], popt_TP_data_LG[1], popt_FD_data_LG[1], popt_HH_data_SG[1], popt_TP_data_SG[1], popt_FD_data_SG[1]])}')

#Plot
Tn = np.arange(2, 6.25, 0.01)
plt.subplot(3,1,1)
plt.title('HH')
plt.scatter(HH_data_LG.energy[2:-1],  HH_data_LG.HH_loc[2:-1])
plt.scatter(HH_data_SG.energy[2:-1],  HH_data_SG.HH_loc[2:-1])
plt.plot(Tn, kornilovRestricted_EJ321P(Tn, popt_HH_data_LG[0]))
plt.plot(Tn, kornilovRestricted_EJ321P(Tn, popt_HH_data_SG[0]))
plt.scatter(LY_sim.energy[4:-4],  LY_sim.means[4:-4])
plt.plot(Tn, kornilovRestricted_EJ321P(Tn, popt_sim[0]))

plt.subplot(3,1,2)
plt.title('TP')
plt.scatter(TP_data_LG.energy[2:-1],  TP_data_LG.TP_loc[2:-1])
plt.scatter(TP_data_SG.energy[2:-1],  TP_data_SG.TP_loc[2:-1])
plt.plot(Tn, kornilovRestricted_EJ321P(Tn, popt_TP_data_LG[0]))
plt.plot(Tn, kornilovRestricted_EJ321P(Tn, popt_TP_data_SG[0]))

plt.subplot(3,1,3)
plt.title('FD')
plt.scatter(FD_data_LG.energy[2:-1],  FD_data_LG.FD_loc[2:-1])
plt.scatter(FD_data_SG.energy[2:-1],  FD_data_SG.FD_loc[2:-1])
plt.plot(Tn, kornilovRestricted_EJ321P(Tn, popt_FD_data_LG[0]))
plt.plot(Tn, kornilovRestricted_EJ321P(Tn, popt_FD_data_SG[0]))

print(f'Chi2/dof HH LG: {promath.chi2red(HH_data_LG.HH_loc[2:-1], kornilovRestricted_EJ321P(HH_data_LG.energy[2:-1], popt_HH_data_LG[0]), HH_data_LG.HH_loc_err[2:-1], 1)}')
print(f'Chi2/dof TP LG: {promath.chi2red(TP_data_LG.TP_loc[2:-1], kornilovRestricted_EJ321P(TP_data_LG.energy[2:-1], popt_TP_data_LG[0]), TP_data_LG.TP_loc_err[2:-1], 1)}')
print(f'Chi2/dof FD LG: {promath.chi2red(FD_data_LG.FD_loc[2:-1], kornilovRestricted_EJ321P(FD_data_LG.energy[2:-1], popt_FD_data_LG[0]), FD_data_LG.FD_loc_err[2:-1], 1)}')

print(f'Chi2/dof HH SG: {promath.chi2red(HH_data_SG.HH_loc[2:-1], kornilovRestricted_EJ321P(HH_data_SG.energy[2:-1], popt_HH_data_SG[0]), HH_data_SG.HH_loc_err[2:-1], 1)}')
print(f'Chi2/dof TP SG: {promath.chi2red(TP_data_SG.TP_loc[2:-1], kornilovRestricted_EJ321P(TP_data_SG.energy[2:-1], popt_TP_data_SG[0]), TP_data_SG.TP_loc_err[2:-1], 1)}')
print(f'Chi2/dof FD SG: {promath.chi2red(FD_data_SG.FD_loc[2:-1], kornilovRestricted_EJ321P(FD_data_SG.energy[2:-1], popt_FD_data_SG[0]), FD_data_SG.FD_loc_err[2:-1], 1)}')
print(f'Chi2/dof simulation: {promath.chi2red(LY_sim.means[4:-4], kornilovRestricted_EJ321P(LY_sim.energy[4:-4], popt_sim[0]), LY_sim.means_err[4:-4], 1)}')

print(f'HH LG: L0={popt_HH_data_LG[0]}')
print(f'TP LG: L0={popt_TP_data_LG[0]}')
print(f'FD LG: L0={popt_FD_data_LG[0]}')
print(f'HH SG: L0={popt_HH_data_SG[0]}')
print(f'TP SG: L0={popt_TP_data_SG[0]}')
print(f'FD SG: L0={popt_FD_data_SG[0]}')

plt.show()




####################################
####################################
####################################
####################################
####################################

#Fit data EJ331 Cecil :::::::::::::::::::::::::::::
#::::::::::::::::::::::::::::::::::::::::::::

# p0 = [0.83, 2.82, 0.25, 0.93]#initial guesses
# popt_HH_data_LG, pcov_HH_data_LG = curve_fit(cecilFull, HH_data_LG.energy[2:-1], HH_data_LG.HH_loc[2:-1], p0=p0)
# popt_TP_data_LG, pcov_TP_data_LG = curve_fit(cecilRestricted_EJ331, TP_data_LG.energy[2:-1], TP_data_LG.TP_loc[2:-1])
# popt_FD_data_LG, pcov_FD_data_LG = curve_fit(cecilRestricted_EJ331, FD_data_LG.energy[2:-1], FD_data_LG.FD_loc[2:-1])
# popt_HH_data_SG, pcov_HH_data_SG = curve_fit(cecilRestricted_EJ331, HH_data_SG.energy[2:-1], HH_data_SG.HH_loc[2:-1])
# popt_TP_data_SG, pcov_TP_data_SG = curve_fit(cecilRestricted_EJ331, TP_data_SG.energy[2:-1], TP_data_SG.TP_loc[2:-1])
# popt_FD_data_SG, pcov_FD_data_SG = curve_fit(cecilRestricted_EJ331, FD_data_SG.energy[2:-1], FD_data_SG.FD_loc[2:-1])
# popt_sim, pocov_sim = curve_fit(cecilRestricted_EJ331, LY_sim.energy[4:-4],  LY_sim.means[4:-4])

# #Plot
# Tn = np.arange(2, 6.25, 0.01)
# plt.subplot(3,1,1)
# plt.title('HH')
# plt.scatter(HH_data_LG.energy[2:-1],  HH_data_LG.HH_loc[2:-1])
# plt.scatter(HH_data_SG.energy[2:-1],  HH_data_SG.HH_loc[2:-1])
# plt.plot(Tn, cecilFull(Tn, popt_HH_data_LG[0], popt_HH_data_LG[1], popt_HH_data_LG[2], popt_HH_data_LG[3]))
# plt.plot(Tn, cecilRestricted_EJ331(Tn, popt_HH_data_SG[0]))
# plt.scatter(LY_sim.energy[4:-4],  LY_sim.means[4:-4])
# plt.plot(Tn, cecilRestricted_EJ331(Tn, popt_sim[0]))

# plt.subplot(3,1,2)
# plt.title('TP')
# plt.scatter(TP_data_LG.energy[2:-1],  TP_data_LG.TP_loc[2:-1])
# plt.scatter(TP_data_SG.energy[2:-1],  TP_data_SG.TP_loc[2:-1])
# plt.plot(Tn, cecilRestricted_EJ331(Tn, popt_TP_data_LG[0]))
# plt.plot(Tn, cecilRestricted_EJ331(Tn, popt_TP_data_SG[0]))

# plt.subplot(3,1,3)
# plt.title('FD')
# plt.scatter(FD_data_LG.energy[2:-1],  FD_data_LG.FD_loc[2:-1])
# plt.scatter(FD_data_SG.energy[2:-1],  FD_data_SG.FD_loc[2:-1])
# plt.plot(Tn, cecilRestricted_EJ331(Tn, popt_FD_data_LG[0]))
# plt.plot(Tn, cecilRestricted_EJ331(Tn, popt_FD_data_SG[0]))

# print(f'Chi2/dof HH LG: {promath.chi2red(HH_data_LG.HH_loc[2:-1], cecilFull(HH_data_LG.energy[2:-1], popt_HH_data_LG[0], popt_HH_data_LG[1], popt_HH_data_LG[2], popt_HH_data_LG[3]), HH_data_LG.HH_loc_err[2:-1], 4)}')
# print(f'Chi2/dof TP LG: {promath.chi2red(TP_data_LG.TP_loc[2:-1], cecilRestricted_EJ331(TP_data_LG.energy[2:-1], popt_TP_data_LG[0]), TP_data_LG.TP_loc_err[2:-1], 1)}')
# print(f'Chi2/dof FD LG: {promath.chi2red(FD_data_LG.FD_loc[2:-1], cecilRestricted_EJ331(FD_data_LG.energy[2:-1], popt_FD_data_LG[0]), FD_data_LG.FD_loc_err[2:-1], 1)}')
# print(f'Chi2/dof HH SG: {promath.chi2red(HH_data_SG.HH_loc[2:-1], cecilRestricted_EJ331(HH_data_SG.energy[2:-1], popt_HH_data_SG[0]), HH_data_SG.HH_loc_err[2:-1], 1)}')
# print(f'Chi2/dof TP SG: {promath.chi2red(TP_data_SG.TP_loc[2:-1], cecilRestricted_EJ331(TP_data_SG.energy[2:-1], popt_TP_data_SG[0]), TP_data_SG.TP_loc_err[2:-1], 1)}')
# print(f'Chi2/dof FD SG: {promath.chi2red(FD_data_SG.FD_loc[2:-1], cecilRestricted_EJ331(FD_data_SG.energy[2:-1], popt_FD_data_SG[0]), FD_data_SG.FD_loc_err[2:-1], 1)}')

# print(f'Chi2/dof simulation: {promath.chi2red(LY_sim.means[4:-4], kornilovRestricted_EJ331(LY_sim.energy[4:-4], popt_sim[0]), LY_sim.means_err[4:-4], 1)}')

# plt.show()


#Fit data EJ331 Kornilov ::::::::::::::::::::::::::
#::::::::::::::::::::::::::::::::::::::::::::

p0 = [0.57, 2.47]#initial guesses
popt_HH_data_LG, pcov_HH_data_LG = curve_fit(kornilovRestricted_EJ331, HH_data_LG.energy[2:-1], HH_data_LG.HH_loc[2:-1])
popt_TP_data_LG, pcov_TP_data_LG = curve_fit(kornilovRestricted_EJ331, TP_data_LG.energy[2:-1], TP_data_LG.TP_loc[2:-1])
popt_FD_data_LG, pcov_FD_data_LG = curve_fit(kornilovRestricted_EJ331, FD_data_LG.energy[2:-1], FD_data_LG.FD_loc[2:-1])
popt_HH_data_SG, pcov_HH_data_SG = curve_fit(kornilovRestricted_EJ331, HH_data_SG.energy[2:-1], HH_data_SG.HH_loc[2:-1])
popt_TP_data_SG, pcov_TP_data_SG = curve_fit(kornilovRestricted_EJ331, TP_data_SG.energy[2:-1], TP_data_SG.TP_loc[2:-1])
popt_FD_data_SG, pcov_FD_data_SG = curve_fit(kornilovRestricted_EJ331, FD_data_SG.energy[2:-1], FD_data_SG.FD_loc[2:-1])
popt_sim, pocov_sim = curve_fit(kornilovRestricted_EJ331, LY_sim.energy[4:-4],  LY_sim.means[4:-4])

# print([popt_HH_data_LG[1], popt_TP_data_LG[1], popt_FD_data_LG[1], popt_HH_data_SG[1], popt_TP_data_SG[1], popt_FD_data_SG[1]])
# print(f'L1 average: {np.average([popt_HH_data_LG[1], popt_TP_data_LG[1], popt_FD_data_LG[1], popt_HH_data_SG[1], popt_TP_data_SG[1], popt_FD_data_SG[1]])}')
# print(f'L1 err: {np.average([np.sqrt(np.diag(pcov_HH_data_LG))[1], np.sqrt(np.diag(pcov_TP_data_LG))[1], np.sqrt(np.diag(pcov_FD_data_LG))[1], np.sqrt(np.diag(pcov_HH_data_SG))[1], np.sqrt(np.diag(pcov_TP_data_SG))[1], np.sqrt(np.diag(pcov_FD_data_SG))[1] ])}')
# print(f'L1 std: {np.std([popt_HH_data_LG[1], popt_TP_data_LG[1], popt_FD_data_LG[1], popt_HH_data_SG[1], popt_TP_data_SG[1], popt_FD_data_SG[1]])}')

#Plot
Tn = np.arange(2, 6.25, 0.01)
plt.subplot(3,1,1)
plt.title('HH')
plt.scatter(HH_data_LG.energy[2:-1],  HH_data_LG.HH_loc[2:-1])
plt.scatter(HH_data_SG.energy[2:-1],  HH_data_SG.HH_loc[2:-1])
plt.plot(Tn, kornilovRestricted_EJ331(Tn, popt_HH_data_LG[0]))
plt.plot(Tn, kornilovRestricted_EJ331(Tn, popt_HH_data_SG[0]))
plt.scatter(LY_sim.energy[4:-4],  LY_sim.means[4:-4])
plt.plot(Tn, kornilovRestricted_EJ331(Tn, popt_sim[0]))

plt.subplot(3,1,2)
plt.title('TP')
plt.scatter(TP_data_LG.energy[2:-1],  TP_data_LG.TP_loc[2:-1])
plt.scatter(TP_data_SG.energy[2:-1],  TP_data_SG.TP_loc[2:-1])
plt.plot(Tn, kornilovRestricted_EJ331(Tn, popt_TP_data_LG[0]))
plt.plot(Tn, kornilovRestricted_EJ331(Tn, popt_TP_data_SG[0]))

plt.subplot(3,1,3)
plt.title('FD')
plt.scatter(FD_data_LG.energy[2:-1],  FD_data_LG.FD_loc[2:-1])
plt.scatter(FD_data_SG.energy[2:-1],  FD_data_SG.FD_loc[2:-1])
plt.plot(Tn, kornilovRestricted_EJ331(Tn, popt_FD_data_LG[0]))
plt.plot(Tn, kornilovRestricted_EJ331(Tn, popt_FD_data_SG[0]))

print(f'Chi2/dof HH LG: {promath.chi2red(HH_data_LG.HH_loc[2:-1], kornilovRestricted_EJ331(HH_data_LG.energy[2:-1], popt_HH_data_LG[0]), HH_data_LG.HH_loc_err[2:-1], 1)}')
print(f'Chi2/dof TP LG: {promath.chi2red(TP_data_LG.TP_loc[2:-1], kornilovRestricted_EJ331(TP_data_LG.energy[2:-1], popt_TP_data_LG[0]), TP_data_LG.TP_loc_err[2:-1], 1)}')
print(f'Chi2/dof FD LG: {promath.chi2red(FD_data_LG.FD_loc[2:-1], kornilovRestricted_EJ331(FD_data_LG.energy[2:-1], popt_FD_data_LG[0]), FD_data_LG.FD_loc_err[2:-1], 1)}')

print(f'Chi2/dof HH SG: {promath.chi2red(HH_data_SG.HH_loc[2:-1], kornilovRestricted_EJ331(HH_data_SG.energy[2:-1], popt_HH_data_SG[0]), HH_data_SG.HH_loc_err[2:-1], 1)}')
print(f'Chi2/dof TP SG: {promath.chi2red(TP_data_SG.TP_loc[2:-1], kornilovRestricted_EJ331(TP_data_SG.energy[2:-1], popt_TP_data_SG[0]), TP_data_SG.TP_loc_err[2:-1], 1)}')
print(f'Chi2/dof FD SG: {promath.chi2red(FD_data_SG.FD_loc[2:-1], kornilovRestricted_EJ331(FD_data_SG.energy[2:-1], popt_FD_data_SG[0]), FD_data_SG.FD_loc_err[2:-1], 1)}')

print(f'Chi2/dof simulation: {promath.chi2red(LY_sim.means[4:-4], kornilovRestricted_EJ331(LY_sim.energy[4:-4], popt_sim[0]), LY_sim.means_err[4:-4], 1)}')

print(f'HH LG: L0={popt_HH_data_LG[0]}')
print(f'TP LG: L0={popt_TP_data_LG[0]}')
print(f'FD LG: L0={popt_FD_data_LG[0]}')
print(f'HH SG: L0={popt_HH_data_SG[0]}')
print(f'TP SG: L0={popt_TP_data_SG[0]}')
print(f'FD SG: L0={popt_FD_data_SG[0]}')

plt.show()