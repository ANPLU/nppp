#!/usr/bin/env python3
from re import I
import sys
from tkinter import X
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
from numpy.core.arrayprint import SubArrayFormat
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit
from scipy import interpolate

# from matplotlib.colors import LinearSegmentedColormap


# IMPORT FROM LIBRARY DIRECTORY
sys.path.insert(0, "../library/")
sys.path.insert(0, "../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "../")
sys.path.insert(0, "data_analysis/")

import processing_math as promath
import processing_data as prodata
import processing_pd as propd
import processing_simulation as prosim
import figure_style as fs

# ##########################################################
# ######### Fundamental parameters #########################
# ##########################################################
pathSim = '/media/gheed/Seagate_Expansion_Drive1/data/simulation'
pathData = '/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper'

neutron_color = '#5b7fe2' #blue
gamma_color = '#ff7860' #red

NE213A_color = '#00b5c5' #aqua
EJ305_color = '#0fa10f' #green
EJ321P_color = '#eb4402' #red
EJ331_color = '#b72cab' #purple

# edepHigh = 1.0004
# edepLow = 0.9996

edepLow = 0.99
edepHigh = 1.01
# plt.savefig('/home/gheed/Documents/writing/mauritzson_lightYield_paper_2022/figures/test.pdf') 

# ##########################################################
# ######### Figure 1 #######################################
# ######### Experimental overview (CAD) ####################
# ##########################################################
# Made using CAD

# ##########################################################
# ######### Figure 2 #######################################
# ######### Pulse processing LG/SG #########################
# ##########################################################

path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop
# df = propd.load_parquet(path+'run01606/', num_events=1, keep_col=['samples_ch1', 'baseline_ch1', 'CFD_rise_ch1', 'amplitude_ch1','CFD_drop_ch1', 'qdc_lg_ch1']) 
# df = df.reset_index()
# df.to_pickle(path+'run01606/data.pkl')

df_waveforms = pd.read_pickle(path+'run01606/data.pkl')

evtn = 3 #event number to use

fs.set_style(textwidth=345)
plt.plot(np.arange(0, 1001, 1), (df_waveforms.samples_ch1[evtn]-df_waveforms.baseline_ch1[evtn])*1.97, lw=1, zorder=3, color='black')
plt.hlines(-25, xmin=0, xmax=1, alpha=1)
plt.hlines(0, xmin=0, xmax=1, alpha=1)
plt.fill_between(np.arange(0, 1001, 1)[0:350], y2=(df_waveforms.samples_ch1[evtn]-df_waveforms.baseline_ch1[evtn])[0:350]*1.97, color=neutron_color, alpha=0.4, y1=0)

plt.vlines(df_waveforms.CFD_rise_ch1[evtn], ymin=-500, ymax=50, alpha=1, color='black', lw=0.5)
plt.vlines(df_waveforms.CFD_rise_ch1[evtn]-25, ymin=-500, ymax=50, alpha=0.5, color='grey')
plt.vlines(df_waveforms.CFD_rise_ch1[evtn]-25+500, ymin=-500, ymax=50, alpha=0.5, color='grey')
plt.vlines(df_waveforms.CFD_rise_ch1[evtn]-25+60, ymin=-500, ymax=50, alpha=0.5, color='grey')
plt.hlines(-25, xmin=0, xmax=1000, color='black', ls='dotted')
plt.ylabel('Amplitude [mV]')
plt.xlabel('Time [ns]')
plt.xlim([160, 730])
plt.ylim([-250, 20])
# plt.savefig('/home/gheed/Documents/writing/mauritzson_lightYield_paper_2022/figures/figure02_A.pdf') 
plt.show()


### ZOOM IN VERSION ###
fs.set_style(textwidth=345*0.55)
plt.plot(np.arange(0, 1001, 1), (df_waveforms.samples_ch1[evtn]-df_waveforms.baseline_ch1[evtn])*1.97, lw=1.75, zorder=3, color='black')
plt.fill_between(np.arange(0, 1001, 1), y2=(df_waveforms.samples_ch1[evtn]-df_waveforms.baseline_ch1[evtn])*1.97, color=neutron_color, alpha=0.4, y1=0)

plt.hlines(-25, xmin=0, xmax=1, alpha=1, zorder=2)
plt.hlines(0, xmin=0, xmax=1, alpha=1, zorder=2)
plt.hlines(-25, xmin=0, xmax=1000, color='black', zorder=2)
plt.hlines(0, xmin=0, xmax=1000, color='black', ls='dashed', zorder=2)
plt.vlines(df_waveforms.CFD_rise_ch1[evtn], ymin=-500, ymax=60, alpha=1, color='black', zorder=2)

plt.xlim([199, 265])
plt.ylim([-265, 60])
# plt.savefig('/home/gheed/Documents/writing/mauritzson_lightYield_paper_2022/figures/figure02_B.pdf') 
plt.show()


# ##########################################################
# ######### Figure 3 #######################################
# ######### Energy calibration [MeVee] #####################
# ##########################################################


CableLengthCorrections = 1.192
#Load data
path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop
Cs137_data =    propd.load_parquet_merge(path, [3259], keep_col=['qdc_lg_ch1'], full=False)*CableLengthCorrections
Th232_data =    propd.load_parquet_merge(path, [3258], keep_col=['qdc_lg_ch1'], full=False)*CableLengthCorrections
AmBe_data =     propd.load_parquet_merge(path, [3257], keep_col=['qdc_lg_ch1'], full=False)*CableLengthCorrections
BG_data =       propd.load_parquet_merge(path, np.arange(3260, 3260+10,1), keep_col=['qdc_lg_ch1'], full=False)*CableLengthCorrections


#Load simulation
path_sim = '/media/gheed/Seagate_Expansion_Drive1/data/simulation/'
PMT_gain = 4e6 #Nominal gain is 7'000'000 at nominal A/lm (model: 9821B p1)
att_factor_12dB = 3.98
q = 1.60217662E-19 #charge of a single electron
detector = 'NE213A'

names = ['evtNum', 
        'optPhotonSum', 
        'optPhotonSumQE', 
        'optPhotonSumCompton', 
        'optPhotonSumComptonQE', 
        'optPhotonSumNPQE', 
        'xLoc', 
        'yLoc', 
        'zLoc', 
        'CsMin', 
        'optPhotonParentID', 
        'optPhotonParentCreatorProcess', 
        'optPhotonParentStartingEnergy',
        'total_edep',
        'gamma_edep',
        'proton_edep',
        'nScatters',
        'nScattersProton']

Cs137_sim_iso =         pd.read_csv(path_sim + f"{detector}/Cs137/isotropic/CSV_optphoton_data_sum.csv", names=names)
Th232_sim_iso =         pd.read_csv(path_sim + f"{detector}/Th232/isotropic/CSV_optphoton_data_sum.csv", names=names)
AmBe_sim_iso =          pd.read_csv(path_sim + f"{detector}/4.44/isotropic/CSV_optphoton_data_sum.csv", names=names)

#append extra data
Cs137_sim_iso =         pd.concat([Cs137_sim_iso, pd.read_csv(path_sim + f'{detector}/Cs137/isotropic/part2/CSV_optphoton_data_sum.csv', names=names)])
Cs137_sim_iso =         pd.concat([Cs137_sim_iso, pd.read_csv(path_sim + f'{detector}/Cs137/isotropic/part3/CSV_optphoton_data_sum.csv', names=names)])
Cs137_sim_iso =         pd.concat([Cs137_sim_iso, pd.read_csv(path_sim + f'{detector}/Cs137/isotropic/part4/CSV_optphoton_data_sum.csv', names=names)])
Cs137_sim_iso =         pd.concat([Cs137_sim_iso, pd.read_csv(path_sim + f'{detector}/Cs137/isotropic/part5/CSV_optphoton_data_sum.csv', names=names)])

Th232_sim_iso =         pd.concat([Th232_sim_iso, pd.read_csv(path_sim + f'{detector}/Th232/isotropic/part2/CSV_optphoton_data_sum.csv', names=names)])

AmBe_sim_iso =          pd.concat([AmBe_sim_iso, pd.read_csv(path_sim + f'{detector}/4.44/isotropic/part2/CSV_optphoton_data_sum.csv', names=names)])

###############################
# RANDOMIZE BINNING
###############################
y, x = np.histogram(Cs137_sim_iso.optPhotonSumQE, bins=4096, range=[0, 4096])
Cs137_sim_iso.optPhotonSumQE = prodata.getRandDist(x, y)
y, x = np.histogram(Th232_sim_iso.optPhotonSumQE, bins=4096, range=[0, 4096])
Th232_sim_iso.optPhotonSumQE = prodata.getRandDist(x, y)
y, x = np.histogram(AmBe_sim_iso.optPhotonSumQE, bins=4096, range=[0, 4096])
AmBe_sim_iso.optPhotonSumQE = prodata.getRandDist(x, y)

###############################
# RESET INDEX
###############################
Cs137_sim_iso =  Cs137_sim_iso.reset_index()
Th232_sim_iso =  Th232_sim_iso.reset_index()
AmBe_sim_iso =   AmBe_sim_iso.reset_index()

###############################
# CALIBRATING SIMULATION DATA
###############################
Cs137_sim_iso.optPhotonSumQE =         prosim.chargeCalibration(Cs137_sim_iso.optPhotonSumQE  * q * PMT_gain)
Th232_sim_iso.optPhotonSumQE =         prosim.chargeCalibration(Th232_sim_iso.optPhotonSumQE  * q * PMT_gain)
AmBe_sim_iso.optPhotonSumQE =          prosim.chargeCalibration(AmBe_sim_iso.optPhotonSumQE   * q * PMT_gain)

#Apply smearing on pencilbeam data
Cs137_sim_iso.optPhotonSumQE = Cs137_sim_iso.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, 0.16)) 
Th232_sim_iso.optPhotonSumQE = Th232_sim_iso.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, 0.07)) 
AmBe_sim_iso.optPhotonSumQE = AmBe_sim_iso.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, 0.11)) 

############################################
# LOAD PENCILBEAM SIMULATIONS
############################################
Cs137_sim_pen = pd.read_csv(path_sim + f"{detector}/Cs137/pencilbeam/CSV_optphoton_data_sum.csv", names=names)

Th232_sim_pen = pd.read_csv(path_sim + f"{detector}/Th232/pencilbeam/CSV_optphoton_data_sum.csv", names=names)
Th232_sim_pen = pd.concat([Th232_sim_pen, pd.read_csv(path_sim + f'{detector}/Th232/pencilbeam/part2/CSV_optphoton_data_sum.csv', names=names)])
Th232_sim_pen = pd.concat([Th232_sim_pen, pd.read_csv(path_sim + f'{detector}/Th232/pencilbeam/part3/CSV_optphoton_data_sum.csv', names=names)])
Th232_sim_pen = pd.concat([Th232_sim_pen, pd.read_csv(path_sim + f'{detector}/Th232/pencilbeam/part4/CSV_optphoton_data_sum.csv', names=names)])
Th232_sim_pen = pd.concat([Th232_sim_pen, pd.read_csv(path_sim + f'{detector}/Th232/pencilbeam/part5/CSV_optphoton_data_sum.csv', names=names)])

AmBe_sim_pen = pd.read_csv(path_sim + f"{detector}/4.44/pencilbeam/CSV_optphoton_data_sum.csv", names=names)
AmBe_sim_pen = pd.concat([AmBe_sim_pen, pd.read_csv(path_sim + f'{detector}/4.44/pencilbeam/part2/CSV_optphoton_data_sum.csv', names=names)])
AmBe_sim_pen = pd.concat([AmBe_sim_pen, pd.read_csv(path_sim + f'{detector}/4.44/pencilbeam/part3/CSV_optphoton_data_sum.csv', names=names)])
AmBe_sim_pen = pd.concat([AmBe_sim_pen, pd.read_csv(path_sim + f'{detector}/4.44/pencilbeam/part4/CSV_optphoton_data_sum.csv', names=names)])

#randomize binning pencilbeam
y, x = np.histogram(Cs137_sim_pen.optPhotonSumComptonQE, bins=4096, range=[0, 4096])
Cs137_sim_pen.optPhotonSumComptonQE = prodata.getRandDist(x, y)
y, x = np.histogram(Th232_sim_pen.optPhotonSumComptonQE, bins=4096, range=[0, 4096])
Th232_sim_pen.optPhotonSumComptonQE = prodata.getRandDist(x, y)
y, x = np.histogram(AmBe_sim_pen.optPhotonSumComptonQE, bins=4096, range=[0, 4096])
AmBe_sim_pen.optPhotonSumComptonQE = prodata.getRandDist(x, y)

#reset index pencilbeam 
Cs137_sim_pen =  Cs137_sim_pen.reset_index()
Th232_sim_pen =  Th232_sim_pen.reset_index()
AmBe_sim_pen =   AmBe_sim_pen.reset_index()

#QDC calibrate simulations pencilbeam
Cs137_sim_pen.optPhotonSumComptonQE =  prosim.chargeCalibration(Cs137_sim_pen.optPhotonSumComptonQE * q * PMT_gain) 
Th232_sim_pen.optPhotonSumComptonQE =  prosim.chargeCalibration(Th232_sim_pen.optPhotonSumComptonQE * q * PMT_gain) 
AmBe_sim_pen.optPhotonSumComptonQE =   prosim.chargeCalibration(AmBe_sim_pen.optPhotonSumComptonQE * q * PMT_gain)

#Apply smearing on pencilbeam data
Cs137_sim_pen.optPhotonSumComptonQE = Cs137_sim_pen.optPhotonSumComptonQE.apply(lambda x: prosim.gaussSmear(x, 0.16)) 
Th232_sim_pen.optPhotonSumComptonQE = Th232_sim_pen.optPhotonSumComptonQE.apply(lambda x: prosim.gaussSmear(x, 0.07)) 
AmBe_sim_pen.optPhotonSumComptonQE = AmBe_sim_pen.optPhotonSumComptonQE.apply(lambda x: prosim.gaussSmear(x, 0.11)) 

################
#MAKING FIGURES
################

xMin = 0
xMax = 62000

QDCcal = np.empty(0)
titleParamters = dict(ha='left', va='center', fontsize=10, color='black')

fs.set_style(textwidth=345)


#Cs137
plt.subplot(3,1,1)

binRange = np.arange(2000, 7400, 550)
w = np.empty(len(BG_data))
w.fill(1/10)
ybg, xbg = np.histogram(BG_data.qdc_lg_ch1*att_factor_12dB, bins=binRange, weights=w)

w = np.empty(len(Cs137_data))
w.fill(1/1)
y, x = np.histogram(Cs137_data.qdc_lg_ch1*att_factor_12dB, bins=binRange, weights=w)
x = prodata.getBinCenters(x)

plt.scatter(x, (y-ybg)*0.028, label='Cs137', color='black', s=3, zorder=3)

offset = 0.386
y, x = np. histogram(Cs137_sim_pen.optPhotonSumComptonQE*offset, bins=binRange)
x = prodata.getBinCenters(x)
plt.step(x, y*1.40, color='black', lw=0.5, zorder=3)
plt.fill_between(x, y1=y*1.40, color=gamma_color, alpha=1, step='pre', zorder=2)
QDCcal = np.append(QDCcal, 4726.28)

plt.vlines(QDCcal[-1], ymin=0, ymax=3000, color='black', ls='dashed', zorder=3)

y, x = np. histogram(Cs137_sim_iso.optPhotonSumQE*offset, bins=binRange)
x = prodata.getBinCenters(x)
plt.step(x, y*1.151*0.028, color='black', lw=0.5)
plt.fill_between(x, y1=y*1.151*0.028, color='gray', alpha=0.2, step='pre', zorder=1)

plt.text(1000, 2300, '$^{137}$Cs (0.48 MeV$_{ee}$)', **titleParamters)
plt.xlim(xMin,xMax)
plt.xticks([])
plt.ylim([0, 2800])

#Th232
plt.subplot(3,1,2)
binRange = np.arange(17800, 30000, 900)
w = np.empty(len(BG_data))
w.fill(1/10)
ybg, xbg = np.histogram(BG_data.qdc_lg_ch1*att_factor_12dB, bins=binRange, weights=w)
w = np.empty(len(Th232_data))
w.fill(1/1)
y, x = np.histogram(Th232_data.qdc_lg_ch1*att_factor_12dB, bins=binRange, weights=w)
x = prodata.getBinCenters(x)
plt.scatter(x, (y-ybg)*0.9, label='Th232', color='black', s=3, zorder=3)

offset = 0.380
y, x = np. histogram(Th232_sim_pen.optPhotonSumComptonQE*offset, bins=np.arange(15000, 32000, 1000))
x = prodata.getBinCenters(x)
plt.step(x, y*1.8, color='black', lw=0.5, zorder=3)
plt.fill_between(x, y1=y*1.8, color=gamma_color, alpha=1, step='pre', zorder=2)
QDCcal = np.append(QDCcal, 24222.33)
plt.vlines(QDCcal[-1], ymin=0, ymax=3000, color='black', ls='dashed', zorder=3)

y, x = np. histogram(Th232_sim_iso.optPhotonSumQE*offset, bins=binRange)
x = prodata.getBinCenters(x)
plt.step(x, y*0.397*0.95, color='black', lw=0.5)
plt.fill_between(x, y1=y*0.397*0.95, color='gray', alpha=0.2, step='pre', zorder=1)

plt.text(1000, 2300, '$^{232}$Th (2.38 MeV$_{ee}$)', **titleParamters)
plt.xlim(xMin,xMax)
plt.ylim([0, 2800])
plt.xticks([])

#AmBe
plt.subplot(3,1,3)
binRange = np.arange(30000, 55000, 1500)
w = np.empty(len(BG_data))
w.fill(1/10)
ybg, xbg = np.histogram(BG_data.qdc_lg_ch1*att_factor_12dB, bins=binRange, weights=w)
w = np.empty(len(AmBe_data))
w.fill(1/1)
y, x = np.histogram(AmBe_data.qdc_lg_ch1*att_factor_12dB, bins=binRange, weights=w)
x = prodata.getBinCenters(x)
plt.scatter(x, (y-ybg)*0.65, label='AmBe', color='black', s=3, zorder=3)

offset = 0.380
y, x = np. histogram(AmBe_sim_pen.optPhotonSumComptonQE*offset, bins=np.arange(16800, 65000, 1500))
x = prodata.getBinCenters(x)
plt.step(x, y*12, color='black', lw=0.5, zorder=3)
plt.fill_between(x, y1=y*12, color=gamma_color, alpha=1, step='pre', zorder=2)
QDCcal = np.append(QDCcal, 40427.27)
plt.vlines(QDCcal[-1], ymin=0, ymax=3000, color='black', ls='dashed', zorder=3)

y, x = np. histogram(AmBe_sim_iso.optPhotonSumQE*offset, bins=binRange)
x = prodata.getBinCenters(x)
plt.step(x, y*1.665*0.7, color='black', lw=0.5)
plt.fill_between(x, y1=y*1.665*0.7, color='gray', alpha=0.2, step='pre', zorder=1)

# plt.vlines(x=4600, ymin=0, ymax=2500, color='black', lw=1) #dividing lines
plt.text(1000, 2300, 'Am/Be (4.20 MeV$_{ee}$)', **titleParamters)

plt.xlabel('QDC ($10^4$) [ch]')
plt.ylabel('Counts')
plt.xlim(xMin,xMax)
plt.ylim([0, 2800])
plt.subplots_adjust(hspace = .001, wspace = .001)

plt.savefig('/home/gheed/Documents/writing/mauritzson_lightYield_paper_2022/figures/calibration_A.pdf')

plt.show()

#Inset figure for calibration (QDC)
fs.set_style(textwidth=345/2.2)
plt.figure()
Ecal = [0.48, 2.38, 4.20] ##energies of compton edges
popt, pcov = curve_fit(promath.linearFunc, QDCcal, Ecal)
pcov = np.diag(np.sqrt(pcov))
plt.plot(np.arange(0, 7, 1), promath.linearFunc(np.arange(0, 7, 1), 1/popt[0], 1/popt[1]), color='grey', lw=0.5, zorder=2)
plt.scatter(Ecal, QDCcal, color='black', s=15, marker='s', zorder=2)
plt.title(f'{round(popt[0]*1000,3)}$\pm${round(pcov[0]*1000,3)}')
plt.xlim([0,6])
plt.ylim([0,60000])

plt.savefig('/home/gheed/Documents/writing/mauritzson_lightYield_paper_2022/figures/calibration_B.pdf')

plt.show()

#MAKE xticks for second x-axis (number of photons)
fs.set_style(textwidth=345)
plt.xlim(xMin*0.028076,xMax*0.028076)
plt.xlabel('Number of Scintillation Photons')

plt.savefig('/home/gheed/Documents/writing/mauritzson_lightYield_paper_2022/figures/calibration_C.pdf')

plt.show()

# ##########################################################
# ######### Figure 4 #######################################
# ######### Detector Edep vs YAP Edep threholds ############
# ##########################################################
path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop
att_factor_12dB = 3.98
detector = 'NE213A'
tofCh = 4
#load data
# data = propd.load_parquet_merge(path, np.arange(2121,2121+50,1), keep_col=['qdc_lg_ch1', 'amplitude_ch2','qdc_lg_ch2','tof_ch2','amplitude_ch3','qdc_lg_ch3','tof_ch3','amplitude_ch4','qdc_lg_ch4','tof_ch4','amplitude_ch5','qdc_lg_ch5','tof_ch5'], full=False)
# data.to_pickle(path+'run02121_data/data.pkl')
data = pd.read_pickle(path+'run02121_data/data.pkl')

#Energy calibrate data NE213A
data.qdc_lg_ch1 = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/{detector}/Ecal/popt.npy', data.qdc_lg_ch1*att_factor_12dB, inverse=True)

#Energy calibrate data YAP
data[f'qdc_lg_ch{tofCh}'] = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/YAP/Ecal/ch{tofCh}/popt.npy', data[f'qdc_lg_ch{tofCh}'], inverse=False)

# YAP_cal = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/YAP/Ecal/ch{tofCh}/popt.npy')
# data[f'qdc_lg_ch{tofCh}'] = promath.linearFunc(data[f'qdc_lg_ch{tofCh}'], YAP_cal[0], YAP_cal[1])

#Make figure
numBins = 150
titleParamters = dict(ha='left', va='center', fontsize=10, color='black')
fs.set_style(textwidth=345)
query = f'amplitude_ch{tofCh}>20 and qdc_lg_ch{tofCh}>0.1 and tof_ch{tofCh}>82 and tof_ch{tofCh}<150 and qdc_lg_ch1>0.1'
plt.hist2d(data.query(query).qdc_lg_ch1, data.query(query)[f'qdc_lg_ch{tofCh}'], bins=numBins, range=[[0, 6], [0, 6]], cmap='turbo', norm=LogNorm())

plt.vlines(0.1, ymin=0, ymax=7, ls='dashed', lw=1, color='black')
plt.text(0.5, 5, 'NE 213A detector threshold', **titleParamters)

plt.hlines(3.0, xmin=0, xmax=7, ls='dashed', lw=1, color='black')
plt.text(2, 2, 'YAP:Ce detector threshold', **titleParamters)

plt.xlabel('NE 213A Light Yield [MeV$_{ee}$]')
plt.ylabel('YAP:Ce Light Yield [MeV$_{ee}$]')
plt.colorbar()

plt.savefig('/home/gheed/Documents/writing/mauritzson_lightYield_paper_2022/figures/figure04_energy_correlation.pdf')

plt.show()

# ##########################################################
# ######### Figure 5 #######################################
# ######### TOF energy slice ###############################
# ##########################################################

path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop
distance = 0.959

#Load data
TOF = propd.load_parquet_merge(path, np.arange(2121,2121+10,1), keep_col=['amplitude_ch2','tof_ch2'], full=False)

#calibrate TOF
flashrange = {'tof_ch2':[60, 73]} #gamma flash fitting ranges
prodata.tof_calibration(TOF, flashrange, distance, phcut=125, calUnit=1e-9, energyCal=True)

#define Neutron energy bin cuts in time (s)
t1_start = prodata.EnergytoTOF(5+0.125 ,distance)
t1_stop = prodata.EnergytoTOF(5-0.125 ,distance)

t2_start = prodata.EnergytoTOF(3+0.125 ,distance)
t2_stop = prodata.EnergytoTOF(3-0.125 ,distance)

#Make figures
numBins = 800
binRange = [-870, 200]

fs.set_style(textwidth=345)

y, x = np.histogram(TOF.query('amplitude_ch2>35').tof_ch2*1e9, bins=numBins, range=binRange)
x = prodata.getBinCenters(x)
plt.step(x, y, color='black', lw=1)

y, x = np.histogram(TOF.query('amplitude_ch2>35 and tof_ch2>-710e-9 and tof_ch2<-10e-9').tof_ch2*1e9, bins=numBins, range=binRange)
x = prodata.getBinCenters(x)
plt.fill_between(x, y, where=(y > 0), color='grey', step='pre')

y, x = np.histogram(TOF.query('amplitude_ch2>35 and tof_ch2>-1e-9 and tof_ch2<5.8e-9').tof_ch2*1e9, bins=numBins, range=binRange)
x = prodata.getBinCenters(x)
plt.fill_between(x, y, where=(y > 0), color=gamma_color, step='pre')
# popt,pcov = curve_fit(promath.gaussFunc, x, y)
# print(popt)
y, x = np.histogram(TOF.query('amplitude_ch2>35 and tof_ch2>26.2e-9 and tof_ch2<90e-9').tof_ch2*1e9, bins=numBins, range=binRange)
x = prodata.getBinCenters(x)
plt.fill_between(x, y, where=(y > 0), color=neutron_color, step='pre')

plt.xlabel('Time-of-Flight [ns]')
plt.ylabel('Counts')
plt.xlim([-900,200])
plt.ylim([-200,2300])
plt.savefig('/home/gheed/Documents/writing/mauritzson_lightYield_paper_2022/figures/figure05_A.pdf')


#Make inset figure
fs.set_style(textwidth=345/1.4)
plt.figure()
y, x = np.histogram(TOF.query('amplitude_ch2>35').tof_ch2*1e9, bins=numBins, range=binRange)
x = prodata.getBinCenters(x)
plt.step(x, y, color='black', lw=1.0)

# y, x = np.histogram(TOF.query(f'amplitude_ch2>35 and tof_ch2>33.6e-9 and tof_ch2<36.81e-9').tof_ch2*1e9, bins=numBins, range=binRange)
y, x = np.histogram(TOF.query(f'amplitude_ch2>35 and tof_ch2>{t1_start} and tof_ch2<{t1_stop}').tof_ch2*1e9, bins=numBins, range=binRange)
x = prodata.getBinCenters(x)
plt.fill_between(x, y, where=(y > 0), color=neutron_color, step='pre')

# y, x = np.histogram(TOF.query(f'amplitude_ch2>35 and tof_ch2>46.0e-9 and tof_ch2<52.8e-9').tof_ch2*1e9, bins=numBins, range=binRange)
y, x = np.histogram(TOF.query(f'amplitude_ch2>35 and tof_ch2>{t2_start} and tof_ch2<{t2_stop}').tof_ch2*1e9, bins=numBins, range=binRange)
x = prodata.getBinCenters(x)
plt.fill_between(x, y, where=(y > 0), color=neutron_color, step='pre')

y, x = np.histogram(TOF.query('amplitude_ch2>35 and tof_ch2>-710e-9 and tof_ch2<-10e-9').tof_ch2*1e9, bins=numBins, range=binRange)
x = prodata.getBinCenters(x)
plt.fill_between(x, y, where=(y > 0), color='grey', step='pre')

y, x = np.histogram(TOF.query('amplitude_ch2>35 and tof_ch2>-1e-9 and tof_ch2<5.8e-9').tof_ch2*1e9, bins=numBins, range=binRange)
x = prodata.getBinCenters(x)
plt.fill_between(x, y, where=(y > 0), color=gamma_color, step='pre')


plt.vlines(t1_start*1e9, ymax = 2000, lw=0.5, ymin= 50, color='purple')
plt.vlines(t1_stop*1e9, ymax = 2000, lw=0.5, ymin= 50, color='purple')

plt.vlines(t2_start*1e9, ymax = 2000, lw=0.5, ymin= 50, color='purple')
plt.vlines(t2_stop*1e9, ymax = 2000, lw=0.5, ymin= 50, color='purple')

plt.xlabel('Time-of-Flight [ns]')
plt.ylabel('counts')
plt.xlim([-15,65])
plt.ylim([-250, 2000])

plt.savefig('/home/gheed/Documents/writing/mauritzson_lightYield_paper_2022/figures/figure05_B.pdf')

plt.show()


# ##########################################################
# ######### Figure 6 #######################################
# ######### Before and after kB optimization ###############
# ##########################################################
detector = 'EJ305'
gate = 'LG'

#define energies to plot
Tn = np.arange(2000, 7000, 1000)

#load fitted parameters for reciprocal smear function
poptReciprocal_smear = np.load(f'{pathData}/{detector}/birks/smear_reciprocal_popt.npy')
pcovReciprocal_smear = np.load(f'{pathData}/{detector}/birks/smear_reciprocal_pcov.npy')

#create smear values to use for each energy based on function
smearFactorList = promath.reciprocalConstantFunc(Tn/1000, poptReciprocal_smear[0], poptReciprocal_smear[1])

smearFactorListBefore = np.array([0.21, 0.15, 0.08, 0.065 , 0.032])
scalingBefore = np.array([2.173, 1.774, 1.30, 1.020, 1.490])
scalingAfter = np.array([1, 1, 0.9, 0.75, 1])

#number of bins to use for each energy
numBinsList = np.array([np.arange(0, 1.12, 0.050+0.020),
                        np.arange(0, 1.30, 0.055+0.025),
                        np.arange(0, 1.80, 0.060+0.025),
                        np.arange(0, 2.40, 0.065+0.025),
                        np.arange(0, 3.00, 0.070+0.025)], dtype=object)

#ranges over which to optmizise scaling between simulation and data
# start = [0.3, 0.7, 1.15, 1.58, 2.12] #For NE 213A
# stop =  [1.0,  1.3,  1.7,  2.8, 3.1] #For NE 213A

start = [0.22,  0.47, 0.82, 1.18, 1.08] #For EJ 305
stop =  [0.70,  1.15, 1.75, 2.20,  2.80] #For EJ 305

names = [ 'evtNum', 
        'optPhotonSum', 
        'optPhotonSumQE', 
        'optPhotonSumCompton', 
        'optPhotonSumComptonQE', 
        'optPhotonSumNPQE', 
        'xLoc', 
        'yLoc', 
        'zLoc', 
        'CsMin', 
        'optPhotonParentID', 
        'optPhotonParentCreatorProcess', 
        'optPhotonParentStartingEnergy',
        'total_edep',
        'gamma_edep',
        'proton_edep',
        'nScatters',
        'nScattersProton']

########
#BEFORE#
########
fs.set_style(textwidth=345)
titleParamters = dict(ha='left', va='center', fontsize=10, color='black')
ratioTotalBefore = 0
ratioTotalAfter = 0
j = 0
gainCorrection = 0.85

for i, energy in enumerate(Tn):
        #import simulations
        simCurrent = pd.read_csv(f"{pathSim}/{detector}/neutrons/neutron_range/5MeV_optimized/{int(energy)}keV/isotropic/CSV_optphoton_data_sum.csv", names=names)

        #randomize binning
        y, x = np.histogram(simCurrent.optPhotonSumQE, bins=4096, range=[0, 4096]) 
        simCurrent.optPhotonSumQE = prodata.getRandDist(x, y)

        #calibrate simulation
        simCurrent.optPhotonSumQE = prodata.calibrateMyDetector(f'{pathData}/{detector}/Simcal/popt.npy', simCurrent.optPhotonSumQE, inverse=False)

        #smear simulation
        simCurrent.optPhotonSumQE = simCurrent.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, smearFactorListBefore[i])) 

        #binning simulations and applying gain offset
        ySimCurrent, xSimCurrent = np.histogram(simCurrent.optPhotonSumQE*gainCorrection, bins=numBinsList[i])
        xSimCurrent = prodata.getBinCenters(xSimCurrent)

        #import data
        nQDC       = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/nQDC_{gate}.npy')
        randQDC    = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/randQDC_{gate}.npy')
        nLength    = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/nLength.npy')
        randLength = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/randLength.npy')
        nQDC = prodata.calibrateMyDetector(f'{pathData}/{detector}/Ecal/popt_LG.npy', nQDC)
        randQDC = prodata.calibrateMyDetector(f'{pathData}/{detector}/Ecal/popt_LG.npy', randQDC)

        #random subtract data
        xDataCurrent, yDataCurrent = prodata.randomTOFSubtractionQDC(   nQDC,
                                                                        randQDC,
                                                                        nLength, 
                                                                        randLength,
                                                                        numBinsList[i], 
                                                                        plot=False)
        #normalize data and simulation to 1
        yDataCurrent = prodata.integralNorm(yDataCurrent, forcePositive=True)
        ySimCurrent = prodata.integralNorm(ySimCurrent, forcePositive=True)

        #fitting data to get range for Chi2
        poptCurrent = promath.gaussFit(xDataCurrent, yDataCurrent, start[i], stop[i], y_error=False, error=False)

        scaleCurrent = scalingBefore[i]

        #calculate the average ratio by first slicing the data sets and then taking the ratio
        xDataCurrentRatio, yDataCurrentRatio = prodata.binDataSlicer(xDataCurrent, yDataCurrent, start[i], stop[i])
        xSimCurrentRatio, ySimCurrentRatio = prodata.binDataSlicer(xSimCurrent, ySimCurrent * scaleCurrent, start[i], stop[i])
        ratioCurrent = np.average(np.abs((yDataCurrentRatio/ySimCurrentRatio)-1))
        ratioStd = np.std((yDataCurrentRatio/ySimCurrentRatio)-1)

        ratioTotalBefore += ratioCurrent/5


        #make plot
        plt.subplot(5, 2, j+1)

        if j==4:
                plt.ylabel('Counts')

        plt.text(2.20, np.max(yDataCurrent)*0.88, f'{np.int32(energy/1000)} MeV', **titleParamters)

        plt.scatter(xDataCurrent, yDataCurrent*scalingAfter[i], s=5, color='black', zorder=3)
        plt.errorbar(xDataCurrent, yDataCurrent*scalingAfter[i], yerr=yDataCurrent*0.065*scalingAfter[i], linestyle='none',color='black')
        plt.step(xSimCurrent, ySimCurrent*scaleCurrent, color='black', ls='solid', lw=0.75)
        plt.fill_between(xSimCurrent, y1=0, y2=ySimCurrent*scaleCurrent, color='black', edgecolor='none', alpha=0.2, step='pre', zorder=1)
        plt.ylim([np.min(yDataCurrent[-15:-1])*0.8, np.max(yDataCurrent)*1.25])
        plt.xlim(0.13, 3.20)#EJ305
        plt.yticks([])

        ########
        #AFTER##
        ########
        simCurrent = pd.read_csv(f"{pathSim}/{detector}/neutrons/neutron_range/{int(energy)}keV/isotropic/1M/CSV_optphoton_data_sum.csv", names=names)

        xSimCurrent, ySimCurrent = prosim.simProcessing(simCurrent, pathData, detector, smearFactorList[i], numBinsList[i])

        #import data
        nQDC       = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/nQDC_{gate}.npy')
        randQDC    = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/randQDC_{gate}.npy')
        nLength    = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/nLength.npy')
        randLength = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/randLength.npy')
        nQDC = prodata.calibrateMyDetector(f'{pathData}/{detector}/Ecal/popt_LG.npy', nQDC)
        randQDC = prodata.calibrateMyDetector(f'{pathData}/{detector}/Ecal/popt_LG.npy', randQDC)

        #random subtract data
        xDataCurrent, yDataCurrent = prodata.randomTOFSubtractionQDC(   nQDC,
                                                                        randQDC,
                                                                        nLength, 
                                                                        randLength,
                                                                        numBinsList[i], 
                                                                        plot=False)
        #normalize data and simulation to 1
        yDataCurrent = prodata.integralNorm(yDataCurrent, forcePositive=True)
        ySimCurrent = prodata.integralNorm(ySimCurrent, forcePositive=True)

        #fitting data to get range for Chi2
        poptCurrent = promath.gaussFit(xDataCurrent, yDataCurrent, start[i], stop[i], y_error=False, error=False)

        ###############################################################################################
        # THIS PART WILL SLICE DATA SETS TO BE USED FOR CALCULATING SCALING PRIOR TO Chi2 CALCULATION #
        ###############################################################################################
        #slicing data and simulation for determining optimal scaling
        xDataCurrentChi2, yDataCurrentScale = prodata.binDataSlicer(xDataCurrent, yDataCurrent, poptCurrent[1]*1, poptCurrent[1]*1.5)
        xSimCurrentChi2, ySimCurrentScale = prodata.binDataSlicer(xSimCurrent, ySimCurrent, poptCurrent[1]*1, poptCurrent[1]*1.5)
        ###############################################################################################

        #Determine optimal scaling for minimal Chi2 value
        scaleCurrent = prodata.chi2Minimize(yDataCurrentScale, ySimCurrentScale, verbose=True)
        
        #calculate the average ratio by first slicing the data sets and then taking the ratio
        xDataCurrentRatio, yDataCurrentRatio = prodata.binDataSlicer(xDataCurrent, yDataCurrent, start[i], stop[i])
        xSimCurrentRatio, ySimCurrentRatio = prodata.binDataSlicer(xSimCurrent, ySimCurrent * scaleCurrent, start[i], stop[i])
        ratioCurrent = np.average(np.abs((yDataCurrentRatio/ySimCurrentRatio)-1))
        ratioStd = np.std((yDataCurrentRatio/ySimCurrentRatio)-1)
        ratioTotalAfter += ratioCurrent/5


        #make plot
        plt.subplot(5, 2, j+2)
        if j==8:
                plt.xlabel('Scintillation Light Yield [MeV$_{ee}$]')       


        plt.scatter(xDataCurrent, yDataCurrent*scalingAfter[i], s=5, color='black', zorder=3)
        plt.errorbar(xDataCurrent, yDataCurrent*scalingAfter[i], yerr=yDataCurrent*0.065*scalingAfter[i], linestyle='none',color='black')
        plt.step(xSimCurrent, ySimCurrent * scaleCurrent * scalingAfter[i], color='black', ls='solid', lw=0.75)
        # plt.step(xSimCurrentRatio, ySimCurrentRatio, color='black', ls='solid', lw=2)
        plt.fill_between(xSimCurrent, y1=0, y2= ySimCurrent * scaleCurrent * scalingAfter[i], color='black', edgecolor='none', alpha=0.20, step='pre', zorder=1)
        plt.ylim([np.min(yDataCurrent[-15:-1])*0.8, np.max(yDataCurrent)*1.25])
        plt.xlim(0.13, 3.30)#EJ305
        plt.yticks([])

        j+=2

print(f'Ratio total before = {np.round(ratioTotalBefore*100, 2)}%')
print(f'Ratio total after = {np.round(ratioTotalAfter*100, 2)}%')

plt.subplots_adjust(hspace = .001, wspace = .001)

plt.savefig(f'/home/gheed/Documents/writing/mauritzson_lightYield_paper_2022/figures/kB_optimization_result.pdf')

plt.show()



#############################################
#kB optimization insert plot
density = 0.893 #g/cm3 EJ305

E_2000 = pd.read_csv(f'{pathData}/{detector}/birks/{2000}keV.csv')
E_2500 = pd.read_csv(f'{pathData}/{detector}/birks/{2500}keV.csv')
E_3000 = pd.read_csv(f'{pathData}/{detector}/birks/{3000}keV.csv')
E_3500 = pd.read_csv(f'{pathData}/{detector}/birks/{3500}keV.csv')
E_4000 = pd.read_csv(f'{pathData}/{detector}/birks/{4000}keV.csv')
E_4500 = pd.read_csv(f'{pathData}/{detector}/birks/{4500}keV.csv')
E_5000 = pd.read_csv(f'{pathData}/{detector}/birks/{5000}keV.csv')
E_5500 = pd.read_csv(f'{pathData}/{detector}/birks/{5500}keV.csv')
E_6000 = pd.read_csv(f'{pathData}/{detector}/birks/{6000}keV.csv')
poptReciprocal_kB = np.load(f'{pathData}/{detector}/birks/kB_reciprocal_popt.npy')
pcovReciprocal_kB = np.load(f'{pathData}/{detector}/birks/kB_reciprocal_pcov.npy')
optimal_kB = np.array([E_2000.optimal_kB[0], 
                        E_2500.optimal_kB[0], 
                        E_3000.optimal_kB[0], 
                        E_3500.optimal_kB[0], 
                        E_4000.optimal_kB[0], 
                        E_4500.optimal_kB[0], 
                        E_5000.optimal_kB[0], 
                        E_5500.optimal_kB[0], 
                        E_6000.optimal_kB[0]])*(density/10)
optimal_kB_err = np.load(f'{pathData}/{detector}/birks/kB_error.npy')*(density/10)
# optimal_kB_err = np.load(f'{pathData}/{detector}/birks/total_error.npy')

#Define variables
Tn = np.array([2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5, 6.0])
Tn_fine = np.arange(1.75, 6.26, 0.01)

#Make plot
fs.set_style(textwidth=345/2.8, ratio=0.8)
fig, ax1 = plt.subplots()


#Optimal kB values figure
plt.scatter(Tn, optimal_kB, color='black', marker='o', lw=0.5, s=25, facecolor='white', zorder=4)
plt.errorbar(Tn, optimal_kB, yerr=optimal_kB_err, color='black', linestyle='none', lw=0.4, zorder=3)

plt.plot(       Tn_fine, 
                promath.reciprocalConstantFunc(Tn_fine, poptReciprocal_kB[0], poptReciprocal_kB[1])*(density/10), 
                lw=0.5, 
                color='black', 
                linestyle='solid', 
                zorder=2)

plt.fill_between(       Tn_fine, 
                        (promath.reciprocalConstantFunc(Tn_fine, poptReciprocal_kB[0], poptReciprocal_kB[1]) + np.sqrt((1/Tn_fine)**2 * pcovReciprocal_kB[0]**2 + pcovReciprocal_kB[1]**2))*(density/10),
                        (promath.reciprocalConstantFunc(Tn_fine, poptReciprocal_kB[0], poptReciprocal_kB[1]) - np.sqrt((1/Tn_fine)**2 * pcovReciprocal_kB[0]**2 + pcovReciprocal_kB[1]**2))*(density/10),                     
                        color='black', alpha=0.2, zorder=1)

plt.ylim([0.0090, 0.020])#kB

plt.xlim([np.min(Tn_fine)-0.45, np.max(Tn_fine)+0.125])
plt.xlabel('Neutron Energy [MeV]')
plt.ylabel('$k_B$ [g/cm$^2$MeV]')

plt.savefig('/home/gheed/Documents/writing/mauritzson_lightYield_paper_2022/figures/kB_optimization_result_inset.pdf')

plt.show()





# ##########################################################
# ######### Figure 7 #######################################
# ######### Neutron Light yield method 2, 4, 6 MeV #########
# ##########################################################

gate = 'LG'
detector = 'EJ305'

numBins2= np.arange(0.00, 1.05, 0.047)
numBins4 = np.arange(0.00, 1.9, 0.06)
numBins6 = np.arange(0.00, 3.3, 0.075)
numBins2pen= np.arange(0.10, 1.05, 0.047)
numBins4pen = np.arange(0.35, 1.9, 0.06)
numBins6pen = np.arange(1.25, 3.3, 0.075)

#Load data
energy = 2000
nQDC       = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/nQDC_{gate}.npy')
randQDC    = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/randQDC_{gate}.npy')
nLength    = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/nLength.npy')
randLength = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/randLength.npy')
nQDC = prodata.calibrateMyDetector(f'{pathData}/{detector}/Ecal/popt_{gate}.npy', nQDC)
randQDC = prodata.calibrateMyDetector(f'{pathData}/{detector}/Ecal/popt_{gate}.npy', randQDC)

x_data_2, y_data_2 = prodata.randomTOFSubtractionQDC( nQDC,
                                                                randQDC,
                                                                nLength, 
                                                                randLength,
                                                                numBins2, 
                                                                plot=False)
energy = 4000
nQDC       = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/nQDC_{gate}.npy')
randQDC    = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/randQDC_{gate}.npy')
nLength    = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/nLength.npy')
randLength = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/randLength.npy')
nQDC = prodata.calibrateMyDetector(f'{pathData}/{detector}/Ecal/popt_{gate}.npy', nQDC)
randQDC = prodata.calibrateMyDetector(f'{pathData}/{detector}/Ecal/popt_{gate}.npy', randQDC)

x_data_4, y_data_4 = prodata.randomTOFSubtractionQDC( nQDC,
                                                                randQDC,
                                                                nLength, 
                                                                randLength,
                                                                numBins4, 
                                                                plot=False)
energy = 6000
nQDC       = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/nQDC_{gate}.npy')
randQDC    = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/randQDC_{gate}.npy')
nLength    = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/nLength.npy')
randLength = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/randLength.npy')
nQDC = prodata.calibrateMyDetector(f'{pathData}/{detector}/Ecal/popt_{gate}.npy', nQDC)
randQDC = prodata.calibrateMyDetector(f'{pathData}/{detector}/Ecal/popt_{gate}.npy', randQDC)

x_data_6, y_data_6 = prodata.randomTOFSubtractionQDC( nQDC,
                                                                randQDC,
                                                                nLength, 
                                                                randLength,
                                                                numBins6, 
                                                                plot=False)

#Load simulation
names = [ 'evtNum', 
        'optPhotonSum', 
        'optPhotonSumQE', 
        'optPhotonSumCompton', 
        'optPhotonSumComptonQE', 
        'optPhotonSumNPQE', 
        'xLoc', 
        'yLoc', 
        'zLoc', 
        'CsMin', 
        'optPhotonParentID', 
        'optPhotonParentCreatorProcess', 
        'optPhotonParentStartingEnergy',
        'total_edep',
        'gamma_edep',
        'proton_edep',
        'nScatters',
        'nScattersProton']

#load sim 2 MeV
energy = 2000
sim_iso_2 = pd.read_csv(f"{pathSim}/{detector}/neutrons/neutron_range/{int(energy)}keV/isotropic/1M/CSV_optphoton_data_sum.csv", names=names)
sim_pen_2 = pd.read_csv(f"{pathSim}/{detector}/neutrons/neutron_range/{int(energy)}keV/pencilbeam/CSV_optphoton_data_sum.csv", names=names)

#load sim 4 MeV
energy = 4000
sim_iso_4 = pd.read_csv(f"{pathSim}/{detector}/neutrons/neutron_range/{int(energy)}keV/isotropic/1M/CSV_optphoton_data_sum.csv", names=names)
sim_pen_4 = pd.read_csv(f"{pathSim}/{detector}/neutrons/neutron_range/{int(energy)}keV/pencilbeam/CSV_optphoton_data_sum.csv", names=names)

#load sim 6 MeV
energy = 6000
sim_iso_6 = pd.read_csv(f"{pathSim}/{detector}/neutrons/neutron_range/{int(energy)}keV/isotropic/1M/CSV_optphoton_data_sum.csv", names=names)
sim_pen_6 = pd.read_csv(f"{pathSim}/{detector}/neutrons/neutron_range/{int(energy)}keV/pencilbeam/CSV_optphoton_data_sum.csv", names=names)

#Find edep maximum location used for cut
y, x = np.histogram(sim_pen_2.proton_edep, bins=np.arange(0, 2000*1.2, .3))
x = prodata.getBinCenters(x)
edepMax_2 = x[np.argmax(y)]

y, x = np.histogram(sim_pen_4.proton_edep, bins=np.arange(0, 4000*1.2, .3))
x = prodata.getBinCenters(x)
edepMax_4 = x[np.argmax(y)]

y, x = np.histogram(sim_pen_6.proton_edep, bins=np.arange(0, 6000*1.2, 1))
x = prodata.getBinCenters(x)
edepMax_6 = x[np.argmax(y)]

edepLow = 0.99
edepHigh = 1.
#Cut pencilbeam on edep
sim_pen_2 = sim_pen_2.query(f'{2000*edepLow}<proton_edep<{2000*edepHigh}')
sim_pen_4 = sim_pen_4.query(f'{4000*edepLow}<proton_edep<{4000*edepHigh}')
sim_pen_6 = sim_pen_6.query(f'{6000*edepLow}<proton_edep<{6000*edepHigh}')

#randomize binning isotropic
y, x = np.histogram(sim_iso_2.optPhotonSumQE, bins=4096, range=[0, 4096]) 
sim_iso_2.optPhotonSumQE = prodata.getRandDist(x, y)

y, x = np.histogram(sim_iso_4.optPhotonSumQE, bins=4096, range=[0, 4096]) 
sim_iso_4.optPhotonSumQE = prodata.getRandDist(x, y)

y, x = np.histogram(sim_iso_6.optPhotonSumQE, bins=4096, range=[0, 4096]) 
sim_iso_6.optPhotonSumQE = prodata.getRandDist(x, y)

#randomize binning pencilbeam
y, x = np.histogram(sim_pen_2.optPhotonSumQE, bins=4096, range=[0, 4096]) 
sim_pen_2.optPhotonSumQE = prodata.getRandDist(x, y)

y, x = np.histogram(sim_pen_4.optPhotonSumQE, bins=4096, range=[0, 4096]) 
sim_pen_4.optPhotonSumQE = prodata.getRandDist(x, y)

y, x = np.histogram(sim_pen_6.optPhotonSumQE, bins=4096, range=[0, 4096]) 
sim_pen_6.optPhotonSumQE = prodata.getRandDist(x, y)

#smearing simulation
sim_iso_2.optPhotonSumQE = sim_iso_2.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, 0.3344))
sim_iso_4.optPhotonSumQE = sim_iso_4.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, 0.1191))
sim_iso_6.optPhotonSumQE = sim_iso_6.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, 0.0473))

sim_pen_2.optPhotonSumQE = sim_pen_2.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, 0.3344))
sim_pen_4.optPhotonSumQE = sim_pen_4.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, 0.1191))
sim_pen_6.optPhotonSumQE = sim_pen_6.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, 0.0473))

#calibrate isotropic
sim_iso_2.optPhotonSumQE = prodata.calibrateMyDetector(f'{pathData}/{detector}/Simcal/popt.npy', sim_iso_2.optPhotonSumQE, inverse=False)
sim_iso_4.optPhotonSumQE = prodata.calibrateMyDetector(f'{pathData}/{detector}/Simcal/popt.npy', sim_iso_4.optPhotonSumQE, inverse=False)
sim_iso_6.optPhotonSumQE = prodata.calibrateMyDetector(f'{pathData}/{detector}/Simcal/popt.npy', sim_iso_6.optPhotonSumQE, inverse=False)

sim_pen_2.optPhotonSumQE = prodata.calibrateMyDetector(f'{pathData}/{detector}/Simcal/popt.npy', sim_pen_2.optPhotonSumQE, inverse=False)
sim_pen_4.optPhotonSumQE = prodata.calibrateMyDetector(f'{pathData}/{detector}/Simcal/popt.npy', sim_pen_4.optPhotonSumQE, inverse=False)
sim_pen_6.optPhotonSumQE = prodata.calibrateMyDetector(f'{pathData}/{detector}/Simcal/popt.npy', sim_pen_6.optPhotonSumQE, inverse=False)

#bin simulations
y_iso_2, x_iso_2 = np.histogram(sim_iso_2.optPhotonSumQE, bins=numBins2)
x_iso_2 = prodata.getBinCenters(x_iso_2)

y_iso_4, x_iso_4 = np.histogram(sim_iso_4.optPhotonSumQE, bins=numBins4)
x_iso_4 = prodata.getBinCenters(x_iso_4)

y_iso_6, x_iso_6 = np.histogram(sim_iso_6.optPhotonSumQE, bins=numBins6)
x_iso_6 = prodata.getBinCenters(x_iso_6)


y_pen_2, x_pen_2 = np.histogram(sim_pen_2.optPhotonSumQE, bins=numBins2pen)
x_pen_2 = prodata.getBinCenters(x_pen_2)

y_pen_4, x_pen_4 = np.histogram(sim_pen_4.optPhotonSumQE, bins=numBins4pen)
x_pen_4 = prodata.getBinCenters(x_pen_4)

y_pen_6, x_pen_6 = np.histogram(sim_pen_6.optPhotonSumQE, bins=numBins6pen)
x_pen_6 = prodata.getBinCenters(x_pen_6)

#set integral to 1
y_iso_2 = prodata.integralNorm(y_iso_2, forcePositive=True)
y_iso_4 = prodata.integralNorm(y_iso_4, forcePositive=True)
y_iso_6 = prodata.integralNorm(y_iso_6, forcePositive=True)

y_pen_2 = prodata.integralNorm(y_pen_2, forcePositive=True)
y_pen_4 = prodata.integralNorm(y_pen_4, forcePositive=True)
y_pen_6 = prodata.integralNorm(y_pen_6, forcePositive=True)

y_data_2 = prodata.integralNorm(y_data_2, forcePositive=True)
y_data_4 = prodata.integralNorm(y_data_4, forcePositive=True)
y_data_6 = prodata.integralNorm(y_data_6, forcePositive=True)

#LY locations
sim_loc_2 = 0.390261
sim_loc_4 = 1.212736
sim_loc_6 = 2.381188




#Make figure

xMin = 0.1
xMax = 3.4

fs.set_style(textwidth=345)

plt.subplot(3,1,1)
penScale = 0.5
isoScale = 1.5
plt.scatter(x_data_2, y_data_2, s=3, color='black', zorder=3)
plt.step(x_iso_2, y_iso_2*isoScale, lw=0.5, color='black', ls='solid')
plt.step(x_pen_2, y_pen_2*penScale, lw=0.5, color='black')
plt.fill_between(x_pen_2, y1=y_pen_2*penScale, color=neutron_color, edgecolor='none', step='pre', zorder=1)
#fit gaussian
start = 0.24
stop = 0.69
popt, pcov = promath.gaussFit(x_pen_2, y_pen_2*penScale, start, stop)
plt.plot(np.arange(0, 0.78, 0.01), promath.gaussFunc(np.arange(0, 0.78, 0.01), popt[0], popt[1], popt[2]), color='black', linestyle='dashed')

plt.vlines(sim_loc_2, ymin=0, ymax=np.max(y_data_2[:-10])*1.1, lw=2, ls='dashed', color='black')
plt.xlim([xMin, xMax])
plt.xlabel('Counts')
plt.ylim([0, 0.178])
plt.xticks([])
plt.yticks([])

plt.subplot(3,1,2)
penScale = 0.18
isoScale = 1.50
plt.scatter(x_data_4, y_data_4, s=3, color='black', zorder=3)
plt.step(x_iso_4, y_iso_4*isoScale, lw=0.5, color='black', ls='solid')
plt.step(x_pen_4, prodata.dataAveraging(y_pen_4, 2)*penScale, lw=0.5, color='black')
plt.fill_between(x_pen_4, y1=prodata.dataAveraging(y_pen_4, 2)*penScale, color=neutron_color, edgecolor='none', step='pre', zorder=1)
#fit gaussian
start = 0.87
stop = 1.88
popt, pcov = promath.gaussFit(x_pen_4, y_pen_4*penScale, start, stop)
plt.plot(np.arange(0.47, 1.9, 0.01), promath.gaussFunc(np.arange(0.47, 1.9, 0.01), popt[0], popt[1], popt[2]), color='black', linestyle='dashed')

plt.vlines(sim_loc_4, ymin=0, ymax=np.max(y_data_4[:-20])*1.1, lw=2, ls='dashed', color='black')
plt.xlim([xMin, xMax])
plt.xlabel('Counts')
plt.ylim([0, 0.08])
plt.xticks([])
plt.yticks([])
plt.ylabel('Counts')

plt.subplot(3,1,3)
penScale = 0.11
isoScale = 1.55
plt.scatter(x_data_6, prodata.dataAveraging(y_data_6, 1), s=3, color='black', zorder=3)
plt.step(x_iso_6, prodata.dataAveraging(y_iso_6, 1)*isoScale, lw=0.5, color='black', ls='solid')
plt.step(x_pen_6, prodata.dataAveraging(y_pen_6, 1)*penScale, lw=0.5, color='black')
plt.fill_between(x_pen_6, y1=prodata.dataAveraging(y_pen_6, 1)*penScale, color=neutron_color, edgecolor='none', step='pre', zorder=1)
#fit gaussian
start = 1.5
stop = 3.25
popt, pcov = promath.gaussFit(x_pen_6, y_pen_6*penScale, start, stop)
plt.plot(np.arange(1.3, 3.3, 0.01), promath.gaussFunc(np.arange(1.3, 3.3, 0.01), popt[0], popt[1], popt[2]), color='black', linestyle='dashed')

plt.vlines(sim_loc_6, ymin=0, ymax=np.max(y_data_6[:-20])*1.1, lw=2, ls='dashed', color='black')
plt.xlim([xMin, xMax])
plt.ylim([0, 0.043])
plt.yticks([])
plt.xlabel('Scintillation Light Yield [MeV$_{ee}$]')


plt.tight_layout()
plt.subplots_adjust(hspace = .001, wspace = .001)

plt.savefig('/home/gheed/Documents/writing/mauritzson_lightYield_paper_2022/figures/neutron_LY_method_1.pdf')

plt.show()




#Make edep inset figure
fs.set_style(textwidth=345/2.2)
energy = 4000
sim_pen_edep = pd.read_csv(f"{pathSim}/{detector}/neutrons/neutron_range/{int(energy)}keV/pencilbeam/CSV_optphoton_data_sum.csv", names=names)

y_sim_edep, x_sim_edep = np.histogram(sim_pen_edep.proton_edep, bins=np.arange(energy*0.95, energy*1.05, 10))
x_sim_edep = prodata.getBinCenters(x_sim_edep)/1000
# edepMaxLoc = x_sim_edep[np.argmax(y_sim_edep)]

plt.vlines(energy*edepLow/1000, ymin=1, ymax=np.max(y_sim_edep)*1.35, lw=1, color='black', ls='dashed')
plt.vlines(energy*edepHigh/1000, ymin=1, ymax=np.max(y_sim_edep)*1.35, lw=1, color='black', ls='dashed')

plt.step(x_sim_edep, prodata.dataAveraging(y_sim_edep, 0), lw=1, color='black')
plt.fill_between(x_sim_edep, 0.1, prodata.dataAveraging(y_sim_edep, 0), color=neutron_color, step='pre')
plt.ylim([0, np.max(y_sim_edep)*1.35])
plt.xlim([3.875, 4.025])
# plt.xticks([])
plt.yticks([])
plt.ylabel('Counts')
plt.xlabel('Deposited Neutron Energy')

plt.savefig('/home/gheed/Documents/writing/mauritzson_lightYield_paper_2022/figures/neutron_LY_method_1_edep_inset.pdf')

plt.show()



# ##########################################################
# ######### Figure 8 #######################################
# ######### Light yield all methods ########################
# ##########################################################
numBins3 = np.arange(0.20, 3.25, 0.065)
numBins5 = np.arange(0.38, 3.5, 0.085)

#Load data
energy = 3000
gate = 'LG'
detector = 'NE213A'
nQDC       = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/nQDC_{gate}.npy')
randQDC    = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/randQDC_{gate}.npy')
nLength    = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/nLength.npy')
randLength = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/randLength.npy')
nQDC = prodata.calibrateMyDetector(f'{pathData}/{detector}/Ecal/popt_{gate}.npy', nQDC)
randQDC = prodata.calibrateMyDetector(f'{pathData}/{detector}/Ecal/popt_{gate}.npy', randQDC)

x_NE213A_data_3, y_NE213A_data_3 = prodata.randomTOFSubtractionQDC( nQDC,
                                                                randQDC,
                                                                nLength, 
                                                                randLength,
                                                                numBins3, 
                                                                plot=False)

detector = 'EJ305'
nQDC       = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/nQDC_{gate}.npy')
randQDC    = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/randQDC_{gate}.npy')
nLength    = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/nLength.npy')
randLength = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/randLength.npy')
nQDC = prodata.calibrateMyDetector(f'{pathData}/{detector}/Ecal/popt_{gate}.npy', nQDC)
randQDC = prodata.calibrateMyDetector(f'{pathData}/{detector}/Ecal/popt_{gate}.npy', randQDC)

x_EJ305_data_3, y_EJ305_data_3 = prodata.randomTOFSubtractionQDC(   nQDC,
                                                                randQDC,
                                                                nLength, 
                                                                randLength,
                                                                numBins3, 
                                                                plot=False)

detector = 'EJ321P'
nQDC       = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/nQDC_{gate}.npy')
randQDC    = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/randQDC_{gate}.npy')
nLength    = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/nLength.npy')
randLength = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/randLength.npy')
nQDC = prodata.calibrateMyDetector(f'{pathData}/{detector}/Ecal/popt_{gate}.npy', nQDC)
randQDC = prodata.calibrateMyDetector(f'{pathData}/{detector}/Ecal/popt_{gate}.npy', randQDC)

x_EJ321P_data_3, y_EJ321P_data_3 = prodata.randomTOFSubtractionQDC( nQDC,
                                                                randQDC,
                                                                nLength, 
                                                                randLength,
                                                                numBins3, 
                                                                plot=False)

detector = 'EJ331'
nQDC       = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/nQDC_{gate}.npy')
randQDC    = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/randQDC_{gate}.npy')
nLength    = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/nLength.npy')
randLength = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/randLength.npy')
nQDC = prodata.calibrateMyDetector(f'{pathData}/{detector}/Ecal/popt_{gate}.npy', nQDC)
randQDC = prodata.calibrateMyDetector(f'{pathData}/{detector}/Ecal/popt_{gate}.npy', randQDC)

x_EJ331_data_3, y_EJ331_data_3 = prodata.randomTOFSubtractionQDC(   nQDC,
                                                                randQDC,
                                                                nLength, 
                                                                randLength,
                                                                numBins3, 
                                                                plot=False)
energy = 5000
gate = 'LG'

detector = 'NE213A'
nQDC       = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/nQDC_{gate}.npy')
randQDC    = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/randQDC_{gate}.npy')
nLength    = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/nLength.npy')
randLength = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/randLength.npy')
nQDC = prodata.calibrateMyDetector(f'{pathData}/{detector}/Ecal/popt_{gate}.npy', nQDC)
randQDC = prodata.calibrateMyDetector(f'{pathData}/{detector}/Ecal/popt_{gate}.npy', randQDC)

x_NE213A_data_5, y_NE213A_data_5 = prodata.randomTOFSubtractionQDC( nQDC,
                                                                randQDC,
                                                                nLength, 
                                                                randLength,
                                                                numBins5, 
                                                                plot=False)

detector = 'EJ305'
nQDC       = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/nQDC_{gate}.npy')
randQDC    = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/randQDC_{gate}.npy')
nLength    = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/nLength.npy')
randLength = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/randLength.npy')
nQDC = prodata.calibrateMyDetector(f'{pathData}/{detector}/Ecal/popt_{gate}.npy', nQDC)
randQDC = prodata.calibrateMyDetector(f'{pathData}/{detector}/Ecal/popt_{gate}.npy', randQDC)

x_EJ305_data_5, y_EJ305_data_5 = prodata.randomTOFSubtractionQDC(   nQDC,
                                                                randQDC,
                                                                nLength, 
                                                                randLength,
                                                                numBins5, 
                                                                plot=False)

detector = 'EJ321P'
nQDC       = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/nQDC_{gate}.npy')
randQDC    = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/randQDC_{gate}.npy')
nLength    = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/nLength.npy')
randLength = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/randLength.npy')
nQDC = prodata.calibrateMyDetector(f'{pathData}/{detector}/Ecal/popt_{gate}.npy', nQDC)
randQDC = prodata.calibrateMyDetector(f'{pathData}/{detector}/Ecal/popt_{gate}.npy', randQDC)

x_EJ321P_data_5, y_EJ321P_data_5 = prodata.randomTOFSubtractionQDC( nQDC,
                                                                randQDC,
                                                                nLength, 
                                                                randLength,
                                                                numBins5, 
                                                                plot=False)

detector = 'EJ331'
nQDC       = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/nQDC_{gate}.npy')
randQDC    = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/randQDC_{gate}.npy')
nLength    = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/nLength.npy')
randLength = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/randLength.npy')
nQDC = prodata.calibrateMyDetector(f'{pathData}/{detector}/Ecal/popt_{gate}.npy', nQDC)
randQDC = prodata.calibrateMyDetector(f'{pathData}/{detector}/Ecal/popt_{gate}.npy', randQDC)

x_EJ331_data_5, y_EJ331_data_5 = prodata.randomTOFSubtractionQDC(   nQDC,
                                                                randQDC,
                                                                nLength, 
                                                                randLength,
                                                                numBins5, 
                                                                plot=False)

#Load simulation
names = [ 'evtNum', 
        'optPhotonSum', 
        'optPhotonSumQE', 
        'optPhotonSumCompton', 
        'optPhotonSumComptonQE', 
        'optPhotonSumNPQE', 
        'xLoc', 
        'yLoc', 
        'zLoc', 
        'CsMin', 
        'optPhotonParentID', 
        'optPhotonParentCreatorProcess', 
        'optPhotonParentStartingEnergy',
        'total_edep',
        'gamma_edep',
        'proton_edep',
        'nScatters',
        'nScattersProton']

#load sim 3 MeV
energy = 3000
detector = 'NE213A'
NE213A_sim_iso_3 = pd.read_csv(f"{pathSim}/{detector}/neutrons/neutron_range/{int(energy)}keV/isotropic/CSV_optphoton_data_sum.csv", names=names)
NE213A_sim_pen_3 = pd.read_csv(f"{pathSim}/{detector}/neutrons/neutron_range/{int(energy)}keV/pencilbeam/CSV_optphoton_data_sum.csv", names=names)
detector = 'EJ305'
EJ305_sim_iso_3 = pd.read_csv(f"{pathSim}/{detector}/neutrons/neutron_range/{int(energy)}keV/isotropic/CSV_optphoton_data_sum.csv", names=names)
EJ305_sim_pen_3 = pd.read_csv(f"{pathSim}/{detector}/neutrons/neutron_range/{int(energy)}keV/pencilbeam/CSV_optphoton_data_sum.csv", names=names)
detector = 'EJ321P'
EJ321P_sim_iso_3 = pd.read_csv(f"{pathSim}/{detector}/neutrons/neutron_range/{int(energy)}keV/isotropic/CSV_optphoton_data_sum.csv", names=names)
EJ321P_sim_pen_3 = pd.read_csv(f"{pathSim}/{detector}/neutrons/neutron_range/{int(energy)}keV/pencilbeam/CSV_optphoton_data_sum.csv", names=names)
detector = 'EJ331'
EJ331_sim_iso_3 = pd.read_csv(f"{pathSim}/{detector}/neutrons/neutron_range/{int(energy)}keV/isotropic/CSV_optphoton_data_sum.csv", names=names)
EJ331_sim_pen_3 = pd.read_csv(f"{pathSim}/{detector}/neutrons/neutron_range/{int(energy)}keV/pencilbeam/CSV_optphoton_data_sum.csv", names=names)

#load sim 5 MeV
energy = 5000
detector = 'NE213A'
NE213A_sim_iso_5 = pd.read_csv(f"{pathSim}/{detector}/neutrons/neutron_range/{int(energy)}keV/isotropic/CSV_optphoton_data_sum.csv", names=names)
NE213A_sim_pen_5 = pd.read_csv(f"{pathSim}/{detector}/neutrons/neutron_range/{int(energy)}keV/pencilbeam/CSV_optphoton_data_sum.csv", names=names)
detector = 'EJ305'
EJ305_sim_iso_5 = pd.read_csv(f"{pathSim}/{detector}/neutrons/neutron_range/{int(energy)}keV/isotropic/CSV_optphoton_data_sum.csv", names=names)
EJ305_sim_pen_5 = pd.read_csv(f"{pathSim}/{detector}/neutrons/neutron_range/{int(energy)}keV/pencilbeam/CSV_optphoton_data_sum.csv", names=names)
detector = 'EJ321P'
EJ321P_sim_iso_5 = pd.read_csv(f"{pathSim}/{detector}/neutrons/neutron_range/{int(energy)}keV/isotropic/CSV_optphoton_data_sum.csv", names=names)
EJ321P_sim_pen_5 = pd.read_csv(f"{pathSim}/{detector}/neutrons/neutron_range/{int(energy)}keV/pencilbeam/CSV_optphoton_data_sum.csv", names=names)
detector = 'EJ331'
EJ331_sim_iso_5 = pd.read_csv(f"{pathSim}/{detector}/neutrons/neutron_range/{int(energy)}keV/isotropic/CSV_optphoton_data_sum.csv", names=names)
EJ331_sim_pen_5 = pd.read_csv(f"{pathSim}/{detector}/neutrons/neutron_range/{int(energy)}keV/pencilbeam/CSV_optphoton_data_sum.csv", names=names)

#Find edep maximum location NE213A
y, x = np.histogram(NE213A_sim_pen_3.total_edep, bins=np.arange(0, 3000*1.2, .3))
x = prodata.getBinCenters(x)
NE213A_edepMax_3 = x[np.argmax(y)]
y, x = np.histogram(NE213A_sim_pen_5.total_edep, bins=np.arange(0, 5000*1.2, .3))
x = prodata.getBinCenters(x)
NE213A_edepMax_5 = x[np.argmax(y)]

y, x = np.histogram(EJ305_sim_pen_3.total_edep, bins=np.arange(0, 3000*1.2, .3))
x = prodata.getBinCenters(x)
EJ305_edepMax_3 = x[np.argmax(y)]
y, x = np.histogram(EJ305_sim_pen_5.total_edep, bins=np.arange(0, 5000*1.2, .3))
x = prodata.getBinCenters(x)
EJ305_edepMax_5 = x[np.argmax(y)]

y, x = np.histogram(EJ321P_sim_pen_3.total_edep, bins=np.arange(0, 3000*1.2, .3))
x = prodata.getBinCenters(x)
EJ321P_edepMax_3 = x[np.argmax(y)]
y, x = np.histogram(EJ321P_sim_pen_5.total_edep, bins=np.arange(0, 5000*1.2, .3))
x = prodata.getBinCenters(x)
EJ321P_edepMax_5 = x[np.argmax(y)]

y, x = np.histogram(EJ331_sim_pen_3.total_edep, bins=np.arange(0, 3000*1.2, .3))
x = prodata.getBinCenters(x)
EJ331_edepMax_3 = x[np.argmax(y)]
y, x = np.histogram(EJ331_sim_pen_5.total_edep, bins=np.arange(0, 5000*1.2, .3))
x = prodata.getBinCenters(x)
EJ331_edepMax_5 = x[np.argmax(y)]

#Cut pencilbeam on edep
NE213A_sim_pen_3 = NE213A_sim_pen_3.query(f'{3000*edepLow}<proton_edep<{3000*edepHigh}')
NE213A_sim_pen_5 = NE213A_sim_pen_5.query(f'{5000*edepLow}<proton_edep<{5000*edepHigh}')

EJ305_sim_pen_3 = EJ305_sim_pen_3.query(f'{3000*edepLow}<proton_edep<{3000*edepHigh}')
EJ305_sim_pen_5 = EJ305_sim_pen_5.query(f'{5000*edepLow}<proton_edep<{5000*edepHigh}')

EJ321P_sim_pen_3 = EJ321P_sim_pen_3.query(f'{3000*edepLow}<proton_edep<{3000*edepHigh}')
EJ321P_sim_pen_5 = EJ321P_sim_pen_5.query(f'{5000*edepLow}<proton_edep<{EJ321P_edepMax_5*edepHigh}')

EJ331_sim_pen_3 = EJ331_sim_pen_3.query(f'{3000*edepLow}<proton_edep<{3000*edepHigh}')
EJ331_sim_pen_5 = EJ331_sim_pen_5.query(f'{5000*edepLow}<proton_edep<{5000*edepHigh}')

#randomize binning isotropic
y, x = np.histogram(NE213A_sim_iso_3.optPhotonSumQE, bins=4096, range=[0, 4096]) 
NE213A_sim_iso_3.optPhotonSumQE = prodata.getRandDist(x, y)
y, x = np.histogram(NE213A_sim_iso_5.optPhotonSumQE, bins=4096, range=[0, 4096]) 
NE213A_sim_iso_5.optPhotonSumQE = prodata.getRandDist(x, y)
y, x = np.histogram(EJ305_sim_iso_3.optPhotonSumQE, bins=4096, range=[0, 4096]) 
EJ305_sim_iso_3.optPhotonSumQE = prodata.getRandDist(x, y)
y, x = np.histogram(EJ305_sim_iso_5.optPhotonSumQE, bins=4096, range=[0, 4096]) 
EJ305_sim_iso_5.optPhotonSumQE = prodata.getRandDist(x, y)
y, x = np.histogram(EJ321P_sim_iso_3.optPhotonSumQE, bins=4096, range=[0, 4096]) 
EJ321P_sim_iso_3.optPhotonSumQE = prodata.getRandDist(x, y)
y, x = np.histogram(EJ321P_sim_iso_5.optPhotonSumQE, bins=4096, range=[0, 4096]) 
EJ321P_sim_iso_5.optPhotonSumQE = prodata.getRandDist(x, y)
y, x = np.histogram(EJ331_sim_iso_3.optPhotonSumQE, bins=4096, range=[0, 4096]) 
EJ331_sim_iso_3.optPhotonSumQE = prodata.getRandDist(x, y)
y, x = np.histogram(EJ331_sim_iso_5.optPhotonSumQE, bins=4096, range=[0, 4096]) 
EJ331_sim_iso_5.optPhotonSumQE = prodata.getRandDist(x, y)
#randomize binning pencilbeam
y, x = np.histogram(NE213A_sim_pen_3.optPhotonSumQE, bins=4096, range=[0, 4096]) 
NE213A_sim_pen_3.optPhotonSumQE = prodata.getRandDist(x, y)
y, x = np.histogram(NE213A_sim_pen_5.optPhotonSumQE, bins=4096, range=[0, 4096]) 
NE213A_sim_pen_5.optPhotonSumQE = prodata.getRandDist(x, y)
y, x = np.histogram(EJ305_sim_pen_3.optPhotonSumQE, bins=4096, range=[0, 4096]) 
EJ305_sim_pen_3.optPhotonSumQE = prodata.getRandDist(x, y)
y, x = np.histogram(EJ305_sim_pen_5.optPhotonSumQE, bins=4096, range=[0, 4096]) 
EJ305_sim_pen_5.optPhotonSumQE = prodata.getRandDist(x, y)
y, x = np.histogram(EJ321P_sim_pen_3.optPhotonSumQE, bins=4096, range=[0, 4096]) 
EJ321P_sim_pen_3.optPhotonSumQE = prodata.getRandDist(x, y)
y, x = np.histogram(EJ321P_sim_pen_5.optPhotonSumQE, bins=4096, range=[0, 4096]) 
EJ321P_sim_pen_5.optPhotonSumQE = prodata.getRandDist(x, y)
y, x = np.histogram(EJ331_sim_pen_3.optPhotonSumQE, bins=4096, range=[0, 4096]) 
EJ331_sim_pen_3.optPhotonSumQE = prodata.getRandDist(x, y)
y, x = np.histogram(EJ331_sim_pen_5.optPhotonSumQE, bins=4096, range=[0, 4096]) 
EJ331_sim_pen_5.optPhotonSumQE = prodata.getRandDist(x, y)

#smearing simulation
NE213A_sim_iso_3.optPhotonSumQE = NE213A_sim_iso_3.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, 0.16061025))
NE213A_sim_pen_3.optPhotonSumQE = NE213A_sim_pen_3.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, 0.16061025))

EJ305_sim_iso_3.optPhotonSumQE = EJ305_sim_iso_3.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, 0.19705592808894773))
EJ305_sim_pen_3.optPhotonSumQE = EJ305_sim_pen_3.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, 0.19705592808894773))

EJ321P_sim_iso_3.optPhotonSumQE = EJ321P_sim_iso_3.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, 0.22896365416938824))
EJ321P_sim_pen_3.optPhotonSumQE = EJ321P_sim_pen_3.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, 0.22896365416938824))

EJ331_sim_iso_3.optPhotonSumQE = EJ331_sim_iso_3.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, 0.21846958647979367))
EJ331_sim_pen_3.optPhotonSumQE = EJ331_sim_pen_3.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, 0.21846958647979367))

NE213A_sim_iso_5.optPhotonSumQE = NE213A_sim_iso_5.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, 0.07307463580073176))
NE213A_sim_pen_5.optPhotonSumQE = NE213A_sim_pen_5.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, 0.07307463580073176))

EJ305_sim_iso_5.optPhotonSumQE = EJ305_sim_iso_5.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, 0.07629809843820852))
EJ305_sim_pen_5.optPhotonSumQE = EJ305_sim_pen_5.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, 0.07629809843820852))

EJ321P_sim_iso_5.optPhotonSumQE = EJ321P_sim_iso_5.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, 0.16097878253664566))
EJ321P_sim_pen_5.optPhotonSumQE = EJ321P_sim_pen_5.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, 0.16097878253664566))

EJ331_sim_iso_5.optPhotonSumQE = EJ331_sim_iso_5.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, 0.15727721101584047))
EJ331_sim_pen_5.optPhotonSumQE = EJ331_sim_pen_5.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, 0.15727721101584047))

#calibrate isotropic
detector = 'NE213A'
NE213A_sim_iso_3.optPhotonSumQE = prodata.calibrateMyDetector(f'{pathData}/{detector}/Simcal/popt.npy', NE213A_sim_iso_3.optPhotonSumQE, inverse=False)
NE213A_sim_iso_5.optPhotonSumQE = prodata.calibrateMyDetector(f'{pathData}/{detector}/Simcal/popt.npy', NE213A_sim_iso_5.optPhotonSumQE, inverse=False)
NE213A_sim_pen_3.optPhotonSumQE = prodata.calibrateMyDetector(f'{pathData}/{detector}/Simcal/popt.npy', NE213A_sim_pen_3.optPhotonSumQE, inverse=False)
NE213A_sim_pen_5.optPhotonSumQE = prodata.calibrateMyDetector(f'{pathData}/{detector}/Simcal/popt.npy', NE213A_sim_pen_5.optPhotonSumQE, inverse=False)
detector = 'EJ305'
EJ305_sim_iso_3.optPhotonSumQE = prodata.calibrateMyDetector(f'{pathData}/{detector}/Simcal/popt.npy', EJ305_sim_iso_3.optPhotonSumQE, inverse=False)
EJ305_sim_iso_5.optPhotonSumQE = prodata.calibrateMyDetector(f'{pathData}/{detector}/Simcal/popt.npy', EJ305_sim_iso_5.optPhotonSumQE, inverse=False)
EJ305_sim_pen_3.optPhotonSumQE = prodata.calibrateMyDetector(f'{pathData}/{detector}/Simcal/popt.npy', EJ305_sim_pen_3.optPhotonSumQE, inverse=False)
EJ305_sim_pen_5.optPhotonSumQE = prodata.calibrateMyDetector(f'{pathData}/{detector}/Simcal/popt.npy', EJ305_sim_pen_5.optPhotonSumQE, inverse=False)
detector = 'EJ321P'
EJ321P_sim_iso_3.optPhotonSumQE = prodata.calibrateMyDetector(f'{pathData}/{detector}/Simcal/popt.npy', EJ321P_sim_iso_3.optPhotonSumQE, inverse=False)
EJ321P_sim_iso_5.optPhotonSumQE = prodata.calibrateMyDetector(f'{pathData}/{detector}/Simcal/popt.npy', EJ321P_sim_iso_5.optPhotonSumQE, inverse=False)
EJ321P_sim_pen_3.optPhotonSumQE = prodata.calibrateMyDetector(f'{pathData}/{detector}/Simcal/popt.npy', EJ321P_sim_pen_3.optPhotonSumQE, inverse=False)
EJ321P_sim_pen_5.optPhotonSumQE = prodata.calibrateMyDetector(f'{pathData}/{detector}/Simcal/popt.npy', EJ321P_sim_pen_5.optPhotonSumQE, inverse=False)
detector = 'EJ331'
EJ331_sim_iso_3.optPhotonSumQE = prodata.calibrateMyDetector(f'{pathData}/{detector}/Simcal/popt.npy', EJ331_sim_iso_3.optPhotonSumQE, inverse=False)
EJ331_sim_iso_5.optPhotonSumQE = prodata.calibrateMyDetector(f'{pathData}/{detector}/Simcal/popt.npy', EJ331_sim_iso_5.optPhotonSumQE, inverse=False)
EJ331_sim_pen_3.optPhotonSumQE = prodata.calibrateMyDetector(f'{pathData}/{detector}/Simcal/popt.npy', EJ331_sim_pen_3.optPhotonSumQE, inverse=False)
EJ331_sim_pen_5.optPhotonSumQE = prodata.calibrateMyDetector(f'{pathData}/{detector}/Simcal/popt.npy', EJ331_sim_pen_5.optPhotonSumQE, inverse=False)

#bin simulations
y_NE213A_iso_3, x_NE213A_iso_3 = np.histogram(NE213A_sim_iso_3.optPhotonSumQE, bins=numBins3)
x_NE213A_iso_3 = prodata.getBinCenters(x_NE213A_iso_3)
y_EJ305_iso_3, x_EJ305_iso_3 = np.histogram(EJ305_sim_iso_3.optPhotonSumQE, bins=numBins3)
x_EJ305_iso_3 = prodata.getBinCenters(x_EJ305_iso_3)
y_EJ321P_iso_3, x_EJ321P_iso_3 = np.histogram(EJ321P_sim_iso_3.optPhotonSumQE, bins=numBins3)
x_EJ321P_iso_3 = prodata.getBinCenters(x_EJ321P_iso_3)
y_EJ331_iso_3, x_EJ331_iso_3 = np.histogram(EJ331_sim_iso_3.optPhotonSumQE, bins=numBins3)
x_EJ331_iso_3 = prodata.getBinCenters(x_EJ331_iso_3)
y_NE213A_pen_3, x_NE213A_pen_3 = np.histogram(NE213A_sim_pen_3.optPhotonSumQE, bins=numBins3)
x_NE213A_pen_3 = prodata.getBinCenters(x_NE213A_pen_3)
y_EJ305_pen_3, x_EJ305_pen_3 = np.histogram(EJ305_sim_pen_3.optPhotonSumQE, bins=numBins3)
x_EJ305_pen_3 = prodata.getBinCenters(x_EJ305_pen_3)
y_EJ321P_pen_3, x_EJ321P_pen_3 = np.histogram(EJ321P_sim_pen_3.optPhotonSumQE, bins=numBins3)
x_EJ321P_pen_3 = prodata.getBinCenters(x_EJ321P_pen_3)
y_EJ331_pen_3, x_EJ331_pen_3 = np.histogram(EJ331_sim_pen_3.optPhotonSumQE, bins=numBins3)
x_EJ331_pen_3 = prodata.getBinCenters(x_EJ331_pen_3)

y_NE213A_iso_5, x_NE213A_iso_5 = np.histogram(NE213A_sim_iso_5.optPhotonSumQE, bins=numBins5)
x_NE213A_iso_5 = prodata.getBinCenters(x_NE213A_iso_5)
y_EJ305_iso_5, x_EJ305_iso_5 = np.histogram(EJ305_sim_iso_5.optPhotonSumQE, bins=numBins5)
x_EJ305_iso_5 = prodata.getBinCenters(x_EJ305_iso_5)
y_EJ321P_iso_5, x_EJ321P_iso_5 = np.histogram(EJ321P_sim_iso_5.optPhotonSumQE, bins=numBins5)
x_EJ321P_iso_5 = prodata.getBinCenters(x_EJ321P_iso_5)
y_EJ331_iso_5, x_EJ331_iso_5 = np.histogram(EJ331_sim_iso_5.optPhotonSumQE, bins=numBins5)
x_EJ331_iso_5 = prodata.getBinCenters(x_EJ331_iso_5)
y_NE213A_pen_5, x_NE213A_pen_5 = np.histogram(NE213A_sim_pen_5.optPhotonSumQE, bins=numBins5)
x_NE213A_pen_5 = prodata.getBinCenters(x_NE213A_pen_5)
y_EJ305_pen_5, x_EJ305_pen_5 = np.histogram(EJ305_sim_pen_5.optPhotonSumQE, bins=numBins5)
x_EJ305_pen_5 = prodata.getBinCenters(x_EJ305_pen_5)
y_EJ321P_pen_5, x_EJ321P_pen_5 = np.histogram(EJ321P_sim_pen_5.optPhotonSumQE, bins=numBins5)
x_EJ321P_pen_5 = prodata.getBinCenters(x_EJ321P_pen_5)
y_EJ331_pen_5, x_EJ331_pen_5 = np.histogram(EJ331_sim_pen_5.optPhotonSumQE, bins=numBins5)
x_EJ331_pen_5 = prodata.getBinCenters(x_EJ331_pen_5)

#set integral to 1
y_NE213A_iso_3 = prodata.integralNorm(y_NE213A_iso_3, forcePositive=True)
y_NE213A_iso_5 = prodata.integralNorm(y_NE213A_iso_5, forcePositive=True)
y_NE213A_pen_3 = prodata.integralNorm(y_NE213A_pen_3, forcePositive=True)
y_NE213A_pen_5 = prodata.integralNorm(y_NE213A_pen_5, forcePositive=True)
y_NE213A_data_3 = prodata.integralNorm(y_NE213A_data_3, forcePositive=True)
y_NE213A_data_5 = prodata.integralNorm(y_NE213A_data_5, forcePositive=True)

y_EJ305_iso_3 = prodata.integralNorm(y_EJ305_iso_3, forcePositive=True)
y_EJ305_iso_5 = prodata.integralNorm(y_EJ305_iso_5, forcePositive=True)
y_EJ305_pen_3 = prodata.integralNorm(y_EJ305_pen_3, forcePositive=True)
y_EJ305_pen_5 = prodata.integralNorm(y_EJ305_pen_5, forcePositive=True)
y_EJ305_data_3 = prodata.integralNorm(y_EJ305_data_3, forcePositive=True)
y_EJ305_data_5 = prodata.integralNorm(y_EJ305_data_5, forcePositive=True)

y_EJ331_iso_3 = prodata.integralNorm(y_EJ331_iso_3, forcePositive=True)
y_EJ331_iso_5 = prodata.integralNorm(y_EJ331_iso_5, forcePositive=True)
y_EJ331_pen_3 = prodata.integralNorm(y_EJ331_pen_3, forcePositive=True)
y_EJ331_pen_5 = prodata.integralNorm(y_EJ331_pen_5, forcePositive=True)
y_EJ331_data_3 = prodata.integralNorm(y_EJ331_data_3, forcePositive=True)
y_EJ331_data_5 = prodata.integralNorm(y_EJ331_data_5, forcePositive=True)

y_EJ321P_iso_3 = prodata.integralNorm(y_EJ321P_iso_3, forcePositive=True)
y_EJ321P_iso_5 = prodata.integralNorm(y_EJ321P_iso_5, forcePositive=True)
y_EJ321P_pen_3 = prodata.integralNorm(y_EJ321P_pen_3, forcePositive=True)
y_EJ321P_pen_5 = prodata.integralNorm(y_EJ321P_pen_5, forcePositive=True)
y_EJ321P_data_3 = prodata.integralNorm(y_EJ321P_data_3, forcePositive=True)
y_EJ321P_data_5 = prodata.integralNorm(y_EJ321P_data_5, forcePositive=True)

#LY locations

NE213A_sim_loc_3 = 0.997694
NE213A_sim_loc_5 = 2.191278

EJ305_sim_loc_3 = 0.653017
EJ305_sim_loc_5 = 1.737186

EJ331_sim_loc_3 = 0.833597
EJ331_sim_loc_5 = 1.807878

EJ321P_sim_loc_3 = 0.543146
EJ321P_sim_loc_5 = 1.329585



#Make figure
xMax3 = 1.80
xMin3 = 0.249
xMax5 = 3.07
xMin5 = 0.65

fs.set_style(textwidth=345)

plt.subplot(4,2,1)
plt.scatter(x_NE213A_data_3, y_NE213A_data_3*2.1, s=3, color='black', zorder=3)
# plt.errorbar(x_NE213A_data_3, y_NE213A_data_3*2.1, yerr=np.sqrt(y_NE213A_data_3*2.1), linestyle='none', zorder=2, color='black')
plt.step(x_NE213A_iso_3, y_NE213A_iso_3*2.1*1.08, lw=0.5, color='black', ls='solid')
plt.step(x_NE213A_pen_3, y_NE213A_pen_3*0.6, lw=0.5, color='black')
plt.fill_between(x_NE213A_pen_3, y1=y_NE213A_pen_3*0.6, color=NE213A_color, edgecolor='none', alpha=0.8, step='pre', zorder=1)
plt.vlines(NE213A_sim_loc_3, ymin=0, ymax=1000, lw=1.5, ls='dashed', color='black')
plt.xlim([xMin3, xMax3])
plt.ylim([0, .2])
plt.yticks([])

plt.subplot(4,2,2)
plt.scatter(x_NE213A_data_5, y_NE213A_data_5*3.0, s=3, color='black', zorder=3)
# plt.errorbar(x_NE213A_data_5, y_NE213A_data_5*3.0, yerr=np.sqrt(y_NE213A_data_5*3.0), linestyle='none', zorder=2, color='black')
plt.step(x_NE213A_iso_5, y_NE213A_iso_5*3.0*1.08, lw=0.5, color='black', ls='solid')
plt.step(x_NE213A_pen_5, y_NE213A_pen_5*0.65, lw=0.5, color='black')
plt.fill_between(x_NE213A_pen_5, y1=y_NE213A_pen_5*0.65, color=NE213A_color, edgecolor='none', alpha=0.8, step='pre', zorder=1)
plt.vlines(NE213A_sim_loc_5, ymin=0, ymax=1000, lw=1.5, ls='dashed', color='black')
plt.xlim([xMin5, xMax5])
plt.ylim([0, .2])
plt.yticks([])

plt.subplot(4,2,3)
plt.scatter(x_EJ305_data_3, y_EJ305_data_3*2.8, s=3, color='black', zorder=3)
# plt.errorbar(x_EJ305_data_3, y_EJ305_data_3*2.8, yerr=np.sqrt(y_EJ305_data_3*2.8), linestyle='none', zorder=2, color='black')
plt.step(x_EJ305_iso_3, y_EJ305_iso_3*2.8*1.08, lw=0.5, color='black', ls='solid')
plt.step(x_EJ305_pen_3, y_EJ305_pen_3*0.9, lw=0.5, color='black')
plt.fill_between(x_EJ305_pen_3, y1=y_EJ305_pen_3*0.9, color=EJ305_color, edgecolor='none', alpha=0.8, step='pre', zorder=1)
plt.vlines(EJ305_sim_loc_3, ymin=0, ymax=1000, lw=1.5, ls='dashed', color='black')
plt.xlim([xMin3, xMax3])
plt.ylim([0, .45])
plt.yticks([])

plt.subplot(4,2,4)
plt.scatter(x_EJ305_data_5, y_EJ305_data_5*5.0, s=3, color='black', zorder=3)
# plt.errorbar(x_EJ305_data_5, y_EJ305_data_5*5.0, yerr=np.sqrt(y_EJ305_data_5*5.0), linestyle='none', zorder=2, color='black')
plt.step(x_EJ305_iso_5, y_EJ305_iso_5*5.0*1.08, lw=0.5, color='black', ls='solid')
plt.step(x_EJ305_pen_5, y_EJ305_pen_5, lw=0.5, color='black')
plt.fill_between(x_EJ305_pen_5, y1=y_EJ305_pen_5, color=EJ305_color, edgecolor='none', alpha=0.8, step='pre', zorder=1)
plt.vlines(EJ305_sim_loc_5, ymin=0, ymax=1000, lw=1.5, ls='dashed', color='black')
plt.xlim([xMin5, xMax5])
plt.ylim([0, .45])
plt.yticks([])

plt.subplot(4,2,5)
plt.scatter(x_EJ331_data_3, y_EJ331_data_3*2.3, s=3, color='black', zorder=3)
# plt.errorbar(x_EJ331_data_3, y_EJ331_data_3*2.3, yerr=np.sqrt(y_EJ331_data_3*2.3), linestyle='none', zorder=2, color='black')
plt.step(x_EJ331_iso_3, y_EJ331_iso_3*2.3*1.04, lw=0.5, color='black', ls='solid')
plt.step(x_EJ331_pen_3, y_EJ331_pen_3*0.8, lw=0.5, color='black')
plt.fill_between(x_EJ331_pen_3, y1=y_EJ331_pen_3*0.8, color=EJ331_color, edgecolor='none', alpha=0.8, step='pre', zorder=1)
plt.vlines(EJ331_sim_loc_3, ymin=0, ymax=1000, lw=1.5, ls='dashed', color='black')
plt.xlim([xMin3, xMax3])
plt.ylim([0, .3])
plt.yticks([])
plt.ylabel('Counts')

plt.subplot(4,2,6)
plt.scatter(x_EJ331_data_5, y_EJ331_data_5*3.8, s=3, color='black', zorder=3)
# plt.errorbar(x_EJ331_data_5, y_EJ331_data_5*3.8, yerr=np.sqrt(y_EJ331_data_5*3.8), linestyle='none', zorder=2, color='black')
plt.step(x_EJ331_iso_5, y_EJ331_iso_5*3.8*1.05, lw=0.5, color='black', ls='solid')
plt.step(x_EJ331_pen_5, y_EJ331_pen_5, lw=0.5, color='black')
plt.fill_between(x_EJ331_pen_5, y1=y_EJ331_pen_5, color=EJ331_color, edgecolor='none', alpha=0.8, step='pre', zorder=1)
plt.vlines(EJ331_sim_loc_5, ymin=0, ymax=1000, lw=1.5, ls='dashed', color='black')
plt.xlim([xMin5, xMax5])
plt.ylim([0, .3])
plt.yticks([])

plt.subplot(4,2,7)
plt.scatter(x_EJ321P_data_3, y_EJ321P_data_3*1.7, s=3, color='black', zorder=3)
# plt.errorbar(x_EJ321P_data_3, y_EJ321P_data_3*1.7, yerr=np.sqrt(y_EJ321P_data_3*1.7), linestyle='none', zorder=2, color='black')
plt.step(x_EJ321P_iso_3, y_EJ321P_iso_3*1.7*1.10, lw=0.5, color='black', ls='solid')
plt.step(x_EJ321P_pen_3, y_EJ321P_pen_3*0.8, lw=0.5, color='black')
plt.fill_between(x_EJ321P_pen_3, y1=y_EJ321P_pen_3*0.8, color=EJ321P_color, edgecolor='none', alpha=0.8, step='pre', zorder=1)
plt.vlines(EJ321P_sim_loc_3, ymin=0, ymax=1000, lw=1.5, ls='dashed', color='black')
plt.xlim([xMin3, xMax3])
plt.ylim([0, .32])
plt.yticks([])

plt.subplot(4,2,8)
plt.scatter(x_EJ321P_data_5, y_EJ321P_data_5*2.8, s=3, color='black', zorder=3)
# plt.errorbar(x_EJ321P_data_5, y_EJ321P_data_5*2.8, yerr=np.sqrt(y_EJ321P_data_5*2.8), linestyle='none', zorder=2, color='black')
plt.step(x_EJ321P_iso_5, y_EJ321P_iso_5*2.8*1.08, lw=0.5, color='black', ls='solid')
plt.step(x_EJ321P_pen_5, y_EJ321P_pen_5, lw=0.5, color='black')
plt.fill_between(x_EJ321P_pen_5, y1=y_EJ321P_pen_5, color=EJ321P_color, edgecolor='none', alpha=0.8, step='pre', zorder=1)
plt.vlines(EJ321P_sim_loc_5, ymin=0, ymax=1000, lw=1.5, ls='dashed', color='black')
plt.xlim([xMin5, xMax5])
plt.yticks([])
plt.ylim([0, .32])

plt.gca().xaxis.set_major_formatter(plt.FormatStrFormatter('%.1f'))
plt.xlabel('Scintillation Light Yield [MeV$_{ee}$]')

plt.tight_layout()

plt.subplots_adjust(hspace = .001, wspace = .001)
plt.savefig('/home/gheed/Documents/writing/mauritzson_lightYield_paper_2022/figures/LY_all_detectors.pdf')

plt.show()


# ##########################################################
# ######### Figure 9 #######################################
# ######### Neutron Light yield methods compared ###########
# ##########################################################
energy = 4000
gate = 'LG'
detector = 'EJ305'
numBins = np.arange(0, 2.5, 0.04)

nQDC       = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/nQDC_{gate}.npy')
randQDC    = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/randQDC_{gate}.npy')
nLength    = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/nLength.npy')
randLength = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/randLength.npy')
nQDC = prodata.calibrateMyDetector(f'{pathData}/{detector}/Ecal/popt_{gate}.npy', nQDC)
randQDC = prodata.calibrateMyDetector(f'{pathData}/{detector}/Ecal/popt_{gate}.npy', randQDC)

x_data, y_data = prodata.randomTOFSubtractionQDC( nQDC,
                                                                randQDC,
                                                                nLength, 
                                                                randLength,
                                                                numBins,#np.arange(0,3,0.03), 
                                                                plot=False)

names = [ 'evtNum', 
        'optPhotonSum', 
        'optPhotonSumQE', 
        'optPhotonSumCompton', 
        'optPhotonSumComptonQE', 
        'optPhotonSumNPQE', 
        'xLoc', 
        'yLoc', 
        'zLoc', 
        'CsMin', 
        'optPhotonParentID', 
        'optPhotonParentCreatorProcess', 
        'optPhotonParentStartingEnergy',
        'total_edep',
        'gamma_edep',
        'proton_edep',
        'nScatters',
        'nScattersProton']

#load sim
sim_iso = pd.read_csv(f"{pathSim}/{detector}/neutrons/neutron_range/{int(energy)}keV/isotropic/CSV_optphoton_data_sum.csv", names=names)
sim_pen = pd.read_csv(f"{pathSim}/{detector}/neutrons/neutron_range/{int(energy)}keV/pencilbeam/CSV_optphoton_data_sum.csv", names=names)

#Find edep maximum location
y_sim_edep, x_sim_edep = np.histogram(sim_pen.total_edep, bins=np.arange(0, energy*1.2, .3))
x_sim_edep = prodata.getBinCenters(x_sim_edep)
edepMaxLoc = x_sim_edep[np.argmax(y_sim_edep)]

#Cut on edep for pencilbeam
sim_pen = sim_pen.query(f'{energy*edepLow}<proton_edep<{energy*edepHigh}')

#randomize binning
y, x = np.histogram(sim_iso.optPhotonSumQE, bins=4096, range=[0, 4096]) 
sim_iso.optPhotonSumQE = prodata.getRandDist(x, y)
y, x = np.histogram(sim_pen.optPhotonSumQE, bins=4096, range=[0, 4096]) 
sim_pen.optPhotonSumQE = prodata.getRandDist(x, y)

#calibrate simulation
sim_iso.optPhotonSumQE = prodata.calibrateMyDetector(f'{pathData}/{detector}/Simcal/popt.npy', sim_iso.optPhotonSumQE, inverse=False)
sim_pen.optPhotonSumQE = prodata.calibrateMyDetector(f'{pathData}/{detector}/Simcal/popt.npy', sim_pen.optPhotonSumQE, inverse=False)

#smear simulation
smearFactor = 0.1191
sim_iso.optPhotonSumQE = sim_iso.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, smearFactor) )
sim_pen.optPhotonSumQE = sim_pen.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, smearFactor) )

#bin simulation
y_sim_iso, x_sim_iso = np.histogram(sim_iso.optPhotonSumQE, bins=numBins)
x_sim_iso = prodata.getBinCenters(x_sim_iso)
y_sim_pen, x_sim_pen = np.histogram(sim_pen.optPhotonSumQE, bins=numBins)
x_sim_pen = prodata.getBinCenters(x_sim_pen)

#LY locations
# HH_loc_3 = 1.083929
# TP_loc_3 = 1.0227
# FD_loc_3 = 1.0581
# sim_loc_3 = 1.015851
HH_loc = 1.259133 #0.776871 #
TP_loc = 1.32925 #0.73100 #
FD_loc = 1.284# 0.800 #
sim_loc = 1.211741 #0.754602 #


#Make main figure
smoothStep = 3
fs.set_style(textwidth=345)
plt.scatter(x_data, prodata.dataAveraging(y_data, smoothStep), s=20, color='black', zorder=3)
plt.step(x_sim_iso, prodata.dataAveraging(y_sim_iso, smoothStep)*5.15, lw=1, ls='dashed', color='black', zorder=2)
plt.step(x_sim_pen, prodata.dataAveraging(y_sim_pen, smoothStep)*3.35, color='black', lw=1, zorder=2)
plt.fill_between(x_sim_pen, 0, prodata.dataAveraging(y_sim_pen, smoothStep)*3.35, color=neutron_color, step='pre', zorder=1)

plt.vlines(HH_loc, ymin=0, ymax=2000, lw=0.5, ls='solid', color='black', label='HH')
plt.vlines(TP_loc, ymin=0, ymax=2000, lw=0.5, ls='dashed', color='black', label='TP')
plt.vlines(FD_loc, ymin=0, ymax=2000, lw=0.5, ls='dashdot', color='black', label='FD')
plt.vlines(sim_loc, ymin=0, ymax=2000, lw=0.5, ls='dotted', color='black', label='SMD')

plt.ylim(0, np.max(y_data)*0.95)
plt.xlim(0.75, 1.75)
plt.ylabel('Counts')
plt.xlabel('Scintillation Light Yield [MeV$_{ee}$]')
# plt.yticks([])
# plt.xticks([])
plt.legend()

plt.savefig('/home/gheed/Documents/writing/mauritzson_lightYield_paper_2022/figures/neutron_LY_method_2.pdf')

plt.show()


# ##########################################################
# ######### Figure 10 NE213A ###############################
# ######### Figure 12 EJ305  ###############################
# ######### Figure 14 EJ331  ###############################
# ######### Figure 15 EJ321P ###############################
# ######### HH, TP, FD & Sim with fits #####################
# ##########################################################

def lightYieldPlot(detector, figureNum, HH_data_LG, TP_data_LG, FD_data_LG, HH_data_SG, TP_data_SG, FD_data_SG, LY_sim, kornilov_LG, kornilov_SG, cecil_LG, cecil_SG, kornilov_SMD):
        if detector == 'NE 213A':
                mainColor = NE213A_color
                detectorName = 'NE213A'
                L1 = 3.67#2.47
                p = [0.83, 2.82, 0.25, 0.93]
        elif detector == 'EJ 305':
                mainColor = EJ305_color
                detectorName = 'EJ305'
                L1 = 6.55#8.5700387
                # p = [1.0, 8.2, 0.1, 0.88] #NE224
                p = [0.817, 2.63, 0.297, 1]
        elif detector == 'EJ 331':
                mainColor = EJ331_color
                detectorName = 'EJ331'
                L1 = 5.34#7.666969
                p = [0.817, 2.63, 0.297, 1] #EJ309
        elif detector == 'EJ 321P':
                mainColor = EJ321P_color
                detectorName = 'EJ321P'
                L1 = 6.68#6.617600
                p = [0.43, 0.77, 0.26, 2.13]
    
        fs.set_style(textwidth=345, ratio=1.61)
        titleParamters = dict(ha='right', va='center', fontsize=10, color='black')
        
        plt.subplot(3,1,1)
        plt.scatter(HH_data_LG.energy, HH_data_LG.HH_loc, s=40, lw=0.5, color=mainColor, marker='o', label='Measured data', facecolor='white', alpha=1, zorder=2)
        plt.errorbar(HH_data_LG.energy, HH_data_LG.HH_loc, yerr=HH_data_LG.HH_loc_err, linestyle='none', color=mainColor, lw=0.5, alpha=1, zorder=1)
        plt.plot(np.arange(1.75, 6.25, 0.01), promath.kornilovEq(np.arange(1.75, 6.25, 0.01), kornilov_LG.query('method=="HH_data"').L0.item(), L1), lw=.5, color='black', alpha=.9, label='Kornilov Eq.[5]')
        # plt.plot(np.arange(1.75, 6.25, 0.01), promath.cecilEq(np.arange(1.75, 6.25, 0.01), cecil_LG.query('method=="HH_data"').C.item(), p[0], p[1], p[2], p[3]), lw=.5, color='black', alpha=.9, ls='dashed')

        plt.scatter(LY_sim.energy,  LY_sim.means, s=35, lw=0.5, edgecolors='none', facecolors=mainColor, marker='v', alpha=1, zorder=4)
        plt.errorbar(LY_sim.energy, LY_sim.means, yerr=LY_sim.means_err, linestyle='none', color=mainColor, lw=0.5, alpha=1, zorder=3)
        plt.plot(np.arange(1.75, 6.25, 0.01), promath.kornilovEq(np.arange(1.75, 6.25, 0.01), kornilov_SMD.L0.item(), L1), lw=1, color=mainColor, alpha=.9, ls='dashed')        
        
        plt.legend(loc=2)
        plt.text(4.15, np.max(HH_data_LG.HH_loc)*1.05, 'HH', **titleParamters)
        plt.xlim([1.75, 6.25])
        plt.ylim([np.min(HH_data_LG.HH_loc)*0.29, np.max(HH_data_LG.HH_loc)*1.15])

        ############################
        plt.subplot(3,1,2)
        plt.scatter(TP_data_LG.energy, TP_data_LG.TP_loc, s=40, lw=0.5, color=mainColor, marker='o', facecolor='white', alpha=1, zorder=2)
        plt.errorbar(TP_data_LG.energy, TP_data_LG.TP_loc, yerr=TP_data_LG.TP_loc_err, linestyle='none', color=mainColor, lw=0.5, alpha=1, zorder=1)
        plt.plot(np.arange(1.75, 6.25, 0.01), promath.kornilovEq(np.arange(1.75, 6.25, 0.01), kornilov_LG.query('method=="TP_data"').L0.item(), L1), lw=0.5, color='black', alpha=.9)
        # plt.plot(np.arange(1.75, 6.25, 0.01), promath.cecilEq(np.arange(1.75, 6.25, 0.01), cecil_LG.query('method=="TP_data"').C.item(), p[0], p[1], p[2], p[3]), lw=0.5, color='black', alpha=.9, ls='dashed', label='Cecil Eq.[4]')

        plt.scatter(LY_sim.energy,  LY_sim.means, s=35,  lw=0.5, edgecolors='none', facecolors=mainColor, marker='v', alpha=1, label='SMD', zorder=4)
        plt.errorbar(LY_sim.energy, LY_sim.means, yerr=LY_sim.means_err, linestyle='none', color=mainColor, lw=0.5, alpha=1, zorder=3)
        plt.plot(np.arange(1.75, 6.25, 0.01), promath.kornilovEq(np.arange(1.75, 6.25, 0.01), kornilov_SMD.L0.item(), L1), lw=1, color=mainColor, alpha=.9, ls='dashed', label='Kornilov Eq.[5]')

        plt.text(4.15, np.max(HH_data_LG.HH_loc)*1.05, 'TP', **titleParamters)
        plt.xlim([1.75, 6.25])
        plt.ylim([np.min(HH_data_LG.HH_loc)*0.29, np.max(HH_data_LG.HH_loc)*1.15])
        # plt.legend(loc=2)
        plt.legend(loc=2)
        plt.ylabel('Scintillation Light Yield [MeV$_{ee}$]')

        ############################
        plt.subplot(3,1,3)
        plt.scatter(FD_data_LG.energy, FD_data_LG.FD_loc, s=40, lw=0.5, color=mainColor, marker='o', facecolor='white', alpha=1, zorder=2)
        plt.errorbar(FD_data_LG.energy, FD_data_LG.FD_loc, yerr=FD_data_LG.FD_loc_err, linestyle='none', color=mainColor, lw=0.5, alpha=1, zorder=1)
        plt.plot(np.arange(1.75, 6.25, 0.01), promath.kornilovEq(np.arange(1.75, 6.25, 0.01), kornilov_LG.query('method=="FD_data"').L0.item(), L1), lw=0.5, color='black', alpha=.9)
        # plt.plot(np.arange(1.75, 6.25, 0.01), promath.cecilEq(np.arange(1.75, 6.25, 0.01), cecil_LG.query('method=="FD_data"').C.item(), p[0], p[1], p[2], p[3]), lw=0.5, color='black', alpha=.9, ls='dashed')

        plt.scatter(LY_sim.energy,  LY_sim.means, s=35, lw=0.5, edgecolors='none', facecolors=mainColor, marker='v', alpha=1, zorder=4)
        plt.errorbar(LY_sim.energy, LY_sim.means, yerr=LY_sim.means_err, linestyle='none', color=mainColor, lw=0.5, alpha=1, zorder=3)
        plt.plot(np.arange(1.75, 6.25, 0.01), promath.kornilovEq(np.arange(1.75, 6.25, 0.01), kornilov_SMD.L0.item(), L1), lw=1, color=mainColor, alpha=.9, ls='dashed', label='Kornilov Eq.[5]')

        plt.text(4.15, np.max(HH_data_LG.HH_loc*1.05), 'FD', **titleParamters)
        plt.text(6.20, np.max(HH_data_LG.HH_loc*.15), f'{detector}', **titleParamters)
        plt.xlim([1.75, 6.25])
        plt.ylim([np.min(HH_data_LG.HH_loc)*0.29, np.max(HH_data_LG.HH_loc)*1.15])
        plt.xlabel('Proton Energy [MeV]')
        
        plt.subplots_adjust(hspace = .001, wspace = .001)
        # plt.savefig(f'/home/gheed/Documents/writing/mauritzson_lightYield_paper_2022/figures/{detectorName}_result.pdf')
        
        plt.show()

detector = 'NE213A'
figureNum = 10
HH_data_LG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/HH_data_LG.pkl')
TP_data_LG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/TP_data_LG.pkl')
FD_data_LG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/FD_data_LG.pkl')
HH_data_SG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/HH_data_SG.pkl')
TP_data_SG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/TP_data_SG.pkl')
FD_data_SG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/FD_data_SG.pkl')
LY_sim = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/simulation/sim_LightYield.pkl')
kornilov_LG = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/HH_TP_FD/kornilov_LG_result.pkl')
kornilov_SG = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/HH_TP_FD/kornilov_SG_result.pkl')
cecil_LG = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/HH_TP_FD/cecil_LG_result.pkl')
cecil_SG = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/HH_TP_FD/cecil_SG_result.pkl')
kornilov_SMD = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/simulation/kornilov_result.pkl')
cecil_SMD = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/simulation/cecil_result.pkl')

print((1-HH_data_LG.HH_loc.to_numpy()/LY_sim.means.to_numpy())*100)
print((1-TP_data_LG.TP_loc.to_numpy()/LY_sim.means.to_numpy())*100)
print((1-FD_data_LG.FD_loc.to_numpy()/LY_sim.means.to_numpy())*100)
lightYieldPlot('NE 213A', figureNum, HH_data_LG, TP_data_LG, FD_data_LG, HH_data_SG, TP_data_SG, FD_data_SG, LY_sim, kornilov_LG, kornilov_SG, cecil_LG, cecil_SG, kornilov_SMD)

detector = 'EJ305'
figureNum = 12
HH_data_LG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/HH_data_LG.pkl')
TP_data_LG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/TP_data_LG.pkl')
FD_data_LG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/FD_data_LG.pkl')
HH_data_SG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/HH_data_SG.pkl')
TP_data_SG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/TP_data_SG.pkl')
FD_data_SG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/FD_data_SG.pkl')
LY_sim = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/simulation/sim_LightYield.pkl')
kornilov_LG = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/HH_TP_FD/kornilov_LG_result.pkl')
kornilov_SG = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/HH_TP_FD/kornilov_SG_result.pkl')
cecil_LG = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/HH_TP_FD/cecil_LG_result.pkl')
cecil_SG = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/HH_TP_FD/cecil_SG_result.pkl')
kornilov_SMD = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/simulation/kornilov_result.pkl')
cecil_SMD = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/simulation/cecil_result.pkl')

print((1-HH_data_LG.HH_loc.to_numpy()/LY_sim.means.to_numpy())*100)
print((1-TP_data_LG.TP_loc.to_numpy()/LY_sim.means.to_numpy())*100)
print((1-FD_data_LG.FD_loc.to_numpy()/LY_sim.means.to_numpy())*100)
lightYieldPlot('EJ 305', figureNum, HH_data_LG, TP_data_LG, FD_data_LG, HH_data_SG, TP_data_SG, FD_data_SG, LY_sim, kornilov_LG, kornilov_SG, cecil_LG, cecil_SG, kornilov_SMD)

detector = 'EJ331'
figureNum = 14
HH_data_LG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/HH_data_LG.pkl')
TP_data_LG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/TP_data_LG.pkl')
FD_data_LG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/FD_data_LG.pkl')
HH_data_SG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/HH_data_SG.pkl')
TP_data_SG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/TP_data_SG.pkl')
FD_data_SG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/FD_data_SG.pkl')
LY_sim = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/simulation/sim_LightYield.pkl')
kornilov_LG = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/HH_TP_FD/kornilov_LG_result.pkl')
kornilov_SG = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/HH_TP_FD/kornilov_SG_result.pkl')
cecil_LG = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/HH_TP_FD/cecil_LG_result.pkl')
cecil_SG = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/HH_TP_FD/cecil_SG_result.pkl')
kornilov_SMD = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/simulation/kornilov_result.pkl')
cecil_SMD = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/simulation/cecil_result.pkl')

print((1-HH_data_LG.HH_loc.to_numpy()/LY_sim.means.to_numpy())*100)
print((1-TP_data_LG.TP_loc.to_numpy()/LY_sim.means.to_numpy())*100)
print((1-FD_data_LG.FD_loc.to_numpy()/LY_sim.means.to_numpy())*100)
lightYieldPlot('EJ 331', figureNum, HH_data_LG, TP_data_LG, FD_data_LG, HH_data_SG, TP_data_SG, FD_data_SG, LY_sim, kornilov_LG, kornilov_SG, cecil_LG, cecil_SG, kornilov_SMD)


detector = 'EJ321P'
figureNum = 15
HH_data_LG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/HH_data_LG.pkl')
TP_data_LG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/TP_data_LG.pkl')
FD_data_LG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/FD_data_LG.pkl')
HH_data_SG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/HH_data_SG.pkl')
TP_data_SG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/TP_data_SG.pkl')
FD_data_SG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/FD_data_SG.pkl')
LY_sim = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/simulation/sim_LightYield.pkl')
kornilov_LG = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/HH_TP_FD/kornilov_LG_result.pkl')
kornilov_SG = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/HH_TP_FD/kornilov_SG_result.pkl')
cecil_LG = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/HH_TP_FD/cecil_LG_result.pkl')
cecil_SG = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/HH_TP_FD/cecil_SG_result.pkl')
kornilov_SMD = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/simulation/kornilov_result.pkl')
cecil_SMD = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/simulation/cecil_result.pkl')

print((1-HH_data_LG.HH_loc.to_numpy()/LY_sim.means.to_numpy())*100)
print((1-TP_data_LG.TP_loc.to_numpy()/LY_sim.means.to_numpy())*100)
print((1-FD_data_LG.FD_loc.to_numpy()/LY_sim.means.to_numpy())*100)
lightYieldPlot('EJ 321P', figureNum, HH_data_LG, TP_data_LG, FD_data_LG, HH_data_SG, TP_data_SG, FD_data_SG, LY_sim, kornilov_LG, kornilov_SG, cecil_LG, cecil_SG, kornilov_SMD)


# ##########################################################
# ######### Figure 11 ######################################
# ######### NE213A compared with old data ##################
# ##########################################################
# This work NE213A
detector = 'NE213A'
LY_sim = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/simulation/sim_LightYield.pkl') #C = 0.731371
# kornilov_SMD = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/simulation/kornilov_result.pkl')
# cecil_SMD = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/simulation/cecil_result.pkl')

HH_data_LG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/HH_data_LG.pkl') #C=0.751487

NE213A_E = LY_sim.energy
NE213A_LY = LY_sim.means
NE213A_LY_err = LY_sim.means_err

# Scherzinger, NE213 Data
# Data taken from https://doi.org/10.1016/j.nima.2016.10.011 using "Plot stealer": https://apps.automeris.io/wpd/
NE213_E = np.array([2, 2.2, 2.4, 2.6, 2.8, 3.0, 3.2, 3.4, 3.6, 3.8, 4.0, 4.2, 4.4, 4.6, 4.8, 5.0, 5.2, 5.4, 5.6, 5.8])
NE213_LY_HH_LG = np.array([0.655151539173894, 0.7150465373541994, 0.7918484860496036, 0.9024066820650116, 0.987647692590417, 1.115084012265827, 1.2171887330236846, 1.2855516198890884, 1.4298516497969507, 1.607907927024816, 1.7015879993802225, 1.769936472818077, 1.9142365027259391, 2.092321606808903, 2.2197435130567635, 2.338740770902172, 2.483040800810034, 2.517647440355435, 2.7041571928408503, 2.8568962845787134])
NE213_LY_TP_LG = np.array([0.6886097574637304, 0.7444127037472685, 0.7752618607516935, 0.8977202711028949, 0.9535652271326942, 1.1093093597714665, 1.2068139808435552, 1.2876127261524668, 1.4267770122668453, 1.5576093653727658, 1.7217274407662573, 1.7359127317537661, 1.9083627401557155, 2.039195093261636, 2.153405590096902, 2.309149722735674, 2.398280401053044, 2.570772419201254, 2.643281241247969, 2.8323951156668343])
NE213_LY_FD_LG = np.array([0.5813888888888892, 0.6847222222222222, 0.7632638888888894, 0.8334027777777782, 0.9450000000000003, 1.0152083333333337, 1.085555555555556, 1.1886805555555555, 1.4159722222222224, 1.4696527777777784, 1.54, 1.6928472222222224, 1.8126388888888891, 1.9242361111111113, 2.0605555555555557, 2.188541666666667, 2.325, 2.37875, 2.5233333333333334, 2.701041666666667])
NE213_LY_HH_SG = np.array([0.4948093644038787, 0.571596899671734, 0.6146281876195858, 0.7251863836349943, 0.7935348570728489, 0.8703368057682539, 1.0483930829961192, 1.116755969861523, 1.2273141658769307, 1.3294188866347887, 1.4146454837326452, 1.4998864942580505, 1.6273228139334603, 1.6788075771388624, 1.7809122978967202, 1.8745923702521268, 1.9091845963699778, 1.9606693595753804, 2.1471791120607957, 2.384308822098667])
NE213_LY_TP_SG = np.array([0.49693328852293694,0.5278244552736227, 0.6002912675740766, 0.6560942138576138, 0.745224892174984, 0.8677253122724466, 1.0234694449112185, 1.0709824679325597, 1.2101047443006774, 1.2409539013051019, 1.346748445639388, 1.4276312104408218, 1.5250938217666492, 1.5809387777964483, 1.686775331876995, 1.7758640004481034, 1.8067551671987896, 1.8542261804738693, 2.0516719879011927, 2.2991514031255247])
NE213_LY_FD_SG = np.array([0.5070138888888889, 0.5525000000000002, 0.6227777777777779, 0.6515972222222222, 0.7962500000000001, 0.8664583333333331, 0.9532638888888889, 1.0068055555555562, 1.1350000000000002, 1.2713888888888891, 1.325138888888889, 1.3870138888888892, 1.523402777777778, 1.618541666666667, 1.680416666666667, 1.8580555555555558, 1.8292361111111115, 1.9820833333333334, 2.0687500000000005, 2.3125694444444447])
8.954
# Gagnon-Moisan, NE213 Data
# Data taken from https://doi.org/10.1016/j.nima.2016.10.011 using "Plot stealer": https://apps.automeris.io/wpd/
NE213_E_GagMo = np.array([2, 2.2, 2.4, 2.6, 2.8, 3.0, 3.2, 3.4, 3.6, 3.8, 4.0, 4.2, 4.4, 4.6, 4.8, 5.0, 5.2, 5.4, 5.6, 5.8])
NE213_LY_GagMo = np.array([0.5947712418300655,0.7254901960784315,0.7908496732026147,0.9019607843137254,1.0130718954248366,1.1045751633986929,1.2222222222222223,1.3333333333333335,1.4379084967320261,1.5424836601307192,1.6797385620915035,1.7843137254901962,1.9215686274509804,2.019607843137255,2.1372549019607843,2.2745098039215685,2.3856209150326797,2.5098039215686274,2.6470588235294117,2.7712418300653594])


Tn = np.arange(0, 15, 0.001)
fs.set_style(textwidth=345)

#This work
plt.scatter(NE213A_E, NE213A_LY,  s=45, lw=0.5, edgecolors='none', facecolors=NE213A_color, marker='v', alpha=1, label='NE 213A, SMD', zorder=4)
plt.errorbar(NE213A_E, NE213A_LY, yerr=NE213A_LY_err, linestyle='none', color=NE213A_color, lw=0.5, alpha=1)

#Scherzinger data
plt.scatter(NE213_E, NE213_LY_TP_LG,  s=35, lw=0.5, edgecolors='black', facecolors='white', marker='s', alpha=1, label='NE 213, Scherzinger et al.', zorder=3)
# plt.errorbar(NE213_E, NE213_LY_TP_LG, yerr=0, linestyle='none', color=EJ305_color, lw=0.5, alpha=1)

#Scherzinger data
plt.scatter(NE213_E_GagMo, NE213_LY_GagMo,  s=35, lw=0.5, edgecolors='black', facecolors='white', marker='o', alpha=1, label='NE 213, Gagnon-Moisan et al.', zorder=3)
# plt.errorbar(NE213_E_GagMo, NE213_LY_GagMo, yerr=0, linestyle='none', color=EJ305_color, lw=0.5, alpha=1)

plt.legend(loc=1)

plt.xlim([1.75, 6.25])
plt.ylim([0.38, 3.20])

plt.ylabel('Scintillation Light Yield [MeV$_{ee}$]')
plt.xlabel('Proton Energy [MeV]')
plt.legend(loc=2)
# plt.grid('both')
# plt.yscale('log')
# plt.xscale('log')

plt.savefig(f'/home/gheed/Documents/writing/mauritzson_lightYield_paper_2022/figures/NE213A_comparison.pdf')
plt.show()

# ##########################################################
# ######### Figure 12 EJ305 ################################
# ######### HH, TP, FD & Sim with fits #####################
# ##########################################################

detector = 'EJ305'
#load data
HH_data_LG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/HH_data_LG.pkl')
TP_data_LG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/TP_data_LG.pkl')
FD_data_LG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/FD_data_LG.pkl')
HH_data_SG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/HH_data_SG.pkl')
TP_data_SG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/TP_data_SG.pkl')
FD_data_SG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/FD_data_SG.pkl')
LY_sim = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/simulation/sim_LightYield.pkl')
kornilov_LG = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/HH_TP_FD/kornilov_LG_result.pkl')
kornilov_SG = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/HH_TP_FD/kornilov_SG_result.pkl')
cecil_LG = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/HH_TP_FD/cecil_LG_result.pkl')
cecil_SG = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/HH_TP_FD/cecil_SG_result.pkl')
kornilov_SMD = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/simulation/kornilov_result.pkl')
cecil_SMD = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/simulation/cecil_result.pkl')

mainColor = EJ305_color
L1 = 6.55
p = [0.817, 2.63, 0.297, 1]

#make figure
fs.set_style(textwidth=345)
titleParamters = dict(ha='right', va='center', fontsize=10, color='black')

plt.scatter(TP_data_LG.energy, TP_data_LG.TP_loc, s=40, lw=0.5, color='black', marker='o', label='Measured data (TP)', facecolor='white', alpha=1, zorder=2)
plt.errorbar(TP_data_LG.energy, TP_data_LG.TP_loc, yerr=TP_data_LG.TP_loc_err, linestyle='none', color='black', lw=0.5, alpha=1, zorder=1)
# plt.plot(np.arange(1.75, 6.25, 0.01), promath.cecilEq(np.arange(1.75, 6.25, 0.01), cecil_LG.query('method=="TP_data"').C.item(), p[0], p[1], p[2], p[3]), lw=.5, color='black', alpha=.9, ls='dashed', label='Cecil Eq.[4]')
plt.plot(np.arange(1.75, 6.25, 0.01), promath.kornilovEq(np.arange(1.75, 6.25, 0.01), kornilov_LG.query('method=="TP_data"').L0.item(), L1), lw=.5, color='black', alpha=.9, label='Kornilov Eq.[5]')

plt.scatter(LY_sim.energy,  LY_sim.means,  s=35, lw=0.5, edgecolors='none', facecolors=mainColor, marker='v', alpha=1, zorder=4, label='SMD')
plt.errorbar(LY_sim.energy, LY_sim.means, yerr=LY_sim.means_err, linestyle='none', color=mainColor, lw=0.5, alpha=1, zorder=3)
# plt.plot(np.arange(1.75, 6.25, 0.01), promath.cecilEq(np.arange(1.75, 6.25, 0.01), cecil_SMD.C.item(), p[0], p[1], p[2], p[3]), lw=.5, color=mainColor, alpha=.9, ls='dashed', label='Cecil Eq.[4]')
plt.plot(np.arange(1.75, 6.25, 0.01), promath.kornilovEq(np.arange(1.75, 6.25, 0.01), kornilov_SMD.L0.item(), L1), lw=.5, color=mainColor, alpha=.9, ls='dashed', label='Kornilov Eq.[5]')

plt.text(4.50, np.max(TP_data_LG.TP_loc*1.075), f'EJ 305', **titleParamters)

plt.xlim([1.75, 6.25])
plt.ylim([np.min(TP_data_LG.TP_loc)*0.29, np.max(TP_data_LG.TP_loc)*1.15])
plt.xlabel('Proton Energy [MeV]')
plt.ylabel('Scintillation Light Yield [MeV$_{ee}$]')
plt.legend()
plt.savefig(f'/home/gheed/Documents/writing/mauritzson_lightYield_paper_2022/figures/{detector}_result.pdf')

plt.show()


# ##########################################################
# ######### Figure 13 ######################################
# ######### EJ305 compared with old data ###################
# ##########################################################
#Load data
# This work EJ305
detector = 'EJ305'
LY_sim = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/simulation/sim_LightYield.pkl') #C = 0.731371
# kornilov_SMD = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/simulation/kornilov_result.pkl')
# cecil_SMD = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/simulation/cecil_result.pkl')

# HH_data_LG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/HH_data_LG.pkl') #C=0.751487
# TP_data_LG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/TP_data_LG.pkl')[2:-1] #C=0.707807
# FD_data_LG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/FD_data_LG.pkl')[2:-1] #C=0.769690
# LY_sim = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/simulation/sim_LightYield.pkl')
# kornilov_LG = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/HH_TP_FD/kornilov_LG_result.pkl')
# cecil_SG = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/HH_TP_FD/cecil_SG_result.pkl')
EJ305_E = LY_sim.energy
EJ305_LY = LY_sim.means
EJ305_LY_err = LY_sim.means_err

# J.B. Czirr, NE224 Data 
# Data taken from doi:https://doi.org/10.1016/0029-554X(64)90163-6
NE224_E_low = np.array([ 0.25,  0.50,  1.00,  3.00])
NE224_LY_low = np.array([0.016, 0.053, 0.208, 0.980])

# R. Madey, NE224 Data 
# Data taken from doi:https://doi.org/10.1016/0029-554X(78)90154-4
NE224_E_high    = np.array([2.43, 5.20, 10.4, 15.60])
NE224_LY_high   = np.array([0.77, 2.42, 6.21, 10.30])
NE224_E_high_err  = np.array([0.06, 0.08, 0.09, 0.06])
NE224_LY_high_err = np.array([0.04, 0.10, 0.20, 0.33])


# Pywell, BC505 parametrization
# Data taken from doi: https://doi.org/10.1016/j.nima.2006.05.087
BC505_E = np.array([0.2027155532093236, 0.22342169014334642, 0.25169434808663393, 0.27673368640467727, 0.308732593102015, 0.3452688870781154, 0.3851882955222254, 0.4349719776833363, 0.48642622685396636, 0.5466273292772947, 0.6083215553301647, 0.6836086723419372 ,0.7497518413088498 ,0.8262976972474473 ,0.9151118900191566 ,1.0036543600229482 ,1.1169426003576362 ,1.2369966144902556 ,1.3370467763504978 ,1.5024890964748043 ,1.7049189388554453 ,1.9159444281396687, 2.153089491858052, 2.5277943059464354, 2.813152178269168, 3.2235363430994717, 3.7937748043265445, 4.574981590992881, 5.557387627540643, 6.718195459536577, 8.2409275815872, 9.676071830210798, 10.980577856747972, 12.64452313098269, 14.141382353583564, 16.848310913129346, 19.120821877746003])
BC505_LY = np.array([0.014103177592922596, 0.016325156087986976, 0.019397230783351663,0.022219640685346327,0.026126640899032594,0.030881646773954996,0.0365022582882076,0.04428846922821918,0.05345625102265105,0.06452106249821987,0.07747140198074559,0.09350706556107681,0.10937928681542515,0.12929043831192646,0.15282447520228015,0.1787654441433481,0.21352598742268125,0.25239296421280505,0.28761971899512906,0.35080610839719467,0.43236594759565444,0.5191367296907093,0.6233213915495543,0.805225101990812,0.9567792179224461,1.1669196746561048,1.507453165941185,1.9780662508223115,2.6366278200222877,3.4416815950876347,4.5634817480009255,5.624021178570566,6.682240702139851,7.980968122429481,9.286546476350324,11.62541955896983,13.456128081632754])

pywellScale = 0.76021422 #err = 0.0854235 (~11.2%). used to fit my SMD data

#########################
#EJ305 comparison figure
Tn = np.arange(0, 15, 0.001)
fs.set_style(textwidth=345)

#This work
plt.scatter(EJ305_E, EJ305_LY,  s=35, lw=0.5, edgecolors='none', facecolors=EJ305_color, marker='v', alpha=1, label='EJ 305, SMD', zorder=4)
plt.errorbar(EJ305_E, EJ305_LY, yerr=EJ305_LY_err, linestyle='none', color=EJ305_color, lw=0.5, alpha=1)

#NE224 low
plt.scatter(NE224_E_low, NE224_LY_low, color='black', marker='d', lw=0.5, s=35, facecolor='white', label='NE 224, Czirr et.al', zorder=3)
plt.legend(loc=1)

#NE224 high
plt.scatter(NE224_E_high, NE224_LY_high, color='black', marker='s', lw=0.5, s=35, facecolor='white', label='NE 224, Madey et.al', zorder=3)
plt.errorbar(NE224_E_high, NE224_LY_high, yerr=NE224_LY_high_err, xerr=NE224_E_high_err, linestyle='none', color='black', lw=0.5, alpha=1)

# plt.plot(Tn, promath.cecilEq_NE224(Tn, 1), color='black', ls='dotted', lw=1.0, label='Cecil parameterization, Czirr et.al', zorder=1)
plt.plot(BC505_E, BC505_LY, color='black', ls='solid', lw=0.5, label='Light response BC 505, Pywell et.al', zorder=1)
plt.plot(BC505_E, BC505_LY*pywellScale, color='black', ls='-.', lw=0.5, label='Light response BC 505, Pywell et.al (x0.76)', zorder=1)

plt.xlim([0.95, 16.8])
plt.ylim([0.13, 15.0])

plt.ylabel('Scintillation Light Yield [MeV$_{ee}$]')
plt.xlabel('Proton Energy [MeV]')
plt.legend(loc=2)
plt.grid('both')
# plt.yscale('log')
# plt.xscale('log')
# plt.xlim([0.8, 16])

# plt.savefig(f'/home/gheed/Documents/writing/mauritzson_lightYield_paper_2022/figures/EJ305_comparison.pdf')
plt.show()

# ##########################################################
# ######### Figure 14 EJ331 ################################
# ######### HH, TP, FD & Sim with fits #####################
# ##########################################################

detector = 'EJ331'
#load data
HH_data_LG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/HH_data_LG.pkl')
TP_data_LG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/TP_data_LG.pkl')
FD_data_LG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/FD_data_LG.pkl')
HH_data_SG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/HH_data_SG.pkl')
TP_data_SG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/TP_data_SG.pkl')
FD_data_SG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/FD_data_SG.pkl')
LY_sim = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/simulation/sim_LightYield.pkl')
kornilov_LG = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/HH_TP_FD/kornilov_LG_result.pkl')
kornilov_SG = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/HH_TP_FD/kornilov_SG_result.pkl')
cecil_LG = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/HH_TP_FD/cecil_LG_result.pkl')
cecil_SG = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/HH_TP_FD/cecil_SG_result.pkl')
kornilov_SMD = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/simulation/kornilov_result.pkl')
cecil_SMD = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/simulation/cecil_result.pkl')

mainColor = EJ331_color
L1 = 5.34#7.666969
p = [0.817, 2.63, 0.297, 1] #EJ309

#make figure
fs.set_style(textwidth=345)
titleParamters = dict(ha='right', va='center', fontsize=10, color='black')

plt.scatter(TP_data_LG.energy, TP_data_LG.TP_loc, s=40, lw=0.5, color='black', marker='o', label='Measured data (TP)', facecolor='white', alpha=1, zorder=2)
plt.errorbar(TP_data_LG.energy, TP_data_LG.TP_loc, yerr=TP_data_LG.TP_loc_err, linestyle='none', color='black', lw=0.5, alpha=1, zorder=1)
# plt.plot(np.arange(1.75, 6.25, 0.01), promath.cecilEq(np.arange(1.75, 6.25, 0.01), cecil_LG.query('method=="TP_data"').C.item(), p[0], p[1], p[2], p[3]), lw=.5, color='black', alpha=.9, ls='dashed', label='Cecil Eq.[4]')
plt.plot(np.arange(1.75, 6.25, 0.01), promath.kornilovEq(np.arange(1.75, 6.25, 0.01), kornilov_LG.query('method=="TP_data"').L0.item(), L1), lw=.5, color='black', alpha=.9, label='Kornilov Eq.[5]')

plt.scatter(LY_sim.energy,  LY_sim.means,  s=35, lw=0.5, edgecolors='none', facecolors=mainColor, marker='v', alpha=1, zorder=4, label='SMD')
plt.errorbar(LY_sim.energy, LY_sim.means, yerr=LY_sim.means_err, linestyle='none', color=mainColor, lw=0.5, alpha=1, zorder=3)
# plt.plot(np.arange(1.75, 6.25, 0.01), promath.cecilEq(np.arange(1.75, 6.25, 0.01), cecil_SMD.C.item(), p[0], p[1], p[2], p[3]), lw=.5, color=mainColor, alpha=.9, ls='dashed', label='Cecil Eq.[4]')
plt.plot(np.arange(1.75, 6.25, 0.01), promath.kornilovEq(np.arange(1.75, 6.25, 0.01), kornilov_SMD.L0.item(), L1), lw=.5, color=mainColor, alpha=.9, ls='dashed', label='Kornilov Eq.[5]')

plt.text(4.50, np.max(TP_data_LG.TP_loc*1.075), f'EJ 331', **titleParamters)

plt.xlim([1.75, 6.25])
plt.ylim([np.min(TP_data_LG.TP_loc)*0.29, np.max(TP_data_LG.TP_loc)*1.15])
plt.xlabel('Proton Energy [MeV]')
plt.ylabel('Scintillation Light Yield [MeV$_{ee}$]')
plt.legend()
plt.savefig(f'/home/gheed/Documents/writing/mauritzson_lightYield_paper_2022/figures/{detector}_result.pdf')

plt.show()



# ##########################################################
# ######### Figure 15 EJ331 ################################
# ######### HH, TP, FD & Sim with fits #####################
# ##########################################################

detector = 'EJ321P'
#load data
HH_data_LG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/HH_data_LG.pkl')
TP_data_LG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/TP_data_LG.pkl')
FD_data_LG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/FD_data_LG.pkl')
HH_data_SG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/HH_data_SG.pkl')
TP_data_SG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/TP_data_SG.pkl')
FD_data_SG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/FD_data_SG.pkl')
LY_sim = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/simulation/sim_LightYield.pkl')
kornilov_LG = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/HH_TP_FD/kornilov_LG_result.pkl')
kornilov_SG = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/HH_TP_FD/kornilov_SG_result.pkl')
cecil_LG = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/HH_TP_FD/cecil_LG_result.pkl')
cecil_SG = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/HH_TP_FD/cecil_SG_result.pkl')
kornilov_SMD = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/simulation/kornilov_result.pkl')
cecil_SMD = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/simulation/cecil_result.pkl')

mainColor = EJ321P_color
L1 = 6.68#6.617600
p = [0.43, 0.77, 0.26, 2.13]

#make figure
fs.set_style(textwidth=345)
titleParamters = dict(ha='right', va='center', fontsize=10, color='black')

plt.scatter(TP_data_LG.energy, TP_data_LG.TP_loc, s=40, lw=0.5, color='black', marker='o', label='Measured data (TP)', facecolor='white', alpha=1, zorder=2)
plt.errorbar(TP_data_LG.energy, TP_data_LG.TP_loc, yerr=TP_data_LG.TP_loc_err, linestyle='none', color='black', lw=0.5, alpha=1, zorder=1)
# plt.plot(np.arange(1.75, 6.25, 0.01), promath.cecilEq(np.arange(1.75, 6.25, 0.01), cecil_LG.query('method=="TP_data"').C.item(), p[0], p[1], p[2], p[3]), lw=.5, color='black', alpha=.9, ls='dashed', label='Cecil Eq.[4]')
plt.plot(np.arange(1.75, 6.25, 0.01), promath.kornilovEq(np.arange(1.75, 6.25, 0.01), kornilov_LG.query('method=="TP_data"').L0.item(), L1), lw=.5, color='black', alpha=.9, label='Kornilov Eq.[5]')

plt.scatter(LY_sim.energy,  LY_sim.means,  s=35, lw=0.5, edgecolors='none', facecolors=mainColor, marker='v', alpha=1, zorder=4, label='SMD')
plt.errorbar(LY_sim.energy, LY_sim.means, yerr=LY_sim.means_err, linestyle='none', color=mainColor, lw=0.5, alpha=1, zorder=3)
# plt.plot(np.arange(1.75, 6.25, 0.01), promath.cecilEq(np.arange(1.75, 6.25, 0.01), cecil_SMD.C.item(), p[0], p[1], p[2], p[3]), lw=.5, color=mainColor, alpha=.9, ls='dashed', label='Cecil Eq.[4]')
plt.plot(np.arange(1.75, 6.25, 0.01), promath.kornilovEq(np.arange(1.75, 6.25, 0.01), kornilov_SMD.L0.item(), L1), lw=.5, color=mainColor, alpha=.9, ls='dashed', label='Kornilov Eq.[5]')

plt.text(4.50, np.max(TP_data_LG.TP_loc*1.075), f'EJ 321P', **titleParamters)

plt.xlim([1.75, 6.25])
plt.ylim([np.min(TP_data_LG.TP_loc)*0.29, np.max(TP_data_LG.TP_loc)*1.15])
plt.xlabel('Proton Energy [MeV]')
plt.ylabel('Scintillation Light Yield [MeV$_{ee}$]')
plt.legend()
plt.savefig(f'/home/gheed/Documents/writing/mauritzson_lightYield_paper_2022/figures/{detector}_result.pdf')

plt.show()






























# ##########################################################
# ######### Figure X #######################################
# ######### Simulations optimal kB ###############
# ##########################################################

#Load simulations results
detector = 'NE213A'
density = 0.874 #g/cm3 NE213A

data_2000 = pd.read_csv(f'{pathData}/{detector}/birks/{2000}keV.csv')
data_2500 = pd.read_csv(f'{pathData}/{detector}/birks/{2500}keV.csv')
data_3000 = pd.read_csv(f'{pathData}/{detector}/birks/{3000}keV.csv')
data_3500 = pd.read_csv(f'{pathData}/{detector}/birks/{3500}keV.csv')
data_4000 = pd.read_csv(f'{pathData}/{detector}/birks/{4000}keV.csv')
data_4500 = pd.read_csv(f'{pathData}/{detector}/birks/{4500}keV.csv')
data_5000 = pd.read_csv(f'{pathData}/{detector}/birks/{5000}keV.csv')
data_5500 = pd.read_csv(f'{pathData}/{detector}/birks/{5500}keV.csv')
data_6000 = pd.read_csv(f'{pathData}/{detector}/birks/{6000}keV.csv')
poptReciprocal_kB = np.load(f'{pathData}/{detector}/birks/kB_reciprocal_popt.npy')
pcovReciprocal_kB = np.sqrt(np.diag(np.load(f'{pathData}/{detector}/birks/kB_reciprocal_pcov.npy')))
optimalkB_err = np.load(f'{pathData}/{detector}/birks/kB_error.npy')
poptReciprocal_smear = np.load(f'{pathData}/{detector}/birks/smear_reciprocal_popt.npy')
pcovReciprocal_smear = np.sqrt(np.diag(np.load(f'{pathData}/{detector}/birks/smear_reciprocal_pcov.npy')))
optimalSmear_err = np.load(f'{pathData}/{detector}/birks/smear_error.npy')
sim_total_error = np.load(f'{pathData}/{detector}/birks/total_error.npy')


#Define variables
Tn = np.array([2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5, 6.0])
Tn_fine = np.arange(1.75, 6.26, 0.01)

optimalkB = np.array([data_2000.optimal[0], 
                        data_2500.optimal[0], 
                        data_3000.optimal[0], 
                        data_3500.optimal[0], 
                        data_4000.optimal[0], 
                        data_4500.optimal[0], 
                        data_5000.optimal[0], 
                        data_5500.optimal[0], 
                        data_6000.optimal[0]])*(density/10)

optimalSmear = np.array([data_2000.smear.mean(), 
                        data_2500.smear.mean(), 
                        data_3000.smear.mean(), 
                        data_3500.smear.mean(), 
                        data_4000.smear.mean(), 
                        data_4500.smear.mean(), 
                        data_5000.smear.mean(), 
                        data_5500.smear.mean(), 
                        data_6000.smear.mean()])


#Make plot
fs.set_style(textwidth=345)
fig, ax1 = plt.subplots()

# Instantiate a second axes that shares the same x-axis
ax2 = ax1.twinx()  

#kB
ax1.scatter(Tn, optimalkB, color='black', marker='o', lw=0.5, s=25, facecolor='white', label='Optimal $k_B$', zorder=4)
ax1.errorbar(Tn, optimalkB, yerr=optimalkB*optimalkB_err, color='black', linestyle='none', lw=0.5, zorder=3)

ax1.plot(Tn_fine, promath.reciprocalConstantFunc(Tn_fine, poptReciprocal_kB[0], poptReciprocal_kB[1])*(density/10), lw=0.5, color='black', linestyle='solid', zorder=2)
ax1.fill_between(       Tn_fine, 
                        promath.reciprocalConstantFunc(Tn_fine, poptReciprocal_kB[0]-pcovReciprocal_kB[0], poptReciprocal_kB[1]-pcovReciprocal_kB[1])*(density/10),#-0.000307836226262341, 
                        promath.reciprocalConstantFunc(Tn_fine, poptReciprocal_kB[0]+pcovReciprocal_kB[0], poptReciprocal_kB[1]+pcovReciprocal_kB[1])*(density/10),#+0.000307836226262341, #0.02895527565858691
                        color='black', alpha=0.1, zorder=1)

#smear
ax2.scatter(Tn, optimalSmear*100, color='black', marker='v', lw=0.5, s=25,  facecolor='white', label='Optimal smearing', zorder=4)
ax2.errorbar(Tn, optimalSmear*100, yerr=optimalSmear*optimalSmear_err*100, color='black', linestyle='none', lw=0.5, zorder=3)


ax2.plot(Tn_fine, promath.reciprocalConstantFunc(Tn_fine, poptReciprocal_smear[0], poptReciprocal_smear[1])*100, lw=0.5, color='black', linestyle='solid', zorder=2)
ax2.fill_between(       Tn_fine, 
                        promath.reciprocalConstantFunc(Tn_fine, poptReciprocal_smear[0]-pcovReciprocal_smear[0], poptReciprocal_smear[1]-pcovReciprocal_smear[1])*100,#-0.02895527565858691, 
                        promath.reciprocalConstantFunc(Tn_fine, poptReciprocal_smear[0]+pcovReciprocal_smear[0], poptReciprocal_smear[1]+pcovReciprocal_smear[1])*100,#+0.02895527565858691,
                        color='black', alpha=0.1, zorder=1)

ax1.set_ylim([0.007, 0.016])#kB
ax2.set_ylim([-25, 37])#smear
ax1.set_xlim([np.min(Tn_fine)-0.125, np.max(Tn_fine)+0.125])
ax1.set_xlabel('Neutron Energy [MeV]')
ax1.set_ylabel('$k_B$ [g/cm$^2$MeV]')
ax2.set_ylabel('Smearing [\%]')

ax1.legend(loc=3)
ax2.legend(loc=1)
plt.tight_layout()

plt.savefig(f'/home/gheed/Documents/writing/mauritzson_lightYield_paper_2022/figures/optimal_kB_smear.pdf')

plt.show()



# ##########################################################
# ######### Figure X #######################################
# ######### EJ305 light yield panels 2-6 MeV ###############
# ##########################################################
#Load data
detector = 'EJ305'
gate = 'LG'

#define energies to plot
Tn = np.arange(2000, 7000, 1000)

#load fitted parameters for reciprocal smear function
poptReciprocal_smear = np.load(f'{pathData}/{detector}/birks/smear_reciprocal_popt.npy')
pcovReciprocal_smear = np.load(f'{pathData}/{detector}/birks/smear_reciprocal_pcov.npy')

#create smear values to use for each energy based on function
smearFactorList = promath.reciprocalConstantFunc(Tn/1000, poptReciprocal_smear[0], poptReciprocal_smear[1])

#number of bins to use for each energy
numBinsList = np.array([np.arange(0, 0.85, 0.045),
                        np.arange(0, 1.30, 0.045),
                        np.arange(0, 1.80, 0.045),
                        np.arange(0, 2.40, 0.045),
                        np.arange(0, 3.00, 0.045)], dtype=object)
                        
#ranges over which to optmizise scaling between simulation and data
start = [0.23, 0.43, 0.78, 1.37, 1.71] #For EJ 305
stop =  [0.77,  1.3,  2.0,  2.3,  3.4] #For EJ 305

names = [ 'evtNum', 
        'optPhotonSum', 
        'optPhotonSumQE', 
        'optPhotonSumCompton', 
        'optPhotonSumComptonQE', 
        'optPhotonSumNPQE', 
        'xLoc', 
        'yLoc', 
        'zLoc', 
        'CsMin', 
        'optPhotonParentID', 
        'optPhotonParentCreatorProcess', 
        'optPhotonParentStartingEnergy',
        'total_edep',
        'gamma_edep',
        'proton_edep',
        'nScatters',
        'nScattersProton']


fs.set_style(textwidth=345)
titleParamters = dict(ha='left', va='center', fontsize=10, color='black')

for i, energy in enumerate(Tn):
        #import simulations
        simCurrent = pd.read_csv(f"{pathSim}/{detector}/neutrons/neutron_range/{int(energy)}keV/isotropic/CSV_optphoton_data_sum.csv", names=names)

        #randomize binning
        y, x = np.histogram(simCurrent.optPhotonSumQE, bins=4096, range=[0, 4096]) 
        simCurrent.optPhotonSumQE = prodata.getRandDist(x, y)

        #calibrate simulation
        simCurrent.optPhotonSumQE = prodata.calibrateMyDetector(f'{pathData}/{detector}/Simcal/popt.npy', simCurrent.optPhotonSumQE, inverse=False)

        #smear simulation
        simCurrent.optPhotonSumQE = simCurrent.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, smearFactorList[i])) 

        #binning simulations and applying gain offset
        ySimCurrent, xSimCurrent = np.histogram(simCurrent.optPhotonSumQE, bins=numBinsList[i])
        xSimCurrent = prodata.getBinCenters(xSimCurrent)

        #import data
        nQDC       = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/nQDC_{gate}.npy')
        randQDC    = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/randQDC_{gate}.npy')
        nLength    = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/nLength.npy')
        randLength = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/randLength.npy')
        nQDC = prodata.calibrateMyDetector(f'{pathData}/{detector}/Ecal/popt_LG.npy', nQDC)
        randQDC = prodata.calibrateMyDetector(f'{pathData}/{detector}/Ecal/popt_LG.npy', randQDC)

        #random subtract data
        xDataCurrent, yDataCurrent = prodata.randomTOFSubtractionQDC(   nQDC,
                                                                        randQDC,
                                                                        nLength, 
                                                                        randLength,
                                                                        numBinsList[i], 
                                                                        plot=False)
        #normalize data and simulation to 1
        yDataCurrent = prodata.integralNorm(yDataCurrent, forcePositive=True)
        ySimCurrent = prodata.integralNorm(ySimCurrent, forcePositive=True)

        #fitting data to get range for Chi2
        poptCurrent = promath.gaussFit(xDataCurrent, yDataCurrent, start[i], stop[i], y_error=False, error=False)

        #########################################################################
        # THIS PART WILL SLICE DATA SETS TO BE USED FOR FINAL Chi2 CALCULATIONS #
        #########################################################################
        #slicing data and simulations for Chi2 calculations from mean value of fit to mean value +3 sigma of fit.
        xDataCurrentChi2, yDataCurrentChi2 = prodata.binDataSlicer(xDataCurrent, yDataCurrent, poptCurrent[1]*1, poptCurrent[1]+3*poptCurrent[2])
        xSimCurrentChi2, ySimCurrentChi2 = prodata.binDataSlicer(xSimCurrent, ySimCurrent, poptCurrent[1]*1, poptCurrent[1]+3*poptCurrent[2])
        ###############################################################################################

        ###############################################################################################
        # THIS PART WILL SLICE DATA SETS TO BE USED FOR CALCULATING SCALING PRIOR TO Chi2 CALCULATION #
        ###############################################################################################
        #slicing data and simulation for determining optimal scaling
        xDataCurrentChi2, yDataCurrentScale = prodata.binDataSlicer(xDataCurrent, yDataCurrent, poptCurrent[1]*1.1, poptCurrent[1]*1.3)
        xSimCurrentChi2, ySimCurrentScale = prodata.binDataSlicer(xSimCurrent, ySimCurrent, poptCurrent[1]*1.1, poptCurrent[1]*1.3)
        ###############################################################################################

        #Determine optimal scaling for minimal Chi2 value
        scaleCurrent = prodata.chi2Minimize(yDataCurrentScale, ySimCurrentScale, verbose=True)

        chi2Current = promath.chi2(yDataCurrentChi2, ySimCurrentChi2 * scaleCurrent, np.sqrt(yDataCurrentChi2))
        
        #make plot
        plt.subplot(5, 1, i+1)
        if i==2:
                plt.ylabel('Counts')
        if i==4:
                plt.xlabel('Scintillation Light Yield [MeV$_{ee}$]')       
        
        plt.text(2.6, np.max(yDataCurrent)*0.80, f'{np.int32(energy/1000)} MeV', **titleParamters)

        plt.scatter(xDataCurrent, yDataCurrent, s=2, color='black', zorder=3)
        plt.errorbar(xDataCurrent, yDataCurrent, yerr=yDataCurrent*0.065, linestyle='none')

        plt.step(xSimCurrent, ySimCurrent * scaleCurrent, color='black', ls='solid', lw=0.5)
        # plt.fill_between(xSimCurrent, y1=0, y2=ySimCurrent * scaleCurrent, color=white, edgecolor='none', alpha=0.8, step='pre', zorder=1)

        plt.ylim([0, np.max(yDataCurrent)*1.35])
        plt.xlim(0.17, 3.1)
        plt.yticks([])
        
plt.subplots_adjust(hspace = .001, wspace = .001)
plt.savefig(f'/home/gheed/Documents/writing/mauritzson_lightYield_paper_2022/figures/EJ305_light_yield_panels.pdf')

plt.show()





# ##########################################################
# ######### Figure X #######################################
# ######### Light yield: Data vs Simulations ###############
# ##########################################################

pathSim = '/media/gheed/Seagate_Expansion_Drive1/data/simulation'
pathData = '/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper'

############
## NE213A ##
############
fs.set_style(textwidth=345*2)
normGain = promath.reciprocalConstantFunc(5, NE213A_poptReciprocal_kB[0], NE213A_poptReciprocal_kB[1])
# offsetsGainList = normGain/promath.reciprocalConstantFunc(Tn, NE213A_poptReciprocal_kB[0], NE213A_poptReciprocal_kB[1])
offsetsGainList = np.ones(9)
#Calculating bootstraping values for smearing
offsetsSmearList = promath.reciprocalConstantFunc(Tn, NE213A_poptReciprocal_smear[0], NE213A_poptReciprocal_smear[1])
# offsetsSmearList = np.array([0.05, 0.05, 0.05, 0.05, 0.05, 0.05, 0.05, 0.05, 0.05]) #at 5 MeV

numBinsList = np.array([np.arange(0, 4, 0.030),
                        np.arange(0, 4, 0.035),
                        np.arange(0, 4, 0.040),
                        np.arange(0, 4, 0.045),
                        np.arange(0, 4, 0.050),
                        np.arange(0, 4, 0.055),
                        np.arange(0, 4, 0.060),
                        np.arange(0, 4, 0.065),
                        np.arange(0, 4, 0.070)])

detector = 'NE213A'
gate = 'LG'
lightYieldFactor = 1
start = [0.24, 0.43, 0.67, 0.81, 0.92, 1.34, 1.58, 1.90, 2.22]
stop =  [1.0,  1.1,  1.7,  1.9,  2.1,  2.5,  2.8,  3.2,  4.0]

NE213A_chi2 = prodata.scintPhotonVsEnergy(pathData, pathSim, detector, gate, numBinsList, offsetsSmearList, offsetsGainList, start, stop, lightYieldFactor, plot=True)

###########
## EJ305 ##
###########
fs.set_style(textwidth=345*2)
# normGain = promath.reciprocalConstantFunc(5, EJ305_poptReciprocal_kB[0], EJ305_poptReciprocal_kB[1])
# offsetsGainList = normGain/promath.reciprocalConstantFunc(Tn, EJ305_poptReciprocal_kB[0], EJ305_poptReciprocal_kB[1])
offsetsGainList = np.ones(9)
#Calculating bootstraping values for smearing
offsetsSmearList = promath.reciprocalConstantFunc(Tn, EJ305_poptReciprocal_smear[0], EJ305_poptReciprocal_smear[1])
# offsetsSmearList = np.array([0.05, 0.05, 0.05, 0.05, 0.05, 0.05, 0.05, 0.05, 0.05]) #at 5 MeV

numBinsList = np.array([np.arange(-.1, 4, 0.018),
                        np.arange(-.1, 4, 0.025),
                        np.arange(-.1, 4, 0.030),
                        np.arange(-.1, 4, 0.035),
                        np.arange(-.1, 4, 0.040),
                        np.arange(-.1, 4, 0.045),
                        np.arange(-.1, 4, 0.050),
                        np.arange(-.1, 4, 0.055),
                        np.arange(-.1, 4, 0.060)])
detector = 'EJ305'
gate = 'LG'
lightYieldFactor = 1
start = [0.22, 0.25, 0.43, 0.61, 0.78, 1.12, 1.27, 1.53, 1.71]
stop =  [1.0,  1.2,  1.3,  1.8,  2.0,  2.1,  2.5,  2.7,  3.4]

EJ305_chi2 = prodata.scintPhotonVsEnergy(pathData, pathSim, detector, gate, numBinsList, offsetsSmearList, offsetsGainList, start, stop, lightYieldFactor, plot=True)



############
## EJ321P ##
############
fs.set_style(textwidth=345*2)
# normGain = promath.reciprocalConstantFunc(5, EJ321P_poptReciprocal_kB[0], EJ321P_poptReciprocal_kB[1])
# offsetsGainList = normGain/promath.reciprocalConstantFunc(Tn, EJ321P_poptReciprocal_kB[0], EJ321P_poptReciprocal_kB[1])
offsetsGainList = np.ones(9)
offsetsSmearList = promath.reciprocalConstantFunc(Tn, EJ321P_poptReciprocal_smear[0], EJ321P_poptReciprocal_smear[1])
# offsetsSmearList = np.array([0.14, 0.14, 0.14, 0.14, 0.14, 0.14, 0.14, 0.14, 0.14, 0.14]) #at 5 MeV

numBinsList = np.array([np.arange(0, 4, 0.030),
                        np.arange(0, 4, 0.035),
                        np.arange(0, 4, 0.040),
                        np.arange(0, 4, 0.045),
                        np.arange(0, 4, 0.050),
                        np.arange(0, 4, 0.055),
                        np.arange(0, 4, 0.060),
                        np.arange(0, 4, 0.065),
                        np.arange(0, 4, 0.070)])
detector = 'EJ321P'
gate = 'LG'
lightYieldFactor = 1
start = [0.25, 0.31, 0.34, 0.43, 0.48, 0.67, 0.87, 1.13, 1.31]
stop =  [0.7,  0.9,  1.1,  1.4,  1.5,  1.9,  2.2,  2.6,  2.9]

EJ321P_chi2 = prodata.scintPhotonVsEnergy(pathData, pathSim, detector, gate, numBinsList, offsetsSmearList, offsetsGainList, start, stop, lightYieldFactor, plot=True)


###########
## EJ331 ##
###########
fs.set_style(textwidth=345*2)
# normGain = promath.reciprocalConstantFunc(5, EJ331_poptReciprocal_kB[0], EJ331_poptReciprocal_kB[1])
# offsetsGainList = normGain/promath.reciprocalConstantFunc(Tn, EJ331_poptReciprocal_kB[0], EJ331_poptReciprocal_kB[1])
offsetsGainList = np.ones(9)
offsetsSmearList = promath.reciprocalConstantFunc(Tn, EJ331_poptReciprocal_smear[0], EJ331_poptReciprocal_smear[1])
# offsetsSmearList = np.array([0.15, 0.15, 0.15, 0.15, 0.15, 0.15, 0.15, 0.15, 0.15, 0.15]) #at 5 MeV

numBinsList = np.array([np.arange(0, 4, 0.030),
                        np.arange(0, 4, 0.035),
                        np.arange(0, 4, 0.040),
                        np.arange(0, 4, 0.045),
                        np.arange(0, 4, 0.050),
                        np.arange(0, 4, 0.055),
                        np.arange(0, 4, 0.060),
                        np.arange(0, 4, 0.065),
                        np.arange(0, 4, 0.070)])
detector = 'EJ331'
gate = 'LG' 
lightYieldFactor = 1
start = [0.29, 0.36, 0.44, 0.49, 0.79, 1.02, 1.22, 1.40, 1.66]
stop =  [1.0,  1.0,  1.5,  1.9,  2.1,  2.3,  2.6,  3.1,  3.5]

EJ331_chi2 = prodata.scintPhotonVsEnergy(pathData, pathSim, detector, gate, numBinsList, offsetsSmearList, offsetsGainList, start, stop, lightYieldFactor, plot=True)


















#-------------------------------- "OLD LINE" --------------------------------------------------------

############################################
######## Pywell, BC-505 / EJ305 DATA #######
# Data taken from https://doi.org/10.1016/j.nima.2006.05.087 using "Plot stealer": https://apps.automeris.io/wpd/
############################################

BC505_E = np.array([1.9897699967058662, 2.5103967309258537, 2.998300388626953, 3.5027919299784362, 4.006378089766157, 4.510190594944817, 4.997415216473094, 5.500775030869873, 6.004134845266654, 6.5072683142724905])
BC505_LY = np.array([0.5410422801064634, 0.7934578098254121, 1.046156271283035, 1.2789484639838022, 1.5907998682346491, 1.8828864695979757, 2.1948793397181596, 2.5264955468565287, 2.8581117539948977, 3.2094927640207853])



############################################
######## Julius, NE213 DATA ################
# Data taken from https://doi.org/10.1016/j.nima.2016.10.011 using "Plot stealer": https://apps.automeris.io/wpd/
############################################

NE213_E = np.array([2, 2.2, 2.4, 2.6, 2.8, 3.0, 3.2, 3.4, 3.6, 3.8, 4.0, 4.2, 4.4, 4.6, 4.8, 5.0, 5.2, 5.4, 5.6, 5.8])
NE213_HH_LG_LY = np.array([0.655151539173894, 0.7150465373541994, 0.7918484860496036, 0.9024066820650116, 0.987647692590417, 1.115084012265827, 1.2171887330236846, 1.2855516198890884, 1.4298516497969507, 1.607907927024816, 1.7015879993802225, 1.769936472818077, 1.9142365027259391, 2.092321606808903, 2.2197435130567635, 2.338740770902172, 2.483040800810034, 2.517647440355435, 2.7041571928408503, 2.8568962845787134])
NE213_TP_LG_LY = np.array([0.6886097574637304, 0.7444127037472685, 0.7752618607516935, 0.8977202711028949, 0.9535652271326942, 1.1093093597714665, 1.2068139808435552, 1.2876127261524668, 1.4267770122668453, 1.5576093653727658, 1.7217274407662573, 1.7359127317537661, 1.9083627401557155, 2.039195093261636, 2.153405590096902, 2.309149722735674, 2.398280401053044, 2.570772419201254, 2.643281241247969, 2.8323951156668343])
NE213_FD_LG_LY = np.array([0.5813888888888892, 0.6847222222222222, 0.7632638888888894, 0.8334027777777782, 0.9450000000000003, 1.0152083333333337, 1.085555555555556, 1.1886805555555555, 1.4159722222222224, 1.4696527777777784, 1.54, 1.6928472222222224, 1.8126388888888891, 1.9242361111111113, 2.0605555555555557, 2.188541666666667, 2.325, 2.37875, 2.5233333333333334, 2.701041666666667])

NE213_HH_SG_LY = np.array([0.4948093644038787, 0.571596899671734, 0.6146281876195858, 0.7251863836349943, 0.7935348570728489, 0.8703368057682539, 1.0483930829961192, 1.116755969861523, 1.2273141658769307, 1.3294188866347887, 1.4146454837326452, 1.4998864942580505, 1.6273228139334603, 1.6788075771388624, 1.7809122978967202, 1.8745923702521268, 1.9091845963699778, 1.9606693595753804, 2.1471791120607957, 2.384308822098667])
NE213_TP_SG_LY = np.array([0.49693328852293694,0.5278244552736227, 0.6002912675740766, 0.6560942138576138, 0.745224892174984, 0.8677253122724466, 1.0234694449112185, 1.0709824679325597, 1.2101047443006774, 1.2409539013051019, 1.346748445639388, 1.4276312104408218, 1.5250938217666492, 1.5809387777964483, 1.686775331876995, 1.7758640004481034, 1.8067551671987896, 1.8542261804738693, 2.0516719879011927, 2.2991514031255247])
NE213_FD_SG_LY = np.array([0.5070138888888889, 0.5525000000000002, 0.6227777777777779, 0.6515972222222222, 0.7962500000000001, 0.8664583333333331, 0.9532638888888889, 1.0068055555555562, 1.1350000000000002, 1.2713888888888891, 1.325138888888889, 1.3870138888888892, 1.523402777777778, 1.618541666666667, 1.680416666666667, 1.8580555555555558, 1.8292361111111115, 1.9820833333333334, 2.0687500000000005, 2.3125694444444447])

############################################
######## Mauritzson vs Julius figure #######
############################################

detector = 'NE213A'
plt.suptitle('Mauritzson vs. Scherzinger')
plt.subplot(3,1,1)
NE213A_HH_data_LG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/HH_data_LG.pkl')[2:-1]
NE213A_HH_data_SG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/HH_data_SG.pkl')[2:-1]
plt.scatter(NE213A_HH_data_LG.energy, NE213A_HH_data_LG.HH_loc, marker='o', color='black', s=60, label='NE213A LG')
plt.scatter(NE213A_HH_data_SG.energy, NE213A_HH_data_SG.HH_loc, marker='s', color='black', s=60, label='NE213A SG')
plt.scatter(NE213_E, NE213_HH_LG_LY, marker='o', color='black', facecolor='none', s=60, label='NE213 LG (Scherzinger et. al)')
plt.scatter(NE213_E, NE213_HH_SG_LY, marker='s', color='black', facecolor='none', s=60, label='NE213 SG (Scherzinger et. al)')
plt.legend()
plt.ylabel('LY [MeV$_{ee}$]')

plt.subplot(3,1,2)
NE213A_TP_data_LG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/TP_data_LG.pkl')[2:-1]
NE213A_TP_data_SG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/TP_data_SG.pkl')[2:-1]
plt.scatter(NE213A_TP_data_LG.energy, NE213A_TP_data_LG.TP_loc, marker='o', color='black', s=60, label='NE213A LG')
plt.scatter(NE213A_TP_data_SG.energy, NE213A_TP_data_SG.TP_loc, marker='s', color='black', s=60, label='NE213A SG')
plt.scatter(NE213_E, NE213_TP_LG_LY, marker='o', color='black', facecolor='none', s=60, label='NE213 LG (Scherzinger et. al)')
plt.scatter(NE213_E, NE213_TP_SG_LY, marker='s', color='black', facecolor='none', s=60, label='NE213 SG (Scherzinger et. al)')
plt.legend()
plt.ylabel('LY [MeV$_{ee}$]')

plt.subplot(3,1,3)
NE213A_FD_data_LG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/FD_data_LG.pkl')[2:-1]
NE213A_FD_data_SG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/FD_data_SG.pkl')[2:-1]
plt.scatter(NE213A_FD_data_LG.energy, NE213A_FD_data_LG.FD_loc, marker='o', color='black', s=60, label='NE213 LG')
plt.scatter(NE213A_FD_data_SG.energy, NE213A_FD_data_SG.FD_loc, marker='s', color='black', s=60, label='NE213 SG')
plt.scatter(NE213_E, NE213_FD_LG_LY, marker='o', color='black', facecolor='none', s=60, label='NE213 LG (Scherzinger et. al)')
plt.scatter(NE213_E, NE213_FD_SG_LY, marker='s', color='black', facecolor='none', s=60, label='NE213 SG (Scherzinger et. al)')
plt.legend()
plt.ylabel('LY [MeV$_{ee}$]')
plt.xlabel('T$_n$ [MeV]')

plt.show()

############################################
######## Mauritzson vs Pywell figure #######
############################################
detector = 'EJ305'
EJ305_HH_data = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/HH_TP_FD/HH_data_LG.pkl')[2:-1]
plt.scatter(EJ305_HH_data.energy, EJ305_HH_data.HH_loc, marker='o', color='black', s=60, label='EJ305 HH')
plt.scatter(BC505_E, BC505_LY, marker='s', color='black', facecolor='none', s=60, label='BC505 (Pywell et. al)')
plt.legend()
plt.show()


####################################################
######## HH comparison between all detectors #######
####################################################
NE213A_HH_data_LG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/NE213A/TOF_slice/HH_TP_FD/HH_data_LG.pkl')[2:-1]
EJ305_HH_data_LG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/EJ305/TOF_slice/HH_TP_FD/HH_data_LG.pkl')[2:-1]
EJ321P_HH_data_LG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/EJ321P/TOF_slice/HH_TP_FD/HH_data_LG.pkl')[2:-1]
EJ331_HH_data_LG = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/EJ331/TOF_slice/HH_TP_FD/HH_data_LG.pkl')[2:-1]

plt.scatter(NE213A_HH_data_LG.energy, NE213A_HH_data_LG.HH_loc, marker='o', color='royalblue', s=60, label='NE213A LG')
plt.scatter(EJ305_HH_data_LG.energy, EJ305_HH_data_LG.HH_loc, marker='o', color='orange', s=60, label='EJ305 HH')
plt.scatter(EJ321P_HH_data_LG.energy, EJ321P_HH_data_LG.HH_loc, marker='o', color='forestgreen', s=60, label='EJ321P HH')
plt.scatter(EJ331_HH_data_LG.energy, EJ331_HH_data_LG.HH_loc, marker='o', color='tomato', s=60, label='EJ331 HH')
plt.legend()
plt.show()

