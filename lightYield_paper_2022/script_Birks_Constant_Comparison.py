#!/usr/bin/env python3
import sys
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit

# IMPORT FROM LIBRARY DIRECTORY
sys.path.insert(0, "../library/")
sys.path.insert(0, "../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "sim_analysis/")
sys.path.insert(0, "data_analysis/")

import digidata
import nicholai_plot_helper as nplt
import processing_utility as prout
import processing_math as promath
import processing_data as prodata
import processing_pd as propd
import processing_simulation as prosim

"""
Script for making figures with optimal kB and smearing at each at each energy
"""

pathSim = '/media/gheed/Seagate_Expansion_Drive1/data/simulation'
pathData = '/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper'

plot = True 

Tn = np.array([2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5, 6])

############
## NE213A ##
############

#Calculating bootstraping values for gain
poptReciGain = np.array([0.12229021, 0.06596143])
normGain = promath.reciprocalConstantFunc(5, poptReciGain[0], poptReciGain[1])
offsetsGainList = normGain/promath.reciprocalConstantFunc(Tn, poptReciGain[0], poptReciGain[1])
# offsetsGainList = np.ones(9)
#Calculating bootstraping values for smearing
poptReciSmear = np.array([ 0.65651711, -0.05822879])
offsetsSmearList = promath.reciprocalConstantFunc(Tn, poptReciSmear[0], poptReciSmear[1])
# offsetsSmearList = np.array([0.05, 0.05, 0.05, 0.05, 0.05, 0.05, 0.05, 0.05, 0.05]) #at 5 MeV

numBinsList = np.array([np.arange(0, 4, 0.030),
                        np.arange(0, 4, 0.035),
                        np.arange(0, 4, 0.040),
                        np.arange(0, 4, 0.045),
                        np.arange(0, 4, 0.050),
                        np.arange(0, 4, 0.055),
                        np.arange(0, 4, 0.060),
                        np.arange(0, 4, 0.065),
                        np.arange(0, 4, 0.070)])

detector = 'NE213A'
gate = 'LG'
lightYieldFactor = 1 
start = [0.24, 0.43, 0.67, 0.81, 0.92, 1.34, 1.58, 1.90, 2.22]
stop =  [1.0,  1.1,  1.7,  1.9,  2.1,  2.5,  2.8,  3.2,  4.0]

NE213A = prodata.scintPhotonVsEnergy(pathData, pathSim, detector, gate, numBinsList, offsetsSmearList, offsetsGainList, start, stop, lightYieldFactor, plot=plot)

###########
## EJ305 ##
###########

#Calculating bootstraping values for gain
poptReciGain = np.array([0.02327588, 0.00702119])
normGain = promath.reciprocalConstantFunc(5, poptReciGain[0], poptReciGain[1])
offsetsGainList = normGain/promath.reciprocalConstantFunc(Tn, poptReciGain[0], poptReciGain[1])
# offsetsGainList = np.ones(9)
#Calculating bootstraping values for smearing
poptReciSmear = np.array([0.97869656, -0.12062769])
offsetsSmearList = promath.reciprocalConstantFunc(Tn, poptReciSmear[0], poptReciSmear[1])
# offsetsSmearList = np.array([0.05, 0.05, 0.05, 0.05, 0.05, 0.05, 0.05, 0.05, 0.05]) #at 5 MeV

numBinsList = np.array([np.arange(0, 4, 0.030),
                        np.arange(0, 4, 0.035),
                        np.arange(0, 4, 0.040),
                        np.arange(0, 4, 0.045),
                        np.arange(0, 4, 0.050),
                        np.arange(0, 4, 0.055),
                        np.arange(0, 4, 0.060),
                        np.arange(0, 4, 0.065),
                        np.arange(0, 4, 0.070)])
detector = 'EJ305'
gate = 'LG'
lightYieldFactor = 1
start = [0.11, 0.25, 0.43, 0.61, 0.78, 1.12, 1.49, 1.79, 2.01]
stop =  [1.0,  1.2,  1.3,  1.8,  2.0,  2.1,  2.5,  2.7,  3.4]

EJ305 = prodata.scintPhotonVsEnergy(pathData, pathSim, detector, gate, numBinsList, offsetsSmearList, offsetsGainList, start, stop, lightYieldFactor, plot=True)



############
## EJ321P ##
############
#Calculating bootstraping values for gain
poptReciGain = np.array([0.01540051, 0.01864399])
normGain = promath.reciprocalConstantFunc(5, poptReciGain[0], poptReciGain[1])
offsetsGainList = normGain/promath.reciprocalConstantFunc(Tn, poptReciGain[0], poptReciGain[1])
# offsetsGainList = np.ones(9)
#Calculating bootstraping values for smearing
poptReciSmear = np.array([0.45407024, 0.07030811])
offsetsSmearList = promath.reciprocalConstantFunc(Tn, poptReciSmear[0], poptReciSmear[1])
# offsetsSmearList = np.array([0.14, 0.14, 0.14, 0.14, 0.14, 0.14, 0.14, 0.14, 0.14, 0.14]) #at 5 MeV

numBinsList = np.array([np.arange(0, 4, 0.030),
                        np.arange(0, 4, 0.035),
                        np.arange(0, 4, 0.040),
                        np.arange(0, 4, 0.045),
                        np.arange(0, 4, 0.050),
                        np.arange(0, 4, 0.055),
                        np.arange(0, 4, 0.060),
                        np.arange(0, 4, 0.065),
                        np.arange(0, 4, 0.070)])
detector = 'EJ321P'
gate = 'LG'
lightYieldFactor = 1/3
start = [0.25, 0.31, 0.34, 0.43, 0.48, 0.67, 0.87, 1.13, 1.31]
stop =  [0.7,  0.9,  1.1,  1.4,  1.5,  1.9,  2.2,  2.6,  2.9]

EJ321P = prodata.scintPhotonVsEnergy(pathData, pathSim, detector, gate, numBinsList, offsetsSmearList, offsetsGainList, start, stop, lightYieldFactor, plot=plot)


###########
## EJ331 ##
###########
#Calculating bootstraping values for gain
poptReciGain = np.array([0.01061201, 0.01215378])
normGain = promath.reciprocalConstantFunc(5, poptReciGain[0], poptReciGain[1])
offsetsGainList = normGain/promath.reciprocalConstantFunc(Tn, poptReciGain[0], poptReciGain[1])
# offsetsGainList = np.ones(9)
#Calculating bootstraping values for smearing
poptReciSmear = np.array([0.47064116, 0.06107632])
offsetsSmearList = promath.reciprocalConstantFunc(Tn, poptReciSmear[0], poptReciSmear[1])
# offsetsSmearList = np.array([0.15, 0.15, 0.15, 0.15, 0.15, 0.15, 0.15, 0.15, 0.15, 0.15]) #at 5 MeV

numBinsList = np.array([np.arange(0, 4, 0.030),
                        np.arange(0, 4, 0.035),
                        np.arange(0, 4, 0.040),
                        np.arange(0, 4, 0.045),
                        np.arange(0, 4, 0.050),
                        np.arange(0, 4, 0.055),
                        np.arange(0, 4, 0.060),
                        np.arange(0, 4, 0.065),
                        np.arange(0, 4, 0.070)])
detector = 'EJ331'
gate = 'LG' 
lightYieldFactor = 1
start = [0.29, 0.36, 0.44, 0.49, 0.79, 1.02, 1.22, 1.40, 1.66]
stop =  [1.0,  1.0,  1.5,  1.9,  2.1,  2.3,  2.6,  3.1,  3.5]

EJ331 = prodata.scintPhotonVsEnergy(pathData, pathSim, detector, gate, numBinsList, offsetsSmearList, offsetsGainList, start, stop, lightYieldFactor, plot=plot)