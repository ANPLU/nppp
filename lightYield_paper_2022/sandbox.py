
#!/usr/bin/env python3
import sys
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit

# IMPORT FROM LIBRARY DIRECTORY
sys.path.insert(0, "../library/")
sys.path.insert(0, "../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "sim_analysis/")
sys.path.insert(0, "data_analysis/")

import digidata
import nicholai_plot_helper as nplt
import processing_utility as prout
import processing_math as promath
import processing_data as prodata
import processing_pd as propd
import processing_simulation as prosim





pathData = '/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper'
pathSim = '/media/gheed/Seagate_Expansion_Drive1/data/simulation'

energy =        '3500'
detector =      'NE213A'
smearFactor =   1
numBins =       np.arange(0.17, 4, 0.05)
optRangeBins =  np.arange(0.61, 1.5, 0.05)
birksFolder =   ['145', '126', '100',   '0917', '085', '060']
birksVal =      [0.145, 0.126,  0.100,  0.0917, 0.085,  0.060]
ampCoeff =      [3.50,  3.60,   3.50,   3.60,   3.50,   3.60]
gainCoeff =     [1.30,  1.18,   1.00,   0.94,   0.90,   0.77]
smearCoeff =    [0.10,  0.10,   0.11,   0.12,   0.13,   0.18]
lightYieldFactor = 1
df = prosim.birksOptimization( pathData, 
                        pathSim, 
                        energy, 
                        detector, 
                        smearFactor, 
                        numBins, 
                        birksFolder, 
                        birksVal, 
                        ampCoeff, 
                        gainCoeff, 
                        smearCoeff, 
                        optRangeBins, 
                        lightYieldFactor=1, 
                        plotTest=False, 
                        plotFinal=False,
                        optimizeOff=False)

energy =        '4000'
detector =      'NE213A'
smearFactor =   1
numBins =       np.arange(0.17, 4, 0.05)
optRangeBins =  np.arange(1.00, 2.1, 0.05)
birksFolder =   ['145', '126', '100',   '0917', '085', '060']
birksVal =      [0.145, 0.126,  0.100,  0.0917, 0.085,  0.060]
ampCoeff =      [3.50,  3.50,   3.30,   3.20,   3.20,   3.10]
gainCoeff =     [1.30,  1.20,   1.05,   0.99,   0.93,   0.78]
smearCoeff =    [0.12,  0.11,   0.11,   0.11,   0.12,   0.12]
lightYieldFactor = 1
df = prosim.birksOptimization( pathData, 
                        pathSim, 
                        energy, 
                        detector, 
                        smearFactor, 
                        numBins, 
                        birksFolder, 
                        birksVal, 
                        ampCoeff, 
                        gainCoeff, 
                        smearCoeff, 
                        optRangeBins, 
                        lightYieldFactor=1, 
                        plotTest=False, 
                        plotFinal=False,
                        optimizeOff=False)


energy = '4500'
detector = 'NE213A'
smearFactor = 1
numBins = np.arange(0.17, 4, 0.05)
birksFolder = ['145', '126', '100', '0917', '085', '060']
birksVal = [0.145, 0.126, 0.100, 0.0917, 0.085, 0.060]
ampCoeff = [3.6, 3.4, 3.4, 3.5, 3.6, 3.5]
gainCoeff = [1.29, 1.20, 1.05, 1.00, 0.95, 0.78]
smearCoeff = [0.06, 0.08, 0.08, 0.08, 0.09, 0.12]
optRangeBins = np.arange(1.40, 2.5, 0.05)
lightYieldFactor = 1
df = prosim.birksOptimization( pathData, 
                        pathSim, 
                        energy, 
                        detector, 
                        smearFactor, 
                        numBins, 
                        birksFolder, 
                        birksVal, 
                        ampCoeff, 
                        gainCoeff, 
                        smearCoeff, 
                        optRangeBins, 
                        lightYieldFactor=1, 
                        plotTest=False, 
                        plotFinal=False,
                        optimizeOff=False)


energy =        '5000'
detector =      'NE213A'
smearFactor =   1
numBins =       np.arange(0.17, 4, 0.06)
optRangeBins =  np.arange(1.50, 3.0, 0.06)
birksFolder =   ['145', '126', '100',   '0917', '085', '060']
birksVal =      [0.145, 0.126,  0.100,  0.0917, 0.085,  0.060]
ampCoeff =      [3.4,   0.65,   3.2,    3.1,    1.3,    3.2]
gainCoeff =     [1.30,  1.20,   1.05,   1.03,   0.97,   0.82]
smearCoeff =    [0.05,  0.05,   0.05,   0.05,   0.06,   0.10]
lightYieldFactor = 1
df = prosim.birksOptimization( pathData, 
                        pathSim, 
                        energy, 
                        detector, 
                        smearFactor, 
                        numBins, 
                        birksFolder, 
                        birksVal, 
                        ampCoeff, 
                        gainCoeff, 
                        smearCoeff, 
                        optRangeBins, 
                        lightYieldFactor=1, 
                        plotTest=False, 
                        plotFinal=False,
                        optimizeOff=False)


energy =        '5500'
detector =      'NE213A'
smearFactor =   1
numBins =       np.arange(0.17, 4, 0.06)
optRangeBins =  np.arange(1.50, 3.0, 0.06)
birksFolder =   ['145', '126', '100',   '0917', '085', '060']
birksVal =      [0.145, 0.126,  0.100,  0.0917, 0.085,  0.060]
ampCoeff =      [3.4,   0.65,   3.2,    3.1,    1.3,    3.2]
gainCoeff =     [1.30,  1.20,   1.05,   1.03,   0.97,   0.82]
smearCoeff =    [0.05,  0.05,   0.05,   0.05,   0.06,   0.10]
lightYieldFactor = 1
df = prosim.birksOptimization( pathData, 
                        pathSim, 
                        energy, 
                        detector, 
                        smearFactor, 
                        numBins, 
                        birksFolder, 
                        birksVal, 
                        ampCoeff, 
                        gainCoeff, 
                        smearCoeff, 
                        optRangeBins, 
                        lightYieldFactor=1, 
                        plotTest=False, 
                        plotFinal=False,
                        optimizeOff=False)


#Check EJ305 data and sim

gate = 'LG'
E = 2000
detector = 'EJ305'
pathSim = '/media/gheed/Seagate_Expansion_Drive1/data/simulation'
pathData = '/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper'
numBinsData = np.arange(-1,4,0.04)
lightYieldFactor = 1 
smearFactor = 0.07

names = [ 'evtNum', 
        'optPhotonSum', 
        'optPhotonSumQE', 
        'optPhotonSumCompton', 
        'optPhotonSumComptonQE', 
        'optPhotonSumNPQE', 
        'xLoc', 
        'yLoc', 
        'zLoc', 
        'CsMin', 
        'optPhotonParentID', 
        'optPhotonParentCreatorProcess', 
        'optPhotonParentStartingEnergy',
        'total_edep',
        'gamma_edep',
        'proton_edep',
        'nScatters',
        'nScattersProton']

#-> load data
nQDC       = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{E}/nQDC_{gate}.npy')
randQDC    = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{E}/randQDC_{gate}.npy')
nLength    = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{E}/nLength.npy')
randLength = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{E}/randLength.npy')

nQDC = prodata.calibrateMyDetector(f'{pathData}/{detector}/Ecal/popt_{gate}.npy', nQDC)
randQDC = prodata.calibrateMyDetector(f'{pathData}/{detector}/Ecal/popt_{gate}.npy', randQDC)

x_data, y_data = prodata.randomTOFSubtractionQDC(   nQDC,
                                                    randQDC,
                                                    nLength, 
                                                    randLength,
                                                    numBinsData, 
                                                    plot=False)
#->load sim
neutron_sim_iso = pd.read_csv(f"{pathSim}/{detector}/neutrons/neutron_range/{int(E)}keV/isotropic/CSV_optphoton_data_sum.csv", names=names)
neutron_sim_iso.optPhotonSumQE = neutron_sim_iso.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, smearFactor)) 

#Applying light yield factor to simulation.
neutron_sim_iso.optPhotonSumQE = neutron_sim_iso.optPhotonSumQE * lightYieldFactor

#calibrate simulations
neutron_sim_iso.optPhotonSumQE = prodata.calibrateMyDetector(f'{pathData}/{detector}/Simcal/popt.npy', neutron_sim_iso.optPhotonSumQE, inverse=False)

#bin simulations
y_sim, x_sim = np.histogram(neutron_sim_iso.optPhotonSumQE, bins=numBinsData)
x_sim = prodata.getBinCenters(x_sim)


plt.step(x_data, y_data)
plt.step(x_sim, y_sim)
plt.show()
