
#!/usr/bin/env python3
import sys
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit

# IMPORT FROM LIBRARY DIRECTORY
sys.path.insert(0, "../library/")
sys.path.insert(0, "../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "sim_analysis/")
sys.path.insert(0, "data_analysis/")

import digidata
import nicholai_plot_helper as nplt
import processing_utility as prout
import processing_math as promath
import processing_data as prodata
import processing_pd as propd
import processing_simulation as prosim





##########################################################

################## NE213A ################################
##### CALIBRATING SCINT PHOTONS TO QDC (bootstrap) #######

##########################################################

path_sim = '/media/gheed/Seagate_Expansion_Drive1/data/simulation/'
names = [ 'evtNum', 
        'optPhotonSum', 
        'optPhotonSumQE', 
        'optPhotonSumCompton', 
        'optPhotonSumComptonQE', 
        'optPhotonSumNPQE', 
        'xLoc', 
        'yLoc', 
        'zLoc', 
        'CsMin', 
        'optPhotonParentID', 
        'optPhotonParentCreatorProcess', 
        'optPhotonParentStartingEnergy',
        'total_edep',
        'gamma_edep',
        'proton_edep',
        'nScatters',
        'nScattersProton']

detector = 'NE213A'


NeutronSim_1500 = pd.read_csv(path_sim + f"{detector}/neutrons/neutron_range/{int(1500)}keV/isotropic/CSV_optphoton_data_sum.csv", names=names)
y, x = np.histogram(NeutronSim_1500.optPhotonSumQE, bins=4096, range=[0, 4096])
NeutronSim_1500.optPhotonSumQE = prodata.getRandDist(x, y)

NeutronSim_2000 = pd.read_csv(path_sim + f"{detector}/neutrons/neutron_range/{int(2000)}keV/isotropic/CSV_optphoton_data_sum.csv", names=names)
y, x = np.histogram(NeutronSim_2000.optPhotonSumQE, bins=4096, range=[0, 4096])
NeutronSim_2000.optPhotonSumQE = prodata.getRandDist(x, y)

NeutronSim_3000 = pd.read_csv(path_sim + f"{detector}/neutrons/neutron_range/{int(3000)}keV/isotropic/CSV_optphoton_data_sum.csv", names=names)
y, x = np.histogram(NeutronSim_3000.optPhotonSumQE, bins=4096, range=[0, 4096])
NeutronSim_3000.optPhotonSumQE = prodata.getRandDist(x, y)

NeutronSim_4000 = pd.read_csv(path_sim + f"{detector}/neutrons/neutron_range/{int(4000)}keV/isotropic/CSV_optphoton_data_sum.csv", names=names)
y, x = np.histogram(NeutronSim_4000.optPhotonSumQE, bins=4096, range=[0, 4096])
NeutronSim_4000.optPhotonSumQE = prodata.getRandDist(x, y)

NeutronSim_5000 = pd.read_csv(path_sim + f"{detector}/neutrons/neutron_range/{int(5000)}keV/isotropic/CSV_optphoton_data_sum.csv", names=names)
y, x = np.histogram(NeutronSim_5000.optPhotonSumQE, bins=4096, range=[0, 4096])
NeutronSim_5000.optPhotonSumQE = prodata.getRandDist(x, y)

NeutronSim_6000 = pd.read_csv(path_sim + f"{detector}/neutrons/neutron_range/{int(6000)}keV/isotropic/CSV_optphoton_data_sum.csv", names=names)
y, x = np.histogram(NeutronSim_6000.optPhotonSumQE, bins=4096, range=[0, 4096])
NeutronSim_6000.optPhotonSumQE = prodata.getRandDist(x, y)

#Calibrating simulation to MeVee
NeutronSim_1500.optPhotonSumQE = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Simcal', NeutronSim_1500.optPhotonSumQE, inverse=False)
NeutronSim_2000.optPhotonSumQE = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Simcal', NeutronSim_2000.optPhotonSumQE, inverse=False)
NeutronSim_3000.optPhotonSumQE = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Simcal', NeutronSim_3000.optPhotonSumQE, inverse=False)
NeutronSim_4000.optPhotonSumQE = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Simcal', NeutronSim_4000.optPhotonSumQE, inverse=False)
NeutronSim_5000.optPhotonSumQE = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Simcal', NeutronSim_5000.optPhotonSumQE, inverse=False)
NeutronSim_6000.optPhotonSumQE = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Simcal', NeutronSim_6000.optPhotonSumQE, inverse=False)

#smearing simulations
smearConstant = 0.05

NeutronSim_1500.optPhotonSumQE = NeutronSim_1500.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, smearConstant)) 
NeutronSim_2000.optPhotonSumQE = NeutronSim_2000.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, smearConstant)) 
NeutronSim_3000.optPhotonSumQE = NeutronSim_3000.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, smearConstant)) 
NeutronSim_4000.optPhotonSumQE = NeutronSim_4000.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, smearConstant)) 
NeutronSim_5000.optPhotonSumQE = NeutronSim_5000.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, smearConstant)) 
NeutronSim_6000.optPhotonSumQE = NeutronSim_6000.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, smearConstant)) 


#DATA
numBins = np.arange(0.0,10,0.1)

E = 1500
nQDC       = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nQDC.npy')
randQDC    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randQDC.npy')
nLength    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nLength.npy')
randLength = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randLength.npy')

nQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', nQDC)
randQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', randQDC)

x_1500, y_1500 = prodata.randomTOFSubtractionQDC(nQDC,
                                                 randQDC,
                                                 nLength, 
                                                 randLength,
                                                 np.arange(0,4,0.05), 
                                                 plot=False)

E = 2000
nQDC       = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nQDC.npy')
randQDC    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randQDC.npy')
nLength    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nLength.npy')
randLength = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randLength.npy')

nQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', nQDC)
randQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', randQDC)

x_2000, y_2000 = prodata.randomTOFSubtractionQDC(nQDC,
                                                 randQDC,
                                                 nLength, 
                                                 randLength,
                                                 np.arange(0,4,0.05), 
                                                 plot=False)

E = 3000
nQDC       = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nQDC.npy')
randQDC    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randQDC.npy')
nLength    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nLength.npy')
randLength = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randLength.npy')

nQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', nQDC)
randQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', randQDC)

x_3000, y_3000 = prodata.randomTOFSubtractionQDC(nQDC,
                                                 randQDC,
                                                 nLength, 
                                                 randLength,
                                                 np.arange(0,4,0.1), 
                                                 plot=False)


E = 4000
nQDC       = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nQDC.npy')
randQDC    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randQDC.npy')
nLength    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nLength.npy')
randLength = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randLength.npy')

nQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', nQDC)
randQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', randQDC)

x_4000, y_4000 = prodata.randomTOFSubtractionQDC(nQDC,
                                                 randQDC,
                                                 nLength, 
                                                 randLength,
                                                 np.arange(0,4,0.1), 
                                                 plot=False)

E = 5000
nQDC       = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nQDC.npy')
randQDC    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randQDC.npy')
nLength    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nLength.npy')
randLength = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randLength.npy')

nQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', nQDC)
randQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', randQDC)

x_5000, y_5000 = prodata.randomTOFSubtractionQDC(nQDC,
                                                 randQDC,
                                                 nLength, 
                                                 randLength,
                                                 np.arange(0,4,0.1), 
                                                 plot=False)

E = 6000
nQDC       = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nQDC.npy')
randQDC    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randQDC.npy')
nLength    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nLength.npy')
randLength = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randLength.npy')

nQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', nQDC)
randQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', randQDC)

x_6000, y_6000 = prodata.randomTOFSubtractionQDC(nQDC,
                                                 randQDC,
                                                 nLength, 
                                                 randLength,
                                                 np.arange(0,4,0.1), 
                                                 plot=False)

plt.figure()
plt.suptitle(f'{detector}, K$_B=0.0917$ mm/MeV')
plt.subplot(6,1,1)
plt.step(x_1500, y_1500*5.3, label='Data')
plt.hist(NeutronSim_1500.optPhotonSumQE, bins=np.arange(0.08,4,0.05), histtype='step', label='sim')
plt.legend()
plt.ylim([0, np.max(y_1500*5.3)*1.25])
plt.ylabel('counts')

plt.subplot(6,1,2)
plt.step(x_2000, y_2000*3.8, label='Data')
plt.hist(NeutronSim_2000.optPhotonSumQE, bins=np.arange(0.08,4,0.05), histtype='step', label='sim')
plt.legend()
plt.ylim([0, np.max(y_2000*3.8)*1.25])
plt.ylabel('counts')

plt.subplot(6,1,3)
plt.step(x_3000, y_3000*3.3, label='Data')
plt.hist(NeutronSim_3000.optPhotonSumQE, bins=np.arange(0.15,4,0.1), histtype='step', label='sim')
plt.legend()
plt.ylim([0, np.max(y_3000*3.3)*1.25])
plt.ylabel('counts')

plt.subplot(6,1,4)
plt.step(x_4000, y_4000*3.3, label='Data')
plt.hist(NeutronSim_4000.optPhotonSumQE, bins=np.arange(0.15,4,0.1), histtype='step', label='sim')
plt.legend()
plt.ylim([0, np.max(y_4000*3.3)*1.25])
plt.ylabel('counts')

plt.subplot(6,1,5)
plt.step(x_5000, y_5000*3.3, label='Data')
plt.hist(NeutronSim_5000.optPhotonSumQE, bins=np.arange(0.15,4,0.1), histtype='step', label='sim')
plt.legend()
plt.ylim([0, np.max(y_5000*3.3)*1.25])
plt.ylabel('counts')
plt.xlabel('Energy [MeVee]')

plt.subplot(6,1,6)
plt.step(x_6000, y_6000*8.3, label='Data')
plt.hist(NeutronSim_6000.optPhotonSumQE, bins=np.arange(0.15,4,0.1), histtype='step', label='sim')
plt.legend()
plt.ylim([0, np.max(y_6000*8.3)*1.25])
plt.ylabel('counts')
plt.xlabel('Energy [MeVee]')
plt.show()
























##########################################################

################## EJ305 #################################
##### CALIBRATING SCINT PHOTONS TO QDC (bootstrap) #######

##########################################################


path_sim = '/media/gheed/Seagate_Expansion_Drive1/data/simulation/'
names = [ 'evtNum', 
        'optPhotonSum', 
        'optPhotonSumQE', 
        'optPhotonSumCompton', 
        'optPhotonSumComptonQE', 
        'optPhotonSumNPQE', 
        'xLoc', 
        'yLoc', 
        'zLoc', 
        'CsMin', 
        'optPhotonParentID', 
        'optPhotonParentCreatorProcess', 
        'optPhotonParentStartingEnergy',
        'total_edep',
        'gamma_edep',
        'proton_edep',
        'nScatters',
        'nScattersProton']

detector = 'EJ305'

NeutronSim_1500 = pd.read_csv(path_sim + f"{detector}/neutrons/neutron_range/{int(1500)}keV/isotropic/CSV_optphoton_data_sum.csv", names=names)
y, x = np.histogram(NeutronSim_1500.optPhotonSumQE, bins=4096, range=[0, 4096])
NeutronSim_1500.optPhotonSumQE = prodata.getRandDist(x, y)

NeutronSim_2000 = pd.read_csv(path_sim + f"{detector}/neutrons/neutron_range/{int(2000)}keV/isotropic/CSV_optphoton_data_sum.csv", names=names)
y, x = np.histogram(NeutronSim_2000.optPhotonSumQE, bins=4096, range=[0, 4096])
NeutronSim_2000.optPhotonSumQE = prodata.getRandDist(x, y)

NeutronSim_3000 = pd.read_csv(path_sim + f"{detector}/neutrons/neutron_range/{int(3000)}keV/isotropic/CSV_optphoton_data_sum.csv", names=names)
y, x = np.histogram(NeutronSim_3000.optPhotonSumQE, bins=4096, range=[0, 4096])
NeutronSim_3000.optPhotonSumQE = prodata.getRandDist(x, y)

NeutronSim_4000 = pd.read_csv(path_sim + f"{detector}/neutrons/neutron_range/{int(4000)}keV/isotropic/CSV_optphoton_data_sum.csv", names=names)
y, x = np.histogram(NeutronSim_4000.optPhotonSumQE, bins=4096, range=[0, 4096])
NeutronSim_4000.optPhotonSumQE = prodata.getRandDist(x, y)

NeutronSim_5000 = pd.read_csv(path_sim + f"{detector}/neutrons/neutron_range/{int(5000)}keV/isotropic/CSV_optphoton_data_sum.csv", names=names)
y, x = np.histogram(NeutronSim_5000.optPhotonSumQE, bins=4096, range=[0, 4096])
NeutronSim_5000.optPhotonSumQE = prodata.getRandDist(x, y)

NeutronSim_6000 = pd.read_csv(path_sim + f"{detector}/neutrons/neutron_range/{int(6000)}keV/isotropic/CSV_optphoton_data_sum.csv", names=names)
y, x = np.histogram(NeutronSim_6000.optPhotonSumQE, bins=4096, range=[0, 4096])
NeutronSim_6000.optPhotonSumQE = prodata.getRandDist(x, y)

#Calibrating simulation to MeVee
NeutronSim_1500.optPhotonSumQE = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Simcal', NeutronSim_1500.optPhotonSumQE, inverse=False)
NeutronSim_2000.optPhotonSumQE = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Simcal', NeutronSim_2000.optPhotonSumQE, inverse=False)
NeutronSim_3000.optPhotonSumQE = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Simcal', NeutronSim_3000.optPhotonSumQE, inverse=False)
NeutronSim_4000.optPhotonSumQE = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Simcal', NeutronSim_4000.optPhotonSumQE, inverse=False)
NeutronSim_5000.optPhotonSumQE = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Simcal', NeutronSim_5000.optPhotonSumQE, inverse=False)
NeutronSim_6000.optPhotonSumQE = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Simcal', NeutronSim_6000.optPhotonSumQE, inverse=False)

#smearing simulations
smearConstant = 0.05

NeutronSim_1500.optPhotonSumQE = NeutronSim_1500.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, smearConstant)) 
NeutronSim_2000.optPhotonSumQE = NeutronSim_2000.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, smearConstant)) 
NeutronSim_3000.optPhotonSumQE = NeutronSim_3000.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, smearConstant)) 
NeutronSim_4000.optPhotonSumQE = NeutronSim_4000.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, smearConstant)) 
NeutronSim_5000.optPhotonSumQE = NeutronSim_5000.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, smearConstant)) 
NeutronSim_6000.optPhotonSumQE = NeutronSim_6000.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, smearConstant)) 

#DATA
# numBins = np.arange(0.0,10,0.1)
# poptQuad = np.array([1.59253101e-10, 1.56202964e-05, 7.32901514e-02]) #calibration constants for EJ305 (quadratic)

E = 1500
nQDC       = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nQDC.npy')
randQDC    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randQDC.npy')
nLength    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nLength.npy')
randLength = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randLength.npy')

nQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', nQDC)
randQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', randQDC)


x_1500, y_1500 = prodata.randomTOFSubtractionQDC(nQDC,
                                                 randQDC,
                                                 nLength, 
                                                 randLength,
                                                 np.arange(0,4,0.05), 
                                                 plot=False)

E = 2000
nQDC       = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nQDC.npy')
randQDC    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randQDC.npy')
nLength    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nLength.npy')
randLength = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randLength.npy')

nQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', nQDC)
randQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', randQDC)

x_2000, y_2000 = prodata.randomTOFSubtractionQDC(nQDC,
                                                 randQDC,
                                                 nLength, 
                                                 randLength,
                                                 np.arange(0,4,0.05), 
                                                 plot=False)

E = 3000
nQDC       = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nQDC.npy')
randQDC    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randQDC.npy')
nLength    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nLength.npy')
randLength = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randLength.npy')

nQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', nQDC)
randQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', randQDC)

x_3000, y_3000 = prodata.randomTOFSubtractionQDC(nQDC,
                                                 randQDC,
                                                 nLength, 
                                                 randLength,
                                                 np.arange(0,4,0.1), 
                                                 plot=False)


E = 4000
nQDC       = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nQDC.npy')
randQDC    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randQDC.npy')
nLength    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nLength.npy')
randLength = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randLength.npy')

nQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', nQDC)
randQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', randQDC)

x_4000, y_4000 = prodata.randomTOFSubtractionQDC(nQDC,
                                                 randQDC,
                                                 nLength, 
                                                 randLength,
                                                 np.arange(0,4,0.1), 
                                                 plot=False)

E = 5000
nQDC       = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nQDC.npy')
randQDC    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randQDC.npy')
nLength    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nLength.npy')
randLength = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randLength.npy')

nQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', nQDC)
randQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', randQDC)

x_5000, y_5000 = prodata.randomTOFSubtractionQDC(nQDC,
                                                 randQDC,
                                                 nLength, 
                                                 randLength,
                                                 np.arange(0,4,0.05), 
                                                 plot=False)

E = 6000
nQDC       = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nQDC.npy')
randQDC    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randQDC.npy')
nLength    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nLength.npy')
randLength = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randLength.npy')

nQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', nQDC)
randQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', randQDC)

x_6000, y_6000 = prodata.randomTOFSubtractionQDC(nQDC,
                                                 randQDC,
                                                 nLength, 
                                                 randLength,
                                                 np.arange(0,4,0.1), 
                                                 plot=False)

plt.figure()
plt.suptitle(f'{detector}, K$_B=0.1106$ mm/MeV')
plt.subplot(6,1,1)
plt.step(x_1500, y_1500*3, label='Data')
plt.hist(NeutronSim_1500.optPhotonSumQE, bins=np.arange(0.08,4,0.05), histtype='step', label='sim')
plt.legend()
plt.ylim([0, np.max(y_1500*3)*1.25])
plt.ylabel('counts')

plt.subplot(6,1,2)
plt.step(x_2000, y_2000*2, label='Data')
plt.hist(NeutronSim_2000.optPhotonSumQE, bins=np.arange(0.08,4,0.05), histtype='step', label='sim')
plt.legend()
plt.ylim([0, np.max(y_2000*2)*1.25])
plt.ylabel('counts')

plt.subplot(6,1,3)
plt.step(x_3000, y_3000*1.45, label='Data')
plt.hist(NeutronSim_3000.optPhotonSumQE, bins=np.arange(0.15,4,0.1), histtype='step', label='sim')
plt.legend()
plt.ylim([0, np.max(y_3000*1.45)*1.25])
plt.ylabel('counts')

plt.subplot(6,1,4)
plt.step(x_4000, y_4000*1.4, label='Data')
plt.hist(NeutronSim_4000.optPhotonSumQE, bins=np.arange(0.15,4,0.1), histtype='step', label='sim')
plt.legend()
plt.ylim([0, np.max(y_4000*1.4)*1.25])
plt.ylabel('counts')

plt.subplot(6,1,5)
plt.step(x_5000, y_5000*1.3, label='Data')
plt.hist(NeutronSim_5000.optPhotonSumQE, bins=np.arange(0.15,4,0.05), histtype='step', label='sim')
plt.legend()
plt.ylim([0, np.max(y_5000*1.3)*1.25])
plt.ylabel('counts')

plt.subplot(6,1,6)
plt.step(x_6000, y_6000*3.2, label='Data')
plt.hist(NeutronSim_6000.optPhotonSumQE, bins=np.arange(0.15,4,0.1), histtype='step', label='sim')
plt.legend()
plt.ylim([0, np.max(y_6000*3.2)*1.25])
plt.ylabel('counts')
plt.xlabel('Energy [MeVee]')
plt.show()
















##########################################################

################## EJ321P #################################
##### CALIBRATING SCINT PHOTONS TO QDC (bootstrap) #######

##########################################################


path_sim = '/media/gheed/Seagate_Expansion_Drive1/data/simulation/'
names = [ 'evtNum', 
        'optPhotonSum', 
        'optPhotonSumQE', 
        'optPhotonSumCompton', 
        'optPhotonSumComptonQE', 
        'optPhotonSumNPQE', 
        'xLoc', 
        'yLoc', 
        'zLoc', 
        'CsMin', 
        'optPhotonParentID', 
        'optPhotonParentCreatorProcess', 
        'optPhotonParentStartingEnergy',
        'total_edep',
        'gamma_edep',
        'proton_edep',
        'nScatters',
        'nScattersProton']

detector = 'EJ321P'

simLightYieldFactor = 1/3 

NeutronSim_1500 = pd.read_csv(path_sim + f"{detector}/neutrons/neutron_range/{int(1500)}keV/isotropic/CSV_optphoton_data_sum.csv", names=names)
y, x = np.histogram(NeutronSim_1500.optPhotonSumQE, bins=4096, range=[0, 4096])
NeutronSim_1500.optPhotonSumQE = prodata.getRandDist(x, y) * simLightYieldFactor

NeutronSim_2000 = pd.read_csv(path_sim + f"{detector}/neutrons/neutron_range/{int(2000)}keV/isotropic/CSV_optphoton_data_sum.csv", names=names)
y, x = np.histogram(NeutronSim_2000.optPhotonSumQE, bins=4096, range=[0, 4096])
NeutronSim_2000.optPhotonSumQE = prodata.getRandDist(x, y) * simLightYieldFactor

NeutronSim_3000 = pd.read_csv(path_sim + f"{detector}/neutrons/neutron_range/{int(3000)}keV/isotropic/CSV_optphoton_data_sum.csv", names=names)
y, x = np.histogram(NeutronSim_3000.optPhotonSumQE, bins=4096, range=[0, 4096])
NeutronSim_3000.optPhotonSumQE = prodata.getRandDist(x, y) * simLightYieldFactor

NeutronSim_4000 = pd.read_csv(path_sim + f"{detector}/neutrons/neutron_range/{int(4000)}keV/isotropic/CSV_optphoton_data_sum.csv", names=names)
y, x = np.histogram(NeutronSim_4000.optPhotonSumQE, bins=4096, range=[0, 4096])
NeutronSim_4000.optPhotonSumQE = prodata.getRandDist(x, y) * simLightYieldFactor

NeutronSim_5000 = pd.read_csv(path_sim + f"{detector}/neutrons/neutron_range/{int(5000)}keV/isotropic/CSV_optphoton_data_sum.csv", names=names)
y, x = np.histogram(NeutronSim_5000.optPhotonSumQE, bins=4096, range=[0, 4096])
NeutronSim_5000.optPhotonSumQE = prodata.getRandDist(x, y) * simLightYieldFactor

NeutronSim_6000 = pd.read_csv(path_sim + f"{detector}/neutrons/neutron_range/{int(6000)}keV/isotropic/CSV_optphoton_data_sum.csv", names=names)
y, x = np.histogram(NeutronSim_6000.optPhotonSumQE, bins=4096, range=[0, 4096])
NeutronSim_6000.optPhotonSumQE = prodata.getRandDist(x, y) * simLightYieldFactor

#Calibrating simulation to MeVee
NeutronSim_1500.optPhotonSumQE = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Simcal', NeutronSim_1500.optPhotonSumQE, inverse=False)
NeutronSim_2000.optPhotonSumQE = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Simcal', NeutronSim_2000.optPhotonSumQE, inverse=False)
NeutronSim_3000.optPhotonSumQE = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Simcal', NeutronSim_3000.optPhotonSumQE, inverse=False)
NeutronSim_4000.optPhotonSumQE = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Simcal', NeutronSim_4000.optPhotonSumQE, inverse=False)
NeutronSim_5000.optPhotonSumQE = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Simcal', NeutronSim_5000.optPhotonSumQE, inverse=False)
NeutronSim_6000.optPhotonSumQE = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Simcal', NeutronSim_6000.optPhotonSumQE, inverse=False)

#smearing simulations
smearConstant = 0.14

NeutronSim_1500.optPhotonSumQE = NeutronSim_1500.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, smearConstant)) 
NeutronSim_2000.optPhotonSumQE = NeutronSim_2000.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, smearConstant)) 
NeutronSim_3000.optPhotonSumQE = NeutronSim_3000.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, smearConstant)) 
NeutronSim_4000.optPhotonSumQE = NeutronSim_4000.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, smearConstant)) 
NeutronSim_5000.optPhotonSumQE = NeutronSim_5000.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, smearConstant)) 
NeutronSim_6000.optPhotonSumQE = NeutronSim_6000.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, smearConstant)) 

#DATA
numBins = np.arange(0.0, 10, 0.1)

E = 1500
nQDC       = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nQDC.npy')
randQDC    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randQDC.npy')
nLength    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nLength.npy')
randLength = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randLength.npy')

nQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', nQDC)
randQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', randQDC)

x_1500, y_1500 = prodata.randomTOFSubtractionQDC(nQDC,
                                                 randQDC,
                                                 nLength, 
                                                 randLength,
                                                 np.arange(0,4,0.05), 
                                                 plot=False)

E = 2000
nQDC       = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nQDC.npy')
randQDC    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randQDC.npy')
nLength    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nLength.npy')
randLength = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randLength.npy')

nQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', nQDC)
randQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', randQDC)

x_2000, y_2000 = prodata.randomTOFSubtractionQDC(nQDC,
                                                 randQDC,
                                                 nLength, 
                                                 randLength,
                                                 np.arange(0,4,0.05), 
                                                 plot=False)

E = 3000
nQDC       = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nQDC.npy')
randQDC    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randQDC.npy')
nLength    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nLength.npy')
randLength = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randLength.npy')

nQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', nQDC)
randQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', randQDC)

x_3000, y_3000 = prodata.randomTOFSubtractionQDC(nQDC,
                                                 randQDC,
                                                 nLength, 
                                                 randLength,
                                                 np.arange(0,4,0.1), 
                                                 plot=False)


E = 4000
nQDC       = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nQDC.npy')
randQDC    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randQDC.npy')
nLength    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nLength.npy')
randLength = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randLength.npy')

nQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', nQDC)
randQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', randQDC)

x_4000, y_4000 = prodata.randomTOFSubtractionQDC(nQDC,
                                                 randQDC,
                                                 nLength, 
                                                 randLength,
                                                 np.arange(0,4,0.1), 
                                                 plot=False)

E = 5000
nQDC       = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nQDC.npy')
randQDC    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randQDC.npy')
nLength    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nLength.npy')
randLength = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randLength.npy')

nQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', nQDC)
randQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', randQDC)

x_5000, y_5000 = prodata.randomTOFSubtractionQDC(nQDC,
                                                 randQDC,
                                                 nLength, 
                                                 randLength,
                                                 np.arange(0,4,0.1), 
                                                 plot=False)

E = 6000
nQDC       = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nQDC.npy')
randQDC    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randQDC.npy')
nLength    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nLength.npy')
randLength = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randLength.npy')

nQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', nQDC)
randQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', randQDC)

x_6000, y_6000 = prodata.randomTOFSubtractionQDC(nQDC,
                                                 randQDC,
                                                 nLength, 
                                                 randLength,
                                                 np.arange(0,4,0.1), 
                                                 plot=False)

plt.figure()
plt.suptitle(f'{detector}, K$_B=0.1828$ mm/MeV')
plt.subplot(6,1,1)
plt.step(x_1500, y_1500*20, label='Data')
plt.hist(NeutronSim_1500.optPhotonSumQE, bins=np.arange(0.08,4,0.05), histtype='step', label='sim')
plt.legend()
plt.ylim([0, np.max(y_1500*20)*1.25])
plt.ylabel('counts')

plt.subplot(6,1,2)
plt.step(x_2000, y_2000*7.3, label='Data')
plt.hist(NeutronSim_2000.optPhotonSumQE, bins=np.arange(0.08,4,0.05), histtype='step', label='sim')
plt.legend()
plt.ylim([0, np.max(y_2000*7.3)*1.25])
plt.ylabel('counts')

plt.subplot(6,1,3)
plt.step(x_3000, y_3000*5.0, label='Data')
plt.hist(NeutronSim_3000.optPhotonSumQE, bins=np.arange(0.15,4,0.1), histtype='step', label='sim')
plt.legend()
plt.ylim([0, np.max(y_3000*5.0)*1.25])
plt.ylabel('counts')

plt.subplot(6,1,4)
plt.step(x_4000, y_4000*6, label='Data')
plt.hist(NeutronSim_4000.optPhotonSumQE, bins=np.arange(0.15,4,0.1), histtype='step', label='sim')
plt.legend()
plt.ylim([0, np.max(y_4000*6)*1.25])
plt.ylabel('counts')

plt.subplot(6,1,5)
plt.step(x_5000, y_5000*3, label='Data')
plt.hist(NeutronSim_5000.optPhotonSumQE, bins=np.arange(0.15,4,0.05), histtype='step', label='sim')
plt.legend()
plt.ylim([0, np.max(y_5000*3)*1.25])
plt.ylabel('counts')

plt.subplot(6,1,6)
plt.step(x_6000, y_6000*12, label='Data')
plt.hist(NeutronSim_6000.optPhotonSumQE, bins=np.arange(0.15,4,0.1), histtype='step', label='sim')
plt.legend()
plt.ylim([0, np.max(y_6000*12)*1.25])
plt.ylabel('counts')
plt.xlabel('Energy [MeVee]')
plt.show()












##########################################################

################## EJ331 #################################
##### CALIBRATING SCINT PHOTONS TO QDC (bootstrap) #######

##########################################################



path_sim = '/media/gheed/Seagate_Expansion_Drive1/data/simulation/'
names = [ 'evtNum', 
        'optPhotonSum', 
        'optPhotonSumQE', 
        'optPhotonSumCompton', 
        'optPhotonSumComptonQE', 
        'optPhotonSumNPQE', 
        'xLoc', 
        'yLoc', 
        'zLoc', 
        'CsMin', 
        'optPhotonParentID', 
        'optPhotonParentCreatorProcess', 
        'optPhotonParentStartingEnergy',
        'total_edep',
        'gamma_edep',
        'proton_edep',
        'nScatters',
        'nScattersProton']

detector = 'EJ331'

NeutronSim_1500 = pd.read_csv(path_sim + f"{detector}/neutrons/neutron_range/{int(1500)}keV/isotropic/CSV_optphoton_data_sum.csv", names=names)
y, x = np.histogram(NeutronSim_1500.optPhotonSumQE, bins=4096, range=[0, 4096])
NeutronSim_1500.optPhotonSumQE = prodata.getRandDist(x, y)

NeutronSim_2000 = pd.read_csv(path_sim + f"{detector}/neutrons/neutron_range/{int(2000)}keV/isotropic/CSV_optphoton_data_sum.csv", names=names)
y, x = np.histogram(NeutronSim_2000.optPhotonSumQE, bins=4096, range=[0, 4096])
NeutronSim_2000.optPhotonSumQE = prodata.getRandDist(x, y)

NeutronSim_3000 = pd.read_csv(path_sim + f"{detector}/neutrons/neutron_range/{int(3000)}keV/isotropic/CSV_optphoton_data_sum.csv", names=names)
y, x = np.histogram(NeutronSim_3000.optPhotonSumQE, bins=4096, range=[0, 4096])
NeutronSim_3000.optPhotonSumQE = prodata.getRandDist(x, y)

NeutronSim_4000 = pd.read_csv(path_sim + f"{detector}/neutrons/neutron_range/{int(4000)}keV/isotropic/CSV_optphoton_data_sum.csv", names=names)
y, x = np.histogram(NeutronSim_4000.optPhotonSumQE, bins=4096, range=[0, 4096])
NeutronSim_4000.optPhotonSumQE = prodata.getRandDist(x, y)

NeutronSim_5000 = pd.read_csv(path_sim + f"{detector}/neutrons/neutron_range/{int(5000)}keV/isotropic/CSV_optphoton_data_sum.csv", names=names)
y, x = np.histogram(NeutronSim_5000.optPhotonSumQE, bins=4096, range=[0, 4096])
NeutronSim_5000.optPhotonSumQE = prodata.getRandDist(x, y)

NeutronSim_6000 = pd.read_csv(path_sim + f"{detector}/neutrons/neutron_range/{int(6000)}keV/isotropic/CSV_optphoton_data_sum.csv", names=names)
y, x = np.histogram(NeutronSim_6000.optPhotonSumQE, bins=4096, range=[0, 4096])
NeutronSim_6000.optPhotonSumQE = prodata.getRandDist(x, y)

#Calibrating simulation to MeVee
NeutronSim_1500.optPhotonSumQE = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Simcal', NeutronSim_1500.optPhotonSumQE, inverse=False)
NeutronSim_2000.optPhotonSumQE = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Simcal', NeutronSim_2000.optPhotonSumQE, inverse=False)
NeutronSim_3000.optPhotonSumQE = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Simcal', NeutronSim_3000.optPhotonSumQE, inverse=False)
NeutronSim_4000.optPhotonSumQE = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Simcal', NeutronSim_4000.optPhotonSumQE, inverse=False)
NeutronSim_5000.optPhotonSumQE = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Simcal', NeutronSim_5000.optPhotonSumQE, inverse=False)
NeutronSim_6000.optPhotonSumQE = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Simcal', NeutronSim_6000.optPhotonSumQE, inverse=False)

#smearing simulations
smearConstant = 0.11

NeutronSim_1500.optPhotonSumQE = NeutronSim_1500.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, smearConstant)) 
NeutronSim_2000.optPhotonSumQE = NeutronSim_2000.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, smearConstant)) 
NeutronSim_3000.optPhotonSumQE = NeutronSim_3000.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, smearConstant)) 
NeutronSim_4000.optPhotonSumQE = NeutronSim_4000.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, smearConstant)) 
NeutronSim_5000.optPhotonSumQE = NeutronSim_5000.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, smearConstant)) 
NeutronSim_6000.optPhotonSumQE = NeutronSim_6000.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, smearConstant)) 


#DATA
numBins = np.arange(0.0,10,0.1)

E = 1500
nQDC       = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nQDC.npy')
randQDC    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randQDC.npy')
nLength    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nLength.npy')
randLength = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randLength.npy')

nQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', nQDC)
randQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', randQDC)

x_1500, y_1500 = prodata.randomTOFSubtractionQDC(nQDC,
                                                 randQDC,
                                                 nLength, 
                                                 randLength,
                                                 np.arange(0,4,0.05), 
                                                 plot=False)

E = 2000
nQDC       = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nQDC.npy')
randQDC    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randQDC.npy')
nLength    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nLength.npy')
randLength = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randLength.npy')

nQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', nQDC)
randQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', randQDC)

x_2000, y_2000 = prodata.randomTOFSubtractionQDC(nQDC,
                                                 randQDC,
                                                 nLength, 
                                                 randLength,
                                                 np.arange(0,4,0.05), 
                                                 plot=False)

E = 3000
nQDC       = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nQDC.npy')
randQDC    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randQDC.npy')
nLength    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nLength.npy')
randLength = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randLength.npy')

nQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', nQDC)
randQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', randQDC)

x_3000, y_3000 = prodata.randomTOFSubtractionQDC(nQDC,
                                                 randQDC,
                                                 nLength, 
                                                 randLength,
                                                 np.arange(0,4,0.1), 
                                                 plot=False)


E = 4000
nQDC       = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nQDC.npy')
randQDC    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randQDC.npy')
nLength    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nLength.npy')
randLength = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randLength.npy')

nQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', nQDC)
randQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', randQDC)

x_4000, y_4000 = prodata.randomTOFSubtractionQDC(nQDC,
                                                 randQDC,
                                                 nLength, 
                                                 randLength,
                                                 np.arange(0,4,0.1), 
                                                 plot=False)

E = 5000
nQDC       = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nQDC.npy')
randQDC    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randQDC.npy')
nLength    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nLength.npy')
randLength = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randLength.npy')

nQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', nQDC)
randQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', randQDC)

x_5000, y_5000 = prodata.randomTOFSubtractionQDC(nQDC,
                                                 randQDC,
                                                 nLength, 
                                                 randLength,
                                                 np.arange(0,4,0.1), 
                                                 plot=False)

E = 6000
nQDC       = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nQDC.npy')
randQDC    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randQDC.npy')
nLength    = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/nLength.npy')
randLength = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/randLength.npy')

nQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', nQDC)
randQDC = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal', randQDC)

x_6000, y_6000 = prodata.randomTOFSubtractionQDC(nQDC,
                                                 randQDC,
                                                 nLength, 
                                                 randLength,
                                                 np.arange(0,4,0.1), 
                                                 plot=False)

plt.figure()
plt.suptitle(f'{detector}, K$_B=0.1276$ mm/MeV')
plt.subplot(6,1,1)
plt.step(x_1500, y_1500*10, label='Data')
plt.hist(NeutronSim_1500.optPhotonSumQE, bins=np.arange(0.08,4,0.05), histtype='step', label='sim')
plt.legend()
plt.ylim([0, np.max(y_1500*10)*1.25])
plt.ylabel('counts')

plt.subplot(6,1,2)
plt.step(x_2000, y_2000*5, label='Data')
plt.hist(NeutronSim_2000.optPhotonSumQE, bins=np.arange(0.08,4,0.05), histtype='step', label='sim')
plt.legend()
plt.ylim([0, np.max(y_2000*5)*1.25])
plt.ylabel('counts')

plt.subplot(6,1,3)
plt.step(x_3000, y_3000*4, label='Data')
plt.hist(NeutronSim_3000.optPhotonSumQE, bins=np.arange(0.15,4,0.1), histtype='step', label='sim')
plt.legend()
plt.ylim([0, np.max(y_3000*4)*1.25])
plt.ylabel('counts')

plt.subplot(6,1,4)
plt.step(x_4000, y_4000*4, label='Data')
plt.hist(NeutronSim_4000.optPhotonSumQE, bins=np.arange(0.15,4,0.1), histtype='step', label='sim')
plt.legend()
plt.ylim([0, np.max(y_4000*4)*1.25])
plt.ylabel('counts')

plt.subplot(6,1,5)
plt.step(x_5000, y_5000*1.7, label='Data')
plt.hist(NeutronSim_5000.optPhotonSumQE, bins=np.arange(0.15,4,0.05), histtype='step', label='sim')
plt.legend()
plt.ylim([0, np.max(y_5000*1.7)*1.25])
plt.ylabel('counts')

plt.subplot(6,1,6)
plt.step(x_6000, y_6000*8, label='Data')
plt.hist(NeutronSim_6000.optPhotonSumQE, bins=np.arange(0.15,4,0.1), histtype='step', label='sim')
plt.legend()
plt.ylim([0, np.max(y_6000*8)*1.25])
plt.ylabel('counts')
plt.xlabel('Energy [MeVee]')
plt.show()