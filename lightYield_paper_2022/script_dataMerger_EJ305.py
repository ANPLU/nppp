#!/usr/bin/env python3
import sys
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit

# IMPORT FROM LIBRARY DIRECTORY
sys.path.insert(0, "../library/")
sys.path.insert(0, "../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "sim_analysis/")
sys.path.insert(0, "data_analysis/")

import digidata
import nicholai_plot_helper as nplt
import processing_utility as prout
import processing_math as promath
import processing_data as prodata
import processing_pd as propd
import processing_simulation as prosim


"""
Script will load all data from one deteector in 5 h increments, 
then apply a small threshold cut to reduce statistics and enable one file with all data to be created!
"""

gainOffsets = np.array([1.    , 0.9999, 0.9999, 0.9999, 0.9999, 0.9902, 0.9922, 0.9902,
                        0.99  , 0.991 , 0.99  , 0.99  , 0.9829, 0.9823, 0.9716, 0.9702,
                        0.9603, 0.9728, 0.9716, 0.9603, 0.9526, 0.9716, 0.9702, 0.973 ,
                        0.971 , 0.9603, 0.9706, 0.9603, 0.9536, 0.9603, 0.9702, 0.9603,
                        0.9605, 0.9623, 0.9641, 0.9514, 0.9548, 0.9702, 0.9643, 0.9504,
                        0.9665, 0.9706, 0.9702, 0.971 , 0.9459, 0.9603, 0.9603, 0.9633,
                        0.9649, 0.9617, 0.9621, 0.9447, 0.9405, 0.9504, 0.9702, 0.9435,
                        0.9407, 0.9621, 0.9413, 0.9405, 0.9605, 0.9427, 0.9613, 0.9407,
                        0.951 , 0.9453, 0.933 , 0.9405, 0.9532, 0.9504, 0.9603, 0.9504,
                        0.9405, 0.9546, 0.9407, 0.9508, 0.9504, 0.9603, 0.934 , 0.9445,
                        0.9603, 0.931 , 0.9449, 0.9324, 0.9429, 0.9409, 0.9508, 0.9603,
                        0.9542, 0.9512, 0.9532, 0.9431, 0.9524, 0.9308, 0.9411, 0.9504,
                        0.9508, 0.9516, 0.9504, 0.956 , 0.9423, 0.9405, 0.9316, 0.9405,
                        0.9407, 0.9405, 0.9443, 0.9508, 0.9603, 0.9603, 0.9518, 0.9603,
                        0.9603, 0.9524, 0.9603, 0.9603, 0.9516, 0.9532, 0.9603, 0.9504,
                        0.9528, 0.9603, 0.9605, 0.9603, 0.9611, 0.9603, 0.9603, 0.9603,
                        0.9506, 0.9504, 0.9512, 0.9603, 0.9435, 0.9605, 0.9801, 0.9504,
                        0.9702, 0.9702, 0.9603, 0.9504, 0.9603, 0.9504, 0.9407, 0.9603,
                        0.9605, 0.9405, 0.9702, 0.9504, 0.953 , 0.9413, 0.9619, 0.9504,
                        0.9603, 0.9637, 0.9617, 0.9603, 0.9532, 0.9417, 0.9504, 0.9702,
                        0.951 , 0.9621, 0.9603, 0.9702, 0.9702, 0.9702, 0.9702, 0.9702,
                        0.9417, 0.9407, 0.9702, 0.9609, 0.971 , 0.9605, 0.9611, 0.9605,
                        0.9413, 0.9405, 0.9504, 0.9508, 0.9413, 0.9613, 0.9613, 0.9427,
                        0.9415, 0.9603, 0.9603, 0.9405, 0.9609, 0.9526, 0.9506, 0.9508,
                        0.9431, 0.9605, 0.9609, 0.9409, 0.9421, 0.9603, 0.9504, 0.951 ,
                        0.9623])

detector = 'EJ305'
att_factor = 8.91 # 19dB

path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop
run1 = 3038
numRuns = 201
runList = np.arange(run1, run1+numRuns)


distance = 0.959
y2_thr = 600#
y3_thr = 600#6985
y4_thr = 600#
y5_thr = 600#

n_thr = 2400

dfFull = pd.DataFrame({'qdc_lg_ch1':[],
                        'qdc_ps_ch1':[],
                        'tof_ch2':[],
                        'tof_ch3':[],
                        'tof_ch4':[],
                        'tof_ch5':[],
                        'amplitude_ch2':[],
                        'amplitude_ch3':[],
                        'amplitude_ch4':[],
                        'amplitude_ch5':[],
                        'qdc_lg_ch2':[],
                        'qdc_lg_ch3':[],
                        'qdc_lg_ch4':[],
                        'qdc_lg_ch5':[],
                        'tofE_ch2':[],
                        'tofE_ch3':[],
                        'tofE_ch4':[],
                        'tofE_ch5':[]})

for i in np.arange(0, len(runList), 5):
    currentRunList      = np.split(runList, [i, i+5])[1] #split runlist into section of 10 events
    currentGainOffsets  = np.split(gainOffsets, [i, i+5])[1]  #split gainoffsets into section of 10 events
    print('-------------------------------------------------')
    print(f'Now working on runs: {currentRunList}')
    PuBe_tof_data = propd.load_parquet_merge_gainadjust(path, currentRunList, keep_col=[    'qdc_lg_ch1', 
                                                                                            'qdc_ps_ch1',
                                                                                            'tof_ch2', 
                                                                                            'tof_ch3', 
                                                                                            'tof_ch4', 
                                                                                            'tof_ch5', 
                                                                                            'amplitude_ch2', 
                                                                                            'amplitude_ch3', 
                                                                                            'amplitude_ch4', 
                                                                                            'amplitude_ch5', 
                                                                                            'qdc_lg_ch2',
                                                                                            'qdc_lg_ch3',
                                                                                            'qdc_lg_ch4',
                                                                                            'qdc_lg_ch5'], 
                                                                                            full=False,
                                                                                            gainOffsets=currentGainOffsets)
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    #   Apply attenuation coefficient
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    PuBe_tof_data.qdc_lg_ch1 = PuBe_tof_data.qdc_lg_ch1*att_factor

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    #   Calibrate ToF
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    flashrange = {'tof_ch3':[55, 66], 'tof_ch4':[55, 66], 'tof_ch5':[55, 66]} #gamma flash fitting ranges
    prodata.tof_calibration(PuBe_tof_data, flashrange, distance, phcut=125, calUnit=1e-9, energyCal=True)
    
    PuBe_tof_data['tofE_ch2'] = np.zeros(len(PuBe_tof_data)) #adding due to empty ch2 
    
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    #   Apply thresholds to Data
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    slice_TMP = PuBe_tof_data.query(f'qdc_lg_ch1>{n_thr} and (qdc_lg_ch3>{y3_thr} or qdc_lg_ch4>{y4_thr} or qdc_lg_ch5>{y5_thr})')
    
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    #   Merge Data
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    dfFull = pd.concat([dfFull, slice_TMP], ignore_index=False)

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    #   INFO print-out
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    print(f'Number of events currently: {len(dfFull)}')

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#   Save to disk
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
dfFull.to_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/{detector}_data.pkl')

