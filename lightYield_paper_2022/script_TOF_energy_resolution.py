
#!/usr/bin/env python3
import sys
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit

# IMPORT FROM LIBRARY DIRECTORY
sys.path.insert(0, "../library/")
sys.path.insert(0, "../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "sim_analysis/")
sys.path.insert(0, "data_analysis/")

import digidata
import nicholai_plot_helper as nplt
import processing_utility as prout
import processing_math as promath
import processing_data as prodata
import processing_pd as propd
import processing_simulation as prosim

"""
Script to calculate energy reoplsution of the TOF setup
"""

pathData = '/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper'
pathSim = '/media/gheed/Seagate_Expansion_Drive1/data/simulation'


# ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
distance = 0.959
steps = np.arange(-0.0315, 0.03175, 0.001)
dd = 0.032

# prodata.TOFtoEnergy(4)
Tn = 2
t0 = prodata.EnergytoTOF(Tn, distance) #time to compare against!
t1 = prodata.EnergytoTOF(Tn, distance+dd)
t2 = prodata.EnergytoTOF(Tn, distance-dd)

E0 = prodata.TOFtoEnergy(t0, distance)
E1 = prodata.TOFtoEnergy(t1, distance)
E2 = prodata.TOFtoEnergy(t2, distance)

E1/E0

E2/E0


dTime = prodata.EnergytoTOF(Tn, distance+steps)/meanTime #difference in time around the mean
res = prodata.TOFtoEnergy(dTime, distance)



time = prodata.EnergytoTOF(Tn, distance+steps) 
energy = prodata.TOFtoEnergy(time, distance+steps)

plt.scatter(steps, res)
plt.show()