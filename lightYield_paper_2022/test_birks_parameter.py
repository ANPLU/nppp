# Make folders
cd /drive2/EJ321P_char_simulations/NE213A/neutrons/neutron_range/2000keV/isotropic
mkdir birks145
mkdir birks126
mkdir birks100
mkdir birks0917
mkdir birks085
mkdir birks060
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/2250keV/isotropic
mkdir birks1516
mkdir birks1260
mkdir birks1100
mkdir birks1000
mkdir birks0911
mkdir birks0683
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/2000keV/isotropic
mkdir birks195
mkdir birks145
mkdir birks135
mkdir birks126
mkdir birks086
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/2000keV/isotropic
mkdir birks165
mkdir birks145
mkdir birks126
mkdir birks086






###3.5 MeV NE213A
cd /drive2/EJ321P_char_simulations/NE213A/neutrons/neutron_range/3500keV/isotropic/birks145
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=3.50 delta_E_MeV=0.125 detector_mat=NE213A birks_constant=0.145 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/NE213A/neutrons/neutron_range/3500keV/isotropic/birks126
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=3.50 delta_E_MeV=0.125 detector_mat=NE213A birks_constant=0.126 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/NE213A/neutrons/neutron_range/3500keV/isotropic/birks100
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=3.50 delta_E_MeV=0.125 detector_mat=NE213A birks_constant=0.100 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/NE213A/neutrons/neutron_range/3500keV/isotropic/birks0917
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=3.50 delta_E_MeV=0.125 detector_mat=NE213A birks_constant=0.0917 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/NE213A/neutrons/neutron_range/3500keV/isotropic/birks085
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=3.50 delta_E_MeV=0.125 detector_mat=NE213A birks_constant=0.085 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/NE213A/neutrons/neutron_range/3500keV/isotropic/birks060
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=3.50 delta_E_MeV=0.125 detector_mat=NE213A birks_constant=0.060 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*

###5.5 MeV NE213A
cd /drive2/EJ321P_char_simulations/NE213A/neutrons/neutron_range/5500keV/isotropic/birks145
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=5.50 delta_E_MeV=0.125 detector_mat=NE213A birks_constant=0.145 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/NE213A/neutrons/neutron_range/5500keV/isotropic/birks126
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=5.50 delta_E_MeV=0.125 detector_mat=NE213A birks_constant=0.126 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/NE213A/neutrons/neutron_range/5500keV/isotropic/birks100
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=5.50 delta_E_MeV=0.125 detector_mat=NE213A birks_constant=0.100 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/NE213A/neutrons/neutron_range/5500keV/isotropic/birks0917
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=5.50 delta_E_MeV=0.125 detector_mat=NE213A birks_constant=0.0917 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/NE213A/neutrons/neutron_range/5500keV/isotropic/birks085
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=5.50 delta_E_MeV=0.125 detector_mat=NE213A birks_constant=0.085 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/NE213A/neutrons/neutron_range/5500keV/isotropic/birks060
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=5.50 delta_E_MeV=0.125 detector_mat=NE213A birks_constant=0.060 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*

###6.0 MeV NE213A
cd /drive2/EJ321P_char_simulations/NE213A/neutrons/neutron_range/6000keV/isotropic/birks145
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=6.00 delta_E_MeV=0.125 detector_mat=NE213A birks_constant=0.145 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/NE213A/neutrons/neutron_range/6000keV/isotropic/birks126
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=6.00 delta_E_MeV=0.125 detector_mat=NE213A birks_constant=0.126 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/NE213A/neutrons/neutron_range/6000keV/isotropic/birks100
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=6.00 delta_E_MeV=0.125 detector_mat=NE213A birks_constant=0.100 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/NE213A/neutrons/neutron_range/6000keV/isotropic/birks0917
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=6.00 delta_E_MeV=0.125 detector_mat=NE213A birks_constant=0.0917 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/NE213A/neutrons/neutron_range/6000keV/isotropic/birks085
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=6.00 delta_E_MeV=0.125 detector_mat=NE213A birks_constant=0.085 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/NE213A/neutrons/neutron_range/6000keV/isotropic/birks060
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=6.00 delta_E_MeV=0.125 detector_mat=NE213A birks_constant=0.060 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
######################################
###2.00 MeV EJ305
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/2000keV/isotropic/birks1516
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=2.0 delta_E_MeV=0.125 detector_mat=EJ305 birks_constant=0.1516 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/2000keV/isotropic/birks1260
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=2.0 delta_E_MeV=0.125 detector_mat=EJ305 birks_constant=0.1260 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/2000keV/isotropic/birks1100
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=2.0 delta_E_MeV=0.125 detector_mat=EJ305 birks_constant=0.1100 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/2000keV/isotropic/birks1000
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=2.0 delta_E_MeV=0.125 detector_mat=EJ305 birks_constant=0.1000 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/2000keV/isotropic/birks0911
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=2.0 delta_E_MeV=0.125 detector_mat=EJ305 birks_constant=0.0911 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/2000keV/isotropic/birks0683
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=2.0 delta_E_MeV=0.125 detector_mat=EJ305 birks_constant=0.0683 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
###2.25 MeV EJ305
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/2250keV/isotropic/birks1516
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=2.250 delta_E_MeV=0.125 detector_mat=EJ305 birks_constant=0.1516 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/2250keV/isotropic/birks1260
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=2.250 delta_E_MeV=0.125 detector_mat=EJ305 birks_constant=0.1260 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/2250keV/isotropic/birks1100
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=2.250 delta_E_MeV=0.125 detector_mat=EJ305 birks_constant=0.1100 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/2250keV/isotropic/birks1000
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=2.250 delta_E_MeV=0.125 detector_mat=EJ305 birks_constant=0.1000 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/2250keV/isotropic/birks0911
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=2.250 delta_E_MeV=0.125 detector_mat=EJ305 birks_constant=0.0911 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/2250keV/isotropic/birks0683
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=2.250 delta_E_MeV=0.125 detector_mat=EJ305 birks_constant=0.0683 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
###2.5 MeV EJ305
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/2500keV/isotropic/birks1516
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=2.50 delta_E_MeV=0.125 detector_mat=EJ305 birks_constant=0.1516 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/2500keV/isotropic/birks1260
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=2.50 delta_E_MeV=0.125 detector_mat=EJ305 birks_constant=0.1260 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/2500keV/isotropic/birks1100
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=2.50 delta_E_MeV=0.125 detector_mat=EJ305 birks_constant=0.1100 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/2500keV/isotropic/birks1000
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=2.50 delta_E_MeV=0.125 detector_mat=EJ305 birks_constant=0.1000 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/2500keV/isotropic/birks0911
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=2.50 delta_E_MeV=0.125 detector_mat=EJ305 birks_constant=0.0911 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/2500keV/isotropic/birks0683
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=2.50 delta_E_MeV=0.125 detector_mat=EJ305 birks_constant=0.0683 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
###3.5 MeV EJ305
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/3500keV/isotropic/birks1516
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=3.50 delta_E_MeV=0.125 detector_mat=EJ305 birks_constant=0.1516 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/3500keV/isotropic/birks1260
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=3.50 delta_E_MeV=0.125 detector_mat=EJ305 birks_constant=0.1260 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/3500keV/isotropic/birks1100
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=3.50 delta_E_MeV=0.125 detector_mat=EJ305 birks_constant=0.1100 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/3500keV/isotropic/birks1000
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=3.50 delta_E_MeV=0.125 detector_mat=EJ305 birks_constant=0.1000 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/3500keV/isotropic/birks0911
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=3.50 delta_E_MeV=0.125 detector_mat=EJ305 birks_constant=0.0911 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/3500keV/isotropic/birks0683
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=3.50 delta_E_MeV=0.125 detector_mat=EJ305 birks_constant=0.0683 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
###4.5 MeV EJ305
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/4500keV/isotropic/birks1516
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=4.50 delta_E_MeV=0.125 detector_mat=EJ305 birks_constant=0.1516 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/4500keV/isotropic/birks1260
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=4.50 delta_E_MeV=0.125 detector_mat=EJ305 birks_constant=0.1260 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/4500keV/isotropic/birks1100
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=4.50 delta_E_MeV=0.125 detector_mat=EJ305 birks_constant=0.1100 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/4500keV/isotropic/birks1000
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=4.50 delta_E_MeV=0.125 detector_mat=EJ305 birks_constant=0.1000 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/4500keV/isotropic/birks0911
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=4.50 delta_E_MeV=0.125 detector_mat=EJ305 birks_constant=0.0911 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/4500keV/isotropic/birks0683
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=4.50 delta_E_MeV=0.125 detector_mat=EJ305 birks_constant=0.0683 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
###5.5 MeV EJ305
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/5500keV/isotropic/birks1516
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=5.50 delta_E_MeV=0.125 detector_mat=EJ305 birks_constant=0.1516 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/5500keV/isotropic/birks1260
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=5.50 delta_E_MeV=0.125 detector_mat=EJ305 birks_constant=0.1260 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/5500keV/isotropic/birks1100
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=5.50 delta_E_MeV=0.125 detector_mat=EJ305 birks_constant=0.1100 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/5500keV/isotropic/birks1000
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=5.50 delta_E_MeV=0.125 detector_mat=EJ305 birks_constant=0.1000 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/5500keV/isotropic/birks0911
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=5.50 delta_E_MeV=0.125 detector_mat=EJ305 birks_constant=0.0911 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/5500keV/isotropic/birks0683
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=5.50 delta_E_MeV=0.125 detector_mat=EJ305 birks_constant=0.0683 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
###5.75 MeV EJ305
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/5750keV/isotropic/birks1516
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=5.750 delta_E_MeV=0.125 detector_mat=EJ305 birks_constant=0.1516 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/5750keV/isotropic/birks1260
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=5.750 delta_E_MeV=0.125 detector_mat=EJ305 birks_constant=0.1260 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/5750keV/isotropic/birks1100
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=5.750 delta_E_MeV=0.125 detector_mat=EJ305 birks_constant=0.1100 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/5750keV/isotropic/birks1000
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=5.750 delta_E_MeV=0.125 detector_mat=EJ305 birks_constant=0.1000 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/5750keV/isotropic/birks0911
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=5.750 delta_E_MeV=0.125 detector_mat=EJ305 birks_constant=0.0911 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/5750keV/isotropic/birks0683
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=5.750 delta_E_MeV=0.125 detector_mat=EJ305 birks_constant=0.0683 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
###6.0 MeV EJ305
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/6000keV/isotropic/birks1516
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=6.0 delta_E_MeV=0.125 detector_mat=EJ305 birks_constant=0.1516 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/6000keV/isotropic/birks1260
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=6.0 delta_E_MeV=0.125 detector_mat=EJ305 birks_constant=0.1260 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/6000keV/isotropic/birks1100
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=6.0 delta_E_MeV=0.125 detector_mat=EJ305 birks_constant=0.1100 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/6000keV/isotropic/birks1000
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=6.0 delta_E_MeV=0.125 detector_mat=EJ305 birks_constant=0.1000 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/6000keV/isotropic/birks0911
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=6.0 delta_E_MeV=0.125 detector_mat=EJ305 birks_constant=0.0911 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/6000keV/isotropic/birks0683
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=6.0 delta_E_MeV=0.125 detector_mat=EJ305 birks_constant=0.0683 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*

#########################################
###2.5 MeV EJ321P
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/2500keV/isotropic/birks195
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=2.50 delta_E_MeV=0.125 detector_mat=EJ321P birks_constant=0.195 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/2500keV/isotropic/birks145
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=2.50 delta_E_MeV=0.125 detector_mat=EJ321P birks_constant=0.145 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/2500keV/isotropic/birks135
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=2.50 delta_E_MeV=0.125 detector_mat=EJ321P birks_constant=0.135 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/2500keV/isotropic/birks126
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=2.50 delta_E_MeV=0.125 detector_mat=EJ321P birks_constant=0.126 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/2500keV/isotropic/birks086
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=2.50 delta_E_MeV=0.125 detector_mat=EJ321P birks_constant=0.086 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
###3.5 MeV EJ321P
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/3500keV/isotropic/birks195
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=3.50 delta_E_MeV=0.125 detector_mat=EJ321P birks_constant=0.195 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/3500keV/isotropic/birks145
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=3.50 delta_E_MeV=0.125 detector_mat=EJ321P birks_constant=0.145 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/3500keV/isotropic/birks135
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=3.50 delta_E_MeV=0.125 detector_mat=EJ321P birks_constant=0.135 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/3500keV/isotropic/birks126
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=3.50 delta_E_MeV=0.125 detector_mat=EJ321P birks_constant=0.126 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/3500keV/isotropic/birks086
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=3.50 delta_E_MeV=0.125 detector_mat=EJ321P birks_constant=0.086 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
###4.5 MeV EJ321P
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/4500keV/isotropic/birks195
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=4.50 delta_E_MeV=0.125 detector_mat=EJ321P birks_constant=0.195 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/4500keV/isotropic/birks145
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=4.50 delta_E_MeV=0.125 detector_mat=EJ321P birks_constant=0.145 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/4500keV/isotropic/birks135
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=4.50 delta_E_MeV=0.125 detector_mat=EJ321P birks_constant=0.135 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/4500keV/isotropic/birks126
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=4.50 delta_E_MeV=0.125 detector_mat=EJ321P birks_constant=0.126 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/4500keV/isotropic/birks086
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=4.50 delta_E_MeV=0.125 detector_mat=EJ321P birks_constant=0.086 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
###5.5 MeV EJ321P
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/5500keV/isotropic/birks195
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=5.50 delta_E_MeV=0.125 detector_mat=EJ321P birks_constant=0.195 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/5500keV/isotropic/birks145
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=5.50 delta_E_MeV=0.125 detector_mat=EJ321P birks_constant=0.145 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/5500keV/isotropic/birks135
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=5.50 delta_E_MeV=0.125 detector_mat=EJ321P birks_constant=0.135 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/5500keV/isotropic/birks126
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=5.50 delta_E_MeV=0.125 detector_mat=EJ321P birks_constant=0.126 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/5500keV/isotropic/birks086
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=5.50 delta_E_MeV=0.125 detector_mat=EJ321P birks_constant=0.086 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
###6.0 MeV EJ321P
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/6000keV/isotropic/birks195
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=6.0 delta_E_MeV=0.125 detector_mat=EJ321P birks_constant=0.195 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/6000keV/isotropic/birks145
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=6.0 delta_E_MeV=0.125 detector_mat=EJ321P birks_constant=0.145 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/6000keV/isotropic/birks135
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=6.0 delta_E_MeV=0.125 detector_mat=EJ321P birks_constant=0.135 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/6000keV/isotropic/birks126
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=6.0 delta_E_MeV=0.125 detector_mat=EJ321P birks_constant=0.126 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/6000keV/isotropic/birks086
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=6.0 delta_E_MeV=0.125 detector_mat=EJ321P birks_constant=0.086 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*



###2.5 MeV EJ331
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/2500keV/isotropic/birks165
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=2.50 delta_E_MeV=0.125 detector_mat=EJ331 birks_constant=0.165 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/2500keV/isotropic/birks145
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=2.50 delta_E_MeV=0.125 detector_mat=EJ331 birks_constant=0.145 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/2500keV/isotropic/birks126
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=2.50 delta_E_MeV=0.125 detector_mat=EJ331 birks_constant=0.126 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/2500keV/isotropic/birks086
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=2.50 delta_E_MeV=0.125 detector_mat=EJ331 birks_constant=0.086 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
###3.5 MeV EJ331
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/3500keV/isotropic/birks165
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=3.50 delta_E_MeV=0.125 detector_mat=EJ331 birks_constant=0.165 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/3500keV/isotropic/birks145
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=3.50 delta_E_MeV=0.125 detector_mat=EJ331 birks_constant=0.145 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/3500keV/isotropic/birks126
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=3.50 delta_E_MeV=0.125 detector_mat=EJ331 birks_constant=0.126 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/3500keV/isotropic/birks086
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=3.50 delta_E_MeV=0.125 detector_mat=EJ331 birks_constant=0.086 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
###4.5 MeV EJ331
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/4500keV/isotropic/birks165
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=4.50 delta_E_MeV=0.125 detector_mat=EJ331 birks_constant=0.165 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/4500keV/isotropic/birks145
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=4.50 delta_E_MeV=0.125 detector_mat=EJ331 birks_constant=0.145 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/4500keV/isotropic/birks126
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=4.50 delta_E_MeV=0.125 detector_mat=EJ331 birks_constant=0.126 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/4500keV/isotropic/birks086
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=4.50 delta_E_MeV=0.125 detector_mat=EJ331 birks_constant=0.086 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
###5.0 MeV EJ331
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/5000keV/isotropic/birks165
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=5.00 delta_E_MeV=0.125 detector_mat=EJ331 birks_constant=0.165 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/5000keV/isotropic/birks145
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=5.00 delta_E_MeV=0.125 detector_mat=EJ331 birks_constant=0.145 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/5000keV/isotropic/birks126
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=5.00 delta_E_MeV=0.125 detector_mat=EJ331 birks_constant=0.126 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/5000keV/isotropic/birks086
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=5.00 delta_E_MeV=0.125 detector_mat=EJ331 birks_constant=0.086 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
###5.5 MeV EJ331
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/5500keV/isotropic/birks165
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=5.50 delta_E_MeV=0.125 detector_mat=EJ331 birks_constant=0.165 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/5500keV/isotropic/birks145
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=5.50 delta_E_MeV=0.125 detector_mat=EJ331 birks_constant=0.145 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/5500keV/isotropic/birks126
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=5.50 delta_E_MeV=0.125 detector_mat=EJ331 birks_constant=0.126 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/5500keV/isotropic/birks086
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=5.50 delta_E_MeV=0.125 detector_mat=EJ331 birks_constant=0.086 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
###6.0 MeV EJ331
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/6000keV/isotropic/birks165
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=6.0 delta_E_MeV=0.125 detector_mat=EJ331 birks_constant=0.165 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/6000keV/isotropic/birks145
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=6.0 delta_E_MeV=0.125 detector_mat=EJ331 birks_constant=0.145 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/6000keV/isotropic/birks126
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=6.0 delta_E_MeV=0.125 detector_mat=EJ331 birks_constant=0.126 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/6000keV/isotropic/birks086
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=6.0 delta_E_MeV=0.125 detector_mat=EJ331 birks_constant=0.086 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*



###2.5 MeV EJ321P
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/2500keV/isotropic/birks195
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=2.50 delta_E_MeV=0.125 detector_mat=EJ321P birks_constant=0.195 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/2500keV/isotropic/birks145
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=2.50 delta_E_MeV=0.125 detector_mat=EJ321P birks_constant=0.145 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/2500keV/isotropic/birks135
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=2.50 delta_E_MeV=0.125 detector_mat=EJ321P birks_constant=0.135 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/2500keV/isotropic/birks126
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=2.50 delta_E_MeV=0.125 detector_mat=EJ321P birks_constant=0.126 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/2500keV/isotropic/birks086
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=2.50 delta_E_MeV=0.125 detector_mat=EJ321P birks_constant=0.086 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
###3.5 MeV EJ321P
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/3500keV/isotropic/birks195
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=3.50 delta_E_MeV=0.125 detector_mat=EJ321P birks_constant=0.195 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/3500keV/isotropic/birks145
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=3.50 delta_E_MeV=0.125 detector_mat=EJ321P birks_constant=0.145 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/3500keV/isotropic/birks135
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=3.50 delta_E_MeV=0.125 detector_mat=EJ321P birks_constant=0.135 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/3500keV/isotropic/birks126
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=3.50 delta_E_MeV=0.125 detector_mat=EJ321P birks_constant=0.126 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/3500keV/isotropic/birks086
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=3.50 delta_E_MeV=0.125 detector_mat=EJ321P birks_constant=0.086 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
###5.5 MeV EJ321P
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/5500keV/isotropic/birks195
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=5.50 delta_E_MeV=0.125 detector_mat=EJ321P birks_constant=0.195 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/5500keV/isotropic/birks145
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=5.50 delta_E_MeV=0.125 detector_mat=EJ321P birks_constant=0.145 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/5500keV/isotropic/birks135
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=5.50 delta_E_MeV=0.125 detector_mat=EJ321P birks_constant=0.135 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/5500keV/isotropic/birks126
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=5.50 delta_E_MeV=0.125 detector_mat=EJ321P birks_constant=0.126 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/5500keV/isotropic/birks086
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=5.50 delta_E_MeV=0.125 detector_mat=EJ321P birks_constant=0.086 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
###6.0 MeV EJ321P
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/6000keV/isotropic/birks195
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=6.0 delta_E_MeV=0.125 detector_mat=EJ321P birks_constant=0.195 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/6000keV/isotropic/birks145
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=6.0 delta_E_MeV=0.125 detector_mat=EJ321P birks_constant=0.145 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/6000keV/isotropic/birks135
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=6.0 delta_E_MeV=0.125 detector_mat=EJ321P birks_constant=0.135 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/6000keV/isotropic/birks126
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=6.0 delta_E_MeV=0.125 detector_mat=EJ321P birks_constant=0.126 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/6000keV/isotropic/birks086
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=6.0 delta_E_MeV=0.125 detector_mat=EJ321P birks_constant=0.086 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*





###2.5 MeV EJ331
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/2500keV/isotropic/birks165
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=2.50 delta_E_MeV=0.125 detector_mat=EJ331 birks_constant=0.165 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/2500keV/isotropic/birks145
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=2.50 delta_E_MeV=0.125 detector_mat=EJ331 birks_constant=0.145 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/2500keV/isotropic/birks126
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=2.50 delta_E_MeV=0.125 detector_mat=EJ331 birks_constant=0.126 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/2500keV/isotropic/birks086
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=2.50 delta_E_MeV=0.125 detector_mat=EJ331 birks_constant=0.086 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
###3.5 MeV EJ331
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/3500keV/isotropic/birks165
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=3.50 delta_E_MeV=0.125 detector_mat=EJ331 birks_constant=0.165 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/3500keV/isotropic/birks145
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=3.50 delta_E_MeV=0.125 detector_mat=EJ331 birks_constant=0.145 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/3500keV/isotropic/birks126
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=3.50 delta_E_MeV=0.125 detector_mat=EJ331 birks_constant=0.126 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/3500keV/isotropic/birks086
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=3.50 delta_E_MeV=0.125 detector_mat=EJ331 birks_constant=0.086 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
###5.0 MeV EJ331
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/5000keV/isotropic/birks086
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=5.00 delta_E_MeV=0.125 detector_mat=EJ331 birks_constant=0.086 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
###5.5 MeV EJ331
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/5500keV/isotropic/birks165
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=5.50 delta_E_MeV=0.125 detector_mat=EJ331 birks_constant=0.165 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/5500keV/isotropic/birks145
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=5.50 delta_E_MeV=0.125 detector_mat=EJ331 birks_constant=0.145 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/5500keV/isotropic/birks126
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=5.50 delta_E_MeV=0.125 detector_mat=EJ331 birks_constant=0.126 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/5500keV/isotropic/birks086
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=5.50 delta_E_MeV=0.125 detector_mat=EJ331 birks_constant=0.086 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
###6.0 MeV EJ331
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/6000keV/isotropic/birks165
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=6.00 delta_E_MeV=0.125 detector_mat=EJ331 birks_constant=0.165 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/6000keV/isotropic/birks145
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=6.00 delta_E_MeV=0.125 detector_mat=EJ331 birks_constant=0.145 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/6000keV/isotropic/birks126
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=6.00 delta_E_MeV=0.125 detector_mat=EJ331 birks_constant=0.126 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/6000keV/isotropic/birks086
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=6.00 delta_E_MeV=0.125 detector_mat=EJ331 birks_constant=0.086 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*







###2.5 MeV NE213A
cd /drive2/EJ321P_char_simulations/NE213A/neutrons/neutron_range/2500keV/isotropic/birks145
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=2.50 delta_E_MeV=0.125 detector_mat=NE213A birks_constant=0.145 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/NE213A/neutrons/neutron_range/2500keV/isotropic/birks126
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=2.50 delta_E_MeV=0.125 detector_mat=NE213A birks_constant=0.126 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/NE213A/neutrons/neutron_range/2500keV/isotropic/birks100
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=2.50 delta_E_MeV=0.125 detector_mat=NE213A birks_constant=0.100 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/NE213A/neutrons/neutron_range/2500keV/isotropic/birks0917
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=2.50 delta_E_MeV=0.125 detector_mat=NE213A birks_constant=0.0917 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/NE213A/neutrons/neutron_range/2500keV/isotropic/birks085
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=2.50 delta_E_MeV=0.125 detector_mat=NE213A birks_constant=0.085 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/NE213A/neutrons/neutron_range/2500keV/isotropic/birks060
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=2.50 delta_E_MeV=0.125 detector_mat=NE213A birks_constant=0.060 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*














cd /drive2/EJ321P_char_simulations/NE213A/neutrons/neutron_range/2500keV/isotropic/birks1073
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=2.5 delta_E_MeV=0.125 detector_mat=NE213A distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/NE213A/neutrons/neutron_range/3500keV/isotropic/birks1073
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=3.5 delta_E_MeV=0.125 detector_mat=NE213A distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/NE213A/neutrons/neutron_range/4500keV/isotropic/birks1073
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=4.5 delta_E_MeV=0.125 detector_mat=NE213A distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/NE213A/neutrons/neutron_range/5500keV/isotropic/birks1073
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=5.5 delta_E_MeV=0.125 detector_mat=NE213A distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/NE213A/neutrons/neutron_range/6000keV/isotropic/birks1073
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=6.0 delta_E_MeV=0.125 detector_mat=NE213A distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*







#NE213A neutron energies with optimal Birks at 5 MeV
cd /drive2/EJ321P_char_simulations/NE213A/neutrons/neutron_range/2500keV/isotropic/birks0917
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=2.5 delta_E_MeV=0.125 detector_mat=NE213A distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/NE213A/neutrons/neutron_range/3500keV/isotropic/birks0917
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=3.5 delta_E_MeV=0.125 detector_mat=NE213A distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/NE213A/neutrons/neutron_range/4500keV/isotropic/birks0917
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=4.5 delta_E_MeV=0.125 detector_mat=NE213A distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/NE213A/neutrons/neutron_range/5500keV/isotropic/birks0917
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=5.5 delta_E_MeV=0.125 detector_mat=NE213A distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
#EJ305 neutron energies with optimal Birks at 5 MeV
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/2000keV/isotropic/birks1073
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=2.0 delta_E_MeV=0.125 detector_mat=EJ305 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/2500keV/isotropic/birks1073
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=2.5 delta_E_MeV=0.125 detector_mat=EJ305 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/3000keV/isotropic/birks1073
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=3.0 delta_E_MeV=0.125 detector_mat=EJ305 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/3500keV/isotropic/birks1073
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=3.5 delta_E_MeV=0.125 detector_mat=EJ305 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/4000keV/isotropic/birks1073
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=4.0 delta_E_MeV=0.125 detector_mat=EJ305 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/4500keV/isotropic/birks1073
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=4.5 delta_E_MeV=0.125 detector_mat=EJ305 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/5000keV/isotropic/birks1073
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=5.0 delta_E_MeV=0.125 detector_mat=EJ305 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/5500keV/isotropic/birks1073
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=5.5 delta_E_MeV=0.125 detector_mat=EJ305 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/6000keV/isotropic/birks1073
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=6.0 delta_E_MeV=0.125 detector_mat=EJ305 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
#EJ321P neutron energies with optimal Birks at 5 MeV
#EJ331 neutron energies with optimal Birks at 5 MeV
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/2000keV/isotropic/birks1179
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=2.0 delta_E_MeV=0.125 detector_mat=EJ331 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/2500keV/isotropic/birks1179
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=2.5 delta_E_MeV=0.125 detector_mat=EJ331 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/3000keV/isotropic/birks1179
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=3.0 delta_E_MeV=0.125 detector_mat=EJ331 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/3500keV/isotropic/birks1179
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=3.5 delta_E_MeV=0.125 detector_mat=EJ331 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/4000keV/isotropic/birks1179
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=4.0 delta_E_MeV=0.125 detector_mat=EJ331 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/4500keV/isotropic/birks1179
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=4.5 delta_E_MeV=0.125 detector_mat=EJ331 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/5000keV/isotropic/birks1179
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=5.0 delta_E_MeV=0.125 detector_mat=EJ331 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/5500keV/isotropic/birks1179
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=5.5 delta_E_MeV=0.125 detector_mat=EJ331 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/6000keV/isotropic/birks1179
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=6.0 delta_E_MeV=0.125 detector_mat=EJ331 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*



#NE213A neutron energies with optimal Birks at 3 MeV
cd /drive2/EJ321P_char_simulations/NE213A/neutrons/neutron_range/2500keV/isotropic/birks1073
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=2.5 delta_E_MeV=0.125 detector_mat=NE213A distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/NE213A/neutrons/neutron_range/3500keV/isotropic/birks1073
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=3.5 delta_E_MeV=0.125 detector_mat=NE213A distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/NE213A/neutrons/neutron_range/4500keV/isotropic/birks1073
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=4.5 delta_E_MeV=0.125 detector_mat=NE213A distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/NE213A/neutrons/neutron_range/5500keV/isotropic/birks1073
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=5.5 delta_E_MeV=0.125 detector_mat=NE213A distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
#EJ305 neutron energies with optimal Birks at 3 MeV
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/2500keV/isotropic/birks1554
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=2.5 delta_E_MeV=0.125 detector_mat=EJ305 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/3500keV/isotropic/birks1554
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=3.5 delta_E_MeV=0.125 detector_mat=EJ305 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/4500keV/isotropic/birks1554
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=4.5 delta_E_MeV=0.125 detector_mat=EJ305 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/5500keV/isotropic/birks1554
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=5.5 delta_E_MeV=0.125 detector_mat=EJ305 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*

cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/2000keV/isotropic/birks1554
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=2.0 delta_E_MeV=0.125 detector_mat=EJ305 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/3000keV/isotropic/birks1554
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=3.0 delta_E_MeV=0.125 detector_mat=EJ305 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/4000keV/isotropic/birks1554
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=4.0 delta_E_MeV=0.125 detector_mat=EJ305 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/5000keV/isotropic/birks1554
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=5.0 delta_E_MeV=0.125 detector_mat=EJ305 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/6000keV/isotropic/birks1554
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=6.0 delta_E_MeV=0.125 detector_mat=EJ305 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*

#EJ321P neutron energies with optimal Birks at 3 MeV
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/2500keV/isotropic/birks2197
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=2.5 delta_E_MeV=0.125 detector_mat=EJ321P distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/3500keV/isotropic/birks2197
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=3.5 delta_E_MeV=0.125 detector_mat=EJ321P distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/4500keV/isotropic/birks2197
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=4.5 delta_E_MeV=0.125 detector_mat=EJ321P distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/5500keV/isotropic/birks2197
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=5.5 delta_E_MeV=0.125 detector_mat=EJ321P distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
#EJ331 neutron energies with optimal Birks at 3 MeV
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/2500keV/isotropic/birks1393
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=2.5 delta_E_MeV=0.125 detector_mat=EJ331 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/3500keV/isotropic/birks1393
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=3.5 delta_E_MeV=0.125 detector_mat=EJ331 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/4500keV/isotropic/birks1393
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=4.5 delta_E_MeV=0.125 detector_mat=EJ331 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/5500keV/isotropic/birks1393
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=5.5 delta_E_MeV=0.125 detector_mat=EJ331 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*

###################################################


#NE213A neutron energies with optimal Birks at 4 MeV
cd /drive2/EJ321P_char_simulations/NE213A/neutrons/neutron_range/2500keV/isotropic/birks0933
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=2.5 delta_E_MeV=0.125 detector_mat=NE213A distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/NE213A/neutrons/neutron_range/3500keV/isotropic/birks0933
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=3.5 delta_E_MeV=0.125 detector_mat=NE213A distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/NE213A/neutrons/neutron_range/4500keV/isotropic/birks0933
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=4.5 delta_E_MeV=0.125 detector_mat=NE213A distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/NE213A/neutrons/neutron_range/5500keV/isotropic/birks0933
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=5.5 delta_E_MeV=0.125 detector_mat=NE213A distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/NE213A/neutrons/neutron_range/6000keV/isotropic/birks0933
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=6.0 delta_E_MeV=0.125 detector_mat=NE213A distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
#EJ305 neutron energies with optimal Birks at 4 MeV
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/2500keV/isotropic/birks1197
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=2.5 delta_E_MeV=0.125 detector_mat=EJ305 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/3500keV/isotropic/birks1197
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=3.5 delta_E_MeV=0.125 detector_mat=EJ305 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/4500keV/isotropic/birks1197
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=4.5 delta_E_MeV=0.125 detector_mat=EJ305 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/5500keV/isotropic/birks1197
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=5.5 delta_E_MeV=0.125 detector_mat=EJ305 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
#EJ321P neutron energies with optimal Birks at 4 MeV
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/2500keV/isotropic/birks1975
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=2.5 delta_E_MeV=0.125 detector_mat=EJ321P distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/3500keV/isotropic/birks1975
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=3.5 delta_E_MeV=0.125 detector_mat=EJ321P distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/4500keV/isotropic/birks1975
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=4.5 delta_E_MeV=0.125 detector_mat=EJ321P distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/5000keV/isotropic/birks1975
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=5.0 delta_E_MeV=0.125 detector_mat=EJ321P distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/5500keV/isotropic/birks1975
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=5.5 delta_E_MeV=0.125 detector_mat=EJ321P distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/6000keV/isotropic/birks1975
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=6.0 delta_E_MeV=0.125 detector_mat=EJ321P distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
#EJ331 neutron energies with optimal Birks at 4 MeV
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/2500keV/isotropic/birks1270
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=2.5 delta_E_MeV=0.125 detector_mat=EJ331 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/3500keV/isotropic/birks1270
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=3.5 delta_E_MeV=0.125 detector_mat=EJ331 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/4500keV/isotropic/birks1270
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=4.5 delta_E_MeV=0.125 detector_mat=EJ331 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/5500keV/isotropic/birks1270
ess_ej321pchar_sim -n100000 event_gen=neutron_range E_MeV=5.5 delta_E_MeV=0.125 detector_mat=EJ331 distance=925 cone_opening_deg=6 -j16 -l ESS_EJ321Pchar -s111
ess_ej321pchar_ana ej321pchar.*
rm ej321pchar.*


########################################################


#make folders for 0.5 MeV bins 5 MeV optimized
cd /drive2/EJ321P_char_simulations/NE213A/neutrons/neutron_range/2500keV/isotropic/
mkdir birks0917
cd /drive2/EJ321P_char_simulations/NE213A/neutrons/neutron_range/3500keV/isotropic/
mkdir birks0917
cd /drive2/EJ321P_char_simulations/NE213A/neutrons/neutron_range/4500keV/isotropic/
mkdir birks0917
cd /drive2/EJ321P_char_simulations/NE213A/neutrons/neutron_range/5500keV/isotropic/
mkdir birks0917

cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/2000keV/isotropic/
mkdir birks1073
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/2500keV/isotropic/
mkdir birks1073
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/3000keV/isotropic/
mkdir birks1073
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/3500keV/isotropic/
mkdir birks1073
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/4000keV/isotropic/
mkdir birks1073
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/4500keV/isotropic/
mkdir birks1073
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/5000keV/isotropic/
mkdir birks1073
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/5500keV/isotropic/
mkdir birks1073
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/6000keV/isotropic/
mkdir birks1073

cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/2500keV/isotropic/
mkdir birks1828
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/3500keV/isotropic/
mkdir birks1828
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/4500keV/isotropic/
mkdir birks1828
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/5500keV/isotropic/
mkdir birks1828
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/2500keV/isotropic/
mkdir birks1276
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/3500keV/isotropic/
mkdir birks1276
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/4500keV/isotropic/
mkdir birks1276
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/5500keV/isotropic/
mkdir birks1276

cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/2000keV/isotropic/
mkdir birks1179
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/2500keV/isotropic/
mkdir birks1179
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/3000keV/isotropic/
mkdir birks1179
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/3500keV/isotropic/
mkdir birks1179
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/4000keV/isotropic/
mkdir birks1179
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/4500keV/isotropic/
mkdir birks1179
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/5000keV/isotropic/
mkdir birks1179
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/5500keV/isotropic/
mkdir birks1179
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/6000keV/isotropic/
mkdir birks1179

#make folders for 0.5 MeV bins 4 MeV optimized
cd /drive2/EJ321P_char_simulations/NE213A/neutrons/neutron_range/2000keV/isotropic/
mkdir birks0933
cd /drive2/EJ321P_char_simulations/NE213A/neutrons/neutron_range/2500keV/isotropic/
mkdir birks0933
cd /drive2/EJ321P_char_simulations/NE213A/neutrons/neutron_range/3000keV/isotropic/
mkdir birks0933
cd /drive2/EJ321P_char_simulations/NE213A/neutrons/neutron_range/3500keV/isotropic/
mkdir birks0933
cd /drive2/EJ321P_char_simulations/NE213A/neutrons/neutron_range/4000keV/isotropic/
mkdir birks0933
cd /drive2/EJ321P_char_simulations/NE213A/neutrons/neutron_range/4500keV/isotropic/
mkdir birks0933
cd /drive2/EJ321P_char_simulations/NE213A/neutrons/neutron_range/5000keV/isotropic/
mkdir birks0933
cd /drive2/EJ321P_char_simulations/NE213A/neutrons/neutron_range/5500keV/isotropic/
mkdir birks0933
cd /drive2/EJ321P_char_simulations/NE213A/neutrons/neutron_range/6000keV/isotropic/
mkdir birks0933
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/2000keV/isotropic/
mkdir birks1197
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/2500keV/isotropic/
mkdir birks1197
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/3000keV/isotropic/
mkdir birks1197
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/3500keV/isotropic/
mkdir birks1197
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/4000keV/isotropic/
mkdir birks1197
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/4500keV/isotropic/
mkdir birks1197
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/5000keV/isotropic/
mkdir birks1197
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/5500keV/isotropic/
mkdir birks1197
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/6000keV/isotropic/
mkdir birks1197
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/2000keV/isotropic/
mkdir birks1975
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/2500keV/isotropic/
mkdir birks1975
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/3000keV/isotropic/
mkdir birks1975
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/3500keV/isotropic/
mkdir birks1975
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/4000keV/isotropic/
mkdir birks1975
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/4500keV/isotropic/
mkdir birks1975
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/5000keV/isotropic/
mkdir birks1975
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/5500keV/isotropic/
mkdir birks1975
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/6000keV/isotropic/
mkdir birks1975
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/2000keV/isotropic/
mkdir birks1270
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/2500keV/isotropic/
mkdir birks1270
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/3000keV/isotropic/
mkdir birks1270
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/3500keV/isotropic/
mkdir birks1270
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/4000keV/isotropic/
mkdir birks1270
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/4500keV/isotropic/
mkdir birks1270
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/5000keV/isotropic/
mkdir birks1270
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/5500keV/isotropic/
mkdir birks1270
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/6000keV/isotropic/
mkdir birks1270


#make folders for 0.5 MeV bins 3 MeV optimized
cd /drive2/EJ321P_char_simulations/NE213A/neutrons/neutron_range/2500keV/isotropic/
mkdir birks1073
cd /drive2/EJ321P_char_simulations/NE213A/neutrons/neutron_range/3500keV/isotropic/
mkdir birks1073
cd /drive2/EJ321P_char_simulations/NE213A/neutrons/neutron_range/4500keV/isotropic/
mkdir birks1073
cd /drive2/EJ321P_char_simulations/NE213A/neutrons/neutron_range/5500keV/isotropic/
mkdir birks1073
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/2000keV/isotropic/
mkdir birks1554
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/2500keV/isotropic/
mkdir birks1554
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/3000keV/isotropic/
mkdir birks1554
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/3500keV/isotropic/
mkdir birks1554
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/4000keV/isotropic/
mkdir birks1554
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/4500keV/isotropic/
mkdir birks1554
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/5000keV/isotropic/
mkdir birks1554
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/5500keV/isotropic/
mkdir birks1554
cd /drive2/EJ321P_char_simulations/EJ305/neutrons/neutron_range/6000keV/isotropic/
mkdir birks1554
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/2500keV/isotropic/
mkdir birks2197
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/3500keV/isotropic/
mkdir birks2197
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/4500keV/isotropic/
mkdir birks2197
cd /drive2/EJ321P_char_simulations/EJ321P/neutrons/neutron_range/5500keV/isotropic/
mkdir birks2197
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/2500keV/isotropic/
mkdir birks1393
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/3500keV/isotropic/
mkdir birks1393
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/4500keV/isotropic/
mkdir birks1393
cd /drive2/EJ321P_char_simulations/EJ331/neutrons/neutron_range/5500keV/isotropic/
mkdir birks1393