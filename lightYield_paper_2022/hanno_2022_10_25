#!/usr/bin/env python3
from re import I
import sys
from tkinter import X
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
from numpy.core.arrayprint import SubArrayFormat
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit

# from matplotlib.colors import LinearSegmentedColormap


# IMPORT FROM LIBRARY DIRECTORY
sys.path.insert(0, "../library/")
sys.path.insert(0, "../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "../")
sys.path.insert(0, "data_analysis/")

import processing_math as promath
import processing_data as prodata
import processing_pd as propd
import processing_simulation as prosim
import figure_style as fs

# ##########################################################
# ######### Fundamental parameters #########################
# ##########################################################
pathSim = '/media/gheed/Seagate_Expansion_Drive1/data/simulation'
pathData = '/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper'

#############################################################
# 
# 

def firstDerivativeHelper(y_data, window):
    """
    Function takes binned data as calculate the first derivative.
    Linear interpolation between +-'window' around each data point is fitted and the sloped saved.

    -----------------------------
    Nicholai Mauritzson
    2022-10-25
    """
    data_slope = np.zeros(len(y_data))
    for i in range(len(y_data)):
        # if not(i-window<0 or i+window>len(y_data)):
        try:     
            popt, pcov = curve_fit( promath.linearFunc, 
                                    np.arange(2*window + 1), 
                                    y_data[i-window:i+window+1])
            data_slope[i] = popt[0]
            # print(data_slope[i])
        except Exception as err : 
            print(f'Warning:{err} at index={i}/{len(y_data)}') 

    return data_slope

energy = 6000
gate = 'SG'
detector = 'EJ331'

numBins = np.arange(0, 4.5, 0.05)

nQDC       = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/nQDC_{gate}.npy')
randQDC    = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/randQDC_{gate}.npy')
nLength    = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/nLength.npy')
randLength = np.load(f'{pathData}/{detector}/TOF_slice/QDC/keV{energy}/randLength.npy')
nQDC = prodata.calibrateMyDetector(f'{pathData}/{detector}/Ecal/popt_{gate}.npy', nQDC)
randQDC = prodata.calibrateMyDetector(f'{pathData}/{detector}/Ecal/popt_{gate}.npy', randQDC)

x_data, y_data = prodata.randomTOFSubtractionQDC( nQDC,
                                                randQDC,
                                                nLength, 
                                                randLength,
                                                numBins, 
                                                plot=False)






window = 5
y_smooth = firstDerivativeHelper(y_data, window)
FD_loc = np.argmin(y_smooth)


plt.title(f'Energy = {energy} keV')
plt.step(x_data, y_data, label='original')
plt.step(x_data, y_smooth, label=f'window = {window}')

plt.vlines(x_data[FD_loc], ymin=-100, ymax=np.max(y_data)/2, color='black', lw=0.5, label=x_data[FD_loc])
plt.legend()
plt.show()

