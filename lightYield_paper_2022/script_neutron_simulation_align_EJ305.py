#!/usr/bin/env python3
from re import I
import sys
from tkinter import X
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
from numpy.core.arrayprint import SubArrayFormat
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit

# IMPORT FROM LIBRARY DIRECTORY
sys.path.insert(0, "../library/")
sys.path.insert(0, "../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "../")
sys.path.insert(0, "data_analysis/")

import processing_math as promath
import processing_data as prodata
import processing_pd as propd
import processing_simulation as prosim
import figure_style as fs

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# FUNCTIONS
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def loadNeutronSimEnergies(path, columnNames, detector='NE213A', E_range=np.arange(1000,7250,250), keepCol='optPhotonSumNPQE', PMT_gain=4e6):
        """
        Routine for loading simulated neutron data and merging into one pandas DataFrame.
        For both isotropic and pencilbeam data.
        """
        
        q = 1.60217662E-19 #charge of a single electron

        # sim_pen = pd.DataFrame()
        sim_iso = pd.DataFrame()
        
        for E in E_range:
                print(f'Simulation Load: Energy = {E} keV')
                # #Load data                
                # pen_data = pd.read_csv(path + f"{detector}/neutrons/pencilbeam/{E}keV/CSV_optphoton_data_sum.csv", names=columnNames)[f'{keepCol}']
                # #Randomize data
                # y, x = np.histogram(pen_data, bins=4096, range=[0, 4096])
                # pen_data = prodata.getRandDist(x, y)
                # #QDC calibrate data
                # # pen_data = prosim.chargeCalibration(pen_data * q * PMT_gain)
                # #Save data to DataFrame
                # sim_pen[f'keV{E}'] = pd.Series(pen_data)

                #Load data                
                iso_data = pd.read_csv(path + f"{detector}/neutrons/neutron_range/{E}keV/isotropic/CSV_optphoton_data_sum.csv", names=columnNames)[f'{keepCol}']
                #Randomize data
                y, x = np.histogram(iso_data, bins=4096, range=[0, 4096])
                iso_data = prodata.getRandDist(x, y)
                #QDC calibrate data
                # iso_data = prosim.chargeCalibration(iso_data * q * PMT_gain)
                #Save data to DataFrame
                sim_iso[f'keV{E}'] = pd.Series(iso_data)
        
        #Reset index before return
        # sim_pen =  sim_pen.reset_index()
        sim_iso =  sim_iso.reset_index()
        
        return sim_iso

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# SETTING PARAMETERS
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
PMT_gain = 4e6 #Nominal gain is 7'000'000 at nominal A/lm (model: 9821B p1)
att_factor_11dB = 3.55
att_factor_12dB = 3.98
att_factor_16_5dB = 6.68
q = 1.60217662E-19 #charge of a single electron

detector = 'EJ305'

distance = 0.959

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# LOAD SIMULATION DATA
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
path_sim = '/media/gheed/Seagate_Expansion_Drive1/data/simulation/'
columnNames = [ 'evtNum', 
                'optPhotonSum', 
                'optPhotonSumQE', 
                'optPhotonSumCompton', 
                'optPhotonSumComptonQE', 
                'optPhotonSumNPQE', 
                'xLoc', 
                'yLoc', 
                'zLoc', 
                'CsMin', 
                'optPhotonParentID', 
                'optPhotonParentCreatorProcess', 
                'optPhotonParentStartingEnergy',
                'total_edep',
                'gamma_edep',
                'proton_edep',
                'nScatters',
                'nScattersProton']

sim_iso = loadNeutronSimEnergies(       path = path_sim, 
                                        columnNames = columnNames, 
                                        detector = detector, 
                                        E_range = np.arange(1000, 6500, 250),
                                        keepCol='optPhotonSumNPQE')

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# LOADING QDC DATA
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

x_data = np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{1500}/x.npy')

data = pd.DataFrame()
for E in np.arange(1500, 6500, 250):
        print(f'Data Load: Energy = {E} keV')
        data[f'keV{E}'] = [prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/FOM_paper/{detector}/Ecal', prodata.getRandDist(x_data, np.load(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/keV{E}/yQDC.npy'))*att_factor_16_5dB) ]


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# ALIGN SIMULATIONS WITH QDC DATA
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

alignINFO = pd.DataFrame({   'energy'        :[1500,                           1750,                         2000,                           2250,                          2500,                          2750,                          3000,                          3250,                       3500,                      3750,                        4000,                         4250,                       4500,                         4750,                       5000,                       5250,                       5500,                      5750,                       6000,                       6250],
                             'scale'         :[0.04,                           0.04,                         0.05,                           0.07,                          0.068,                         0.068,                         0.08,                          0.35,                       0.32,                      0.30,                        0.31,                         0.2618012,                  0.2618012,                    0.2618012,                  0.2618012,                  0.2618012,                  0.2600624,                 0.2211852,                  0.1606004,                  0.0765848],
                             'gain'          :[0.00240322,                     0.00240322,                   0.00240322,                     0.00300322,                    0.00300322,                    0.00300322,                    0.00320322,                    0.00340322,                 0.00340322,                0.00394516,                  0.00404516,                   0.00404516,                 0.00404516,                   0.00404516,                 0.00404516,                 0.00404516,                 0.0043129,                 0.00440322,                 0.00436774,                 0.0042871],
                             'smear'         :[0.03,                           0.03,                         0.06,                           0.05,                          0.08,                          0.05,                          0.035,                         0.035,                      0.035,                     0.03,                        0.03,                         0.03,                       0.03,                         0.03,                       0.03,                       0.03,                       0.01,                      0.01,                       0.01,                       0.01],
                             'scaleBound'    :[[0.005, 0.10],                  [0.005, 0.10],                [0.005, 0.10],                  [0.005, 0.10],                 [0.005, 0.10],                 [0.005, 0.10],                 [0.005, 0.10],                 [0.01, 0.50],               [0.01, 0.50],              [0.01, 0.50],                [0.01, 0.50],                 [0.01, 0.50],               [0.01, 0.50],                 [0.01, 0.50],               [0.01, 0.50],               [0.01, 0.50],               [0.01, 0.50],              [0.01, 0.50],               [0.01, 0.50],               [0.01, 0.50]],
                             'gainBound'     :[[0.001, 0.01],                  [0.001, 0.01],                [0.001, 0.01],                  [0.001, 0.01],                 [0.001, 0.01],                 [0.001, 0.01],                 [0.001, 0.01],                 [0.001, 0.01],              [0.001, 0.01],             [0.001, 0.01],               [0.001, 0.01],                [0.001, 0.01],              [0.001, 0.01],                [0.001, 0.01],              [0.001, 0.01],              [0.001, 0.01],              [0.001, 0.01],             [0.001, 0.01],              [0.001, 0.01],              [0.001, 0.01]],
                             'binRange'      :[np.arange(0.134, 0.316, 0.015), np.arange(0.151, 0.41, 0.015),np.arange(0.167, 0.508, 0.025), np.arange(0.252, 0.606, 0.03), np.arange(0.252, 0.702, 0.03), np.arange(0.356, 0.890, 0.03), np.arange(0.454, 1.07, 0.05), np.arange(0.59, 1.12, 0.04),np.arange(0.63, 1.31, 0.04), np.arange(0.76, 1.5, 0.05), np.arange(0.89, 1.66, 0.05), np.arange(1.08, 1.78, 0.05), np.arange(1.71, 2.03, 0.05),  np.arange(1.28, 2.2, 0.05), np.arange(1.47, 2.41, 0.08), np.arange(1.65, 2.54, 0.08), np.arange(1.7, 2.7, 0.1), np.arange(1.89, 2.9, 0.1), np.arange(2.1, 2.96, 0.1), np.arange(2.2, 3.18, 0.12)],
                             })

dfAlign = pd.DataFrame()
plot=True
for E in alignINFO.energy:
        print('------------------------------------------')
        print(f'Aligning: {E} keV')

        #smearing data before fitting:
        # for i in range(len()):
        # model = 
        smearFactor = alignINFO.query(f'energy=={E}').smear.item()

        outTMP = prosim.PMTGainAlignNeutrons(model = sim_iso[f'keV{E}'].apply(lambda x: prosim.gaussSmear(x, smearFactor)),#simulations
                                             data = data[f'keV{E}'][0],
                                             scale = alignINFO.query(f'energy=={E}').scale.item(),
                                             gain = alignINFO.query(f'energy=={E}').gain.item(),
                                             scaleBound = [alignINFO.query(f'energy=={E}').scaleBound.item()[0], alignINFO.query(f'energy=={E}').scaleBound.item()[1]],
                                             gainBound = [alignINFO.query(f'energy=={E}').gainBound.item()[0], alignINFO.query(f'energy=={E}').gainBound.item()[1]],
                                             stepSize = 1e-3,
                                             binRange = alignINFO.query(f'energy=={E}').binRange.item(),
                                             plot = plot)

        #append current energy to dictionary 'outTMP'
        outTMP.update({'energy':E})

        #save results to main DataFrame
        dfAlign = dfAlign.append(pd.DataFrame([outTMP]))
dfAlign = dfAlign.reset_index(drop=True)

#Save Align data to disk
dfAlign.to_csv(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/neutrons/neutron_align_data/alignData.zip')


plt.scatter(dfAlign.energy, dfAlign.gain)
plt.show()



####REMOVE ME#####
binRange = np.arange(0.1, 8, 0.025)
for E in alignINFO.energy:
        numBins = (alignINFO.query(f'energy=={E}').binRange.item().max()-alignINFO.query(f'energy=={E}').binRange.item().min())/len(alignINFO.query(f'energy=={E}').binRange.item())
        yData, xData = np.histogram(data[f'keV{E}'][0], bins=np.arange(0.1,10,numBins))#alignINFO.query(f'energy=={E}').binRange.item())
        xData = prodata.getBinCenters(xData)

        ySim, xSim = np.histogram(sim_iso[f'keV{E}']*alignINFO.query(f'energy=={E}').gain.item(), bins=np.arange(0.1,10,numBins))#alignINFO.query(f'energy=={E}').binRange.item())
        xSim = prodata.getBinCenters(xSim)

        plt.title(E)
        plt.vlines(alignINFO.query(f'energy=={E}').binRange.item().min(), 0, 4000, color='black')
        plt.vlines(alignINFO.query(f'energy=={E}').binRange.item().max(), 0, 4000, color='black')
        plt.step(xData, yData, label='Data')
        plt.step(xSim, ySim*alignINFO.query(f'energy=={E}').scale.item(), label='Sim')
        plt.ylim([0, max(yData*1.3)])
        plt.xlim([0, alignINFO.query(f'energy=={E}').binRange.item().max()*1.3])
        plt.legend()
        # plt.yscale('log')
        plt.show()

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# FIT PENCILBEAM DATA AND DERIVE Tn to MeVee
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#A fit will get position in MeVee
fitINFO = pd.DataFrame({   'energy'        :[1500,                     1750,                         2000,                           2250,                          2500,                          2750,                          3000,                          3250,                       3500,                      3750,                        4000,                         4250,                       4500,                         4750,                       5000,                       5250,                       5500,                      5750,                       6000,                       6250],
                           'start'         :[0.336,                    0.399,                        0.467,                          0.536,                         0.620,                         0.686,                         0.804,                         0.900,                      1.037,                     1.150,                       1.220,                        1.290,                      1.500,                        1.642,                      1.895,                      1.820,                      2.000,                     2.220,                      2.300,                      2.390],
                           'stop'          :[0.612,                    0.726,                        0.839,                          0.973,                         1.100,                         1.200,                         1.300,                         1.500,                      1.800,                     1.900,                       2.100,                        2.580,                      2.720,                        2.950,                      3.111,                      3.291,                      3.530,                     3.790,                      3.900,                      3.950],
                           'binRange'      :[np.arange(0.1, 10, 0.02), np.arange(0.1, 10, 0.02),     np.arange(0.1, 10, 0.02),       np.arange(0.1, 10, 0.02),      np.arange(0.1, 10, 0.02),      np.arange(0.1, 10, 0.02),      np.arange(0.1, 10, 0.03),      np.arange(0.1, 10, 0.03),   np.arange(0.1, 10, 0.03),  np.arange(0.1, 10, 0.04),    np.arange(0.1, 10, 0.04),     np.arange(0.1, 10, 0.04),   np.arange(0.1, 10, 0.04),     np.arange(0.1, 10, 0.04),   np.arange(0.1, 10, 0.06),   np.arange(0.1, 10, 0.06),   np.arange(0.1, 10, 0.06),  np.arange(0.1, 10, 0.06),   np.arange(0.1, 10, 0.08),   np.arange(0.1, 10, 0.1)]
                           })


#Apply gain to pencilbeam data.
dfFit = pd.DataFrame()

for E in dfAlign.energy:
        

        y,x = np.histogram(sim_iso[f'keV{E}'] * dfAlign.query(f'energy=={E}').gain.item(), bins=fitINFO.query(f'energy=={E}').binRange.item())
        x = prodata.getBinCenters(x)
        start = fitINFO.query(f'energy=={E}').start.item()
        stop = fitINFO.query(f'energy=={E}').stop.item()
        parameters = prodata.numericAverage(x, y, fitLim=[start, stop], stdFactor = 3)
        parameters.update({'energy':E})
        #save results to main DataFrame
        dfFit = dfFit.append(pd.DataFrame([parameters]))
        

        plt.title(f'{E} keV')
        popt, pcov = promath.gaussFit(x, y, fitINFO.query(f'energy=={E}').start.item(), fitINFO.query(f'energy=={E}').stop.item(), error=True)

        # if E == 1750:
        plt.plot(np.arange(0,10, 0.01), promath.gaussFunc(np.arange(0,10, 0.01), popt[0],popt[1],popt[2]))
        plt.step(x,y)
        plt.vlines(dfFit.query(f'energy=={E}').meanVal.item(), ymin=0, ymax=max(y), color='black', ls='dashed')
        plt.vlines(fitINFO.query(f'energy=={E}').start.item(), ymin=0, ymax=max(y), color='green', lw=2)
        plt.vlines(fitINFO.query(f'energy=={E}').stop.item(), ymin=0, ymax=max(y), color='green', lw=2)
        plt.xlim([0, popt[1]*2.5])
        plt.ylim([0, popt[0]*1.2])
        plt.show()

dfFit = dfFit.reset_index(drop=True)

plt.scatter(dfFit.meanVal, dfFit.energy)
plt.errorbar(dfFit.meanVal, dfFit.energy, xerr=dfFit.err_meanVal, ls='none')
plt.ylabel('L [MeVee]')
plt.xlabel('Tn [MeV]')
plt.show()



# # kornilovEq(Ep, L0, L1):
# # cecilEq(Ep, C):

# curve_fit 

# plt.figure()
# plt.scatter(dfFit.energy/1000, dfFit.meanVal)
# plt.title(f'{detector}')
# plt.ylabel('L [MeV$_{ee}$]')
# plt.xlabel('T$_n$ [MeV]')
# # plt.show()



# ##### REMOVE ME ######
# plt.figure()
# binRange = np.arange(0,300,3)
# plt.suptitle(f'{detector}')
# plt.subplot(1, 3,1)
# for E in np.arange(2000, 7000, 1000):
#         plt.hist(sim_iso[f'keV{E}'].dropna(), bins=binRange, label=f'T$_n$ = {E} keV', histtype='step', lw=2)
# plt.title('Isotropic neutrons, dE=0.125 keV')
# plt.xlabel('# scint. photons')
# plt.ylabel('Counts')
# plt.legend()

# plt.subplot(1,3,2)
# for E in np.arange(2000, 7000, 1000):
#         plt.hist(sim_pen[f'keV{E}'].dropna(), bins=binRange, label=f'T$_n$ = {E} keV', histtype='step', lw=2)
# plt.title('Pencilbeam neutrons, dE=0')
# plt.xlabel('# scint. photons')
# # plt.ylabel('Counts')
# plt.legend()


# plt.subplot(1,3,3)
# binRange = np.arange(0, 5, 0.07)
# for E in np.arange(2000, 7000, 1000):
#         plt.hist(data[f'keV{E}'][0], bins=binRange, label=f'T$_n$ = {E} keV', histtype='step', lw=2)
# plt.title('Data ToF slice, dE=0.125')
# plt.xlabel('QDC')
# # plt.ylabel('Counts')
# plt.legend()


# plt.show()






# ### REMOVE ME########
# #Edep slice tests ############################
# E = 6000
# detector = "NE213A"

# pen_data = pd.read_csv(path_sim + f"{detector}/neutrons/isotropic/{E}keV/CSV_optphoton_data_sum.csv", names=columnNames)

# cut0 = [1475-200, 1650-200]
# cut1 = [0, 5000]
# cut2 = [5935, 6050]
# cut3 = [6500, 8400]

# binRange = np.arange(0, 400,1)

# plt.subplot(5,1,1)
# plt.hist(pen_data.proton_edep, bins=256, range=[0,10000], alpha=0.5)
# plt.yscale('log')

# plt.vlines(cut0[0], ymin=0.1, ymax=1500, color='grey', lw=2)
# plt.vlines(cut0[1], ymin=0.1, ymax=1500, color='grey', lw=2)

# plt.vlines(cut1[0], ymin=0.1, ymax=1500, color='black', lw=2)
# plt.vlines(cut1[1], ymin=0.1, ymax=1500, color='black', lw=2)

# plt.vlines(cut2[0], ymin=0.1, ymax=1500, color='red', lw=2)
# plt.vlines(cut2[1], ymin=0.1, ymax=1500, color='red', lw=2)

# plt.vlines(cut3[0]+40, ymin=0.1, ymax=1500, color='blue', lw=2)
# plt.vlines(cut3[1], ymin=0.1, ymax=1500, color='blue', lw=2)


# plt.subplot(5,1,2)
# plt.hist(pen_data.query(f'proton_edep>{cut0[0]} and proton_edep<{cut0[1]}').optPhotonSumNPQE, bins=binRange, alpha=1, color='grey')

# plt.subplot(5,1,3)
# plt.hist(pen_data.query(f'proton_edep>{cut1[0]} and proton_edep<{cut1[1]}').optPhotonSumNPQE, bins=binRange, alpha=1, color='black')

# plt.subplot(5,1,4)
# plt.hist(pen_data.query(f'proton_edep>{cut2[0]} and proton_edep<{cut2[1]}').optPhotonSumNPQE, bins=binRange, alpha=1, color='red')

# plt.subplot(5,1,5)
# plt.hist(pen_data.query(f'proton_edep>{cut3[0]} and proton_edep<{cut3[1]}').optPhotonSumNPQE, bins=binRange, alpha=1, color='blue')
# plt.xlabel('#photons')

# plt.show()






# plt.hist2d(pen_data.proton_edep, pen_data.optPhotonSumNPQE, bins=128, range=[[-100, 6500],[-50,400]],norm=LogNorm())
# plt.show()







# ##############################################

# #load isotropic

# # iso_datakeV5000 = pd.read_csv(path_sim + f"{detector}/neutrons/isotropic/{5000}keV/CSV_optphoton_data_sum.csv", names=columnNames).optPhotonSumNPQE
# # pen_datakeV5000 = pd.read_csv(path_sim + f"{detector}/neutrons/pencilbeam/{5000}keV/CSV_optphoton_data_sum.csv", names=columnNames).optPhotonSumNPQE

# # iso_datakeV4000 = pd.read_csv(path_sim + f"{detector}/neutrons/isotropic/{4000}keV/CSV_optphoton_data_sum.csv", names=columnNames).optPhotonSumNPQE
# # pen_datakeV4000 = pd.read_csv(path_sim + f"{detector}/neutrons/pencilbeam/{4000}keV/CSV_optphoton_data_sum.csv", names=columnNames).optPhotonSumNPQE


# numBins = np.arange(0.1, 10, 0.065)

# # gainOff = 0.0115 # 5000 keV pen
# # scaleOff = 0.295  # 5000 keV pen
# # gainOff = 0.0119 # 4000 keV pen
# # scaleOff = 0.300  # 4000 keV pen

# gainOff = 0.012 # 5000 keV iso
# scaleOff = 1.45  # 5000 keV iso
# # gainOff = 0.0123 # 4000 keV iso
# # scaleOff = 1.45  # 4000 keV iso

# # yPen, xPen = np.histogram(sim_pen.keV4000*gainOff, bins=numBins)
# # xPen = prodata.getBinCenters(xPen)
# # plt.step(xPen, yPen*scaleOff, color='red', label='Simulation')

# yIso, xIso = np.histogram(sim_iso.keV5000*gainOff, bins=numBins)
# xIso = prodata.getBinCenters(xIso)
# plt.step(xIso, yIso*scaleOff, color='red', label='Simulation')

# smearFactor = 0.025
# sim_iso_keV5000_smear = sim_iso.keV5000.apply(lambda x: prosim.gaussSmear(x*gainOff, smearFactor)) 
# yPenSmear, xPenSmear = np.histogram(sim_iso_keV5000_smear, bins=numBins)
# xPenSmear = prodata.getBinCenters(xPenSmear)
# plt.step(xPenSmear, yPenSmear*scaleOff, color='green', label=f'Simulation incl. smear, sig={smearFactor}')
# # plt.errorbar(xPenSmear, yPenSmear*scaleOff, yerr=np.sqrt(yPenSmear), linestyle='none', color='red')

# # smearFactor = 0.075
# # sim_iso_keV4000_smear = sim_iso.keV4000.apply(lambda x: prosim.gaussSmear(x*gainOff, smearFactor)) 
# # yPenSmear, xPenSmear = np.histogram(sim_iso_keV4000_smear, bins=numBins)
# # xPenSmear = prodata.getBinCenters(xPenSmear)
# # plt.step(xPenSmear, yPenSmear*scaleOff, color='green', label=f'Simulation incl. smear, sig={smearFactor}')

# yData, xData = np.histogram(data.keV5000[0], bins=numBins)
# xData = prodata.getBinCenters(xData)
# plt.scatter(xData, yData, color='blue', label='Data')
# plt.errorbar(xData, yData, yerr=np.sqrt(yData), linestyle='none', color='blue')
# # plt.yscale('log')
# plt.legend()
# plt.xlim([0, 4])
# plt.show()
