#!/usr/bin/env python3
from re import I
import sys
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
from numpy.core.arrayprint import SubArrayFormat
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit

# IMPORT FROM LIBRARY DIRECTORY
sys.path.insert(0, "../library/")
sys.path.insert(0, "../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "../")
sys.path.insert(0, "data_analysis/")

import processing_math as promath
import processing_data as prodata
import processing_pd as propd
import processing_simulation as prosim


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#    DEFINE PARAMETERS FOR SLICE
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
distance = 0.959
gTOFRange = [0.5e-9, 6e-9] #Define range of gamma-flash
randTOFRange = [-710e-9, -10e-9] #Define range of random events
gLength = np.abs(gTOFRange[1]-gTOFRange[0]) #Calculate the lenght of cut for gammas
randLength = np.abs(randTOFRange[1]-randTOFRange[0]) #Calculate lenght of cut for random events

minE = 1625
maxE = 6625
dE = 250
nLength = pd.DataFrame()
for i in np.arange(minE, maxE, dE):
    E1 = round(i-dE,3)
    E2 = round(i,3)
    t1 = prodata.EnergytoTOF(E1/1000, distance)
    t2 = prodata.EnergytoTOF(E2/1000, distance)
    print(f'dE = {round(E1, 3)}-{round(E2, 3)} keV, center = {E1-dE/2} -> dt = {np.round((t1-t2)*1e9, 4)} ns')
    nLength[f'keV{int((i-dE/2))}'] = [np.abs(t1-t2)]

y2ThrQDC = 6450 #QDC threhold in channels
y3ThrQDC = 6000 #QDC threhold in channels
y4ThrQDC = 6000 #QDC threhold in channels
y5ThrQDC = 7700 #QDC threhold in channels
nThrQDC = 0 #QDC threshold in channels

gainOffsets = np.array([1.    , 0.9999, 0.9999, 0.9999, 0.9999, 0.9999, 0.9999, 0.9932,
       0.9906, 0.9942, 0.9926, 0.992 , 0.9934, 0.99  , 0.99  , 0.99  ,
       0.9942, 0.99  , 0.99  , 0.99  , 0.99  , 0.99  , 0.99  , 0.9829,
       0.99  , 0.99  , 0.9831, 0.9811, 0.9811, 0.9825, 0.9817, 0.9801,
       0.9803, 0.9821, 0.9801, 0.9801, 0.9813, 0.9811, 0.9803, 0.9801,
       0.9801, 0.9801, 0.9801, 0.9801, 0.9801, 0.9803, 0.9801, 0.9801,
       0.9803, 0.9744, 0.9801, 0.9732, 0.9801, 0.9801, 0.9732, 0.9738,
       0.9801, 0.9724, 0.9728, 0.9732, 0.9732, 0.9716, 0.9714, 0.9722,
       0.9718, 0.972 , 0.971 , 0.9702, 0.9704, 0.9702, 0.9702, 0.971 ,
       0.9702, 0.9704, 0.9702, 0.9702, 0.9801, 0.9704, 0.9702, 0.9704,
       0.9702, 0.9704, 0.9702, 0.9702, 0.9702, 0.9702, 0.9702, 0.9637,
       0.9702, 0.9649, 0.9625, 0.9702, 0.9639, 0.9639, 0.9607, 0.9613,
       0.9603, 0.9631, 0.9635, 0.9617, 0.9625, 0.9605, 0.9619, 0.9605,
       0.9617, 0.9609, 0.9617, 0.9613, 0.9603, 0.9607, 0.9605, 0.9603,
       0.9607, 0.9603, 0.9607, 0.9605, 0.9609, 0.9603, 0.9605, 0.9603,
       0.9603, 0.9603, 0.9603, 0.9603, 0.9603, 0.9603, 0.9603, 0.9603,
       0.9603, 0.9603, 0.9603, 0.9603, 0.9603, 0.9603, 0.9603, 0.9603,
       0.9603, 0.9544, 0.954 , 0.954 , 0.9603, 0.9536, 0.9603, 0.9524,
       0.9603, 0.953 , 0.952 , 0.9528, 0.9603, 0.9536, 0.9603, 0.9538,
       0.9522, 0.9526, 0.9538, 0.953 , 0.9518, 0.9526, 0.954 , 0.9512,
       0.9508, 0.953 , 0.9506, 0.9508, 0.952 , 0.9534, 0.953 ])

detector = 'EJ305'
att_factor = 6.31 # 16dB

# predefine holder for final UNBINNED data sets
neutron_QDC_FIN = pd.DataFrame()
random_QDC_FIN = pd.DataFrame()

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# LOADING DATA & SLICING LOOP
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop
run1 = 3439
numRuns = 167
runList = np.arange(run1, run1+numRuns)
stepSize = 5

for i in np.arange(0, len(runList), stepSize):
    currentRunList      = np.split(runList, [i, i+stepSize])[1] #split runlist into section of stepSize events
    currentGainOffsets  = np.split(gainOffsets, [i, i+stepSize])[1]  #split gainoffsets into section of stepSize events
    print('-------------------------------------------------')
    print(f'Now working on runs: {currentRunList}')
    PuBe_tof_data = propd.load_parquet_merge_gainadjust(path, currentRunList, keep_col=[    'qdc_sg_ch1', 
                                                                                            'tof_ch2', 
                                                                                            'tof_ch3', 
                                                                                            'tof_ch4', 
                                                                                            'tof_ch5', 
                                                                                            'amplitude_ch2', 
                                                                                            'amplitude_ch3', 
                                                                                            'amplitude_ch4', 
                                                                                            'amplitude_ch5', 
                                                                                            'qdc_lg_ch2',
                                                                                            'qdc_lg_ch3',
                                                                                            'qdc_lg_ch4',
                                                                                            'qdc_lg_ch5'], 
                                                                                            full  = False,
                                                                                            gainOffsets = currentGainOffsets,
                                                                                            gainOffsetCol = 'qdc_sg_ch1')

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    #      APPLY ATTENUATION COEFF. BEFORE BINNING
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    PuBe_tof_data.qdc_sg_ch1 = PuBe_tof_data.qdc_sg_ch1*att_factor

    #~~~~~~~~~~~~~~~~~~~~~~~~
    #      RESET INDEX
    #~~~~~~~~~~~~~~~~~~~~~~~~
    PuBe_tof_data = PuBe_tof_data.reset_index(drop=True)

    #~~~~~~~~~~~~~~~~~~~~~~~~
    #    CALIBRATING ToF
    #~~~~~~~~~~~~~~~~~~~~~~~~
    flashrange = {'tof_ch2':[55, 66], 'tof_ch3':[55, 66], 'tof_ch4':[55, 66], 'tof_ch5':[55, 66]} #gamma flash fitting ranges
    prodata.tof_calibration(PuBe_tof_data, flashrange, distance, phcut=125, numBins=np.arange(-250, 750, 1), calUnit=1e-9, energyCal=True)

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    #   Get random QDC LG
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    #get random data
    y2_random_QDC_TMP = PuBe_tof_data.query(f'qdc_sg_ch1>{nThrQDC} and qdc_lg_ch2>{y2ThrQDC} and tof_ch2>{randTOFRange[0]} and tof_ch2<={randTOFRange[1]}').qdc_sg_ch1
    y3_random_QDC_TMP = PuBe_tof_data.query(f'qdc_sg_ch1>{nThrQDC} and qdc_lg_ch3>{y3ThrQDC} and tof_ch3>{randTOFRange[0]} and tof_ch3<={randTOFRange[1]}').qdc_sg_ch1
    y4_random_QDC_TMP = PuBe_tof_data.query(f'qdc_sg_ch1>{nThrQDC} and qdc_lg_ch4>{y4ThrQDC} and tof_ch4>{randTOFRange[0]} and tof_ch4<={randTOFRange[1]}').qdc_sg_ch1
    y5_random_QDC_TMP = PuBe_tof_data.query(f'qdc_sg_ch1>{nThrQDC} and qdc_lg_ch5>{y5ThrQDC} and tof_ch5>{randTOFRange[0]} and tof_ch5<={randTOFRange[1]}').qdc_sg_ch1
    #check for random multiplicity
    mpIdxRandom = prodata.uniqueEventHelper(y2_random_QDC_TMP, y3_random_QDC_TMP, y4_random_QDC_TMP, y5_random_QDC_TMP)

    #Merge Random data
    random_QDC_FIN = pd.concat([random_QDC_FIN,
                                y2_random_QDC_TMP.drop(mpIdxRandom, errors='ignore'),
                                y3_random_QDC_TMP.drop(mpIdxRandom, errors='ignore'), 
                                y4_random_QDC_TMP.drop(mpIdxRandom, errors='ignore'), 
                                y5_random_QDC_TMP.drop(mpIdxRandom, errors='ignore')])
    
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    #   SLICE Neutron ToF ENERGY for QDC LG
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    y2_neutron_QDC_TMP, y3_neutron_QDC_TMP, y4_neutron_QDC_TMP, y5_neutron_QDC_TMP =    prodata.tofEnergySlicer(PuBe_tof_data, 
                                                                                                                minE = minE/1000, 
                                                                                                                maxE = maxE/1000, 
                                                                                                                dE = dE/1000, 
                                                                                                                colKeep = 'qdc_sg_ch1', 
                                                                                                                n_thr = nThrQDC, 
                                                                                                                y_thr = [y2ThrQDC, y3ThrQDC, y4ThrQDC, y5ThrQDC],
                                                                                                                QDC = 'sg'
                                                                                                                )

    # Merge QDC data
    neutron_QDC_FIN = pd.concat([   neutron_QDC_FIN,
                                    y2_neutron_QDC_TMP,
                                    y3_neutron_QDC_TMP, 
                                    y4_neutron_QDC_TMP, 
                                    y5_neutron_QDC_TMP])

    print(f'> neutrons: {len(neutron_QDC_FIN)} events')
    print(f'> random: {len(random_QDC_FIN)} events')

#Save UNBINNED data!
for col in nLength.columns:
    print(f'Saving to... {col}')
    np.save(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/{col}/nQDC_SG', neutron_QDC_FIN[col])
    np.save(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/{col}/randQDC_SG', random_QDC_FIN)
    # np.save(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/{col}/nLength', nLength[col].item())
    # np.save(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/{col}/randLength', randLength)
