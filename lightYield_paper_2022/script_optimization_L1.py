#!/usr/bin/env python3
from re import I
import sys
from tkinter import X
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
from numpy.core.arrayprint import SubArrayFormat
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit
from scipy import interpolate

# from matplotlib.colors import LinearSegmentedColormap


# IMPORT FROM LIBRARY DIRECTORY
sys.path.insert(0, "../library/")
sys.path.insert(0, "../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "../")
sys.path.insert(0, "data_analysis/")

import processing_math as promath
import processing_data as prodata
import processing_pd as propd
import processing_simulation as prosim
import figure_style as fs

# def kornilovMod(Ep, L0, L1):
#     return L0*Ep**2/(Ep+L1**2)

for detector in ['NE213A', 'EJ305','EJ331', 'EJ321P']:

    detector='EJ321P'
    gate = 'LG'
    pathData = '/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper'

    HH_data = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/HH_TP_FD/HH_data_{gate}.pkl')
    TP_data = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/HH_TP_FD/TP_data_{gate}.pkl')
    FD_data = pd.read_pickle(f'{pathData}/{detector}/TOF_slice/HH_TP_FD/FD_data_{gate}.pkl')

    energy = HH_data.energy

    HH_loc = HH_data.HH_loc
    TP_loc = TP_data.TP_loc
    FD_loc = FD_data.FD_loc

    HH_loc_err = HH_data.HH_loc_err
    TP_loc_err = TP_data.TP_loc_err
    FD_loc_err = FD_data.FD_loc_err

    # popt_HH_mod, pcov_HH_mod = curve_fit(kornilovMod, energy, HH_loc)
    # pcov_HH_mod = np.diag(np.sqrt(pcov_HH_mod))

    popt_HH, pcov_HH = curve_fit(promath.kornilovEq, energy, HH_loc)
    pcov_HH = np.diag(np.sqrt(pcov_HH))

    popt_TP, pcov_TP = curve_fit(promath.kornilovEq, energy, TP_loc)
    pcov_TP = np.diag(np.sqrt(pcov_TP))

    popt_FD, pcov_FD = curve_fit(promath.kornilovEq, energy, FD_loc)
    pcov_FD = np.diag(np.sqrt(pcov_FD))

    redChi2_HH = promath.chi2red(HH_loc, promath.kornilovEq(energy, popt_HH[0], popt_HH[1]), HH_loc_err, 2)
    redChi2_TP = promath.chi2red(TP_loc, promath.kornilovEq(energy, popt_TP[0], popt_TP[1]), TP_loc_err, 2)
    redChi2_FD = promath.chi2red(FD_loc, promath.kornilovEq(energy, popt_FD[0], popt_FD[1]), FD_loc_err, 2)    

    L1_avg = np.average([popt_HH[1], popt_TP[1], popt_FD[1]])
    L1_avg_err  = np.average([pcov_HH[1], pcov_TP[1], pcov_FD[1]])

    print(f'Scintillator: {detector}')
    print(f'HH.....L1 = {np.round(popt_HH[1],3)} +/- {np.round(pcov_HH[1],3)} ({round((popt_HH[1]-L1_avg)/L1_avg*100,1)}%)')
    print(f'TP.....L1 = {np.round(popt_TP[1],3)} +/- {np.round(pcov_TP[1],3)} ({round((popt_TP[1]-L1_avg)/L1_avg*100,1)}%)')
    print(f'FD.....L1 = {np.round(popt_FD[1],3)} +/- {np.round(pcov_FD[1],3)} ({round((popt_FD[1]-L1_avg)/L1_avg*100,1)}%)')
    print(f'Avg....L1 = {np.round(L1_avg,2)} +/- {np.round(L1_avg_err,2)}')
    print('--------------------------------------------')

    plt.suptitle(detector)
    plt.subplot(3,1,1)
    plt.scatter(energy, HH_loc, label='HH',color='blue', s=10)
    plt.errorbar(energy, HH_loc, yerr=HH_loc_err, ls='none',color='blue', lw=0.5)
    plt.plot(energy, promath.kornilovEq(energy, popt_HH[0], popt_HH[1]),color='blue', lw=0.5)
    
    # plt.plot(energy, kornilovMod(energy, popt_HH_mod[0], popt_HH_mod[1]),color='blue', lw=1, ls='dashed')
    plt.legend()

    plt.subplot(3,1,2)
    plt.scatter(energy, TP_loc, label='TP',color='red', s=10)
    plt.errorbar(energy, TP_loc, yerr=TP_loc_err, ls='none',color='red', lw=0.5)
    plt.plot(energy, promath.kornilovEq(energy, popt_TP[0], popt_TP[1]),color='red', lw=0.5)
    plt.legend()

    plt.subplot(3,1,3)
    plt.scatter(energy, FD_loc, label='FD',color='green', s=10)
    plt.errorbar(energy, FD_loc, yerr=FD_loc_err, ls='none',color='green', lw=0.5)
    plt.plot(energy, promath.kornilovEq(energy, popt_FD[0], popt_FD[1]),color='green', lw=0.5)
    plt.legend()

    plt.show()