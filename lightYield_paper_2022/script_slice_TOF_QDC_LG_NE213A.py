#!/usr/bin/env python3
from re import I
import sys
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
from numpy.core.arrayprint import SubArrayFormat
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit

# IMPORT FROM LIBRARY DIRECTORY
sys.path.insert(0, "../library/")
sys.path.insert(0, "../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "../")
sys.path.insert(0, "data_analysis/")

import processing_math as promath
import processing_data as prodata
import processing_pd as propd
import processing_simulation as prosim



#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#    DEFINE PARAMETERS FOR SLICE
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
distance = 0.959
gTOFRange = [1e-9, 6e-9] #Define range of gamma-flash
randTOFRange = [-710e-9,-10e-9] #Define range of random events
gLength = np.abs(gTOFRange[1]-gTOFRange[0]) #Calculate the lenght of cut for gammas
randLength = np.abs(randTOFRange[1]-randTOFRange[0]) #Calculate lenght of cut for random events

minE = 1625
maxE = 6625
dE = 250
nLength = pd.DataFrame()
for i in np.arange(minE, maxE, dE):
    E1 = round(i-dE,3)
    E2 = round(i,3)
    t1 = prodata.EnergytoTOF(E1/1000, distance)
    t2 = prodata.EnergytoTOF(E2/1000, distance)
    print(f'dE = {round(E1, 3)}-{round(E2, 3)} keV, center = {E1-dE/2} -> dt = {np.round((t1-t2)*1e9, 4)} ns')
    nLength[f'keV{int((i-dE/2))}'] = [np.abs(t1-t2)]

y2ThrQDC = 6450 #QDC threhold in channels
y3ThrQDC = 6985 #QDC threhold in channels
y4ThrQDC = 7500 #QDC threhold in channels
y5ThrQDC = 7850 #QDC threhold in channels
nThrQDC = 0 #QDC threshold in channels

gainOffsets = np.array([1.    , 1.0098, 1.0106, 1.01  , 1.0098, 1.0098, 1.0098, 1.0098,
                        1.0098, 1.0098, 1.01  , 1.0199, 1.0104, 1.0308, 1.0199, 1.0197,
                        0.99  , 1.0098, 1.0098, 1.0197, 1.0199, 1.0108, 0.9999, 0.9999,
                        1.0003, 1.0011, 1.0003, 1.0001, 1.0001, 1.0001, 1.0001, 1.0098,
                        0.9902, 0.9999, 0.9908, 0.9902, 0.9906, 0.991 , 1.0197, 0.9999,
                        0.9807, 0.9999, 0.9813, 0.9801, 0.9702, 0.99  , 0.99  , 0.9902,
                        0.9999, 0.99  , 1.0098, 0.99  , 1.0197, 0.99  , 0.99  , 1.0102,
                        0.9801, 0.99  , 0.9999, 0.9801, 0.9803, 1.0199, 0.9809, 0.9801,
                        0.9902, 0.9813, 0.9807, 1.0098, 0.99  , 0.9999, 1.0098, 0.99  ,
                        0.9803, 1.0098, 0.9999, 1.0098, 1.0005, 1.0001, 1.01  , 0.9702,
                        1.0017, 0.9803, 0.9912, 0.9999, 0.9904, 1.011 , 1.0098, 1.0197,
                        0.9823, 1.01  , 1.0001, 1.0098, 1.0197, 0.99  , 1.0005, 1.0009,
                        0.9906, 0.9704, 0.9999, 0.9999, 1.0116, 0.9902, 0.9906, 0.9914,
                        0.99  , 0.99  , 1.0001, 0.99  , 1.0009, 0.99  , 0.99  , 0.9999,
                        0.9904, 0.9904, 0.9902, 0.9908, 0.9999, 0.9805, 0.9906, 0.9817,
                        0.9902, 0.9803, 0.99  , 0.99  , 0.9807, 0.99  , 0.9817, 0.9718,
                        0.9805, 0.9801, 0.9805, 0.9809, 0.9819, 0.9801, 0.9805, 0.9803,
                        0.9805, 0.9801, 0.99  , 0.9801, 0.9803])

detector = 'NE213A'
att_factor = 3.98 # 12dB

# predefine holder for final UNBINNED data sets
neutron_QDC_FIN = pd.DataFrame()
random_QDC_FIN = pd.DataFrame()

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# LOADING DATA & SLICING LOOP
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop
run1 = 2121
runList = np.arange(run1, run1+141)

for i in np.arange(0, len(runList), 10):
    currentRunList      = np.split(runList, [i, i+10])[1] #split runlist into section of 10 events
    currentGainOffsets  = np.split(gainOffsets, [i, i+10])[1]  #split gainoffsets into section of 10 events
    print('-------------------------------------------------')
    print(f'Now working on runs: {currentRunList}')
    PuBe_tof_data = propd.load_parquet_merge_gainadjust(path, currentRunList, keep_col=[    'qdc_lg_ch1', 
                                                                                            'tof_ch2', 
                                                                                            'tof_ch3', 
                                                                                            'tof_ch4', 
                                                                                            'tof_ch5', 
                                                                                            'amplitude_ch2', 
                                                                                            'amplitude_ch3', 
                                                                                            'amplitude_ch4', 
                                                                                            'amplitude_ch5', 
                                                                                            'qdc_lg_ch2',
                                                                                            'qdc_lg_ch3',
                                                                                            'qdc_lg_ch4',
                                                                                            'qdc_lg_ch5'], 
                                                                                            full=False,
                                                                                            gainOffsets=currentGainOffsets,
                                                                                            gainOffsetCol = 'qdc_lg_ch1')
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    #      APPLY ATTENUATION COEFF. BEFORE BINNING
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    PuBe_tof_data.qdc_lg_ch1 = PuBe_tof_data.qdc_lg_ch1*att_factor

    #~~~~~~~~~~~~~~~~~~~~~~~~
    #      RESET INDEX
    #~~~~~~~~~~~~~~~~~~~~~~~~
    PuBe_tof_data = PuBe_tof_data.reset_index(drop=True)

    #~~~~~~~~~~~~~~~~~~~~~~~~
    #    CALIBRATING ToF
    #~~~~~~~~~~~~~~~~~~~~~~~~
    flashrange = {'tof_ch2':[60, 73], 'tof_ch3':[60, 73], 'tof_ch4':[60, 73], 'tof_ch5':[60, 73]} #gamma flash fitting ranges
    prodata.tof_calibration(PuBe_tof_data, flashrange, distance, phcut=125, calUnit=1e-9, energyCal=True)

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    #   Get random QDC LG
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    #get random data
    y2_random_QDC_TMP = PuBe_tof_data.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch2>{y2ThrQDC} and tof_ch2>{randTOFRange[0]} and tof_ch2<={randTOFRange[1]}').qdc_lg_ch1
    y3_random_QDC_TMP = PuBe_tof_data.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch3>{y3ThrQDC} and tof_ch3>{randTOFRange[0]} and tof_ch3<={randTOFRange[1]}').qdc_lg_ch1
    y4_random_QDC_TMP = PuBe_tof_data.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch4>{y4ThrQDC} and tof_ch4>{randTOFRange[0]} and tof_ch4<={randTOFRange[1]}').qdc_lg_ch1
    y5_random_QDC_TMP = PuBe_tof_data.query(f'qdc_lg_ch1>{nThrQDC} and qdc_lg_ch5>{y5ThrQDC} and tof_ch5>{randTOFRange[0]} and tof_ch5<={randTOFRange[1]}').qdc_lg_ch1
    #check for random multiplicity
    mpIdxRandom = prodata.uniqueEventHelper(y2_random_QDC_TMP, y3_random_QDC_TMP, y4_random_QDC_TMP, y5_random_QDC_TMP)

    #Merge Random data
    random_QDC_FIN = pd.concat([random_QDC_FIN,
                                y2_random_QDC_TMP.drop(mpIdxRandom, errors='ignore'),
                                y3_random_QDC_TMP.drop(mpIdxRandom, errors='ignore'), 
                                y4_random_QDC_TMP.drop(mpIdxRandom, errors='ignore'), 
                                y5_random_QDC_TMP.drop(mpIdxRandom, errors='ignore')])
    
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    #   SLICE Neutron ToF ENERGY for QDC LG
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    y2_neutron_QDC_TMP, y3_neutron_QDC_TMP, y4_neutron_QDC_TMP, y5_neutron_QDC_TMP =    prodata.tofEnergySlicer(PuBe_tof_data, 
                                                                                                                minE = minE/1000, 
                                                                                                                maxE = maxE/1000, 
                                                                                                                dE = dE/1000, 
                                                                                                                colKeep = 'qdc_lg_ch1', 
                                                                                                                n_thr = nThrQDC, 
                                                                                                                y_thr = [y2ThrQDC, y3ThrQDC, y4ThrQDC, y5ThrQDC]
                                                                                                                )

    # Merge QDC data
    neutron_QDC_FIN = pd.concat([   neutron_QDC_FIN,
                                    y2_neutron_QDC_TMP,
                                    y3_neutron_QDC_TMP, 
                                    y4_neutron_QDC_TMP, 
                                    y5_neutron_QDC_TMP])

    print(f'> neutrons: {len(neutron_QDC_FIN)} events')
    print(f'> random: {len(random_QDC_FIN)} events')

#Save UNBINNED data!
for col in nLength.columns:
    print(f'Saving to... {col}')
    np.save(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/{col}/nQDC_LG', neutron_QDC_FIN[col])
    np.save(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/{col}/randQDC_LG', random_QDC_FIN)
    np.save(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/{col}/nLength', nLength[col].item())
    np.save(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/TOF_slice/QDC/{col}/randLength', randLength)
