#!/usr/bin/env python3
import matplotlib.pyplot as plt
import numpy as np
import sys

sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries

import processing_math as promath





def chi2_pearson(obs, exp):
    return np.sum((obs-exp)**2/exp)

def chi2(obs, exp, obs_var):
    return np.sum((obs-exp)**2/obs_var**2)

def chi2_reduced(obs, exp, obs_var, npar):
    return chi2(obs, exp, obs_var)/(len(obs)-npar)

def chi2_reduced_loop(obs, exp, obs_var, npar):
    chi2_val = 0
    for i in range(len(obs)):
        chi2_val += (obs[i]-exp[i])**2/obs_var[i]**2
    return chi2_val/(len(obs)-npar)


################
################
## DATA SET 1 ##
################
################

def func1(x, L):
    """
    Model used to produce 'exp1' values
    x.......is the dependent variable
    L.......is a parameter
    """
    return L*x**2/(x+2.47)

#x-values
x = np.arange(2, 6.25, 0.25)

#Observed values (experimental data)
obs1 = np.array([0.598388, 0.694939, 0.804474, 0.954546, 1.088382, 1.225106, 1.374609, 1.515335, 1.697776, 1.831172, 1.993159, 2.170514, 2.345604, 2.444764, 2.624834, 2.807334,2.954537])
obs_err1 = np.array([0.013367, 0.015047, 0.020961, 0.019530, 0.039233, 0.034347, 0.030476, 0.039998, 0.037191, 0.049420, 0.035377, 0.043806, 0.034618, 0.065357, 0.096957, 0.066965, 0.085285])

#Expected values (fitted model)
exp1 = np.array([0.6088198 , 0.72972519, 0.85557863, 0.9856692 , 1.11941594, 1.25633944, 1.39604063, 1.53818458, 1.68248811, 1.82871021, 1.97664442, 2.1261129 , 2.27696161, 2.42905645, 2.58228017, 2.73652975, 2.89171436])

#analysis
print(f'My chi2 = {round(promath.chi2red(obs1, exp1, obs_err1, 1),3)}')
# print(f'new chi2 = {round(chi2(obs1, exp1, obs_err1),3)}')
print('--------------------------------------')
# print(f'My chi2_red = {round(promath.chi2red(obs1, exp1, obs_err1,1), 3)}')
# print(f'new chi2_red = {round(chi2_reduced(obs1, exp1, obs_err1,1),3)}')
# print(f'new chi2_red loop = {round(chi2_reduced_loop(obs1, exp1, obs_err1,1),3)}')
# print('--------------------------------------')
# print(f'My pearson = {round(promath.pearsonChi2(obs1, exp1), 3)}')
# print(f'new pearson = {round(chi2_pearson(obs1, exp1), 3)}')


# from lmfit import Model
# model = Model(func1)
# params = model.make_params(L=0.25)
# result = model.fit(obs1, params, weights=1/obs_err1, x=x)
# print(f'Chi-square = {result.chisqr:.4f}, Reduced Chi-square = {result.redchi:.4f}')
# print(result.fit_report())

plt.plot(x, obs1, label='data')
plt.plot(x, result.best_fit, label='best fit')
plt.legend()
plt.show()


#make plot
plt.title('Data-set 1')
plt.scatter(x, obs1, label='Observed values (experimental data', color='black', s=50)
plt.errorbar(x, obs1, obs_err1, ls='none', color='black')
plt.plot(x, exp1, label='Expected values (fitted model)', color='red')
plt.xlabel('x-value')
plt.ylabel('y-value')
plt.legend()
plt.show()



################
################
## DATA SET 2 ##
################
################

def func2(x, K):
    """
    Model used to produce 'exp2' values
    x.......is the dependent variable
    K.......is a parameter
    """
    return K*(0.83*x-2.82*(1-np.exp(-0.25*np.power(x,0.93))))

#x-values
x = np.arange(2, 6.25, 0.25)

#Observed values (experimental data)
obs2 = np.array([0.404075, 0.481974, 0.583170, 0.681263, 0.782983, 0.902948, 1.003392, 1.128516, 1.272921, 1.410279, 1.550649, 1.695898, 1.825645, 1.972691, 2.118115, 2.255240, 2.396789])
obs_err2 = np.array([0.014836, 0.013064, 0.016448, 0.019111, 0.018774, 0.018828, 0.025366, 0.033151, 0.033400, 0.038124, 0.035817, 0.047462, 0.048854, 0.054614, 0.073079, 0.065086, 0.077062])

#Expected values (fitted model)
exp2 = np.array([0.47303978, 0.55999061, 0.65032735, 0.74378496, 0.84014197, 0.93920985, 1.04082558, 1.14484636, 1.25114573, 1.35961065, 1.47013924, 1.58263906, 1.69702569, 1.81322163, 1.93115539, 2.0507607 , 2.17197597])

print(f'My chi2 = {round(promath.chi2(obs2, exp2, obs_err2),3)}')
print(f'new chi2 = {round(chi2(obs2, exp2, obs_err2),3)}')
print('--------------------------------------')
print(f'My chi2_red = {round(promath.chi2red(obs2, exp2, obs_err2,1), 3)}')
print(f'new chi2_red = {round(chi2_reduced(obs2, exp2, obs_err2,1),3)}')
print(f'new chi2_red loop = {round(chi2_reduced_loop(obs2, exp2, obs_err2,1),3)}')
print('--------------------------------------')
print(f'My pearson = {round(promath.pearsonChi2(obs2, exp2), 3)}')
print(f'new pearson = {round(chi2_pearson(obs2, exp2), 3)}')



from lmfit import Model
model = Model(func2)
params = model.make_params(K=0.7)
result = model.fit(obs2, params, weights=1/obs_err2,  x=x)
print(result.fit_report())


print(f'Chi-square = {result.chisqr:.4f}, Reduced Chi-square = {result.redchi:.4f}')

opt_params = result.best_values
opt_params_err = np.diag(np.sqrt(result.covar))
chi2 = result.chisqr
chi2red = result.redchi
print(opt_params, opt_params_err, chi2, chi2red)

plt.plot(x, obs2, label='data')
plt.plot(x, result.best_fit, label='best fit')
plt.plot(x, exp2, label='Expected values (fitted model)', color='red')
plt.legend()
plt.show()

#make plot
plt.title('Data-set 2')
plt.scatter(x, obs2, label='Observed values (experimental data', color='black', s=50)
plt.errorbar(x, obs2, obs_err2, ls='none', color='black')
plt.plot(x, exp2, label='Expected values (fitted model)', color='red')
plt.xlabel('x-value')
plt.ylabel('y-value')
plt.legend()
plt.show()



from lmfit import Model
superFit(x, obs1, obs_err1, {'K':0.65})


#simple fittin routine to get paramter paramter_error, chi2 and chi2red

def superFit(x, y, y_err, param_guess, fitFunc):
    """
    ....
    """

    #params_guess = [0.7]
   
    model = Model(func1)
    params = model.make_params(K=0.65)
    result = model.fit(y, params, weights=1/y_err,  x=x)



    return opt_params, opt_params_err, chi2, chi2red
