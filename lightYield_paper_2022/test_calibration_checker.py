#!/usr/bin/env python3
from re import I
import sys
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
from numpy.core.arrayprint import SubArrayFormat
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit

# IMPORT FROM LIBRARY DIRECTORY
sys.path.insert(0, "../library/")
sys.path.insert(0, "../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "../")
sys.path.insert(0, "data_analysis/")

import processing_math as promath
import processing_data as prodata
import processing_pd as propd
import processing_simulation as prosim


"""
Scrip to check calibrations in MeVee between detectors.
"""


####################################################
path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop

NE213A_Cs = propd.load_parquet_merge(path, [3259], keep_col=['qdc_lg_ch1', 'qdc_sg_ch1'], full=False)
NE213A_Th = propd.load_parquet_merge(path, [3258], keep_col=['qdc_lg_ch1', 'qdc_sg_ch1'], full=False)
NE213A_AmBe = propd.load_parquet_merge(path, [3257], keep_col=['qdc_lg_ch1', 'qdc_sg_ch1'], full=False)
NE213A_BG = propd.load_parquet_merge(path, [3260,3261,3262,3263,3264], keep_col=['qdc_lg_ch1', 'qdc_sg_ch1'], full=False)

EJ305_Cs = propd.load_parquet_merge(path, [3240], keep_col=['qdc_lg_ch1', 'qdc_sg_ch1'], full=False)
EJ305_Th = propd.load_parquet_merge(path, [3241], keep_col=['qdc_lg_ch1', 'qdc_sg_ch1'], full=False)
EJ305_AmBe = propd.load_parquet_merge(path, [3242], keep_col=['qdc_lg_ch1', 'qdc_sg_ch1'], full=False)
EJ305_BG = propd.load_parquet_merge(path, [3243,3244,3245,3246,3247], keep_col=['qdc_lg_ch1', 'qdc_sg_ch1'], full=False)

EJ321P_Cs = propd.load_parquet_merge(path, [2824], keep_col=['qdc_lg_ch1', 'qdc_sg_ch1'], full=False)
EJ321P_Th = propd.load_parquet_merge(path, [2826], keep_col=['qdc_lg_ch1', 'qdc_sg_ch1'], full=False)
EJ321P_AmBe = propd.load_parquet_merge(path, [3025], keep_col=['qdc_lg_ch1', 'qdc_sg_ch1'], full=False)
EJ321P_BG = propd.load_parquet_merge(path, [2801,2802,2803,2804,2805], keep_col=['qdc_lg_ch1', 'qdc_sg_ch1'], full=False)

# EJ331_Cs = propd.load_parquet_merge(path, [1952], keep_col=['qdc_lg_ch1', 'qdc_sg_ch1'], full=False)
# EJ331_Th = propd.load_parquet_merge(path, [1967], keep_col=['qdc_lg_ch1', 'qdc_sg_ch1'], full=False)
# EJ331_AmBe = propd.load_parquet_merge(path, [1955], keep_col=['qdc_lg_ch1', 'qdc_sg_ch1'], full=False)
# EJ331_BG = propd.load_parquet_merge(path, [1942,1943,1944,1945,1946], keep_col=['qdc_lg_ch1', 'qdc_sg_ch1'], full=False)

CableLengthCorrections = 1.192
NE213A_Cs.qdc_lg_ch1 = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/NE213A/Ecal', NE213A_Cs.qdc_lg_ch1*3.98*CableLengthCorrections)
NE213A_Th.qdc_lg_ch1 = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/NE213A/Ecal', NE213A_Th.qdc_lg_ch1*3.98*CableLengthCorrections)
NE213A_AmBe.qdc_lg_ch1 = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/NE213A/Ecal', NE213A_AmBe.qdc_lg_ch1*3.98*CableLengthCorrections)
NE213A_BG.qdc_lg_ch1 = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/NE213A/Ecal', NE213A_BG.qdc_lg_ch1*3.98*CableLengthCorrections)

# CableLengthCorrections = 0.97
# EJ305_Cs.qdc_lg_ch1 = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/EJ305/Ecal', EJ305_Cs.qdc_lg_ch1*8.91*CableLengthCorrections)
# EJ305_Th.qdc_lg_ch1 = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/EJ305/Ecal', EJ305_Th.qdc_lg_ch1*8.91*CableLengthCorrections)
# EJ305_AmBe.qdc_lg_ch1 = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/EJ305/Ecal', EJ305_AmBe.qdc_lg_ch1*8.91*CableLengthCorrections)
# EJ305_BG.qdc_lg_ch1 = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/EJ305/Ecal', EJ305_BG.qdc_lg_ch1*8.91*CableLengthCorrections)

poptQuad = np.array([1.59253101e-10, 1.56202964e-05, 7.32901514e-02])
CableLengthCorrections = 0.97
EJ305_Cs.qdc_lg_ch1 =   promath.quadraticFunc(EJ305_Cs.qdc_lg_ch1*8.91*CableLengthCorrections,   poptQuad[0],  poptQuad[1],  poptQuad[2])
EJ305_Th.qdc_lg_ch1 =   promath.quadraticFunc(EJ305_Th.qdc_lg_ch1*8.91*CableLengthCorrections,   poptQuad[0],  poptQuad[1],  poptQuad[2])
EJ305_AmBe.qdc_lg_ch1 = promath.quadraticFunc(EJ305_AmBe.qdc_lg_ch1*8.91*CableLengthCorrections, poptQuad[0],  poptQuad[1],  poptQuad[2])
EJ305_BG.qdc_lg_ch1 =   promath.quadraticFunc(EJ305_BG.qdc_lg_ch1*8.91*CableLengthCorrections,   poptQuad[0],  poptQuad[1],  poptQuad[2])

CableLengthCorrections = 1.07
EJ321P_Cs.qdc_lg_ch1 = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/EJ321P/Ecal', EJ321P_Cs.qdc_lg_ch1*2.82*CableLengthCorrections)
EJ321P_Th.qdc_lg_ch1 = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/EJ321P/Ecal', EJ321P_Th.qdc_lg_ch1*2.82*CableLengthCorrections)
EJ321P_AmBe.qdc_lg_ch1 = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/EJ321P/Ecal', EJ321P_AmBe.qdc_lg_ch1*2.82*CableLengthCorrections)
EJ321P_BG.qdc_lg_ch1 = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/EJ321P/Ecal', EJ321P_BG.qdc_lg_ch1*2.82*CableLengthCorrections)

# CableLengthCorrections = 1.05
# EJ331_Cs.qdc_lg_ch1 = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/EJ331/Ecal', EJ331_Cs.qdc_lg_ch1*5.62*CableLengthCorrections)
# EJ331_Th.qdc_lg_ch1 = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/EJ331/Ecal', EJ331_Th.qdc_lg_ch1*5.62*CableLengthCorrections)
# EJ331_AmBe.qdc_lg_ch1 = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/EJ331/Ecal', EJ331_AmBe.qdc_lg_ch1*5.62*CableLengthCorrections)
# EJ331_BG.qdc_lg_ch1 = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/EJ331/Ecal', EJ331_BG.qdc_lg_ch1*5.62*CableLengthCorrections)

numBins = np.arange(-1, 8, 0.0100)

w = np.empty(len(NE213A_BG))
w.fill(1/5)
NE213A_BGcounts, x = np.histogram(NE213A_BG.qdc_lg_ch1, bins=numBins, weights=w)

w = np.empty(len(EJ305_BG))
w.fill(1/5)
EJ305_BGcounts, x = np.histogram(EJ305_BG.qdc_lg_ch1, bins=numBins, weights=w)

w = np.empty(len(EJ321P_BG))
w.fill(1/5)
EJ321P_BGcounts, x = np.histogram(EJ321P_BG.qdc_lg_ch1, bins=numBins, weights=w)

# w = np.empty(len(EJ331_BG))
# w.fill(1/5)
# EJ331_BGcounts, x = np.histogram(EJ331_BG.qdc_lg_ch1, bins=numBins, weights=w)




plt.figure()
y, x = np.histogram(NE213A_Cs.qdc_lg_ch1, bins=numBins)
x1 = prodata.getBinCenters(x)
plt.step(x1, y - NE213A_BGcounts, label='NE213A')

y, x = np.histogram(EJ305_Cs.qdc_lg_ch1, bins=numBins)
plt.step(x1, y - EJ305_BGcounts, label='EJ305')

y, x = np.histogram(EJ321P_Cs.qdc_lg_ch1, bins=numBins)
plt.step(x1, y - EJ321P_BGcounts, label='EJ321P')

# y, x = np.histogram(EJ331_Cs.qdc_lg_ch1, bins=numBins)
# plt.step(x1, y - EJ331_BGcounts, label='EJ331')
plt.ylabel('counts')
plt.legend()
plt.yscale('log')


plt.figure()
y, x = np.histogram(NE213A_Th.qdc_lg_ch1, bins=numBins)
x1 = prodata.getBinCenters(x)
plt.step(x1, y - NE213A_BGcounts, label='NE213A')

y, x = np.histogram(EJ305_Th.qdc_lg_ch1, bins=numBins)
plt.step(x1, y - EJ305_BGcounts, label='EJ305')

y, x = np.histogram(EJ321P_Th.qdc_lg_ch1, bins=numBins)
plt.step(x1, y - EJ321P_BGcounts, label='EJ321P')

# y, x = np.histogram(EJ331_Th.qdc_lg_ch1, bins=numBins)
# plt.step(x1, y - EJ331_BGcounts, label='EJ331')
plt.ylabel('counts')
plt.legend()
plt.yscale('log')


plt.figure()
y, x = np.histogram(NE213A_AmBe.qdc_lg_ch1, bins=numBins)
x1 = prodata.getBinCenters(x)
plt.step(x1, y - NE213A_BGcounts, label='NE213A')

y, x = np.histogram(EJ305_AmBe.qdc_lg_ch1, bins=numBins)
plt.step(x1, y - EJ305_BGcounts, label='EJ305')

y, x = np.histogram(EJ321P_AmBe.qdc_lg_ch1, bins=numBins)
plt.step(x1, y - EJ321P_BGcounts, label='EJ321P')

# y, x = np.histogram(EJ331_AmBe.qdc_lg_ch1, bins=numBins)
# plt.step(x1, y - EJ331_BGcounts, label='EJ331')
plt.ylabel('counts')
plt.legend()
plt.yscale('log')

plt.show()
