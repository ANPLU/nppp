#!/usr/bin/env python3
import sys
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit

# IMPORT FROM LIBRARY DIRECTORY
sys.path.insert(0, "../library/")
sys.path.insert(0, "../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "sim_analysis/")
sys.path.insert(0, "data_analysis/")

import digidata
import nicholai_plot_helper as nplt
import processing_utility as prout
import processing_math as promath
import processing_data as prodata
import processing_pd as propd
import processing_simulation as prosim


"""
Test to try and align simulated gamma spectra with simulated neutron spectra
"""


path_sim = '/media/gheed/Seagate_Expansion_Drive1/data/simulation/'
names = [ 'evtNum', 
        'optPhotonSum', 
        'optPhotonSumQE', 
        'optPhotonSumCompton', 
        'optPhotonSumComptonQE', 
        'optPhotonSumNPQE', 
        'xLoc', 
        'yLoc', 
        'zLoc', 
        'CsMin', 
        'optPhotonParentID', 
        'optPhotonParentCreatorProcess', 
        'optPhotonParentStartingEnergy',
        'total_edep',
        'gamma_edep',
        'proton_edep',
        'nScatters',
        'nScattersProton']

fitRange = [np.arange(10,1000,4), #1500
            np.arange(10,1000,4), #1750
            np.arange(10,1000,5), #2000
            np.arange(10,1000,5), #2250
            np.arange(10,1000,5), #2500
            np.arange(10,1000,6), #2750
            np.arange(10,1000,6), #3000
            np.arange(10,1000,6), #3250
            np.arange(10,1000,7), #3500
            np.arange(10,1000,7), #3750
            np.arange(10,1000,7), #4000
            np.arange(10,1000,7), #4250
            np.arange(10,1000,8), #4500
            np.arange(10,1000,8), #4750
            np.arange(10,1000,8), #5000
            np.arange(10,1000,9), #5250
            np.arange(10,1000,9), #5500
            np.arange(10,1000,9), #5750
            np.arange(10,1000,10), #6000
            np.arange(10,1000,10)] #6250

fitScale = [1.2, #1500
            1.1, #1750
            1.2, #2000
            0.93, #2250
            1.2, #2500
            0.88, #2750
            0.86, #3000
            0.83, #3250
            0.81, #3500
            0.78, #3750
            0.76, #4000
            0.74, #4250
            0.72, #4500
            0.69, #4750
            0.67, #5000
            0.64, #5250
            0.62, #5500
            0.60, #5750
            0.57, #6000
            0.55] #6250

fitGain = [ 0.12, #1500
            0.15, #1750
            0.19, #2000
            0.24, #2250
            0.25, #2500
            0.33, #2750
            0.37, #3000
            0.42, #3250
            0.47, #3500
            0.51, #3750
            0.56, #4000
            0.60, #4250
            0.65, #4500
            0.70, #4750
            0.74, #5000
            0.78, #5250
            0.83, #5500
            0.88, #5750
            0.92, #6000
            0.97] #6250

smear = [   0.19, #1500
            0.184, #1750
            0.179, #2000
            0.16, #2250
            0.168, #2500
            0.162, #2750
            0.157, #3000
            0.151, #3250
            0.146, #3500
            0.140, #3750
            0.135, #4000
            0.129, #4250
            0.124, #4500
            0.118, #4750
            0.113, #5000
            0.107, #5250
            0.102, #5500
            0.096, #5750
            0.090, #6000
            0.085] #6250

detector = 'NE213A'

Th232_sim =  pd.read_csv(path_sim + f"{detector}/Th232/isotropic/CSV_optphoton_data_sum.csv", names=names)
Th232_sim = pd.concat([Th232_sim, pd.read_csv(path_sim + 'NE213A/Th232/isotropic/part2/CSV_optphoton_data_sum.csv', names=names)])
y, x = np.histogram(Th232_sim.optPhotonSumQE, bins=4096, range=[0, 4096])

Th232_sim.optPhotonSumQE = prodata.getRandDist(x, y)
y, x = np.histogram(Th232_sim.optPhotonSumComptonQE, bins=4096, range=[0, 4096])
Th232_sim.optPhotonSumComptonQE = prodata.getRandDist(x, y)

Th232_sim.optPhotonSumQE = Th232_sim.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, smearFactor)) 
Th232_sim.optPhotonSumComptonQE = Th232_sim.optPhotonSumComptonQE.apply(lambda x: prosim.gaussSmear(x, smearFactor)) 


smearFactor = 0

for i, E in enumerate(np.arange(1500,6500,250)):


    neutron_sim = pd.read_csv(path_sim + f"{detector}/neutrons/neutron_range/{int(E)}keV/isotropic/CSV_optphoton_data_sum.csv", names=names)
    # neutron_sim.optPhotonSumNPQE = neutron_sim.optPhotonSumNPQE.apply(lambda x: prosim.gaussSmear(x, 0)) 
    y, x = np.histogram(neutron_sim.optPhotonSumNPQE, bins=4096, range=[0, 4096])
    neutron_sim.optPhotonSumNPQE = prodata.getRandDist(x, y)


    # res = prosim.PMTGainAlignSmear(  model = Th232_sim.optPhotonSumComptonQE, #gamma data
    #                             data =  neutron_sim.optPhotonSumNPQE, #neutron data
    #                             bg = np.zeros(len(Th232_sim)),
    #                             scale = 0.75,
    #                             gain = 0.74,
    #                             data_weight = 1,
    #                             bg_weight = 1,
    #                             scaleBound = [0.6 , 0.85],
    #                             gainBound = [0.55, 0.85],
    #                             stepSize = 1e-2,
    #                             binRange = fitRange[i],
    #                             plot = False)

    res = prosim.PMTGainAlignSmear(  model = Th232_sim.optPhotonSumComptonQE, 
                            data = neutron_sim.optPhotonSumNPQE, 
                            scale = fitScale[i], 
                            gain = fitGain[i], 
                            smear = smear[i], 
                            bg = np.zeros(10), 
                            data_weight=1, 
                            bg_weight=1, 
                            scaleBound=[-np.inf, np.inf], 
                            gainBound=[-np.inf, np.inf], 
                            smearBound=[-np.inf, np.inf], 
                            stepSize=1e-2, 
                            binRange=list(range(0,500,2)), 
                            plot=False):
    #load gamma data
    Th232_sim =  pd.read_csv(path_sim + f"{detector}/Th232/isotropic/CSV_optphoton_data_sum.csv", names=names)
    Th232_sim = pd.concat([Th232_sim, pd.read_csv(path_sim + 'NE213A/Th232/isotropic/part2/CSV_optphoton_data_sum.csv', names=names)])

    #randomize gamma gata data
    y, x = np.histogram(Th232_sim.optPhotonSumQE, bins=4096, range=[0, 4096])
    Th232_sim.optPhotonSumQE = prodata.getRandDist(x, y)


    Th232_sim.optPhotonSumQE = Th232_sim.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, smear[i])) 


    y1, x1 = np.histogram(neutron_sim.optPhotonSumNPQE, bins=fitRange[i])
    x1 = prodata.getBinCenters(x1)

    y2, x2 = np.histogram(Th232_sim.optPhotonSumQE*fitGain[i], bins=fitRange[i])
    x2 = prodata.getBinCenters(x2)

    plt.step(x1, y1, label='neutron')
    plt.step(x2, y2*fitScale[i], label='gamma')
    plt.title(f'Energy = {E} keV')
    plt.legend()
    plt.show()












neutron_sim = pd.read_csv(path_sim + f"{detector}/neutrons/neutron_range/{6250}keV/isotropic/CSV_optphoton_data_sum.csv", names=names)
neutron_sim.optPhotonSumNPQE = neutron_sim.optPhotonSumNPQE.apply(lambda x: prosim.gaussSmear(x, 0)) 

#randomize data
y, x = np.histogram(neutron_sim.optPhotonSumNPQE, bins=4096, range=[0, 4096])
neutron_sim.optPhotonSumNPQE = prodata.getRandDist(x, y)



Th232_sim =  pd.read_csv(path_sim + f"{detector}/Th232/isotropic/CSV_optphoton_data_sum.csv", names=names)
Th232_sim = pd.concat([Th232_sim, pd.read_csv(path_sim + 'NE213A/Th232/isotropic/part2/CSV_optphoton_data_sum.csv', names=names)])

#randomize data
y, x = np.histogram(Th232_sim.optPhotonSumQE, bins=4096, range=[0, 4096])
Th232_sim.optPhotonSumQE = prodata.getRandDist(x, y)
# y, x = np.histogram(Th232_sim.optPhotonSumComptonQE, bins=4096, range=[0, 4096])
# Th232_sim.optPhotonSumComptonQE = prodata.getRandDist(x, y)

smearFactor = 0.085

Th232_sim.optPhotonSumQE = Th232_sim.optPhotonSumQE.apply(lambda x: prosim.gaussSmear(x, smearFactor)) 
# Th232_sim.optPhotonSumComptonQE = Th232_sim.optPhotonSumComptonQE.apply(lambda x: prosim.gaussSmear(x, smearFactor)) 


y1, x1 = np.histogram(neutron_sim.optPhotonSumNPQE, bins=np.arange(10,1000,10))
x1 = prodata.getBinCenters(x1)

y2, x2 = np.histogram(Th232_sim.optPhotonSumQE*0.97, bins=np.arange(100,1000,10))
x2 = prodata.getBinCenters(x2)

plt.step(x1, y1, label='neutron')
plt.step(x2, y2*0.55, label='gamma')
plt.legend()
plt.show()













detector= 'NE213A'
numBins = np.arange(0, 1024, 12)

neutron_sim_5000 = pd.read_csv(path_sim + f"{detector}/neutrons/neutron_range/{int(5000)}keV/isotropic/CSV_optphoton_data_sum.csv", names=names)

y, x = np.histogram(neutron_sim_5000.optPhotonSumQE, bins=4096, range=[0, 4096])
neutron_sim_5000.optPhotonSumQE = prodata.getRandDist(x, y)

y,x = np.histogram(neutron_sim_5000.optPhotonSumQE, bins=numBins)
x = prodata.getBinCenters(x)
plt.step(x,y,label='isotropic')




neutron_sim_5000_pen = pd.read_csv(path_sim + f"{detector}/neutrons/neutron_range/{int(5000)}keV/pencilbeam/CSV_optphoton_data_sum.csv", names=names)

y, x = np.histogram(neutron_sim_5000_pen.optPhotonSumQE, bins=4096, range=[0, 4096])
neutron_sim_5000_pen.optPhotonSumQE = prodata.getRandDist(x, y)


y,x = np.histogram(neutron_sim_5000_pen.optPhotonSumQE, bins=numBins)
x = prodata.getBinCenters(x)
plt.step(x,y*0.2,label='pencilbeam')

plt.legend()
plt.show()
