#!/usr/bin/env python3
import sys
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit

# IMPORT FROM LIBRARY DIRECTORY
sys.path.insert(0, "../library/")
sys.path.insert(0, "../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "sim_analysis/")
sys.path.insert(0, "data_analysis/")

import digidata
import nicholai_plot_helper as nplt
import processing_utility as prout
import processing_math as promath
import processing_data as prodata
import processing_pd as propd
import processing_simulation as prosim

"""
Script for running both data and simulation and calibrating data of EJ305 detector.
"""

PMT_gain = 4e6 #Nominal gain is 7'000'000 at nominal A/lm (model: 9821B p1)
att_factor_11dB = 3.55
att_factor_12dB = 3.98
att_factor_16_5dB = 6.68
att_factor_19dB = 8.91
q = 1.60217662E-19 #charge of a single electron
plot = False

path_sim = '/media/gheed/Seagate_Expansion_Drive1/data/simulation/'


###############################
# LOADING SIMULATION DATA
###############################
detector = 'EJ305'

path_sim = '/media/gheed/Seagate_Expansion_Drive1/data/simulation/'
names = ['evtNum', 
        'optPhotonSum', 
        'optPhotonSumQE', 
        'optPhotonSumCompton', 
        'optPhotonSumComptonQE', 
        'optPhotonSumNPQE', 
        'xLoc', 
        'yLoc', 
        'zLoc', 
        'CsMin', 
        'optPhotonParentID', 
        'optPhotonParentCreatorProcess', 
        'optPhotonParentStartingEnergy',
        'total_edep',
        'gamma_edep',
        'proton_edep',
        'nScatters',
        'nScattersProton']

############################################
# LOAD ISOTROPIC SIMULATIONS
############################################
Cs137_sim =         pd.read_csv(path_sim + f"{detector}/Cs137/isotropic/CSV_optphoton_data_sum.csv", names=names)
Th232_sim =         pd.read_csv(path_sim + f"{detector}/Th232/isotropic/CSV_optphoton_data_sum.csv", names=names)
AmBe_sim =          pd.read_csv(path_sim + f"{detector}/4.44/isotropic/CSV_optphoton_data_sum.csv", names=names)

Cs137_sim =         pd.concat([Cs137_sim, pd.read_csv(path_sim + f"{detector}/Cs137/isotropic/part2/CSV_optphoton_data_sum.csv", names=names)])
Cs137_sim =         pd.concat([Cs137_sim, pd.read_csv(path_sim + f"{detector}/Cs137/isotropic/part3/CSV_optphoton_data_sum.csv", names=names)])

Th232_sim =         pd.concat([Th232_sim, pd.read_csv(path_sim + f"{detector}/Th232/isotropic/part2/CSV_optphoton_data_sum.csv", names=names)])
Th232_sim =         pd.concat([Th232_sim, pd.read_csv(path_sim + f"{detector}/Th232/isotropic/part3/CSV_optphoton_data_sum.csv", names=names)])

AmBe_sim =          pd.concat([AmBe_sim, pd.read_csv(path_sim + f"{detector}/4.44/isotropic/part3/CSV_optphoton_data_sum.csv", names=names)])
AmBe_sim =          pd.concat([AmBe_sim, pd.read_csv(path_sim + f"{detector}/4.44/isotropic/part3/CSV_optphoton_data_sum.csv", names=names)])

############################################
# LOAD PENCILBEAM SIMULATIONS
############################################
Cs137_sim_pen = pd.read_csv(path_sim + f"{detector}/Cs137/pencilbeam/CSV_optphoton_data_sum.csv", names=names)
Th232_sim_pen = pd.read_csv(path_sim + f"{detector}/Th232/pencilbeam/CSV_optphoton_data_sum.csv", names=names)
AmBe_sim_pen = pd.read_csv(path_sim + f"{detector}/4.44/pencilbeam/CSV_optphoton_data_sum.csv", names=names)

Cs137_sim_pen = pd.concat([Cs137_sim_pen, pd.read_csv(path_sim + f"{detector}/Cs137/pencilbeam/part2/CSV_optphoton_data_sum.csv", names=names)])
Cs137_sim_pen = pd.concat([Cs137_sim_pen, pd.read_csv(path_sim + f"{detector}/Cs137/pencilbeam/part3/CSV_optphoton_data_sum.csv", names=names)])

Th232_sim_pen = pd.concat([Th232_sim_pen, pd.read_csv(path_sim + f"{detector}/Th232/pencilbeam/part2/CSV_optphoton_data_sum.csv", names=names)])
Th232_sim_pen = pd.concat([Th232_sim_pen, pd.read_csv(path_sim + f"{detector}/Th232/pencilbeam/part3/CSV_optphoton_data_sum.csv", names=names)])

AmBe_sim_pen = pd.concat([AmBe_sim_pen, pd.read_csv(path_sim + f"{detector}/4.44/pencilbeam/part2/CSV_optphoton_data_sum.csv", names=names)])
AmBe_sim_pen = pd.concat([AmBe_sim_pen, pd.read_csv(path_sim + f"{detector}/4.44/pencilbeam/part3/CSV_optphoton_data_sum.csv", names=names)])

###############################
# RANDOMIZE BINNING
###############################
y, x = np.histogram(Cs137_sim.optPhotonSumQE, bins=4096, range=[0, 4096])
Cs137_sim.optPhotonSumQE = prodata.getRandDist(x, y)
y, x = np.histogram(Th232_sim.optPhotonSumQE, bins=4096, range=[0, 4096])
Th232_sim.optPhotonSumQE = prodata.getRandDist(x, y)
y, x = np.histogram(AmBe_sim.optPhotonSumQE, bins=4096, range=[0, 4096])
AmBe_sim.optPhotonSumQE = prodata.getRandDist(x, y)

y, x = np.histogram(Cs137_sim_pen.optPhotonSumComptonQE, bins=4096, range=[0, 4096])
Cs137_sim_pen.optPhotonSumComptonQE = prodata.getRandDist(x, y)
y, x = np.histogram(Th232_sim_pen.optPhotonSumComptonQE, bins=4096, range=[0, 4096])
Th232_sim_pen.optPhotonSumComptonQE = prodata.getRandDist(x, y)
y, x = np.histogram(AmBe_sim_pen.optPhotonSumComptonQE, bins=4096, range=[0, 4096])
AmBe_sim_pen.optPhotonSumComptonQE = prodata.getRandDist(x, y)

###############################
# RESET INDEX
###############################
Cs137_sim =  Cs137_sim.reset_index()
Th232_sim =  Th232_sim.reset_index()
AmBe_sim =   AmBe_sim.reset_index()

Cs137_sim_pen =  Cs137_sim_pen.reset_index()
Th232_sim_pen =  Th232_sim_pen.reset_index()
AmBe_sim_pen =   AmBe_sim_pen.reset_index()

###############################
# CALIBRATING SIMULATION DATA
###############################
Cs137_sim.optPhotonSumQE =         prosim.chargeCalibration(Cs137_sim.optPhotonSumQE      * q * PMT_gain)
Th232_sim.optPhotonSumQE =         prosim.chargeCalibration(Th232_sim.optPhotonSumQE      * q * PMT_gain)
AmBe_sim.optPhotonSumQE =          prosim.chargeCalibration(AmBe_sim.optPhotonSumQE       * q * PMT_gain)

Cs137_sim_pen.optPhotonSumComptonQE =      prosim.chargeCalibration(Cs137_sim_pen.optPhotonSumComptonQE * q * PMT_gain) 
Th232_sim_pen.optPhotonSumComptonQE =      prosim.chargeCalibration(Th232_sim_pen.optPhotonSumComptonQE * q * PMT_gain) 
AmBe_sim_pen.optPhotonSumComptonQE =      prosim.chargeCalibration(AmBe_sim_pen.optPhotonSumComptonQE * q * PMT_gain) 



################################
# MAKE PLOTS
###############################
plt.subplot(2,1,1)
plt.hist(Cs137_sim.optPhotonSumQE, bins=np.arange(0,25000,2000), histtype='step', label='Cs137')
plt.hist(Th232_sim.optPhotonSumQE, bins=np.arange(0,100000,250), histtype='step', label='Th232')
plt.hist(AmBe_sim.optPhotonSumQE, bins=np.arange(0,150000,400), histtype='step', label='AmBe')
plt.legend()
plt.yscale('log')

plt.subplot(2,1,2)
plt.hist(Cs137_sim_pen.optPhotonSumComptonQE, bins=np.arange(0,25000,200), histtype='step', label='Cs137')
plt.hist(Th232_sim_pen.optPhotonSumComptonQE, bins=np.arange(0,100000,250), histtype='step', label='Th232')
plt.hist(AmBe_sim_pen.optPhotonSumComptonQE, bins=np.arange(0,150000,400), histtype='step', label='AmBe')
plt.legend()
plt.yscale('log')

plt.show()





##################################################
#Calculate calibration constant
CE_energy = np.array([0, promath.comptonMax(0.66166), promath.comptonMax(2.614533), promath.comptonMax(4.439)]) #energies in MeVee of Compton edges
# QDC_loc_data_2050 = np.array([0, 1565, 8340, 12550])
# QDC_loc_data_2000 = np.array([0, 1287, 6782, 10620])
QDC_loc_data_1950 = np.array([0, 977, 5344, 8580])
QDC_loc_data_1925 = np.array([0, 810, 4490, 7700])
QDC_loc_data_1900 = np.array([0, 755, 3925, 6813])#19dB
# QDC_loc_data_1900 = np.array([0, 1038, 5760, 9640])#*6.31 #16dB
# QDC_loc_sim = np.array([0, 12900, 69800, 124500])
# QDC_loc_sim_pen = np.array([0, 13400, 73700, 129100])

popt_data_2050, pcov_data_2050 = curve_fit(promath.linearFunc, QDC_loc_data_2050, CE_energy)
# popt_data_2000, pcov_data_2000 = curve_fit(promath.linearFunc, QDC_loc_data_2000, CE_energy)
popt_data_1950, pcov_data_1950 = curve_fit(promath.linearFunc, QDC_loc_data_1950, CE_energy)
popt_data_1925, pcov_data_1925 = curve_fit(promath.linearFunc, QDC_loc_data_1925, CE_energy)
popt_data_1900, pcov_data_1900 = curve_fit(promath.linearFunc, QDC_loc_data_1900, CE_energy)
popt_sim, pcov_sim = curve_fit(promath.linearFunc, QDC_loc_sim, CE_energy)
popt_sim_pen, pcov_sim_pen = curve_fit(promath.linearFunc, QDC_loc_sim_pen, CE_energy)


# plt.scatter(CE_energy, QDC_loc_data_2050, label='data 2050 V')
# plt.scatter(CE_energy, QDC_loc_data_2000,  label='data 2000 V')
plt.scatter(CE_energy, QDC_loc_data_1950,  label='data 1950 V')
plt.scatter(CE_energy, QDC_loc_data_1925, label='data 1925 V')
plt.scatter(CE_energy, QDC_loc_data_1900, label='data 1900 V')
# plt.scatter(QDC_loc_sim, CE_energy, label='sim')
# plt.scatter(QDC_loc_sim_pen, CE_energy, label='sim pen')


# plt.plot(promath.linearFunc(np.arange(0, 1500*att_factor_19dB, 1000), popt_data_2050[0], popt_data_2050[1]), np.arange(0,1500*att_factor_19dB, 1000))
# plt.plot(promath.linearFunc(np.arange(0, 1500*att_factor_19dB, 1000), popt_data_2000[0], popt_data_2000[1]), np.arange(0,1500*att_factor_19dB, 1000))
plt.plot(promath.linearFunc(np.arange(0, 1500*att_factor_19dB, 1000), popt_data_1950[0], popt_data_1950[1]), np.arange(0,1500*att_factor_19dB, 1000))
plt.plot(promath.linearFunc(np.arange(0, 1500*att_factor_19dB, 1000), popt_data_1925[0], popt_data_1925[1]), np.arange(0,1500*att_factor_19dB, 1000))
plt.plot(promath.linearFunc(np.arange(0, 1500*att_factor_19dB, 1000), popt_data_1900[0], popt_data_1900[1]), np.arange(0,1500*att_factor_19dB, 1000))
# plt.plot(np.arange(0,65000, 1000), promath.linearFunc(np.arange(0, 65000, 1000), popt_data_1900[0], popt_data_1900[1]))

# plt.plot(promath.linearFunc(np.arange(0, 1500*att_factor_19dB, 1000), popt_data_2050[0], popt_data_2050[1]), np.arange(0,1500*att_factor_19dB, 1000)) 
# plt.plot(promath.linearFunc(np.arange(0, 1500*att_factor_19dB, 1000), popt_data_2000[0], popt_data_2000[1]), np.arange(0,1500*att_factor_19dB, 1000))
# plt.plot(promath.linearFunc(np.arange(0, 1500*att_factor_19dB, 1000), popt_data_1950[0], popt_data_1950[1]), np.arange(0,1500*att_factor_19dB, 1000))
# plt.plot(promath.linearFunc(np.arange(0, 1500*att_factor_19dB, 1000), popt_data_1925[0], popt_data_1925[1]), np.arange(0,1500*att_factor_19dB, 1000))
# plt.plot(promath.linearFunc(np.arange(0, 1500*att_factor_19dB, 1000), popt_data_1900[0], popt_data_1900[1]), np.arange(0,1500*att_factor_19dB, 1000))

# plt.plot(np.arange(0,1500*att_factor_19dB, 1000), promath.linearFunc(np.arange(0, 1500*att_factor_19dB, 1000), popt_data_1950[0], popt_data_1950[1]))
# plt.plot(np.arange(0,1500*att_factor_19dB, 1000), promath.linearFunc(np.arange(0, 1500*att_factor_19dB, 1000), popt_data_1925[0], popt_data_1925[1]))
# plt.plot(np.arange(0,1500*att_factor_19dB, 1000), promath.linearFunc(np.arange(0, 1500*att_factor_19dB, 1000), popt_data_1900[0], popt_data_1900[1]))
# plt.plot(np.arange(0,15000*att_factor_19dB, 1000), promath.linearFunc(np.arange(0, 15000*att_factor_19dB, 1000), popt_sim[0], popt_sim[1]))
# plt.plot(np.arange(0,15000*att_factor_19dB, 1000), promath.linearFunc(np.arange(0, 15000*att_factor_19dB, 1000), popt_sim_pen[0], popt_sim_pen[1]))

plt.xlabel('MeV$_{ee}$')
plt.ylabel('QDC [channels]')
plt.legend()
plt.show()