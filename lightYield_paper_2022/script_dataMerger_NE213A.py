#!/usr/bin/env python3
import sys
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit

# IMPORT FROM LIBRARY DIRECTORY
sys.path.insert(0, "../library/")
sys.path.insert(0, "../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "sim_analysis/")
sys.path.insert(0, "data_analysis/")

import digidata
import nicholai_plot_helper as nplt
import processing_utility as prout
import processing_math as promath
import processing_data as prodata
import processing_pd as propd
import processing_simulation as prosim


"""
Script will load all data from one deteector in 5 h increments, 
then apply a small threshold cut to reduce statistics and enable one file with all data to be created!
"""

gainOffsets = np.array([1.    , 1.0098, 1.0106, 1.01  , 1.0098, 1.0098, 1.0098, 1.0098,
                        1.0098, 1.0098, 1.01  , 1.0199, 1.0104, 1.0308, 1.0199, 1.0197,
                        0.99  , 1.0098, 1.0098, 1.0197, 1.0199, 1.0108, 0.9999, 0.9999,
                        1.0003, 1.0011, 1.0003, 1.0001, 1.0001, 1.0001, 1.0001, 1.0098,
                        0.9902, 0.9999, 0.9908, 0.9902, 0.9906, 0.991 , 1.0197, 0.9999,
                        0.9807, 0.9999, 0.9813, 0.9801, 0.9702, 0.99  , 0.99  , 0.9902,
                        0.9999, 0.99  , 1.0098, 0.99  , 1.0197, 0.99  , 0.99  , 1.0102,
                        0.9801, 0.99  , 0.9999, 0.9801, 0.9803, 1.0199, 0.9809, 0.9801,
                        0.9902, 0.9813, 0.9807, 1.0098, 0.99  , 0.9999, 1.0098, 0.99  ,
                        0.9803, 1.0098, 0.9999, 1.0098, 1.0005, 1.0001, 1.01  , 0.9702,
                        1.0017, 0.9803, 0.9912, 0.9999, 0.9904, 1.011 , 1.0098, 1.0197,
                        0.9823, 1.01  , 1.0001, 1.0098, 1.0197, 0.99  , 1.0005, 1.0009,
                        0.9906, 0.9704, 0.9999, 0.9999, 1.0116, 0.9902, 0.9906, 0.9914,
                        0.99  , 0.99  , 1.0001, 0.99  , 1.0009, 0.99  , 0.99  , 0.9999,
                        0.9904, 0.9904, 0.9902, 0.9908, 0.9999, 0.9805, 0.9906, 0.9817,
                        0.9902, 0.9803, 0.99  , 0.99  , 0.9807, 0.99  , 0.9817, 0.9718,
                        0.9805, 0.9801, 0.9805, 0.9809, 0.9819, 0.9801, 0.9805, 0.9803,
                        0.9805, 0.9801, 0.99  , 0.9801, 0.9803])
detector = 'NE213A'

path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop
run1 = 2121
runList = np.arange(run1, run1+141)

att_factor = 3.98 #12dB

distance = 0.959
y2_thr = 600#
y3_thr = 600#6985
y4_thr = 600#
y5_thr = 600#

n_thr = 2400

dfFull = pd.DataFrame({'qdc_lg_ch1':[],
                        'qdc_ps_ch1':[],
                        'tof_ch2':[],
                        'tof_ch3':[],
                        'tof_ch4':[],
                        'tof_ch5':[],
                        'amplitude_ch2':[],
                        'amplitude_ch3':[],
                        'amplitude_ch4':[],
                        'amplitude_ch5':[],
                        'qdc_lg_ch2':[],
                        'qdc_lg_ch3':[],
                        'qdc_lg_ch4':[],
                        'qdc_lg_ch5':[],
                        'tofE_ch2':[],
                        'tofE_ch3':[],
                        'tofE_ch4':[],
                        'tofE_ch5':[]})

for i in np.arange(0, len(runList), 5):
    currentRunList      = np.split(runList, [i, i+5])[1] #split runlist into section of 10 events
    currentGainOffsets  = np.split(gainOffsets, [i, i+5])[1]  #split gainoffsets into section of 10 events
    print('-------------------------------------------------')
    print(f'Now working on runs: {currentRunList}')
    PuBe_tof_data = propd.load_parquet_merge_gainadjust(path, currentRunList, keep_col=[    'qdc_lg_ch1', 
                                                                                            'qdc_ps_ch1',
                                                                                            'tof_ch2', 
                                                                                            'tof_ch3', 
                                                                                            'tof_ch4', 
                                                                                            'tof_ch5', 
                                                                                            'amplitude_ch2', 
                                                                                            'amplitude_ch3', 
                                                                                            'amplitude_ch4', 
                                                                                            'amplitude_ch5', 
                                                                                            'qdc_lg_ch2',
                                                                                            'qdc_lg_ch3',
                                                                                            'qdc_lg_ch4',
                                                                                            'qdc_lg_ch5'], 
                                                                                            full=False,
                                                                                            gainOffsets=currentGainOffsets)
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    #   Apply attenuation coefficient
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    PuBe_tof_data.qdc_lg_ch1 = PuBe_tof_data.qdc_lg_ch1*att_factor

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    #   Calibrate ToF
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    flashrange = {'tof_ch2':[59, 73], 'tof_ch3':[59, 73], 'tof_ch4':[59, 73], 'tof_ch5':[59, 73]} #gamma flash fitting ranges
    prodata.tof_calibration(PuBe_tof_data, flashrange, distance, phcut=125, calUnit=1e-9, energyCal=True)
    
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    #   Apply thresholds to Data
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    slice_TMP = PuBe_tof_data.query(f'qdc_lg_ch1>{n_thr} and (qdc_lg_ch2>{y2_thr} or qdc_lg_ch3>{y3_thr} or qdc_lg_ch4>{y4_thr} or qdc_lg_ch5>{y5_thr})')
    
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    #   Merge Data
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    dfFull = pd.concat([dfFull, slice_TMP], ignore_index=False)

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    #   INFO print-out
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    print(f'Number of events currently: {len(dfFull)}')

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#   Save to disk
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
dfFull.to_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/{detector}_data.pkl')

    # plt.hist(prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/NE213A/Ecal', dfFull.qdc_lg_ch1), np.arange(0,10,0.1))
    # # plt.hist(dfFull.query('amplitude_ch3>100').tof_ch3,bins=750)
    # plt.show()

