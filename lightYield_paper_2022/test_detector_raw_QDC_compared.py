PMT_gain = 4e6 #Nominal gain is 7'000'000 at nominal A/lm (model: 9821B p1)
att_factor_9dB = 2.82
att_factor_11dB = 3.55
att_factor_12dB = 3.98
att_factor_15dB = 5.62
att_factor_16dB = 6.31
att_factor_16_5dB = 6.68
att_factor_19dB = 8.91
q = 1.60217662E-19 #charge of a single electron
plot = False


path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop

#NE213A
Cs137_runs =    [3259]#list(range(1656,1666))
Th232_runs =    [3258]#list(range(1671,1676))
AmBe_runs =     [3257]#list(range(1693,1703))
PuBe_runs =     [2121]
CableLengthCorrections = 1.192 #correction constant between inside cave and outside due to different cable lengthsc
NE213A_Cs137 =      propd.load_parquet_merge(path, Cs137_runs,  keep_col=['qdc_lg_ch1'], full=False)*att_factor_12dB
NE213A_Th232 =      propd.load_parquet_merge(path, Th232_runs,  keep_col=['qdc_lg_ch1'], full=False)*att_factor_12dB

#EJ305
Cs137_runs =    [3432]#[3240]
Th232_runs =    [3433]#[3241]
AmBe_runs =     [3434]#[3242]
PuBe_runs =     [3438]#list(range(2930, 2933))
CableLengthCorrections = 1#1.074 #0.97 #correction constant between inside cave and outside due to different cable length
EJ305_Cs137 =      propd.load_parquet_merge(path, Cs137_runs,  keep_col=['qdc_lg_ch1'], full=False)*att_factor_16dB
EJ305_Th232 =      propd.load_parquet_merge(path, Th232_runs,  keep_col=['qdc_lg_ch1'], full=False)*att_factor_16dB


#EJ321P
Cs137_runs =    [2824, 2825]
Th232_runs =    [2826, 2827]
AmBe_runs =     list(range(3025, 3034))
PuBe_runs =     [2423, 2424] 
CableLengthCorrections = 1.07 #correction constant between inside cave and outside due to different cable lengthsc
EJ321P_Cs137 =      propd.load_parquet_merge(path, Cs137_runs,  keep_col=['qdc_lg_ch1'], full=False)*att_factor_9dB
EJ321P_Th232 =      propd.load_parquet_merge(path, Th232_runs,  keep_col=['qdc_lg_ch1'], full=False)*att_factor_9dB

#EJ331
Cs137_runs =    [1952, 1953]
Th232_runs =    [1967, 1968]
AmBe_runs =     list(range(1955, 1960 ))
PuBe_runs =     [1881, 1882]
CableLengthCorrections = 1.05 #correction constant between inside cave and outside due to different cable lengthsc
EJ331_Cs137 =      propd.load_parquet_merge(path, Cs137_runs,  keep_col=['qdc_lg_ch1'], full=False)*att_factor_15dB
EJ331_Th232 =      propd.load_parquet_merge(path, Th232_runs,  keep_col=['qdc_lg_ch1'], full=False)*att_factor_15dB



#make figure
numBins = np.arange(0,20000,200)

plt.figure()
plt.title('Cs137')
plt.hist(NE213A_Cs137.qdc_lg_ch1, histtype='step', lw=2, bins=numBins, label='NE213A')
plt.hist(EJ305_Cs137.qdc_lg_ch1, histtype='step', lw=2, bins=numBins, label='EJ305')
plt.hist(EJ321P_Cs137.qdc_lg_ch1, histtype='step', lw=2, bins=numBins, label='EJ321P')
plt.hist(EJ331_Cs137.qdc_lg_ch1, histtype='step', lw=2, bins=numBins, label='EJ331')
plt.legend()
plt.yscale('log')

numBins = np.arange(0,55000,200)
plt.figure()
plt.title('Th232')
plt.hist(NE213A_Th232.qdc_lg_ch1, histtype='step', lw=2, bins=numBins, label='NE213A')
plt.hist(EJ305_Th232.qdc_lg_ch1, histtype='step', lw=2, bins=numBins, label='EJ305')
plt.hist(EJ321P_Th232.qdc_lg_ch1, histtype='step', lw=2, bins=numBins, label='EJ321P')
plt.hist(EJ331_Th232.qdc_lg_ch1, histtype='step', lw=2, bins=numBins, label='EJ331')
plt.legend()
plt.yscale('log')

plt.show()










