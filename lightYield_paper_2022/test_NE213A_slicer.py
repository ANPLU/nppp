#!/usr/bin/env python3
import sys
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
import pandas as pd
from tqdm import tqdm
from scipy.optimize import curve_fit

# IMPORT FROM LIBRARY DIRECTORY
sys.path.insert(0, "../library/")
sys.path.insert(0, "../")
sys.path.insert(0, "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/library") #Import my own libraries
sys.path.insert(0, "sim_analysis/")
sys.path.insert(0, "data_analysis/")

import digidata
import nicholai_plot_helper as nplt
import processing_utility as prout
import processing_math as promath
import processing_data as prodata
import processing_pd as propd
import processing_simulation as prosim

randTOFRange = [-710e-9,-10e-9] #Define range of random events
randLength = np.abs(randTOFRange[1]-randTOFRange[0]) #Calculate lenght of cut for random events
nLength = 7.749471e-10 #5 MeV +/- 125 keV

distance = 0.959


detector = 'EJ305'
detector = 'NE213A'

df = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/{detector}_data.pkl')

YAPch = 3

queryRand = f'tof_ch{YAPch}>{-710e-9} and tof_ch{YAPch}<={-10e-9} and qdc_lg_ch{YAPch}>800'
queryNeutron = f'tofE_ch{YAPch}>{4.875} and tofE_ch{YAPch}<={5.125} and qdc_lg_ch{YAPch}>800'


#QDC
numBins = np.arange(0, 40000, 500)
# numBins = np.arange(0, 80000, 1500)
yn, xn = np.histogram(df.query(queryNeutron).qdc_lg_ch1, bins=numBins)
xn = prodata.getBinCenters(xn)

yn2, xn2 = np.histogram(df.query(queryNeutron+'and qdc_ps_ch1>0.3').qdc_lg_ch1, bins=numBins)
xn2 = prodata.getBinCenters(xn2)

yn3, xn3 = np.histogram(df.query(queryNeutron+f'and qdc_lg_ch{YAPch}>6500').qdc_lg_ch1, bins=numBins)
xn3 = prodata.getBinCenters(xn3)

yrand, xrand = np.histogram(df.query(queryRand).qdc_lg_ch1, bins=numBins)
xrand = prodata.getBinCenters(xrand)

R = nLength/randLength

plt.step(xn, yn, label='neutron')
plt.step(xn2, yn2, label='neutron + PS>0.3')
plt.step(xn3, yn3, label='neutron + YAPqdc>6500')
plt.step(xrand, yrand*R, label='randoms')
plt.title('Tn = 5 +/- 0.125 MeV')
plt.xlabel('QDC')
plt.ylabel('counts')
plt.legend()
plt.show()




#TOF
numBins = np.arange(-750e-9, 300e-9, 1e-9)
yn, xn = np.histogram(df.query(queryNeutron).tof_ch3, bins=numBins)
xn = prodata.getBinCenters(xn)

yrand, xrand = np.histogram(df.query(queryRand).tof_ch3, bins=numBins)
xrand = prodata.getBinCenters(xrand)

plt.step(xn, yn)
plt.step(xrand, yrand)
plt.show()

 












path_sim = '/media/gheed/Seagate_Expansion_Drive1/data/simulation/'
names = [   'evtNum', 
            'optPhotonSum', 
            'optPhotonSumQE', 
            'optPhotonSumCompton', 
            'optPhotonSumComptonQE', 
            'optPhotonSumNPQE', 
            'xLoc', 
            'yLoc', 
            'zLoc', 
            'CsMin', 
            'optPhotonParentID', 
            'optPhotonParentCreatorProcess', 
            'optPhotonParentStartingEnergy',
            'edep_total',
            'edep_gamma',
            'edep_neutron',
            'nScatter_total',
            'nScatter_neutron']

detector = 'NE213A'

distance = 0.959

df = pd.read_pickle(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/{detector}_data.pkl')

df.qdc_lg_ch1 = prodata.calibrateMyDetector(f'/media/gheed/Seagate_Expansion_Drive1/data/lightYield_paper/{detector}/Ecal/', df.qdc_lg_ch1*0.83)
#make random lengths
randTOFRange = [-710e-9,-10e-9] #Define range of random events
randLength = np.abs(randTOFRange[1]-randTOFRange[0]) #Calculate lenght of cut for random events

numBins = [ np.arange(0.011, 7, 0.0202),#1500 keV
            np.arange(0.011, 7, 0.0244),
            np.arange(0.011, 7, 0.0286),
            np.arange(0.011, 7, 0.0328),
            np.arange(0.011, 7, 0.0370),
            np.arange(0.011, 7, 0.0412),
            np.arange(0.011, 7, 0.0454),
            np.arange(0.011, 7, 0.0496),#3250 keV
            np.arange(0.011, 7, 0.0538),
            np.arange(0.011, 7, 0.0580),
            np.arange(0.011, 7, 0.0622),
            np.arange(0.011, 7, 0.0664),
            np.arange(0.011, 7, 0.0706),
            np.arange(0.011, 7, 0.0748),
            np.arange(0.011, 7, 0.0790),
            np.arange(0.011, 7, 0.0832),
            np.arange(0.011, 7, 0.0874),
            np.arange(0.011, 7, 0.0916),
            np.arange(0.011, 7, 0.050),
            np.arange(0.011, 7, 0.1000)]#6250 keV
            
# numBins = [ np.arange(0, 40000, 202),#1500 keV
#             np.arange(0, 40000, 244),
#             np.arange(0, 40000, 286),
#             np.arange(0, 40000, 328),
#             np.arange(0, 40000, 370),
#             np.arange(0, 40000, 412),
#             np.arange(0, 40000, 454),
#             np.arange(0, 40000, 496),#3250 keV
#             np.arange(0, 40000, 538),#
#             np.arange(0, 40000, 580),#
#             np.arange(0, 40000, 622),#
#             np.arange(0, 40000, 664),#
#             np.arange(0, 40000, 706),#
#             np.arange(0, 40000, 748),#
#             np.arange(0, 40000, 790),#5000keV
#             np.arange(0, 40000, 832),
#             np.arange(0, 40000, 874),
#             np.arange(0, 40000, 916),
#             np.arange(0, 40000, 958),
#             np.arange(0, 40000, 1000)]#6250 keV

PSthr = 0.3

Ythr = 800

randQDCy2 = df.query(f'tof_ch{2}>{randTOFRange[0]} and tof_ch{2}<={randTOFRange[1]} and qdc_lg_ch{2}>{Ythr} and qdc_ps_ch1>{PSthr}').qdc_lg_ch1
randQDCy3 = df.query(f'tof_ch{3}>{randTOFRange[0]} and tof_ch{3}<={randTOFRange[1]} and qdc_lg_ch{3}>{Ythr} and qdc_ps_ch1>{PSthr}').qdc_lg_ch1
randQDCy4 = df.query(f'tof_ch{4}>{randTOFRange[0]} and tof_ch{4}<={randTOFRange[1]} and qdc_lg_ch{4}>{Ythr} and qdc_ps_ch1>{PSthr}').qdc_lg_ch1
randQDCy5 = df.query(f'tof_ch{5}>{randTOFRange[0]} and tof_ch{5}<={randTOFRange[1]} and qdc_lg_ch{5}>{Ythr} and qdc_ps_ch1>{PSthr}').qdc_lg_ch1

# randPSy2 = df.query(f'tof_ch{2}>{randTOFRange[0]} and tof_ch{2}<={randTOFRange[1]} and qdc_lg_ch{2}>{Ythr} and qdc_ps_ch1>{PSthr}').qdc_ps_ch1
# randPSy3 = df.query(f'tof_ch{3}>{randTOFRange[0]} and tof_ch{3}<={randTOFRange[1]} and qdc_lg_ch{3}>{Ythr} and qdc_ps_ch1>{PSthr}').qdc_ps_ch1
# randPSy4 = df.query(f'tof_ch{4}>{randTOFRange[0]} and tof_ch{4}<={randTOFRange[1]} and qdc_lg_ch{4}>{Ythr} and qdc_ps_ch1>{PSthr}').qdc_ps_ch1
# randPSy5 = df.query(f'tof_ch{5}>{randTOFRange[0]} and tof_ch{5}<={randTOFRange[1]} and qdc_lg_ch{5}>{Ythr} and qdc_ps_ch1>{PSthr}').qdc_ps_ch1

dE = 0.125
for i, E in enumerate(np.arange(1.5, 6.50, dE*2)):
    E = np.round(E, 2)
    E1 = round(E-dE/2, 3)
    E2 = round(E+dE/2, 3)
    t1 = prodata.EnergytoTOF(E1, distance)
    t2 = prodata.EnergytoTOF(E2, distance)

    print(f'Processing: Tn = {E} MeV')

    nLength = t1-t2
  
    nQDCy2 = df.query(f'tofE_ch{2}>{E-dE} and tofE_ch{2}<={E+dE} and qdc_lg_ch{2}>{Ythr} and qdc_ps_ch1>{PSthr}').qdc_lg_ch1
    nQDCy3 = df.query(f'tofE_ch{3}>{E-dE} and tofE_ch{3}<={E+dE} and qdc_lg_ch{3}>{Ythr} and qdc_ps_ch1>{PSthr}').qdc_lg_ch1
    nQDCy4 = df.query(f'tofE_ch{4}>{E-dE} and tofE_ch{4}<={E+dE} and qdc_lg_ch{4}>{Ythr} and qdc_ps_ch1>{PSthr}').qdc_lg_ch1
    nQDCy5 = df.query(f'tofE_ch{5}>{E-dE} and tofE_ch{5}<={E+dE} and qdc_lg_ch{5}>{Ythr} and qdc_ps_ch1>{PSthr}').qdc_lg_ch1

    # nPSy2 = df.query(f'tofE_ch{2}>{E-dE} and tofE_ch{2}<={E+dE} and qdc_lg_ch{2}>{Ythr} and qdc_ps_ch1>{PSthr}').qdc_ps_ch1
    # nPSy3 = df.query(f'tofE_ch{3}>{E-dE} and tofE_ch{3}<={E+dE} and qdc_lg_ch{3}>{Ythr} and qdc_ps_ch1>{PSthr}').qdc_ps_ch1
    # nPSy4 = df.query(f'tofE_ch{4}>{E-dE} and tofE_ch{4}<={E+dE} and qdc_lg_ch{4}>{Ythr} and qdc_ps_ch1>{PSthr}').qdc_ps_ch1
    # nPSy5 = df.query(f'tofE_ch{5}>{E-dE} and tofE_ch{5}<={E+dE} and qdc_lg_ch{5}>{Ythr} and qdc_ps_ch1>{PSthr}').qdc_ps_ch1

    x_neutron_QDC, y_neutron_QDC2, y_err2 = prodata.randomTOFSubtractionQDC(nQDCy2,
                                                                    randQDCy2,
                                                                    nLength, 
                                                                    randLength,
                                                                    numBins[i],
                                                                    error=True, 
                                                                    plot=False)

    x_neutron_QDC, y_neutron_QDC3, y_err3 = prodata.randomTOFSubtractionQDC(nQDCy3,
                                                                    randQDCy3,
                                                                    nLength, 
                                                                    randLength,
                                                                    numBins[i], 
                                                                    error=True,
                                                                    plot=False)
    
    x_neutron_QDC, y_neutron_QDC4, y_err4 = prodata.randomTOFSubtractionQDC(nQDCy4,
                                                                    randQDCy4,
                                                                    nLength,
                                                                    randLength,
                                                                    numBins[i],
                                                                    error=True, 
                                                                    plot=False)
    
    x_neutron_QDC, y_neutron_QDC5, y_err5 = prodata.randomTOFSubtractionQDC(nQDCy5,
                                                                    randQDCy5,
                                                                    nLength, 
                                                                    randLength,
                                                                    numBins[i], 
                                                                    error=True,
                                                                    plot=False)

    # x_neutron_PS, y_neutron_PS5 = prodata.randomTOFSubtractionQDC(  nPSy2,
    #                                                                 randPSy5,
    #                                                                 nLength, 
    #                                                                 randLength,
    #                                                                 np.arange(0,1,0.01), 
    #                                                                 plot=False)

    #import simulation data                                                                
    smearFactor = 0.175
    neutron_sim_iso = pd.read_csv(path_sim + f"{detector}/neutrons/neutron_range/{int(E*1000)}keV/isotropic/CSV_optphoton_data_sum.csv", names=names)
    neutron_sim_iso.optPhotonSumNPQE = neutron_sim_iso.optPhotonSumNPQE.apply(lambda x: prosim.gaussSmear(x, smearFactor)) 
    #error propagation
    error = promath.errorPropAdd([])

    plt.subplot(4, 5, i+1)
    # if E == 5.0:
    
    plt.errorbar(x_neutron_QDC, (y_neutron_QDC2 + y_neutron_QDC3 + y_neutron_QDC4 + y_neutron_QDC5), yerr=promath.errorPropAdd([y_err2,y_err3,y_err4,y_err5]),  linestyle='')
    plt.scatter(x_neutron_QDC, (y_neutron_QDC2 + y_neutron_QDC3 + y_neutron_QDC4 + y_neutron_QDC5), s=5)
    # plt.hist(neutron_sim_iso.edep_neutron/1000, bins=numBins[i], histtype='step')
    # plt.hist(neutron_sim_iso.edep_total/1000, bins=numBins[i], histtype='step')
    plt.hist(0.00423856*neutron_sim_iso.optPhotonSumNPQE - 0.06867948, bins=numBins[i], histtype='step')
    plt.title(E)

plt.show()



#####################################################
#####################################################
#check new and old NE213A simulations
Cs137_old = pd.read_csv(path_sim + f"{detector}/Cs137/pencilbeam/CSV_optphoton_data_sum.csv", names=['evtNum', 'optPhotonSum', 'optPhotonSumQE', 'optPhotonSumCompton', 'optPhotonSumComptonQE', 'xLoc', 'yLoc', 'zLoc', 'CsMin', 'optPhotonParentID', 'optPhotonParentCreatorProcess', 'optPhotonParentStartingEnergy','edep'])
Cs137_new = pd.read_csv(path_sim + f"{detector}/Cs137/pencilbeam/new/CSV_optphoton_data_sum.csv", names=names)

#####################################################
#####################################################















plt.hist(Cs137_old.edep, bins=500, histtype='step', label='old')
plt.hist(Cs137_new.edep_total, bins=500, histtype='step', label='new')
plt.title('NE213A')
plt.legend()
plt.show()


plt.hist(Cs137_old.optPhotonSumComptonQE*2.1, bins=np.arange(5,300,4), histtype='step', label='old')
plt.hist(Cs137_new.optPhotonSumComptonQE, bins=np.arange(5,300,4), histtype='step', label='new')
plt.title('NE213A')
plt.legend()
plt.show()