import dask.array as da, dask.dataframe as dd
import pandas as pd
import digviz as dv
import timeit
import numpy as np

def test_analysis_map_partitions():
    df = dv.load_h5_da("/drive2/SKB/gamma_tests/jadaq-00166.h5")
    # calculate baseline based on the first few samples
    # def = baseline(df):
    #     df.samples.apply(lambda x: np.mean(x[:20]), meta=df['baseline'])
    df['baseline'] = np.float64(0)
    df['baseline'] = df.samples.map_partitions(lambda x: np.mean(x[:20]), meta=df['baseline'])
    # # calculate simple pulse-height
    # df['PH'] = np.int16(0)
    # df['PH'] = df.samples.apply(lambda x: min(x), meta=df['PH']) - df['baseline']
    # # calculate overshoot
    # df['overshoot'] = np.int16(0)
    # df['overshoot'] = df.samples.apply(lambda x: max(x), meta=df['overshoot'])-df['baseline']
    # # find number of samples
    # nsamples = df.samples.head(1).apply(lambda x: len(x)).values[0]
    # # calculate simple charge-integration
    # df['QDC'] = np.int32(0)
    # df['QDC'] = df.samples.apply(lambda x: np.sum(x), meta=df['QDC'])-(nsamples*df['baseline'])
    df.to_parquet("/drive2/jadaq-00166_map_partitions", engine='pyarrow')
    # return df

def test_analysis_apply():
    df = dv.load_h5_da("/drive2/SKB/gamma_tests/jadaq-00166.h5")
    # calculate baseline based on the first few samples
    df['baseline'] = np.float64(0)
    df['baseline'] = df.samples.apply(lambda x: np.mean(x[:20]), meta=df['baseline'])
    # # calculate simple pulse-height
    # df['PH'] = np.int16(0)
    # df['PH'] = df.samples.apply(lambda x: min(x), meta=df['PH']) - df['baseline']
    # # calculate overshoot
    # df['overshoot'] = np.int16(0)
    # df['overshoot'] = df.samples.apply(lambda x: max(x), meta=df['overshoot'])-df['baseline']
    # # find number of samples
    # nsamples = df.samples.head(1).apply(lambda x: len(x)).values[0]
    # # calculate simple charge-integration
    # df['QDC'] = np.int32(0)
    # df['QDC'] = df.samples.apply(lambda x: np.sum(x), meta=df['QDC'])-(nsamples*df['baseline'])
    df.to_parquet("/drive2/jadaq-00166_apply", engine='pyarrow')
    # return df

if __name__ == '__main__':
    import pyarrow
    import dask
    print('Type \'1\': map_partitions test:')
    print('Type \'2\': apply test:')
    ans = input()
    if ans == '1':
        print('Running map_partions...')
        timer = timeit.timeit(test_analysis_map_partitions, number=1)
        print("PyArrow version: " + pyarrow.__version__)
        print('Dask version: ' + dask.__version__)
        print(f'Time results: {timer} seconds')
    elif ans == '2':
        print('Running apply...')
        timer = timeit.timeit(test_analysis_apply, number=1)
        print("PyArrow version: " + pyarrow.__version__)
        print('Dask version: ' + dask.__version__)
        print(f'Time results: {timer} seconds')
    else:
        print('restart, must choose an options!')
    
    
