import dask.array as da, dask.dataframe as dd
import pandas as pd
import digviz as dv

import timeit
import numpy as np


def test_analysis_parquet_pyarrow():
    df = dv.load_h5_da("/drive2/SKB/gamma_tests/jadaq-00166.h5")
    # calculate baseline based on the first few samples
    df['baseline'] = np.float64(0)
    df['baseline'] = df.samples.apply(lambda x: np.mean(x[:20]), meta=df['baseline'])
    # calculate simple pulse-height
    df['PH'] = np.int16(0)
    df['PH'] = df.samples.apply(lambda x: min(x), meta=df['PH']) - df['baseline']
    # calculate overshoot
    df['overshoot'] = np.int16(0)
    df['overshoot'] = df.samples.apply(lambda x: max(x), meta=df['overshoot'])-df['baseline']
    # find number of samples
    nsamples = df.samples.head(1).apply(lambda x: len(x)).values[0]
    # calculate simple charge-integration
    df['QDC'] = np.int32(0)
    df['QDC'] = df.samples.apply(lambda x: np.sum(x), meta=df['QDC'])-(nsamples*df['baseline'])
    df.to_parquet("/drive2/parquet166_pyarrow", engine='pyarrow')

if __name__ == '__main__':
    import pyarrow
    print("PyArrow version: " + pyarrow.__version__)
    print(timeit.timeit(test_analysis_parquet_pyarrow, number=1))
