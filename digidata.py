#!/usr/bin/env python3
"""

loading routines for data files recorded with CAEN digitizers by jadaq

"""

import sys
import os
import argparse
import datetime
import math

import numpy as np
import pandas as pd

import dask.array as da, dask.dataframe as dd
import dask

import logging

import h5py
import pyarrow.parquet as pq

from enum import Enum

import helpers as hlp # helper routines
import colorer # colored log output

class DataType(Enum):
    # define the supported data formats (that need individual parsing)
    DPPQDC = 1
    DPPQDC_EXT = 2
    STANDARD = 3
    DPPQDC_MIXED = 257
    DPPQDC_EXT_MIXED = 258

def read_csv(file_name):
    '''
    Loads data from a CSV file. Formats currently supported:
    - DPP-QDC data (XX740-family)
    '''
    return pd.read_csv(file_name, sep=" ", header=None, names=['ts','dig','ch','ph'])

def parquet_getColNames(fname):
    """ returns column names stored in parquet data stored in file 'fname' """
    pfile = pq.ParquetFile(fname)
    return [n for n in pfile.schema.to_arrow_schema().names if not '__index' in n and not n == 'index'] # filter index column

def parquet_hasRows(fname):
    """ returns true if parquet data stored in file 'fname' has at least one row """
    pfile = pq.ParquetFile(fname)
    return pfile.metadata.num_rows>0

def list_parquet(path):
    """ returns a list with full path to all parquet files in a directory 'path' which have non-empty tables stored """
    pfiles = [f for f in os.listdir(path)
              if os.path.isfile(os.path.join(path, f))
              and f.endswith('parquet')] # all parquet files in directory
    return [os.path.join(path, f) for f in pfiles if parquet_hasRows(os.path.join(path, f)) ]

def read_parquet(path, ignore_cols = [], filters = None):
    """reads a directory with parquet files and returns a Dask dataframe. Ignores
columns with names matching an entry in 'ignore_cols' list. Includes only files
with non-empty dataframes stored in them."""
    log = hlp.init_logging('digidata')  # set up logging
    pfiles = list_parquet(path) # NOTE this works around issues with
                                # TypeError/inconsistent schemas when opening
                                # partitioned parquet files if any of them
                                # contain empty data frames
                                # (https://github.com/dask/dask/issues/4194);
                                # might be solvable by providing a 'schema='
                                # argument when calling 'to_parquet'
    cols = parquet_getColNames(pfiles[0])
    log.debug(f"Found {len(pfiles)} non-empty parquet files with columns {cols}, ignoring {ignore_cols}")
    # NOTE only the pyarrow engine works reliable with our ndarrays stored in the sample column of the dataframe
    ddf = dd.read_parquet(pfiles, columns = [c for c in cols if not c in ignore_cols], engine="pyarrow", filters = filters)
    return ddf

def read_jadaq_timestamps(file_name, singlerow=False):
    """ returns a DataFrame with DateTime timestamps for each data block of a jadaq HDF5 file and the corresponding index range when loading the actual file into a DataFrame with read_jadaq.

    Allows run-time calculations using: read_jadaq_timestamp(file).clock.diff().sum()

    WARNING: data stored with jadaq prior commit 348dbe45 have a '0' as first clock time stamp which might lead to miscalculations if not handled properly.
    """
    log = hlp.init_logging('digidata')  # set up logging

    f = h5py.File(file_name, 'r')
    # TODO check version of data format in file and give out warning if newer than expected

    all_digi = list(f.keys())
    log.debug("File {} contains data from digitizers: {}".format(file_name, ",".join(all_digi)))

    # list holding dataframes (to be concatenated later)
    dfs = []

    for digi in all_digi:
        # TODO catch exception should data format not be known
        dformat = DataType(f[digi].attrs.get('JADAQ_DATA_TYPE'))
        log.debug(f"Data format for digitizer {digi}: {dformat}")

        if dformat == DataType.STANDARD:
            if singlerow:
                # adjust number of channels accordingly
                nch = 1
            else:
                # each event consists of several active channels; get number from channel mask of first event
                first_data = list(f[digi].keys())[0]
                first_event = f[digi][first_data][0]
                chmask = first_event['channelMask']
                nch = bin(chmask).count('1') # count active channels in mask

        nblocks = len(list(f[digi].keys()))

        # loop over data blocks
        dbpos = 0 # count number of evts processed
        clocks = np.zeros(nblocks, dtype=np.int) # clock time stamps
        idxstart = np.zeros(nblocks, dtype=np.int) # start of block
        idxstop = np.zeros(nblocks, dtype=np.int)  # stop of block
        evtfirst = np.zeros(nblocks, dtype=np.int) # first evt no
        evtlast = np.zeros(nblocks, dtype=np.int)  # last evt no
        for idx, dblock in enumerate(list(f[digi].keys())):
            log.debug(f"Shape of current H5 block: {f[digi][dblock].shape}")
            clocks[idx] = int(dblock)
            if dformat == DataType.STANDARD:
                idxstart[idx] = dbpos*nch
                idxstop[idx] = dbpos*nch+len(f[digi][dblock])*nch - 1
                evtfirst[idx] = f[digi][dblock][0]['eventNo']
                evtlast[idx] = f[digi][dblock][-1]['eventNo']
            dbpos = dbpos + len(f[digi][dblock])
        # create DataFrame from arrays
        dfs.append(pd.DataFrame({"clock":clocks,
                                 "startidx":idxstart,
                                 "stopidx":idxstop,
                                 "firstevt":evtfirst,
                                 "lastevt":evtlast,
                                 "digi":np.array([digi for _ in range(nblocks)])}))
        # adjust clock format to datetime
        dfs[-1]["clock"] = pd.to_datetime(dfs[-1]["clock"], unit='ms')
    # concatenate results (if needed)
    if len(dfs) == 1:
        return dfs[0]
    else:
        return pd.concat(dfs)


def read_jadaq(file_name, singlerow=False, redfacnpart = 1, append_gts = False, sorted_divisions=False):
    """Reads in digitizer event data from an HDF5 file created by jadaq into a Dask DataFrame.

    Parameters:
    - file_name: file name to read in
    - singlerow: keep full event on a single row of the DataFrame. Default: one row per channel
    - redfacnpart: factor by which the number of partitions of the resulting dataframe are reduced
    - append_gts: append a global time stamp as saved by Jadaq for each data block (based on the DAQ computer's clock)
    - sorted_divisions: indicates to Dask that the DataFrame is sorted along the index; slows down initial loading but increases speed on operations along the index.

    Supports so far:
    - JADAQ Standard waveform samples format (recorded with XX751)

    NOTE The Dask scheduler "processes" does not work with this routine due to
    local variables (i.e. the HDF5 file) and issues with parallel access to a
    single HDF5 file.

    """

    # TODO by wrapping the HDF5 file access into a helper function that returns
    # a given chunk one might be able to isolate the "separatechannel" function
    # enough from any local variables so that the loading could be performed
    # using the process scheduler of dask (fully parallelized). Possibly
    # related: https://github.com/dask/dask/issues/1777

    # TODO digitizer should not be stored as string but as model+serial shifted into 32bit
    log = hlp.init_logging('digidata')  # set up logging

    f = h5py.File(file_name, 'r')
    # TODO check version of data format in file and give out warning if newer than expected

    all_digi = list(f.keys())
    log.info("File {} contains data from digitizers: {}".format(file_name, ",".join(all_digi)))

    # list holding dataframes (to be concatenated later)
    dfs = []

    for digi in all_digi:
        # TODO catch exception should data format not be known
        dformat = DataType(f[digi].attrs.get('JADAQ_DATA_TYPE'))
        log.info(f"Data format for digitizer {digi}: {dformat}")

        if dformat == DataType.STANDARD:
            # data length calculation
            nevts = sum([len(f[digi][g]) for g in f[digi].keys()])
            # each event consists of several active channels; get number from channel mask of first event
            first_data = list(f[digi].keys())[0]
            first_event = f[digi][first_data][0]
            chmask = first_event['channelMask']
            nch = bin(chmask).count('1') # count active channels in mask
            # compose list of active channels from channel mask
            channels = [position for position, bit in enumerate([(chmask >> bit) & 1 for bit in range(8)]) if bit]
            # adjust number of events accordingly
            nevts *= nch
            # find number of samples
            nsamples = first_event['num_samples']

            def seperatechannel(d, nch, pos, nevt, gts):
                """ helper function to separate channels in event data and return a df with one row per channel per event. 'd' is expected to be numpy ndarray with event data, nch the number of channels and nevt the number of events/rows and pos the number of events seen so far (for the index generation) as well as a global timestamp to assign to this block."""
                log.debug("Splitting")
                # split samples according to channel
                s = np.split(d['samples'], nch, axis=1)
                # if we keep the event on a single row:
                if singlerow:
                    if gts:
                        # with global time stamp
                        evtform = np.dtype([('evtno', np.uint32), ('global_ts', "datetime64[ns]"), ('ts', np.uint32), ('digitizer', np.object)]
                                           + [(f'samples_ch{channels[ch]}', np.object) for ch in range(nch)])
                    else:
                        evtform = np.dtype([('evtno', np.uint32), ('ts', np.uint32), ('digitizer', np.object)]
                                           + [(f'samples_ch{channels[ch]}', np.object) for ch in range(nch)])
                    result = np.empty((nevt,), dtype=evtform)
                    result['evtno'] = d['eventNo']
                    if gts:
                        result['global_ts'] = np.datetime64(gts, 'ms')
                    result['ts'] = d['time']
                    result['digitizer'] = str(digi) # TODO str wastes storage space here; keep a uint8 digitizer index and a map to the name instead?
                    for ch in range(nch):
                        result[f'samples_ch{channels[ch]}']= tuple(s[ch])
                    index = range(pos,pos+nevt)
                else:
                    if gts:
                        # with global time stamp
                        evtform = np.dtype([('evtno', np.uint32), ('global_ts', "datetime64[ns]"), ('ts', np.uint32), ('channel', np.uint8), ('digitizer', np.object),('samples', np.object)])
                    else:
                        evtform = np.dtype([('evtno', np.uint32), ('ts', np.uint32), ('channel', np.uint8), ('digitizer', np.object),('samples', np.object)])
                    index=range(pos*nch,pos*nch+nevt*nch)
                    result = np.empty((nevt*nch,), dtype=evtform)
                    # assign data to the output array (offset by nch)
                    for ch in range(nch):
                        result['evtno'][ch::nch] = d['eventNo']
                        if gts:
                            result['global_ts'][ch::nch] = np.datetime64(gts, 'ms')
                        result['ts'][ch::nch] = d['time']
                        result['channel'][ch::nch] = np.uint8(channels[ch])
                        result['digitizer'][ch::nch] = str(digi) # TODO str wastes storage space here; keep a uint8 digitizer index and a map to the name instead?
                        result['samples'][ch::nch] = tuple(s[ch])
                return pd.DataFrame(result, index=index)

            # run a small test-sample to get the meta information right
            sample = seperatechannel(f[digi][first_data][0:2], nch, pos=0,
                                     nevt=f[digi][first_data][0:2].size,
                                     gts=(int(np.datetime64(datetime.datetime.now()).astype('int64')/1e3) if append_gts else None))

        nblocks = len(list(f[digi].keys()))
        log.info("Found a total of {} samples (or events) for {} active channels stored in {} data blocks".format(nevts, len(channels), nblocks))

        # loop over data blocks
        dbpos = 0 # count number of evts processed
        for dblock in list(f[digi].keys()):
            # create Dask delayed object from HDF5 data for this data block
            d = da.from_array(f[digi][dblock], chunks=f[digi][dblock].shape, lock=True) # use lock to prevent concurrency issues with HDF5
            log.debug(f"Shape of current H5 block ({dblock}): {f[digi][dblock].shape}")
            if dformat == DataType.STANDARD:
                if append_gts:
                    gts = int(dblock)
                else:
                    gts = None
                dfs.append(dask.delayed(seperatechannel)(d, nch, pos = dbpos,
                                                         nevt=len(f[digi][dblock]),
                                                         gts=gts))
            dbpos = dbpos + len(f[digi][dblock])

    # create Dask DataFrame from list of Dask delayed
    ddf = dd.from_delayed(dfs, meta=sample, divisions="sorted" if sorted_divisions else None)
    # reduce the number of partitions if requested (chunk size in jadaq's HDF5 files tends to be very small)
    if not math.isclose(redfacnpart, 1):
        newnpart = int(ddf.npartitions//redfacnpart)
        log.info(f"Adjusting number of partitions from {ddf.npartitions} to {max(1, newnpart)}")
        ddf = ddf.repartition(npartitions=max(1, newnpart)) # divide by 20, keep at least 1
    log.info(f"File read-in done")
    return ddf

def read_jadaq_pd(file_name, max_evts = None):
    """
    Loads digitizer event data from a Jadaq HDF5 file into a Pandas DataFrame.

    Supports so far:
    - JADAQ Standard waveform samples format (recorded with XX751)
    """
    log = hlp.init_logging('digidata')  # set up logging

    f = h5py.File(file_name, 'r')
    # TODO check version of data format in file and give out warning if newer than expected

    all_digi = list(f.keys())
    log.info("File {} contains data from digitizers: {}".format(file_name, ",".join(all_digi)))

    dfs = []

    for digi in all_digi:
        # TODO catch exception should data format not be known
        dformat = DataType(f[digi].attrs.get('JADAQ_DATA_TYPE'))
        log.info(f"Data format for digitizer {digi}: {dformat}")

        if dformat == DataType.STANDARD:
            # data length calculation
            nevts = sum([len(f[digi][g]) for g in f[digi].keys()])
            # each event consists of several active channels; get number from channel mask of first event
            first_data = list(f[digi].keys())[0]
            first_event = f[digi][first_data][0]
            chmask = first_event[1]
            nch = bin(chmask).count('1') # count active channels in mask
            # compose list of active channels from channel mask
            channels = [position for position, bit in enumerate([(chmask >> bit) & 1 for bit in range(8)]) if bit]
            # adjust number of events accordingly
            nevts *= nch
            # define data type
            evtform = np.dtype([('evtno', np.uint32), ('ts', np.uint32), ('ch', np.uint8),
                                ('samples', np.object)])
            # alternative solution (no significant speedup observed so far):
            # (requires 'tolist()' call when converting to DataFrame)
            # evtform = np.dtype([('evtno', np.uint32), ('ts', np.uint32), ('ch', np.uint8),
            #                    ('samples', np.uint16, len(first_event[4]))])


        nblocks = len(list(f[digi].keys()))
        log.info("Found a total of {} samples (or events) for {} active channels stored in {} data blocks".format(nevts, len(channels), nblocks))

        if max_evts and max_evts<nevts:
            log.info(f"Limiting readout to {max_evts} of {nevts} events")
            nevts = max_evts

        # reserve memory for numpy data array holding events
        data = np.zeros(nevts, dtype=evtform)

        # loop over all events in all data blocks stored for this digitizer
        idx = 0
        try:
            for dblock in list(f[digi].keys()):
                for evt in f[digi][dblock]:
                    if dformat == DataType.STANDARD:
                        # split the combined samples for all channels and create single events
                        #evt[2]=channelnumber, evt[0]=timestamp, s=samples, i=nch=sum of 1 in binary rep of channel mask
                        for i,s in enumerate(np.split(evt[4],nch)):
                            data[idx*nch+i] = tuple([evt[2], evt[0], np.uint8(channels[i]), s])
                    idx += 1
        except IndexError:
            log.info(f"Specified read-in limit reached")

#        df = pd.DataFrame(data.tolist())
        df = pd.DataFrame(data)
        # add an identifier column for the digitizer model+serial
        df["digitizer"] = str(digi) # TODO str wastes storage space here; keep a uint8 digitizer index and a map to the name instead?
        dfs.append(df)
    log.info(f"File read-in done")

    # concatenate results (if needed)
    if len(dfs) == 1:
        return dfs[0]
    else:
        return pd.concat(dfs)




if __name__ == "__main__":
    log = hlp.init_logging('digidata')

    # command line argument parsing
    argv = sys.argv
    progName = os.path.basename(argv.pop(0))
    parser = argparse.ArgumentParser(
        prog=progName,
        description=
        "Read data files recorded with CAEN digitizers by Jadaq"
    )
    parser.add_argument(
        "-l",
        "--log-level",
        default="info",
        help=
        "Sets the verbosity of log messages where LEVEL is either debug, info, warning or error",
        metavar="LEVEL")
    parser.add_argument(
        "file",
        nargs='+',
        help=
        "The csv data file(s) to be processed; additional info STRING to be included in the plot legend can be added by specifiying FILE:STRING"
    )

    # parse the arguments
    args = parser.parse_args(argv)
    # set the logging level
    numeric_level = getattr(logging, "DEBUG",
                            None)  # set default
    if args.log_level:
        # Convert log level to upper case to allow the user to specify --log-level=DEBUG or --log-level=debug
        numeric_level = getattr(logging, args.log_level.upper(), None)
        if not isinstance(numeric_level, int):
            log.error('Invalid log level: %s' % args.log_level)
            sys.exit(2)
    log.setLevel(numeric_level)

    # parse file names and extract additionally provided info
    fileNames = []
    fileDescr = []
    for thisFile in args.file:
        s = thisFile.strip().split(':', 1)  # try to split the string
        if (len(s) == 1):
            # didn't work, only have one entry; use file name instead
            fileNames.append(s[0])
            fileDescr.append(s[0])
        else:
            fileNames.append(s[0])
            fileDescr.append(s[1])

    log.debug("Command line arguments used: %s ", args)
    log.debug("Libraries loaded:")
    log.debug("   - Matplotlib version {}".format(matplotlib.__version__))
    log.debug("   - Pandas version {}".format(pd.__version__))
    log.debug("   - Numpy version {}".format(np.__version__))

    msrmts = [] # list to hold all measurements
    for idx, f in enumerate(fileNames):
        df = read_jadaq(f)
        if df is None:
            log.error("Error encountered, skipping file {}".format(f))
            continue
        msrmts.append(df)
        mem_used = df.memory_usage(deep=True).sum()
        log.info("Approximately {} MB of memory used for data loaded from {}".format(round(mem_used/(1024*1024),2),f))

    print(" Interactive IPython shell ")
    print(" ========================= ")
    print(" Quick command usage:")
    print("  - 'who' or 'whos' to see all (locally) defined variables")
    import IPython
    IPython.embed()

    if log.error.counter>0:
        log.warning("There were "+str(log.error.counter)+" error messages reported")
