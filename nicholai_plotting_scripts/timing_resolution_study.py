import sys
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import dask.dataframe as dd
import dask.array as da
import random
from math import cos, pi, radians, factorial
from scipy import signal
from scipy.stats import chisquare
from scipy.integrate import quad
from scipy.signal import find_peaks
from tqdm import tqdm
from scipy.optimize import curve_fit
from matplotlib.colors import LogNorm
from dask.diagnostics import ProgressBar
from scipy.interpolate import UnivariateSpline, interp1d, splrep
import seaborn as sns
# import helpers as hlp # helper routines

# IMPORT FROM LOCAL LIBRARY
sys.path.insert(0, "../library/") #Import my own libraries
import processing_math as promath
import processing_utility as prout
import processing_pd as propd
import processing_data as prodata
import nicholai_plot_helper as nplt
import nicholai_exclusive as ne
import SignalGenerator as sg

# IMPORT FROM MAIN DIRECTORY
sys.path.insert(0, "../")
import processing


path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/'#run00216'
df = propd.load_parquet_merge(path,[215,216,217,218,219,220])
# df = pd.read_parquet(path, columns=['samples_ch1', 'samples_ch2', 'amplitude_ch1', 'amplitude_ch2', 'baseline_ch1', 'baseline_ch2', 'peak_index_ch1', 'peak_index_ch2', 'tof_ch2'])

# FILTER
# thr = 0
# df = df.query(f'amplitude_ch2>{thr}')

#TOF CALIBRATION
prodata.tof_calibration(df, flashrange={'tof_ch2':[40,60],'tof_ch3':[40,60],'tof_ch4':[40,60],'tof_ch5':[40,60]}, distance=(1.305+(0.0615/2)+0.0032))

# calculate new TOF with CFD X method
# df['cfdX_ch1']=np.nan #time pick-off method: zero crossing
# df['cfdX_ch2']=np.nan #time pick-off method: zero crossing
# df['tof_X_ch2']=np.nan #time pick-off method: zero crossing
# df['tof_X_ch3']=np.nan #time pick-off method: zero crossing
# df['tof_X_ch4']=np.nan #time pick-off method: zero crossing
# df['tof_X_ch5']=np.nan #time pick-off method: zero crossing
# # df['tof_X2_ch2']=np.nan #time pick-off method: zero crossing
# # df['tof_thr_ch2']=np.nan #time pick-off method: threshold
# thr = 10 #mV
# max = len(df)
# for idx in tqdm(df.index[0:max]):
#     # df['cfdX_ch1'][idx] = prodata.cfdX(df.samples_ch1[idx], df.baseline_ch1[idx], df.peak_index_ch1[idx], 7, time_frac=.2)
#     # df['cfdX_ch2'][idx] = prodata.cfdX(df.samples_ch2[idx], df.baseline_ch2[idx], df.peak_index_ch2[idx], 7, time_frac=.2)
#     # df['tof_X_ch2'][idx] = df['cfdX_ch1'][idx]-df['cfdX_ch2'][idx]
#     df['cfdX_ch1'][idx] = prodata.cfdX(df.samples_ch1[idx], df.baseline_ch1[idx], df.peak_index_ch1[idx], 7, time_frac=.36)
#     df['cfdX_ch2'][idx] = prodata.cfdX(df.samples_ch2[idx], df.baseline_ch2[idx], df.peak_index_ch2[idx], 7, time_frac=.36)
#     df['cfdX_ch3'][idx] = prodata.cfdX(df.samples_ch3[idx], df.baseline_ch3[idx], df.peak_index_ch3[idx], 7, time_frac=.36)
#     df['cfdX_ch4'][idx] = prodata.cfdX(df.samples_ch4[idx], df.baseline_ch4[idx], df.peak_index_ch4[idx], 7, time_frac=.36)
#     df['cfdX_ch5'][idx] = prodata.cfdX(df.samples_ch5[idx], df.baseline_ch5[idx], df.peak_index_ch5[idx], 7, time_frac=.36)
#     df['tof_X_ch2'][idx] = df['cfdX_ch1'][idx]-df['cfdX_ch2'][idx]
#     df['tof_X_ch3'][idx] = df['cfdX_ch1'][idx]-df['cfdX_ch3'][idx]
#     df['tof_X_ch4'][idx] = df['cfdX_ch1'][idx]-df['cfdX_ch4'][idx]
#     df['tof_X_ch5'][idx] = df['cfdX_ch1'][idx]-df['cfdX_ch5'][idx]
#     # thr_time_ch1 = prodata.CFD_threshold(df.samples_ch1[idx], df.baseline_ch1[idx], 25)    
#     # thr_time_ch2 = prodata.CFD_threshold(df.samples_ch2[idx], df.baseline_ch2[idx], 25)  
#     # df['tof_thr_ch2'][idx] = thr_time_ch1-thr_time_ch2

# # PLOT AND FIT TIMING RESOLUTION testing three difference time pick-off methods
bins = 500
thr = 0
plt.figure(0)
plt.suptitle('Three difference time pick-off methods')
plt.subplot(1,2,1)
plt.title('Amplitude fraction (f=0.35)')
y_val, x_val = np.histogram(df.query(f'amplitude_ch1>{thr}').tof_ch2, bins=bins, range=(30, 70))
fit_val, fit_err = promath.gaussFit(prodata.getBinCenters(x_val), y_val)
plt.plot(prodata.getBinCenters(x_val), y_val, lw=3, label='data')
plt.plot(np.arange(30,70,.1), promath.gaussFunc(np.arange(30,70,.1), fit_val[0], fit_val[1], fit_val[2]), lw=2, label=f'$\mu$={round(fit_val[1],2)}$\pm${round(fit_err[1],2)} ns, $\sigma$={round(abs(fit_val[2]),2)}$\pm${round(fit_err[2])} ns')
plt.ylabel('counts')
plt.xlabel('ToF value [ns]')
# plt.xlim(31, 33.5)
plt.legend()

plt.subplot(1,2,2)
plt.title('Zero crossing (f=0.36)')
# y_val, x_val = np.histogram(df.query(f'amplitude_ch1>{thr}').tof_X_ch2, bins=bins, range=(40, 60))
# fit_val, fit_err = promath.gaussFit(prodata.getBinCenters(x_val), y_val)
# plt.plot(prodata.getBinCenters(x_val), y_val, lw=3, label='data')
# plt.plot(np.arange(40,60,.1), promath.gaussFunc(np.arange(40,60,.1), fit_val[0], fit_val[1], fit_val[2]), lw=2, label=f'$\mu$={round(fit_val[1],3)}$\pm${round(fit_err[1],2)} ns, $\sigma$={round(abs(fit_val[2]),3)}$\pm${round(fit_err[2])} ns')
# plt.ylabel('counts')
# plt.xlabel('ToF value [ns]')
# # plt.xlim(31, 33.5)
# plt.legend()
plt.show()

# plt.subplot(1,3,3)
# plt.title('Threshold method')
# y_val, x_val = np.histogram(df.tof_thr_ch2*-1, bins=bins, range=(28, 36))
# fit_val, fit_err = promath.gaussFit(prodata.getBinCenters(x_val), y_val)
# plt.plot(prodata.getBinCenters(x_val), y_val, lw=3, label='data')
# plt.plot(prodata.getBinCenters(x_val), promath.gaussFunc(prodata.getBinCenters(x_val), fit_val[0], fit_val[1], fit_val[2]), lw=2, label=f'$\mu$={round(fit_val[1],3)}$\pm${round(fit_err[1],2)} ns, $\sigma$={round(abs(fit_val[2]),3)}$\pm${round(fit_err[2])} ns')
# plt.ylabel('counts')
# plt.xlabel('ToF value [ns]')
# plt.xlim(28,36)
# plt.legend()

# plt.show()


# PLOT AND FIT TIMING RESOLUTION
#::::::::::::::::::::::::::::::::::::::::::::::::
# path1 = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/run00323' #EJ321P
# path2 = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/run00324' #YAP
# df1 = pd.read_parquet(path1, columns=['tof_ch2'])
# df2 = pd.read_parquet(path2, columns=['tof_ch2'])

# bins = 1000
# plt.figure(0)
# plt.suptitle(f'Split pulse ToF, delay = 32 ns')
# plt.subplot(1,2,1)
# plt.title('EJ321P detector')
# y_val, x_val = np.histogram(df1.tof_ch2*-1, bins=bins, range=(28, 36))
# fit_val, fit_err = promath.gaussFit(prodata.getBinCenters(x_val), y_val)
# plt.plot(prodata.getBinCenters(x_val), y_val, lw=3, label='data')
# plt.plot(prodata.getBinCenters(x_val), promath.gaussFunc(prodata.getBinCenters(x_val), fit_val[0], fit_val[1], fit_val[2]*-1), lw=3, label=f'$\mu$={round(fit_val[1],3)}$\pm${round(fit_err[1],2)} ns, $\sigma$={round(abs(fit_val[2]),3)}$\pm${round(fit_err[2])} ns')
# plt.ylabel('counts')
# plt.xlabel('ToF value [ns]')
# plt.xlim(31.5,32.8)
# plt.legend()
# plt.subplot(1,2,2)
# plt.title('YAP detector')
# y_val, x_val = np.histogram(df2.tof_ch2*-1, bins=bins, range=(28, 36))
# fit_val, fit_err = promath.gaussFit(prodata.getBinCenters(x_val), y_val)
# plt.plot(prodata.getBinCenters(x_val), y_val, lw=3, label='data')
# plt.plot(prodata.getBinCenters(x_val), promath.gaussFunc(prodata.getBinCenters(x_val), fit_val[0], fit_val[1], fit_val[2]*-1), lw=3, label=f'$\mu$={round(fit_val[1],3)}$\pm${round(fit_err[1],2)} ns, $\sigma$={round(abs(fit_val[2]),3)}$\pm${round(fit_err[2])} ns')
# plt.ylabel('counts')
# plt.xlabel('ToF value [ns]')
# plt.xlim(31.5,32.8)
# plt.legend()

# plt.show()


# TESTING CFD X METHOD WITH DIFFERENT FRACTIONS
#::::::::::::::::::::::::::::::::::::::::::
# path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/run00323'
# df = pd.read_parquet(path, columns=['samples_ch1', 'samples_ch2', 'amplitude_ch1', 'amplitude_ch2', 'baseline_ch1', 'baseline_ch2', 'peak_index_ch1', 'peak_index_ch2', 'tof_ch2'])

# #calculate new TOF with CFD X method
# df['cfdX_ch1']=np.nan #time pick-off method: zero crossing
# df['cfdX_ch2']=np.nan #time pick-off method: zero crossing
# df['tof_X1_ch2']=np.nan #time pick-off method: zero crossing
# df['tof_X2_ch2']=np.nan #time pick-off method: zero crossing
# df['tof_X3_ch2']=np.nan #time pick-off method: zero crossing
# df['tof_X4_ch2']=np.nan #time pick-off method: zero crossing
# df['tof_X5_ch2']=np.nan #time pick-off method: zero crossing
# df['tof_X6_ch2']=np.nan #time pick-off method: zero crossing

# for idx in tqdm(df.index[0:10000]):
#     df['cfdX_ch1'][idx] = prodata.cfdX(df.samples_ch1[idx], df.baseline_ch1[idx], df.peak_index_ch1[idx], 7, time_frac=1.0)
#     df['cfdX_ch2'][idx] = prodata.cfdX(df.samples_ch2[idx], df.baseline_ch2[idx], df.peak_index_ch2[idx], 7, time_frac=1.0)    
#     df['tof_X1_ch2'][idx] = df['cfdX_ch1'][idx]-df['cfdX_ch2'][idx]
#     df['cfdX_ch1'][idx] = prodata.cfdX(df.samples_ch1[idx], df.baseline_ch1[idx], df.peak_index_ch1[idx], 7, time_frac=.8)
#     df['cfdX_ch2'][idx] = prodata.cfdX(df.samples_ch2[idx], df.baseline_ch2[idx], df.peak_index_ch2[idx], 7, time_frac=.8)
#     df['tof_X2_ch2'][idx] = df['cfdX_ch1'][idx]-df['cfdX_ch2'][idx]
#     df['cfdX_ch1'][idx] = prodata.cfdX(df.samples_ch1[idx], df.baseline_ch1[idx], df.peak_index_ch1[idx], 7, time_frac=.6)
#     df['cfdX_ch2'][idx] = prodata.cfdX(df.samples_ch2[idx], df.baseline_ch2[idx], df.peak_index_ch2[idx], 7, time_frac=.6)
#     df['tof_X3_ch2'][idx] = df['cfdX_ch1'][idx]-df['cfdX_ch2'][idx]
#     df['cfdX_ch1'][idx] = prodata.cfdX(df.samples_ch1[idx], df.baseline_ch1[idx], df.peak_index_ch1[idx], 7, time_frac=.4)
#     df['cfdX_ch2'][idx] = prodata.cfdX(df.samples_ch2[idx], df.baseline_ch2[idx], df.peak_index_ch2[idx], 7, time_frac=.4)
#     df['tof_X4_ch2'][idx] = df['cfdX_ch1'][idx]-df['cfdX_ch2'][idx]
#     df['cfdX_ch1'][idx] = prodata.cfdX(df.samples_ch1[idx], df.baseline_ch1[idx], df.peak_index_ch1[idx], 7, time_frac=.2)
#     df['cfdX_ch2'][idx] = prodata.cfdX(df.samples_ch2[idx], df.baseline_ch2[idx], df.peak_index_ch2[idx], 7, time_frac=.2)
#     df['tof_X5_ch2'][idx] = df['cfdX_ch1'][idx]-df['cfdX_ch2'][idx]
#     df['cfdX_ch1'][idx] = prodata.cfdX(df.samples_ch1[idx], df.baseline_ch1[idx], df.peak_index_ch1[idx], 7, time_frac=.1)
#     df['cfdX_ch2'][idx] = prodata.cfdX(df.samples_ch2[idx], df.baseline_ch2[idx], df.peak_index_ch2[idx], 7, time_frac=.1)
#     df['tof_X6_ch2'][idx] = df['cfdX_ch1'][idx]-df['cfdX_ch2'][idx]

# bins = 200
# min, max = 32, 32.55
# plt.figure(3)
# plt.suptitle('Varying the fraction for CFD X method')

# plt.subplot(2,3,1)
# plt.title('frac = 1.0')
# y_val, x_val = np.histogram(df.tof_X1_ch2*-1, bins=bins, range=(min, max))
# fit_val, fit_err = promath.gaussFit(prodata.getBinCenters(x_val), y_val)
# plt.plot(prodata.getBinCenters(x_val), y_val, lw=3, label='data')
# chi2 = round(promath.chi2_normal(y_val, fit_val[1], fit_val[2]),2)

# plt.plot(prodata.getBinCenters(x_val), promath.gaussFunc(prodata.getBinCenters(x_val), fit_val[0], fit_val[1], fit_val[2]), lw=2, label=f'$\chi^2$/ndeg = {chi2}, $\mu$={round(fit_val[1],2)} ns, $\sigma$={round(abs(fit_val[2]),4)} ns')
# plt.ylabel('counts')
# plt.xlabel('ToF value [ns]')
# # plt.xlim(31, 33.5)
# plt.legend()

# plt.subplot(2,3,2)
# plt.title('frac = 0.8')
# y_val, x_val = np.histogram(df.tof_X2_ch2*-1, bins=bins, range=(min, max))
# fit_val, fit_err = promath.gaussFit(prodata.getBinCenters(x_val), y_val)
# plt.plot(prodata.getBinCenters(x_val), y_val, lw=3, label='data')
# chi2 = round(promath.chi2_normal(y_val, fit_val[1], fit_val[2]),2)

# plt.plot(prodata.getBinCenters(x_val), promath.gaussFunc(prodata.getBinCenters(x_val), fit_val[0], fit_val[1], fit_val[2]), lw=2, label=f'$\chi^2$/ndeg = {chi2}, $\mu$={round(fit_val[1],2)} ns, $\sigma$={round(abs(fit_val[2]),4)} ns')
# plt.ylabel('counts')
# plt.xlabel('ToF value [ns]')
# # plt.xlim(31, 33.5)
# plt.legend()

# plt.subplot(2,3,3)
# plt.title('frac = 0.6')
# y_val, x_val = np.histogram(df.tof_X3_ch2*-1, bins=bins, range=(min, max))
# fit_val, fit_err = promath.gaussFit(prodata.getBinCenters(x_val), y_val)
# plt.plot(prodata.getBinCenters(x_val), y_val, lw=3, label='data')
# chi2 = round(promath.chi2_normal(y_val, fit_val[1], fit_val[2]),2)

# plt.plot(prodata.getBinCenters(x_val), promath.gaussFunc(prodata.getBinCenters(x_val), fit_val[0], fit_val[1], fit_val[2]), lw=2, label=f'$\chi^2$/ndeg = {chi2}, $\mu$={round(fit_val[1],2)} ns, $\sigma$={round(abs(fit_val[2]),4)} ns')
# plt.ylabel('counts')
# plt.xlabel('ToF value [ns]')
# # plt.xlim(31, 33.5)
# plt.legend()

# plt.subplot(2,3,4)
# plt.title('frac = 0.4')
# y_val, x_val = np.histogram(df.tof_X4_ch2*-1, bins=bins, range=(min, max))
# fit_val, fit_err = promath.gaussFit(prodata.getBinCenters(x_val), y_val)
# plt.plot(prodata.getBinCenters(x_val), y_val, lw=3, label='data')
# chi2 = round(promath.chi2_normal(y_val, fit_val[1], fit_val[2]),2)

# plt.plot(prodata.getBinCenters(x_val), promath.gaussFunc(prodata.getBinCenters(x_val), fit_val[0], fit_val[1], fit_val[2]), lw=2, label=f'$\chi^2$/ndeg = {chi2}, $\mu$={round(fit_val[1],2)} ns, $\sigma$={round(abs(fit_val[2]),4)} ns')
# plt.ylabel('counts')
# plt.xlabel('ToF value [ns]')
# # plt.xlim(31, 33.5)
# plt.legend()

# plt.subplot(2,3,5)
# plt.title('frac = 0.2')
# y_val, x_val = np.histogram(df.tof_X5_ch2*-1, bins=bins, range=(min, max))
# fit_val, fit_err = promath.gaussFit(prodata.getBinCenters(x_val), y_val)
# plt.plot(prodata.getBinCenters(x_val), y_val, lw=3, label='data')
# chi2 = round(promath.chi2_normal(y_val, fit_val[1], fit_val[2]),2)

# plt.plot(prodata.getBinCenters(x_val), promath.gaussFunc(prodata.getBinCenters(x_val), fit_val[0], fit_val[1], fit_val[2]), lw=2, label=f'$\chi^2$/ndeg = {chi2}, $\mu$={round(fit_val[1],2)} ns, $\sigma$={round(abs(fit_val[2]),4)} ns')
# plt.ylabel('counts')
# plt.xlabel('ToF value [ns]')
# # plt.xlim(31, 33.5)
# plt.legend()

# plt.subplot(2,3,6)
# plt.title('frac = 0.1')
# y_val, x_val = np.histogram(df.tof_X6_ch2*-1, bins=bins, range=(min, max))
# fit_val, fit_err = promath.gaussFit(prodata.getBinCenters(x_val), y_val)
# plt.plot(prodata.getBinCenters(x_val), y_val, lw=3, label='data')
# chi2 = round(promath.chi2_normal(y_val, fit_val[1], fit_val[2]),2)

# plt.plot(prodata.getBinCenters(x_val), promath.gaussFunc(prodata.getBinCenters(x_val), fit_val[0], fit_val[1], fit_val[2]), lw=2, label=f'$\chi^2$/ndeg = {chi2}, $\mu$={round(fit_val[1],2)} ns, $\sigma$={round(abs(fit_val[2]),4)} ns')
# plt.ylabel('counts')
# plt.xlabel('ToF value [ns]')
# # plt.xlim(31, 33.5)
# plt.legend()

# plt.show()