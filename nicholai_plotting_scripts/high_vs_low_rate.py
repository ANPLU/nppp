import sys 
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
sys.path.insert(0, "../")
import digviz as dv

# :::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#  ELOG: https://stf02.nuclear.lu.se/Neutron+Tagging/508
# :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#Low and high rate comparision:
df_low = dv.load_h5('/drive2/TNT/digital/NE213_cup/jadaq-00047.h5', 100000) 
df_high = dv.load_h5('/drive2/TNT/digital/NE213_cup/jadaq-00036.h5', 100000) 

plt.figure(0)
for i in df_high.query('ch==1').samples: 
    plt.subplot(1,2,1) 
    plt.plot(i) 
    plt.title('High rate, jadaq-00036') 
for j in df_low.query('ch==1').samples: 
    plt.subplot(1,2,2) 
    plt.plot(j) 
    plt.title('Low rate, jadaq-00047')

df_high['baseline'] = df_high.samples.apply(lambda x: np.mean(x[:20]))
df_low['baseline'] = df_low.samples.apply(lambda x: np.mean(x[:20]))

plt.figure(1)
plt.subplot(1,2,1) 
plt.hist(df_low.query('ch==1').baseline, bins=50) 
plt.title('High rate, baseline, jadaq-00036') 
plt.yscale('log')
plt.subplot(1,2,2) 
plt.hist(df_high.query('ch==1').baseline, bins=50) 
plt.title('Low rate, baseline, jadaq-00047') 
plt.yscale('log')

df_low['QDC'] = np.abs(df_low.samples.apply(lambda x: np.sum(x))-(len(df_low.samples[0])*df_low['baseline']))
df_high['QDC'] = np.abs(df_high.samples.apply(lambda x: np.sum(x))-(len(df_high.samples[0])*df_high['baseline']))

plt.figure(2)
plt.subplot(1,2,1) 
plt.hist(df_low.query('ch==1').QDC, bins=100) 
plt.title('High rate, QDC, jadaq-00036') 
plt.yscale('log')
plt.subplot(1,2,2) 
plt.hist(df_high.query('ch==1').QDC, bins=100) 
plt.title('Low rate, QDC, jadaq-00047')
plt.yscale('log')