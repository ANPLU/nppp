import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import sys 

import dask
import dask.dataframe as dd
import dask.array as da
from dask.diagnostics import ProgressBar

sys.path.insert(0, "../library/") #Import my own libraries
import processing_math as promath
import processing_utility as prout
import processing_pd as propd
import processing_data as prodata
import nicholai_plot_helper as nplt
import nicholai_exclusive as ne


def timeNorm(data, time):
    """
    Time normalization function. Returns matplotlib 'weight' factor.

    Parameters:
    - data......container with data.
    - time......the normalization time in seconds.
    """
    w=np.empty(len(data)) #Make array same length as data set
    w.fill(1/time) #Fill array with constant: 1/live_time [s]
    return w


exclude_col=['samples_ch1', 'samples_ch2', 'samples_ch3', 'samples_ch4', 'samples_ch5', 'evtno', 'ts', 'digitizer', 
            'baseline_ch1', 'peak_index_ch1',  
            'baseline_ch2', 'peak_index_ch2', 
            'baseline_ch3', 'peak_index_ch3',  
            'baseline_ch4', 'peak_index_ch4',  
            'baseline_ch5', 'peak_index_ch5', 
            'qdc_sg_ch1', 'qdc_ps_ch1',
            'qdc_sg_ch2', 'qdc_ps_ch2', 
            'qdc_sg_ch3', 'qdc_ps_ch3', 
            'qdc_sg_ch4', 'qdc_ps_ch4',
            'qdc_sg_ch5', 'qdc_ps_ch5',
            'global_ts', 
            'CFD_drop_ch1', 'risetime_ch1', 'siglen_ch1',
            'min_val_ch2', 'max_val_ch2', 'CFD_drop_ch2', 'risetime_ch2',
            'siglen_ch2', 'min_val_ch3', 'max_val_ch3', 'CFD_drop_ch3',
            'risetime_ch3', 'siglen_ch3', 'min_val_ch4', 'max_val_ch4',
            'CFD_drop_ch4', 'risetime_ch4', 'siglen_ch4', 'min_val_ch5',
            'max_val_ch5', 'CFD_drop_ch5', 'risetime_ch5', 'siglen_ch5']

path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop


def NE213_16dB():
    """
    NE213 detector with german attenuator at 16 dB
    """
    # :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    # ----- NE213 - STUDY OF DIFFERCE BETWEEN DETECTOR INSIDE BEAM PORT (2.22 MeV calibration) AND OUTSIDE (normal run) -----
    # :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    df_213_0 = propd.load_parquet(path+'run00850', exclude_col) #out, stable run
    df_213_1 = propd.load_parquet(path+'run00941', exclude_col) #out
    df_213_2 = propd.load_parquet(path+'run00942', exclude_col) #in
    df_213_3 = propd.load_parquet(path+'run00943', exclude_col) #out
    df_213_4 = propd.load_parquet(path+'run00944', exclude_col) #in
    df_213_5 = propd.load_parquet(path+'run00945', exclude_col) #out
    df_213_6 = propd.load_parquet(path+'run00946', exclude_col) #in
    df_213_7 = propd.load_parquet(path+'run00947', exclude_col) #out
    df_213_8 = propd.load_parquet(path+'run00948', exclude_col) #in
    df_213_9 = propd.load_parquet(path+'run00949', exclude_col) #out
    df_213_10 = propd.load_parquet(path+'run00950', exclude_col) #in (level)
    df_213_11 = propd.load_parquet(path+'run00951', exclude_col) #out (level)
    df_213_12 = propd.load_parquet(path+'run00952', exclude_col) #in (level)
    df_213_13 = propd.load_parquet(path+'run00953', exclude_col) #out (level)
    df_213_14 = propd.load_parquet(path+'run00954', exclude_col) #in (level)


    minevt = 0
    maxevt = -1

    numBins = 100
    binRange = [0,10000]
    plt.figure(0)
    plt.subplot(1,2,1)
    plt.title('NE-213, QDC (Outside)')
    plt.hist(df_213_0.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=10, color='black', weights=timeNorm(df_213_0.qdc_lg_ch1[minevt:maxevt], 600), alpha=.5, range=binRange, histtype='step', label='outside STABLE (run 850)')
    plt.hist(df_213_1.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, linestyle='-', weights=timeNorm(df_213_1.qdc_lg_ch1[minevt:maxevt], 120),  range=binRange, histtype='step', label='outside (run 941)')
    plt.hist(df_213_3.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, linestyle='-', weights=timeNorm(df_213_3.qdc_lg_ch1[minevt:maxevt], 120), range=binRange, histtype='step', label='outside (run 943)')
    plt.hist(df_213_5.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, linestyle='-', weights=timeNorm(df_213_5.qdc_lg_ch1[minevt:maxevt], 120), range=binRange, histtype='step', label='outside (run 945)')
    plt.hist(df_213_7.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, linestyle='-', weights=timeNorm(df_213_7.qdc_lg_ch1[minevt:maxevt], 120),  range=binRange, histtype='step', label='outside (run 947)')
    plt.hist(df_213_9.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, linestyle='-', weights=timeNorm(df_213_9.qdc_lg_ch1[minevt:maxevt], 120),  range=binRange, histtype='step', label='outside (run 949)')
    plt.hist(df_213_11.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, linestyle='-', weights=timeNorm(df_213_11.qdc_lg_ch1[minevt:maxevt], 120),  range=binRange, histtype='step', label='outside, (run 951)')
    plt.hist(df_213_13.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, linestyle='-', weights=timeNorm(df_213_13.qdc_lg_ch1[minevt:maxevt], 120),  range=binRange, histtype='step', label='outside, (run 953)')
    # plt.grid(which='both')
    plt.ylabel('Rate [1/s]')
    plt.xlabel('QDC [arb. units]')
    plt.legend()
    plt.yscale('log')

    plt.subplot(1,2,2)
    plt.title('NE-213, QDC (Inside)')
    plt.hist(df_213_0.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=10, weights=timeNorm(df_213_0.qdc_lg_ch1[minevt:maxevt], 600), color='black', alpha=.5,         range=binRange, histtype='step', label='outside STABLE (run 850)')
    plt.hist(df_213_2.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, weights=timeNorm(df_213_2.qdc_lg_ch1[minevt:maxevt], 120), color='orange', linestyle='-',  range=binRange, histtype='step', label='inside (run 942)')
    plt.hist(df_213_4.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, weights=timeNorm(df_213_4.qdc_lg_ch1[minevt:maxevt], 120), color='orange', linestyle='-', range=binRange, histtype='step', label='inside (run 944)')
    plt.hist(df_213_6.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, weights=timeNorm(df_213_6.qdc_lg_ch1[minevt:maxevt], 120), color='orange', linestyle='-',  range=binRange, histtype='step', label='inside (run 946)')
    plt.hist(df_213_8.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, weights=timeNorm(df_213_8.qdc_lg_ch1[minevt:maxevt], 120), color='orange', linestyle='-',  range=binRange, histtype='step', label='inside (run 948)')
    plt.hist(df_213_10.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, weights=timeNorm(df_213_10.qdc_lg_ch1[minevt:maxevt], 120), color='blue', linestyle='-',  range=binRange, histtype='step', label='inside, level (run 950)')
    plt.hist(df_213_12.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, weights=timeNorm(df_213_12.qdc_lg_ch1[minevt:maxevt], 120), color='blue', linestyle='-',  range=binRange, histtype='step', label='inside, level (run 951)')
    plt.hist(df_213_14.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, weights=timeNorm(df_213_14.qdc_lg_ch1[minevt:maxevt], 120), color='blue', linestyle='-',  range=binRange, histtype='step', label='inside, level (run 953)')
    # plt.grid(which='both')
    plt.ylabel('Rate [1/s]')
    plt.xlabel('QDC [arb. units]')
    plt.legend()
    plt.yscale('log')



    numBins = 100
    binRange = [0,1100]
    plt.figure(2)
    plt.subplot(1,2,1)
    plt.title('NE-213, Pulse-height (Outside)')
    plt.hist(df_213_0.amplitude_ch1[minevt:maxevt], bins=numBins, lw=10, color='black', weights=timeNorm(df_213_0.amplitude_ch1[minevt:maxevt], 600), alpha=.5, range=binRange, histtype='step', label='outside STABLE (run 850)')
    plt.hist(df_213_1.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, linestyle='-', weights=timeNorm(df_213_1.amplitude_ch1[minevt:maxevt], 120),  range=binRange, histtype='step', label='outside (run 941)')
    plt.hist(df_213_3.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, linestyle='-', weights=timeNorm(df_213_3.amplitude_ch1[minevt:maxevt], 120), range=binRange, histtype='step', label='outside (run 943)')
    plt.hist(df_213_5.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, linestyle='-', weights=timeNorm(df_213_5.amplitude_ch1[minevt:maxevt], 120), range=binRange, histtype='step', label='outside (run 945)')
    plt.hist(df_213_7.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, linestyle='-', weights=timeNorm(df_213_7.amplitude_ch1[minevt:maxevt], 120),  range=binRange, histtype='step', label='outside (run 947)')
    plt.hist(df_213_9.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, linestyle='-', weights=timeNorm(df_213_9.amplitude_ch1[minevt:maxevt], 120),  range=binRange, histtype='step', label='outside (run 949)')
    plt.hist(df_213_11.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, linestyle='-', weights=timeNorm(df_213_11.amplitude_ch1[minevt:maxevt], 120),  range=binRange, histtype='step', label='outside, (run 951)')
    plt.hist(df_213_13.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, linestyle='-', weights=timeNorm(df_213_13.amplitude_ch1[minevt:maxevt], 120),  range=binRange, histtype='step', label='outside, (run 953)')
    # plt.grid(which='both')
    plt.ylabel('Counts')
    plt.xlabel('Amplitude [mV]')
    plt.legend()
    plt.yscale('log')

    plt.subplot(1,2,2)
    plt.title('NE-213, Pulse-height (Inside)')
    plt.hist(df_213_0.amplitude_ch1[minevt:maxevt], bins=numBins, lw=10, weights=timeNorm(df_213_0.amplitude_ch1[minevt:maxevt], 600), color='black', alpha=.5,         range=binRange, histtype='step', label='outside STABLE (run 850)')
    plt.hist(df_213_2.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, weights=timeNorm(df_213_2.amplitude_ch1[minevt:maxevt], 120), color='orange', linestyle='-',  range=binRange, histtype='step', label='inside (run 942)')
    plt.hist(df_213_4.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, weights=timeNorm(df_213_4.amplitude_ch1[minevt:maxevt], 120), color='orange', linestyle='-', range=binRange, histtype='step', label='inside (run 944)')
    plt.hist(df_213_6.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, weights=timeNorm(df_213_6.amplitude_ch1[minevt:maxevt], 120), color='orange', linestyle='-',  range=binRange, histtype='step', label='inside (run 946)')
    plt.hist(df_213_8.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, weights=timeNorm(df_213_8.amplitude_ch1[minevt:maxevt], 120), color='orange', linestyle='-',  range=binRange, histtype='step', label='inside (run 948)')
    plt.hist(df_213_10.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, weights=timeNorm(df_213_10.amplitude_ch1[minevt:maxevt], 120), color='blue', linestyle='-',  range=binRange, histtype='step', label='inside, level (run 950)')
    plt.hist(df_213_12.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, weights=timeNorm(df_213_12.amplitude_ch1[minevt:maxevt], 120), color='blue', linestyle='-',  range=binRange, histtype='step', label='inside, level (run 951)')
    plt.hist(df_213_14.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, weights=timeNorm(df_213_14.amplitude_ch1[minevt:maxevt], 120), color='blue', linestyle='-',  range=binRange, histtype='step', label='inside, level (run 953)')
    # plt.grid(which='both')
    plt.ylabel('Counts')
    plt.xlabel('Amplitude [mV]')
    plt.legend()
    plt.yscale('log')

    plt.show()

def NE213_distance():
    """
    Comparing NE213 detector with german -16 db attenuator with varying distance to source
    """
    # :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    # ----- NE213 - STUDY OF DIFFERCE BETWEEN DETECTOR DISTANCE TO SOURCE -----
    # :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    df_213_0 = propd.load_parquet(path+'run00850', exclude_col) #1306 mm stable run
    df_213_1 = propd.load_parquet(path+'run00941', exclude_col) #1306 mm
    df_213_2 = propd.load_parquet(path+'run00957', exclude_col) #1200 mm
    df_213_3 = propd.load_parquet(path+'run00958', exclude_col) #1100 mm
    df_213_4 = propd.load_parquet(path+'run00959', exclude_col) #1000 mm
    df_213_5 = propd.load_parquet(path+'run00960', exclude_col) #900 mm
    df_213_6 = propd.load_parquet(path+'run00961', exclude_col) #800 mm
    df_213_7 = propd.load_parquet(path+'run00962', exclude_col) #700 mm
    df_213_8 = propd.load_parquet(path+'run00963', exclude_col) #600 mm
    df_213_9 = propd.load_parquet(path+'run00964', exclude_col) #500 mm
    df_213_10 = propd.load_parquet(path+'run00965', exclude_col) #400 mm

    current = [314.178, 314.180, 314.190, 314.206, 314.222, 314.252, 314.310, 314.390, 314.516, 314.718]
    distance = [1306,1200,1100,1000,900,800,700,600,500,400]

    minevt = 0
    maxevt = -1

    numBins = 100
    binRange = [0,10000]
    plt.figure(3)
    plt.title('NE-213, QDC (varying distance to source)')
    plt.hist(df_213_0.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=10, color='black', weights=timeNorm(df_213_0.qdc_lg_ch1[minevt:maxevt], 600), alpha=.5, range=binRange, histtype='step', label='outside STABLE (run 850)')
    plt.hist(df_213_1.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, linestyle='-', weights=timeNorm(df_213_1.qdc_lg_ch1[minevt:maxevt], 120),  range=binRange, histtype='step', label='1306 mm (run 956)')
    plt.hist(df_213_2.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, linestyle='-', weights=timeNorm(df_213_2.qdc_lg_ch1[minevt:maxevt], 120), range=binRange, histtype='step', label='1200 mm (run 957)')
    plt.hist(df_213_3.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, linestyle='-', weights=timeNorm(df_213_3.qdc_lg_ch1[minevt:maxevt], 120), range=binRange, histtype='step', label='1100 mm (run 958)')
    plt.hist(df_213_4.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, linestyle='-', weights=timeNorm(df_213_4.qdc_lg_ch1[minevt:maxevt], 120),  range=binRange, histtype='step', label='1000 mm (run 959)')
    plt.hist(df_213_5.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, linestyle='-', weights=timeNorm(df_213_5.qdc_lg_ch1[minevt:maxevt], 120),  range=binRange, histtype='step', label='900 mm (run 960)')
    plt.hist(df_213_6.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, linestyle='-', weights=timeNorm(df_213_6.qdc_lg_ch1[minevt:maxevt], 120),  range=binRange, histtype='step', label='800 mm, (run 961)')
    plt.hist(df_213_7.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, linestyle='-', weights=timeNorm(df_213_7.qdc_lg_ch1[minevt:maxevt], 120),  range=binRange, histtype='step', label='700 mm, (run 962)')
    plt.hist(df_213_8.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, linestyle='-', weights=timeNorm(df_213_8.qdc_lg_ch1[minevt:maxevt], 120),  range=binRange, histtype='step', label='600 mm, (run 963)')
    plt.hist(df_213_9.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, linestyle='-', weights=timeNorm(df_213_9.qdc_lg_ch1[minevt:maxevt], 120),  range=binRange, histtype='step', label='500 mm, (run 964)')
    plt.hist(df_213_10.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, linestyle='-', weights=timeNorm(df_213_10.qdc_lg_ch1[minevt:maxevt], 120),  range=binRange, histtype='step', label='400 mm, (run 964)')
    # plt.grid(which='both')
    plt.ylabel('Rate [1/s]')
    plt.xlabel('QDC [arb. units]')
    plt.legend()
    plt.yscale('log')


    plt.figure(4)
    plt.title('NE-213, Current vs. Distance to source')
    plt.plot(distance, current, lw=2)
    plt.ylabel('Current [uA]')
    plt.xlabel('Distance [mm]')

    plt.show()

def EJ305_20dB():
    """
    IN/OUT measurements with EJ305 and german attenuator at -20dB
    """
    # :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    # ----- EJ-305 -20dB - STUDY OF DIFFERCE BETWEEN DETECTOR INSIDE BEAM PORT (2.22 MeV calibration) AND OUTSIDE (normal run) -----
    # :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    df_305_1 = propd.load_parquet(path+'run00968', exclude_col) #out
    df_305_2 = propd.load_parquet(path+'run00969', exclude_col) #in
    df_305_3 = propd.load_parquet(path+'run00970', exclude_col) #out
    df_305_4 = propd.load_parquet(path+'run00971', exclude_col) #in
    df_305_5 = propd.load_parquet(path+'run00972', exclude_col) #out
    df_305_6 = propd.load_parquet(path+'run00973', exclude_col) #in
    df_305_7 = propd.load_parquet(path+'run00974', exclude_col) #out
    df_305_8 = propd.load_parquet(path+'run00975', exclude_col) #in
    df_305_9 = propd.load_parquet(path+'run00976', exclude_col) #out
    df_305_10 = propd.load_parquet(path+'run00977', exclude_col) #in
    df_305_11 = propd.load_parquet(path+'run00978', exclude_col) #out
    df_305_12 = propd.load_parquet(path+'run00979', exclude_col) #in

    minevt = 0
    maxevt = -1

    scaleFactor = 10

    numBins = 100
    binRange = [0,17500]
    plt.figure(1)
    plt.title('EJ-305, QDC (Outside)')
    plt.hist(df_305_1.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue', linestyle='-', weights=timeNorm(df_305_1.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,  range=binRange, histtype='step', label='outside (run 968)')
    plt.hist(df_305_2.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_305_2.qdc_lg_ch1[minevt:maxevt], 120),  range=binRange, histtype='step', label='inside (run 969)')
    plt.hist(df_305_3.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue', linestyle='-', weights=timeNorm(df_305_3.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,  range=binRange, histtype='step', label='outside (run 970)')
    plt.hist(df_305_4.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_305_4.qdc_lg_ch1[minevt:maxevt], 120),  range=binRange, histtype='step', label='inside (run 971)')
    plt.hist(df_305_5.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue', linestyle='-', weights=timeNorm(df_305_5.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,  range=binRange, histtype='step', label='outside (run 972)')
    plt.hist(df_305_6.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_305_6.qdc_lg_ch1[minevt:maxevt], 120),  range=binRange, histtype='step', label='inside (run 973)')
    plt.hist(df_305_7.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue', linestyle='-', weights=timeNorm(df_305_7.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,  range=binRange, histtype='step', label='outside (run 974)')
    plt.hist(df_305_8.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_305_8.qdc_lg_ch1[minevt:maxevt], 120),  range=binRange, histtype='step', label='inside (run 975)')
    plt.hist(df_305_9.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue', linestyle='-', weights=timeNorm(df_305_9.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,  range=binRange, histtype='step', label='outside (run 976)')
    plt.hist(df_305_10.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_305_10.qdc_lg_ch1[minevt:maxevt], 120),  range=binRange, histtype='step', label='inside (run 977)')
    plt.hist(df_305_11.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue', linestyle='-', weights=timeNorm(df_305_11.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,  range=binRange, histtype='step', label='outside (run 978)')
    plt.hist(df_305_12.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_305_12.qdc_lg_ch1[minevt:maxevt], 120),  range=binRange, histtype='step', label='inside (run 979)')
    plt.ylabel('[arb. units]')
    plt.xlabel('QDC [arb. units]')
    plt.grid(which='both')
    plt.legend()
    plt.yscale('log')


    numBins = 100
    binRange = [0,1050]
    plt.figure(2)
    plt.title('NE-305, Pulse-height (Outside)')
    plt.hist(df_305_1.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue', linestyle='-', weights=timeNorm(df_305_1.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,  range=binRange, histtype='step', label='outside (run 968)')
    plt.hist(df_305_2.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_305_2.qdc_lg_ch1[minevt:maxevt], 120),  range=binRange, histtype='step', label='inside (run 969)')
    plt.hist(df_305_3.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue', linestyle='-', weights=timeNorm(df_305_3.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,  range=binRange, histtype='step', label='outside (run 970)')
    plt.hist(df_305_4.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_305_4.qdc_lg_ch1[minevt:maxevt], 120),  range=binRange, histtype='step', label='inside (run 971)')
    plt.hist(df_305_5.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue', linestyle='-', weights=timeNorm(df_305_5.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,  range=binRange, histtype='step', label='outside (run 972)')
    plt.hist(df_305_6.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_305_6.qdc_lg_ch1[minevt:maxevt], 120),  range=binRange, histtype='step', label='inside (run 973)')
    plt.hist(df_305_7.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue', linestyle='-', weights=timeNorm(df_305_7.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,  range=binRange, histtype='step', label='outside (run 974)')
    plt.hist(df_305_8.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_305_8.qdc_lg_ch1[minevt:maxevt], 120),  range=binRange, histtype='step', label='inside (run 975)')
    plt.hist(df_305_9.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue', linestyle='-', weights=timeNorm(df_305_9.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,  range=binRange, histtype='step', label='outside (run 976)')
    plt.hist(df_305_10.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_305_10.qdc_lg_ch1[minevt:maxevt], 120),  range=binRange, histtype='step', label='inside (run 977)')
    plt.hist(df_305_11.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue', linestyle='-', weights=timeNorm(df_305_11.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,  range=binRange, histtype='step', label='outside (run 978)')
    plt.hist(df_305_12.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_305_12.qdc_lg_ch1[minevt:maxevt], 120),  range=binRange, histtype='step', label='inside (run 979)')
    plt.ylabel('[arb. units]')
    plt.xlabel('Amplitude [mV]')
    plt.legend()
    plt.grid(which='both')
    plt.yscale('log')
    plt.show()

def EJ331_20db():
    """
    IN/OUT measurements with EJ331 and german attenuator at -20dB
    """
    # :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    # ----- EJ-331 -20dB - STUDY OF DIFFERCE BETWEEN DETECTOR INSIDE BEAM PORT (2.22 MeV calibration) AND OUTSIDE (normal run) -----
    # :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    df_331_1 = propd.load_parquet(path+'run00980', exclude_col) #out
    df_331_2 = propd.load_parquet(path+'run00981', exclude_col) #in
    df_331_3 = propd.load_parquet(path+'run00982', exclude_col) #out
    df_331_4 = propd.load_parquet(path+'run00983', exclude_col) #in
    df_331_5 = propd.load_parquet(path+'run00984', exclude_col) #out
    df_331_6 = propd.load_parquet(path+'run00985', exclude_col) #in
    df_331_7 = propd.load_parquet(path+'run00986', exclude_col) #out
    df_331_8 = propd.load_parquet(path+'run00987', exclude_col) #in
    df_331_9 = propd.load_parquet(path+'run00988', exclude_col) #out
    df_331_10 = propd.load_parquet(path+'run00989', exclude_col) #in
    df_331_11 = propd.load_parquet(path+'run00990', exclude_col) #out
    df_331_12 = propd.load_parquet(path+'run00991', exclude_col) #in

    minevt = 0
    maxevt = -1

    scaleFactor = 14

    numBins = 100
    binRange = [0,10000]
    plt.figure(3)
    plt.title('EJ-331, QDC (Outside)')
    plt.hist(df_331_1.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue', linestyle='-', weights=timeNorm(df_331_1.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,  range=binRange, histtype='step', label='outside (run 980)')
    plt.hist(df_331_2.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_331_2.qdc_lg_ch1[minevt:maxevt], 120),  range=binRange, histtype='step', label='inside (run 981)')
    plt.hist(df_331_3.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue', linestyle='-', weights=timeNorm(df_331_3.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,  range=binRange, histtype='step', label='outside (run 982)')
    plt.hist(df_331_4.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_331_4.qdc_lg_ch1[minevt:maxevt], 120),  range=binRange, histtype='step', label='inside (run 983)')
    plt.hist(df_331_5.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue', linestyle='-', weights=timeNorm(df_331_5.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,  range=binRange, histtype='step', label='outside (run 984)')
    plt.hist(df_331_6.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_331_6.qdc_lg_ch1[minevt:maxevt], 120),  range=binRange, histtype='step', label='inside (run 985)')
    plt.hist(df_331_7.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue', linestyle='-', weights=timeNorm(df_331_7.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,  range=binRange, histtype='step', label='outside (run 986)')
    plt.hist(df_331_8.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_331_8.qdc_lg_ch1[minevt:maxevt], 120),  range=binRange, histtype='step', label='inside (run 987)')
    plt.hist(df_331_9.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue', linestyle='-', weights=timeNorm(df_331_9.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,  range=binRange, histtype='step', label='outside (run 988)')
    plt.hist(df_331_10.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_331_10.qdc_lg_ch1[minevt:maxevt], 120),  range=binRange, histtype='step', label='inside (run 989)')
    plt.hist(df_331_11.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue', linestyle='-', weights=timeNorm(df_331_11.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,  range=binRange, histtype='step', label='outside (run 990)')
    plt.hist(df_331_12.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_331_12.qdc_lg_ch1[minevt:maxevt], 120),  range=binRange, histtype='step', label='inside (run 991)')
    plt.ylabel('[arb. units]')
    plt.xlabel('QDC [arb. units]')
    plt.grid(which='both')
    plt.legend()
    plt.yscale('log')


    numBins = 100
    binRange = [0,1050]
    plt.figure(4)
    plt.title('NE-331, Pulse-height (Outside)')
    plt.hist(df_331_1.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue', linestyle='-', weights=timeNorm(df_331_1.amplitude_ch1[minevt:maxevt], 120)*scaleFactor,  range=binRange, histtype='step', label='outside (run 980)')
    plt.hist(df_331_2.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_331_2.amplitude_ch1[minevt:maxevt], 120),  range=binRange, histtype='step', label='inside (run 981)')
    plt.hist(df_331_3.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue', linestyle='-', weights=timeNorm(df_331_3.amplitude_ch1[minevt:maxevt], 120)*scaleFactor,  range=binRange, histtype='step', label='outside (run 982)')
    plt.hist(df_331_4.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_331_4.amplitude_ch1[minevt:maxevt], 120),  range=binRange, histtype='step', label='inside (run 983)')
    plt.hist(df_331_5.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue', linestyle='-', weights=timeNorm(df_331_5.amplitude_ch1[minevt:maxevt], 120)*scaleFactor,  range=binRange, histtype='step', label='outside (run 984)')
    plt.hist(df_331_6.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_331_6.amplitude_ch1[minevt:maxevt], 120),  range=binRange, histtype='step', label='inside (run 985)')
    plt.hist(df_331_7.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue', linestyle='-', weights=timeNorm(df_331_7.amplitude_ch1[minevt:maxevt], 120)*scaleFactor,  range=binRange, histtype='step', label='outside (run 986)')
    plt.hist(df_331_8.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_331_8.amplitude_ch1[minevt:maxevt], 120),  range=binRange, histtype='step', label='inside (run 987)')
    plt.hist(df_331_9.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue', linestyle='-', weights=timeNorm(df_331_9.amplitude_ch1[minevt:maxevt], 120)*scaleFactor,  range=binRange, histtype='step', label='outside (run 988)')
    plt.hist(df_331_10.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_331_10.amplitude_ch1[minevt:maxevt], 120),  range=binRange, histtype='step', label='inside (run 989)')
    plt.hist(df_331_11.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue', linestyle='-', weights=timeNorm(df_331_11.amplitude_ch1[minevt:maxevt], 120)*scaleFactor,  range=binRange, histtype='step', label='outside (run 990)')
    plt.hist(df_331_12.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_331_12.amplitude_ch1[minevt:maxevt], 120),  range=binRange, histtype='step', label='inside (run 991)')
    plt.ylabel('[arb. units]')
    plt.xlabel('Amplitude [mV]')
    plt.legend()
    plt.grid(which='both')
    plt.yscale('log')
    plt.show()

def NE213_20db():
    """
    IN/OUT measurements with NE213 and german attenuator at -20dB
    """

    # :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    # ----- NE-213 -20dB - STUDY OF DIFFERCE BETWEEN DETECTOR INSIDE BEAM PORT (2.22 MeV calibration) AND OUTSIDE (normal run) -----
    # :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    df_213_1 = propd.load_parquet(path+'run00992', exclude_col) #out
    df_213_2 = propd.load_parquet(path+'run00993', exclude_col) #in
    df_213_3 = propd.load_parquet(path+'run00994', exclude_col) #out
    df_213_4 = propd.load_parquet(path+'run00995', exclude_col) #in
    df_213_5 = propd.load_parquet(path+'run00996', exclude_col) #out
    df_213_6 = propd.load_parquet(path+'run00997', exclude_col) #in
    df_213_7 = propd.load_parquet(path+'run00998', exclude_col) #out
    df_213_8 = propd.load_parquet(path+'run00999', exclude_col) #in
    df_213_9 = propd.load_parquet(path+'run01000', exclude_col) #out
    df_213_10 = propd.load_parquet(path+'run01001', exclude_col) #in
    df_213_11 = propd.load_parquet(path+'run01002', exclude_col) #out
    df_213_12 = propd.load_parquet(path+'run01003', exclude_col) #in

    minevt = 0
    maxevt = -1

    scaleFactor = 14

    numBins = 100
    binRange = [0,10000]
    plt.figure(5)
    plt.title('NE-213 (-20dB), QDC (Outside)')
    plt.hist(df_213_1.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue', linestyle='-', weights=timeNorm(df_213_1.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,  range=binRange, histtype='step', label='outside (run 992)')
    plt.hist(df_213_2.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_213_2.qdc_lg_ch1[minevt:maxevt], 120),  range=binRange, histtype='step', label='inside (run 993)')
    plt.hist(df_213_3.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue', linestyle='-', weights=timeNorm(df_213_3.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,  range=binRange, histtype='step', label='outside (run 994)')
    plt.hist(df_213_4.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_213_4.qdc_lg_ch1[minevt:maxevt], 120),  range=binRange, histtype='step', label='inside (run 995)')
    plt.hist(df_213_5.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue', linestyle='-', weights=timeNorm(df_213_5.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,  range=binRange, histtype='step', label='outside (run 996)')
    plt.hist(df_213_6.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_213_6.qdc_lg_ch1[minevt:maxevt], 120),  range=binRange, histtype='step', label='inside (run 997)')
    plt.hist(df_213_7.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue', linestyle='-', weights=timeNorm(df_213_7.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,  range=binRange, histtype='step', label='outside (run 998)')
    plt.hist(df_213_8.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_213_8.qdc_lg_ch1[minevt:maxevt], 120),  range=binRange, histtype='step', label='inside (run 999)')
    plt.hist(df_213_9.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue', linestyle='-', weights=timeNorm(df_213_9.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,  range=binRange, histtype='step', label='outside (run 1000)')
    plt.hist(df_213_10.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_213_10.qdc_lg_ch1[minevt:maxevt], 120),  range=binRange, histtype='step', label='inside (run 1001)')
    plt.hist(df_213_11.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue', linestyle='-', weights=timeNorm(df_213_11.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,  range=binRange, histtype='step', label='outside (run 1002)')
    plt.hist(df_213_12.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_213_12.qdc_lg_ch1[minevt:maxevt], 120),  range=binRange, histtype='step', label='inside (run 1003)')
    plt.ylabel('[arb. units]')
    plt.xlabel('QDC [arb. units]')
    plt.grid(which='both')
    plt.legend()
    plt.yscale('log')


    numBins = 100
    binRange = [0,1050]
    plt.figure(6)
    plt.title('NE-213 (-20dB), Pulse-height (Outside)')
    plt.hist(df_213_1.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue', linestyle='-', weights=timeNorm(df_213_1.amplitude_ch1[minevt:maxevt], 120)*scaleFactor,  range=binRange, histtype='step', label='outside (run 992)')
    plt.hist(df_213_2.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_213_2.amplitude_ch1[minevt:maxevt], 120),  range=binRange, histtype='step', label='inside (run 993)')
    plt.hist(df_213_3.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue', linestyle='-', weights=timeNorm(df_213_3.amplitude_ch1[minevt:maxevt], 120)*scaleFactor,  range=binRange, histtype='step', label='outside (run 994)')
    plt.hist(df_213_4.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_213_4.amplitude_ch1[minevt:maxevt], 120),  range=binRange, histtype='step', label='inside (run 995)')
    plt.hist(df_213_5.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue', linestyle='-', weights=timeNorm(df_213_5.amplitude_ch1[minevt:maxevt], 120)*scaleFactor,  range=binRange, histtype='step', label='outside (run 996)')
    plt.hist(df_213_6.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_213_6.amplitude_ch1[minevt:maxevt], 120),  range=binRange, histtype='step', label='inside (run 997)')
    plt.hist(df_213_7.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue', linestyle='-', weights=timeNorm(df_213_7.amplitude_ch1[minevt:maxevt], 120)*scaleFactor,  range=binRange, histtype='step', label='outside (run 998)')
    plt.hist(df_213_8.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_213_8.amplitude_ch1[minevt:maxevt], 120),  range=binRange, histtype='step', label='inside (run 999)')
    plt.hist(df_213_9.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue', linestyle='-', weights=timeNorm(df_213_9.amplitude_ch1[minevt:maxevt], 120)*scaleFactor,  range=binRange, histtype='step', label='outside (run 1000)')
    plt.hist(df_213_10.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_213_10.amplitude_ch1[minevt:maxevt], 120),  range=binRange, histtype='step', label='inside (run 1001)')
    plt.hist(df_213_11.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue', linestyle='-', weights=timeNorm(df_213_11.amplitude_ch1[minevt:maxevt], 120)*scaleFactor,  range=binRange, histtype='step', label='outside (run 1002)')
    plt.hist(df_213_12.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_213_12.amplitude_ch1[minevt:maxevt], 120),  range=binRange, histtype='step', label='inside (run 1003)')
    plt.ylabel('[arb. units]')
    plt.xlabel('Amplitude [mV]')
    plt.legend()
    plt.grid(which='both')
    plt.yscale('log')
    plt.show()

def EJ321P_20db():
    """
    IN/OUT measurements with EJ321P and german attenuator at -20dB
    """
    # :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    # ----- EJ-321P -20dB - STUDY OF DIFFERCE BETWEEN DETECTOR INSIDE BEAM PORT (2.22 MeV calibration) AND OUTSIDE (normal run) -----
    # :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    df_321P_1 = propd.load_parquet(path+'run01004', exclude_col) #out
    df_321P_2 = propd.load_parquet(path+'run01005', exclude_col) #in
    df_321P_3 = propd.load_parquet(path+'run01006', exclude_col) #out
    df_321P_4 = propd.load_parquet(path+'run01007', exclude_col) #in
    df_321P_5 = propd.load_parquet(path+'run01008', exclude_col) #out
    df_321P_6 = propd.load_parquet(path+'run01009', exclude_col) #in
    df_321P_7 = propd.load_parquet(path+'run01010', exclude_col) #out
    df_321P_8 = propd.load_parquet(path+'run01011', exclude_col) #in
    df_321P_9 = propd.load_parquet(path+'run01012', exclude_col) #out
    df_321P_10 = propd.load_parquet(path+'run01013', exclude_col) #in
    df_321P_11 = propd.load_parquet(path+'run01014', exclude_col) #out
    df_321P_12 = propd.load_parquet(path+'run01015', exclude_col) #in

    minevt = 0
    maxevt = -1

    scaleFactor = 14

    numBins = 100
    binRange = [0,10000]
    plt.figure(7)
    plt.title('EJ-321P (-20dB), QDC (Outside)')
    plt.hist(df_321P_1.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue',    linestyle='-', weights=timeNorm(df_321P_1.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,     range=binRange, histtype='step', label='outside (run 1004)')
    plt.hist(df_321P_2.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange',  linestyle='-', weights=timeNorm(df_321P_2.qdc_lg_ch1[minevt:maxevt], 120),                 range=binRange, histtype='step', label='inside (run 1005)')
    plt.hist(df_321P_3.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue',    linestyle='-', weights=timeNorm(df_321P_3.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,     range=binRange, histtype='step', label='outside (run 1006)')
    plt.hist(df_321P_4.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange',  linestyle='-', weights=timeNorm(df_321P_4.qdc_lg_ch1[minevt:maxevt], 120),                 range=binRange, histtype='step', label='inside (run 1007)')
    plt.hist(df_321P_5.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue',    linestyle='-', weights=timeNorm(df_321P_5.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,     range=binRange, histtype='step', label='outside (run 1008)')
    plt.hist(df_321P_6.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange',  linestyle='-', weights=timeNorm(df_321P_6.qdc_lg_ch1[minevt:maxevt], 120),                 range=binRange, histtype='step', label='inside (run 1009)')
    plt.hist(df_321P_7.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue',    linestyle='-', weights=timeNorm(df_321P_7.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,     range=binRange, histtype='step', label='outside (run 1010)')
    plt.hist(df_321P_8.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange',  linestyle='-', weights=timeNorm(df_321P_8.qdc_lg_ch1[minevt:maxevt], 120),                 range=binRange, histtype='step', label='inside (run 1011)')
    plt.hist(df_321P_9.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue',    linestyle='-', weights=timeNorm(df_321P_9.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,     range=binRange, histtype='step', label='outside (run 1012)')
    plt.hist(df_321P_10.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_321P_10.qdc_lg_ch1[minevt:maxevt], 120),                range=binRange, histtype='step', label='inside (run 1013)')
    plt.hist(df_321P_11.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue',   linestyle='-', weights=timeNorm(df_321P_11.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,    range=binRange, histtype='step', label='outside (run 1014)')
    plt.hist(df_321P_12.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_321P_12.qdc_lg_ch1[minevt:maxevt], 120),                range=binRange, histtype='step', label='inside (run 1015)')
    plt.ylabel('[arb. units]')
    plt.xlabel('QDC [arb. units]')
    plt.grid(which='both')
    plt.legend()
    plt.yscale('log')


    numBins = 100
    binRange = [0,1050]
    plt.figure(8)
    plt.title('EJ-321P (-20dB), Pulse-height (Outside)')
    plt.hist(df_321P_1.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue',    linestyle='-', weights=timeNorm(df_321P_1.amplitude_ch1[minevt:maxevt], 120)*scaleFactor,     range=binRange, histtype='step', label='outside (run 1004)')
    plt.hist(df_321P_2.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange',  linestyle='-', weights=timeNorm(df_321P_2.amplitude_ch1[minevt:maxevt], 120),                 range=binRange, histtype='step', label='inside (run 1005)')
    plt.hist(df_321P_3.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue',    linestyle='-', weights=timeNorm(df_321P_3.amplitude_ch1[minevt:maxevt], 120)*scaleFactor,     range=binRange, histtype='step', label='outside (run 1006)')
    plt.hist(df_321P_4.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange',  linestyle='-', weights=timeNorm(df_321P_4.amplitude_ch1[minevt:maxevt], 120),                 range=binRange, histtype='step', label='inside (run 1007)')
    plt.hist(df_321P_5.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue',    linestyle='-', weights=timeNorm(df_321P_5.amplitude_ch1[minevt:maxevt], 120)*scaleFactor,     range=binRange, histtype='step', label='outside (run 1008)')
    plt.hist(df_321P_6.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange',  linestyle='-', weights=timeNorm(df_321P_6.amplitude_ch1[minevt:maxevt], 120),                 range=binRange, histtype='step', label='inside (run 1009)')
    plt.hist(df_321P_7.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue',    linestyle='-', weights=timeNorm(df_321P_7.amplitude_ch1[minevt:maxevt], 120)*scaleFactor,     range=binRange, histtype='step', label='outside (run 1010)')
    plt.hist(df_321P_8.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange',  linestyle='-', weights=timeNorm(df_321P_8.amplitude_ch1[minevt:maxevt], 120),                 range=binRange, histtype='step', label='inside (run 1011)')
    plt.hist(df_321P_9.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue',    linestyle='-', weights=timeNorm(df_321P_9.amplitude_ch1[minevt:maxevt], 120)*scaleFactor,     range=binRange, histtype='step', label='outside (run 1012)')
    plt.hist(df_321P_10.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_321P_10.amplitude_ch1[minevt:maxevt], 120),                range=binRange, histtype='step', label='inside (run 1013)')
    plt.hist(df_321P_11.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue',   linestyle='-', weights=timeNorm(df_321P_11.amplitude_ch1[minevt:maxevt], 120)*scaleFactor,    range=binRange, histtype='step', label='outside (run 1014)')
    plt.hist(df_321P_12.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_321P_12.amplitude_ch1[minevt:maxevt], 120),                range=binRange, histtype='step', label='inside (run 1015)')
    plt.ylabel('[arb. units]')
    plt.xlabel('Amplitude [mV]')
    plt.legend()
    plt.grid(which='both')
    plt.yscale('log')
    plt.show()

def all_detectors_20dB():
    """
    Comparing all detectors with German attenuator @ -20dB
    """
    # :::::::::::::::::::::::::::::::::::::::::::::::::::
    # MULTIPLE PLOTS IN/OUT COMPARISON WITH -20dB
    # :::::::::::::::::::::::::::::::::::::::::::::::::::
    df_331_1 = propd.load_parquet(path+'run00980', exclude_col) #out
    df_331_2 = propd.load_parquet(path+'run00981', exclude_col) #in
    df_331_3 = propd.load_parquet(path+'run00982', exclude_col) #out
    df_331_4 = propd.load_parquet(path+'run00983', exclude_col) #in
    df_331_5 = propd.load_parquet(path+'run00984', exclude_col) #out
    df_331_6 = propd.load_parquet(path+'run00985', exclude_col) #in
    df_331_7 = propd.load_parquet(path+'run00986', exclude_col) #out
    df_331_8 = propd.load_parquet(path+'run00987', exclude_col) #in
    df_331_9 = propd.load_parquet(path+'run00988', exclude_col) #out
    df_331_10 = propd.load_parquet(path+'run00989', exclude_col) #in
    df_331_11 = propd.load_parquet(path+'run00990', exclude_col) #out
    df_331_12 = propd.load_parquet(path+'run00991', exclude_col) #in

    df_305_1 = propd.load_parquet(path+'run00968', exclude_col) #out
    df_305_2 = propd.load_parquet(path+'run00969', exclude_col) #in
    df_305_3 = propd.load_parquet(path+'run00970', exclude_col) #out
    df_305_4 = propd.load_parquet(path+'run00971', exclude_col) #in
    df_305_5 = propd.load_parquet(path+'run00972', exclude_col) #out
    df_305_6 = propd.load_parquet(path+'run00973', exclude_col) #in
    df_305_7 = propd.load_parquet(path+'run00974', exclude_col) #out
    df_305_8 = propd.load_parquet(path+'run00975', exclude_col) #in
    df_305_9 = propd.load_parquet(path+'run00976', exclude_col) #out
    df_305_10 = propd.load_parquet(path+'run00977', exclude_col) #in
    df_305_11 = propd.load_parquet(path+'run00978', exclude_col) #out
    df_305_12 = propd.load_parquet(path+'run00979', exclude_col) #in

    df_213_1 = propd.load_parquet(path+'run00992', exclude_col) #out
    df_213_2 = propd.load_parquet(path+'run00993', exclude_col) #in
    df_213_3 = propd.load_parquet(path+'run00994', exclude_col) #out
    df_213_4 = propd.load_parquet(path+'run00995', exclude_col) #in
    df_213_5 = propd.load_parquet(path+'run00996', exclude_col) #out
    df_213_6 = propd.load_parquet(path+'run00997', exclude_col) #in
    df_213_7 = propd.load_parquet(path+'run00998', exclude_col) #out
    df_213_8 = propd.load_parquet(path+'run00999', exclude_col) #in
    df_213_9 = propd.load_parquet(path+'run01000', exclude_col) #out
    df_213_10 = propd.load_parquet(path+'run01001', exclude_col) #in
    df_213_11 = propd.load_parquet(path+'run01002', exclude_col) #out
    df_213_12 = propd.load_parquet(path+'run01003', exclude_col) #in

    df_321P_1 = propd.load_parquet(path+'run01004', exclude_col) #out
    df_321P_2 = propd.load_parquet(path+'run01005', exclude_col) #in
    df_321P_3 = propd.load_parquet(path+'run01006', exclude_col) #out
    df_321P_4 = propd.load_parquet(path+'run01007', exclude_col) #in
    df_321P_5 = propd.load_parquet(path+'run01008', exclude_col) #out
    df_321P_6 = propd.load_parquet(path+'run01009', exclude_col) #in
    df_321P_7 = propd.load_parquet(path+'run01010', exclude_col) #out
    df_321P_8 = propd.load_parquet(path+'run01011', exclude_col) #in
    df_321P_9 = propd.load_parquet(path+'run01012', exclude_col) #out
    df_321P_10 = propd.load_parquet(path+'run01013', exclude_col) #in
    df_321P_11 = propd.load_parquet(path+'run01014', exclude_col) #out
    df_321P_12 = propd.load_parquet(path+'run01015', exclude_col) #in

    #QDC
    scaleFactor = 12.25
    plt.figure(9)
    plt.subplot(1,4,1)
    numBins = 100
    binRange = [0,17000]
    plt.hist(df_305_1.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue',    linestyle='-', weights=timeNorm(df_305_1.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,     range=binRange, histtype='step', label='outside')
    plt.hist(df_305_2.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange',  linestyle='-', weights=timeNorm(df_305_2.qdc_lg_ch1[minevt:maxevt], 120),                 range=binRange, histtype='step', label='inside')
    plt.hist(df_305_3.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue',    linestyle='-', weights=timeNorm(df_305_3.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,     range=binRange, histtype='step')
    plt.hist(df_305_4.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange',  linestyle='-', weights=timeNorm(df_305_4.qdc_lg_ch1[minevt:maxevt], 120),                 range=binRange, histtype='step')
    plt.hist(df_305_5.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue',    linestyle='-', weights=timeNorm(df_305_5.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,     range=binRange, histtype='step')
    plt.hist(df_305_6.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange',  linestyle='-', weights=timeNorm(df_305_6.qdc_lg_ch1[minevt:maxevt], 120),                 range=binRange, histtype='step')
    plt.hist(df_305_7.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue',    linestyle='-', weights=timeNorm(df_305_7.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,     range=binRange, histtype='step')
    plt.hist(df_305_8.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange',  linestyle='-', weights=timeNorm(df_305_8.qdc_lg_ch1[minevt:maxevt], 120),                 range=binRange, histtype='step')
    plt.hist(df_305_9.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue',    linestyle='-', weights=timeNorm(df_305_9.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,     range=binRange, histtype='step')
    plt.hist(df_305_10.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_305_10.qdc_lg_ch1[minevt:maxevt], 120),                range=binRange, histtype='step')
    plt.hist(df_305_11.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue',   linestyle='-', weights=timeNorm(df_305_11.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,    range=binRange, histtype='step')
    plt.hist(df_305_12.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_305_12.qdc_lg_ch1[minevt:maxevt], 120),                range=binRange, histtype='step')
    plt.title('EJ-305')
    plt.ylabel('[arb. units]')
    plt.xlabel('QDC [arb. units]')
    # plt.grid(which='both')
    plt.legend()
    plt.yscale('log')

    scaleFactor = 13
    plt.subplot(1,4,2)
    numBins = 100
    binRange = [0,9000]
    plt.hist(df_331_1.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue',    linestyle='-', weights=timeNorm(df_331_1.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,     range=binRange, histtype='step', label='outside')
    plt.hist(df_331_2.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange',  linestyle='-', weights=timeNorm(df_331_2.qdc_lg_ch1[minevt:maxevt], 120),                 range=binRange, histtype='step', label='inside')
    plt.hist(df_331_3.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue',    linestyle='-', weights=timeNorm(df_331_3.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,     range=binRange, histtype='step')
    plt.hist(df_331_4.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange',  linestyle='-', weights=timeNorm(df_331_4.qdc_lg_ch1[minevt:maxevt], 120),                 range=binRange, histtype='step')
    plt.hist(df_331_5.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue',    linestyle='-', weights=timeNorm(df_331_5.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,     range=binRange, histtype='step')
    plt.hist(df_331_6.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange',  linestyle='-', weights=timeNorm(df_331_6.qdc_lg_ch1[minevt:maxevt], 120),                 range=binRange, histtype='step')
    plt.hist(df_331_7.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue',    linestyle='-', weights=timeNorm(df_331_7.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,     range=binRange, histtype='step')
    plt.hist(df_331_8.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange',  linestyle='-', weights=timeNorm(df_331_8.qdc_lg_ch1[minevt:maxevt], 120),                 range=binRange, histtype='step')
    plt.hist(df_331_9.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue',    linestyle='-', weights=timeNorm(df_331_9.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,     range=binRange, histtype='step')
    plt.hist(df_331_10.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_331_10.qdc_lg_ch1[minevt:maxevt], 120),                range=binRange, histtype='step')
    plt.hist(df_331_11.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue',   linestyle='-', weights=timeNorm(df_331_11.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,    range=binRange, histtype='step')
    plt.hist(df_331_12.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_331_12.qdc_lg_ch1[minevt:maxevt], 120),                range=binRange, histtype='step')
    plt.title('EJ-331(Gd)')
    plt.xlabel('QDC [arb. units]')
    # plt.grid(which='both')
    plt.legend()
    plt.yscale('log')

    scaleFactor = 11.75
    plt.subplot(1,4,3)
    numBins = 100
    binRange = [0,7500]
    plt.hist(df_213_1.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue',    linestyle='-', weights=timeNorm(df_213_1.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,     range=binRange, histtype='step', label='outside')
    plt.hist(df_213_2.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange',  linestyle='-', weights=timeNorm(df_213_2.qdc_lg_ch1[minevt:maxevt], 120),                 range=binRange, histtype='step', label='inside')
    plt.hist(df_213_3.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue',    linestyle='-', weights=timeNorm(df_213_3.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,     range=binRange, histtype='step')
    plt.hist(df_213_4.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange',  linestyle='-', weights=timeNorm(df_213_4.qdc_lg_ch1[minevt:maxevt], 120),                 range=binRange, histtype='step')
    plt.hist(df_213_5.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue',    linestyle='-', weights=timeNorm(df_213_5.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,     range=binRange, histtype='step')
    plt.hist(df_213_6.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange',  linestyle='-', weights=timeNorm(df_213_6.qdc_lg_ch1[minevt:maxevt], 120),                 range=binRange, histtype='step')
    plt.hist(df_213_7.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue',    linestyle='-', weights=timeNorm(df_213_7.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,     range=binRange, histtype='step')
    plt.hist(df_213_8.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange',  linestyle='-', weights=timeNorm(df_213_8.qdc_lg_ch1[minevt:maxevt], 120),                 range=binRange, histtype='step')
    plt.hist(df_213_9.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue',    linestyle='-', weights=timeNorm(df_213_9.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,     range=binRange, histtype='step')
    plt.hist(df_213_10.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_213_10.qdc_lg_ch1[minevt:maxevt], 120),                range=binRange, histtype='step')
    plt.hist(df_213_11.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue',   linestyle='-', weights=timeNorm(df_213_11.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,    range=binRange, histtype='step')
    plt.hist(df_213_12.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_213_12.qdc_lg_ch1[minevt:maxevt], 120),                range=binRange, histtype='step')
    plt.title('NE-213')
    plt.xlabel('QDC [arb. units]')
    # plt.grid(which='both')
    plt.legend()
    plt.yscale('log')

    scaleFactor = 11.75
    plt.subplot(1,4,4)
    numBins = 100
    binRange = [0,6500]
    plt.hist(df_321P_1.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue',    linestyle='-', weights=timeNorm(df_321P_1.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,     range=binRange, histtype='step', label='outside')
    plt.hist(df_321P_2.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange',  linestyle='-', weights=timeNorm(df_321P_2.qdc_lg_ch1[minevt:maxevt], 120),                 range=binRange, histtype='step', label='inside')
    plt.hist(df_321P_3.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue',    linestyle='-', weights=timeNorm(df_321P_3.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,     range=binRange, histtype='step')
    plt.hist(df_321P_4.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange',  linestyle='-', weights=timeNorm(df_321P_4.qdc_lg_ch1[minevt:maxevt], 120),                 range=binRange, histtype='step')
    plt.hist(df_321P_5.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue',    linestyle='-', weights=timeNorm(df_321P_5.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,     range=binRange, histtype='step')
    plt.hist(df_321P_6.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange',  linestyle='-', weights=timeNorm(df_321P_6.qdc_lg_ch1[minevt:maxevt], 120),                 range=binRange, histtype='step')
    plt.hist(df_321P_7.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue',    linestyle='-', weights=timeNorm(df_321P_7.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,     range=binRange, histtype='step')
    plt.hist(df_321P_8.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange',  linestyle='-', weights=timeNorm(df_321P_8.qdc_lg_ch1[minevt:maxevt], 120),                 range=binRange, histtype='step')
    plt.hist(df_321P_9.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue',    linestyle='-', weights=timeNorm(df_321P_9.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,     range=binRange, histtype='step')
    plt.hist(df_321P_10.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_321P_10.qdc_lg_ch1[minevt:maxevt], 120),                range=binRange, histtype='step')
    plt.hist(df_321P_11.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue',   linestyle='-', weights=timeNorm(df_321P_11.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,    range=binRange, histtype='step')
    plt.hist(df_321P_12.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_321P_12.qdc_lg_ch1[minevt:maxevt], 120),                range=binRange, histtype='step')
    plt.title('EJ-321P')
    plt.ylabel('[arb. units]')
    plt.xlabel('QDC [arb. units]')
    # plt.grid(which='both')
    plt.legend()
    plt.yscale('log')
    # plt.show()

    #AMPLITUDE
    numBins = 100
    binRange = [0,1050]
    plt.figure(10)
    plt.subplot(1,4,1)
    plt.hist(df_331_1.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue',    linestyle='-', weights=timeNorm(df_331_1.amplitude_ch1[minevt:maxevt], 120)*scaleFactor,   range=binRange, histtype='step', label='outside')
    plt.hist(df_331_2.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange',  linestyle='-', weights=timeNorm(df_331_2.amplitude_ch1[minevt:maxevt], 120),               range=binRange, histtype='step', label='inside')
    plt.hist(df_331_3.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue',    linestyle='-', weights=timeNorm(df_331_3.amplitude_ch1[minevt:maxevt], 120)*scaleFactor,   range=binRange, histtype='step')
    plt.hist(df_331_4.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange',  linestyle='-', weights=timeNorm(df_331_4.amplitude_ch1[minevt:maxevt], 120),               range=binRange, histtype='step')
    plt.hist(df_331_5.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue',    linestyle='-', weights=timeNorm(df_331_5.amplitude_ch1[minevt:maxevt], 120)*scaleFactor,   range=binRange, histtype='step')
    plt.hist(df_331_6.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange',  linestyle='-', weights=timeNorm(df_331_6.amplitude_ch1[minevt:maxevt], 120),               range=binRange, histtype='step')
    plt.hist(df_331_7.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue',    linestyle='-', weights=timeNorm(df_331_7.amplitude_ch1[minevt:maxevt], 120)*scaleFactor,   range=binRange, histtype='step')
    plt.hist(df_331_8.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange',  linestyle='-', weights=timeNorm(df_331_8.amplitude_ch1[minevt:maxevt], 120),               range=binRange, histtype='step')
    plt.hist(df_331_9.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue',    linestyle='-', weights=timeNorm(df_331_9.amplitude_ch1[minevt:maxevt], 120)*scaleFactor,   range=binRange, histtype='step')
    plt.hist(df_331_10.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_331_10.amplitude_ch1[minevt:maxevt], 120),              range=binRange, histtype='step')
    plt.hist(df_331_11.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue',   linestyle='-', weights=timeNorm(df_331_11.amplitude_ch1[minevt:maxevt], 120)*scaleFactor,  range=binRange, histtype='step')
    plt.hist(df_331_12.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_331_12.amplitude_ch1[minevt:maxevt], 120),              range=binRange, histtype='step')
    plt.title('EJ-331(Gd)')
    plt.ylabel('[arb. units]')
    plt.xlabel('Amplitude [mV]')
    # plt.grid(which='both')
    plt.legend()
    plt.yscale('log')

    plt.subplot(1,4,2)
    plt.hist(df_305_1.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue',    linestyle='-', weights=timeNorm(df_305_1.amplitude_ch1[minevt:maxevt], 120)*scaleFactor,   range=binRange, histtype='step', label='outside')
    plt.hist(df_305_2.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange',  linestyle='-', weights=timeNorm(df_305_2.amplitude_ch1[minevt:maxevt], 120),               range=binRange, histtype='step', label='inside')
    plt.hist(df_305_3.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue',    linestyle='-', weights=timeNorm(df_305_3.amplitude_ch1[minevt:maxevt], 120)*scaleFactor,   range=binRange, histtype='step')
    plt.hist(df_305_4.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange',  linestyle='-', weights=timeNorm(df_305_4.amplitude_ch1[minevt:maxevt], 120),               range=binRange, histtype='step')
    plt.hist(df_305_5.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue',    linestyle='-', weights=timeNorm(df_305_5.amplitude_ch1[minevt:maxevt], 120)*scaleFactor,   range=binRange, histtype='step')
    plt.hist(df_305_6.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange',  linestyle='-', weights=timeNorm(df_305_6.amplitude_ch1[minevt:maxevt], 120),               range=binRange, histtype='step')
    plt.hist(df_305_7.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue',    linestyle='-', weights=timeNorm(df_305_7.amplitude_ch1[minevt:maxevt], 120)*scaleFactor,   range=binRange, histtype='step')
    plt.hist(df_305_8.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange',  linestyle='-', weights=timeNorm(df_305_8.amplitude_ch1[minevt:maxevt], 120),               range=binRange, histtype='step')
    plt.hist(df_305_9.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue',    linestyle='-', weights=timeNorm(df_305_9.amplitude_ch1[minevt:maxevt], 120)*scaleFactor,   range=binRange, histtype='step')
    plt.hist(df_305_10.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_305_10.amplitude_ch1[minevt:maxevt], 120),              range=binRange, histtype='step')
    plt.hist(df_305_11.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue',   linestyle='-', weights=timeNorm(df_305_11.amplitude_ch1[minevt:maxevt], 120)*scaleFactor,  range=binRange, histtype='step')
    plt.hist(df_305_12.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_305_12.amplitude_ch1[minevt:maxevt], 120),              range=binRange, histtype='step')
    plt.title('EJ-305')
    plt.xlabel('Amplitude [mV]')
    # plt.grid(which='both')
    plt.legend()
    plt.yscale('log')

    plt.subplot(1,4,3)
    plt.hist(df_213_1.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue',    linestyle='-', weights=timeNorm(df_213_1.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,      range=binRange, histtype='step', label='outside')
    plt.hist(df_213_2.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange',  linestyle='-', weights=timeNorm(df_213_2.qdc_lg_ch1[minevt:maxevt], 120),                  range=binRange, histtype='step', label='inside')
    plt.hist(df_213_3.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue',    linestyle='-', weights=timeNorm(df_213_3.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,      range=binRange, histtype='step')
    plt.hist(df_213_4.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange',  linestyle='-', weights=timeNorm(df_213_4.qdc_lg_ch1[minevt:maxevt], 120),                  range=binRange, histtype='step')
    plt.hist(df_213_5.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue',    linestyle='-', weights=timeNorm(df_213_5.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,      range=binRange, histtype='step')
    plt.hist(df_213_6.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange',  linestyle='-', weights=timeNorm(df_213_6.qdc_lg_ch1[minevt:maxevt], 120),                  range=binRange, histtype='step')
    plt.hist(df_213_7.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue',    linestyle='-', weights=timeNorm(df_213_7.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,      range=binRange, histtype='step')
    plt.hist(df_213_8.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange',  linestyle='-', weights=timeNorm(df_213_8.qdc_lg_ch1[minevt:maxevt], 120),                  range=binRange, histtype='step')
    plt.hist(df_213_9.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue',    linestyle='-', weights=timeNorm(df_213_9.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,      range=binRange, histtype='step')
    plt.hist(df_213_10.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_213_10.qdc_lg_ch1[minevt:maxevt], 120),                 range=binRange, histtype='step')
    plt.hist(df_213_11.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue',   linestyle='-', weights=timeNorm(df_213_11.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,     range=binRange, histtype='step')
    plt.hist(df_213_12.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_213_12.qdc_lg_ch1[minevt:maxevt], 120),                 range=binRange, histtype='step')
    plt.title('NE-213')
    plt.xlabel('Amplitude [mV]')
    # plt.grid(which='both')
    plt.legend()
    plt.yscale('log')

    plt.subplot(1,4,4)
    plt.hist(df_321P_1.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue',    linestyle='-', weights=timeNorm(df_321P_1.amplitude_ch1[minevt:maxevt], 120)*scaleFactor,     range=binRange, histtype='step', label='outside')
    plt.hist(df_321P_2.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange',  linestyle='-', weights=timeNorm(df_321P_2.amplitude_ch1[minevt:maxevt], 120),                 range=binRange, histtype='step', label='inside')
    plt.hist(df_321P_3.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue',    linestyle='-', weights=timeNorm(df_321P_3.amplitude_ch1[minevt:maxevt], 120)*scaleFactor,     range=binRange, histtype='step')
    plt.hist(df_321P_4.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange',  linestyle='-', weights=timeNorm(df_321P_4.amplitude_ch1[minevt:maxevt], 120),                 range=binRange, histtype='step')
    plt.hist(df_321P_5.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue',    linestyle='-', weights=timeNorm(df_321P_5.amplitude_ch1[minevt:maxevt], 120)*scaleFactor,     range=binRange, histtype='step')
    plt.hist(df_321P_6.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange',  linestyle='-', weights=timeNorm(df_321P_6.amplitude_ch1[minevt:maxevt], 120),                 range=binRange, histtype='step')
    plt.hist(df_321P_7.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue',    linestyle='-', weights=timeNorm(df_321P_7.amplitude_ch1[minevt:maxevt], 120)*scaleFactor,     range=binRange, histtype='step')
    plt.hist(df_321P_8.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange',  linestyle='-', weights=timeNorm(df_321P_8.amplitude_ch1[minevt:maxevt], 120),                 range=binRange, histtype='step')
    plt.hist(df_321P_9.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue',    linestyle='-', weights=timeNorm(df_321P_9.amplitude_ch1[minevt:maxevt], 120)*scaleFactor,     range=binRange, histtype='step')
    plt.hist(df_321P_10.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_321P_10.amplitude_ch1[minevt:maxevt], 120),                range=binRange, histtype='step')
    plt.hist(df_321P_11.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue',   linestyle='-', weights=timeNorm(df_321P_11.amplitude_ch1[minevt:maxevt], 120)*scaleFactor,    range=binRange, histtype='step')
    plt.hist(df_321P_12.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_321P_12.amplitude_ch1[minevt:maxevt], 120),                range=binRange, histtype='step')
    plt.title('EJ-321P')
    plt.ylabel('[arb. units]')
    plt.xlabel('Amplitude [mV]')
    # plt.grid(which='both')
    plt.legend()
    plt.yscale('log')

    plt.show()

def EJ305_DE_vs_CAEN():
    """
    Comparing EJ305 detector with german attenuator -20 db and CAEN attenuator -20dB
    """
    df_305_1 = propd.load_parquet(path+'run00968', exclude_col) #out
    df_305_2 = propd.load_parquet(path+'run00969', exclude_col) #in
    df_305_3 = propd.load_parquet(path+'run00970', exclude_col) #out
    df_305_4 = propd.load_parquet(path+'run00971', exclude_col) #in
    df_305_5 = propd.load_parquet(path+'run00972', exclude_col) #out
    df_305_6 = propd.load_parquet(path+'run00973', exclude_col) #in
    df_305_7 = propd.load_parquet(path+'run00974', exclude_col) #out
    df_305_8 = propd.load_parquet(path+'run00975', exclude_col) #in
    df_305_9 = propd.load_parquet(path+'run00976', exclude_col) #out
    df_305_10 = propd.load_parquet(path+'run00977', exclude_col) #in
    df_305_11 = propd.load_parquet(path+'run00978', exclude_col) #out
    df_305_12 = propd.load_parquet(path+'run00979', exclude_col) #in

    minevt = 0
    maxevt = -1

    #QDC
    scaleFactor = 10
    plt.figure(9)
    numBins = 100
    binRange = [0,17000]
    plt.hist(df_305_1.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue',    linestyle='-', weights=timeNorm(df_305_1.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,     range=binRange, histtype='step', label='outside')
    plt.hist(df_305_2.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange',  linestyle='-', weights=timeNorm(df_305_2.qdc_lg_ch1[minevt:maxevt], 120),                 range=binRange, histtype='step', label='inside')
    plt.hist(df_305_3.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue',    linestyle='-', weights=timeNorm(df_305_3.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,     range=binRange, histtype='step')
    plt.hist(df_305_4.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange',  linestyle='-', weights=timeNorm(df_305_4.qdc_lg_ch1[minevt:maxevt], 120),                 range=binRange, histtype='step')
    plt.hist(df_305_5.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue',    linestyle='-', weights=timeNorm(df_305_5.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,     range=binRange, histtype='step')
    plt.hist(df_305_6.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange',  linestyle='-', weights=timeNorm(df_305_6.qdc_lg_ch1[minevt:maxevt], 120),                 range=binRange, histtype='step')
    plt.hist(df_305_7.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue',    linestyle='-', weights=timeNorm(df_305_7.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,     range=binRange, histtype='step')
    plt.hist(df_305_8.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange',  linestyle='-', weights=timeNorm(df_305_8.qdc_lg_ch1[minevt:maxevt], 120),                 range=binRange, histtype='step')
    plt.hist(df_305_9.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue',    linestyle='-', weights=timeNorm(df_305_9.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,     range=binRange, histtype='step')
    plt.hist(df_305_10.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_305_10.qdc_lg_ch1[minevt:maxevt], 120),                range=binRange, histtype='step')
    plt.hist(df_305_11.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue',   linestyle='-', weights=timeNorm(df_305_11.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,    range=binRange, histtype='step')
    plt.hist(df_305_12.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_305_12.qdc_lg_ch1[minevt:maxevt], 120),                range=binRange, histtype='step')
    plt.title('EJ-305')
    plt.ylabel('[arb. units]')
    plt.xlabel('QDC [arb. units]')
    # plt.grid(which='both')
    plt.legend()
    plt.yscale('log')

    # :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    # ----- EJ-305 -20dB WITH CAEN ATTENUATOR - STUDY OF DIFFERCE BETWEEN DETECTOR INSIDE BEAM PORT (2.22 MeV calibration) AND OUTSIDE (normal run) -----
    # :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    df_305_1 = propd.load_parquet(path+'run01016', exclude_col) #out
    df_305_2 = propd.load_parquet(path+'run01017', exclude_col) #in
    df_305_3 = propd.load_parquet(path+'run01018', exclude_col) #out
    df_305_4 = propd.load_parquet(path+'run01019', exclude_col) #in
    df_305_5 = propd.load_parquet(path+'run01020', exclude_col) #out
    df_305_6 = propd.load_parquet(path+'run01021', exclude_col) #in
    df_305_7 = propd.load_parquet(path+'run01022', exclude_col) #out
    df_305_8 = propd.load_parquet(path+'run01023', exclude_col) #in
    df_305_9 = propd.load_parquet(path+'run01024', exclude_col) #out
    df_305_10 = propd.load_parquet(path+'run01025', exclude_col) #in
    df_305_11 = propd.load_parquet(path+'run01026', exclude_col) #out
    df_305_12 = propd.load_parquet(path+'run01027', exclude_col) #in

    minevt = 0
    maxevt = -1

    scaleFactor = 10

    numBins = 100
    binRange = [0,17000]
    plt.figure(1)
    plt.title('EJ-305, QDC (Outside) /w CAEN')
    plt.hist(df_305_1.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue', linestyle='-', weights=timeNorm(df_305_1.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,  range=binRange, histtype='step', label='outside (run 1016)')
    plt.hist(df_305_2.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_305_2.qdc_lg_ch1[minevt:maxevt], 120),  range=binRange, histtype='step', label='inside (run 1017)')
    plt.hist(df_305_3.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue', linestyle='-', weights=timeNorm(df_305_3.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,  range=binRange, histtype='step', label='outside (run 1018)')
    plt.hist(df_305_4.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_305_4.qdc_lg_ch1[minevt:maxevt], 120),  range=binRange, histtype='step', label='inside (run 1019)')
    plt.hist(df_305_5.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue', linestyle='-', weights=timeNorm(df_305_5.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,  range=binRange, histtype='step', label='outside (run 1020)')
    plt.hist(df_305_6.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_305_6.qdc_lg_ch1[minevt:maxevt], 120),  range=binRange, histtype='step', label='inside (run 1021)')
    plt.hist(df_305_7.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue', linestyle='-', weights=timeNorm(df_305_7.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,  range=binRange, histtype='step', label='outside (run 1022)')
    plt.hist(df_305_8.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_305_8.qdc_lg_ch1[minevt:maxevt], 120),  range=binRange, histtype='step', label='inside (run 1023)')
    plt.hist(df_305_9.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue', linestyle='-', weights=timeNorm(df_305_9.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,  range=binRange, histtype='step', label='outside (run 1024)')
    plt.hist(df_305_10.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_305_10.qdc_lg_ch1[minevt:maxevt], 120),  range=binRange, histtype='step', label='inside (run 1025)')
    plt.hist(df_305_11.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue', linestyle='-', weights=timeNorm(df_305_11.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,  range=binRange, histtype='step', label='outside (run 1026)')
    plt.hist(df_305_12.qdc_lg_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_305_12.qdc_lg_ch1[minevt:maxevt], 120),  range=binRange, histtype='step', label='inside (run 1027)')
    plt.ylabel('[arb. units]')
    plt.xlabel('QDC [arb. units]')
    # plt.grid(which='both')
    plt.legend()
    plt.yscale('log')


    numBins = 100
    binRange = [0,1050]
    plt.figure(2)
    plt.title('NE-305, Pulse-height (Outside) /w CAEN')
    plt.hist(df_305_1.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue', linestyle='-', weights=timeNorm(df_305_1.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,  range=binRange, histtype='step', label='outside (run 1016)')
    plt.hist(df_305_2.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_305_2.qdc_lg_ch1[minevt:maxevt], 120),  range=binRange, histtype='step', label='inside (run 1017)')
    plt.hist(df_305_3.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue', linestyle='-', weights=timeNorm(df_305_3.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,  range=binRange, histtype='step', label='outside (run 1018)')
    plt.hist(df_305_4.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_305_4.qdc_lg_ch1[minevt:maxevt], 120),  range=binRange, histtype='step', label='inside (run 1019)')
    plt.hist(df_305_5.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue', linestyle='-', weights=timeNorm(df_305_5.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,  range=binRange, histtype='step', label='outside (run 1020)')
    plt.hist(df_305_6.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_305_6.qdc_lg_ch1[minevt:maxevt], 120),  range=binRange, histtype='step', label='inside (run 1021)')
    plt.hist(df_305_7.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue', linestyle='-', weights=timeNorm(df_305_7.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,  range=binRange, histtype='step', label='outside (run 1022)')
    plt.hist(df_305_8.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_305_8.qdc_lg_ch1[minevt:maxevt], 120),  range=binRange, histtype='step', label='inside (run 1023)')
    plt.hist(df_305_9.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue', linestyle='-', weights=timeNorm(df_305_9.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,  range=binRange, histtype='step', label='outside (run 1024)')
    plt.hist(df_305_10.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_305_10.qdc_lg_ch1[minevt:maxevt], 120),  range=binRange, histtype='step', label='inside (run 1025)')
    plt.hist(df_305_11.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='blue', linestyle='-', weights=timeNorm(df_305_11.qdc_lg_ch1[minevt:maxevt], 120)*scaleFactor,  range=binRange, histtype='step', label='outside (run 1026)')
    plt.hist(df_305_12.amplitude_ch1[minevt:maxevt], bins=numBins, lw=2, color='orange', linestyle='-', weights=timeNorm(df_305_12.qdc_lg_ch1[minevt:maxevt], 120),  range=binRange, histtype='step', label='inside (run 1027)')
    plt.ylabel('[arb. units]')
    plt.xlabel('Amplitude [mV]')
    plt.legend()
    # plt.grid(which='both')
    plt.yscale('log')
    plt.show()

def EJ305_baseline():
    """
    Plotting baseline around 2.2 MeV and 4.44 MeV from EJ305 detector with CAEN attenuator at -20dB
    """
    path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop
    
    range_2MeV_OUT = [5448,6453]
    range_2MeV_IN = [6137,7142] 
    range_4MeV_OUT = [11189,12435]
    range_4MeV_IN = [11754,13000] 
    
    # range_2MeV_OUT = [0,17000]
    # range_2MeV_IN = [0,17000]
    # range_4MeV_OUT = [0,17000]
    # range_4MeV_IN = [0,17000]

    df_305_1 = pd.read_parquet(path+'run01016', columns=['qdc_lg_ch1','baseline_ch1']) #out
    df_305_2 = pd.read_parquet(path+'run01017', columns=['qdc_lg_ch1','baseline_ch1']) #in
    df_305_3 = pd.read_parquet(path+'run01018', columns=['qdc_lg_ch1','baseline_ch1']) #out
    df_305_4 = pd.read_parquet(path+'run01019', columns=['qdc_lg_ch1','baseline_ch1']) #in
    df_305_5 = pd.read_parquet(path+'run01020', columns=['qdc_lg_ch1','baseline_ch1']) #out
    df_305_6 = pd.read_parquet(path+'run01021', columns=['qdc_lg_ch1','baseline_ch1']) #in
    df_305_7 = pd.read_parquet(path+'run01022', columns=['qdc_lg_ch1','baseline_ch1']) #out
    df_305_8 = pd.read_parquet(path+'run01023', columns=['qdc_lg_ch1','baseline_ch1']) #in
    df_305_9 = pd.read_parquet(path+'run01024', columns=['qdc_lg_ch1','baseline_ch1']) #out
    df_305_10 = pd.read_parquet(path+'run01025', columns=['qdc_lg_ch1','baseline_ch1']) #in
    df_305_11 = pd.read_parquet(path+'run01026', columns=['qdc_lg_ch1','baseline_ch1']) #out
    df_305_12 = pd.read_parquet(path+'run01027', columns=['qdc_lg_ch1','baseline_ch1']) #in
    

    # df_305_OUT_2MeV = df_305_1.query(f'qdc_lg_ch1>{range_2MeV_OUT[0]} and qdc_lg_ch1<{range_2MeV_OUT[1]}').baseline
    # df_305_IN_2MeV = df_305_2.query(f'qdc_lg_ch1>{range_2MeV_IN[0]} and qdc_lg_ch1<{range_2MeV_IN[1]}')
    # df_305_OUT_4MeV = df_305_1.query(f'qdc_lg_ch1>{range_4MeV_OUT[0]} and qdc_lg_ch1<{range_4MeV_OUT[1]}')
    # df_305_IN_4MeV = df_305_2.query(f'qdc_lg_ch1>{range_4MeV_IN[0]} and qdc_lg_ch1<{range_4MeV_IN[1]}')

    # df_305_OUT_2MeV = df_305_OUT_2MeV.reset_index()
    # df_305_IN_2MeV = df_305_IN_2MeV.reset_index()
    # df_305_OUT_4MeV = df_305_OUT_4MeV.reset_index()
    # df_305_IN_4MeV = df_305_IN_4MeV.reset_index()


    plt.figure(1)
    plt.subplot(2,1,1)
    plt.title('EJ-305, baseline @ 2.2 MeV')
    binRange = [1000,1007]
    numBins = 70
    plt.hist(df_305_1.query(f'qdc_lg_ch1>{range_2MeV_OUT[0]} and qdc_lg_ch1<{range_2MeV_OUT[1]}').baseline_ch1, bins=numBins, range=binRange, color='blue', density=True, lw=3, histtype='step', label='run01016')
    plt.hist(df_305_2.query(f'qdc_lg_ch1>{range_2MeV_IN[0]} and qdc_lg_ch1<{range_2MeV_IN[1]}').baseline_ch1, bins=numBins, range=binRange, color='orange', density=True, lw=3, histtype='step', label='run01017')
    plt.hist(df_305_3.query(f'qdc_lg_ch1>{range_2MeV_OUT[0]} and qdc_lg_ch1<{range_2MeV_OUT[1]}').baseline_ch1, bins=numBins, range=binRange, color='blue', density=True,lw=3, histtype='step', label='run01018')
    plt.hist(df_305_4.query(f'qdc_lg_ch1>{range_2MeV_IN[0]} and qdc_lg_ch1<{range_2MeV_IN[1]}').baseline_ch1, bins=numBins, range=binRange, color='orange', density=True, lw=3, histtype='step', label='run01019')
    plt.hist(df_305_5.query(f'qdc_lg_ch1>{range_2MeV_OUT[0]} and qdc_lg_ch1<{range_2MeV_OUT[1]}').baseline_ch1, bins=numBins, range=binRange, color='blue', density=True,lw=3, histtype='step', label='run01020')
    plt.hist(df_305_6.query(f'qdc_lg_ch1>{range_2MeV_IN[0]} and qdc_lg_ch1<{range_2MeV_IN[1]}').baseline_ch1, bins=numBins, range=binRange, color='orange', density=True, lw=3, histtype='step', label='run01021')
    plt.hist(df_305_7.query(f'qdc_lg_ch1>{range_2MeV_OUT[0]} and qdc_lg_ch1<{range_2MeV_OUT[1]}').baseline_ch1, bins=numBins, range=binRange, color='blue', density=True,  lw=3, histtype='step', label='run01022')
    plt.hist(df_305_8.query(f'qdc_lg_ch1>{range_2MeV_IN[0]} and qdc_lg_ch1<{range_2MeV_IN[1]}').baseline_ch1, bins=numBins, range=binRange, color='orange', density=True,  lw=3, histtype='step', label='run01023')
    plt.hist(df_305_9.query(f'qdc_lg_ch1>{range_2MeV_OUT[0]} and qdc_lg_ch1<{range_2MeV_OUT[1]}').baseline_ch1, bins=numBins, range=binRange, color='blue', density=True,  lw=3, histtype='step', label='run01024')
    plt.hist(df_305_10.query(f'qdc_lg_ch1>{range_2MeV_IN[0]} and qdc_lg_ch1<{range_2MeV_IN[1]}').baseline_ch1, bins=numBins, range=binRange, color='orange', density=True,  lw=3, histtype='step', label='run01025')
    plt.hist(df_305_11.query(f'qdc_lg_ch1>{range_2MeV_OUT[0]} and qdc_lg_ch1<{range_2MeV_OUT[1]}').baseline_ch1, bins=numBins, range=binRange, color='blue', density=True,  lw=3, histtype='step', label='run01026')
    plt.hist(df_305_12.query(f'qdc_lg_ch1>{range_2MeV_IN[0]} and qdc_lg_ch1<{range_2MeV_IN[1]}').baseline_ch1, bins=numBins, range=binRange, color='orange', density=True, lw=3, histtype='step', label='run01027')
    plt.ylabel('Counts')
    plt.xlabel('Amplitude [mV]')
    plt.legend()
    plt.subplot(2,1,2)
    plt.title('EJ-305, baseline @ 4.4 MeV')
    plt.hist(df_305_1.query(f'qdc_lg_ch1>{range_4MeV_OUT[0]} and qdc_lg_ch1<{range_4MeV_OUT[1]}').baseline_ch1, bins=numBins, range=binRange, color='blue', density=True, lw=3, histtype='step', label='run01016')
    plt.hist(df_305_2.query(f'qdc_lg_ch1>{range_4MeV_IN[0]} and qdc_lg_ch1<{range_4MeV_IN[1]}').baseline_ch1, bins=numBins, range=binRange, color='orange', density=True,  lw=3, histtype='step', label='run01017')
    plt.hist(df_305_3.query(f'qdc_lg_ch1>{range_4MeV_OUT[0]} and qdc_lg_ch1<{range_4MeV_OUT[1]}').baseline_ch1, bins=numBins, range=binRange, color='blue', density=True, lw=3, histtype='step', label='run01018')
    plt.hist(df_305_4.query(f'qdc_lg_ch1>{range_4MeV_IN[0]} and qdc_lg_ch1<{range_4MeV_IN[1]}').baseline_ch1, bins=numBins, range=binRange, color='orange', density=True,  lw=3, histtype='step', label='run01019')
    plt.hist(df_305_5.query(f'qdc_lg_ch1>{range_4MeV_OUT[0]} and qdc_lg_ch1<{range_4MeV_OUT[1]}').baseline_ch1, bins=numBins, range=binRange, color='blue', density=True, lw=3, histtype='step', label='run01020')
    plt.hist(df_305_6.query(f'qdc_lg_ch1>{range_4MeV_IN[0]} and qdc_lg_ch1<{range_4MeV_IN[1]}').baseline_ch1, bins=numBins, range=binRange, color='orange', density=True, lw=3, histtype='step', label='run01021')
    plt.hist(df_305_7.query(f'qdc_lg_ch1>{range_4MeV_OUT[0]} and qdc_lg_ch1<{range_4MeV_OUT[1]}').baseline_ch1, bins=numBins, range=binRange, color='blue', density=True,  lw=3, histtype='step', label='run01022')
    plt.hist(df_305_8.query(f'qdc_lg_ch1>{range_4MeV_IN[0]} and qdc_lg_ch1<{range_4MeV_IN[1]}').baseline_ch1, bins=numBins, range=binRange, color='orange', density=True,  lw=3, histtype='step', label='run01023')
    plt.hist(df_305_9.query(f'qdc_lg_ch1>{range_4MeV_OUT[0]} and qdc_lg_ch1<{range_4MeV_OUT[1]}').baseline_ch1, bins=numBins, range=binRange, color='blue', density=True,  lw=3, histtype='step', label='run01024')
    plt.hist(df_305_10.query(f'qdc_lg_ch1>{range_4MeV_IN[0]} and qdc_lg_ch1<{range_4MeV_IN[1]}').baseline_ch1, bins=numBins, range=binRange, color='orange', density=True, lw=3, histtype='step', label='run01025')
    plt.hist(df_305_11.query(f'qdc_lg_ch1>{range_4MeV_OUT[0]} and qdc_lg_ch1<{range_4MeV_OUT[1]}').baseline_ch1, bins=numBins, range=binRange, color='blue', density=True, lw=3, histtype='step', label='run01026')
    plt.hist(df_305_12.query(f'qdc_lg_ch1>{range_4MeV_IN[0]} and qdc_lg_ch1<{range_4MeV_IN[1]}').baseline_ch1, bins=numBins, range=binRange, color='orange', density=True,  lw=3, histtype='step', label='run01027')
    plt.ylabel('Counts')
    plt.xlabel('Amplitude [mV]')
    plt.legend()

    plt.show()

def EJ305_pulses():
    """
    Plotting pulses around 2.2 MeV and 4.44 MeV from EJ305 detector with CAEN attenuator at -20dB
    """
    path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop
    
    #query ranged for the two photon compton edges IN and OUT
    range_2MeV_OUT = [5448,6453]
    range_2MeV_IN = [6137,7142] 
    range_4MeV_OUT = [11189,12435]
    range_4MeV_IN = [11754,13000] 
    
    #loading data
    # num_events = 16500 #number of events to include

    df_305_1 = dd.read_parquet(path+'run01016', columns=['samples_ch1','qdc_lg_ch1', 'baseline_ch1', 'CFD_rise_ch1'], engine='pyarrow')#out
    df_305_2 = dd.read_parquet(path+'run01017', columns=['samples_ch1','qdc_lg_ch1', 'baseline_ch1', 'CFD_rise_ch1'], engine='pyarrow')#in

    df_305_OUT_2MeV = df_305_1.partitions[10:12].sample(frac=.4).query(f'qdc_lg_ch1>{range_2MeV_OUT[0]} and qdc_lg_ch1<{range_2MeV_OUT[1]}').compute()
    df_305_IN_2MeV = df_305_2.partitions[10:12].sample(frac=.04).query(f'qdc_lg_ch1>{range_2MeV_IN[0]} and qdc_lg_ch1<{range_2MeV_IN[1]}').compute()
    df_305_OUT_4MeV = df_305_1.partitions[10:12].sample(frac=1).query(f'qdc_lg_ch1>{range_4MeV_OUT[0]} and qdc_lg_ch1<{range_4MeV_OUT[1]}').compute()
    df_305_IN_4MeV = df_305_2.partitions[10:12].sample(frac=.2).query(f'qdc_lg_ch1>{range_4MeV_IN[0]} and qdc_lg_ch1<{range_4MeV_IN[1]}').compute()

    df_305_OUT_2MeV = df_305_OUT_2MeV.reset_index()
    df_305_IN_2MeV = df_305_IN_2MeV.reset_index()
    df_305_OUT_4MeV = df_305_OUT_4MeV.reset_index()
    df_305_IN_4MeV = df_305_IN_4MeV.reset_index()

    print(f'df_305_OUT_2MeV events: {len(df_305_OUT_2MeV)}')
    print(f'df_305_IN_2MeV events: {len(df_305_IN_2MeV)}')
    print(f'df_305_OUT_4MeV events: {len(df_305_OUT_4MeV)}')
    print(f'df_305_IN_4MeV events: {len(df_305_IN_4MeV)}')

    for evt in range(40):
        sample_OUT_2 = df_305_OUT_2MeV.samples_ch1[evt]-df_305_OUT_2MeV.baseline_ch1[evt]
        sample_OUT_2 = sample_OUT_2[(int(df_305_OUT_2MeV.CFD_rise_ch1[evt])-25):-1]
        sample_OUT_2 = sample_OUT_2[0:450]

        sample_IN_2 = df_305_IN_2MeV.samples_ch1[evt]-df_305_IN_2MeV.baseline_ch1[evt]
        sample_IN_2 = sample_IN_2[(int(df_305_IN_2MeV.CFD_rise_ch1[evt])-25):-1]
        sample_IN_2 = sample_IN_2[0:450]

        sample_OUT_4 = df_305_OUT_4MeV.samples_ch1[evt]-df_305_OUT_4MeV.baseline_ch1[evt]
        sample_OUT_4 = sample_OUT_4[(int(df_305_OUT_4MeV.CFD_rise_ch1[evt])-25):-1]
        sample_OUT_4 = sample_OUT_4[0:450]

        sample_IN_4 = df_305_IN_4MeV.samples_ch1[evt]-df_305_IN_4MeV.baseline_ch1[evt]
        sample_IN_4 = sample_IN_4[(int(df_305_IN_4MeV.CFD_rise_ch1[evt])-25):-1]
        sample_IN_4 = sample_IN_4[0:450]



        plt.figure(10, figsize=(10,8))
        plt.subplot(2,1,1)    
        plt.plot(sample_OUT_2, lw=2, color='blue', label=f'Outside')
        plt.plot(sample_IN_2, lw=2, color='orange', label=f'Inside')
        plt.plot(sample_IN_2-sample_OUT_2, lw=2, color='purple', label=f'Inside - Outside')
        plt.subplot(2,1,2)
        plt.plot(sample_OUT_4, lw=2, color='blue', label=f'Outside')
        plt.plot(sample_IN_4, lw=2, color='orange', label=f'Inside')
        plt.plot(sample_IN_4-sample_OUT_4, lw=2, color='purple', label=f'Inside - Outside')
        plt.subplot(2,1,1)    
        plt.title(f'EJ-305, pulses @ 2.2 MeV, evt={evt}')
        plt.ylabel('Amplitude [mV]')
        plt.xlabel('Time [ns]')
        plt.ylim([-700,100])
        plt.xlim([0,350])
        plt.legend()

        plt.subplot(2,1,2)    
        plt.title(f'EJ-305, pulses @ 4.4 MeV, evt={evt}')
        plt.ylabel('Amplitude [mV]')
        plt.xlabel('Time [ns]')
        plt.ylim([-800,100])
        plt.xlim([0,350])
        plt.legend()
        plt.show()


def EJ305_distance():
    """
    Distance measurement using EJ-305 detector and CAEN attenuator at -20dB
    """
    path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop
    
    current = [316.536, 316.548, 316.576, 316.624, 316.668, 316.745, 316.900, 317.116, 317.428, 317.916]
    distance = [1306,1200,1100,1000,900,800,700,600,500,400]
    rate = []

    df_305_1 = pd.read_parquet(path+'run01028', columns=['qdc_lg_ch1', 'baseline_ch1', 'CFD_rise_ch1'], engine='pyarrow')
    df_305_2 = pd.read_parquet(path+'run01029', columns=['qdc_lg_ch1', 'baseline_ch1', 'CFD_rise_ch1'], engine='pyarrow')
    df_305_3 = pd.read_parquet(path+'run01030', columns=['qdc_lg_ch1', 'baseline_ch1', 'CFD_rise_ch1'], engine='pyarrow')
    df_305_4 = pd.read_parquet(path+'run01031', columns=['qdc_lg_ch1', 'baseline_ch1', 'CFD_rise_ch1'], engine='pyarrow')
    df_305_5 = pd.read_parquet(path+'run01032', columns=['qdc_lg_ch1', 'baseline_ch1', 'CFD_rise_ch1'], engine='pyarrow')
    df_305_6 = pd.read_parquet(path+'run01033', columns=['qdc_lg_ch1', 'baseline_ch1', 'CFD_rise_ch1'], engine='pyarrow')
    df_305_7 = pd.read_parquet(path+'run01034', columns=['qdc_lg_ch1', 'baseline_ch1', 'CFD_rise_ch1'], engine='pyarrow')
    df_305_8 = pd.read_parquet(path+'run01035', columns=['qdc_lg_ch1', 'baseline_ch1', 'CFD_rise_ch1'], engine='pyarrow')
    df_305_9 = pd.read_parquet(path+'run01036', columns=['qdc_lg_ch1', 'baseline_ch1', 'CFD_rise_ch1'], engine='pyarrow')
    df_305_10 = pd.read_parquet(path+'run01037', columns=['qdc_lg_ch1', 'baseline_ch1', 'CFD_rise_ch1'], engine='pyarrow')
    
    rate.append(len(df_305_1)/120)
    rate.append(len(df_305_2)/120)
    rate.append(len(df_305_3)/120)
    rate.append(len(df_305_4)/120)
    rate.append(len(df_305_5)/120)
    rate.append(len(df_305_6)/120)
    rate.append(len(df_305_7)/120)
    rate.append(len(df_305_8)/120)
    rate.append(len(df_305_9)/120)
    rate.append(len(df_305_10)/120)

    plt.figure(10, figsize=(10,8))
    plt.plot(distance, rate, lw=2, color='blue', label=f'Rate')
    plt.legend()
    plt.ylabel('Rate [1/s]')
    plt.xlabel('Distance [mm]')

    plt.figure(11, figsize=(10,8))
    plt.plot(distance, current, lw=2, color='blue', label=f'Current')
    plt.legend()
    plt.ylabel('Current [uA]')
    plt.xlabel('Distance [mm]')

    plt.figure(12, figsize=(10,8))
    numBins = 200
    binRange = [0,17500]
    plt.hist(df_305_1.qdc_lg_ch1, lw=2, histtype='step', bins=numBins, range=binRange, weights=timeNorm(df_305_1.qdc_lg_ch1, 120), label=f'run1028 (1306 mm)')
    plt.hist(df_305_2.qdc_lg_ch1, lw=2, histtype='step', bins=numBins, range=binRange, weights=timeNorm(df_305_2.qdc_lg_ch1, 120), label=f'run1029 (1200 mm)')
    plt.hist(df_305_3.qdc_lg_ch1, lw=2, histtype='step', bins=numBins, range=binRange, weights=timeNorm(df_305_3.qdc_lg_ch1, 120), label=f'run1030 (1100 mm)')
    plt.hist(df_305_4.qdc_lg_ch1, lw=2, histtype='step', bins=numBins, range=binRange, weights=timeNorm(df_305_4.qdc_lg_ch1, 120), label=f'run1031 (1000 mm)')
    plt.hist(df_305_5.qdc_lg_ch1, lw=2, histtype='step', bins=numBins, range=binRange, weights=timeNorm(df_305_5.qdc_lg_ch1, 120), label=f'run1032 (900 mm)')
    plt.hist(df_305_6.qdc_lg_ch1, lw=2, histtype='step', bins=numBins, range=binRange, weights=timeNorm(df_305_6.qdc_lg_ch1, 120), label=f'run1033 (800 mm)')
    plt.hist(df_305_7.qdc_lg_ch1, lw=2, histtype='step', bins=numBins, range=binRange, weights=timeNorm(df_305_7.qdc_lg_ch1, 120), label=f'run1034 (700 mm)')
    plt.hist(df_305_8.qdc_lg_ch1, lw=2, histtype='step', bins=numBins, range=binRange, weights=timeNorm(df_305_8.qdc_lg_ch1, 120), label=f'run1035 (600 mm)')
    plt.hist(df_305_9.qdc_lg_ch1, lw=2, histtype='step', bins=numBins, range=binRange, weights=timeNorm(df_305_9.qdc_lg_ch1, 120), label=f'run1036 (500 mm)')
    plt.hist(df_305_10.qdc_lg_ch1, lw=2, histtype='step', bins=numBins, range=binRange, weights=timeNorm(df_305_10.qdc_lg_ch1, 120), label=f'run1037 (400 mm)')

    plt.legend()
    plt.title('Distance to source, EJ-305 @ -20dB')
    plt.ylabel('Rate [1/s]')
    plt.xlabel('QDC [arb. units]')
    plt.yscale('log')
    plt.show()

