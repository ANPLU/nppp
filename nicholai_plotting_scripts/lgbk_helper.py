#!/usr/bin/env python3
#http://patorjk.com/software/taag/#p=display&f=ANSI%20Shadow
# -------------------------------------------------------------------------------------
# ██╗      ██████╗ ██████╗ ██╗  ██╗    ██╗  ██╗███████╗██╗     ██████╗ ███████╗██████╗ 
# ██║     ██╔════╝ ██╔══██╗██║ ██╔╝    ██║  ██║██╔════╝██║     ██╔══██╗██╔════╝██╔══██╗
# ██║     ██║  ███╗██████╔╝█████╔╝     ███████║█████╗  ██║     ██████╔╝█████╗  ██████╔╝
# ██║     ██║   ██║██╔══██╗██╔═██╗     ██╔══██║██╔══╝  ██║     ██╔═══╝ ██╔══╝  ██╔══██╗
# ███████╗╚██████╔╝██████╔╝██║  ██╗    ██║  ██║███████╗███████╗██║     ███████╗██║  ██║
# ╚══════╝ ╚═════╝ ╚═════╝ ╚═╝  ╚═╝    ╚═╝  ╚═╝╚══════╝╚══════╝╚═╝     ╚══════╝╚═╝  ╚═╝
#                 Methods for adding meta data to a log book.
#       
#   Author: Nicholai Mauritzson 2019
#           nicholai.mauritzson@nuclear.lu.se                                                                                      
# -------------------------------------------------------------------------------------

import sys
import pandas as pd
import numpy as np
import csv
import matplotlib.pyplot as plt
sys.path.insert(0, "../library/") #Import my own libraries
import nicholai_math as nm
import nicholai_utility as nu
sys.path.insert(0, "../") #Import other local libraries
import digviz as dv


def main():
    """
    --------------------------------------------------------------
    Logging format for lgbk_main.csv
   
    A helper script for keeping a log-book in with metadata from different runs. 

    To use: Manually edit variable below with relevant information and run 'lgbk_helper.py' script.
    
    --------------------------------------------------------------
    Nicholai Mauritzson
    2019-07-24
    
    """
    # ------ Manually fill below ------
    runNum          = 32          #run number of data file.
    date            = "2019-07-30"  #Date for run
    source          = "PuBe"        #what type of source was used during run
    distance        = 410           #Distance between source and detector (mm)
    detector        = 'NE213'       #Name of detector. Used to locate filepath to data.
    ch0             = None          #What is connected to ch0? Else: None
    ch1             = 'NE213'          #What is connected to ch1? Else: None
    ch2             = 'YAP_front'          #What is connected to ch2? Else: None
    ch3             = 'YAP_back'          #What is connected to ch3? Else: None
    ch4             = 'YAP_left'          #What is connected to ch4? Else: None
    ch5             = 'YAP_right'          #What is connected to ch5? Else: None
    ch6             = None          #What is connected to ch6? Else: None
    ch7             = None          #What is connected to ch6? Else: None
    ch0_bias        = None          #What is the bias set to detector in ch0? Else: None
    ch1_bias        = -1850          #What is the bias set to detector in ch1? Else: None
    ch2_bias        = -715          #What is the bias set to detector in ch2? Else: None
    ch3_bias        = -755          #What is the bias set to detector in ch3? Else: None
    ch4_bias        = -820          #What is the bias set to detector in ch4? Else: None
    ch5_bias        = -708          #What is the bias set to detector in ch5? Else: None
    ch6_bias        = None          #What is the bias set to detector in ch6? Else: None
    ch7_bias        = None          #What is the bias set to detector in ch7? Else: None
    ch0_thr         = None          #What is the threshold (digitizer) set to for ch0? Else: None
    ch1_thr         = 985          #What is the threshold (digitizer) set to for ch1? Else: None
    ch2_thr         = 'Default'          #What is the threshold (digitizer) set to for ch2? Else: None
    ch3_thr         = 'Default'          #What is the threshold (digitizer) set to for ch3? Else: None
    ch4_thr         = 'Default'          #What is the threshold (digitizer) set to for ch4? Else: None
    ch5_thr         = 'Default'          #What is the threshold (digitizer) set to for ch5? Else: None
    ch6_thr         = None          #What is the threshold (digitizer) set to for ch6? Else: None
    ch7_thr         = None          #What is the threshold (digitizer) set to for ch7? Else: None
    recordLength    = 1000          #Number of samples in the acquisition window
    postTrigger     = '80%'            #fraction of acq. window after trigger [0--100%]
    beamport        = 0             #What beamport on the aquarium was used (0-3)?
    collimated      = False          #Is the source collimated? True/False
    shielding       = True          #False/True dependent on if shelding around the detector was used or not
    comments        = ""            #Any additional comment

    # --------- Get information from data file ---------
    dataPath = f"/media/gheed/Seagate_Expansion_Drive1/data/TNT/digital/{detector}_cup/jadaq-{runNum:05}.h5" #Location of all relevant data files
    num_events = len(dv.load_h5(f'{dataPath}'))
    # num_events, rate = nu.logFileStripper(dataPath, runNum) #Get metadata from .log file when using the NE213 detctors.

    # ------- COMPILE ALL VARIABLES AND SAVE TO LOG BOOK ---------
    #-------- Save row entry to append to metadata file. ---------
    row = [runNum, 
    date, 
    num_events,
    source, 
    distance, 
    ch0, 
    ch1, 
    ch2, 
    ch3, 
    ch4, 
    ch5, 
    ch6, 
    ch7,
    ch0_bias, 
    ch1_bias, 
    ch2_bias, 
    ch3_bias, 
    ch4_bias, 
    ch5_bias, 
    ch6_bias, 
    ch7_bias,
    ch0_thr, 
    ch1_thr, 
    ch2_thr, 
    ch3_thr, 
    ch4_thr, 
    ch5_thr, 
    ch6_thr, 
    ch7_thr,
    recordLength,
    postTrigger,
    shielding, 
    collimated, 
    beamport, 
    comments]

    # ------- Append row to .csv file --------
    logFile = "/home/gheed/Documents/projects/TNT/analysis/code_repo/glu_digital_psd_analysis/nicholai_plotting_scripts/lgbk/lgbk_main.csv" #Location of log file to write to.
    with open(f"{logFile}", "a") as csvFile: #Open and append log file.
        writer = csv.writer(csvFile)
        writer.writerow(row)
    csvFile.close()
    print(f'LOG COMPLETE!')


if __name__ == "__main__":
    main()