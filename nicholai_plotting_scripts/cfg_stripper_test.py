#!/usr/bin/env python3

import sys
sys.path.insert(0, "../library/") #Import my own libraries
import nicholai_math as nm
import nicholai_utility as nu
import pandas as pd
import numpy as np
import csv
import matplotlib.pyplot as plt

runNum = 2
detector = 'NE213'


cfgPath = f'/media/gheed/Seagate_Expansion_Drive1/data/TNT/digital/{detector}_cup/jadaq-{runNum:05}.cfg'
    # dataPath = f"/media/gheed/Seagate_Expansion_Drive1/data/TNT/digital/{detector}_cup/" #Location of all relevant data files
    # num_events, start_time, stop_time, real_time, cpu_time, rate = nu.logFileStripper(dataPath, runNum) #Get metadata from .log file when using the NE213 detctors.

#OPEN AND RETRIVE INFORMATION FROM CONFIG FILE .cfg
#Open run config file
f = open(f"{cfgPath}", "r") 

#Start itterating through config file.
for line in f: 
    #Find which channels where enabled from the "ChannelEnableMask" variable.
    #If channel was enabled it is up to the user to define which detector was conncted to that channel.
    if 'ChannelEnableMask' in line: 
        #Check if line was commented out in config file.
        if line[0]=='#':
            break
        temp = line.split('=')[1].rstrip() #Split on '='
        if temp[7]=='0':  #ch 0
            ch0 = None #If channel was not enabled!
        if temp[6]=='0':  #ch 1
            ch1 = None #If channel was not enabled!
        if temp[5]=='0':  #ch 2
            ch2 = None #If channel was not enabled!
        if temp[4]=='0':  #ch 3
            ch3 = None #If channel was not enabled!
        if temp[3]=='0':  #ch 4
            ch4 = None #If channel was not enabled!
        if temp[2]=='0':  #ch 5
            ch5 = None #If channel was not enabled!
        if temp[1]=='0':  #ch 6
            ch6 = None #If channel was not enabled!
        if temp[0]=='0':  #ch 7
            ch7 = None #If channel was not enabled!
    
    #Check if double sampling was enabled (DESMode=True/False)
    if 'DESMode' in line:
        #Check if line was commented out in config file.
        if line[0]!='#':
            if line.split('=')[1].rstrip()[0]=='0':
                DESMode = False #Set False if disabled.
            if line.split('=')[1].rstrip()[0]=='1':
                DESMode = True #Set True if enabled.

    if 'ChannelDCOffset' in line:
        #Check if line was commented out in config file.
        if line[0]!='#':
            ch_temp = line.split('=')[0][-2]
            offset_temp = line.split('=')[1].rstrip()
            if ch_temp == '0':
                ch0_DCOffset = int(offset_temp)
            if ch_temp == '1':
                ch1_DCOffset = int(offset_temp)
            if ch_temp == '2':
                ch2_DCOffset = int(offset_temp)
            if ch_temp == '3':
                ch3_DCOffset = int(offset_temp)
            if ch_temp == '4':
                ch4_DCOffset = int(offset_temp)
            if ch_temp == '5':
                ch5_DCOffset = int(offset_temp)
            if ch_temp == '6':
                ch6_DCOffset = int(offset_temp)
            if ch_temp == '7':
                ch7_DCOffset = int(offset_temp)

    if 'ChannelSelfTrigger' in line:
        #Check if line was commented out in config file.
        if line[0]!='#':
            ch_temp = line.split('=')[0].rstrip()[-2]
            trig_temp = line.split('=')[1].rstrip()[0]
            if trig_temp == '0':
                trig_temp = 'disabled'
            elif trig_temp == '1':
                trig_temp = 'acq only'
            elif trig_temp == '2':
                trig_temp = 'extout only'
            elif trig_temp == '3':
                trig_temp = 'acq and extout'

            if ch_temp == '0':
                ch0_trig = trig_temp
            if ch_temp == '1':
                ch1_trig = trig_temp
            if ch_temp == '2':
                ch2_trig = trig_temp
            if ch_temp == '3':
                ch3_trig = trig_temp
            if ch_temp == '4':
                ch4_trig = trig_temp
            if ch_temp == '5':
                ch5_trig = trig_temp
            if ch_temp == '6':
                ch6_trig = trig_temp
            if ch_temp == '7':
                ch7_trig = trig_temp
    if 'ChannelTriggerThreshold' in line:
        if line[0]!='#':
            # break
            ch_temp = line.split('=')[0].rstrip()[-2]
            thr_temp = line.split('=')[1].split(' ')[0]
            if ch_temp == '0':
                ch0_thr = int(thr_temp)
            if ch_temp == '1':
                ch1_thr = int(thr_temp)
            if ch_temp == '2':
                ch2_thr = int(thr_temp)
            if ch_temp == '3':
                ch3_thr = int(thr_temp)
            if ch_temp == '4':
                ch4_thr = int(thr_temp)
            if ch_temp == '5':
                ch5_thr = int(thr_temp)
            if ch_temp == '6':
                ch6_thr = int(thr_temp)
            if ch_temp == '7':
                ch7_thr = int(thr_temp)
    
    if 'RecordLength' in line:
        if line[0]!='#':
            record_length = line.split('=')[1].split(' ')[0] 
            
    #TODO: channel polarity!

f.close()


    # if "processed" in line: #Find the number of events of run.
    #     num_events = int(line.split(" ")[6]) #Split line and get number of events as an integer.
    # if "RealTime" in line: #Get real time and cpu time as well as stop time of run.
    #     stop_time = line[1:9] #Saveing stopping time.
    #     split_1 = line.split("=") #Splitting on "=" sign
    #     real_time = float(split_1[1].split(" ")[0]) #Splittin on space and saving variable as float.
    #     cpu_time = float(split_1[-1].split(" ")[0]) #Splittin on space and saving variable as float.
    #     
    #     break





