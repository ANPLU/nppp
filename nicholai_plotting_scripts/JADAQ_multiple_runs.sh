#!/bin/bash

echo 'Running JADAQ for 10 min x '$2' times ...'

for ((i=1;i<=$2;i++)); do
    $HOME/src/jadaq/build/jadaq --stats 5 -t 600 -H --config_out conf.out $1
done
