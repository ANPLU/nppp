
# plt.figure(0)
# plt.hist(df.query(f'ch==1').QDC_LG, bins=500, label='QDC_LG 500 ns', histtype='step')
# plt.hist(df.query(f'ch==1').QDC_SG, bins=500, label='QDC_SG 60 ns', histtype='step')
# plt.legend()
# plt.grid()
# plt.yscale('log')

# plt.figure(1)
# plt.hexbin(df.query('ch==1').QDC_LG, df.query('ch==1').PS, extent=(0, 10000, -1, 1), norm=LogNorm())
# plt.colorbar()
# plt.show()

# plt.figure(0) #make histogram of FOM, a and b values.
# plt.hist2d(FOM[1], FOM[2], weights=FOM[0], bins=20)
# plt.plot(FOM[1][np.argmax(FOM[0])], FOM[2][np.argmax(FOM[0])], 'ko', label='Maximum FOM point')
# plt.xlabel('a')
# plt.xlabel('b')
# plt.title('PS FoM')
# plt.colorbar()

# plt.figure(1) #make histogram with most optimal PS parameters.
# plt.hist(PSDcharge(df1.query('ch==1'), LG='QDC_LG', SG='QDC_SG', a=FOM[1][np.argmax(FOM[0])], b=FOM[2][np.argmax(FOM[0])]), bins=200)
# plt.title(f'Best: FoM={round(max(FOM[0]),2)}, a={round(FOM[1][np.argmax(FOM[0])])}, b={round(FOM[2][np.argmax(FOM[0])])}')

# plt.figure(2) #make histogram with lest optimal PS parameters.
# plt.hist(PSDcharge(df1.query('ch==1'), LG='QDC_LG', SG='QDC_SG', a=FOM[1][np.argmin(FOM[0])], b=FOM[2][np.argmin(FOM[0])]), bins=200)
# plt.title(f'Worst: FoM={round(min(FOM[0]),2)}, a={round(FOM[1][np.argmin(FOM[0])])}, b={round(FOM[2][np.argmin(FOM[0])])}')

# plt.figure(3) #make 2D histogram with most optimal PS parameters.
# plt.hexbin(df1.query('ch==1').QDC_LG, PSDcharge(df1.query('ch==1'), LG='QDC_LG', SG='QDC_SG', a=FOM[1][np.argmax(FOM[0])], b=FOM[2][np.argmax(FOM[0])]), extent=(0, 25000, -1, 1), norm=LogNorm())
# plt.title(f'Best: FoM={round(max(FOM[0]),2)}, a={round(FOM[1][np.argmax(FOM[0])])}, b={round(FOM[2][np.argmax(FOM[0])])}')
# plt.colorbar()

# plt.figure(4) #make 2D histogram with least optimal PS parameters.
# plt.hexbin(df1.query('ch==1').QDC_LG, PSDcharge(df1.query('ch==1'), LG='QDC_LG', SG='QDC_SG', a=FOM[1][np.argmin(FOM[0])], b=FOM[2][np.argmin(FOM[0])]), extent=(0, 25000, -1, 1), norm=LogNorm())
# plt.title(f'Worst: FoM={round(min(FOM[0]),2)}, a={round(FOM[1][np.argmin(FOM[0])])}, b={round(FOM[2][np.argmin(FOM[0])])}')
# plt.colorbar()

# plt.show()
# plt.grid()
# plt.legend()
# plt.show()