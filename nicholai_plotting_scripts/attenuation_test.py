#!/usr/bin/env python3
import sys
from os import listdir
import pandas as pd
import numpy as np
sys.path.insert(0, "../library/") #Import my own libraries
import dask.dataframe as dd
import processing_math as promath
import processing_utility as prout
import processing_pd as propd
import processing_data as prodata
import logging
import matplotlib.pyplot as plt
sys.path.insert(0, "../")
import digviz as dv
import tof

df1 = propd.load_parquet('/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/run00122/', ('samples_ch1','samples_ch2','samples_ch3','samples_ch4','samples_ch5')) 
df2 = propd.load_parquet('/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/run00108/', ('samples_ch1','samples_ch2','samples_ch3','samples_ch4','samples_ch5')) 

label1 = 'run 122, bias: -2080 V - 10 dB attenuation, thr: 1000'
label2 = 'run 108, bias: -1875 V, thr: 1000'



plt.figure(0)
plt.hist(df1.amplitude_ch1*1000/1024, bins=100, range=(0,1000), histtype='step', label=label1, linewidth=2)
plt.hist(df2.amplitude_ch1*1000/1024, bins=100, range=(0,1000), histtype='step', label=label2, linewidth=2)
plt.yscale('log')
plt.xlabel('PH [mV]')
plt.ylabel('counts')
plt.legend()
plt.grid(which='both')



flashrange = {'tof_ch2':[40,60], 'tof_ch3':[40,60], 'tof_ch4':[40,60], 'tof_ch5':[40,60]}
prodata.tof_calibration(df1, flashrange, 1.306, calUnit=1e-9, energyCal=False)
prodata.tof_calibration(df2, flashrange, 1.306, calUnit=1e-9, energyCal=False)

y_bins = 100
y_range = [0,100]
n_thr = 10
y_thr = 100

plt.figure(1)
plt.subplot(2,2,1)
plt.hist(df1.query(f'amplitude_ch1>{n_thr} and amplitude_ch2>{y_thr}').tof_ch2*1e9, range=(y_range[0],y_range[1]), bins=y_bins, histtype='step', label=label1, linewidth=2)
plt.hist(df2.query(f'amplitude_ch1>{n_thr} and amplitude_ch2>{y_thr}').tof_ch2*1e9, range=(y_range[0],y_range[1]), bins=y_bins, histtype='step', label=label2, linewidth=2)
# plt.yscale('log')
plt.ylim([0,350])
plt.ylabel('counts')
plt.xlabel('TOF [ns]')
plt.title('TOF, YAP_front (ch2)')
plt.legend()
plt.grid(which='both')
plt.subplot(2,2,2)
plt.hist(df1.query(f'amplitude_ch1>{n_thr} and amplitude_ch3>{y_thr}').tof_ch3*1e9, range=(y_range[0],y_range[1]), bins=y_bins, histtype='step', label=label1, linewidth=2)
plt.hist(df2.query(f'amplitude_ch1>{n_thr} and amplitude_ch3>{y_thr}').tof_ch3*1e9, range=(y_range[0],y_range[1]), bins=y_bins, histtype='step', label=label2, linewidth=2)
# plt.yscale('log')
plt.ylim([0,350])
plt.ylabel('counts')
plt.xlabel('TOF [ns]')
plt.title('TOF, YAP_back (ch3)')
plt.legend()
plt.grid(which='both')
plt.subplot(2,2,3)
plt.hist(df1.query(f'amplitude_ch1>{n_thr} and amplitude_ch4>{y_thr}').tof_ch4*1e9, range=(y_range[0],y_range[1]), bins=y_bins, histtype='step', label=label1, linewidth=2)
plt.hist(df2.query(f'amplitude_ch1>{n_thr} and amplitude_ch4>{y_thr}').tof_ch4*1e9, range=(y_range[0],y_range[1]), bins=y_bins, histtype='step', label=label2, linewidth=2)
# plt.yscale('log')
plt.ylim([0,350])
plt.ylabel('counts')
plt.xlabel('TOF [ns]')
plt.title('TOF, YAP_left (ch4)')
plt.legend()
plt.grid(which='both')
plt.subplot(2,2,4)
plt.hist(df1.query(f'amplitude_ch1>{n_thr} and amplitude_ch5>{y_thr}').tof_ch5*1e9, range=(y_range[0],y_range[1]), bins=y_bins, histtype='step', label=label1, linewidth=2)
plt.hist(df2.query(f'amplitude_ch1>{n_thr} and amplitude_ch5>{y_thr}').tof_ch5*1e9, range=(y_range[0],y_range[1]), bins=y_bins, histtype='step', label=label2, linewidth=2)
# plt.yscale('log')
plt.ylim([0,350])
plt.ylabel('counts')
plt.xlabel('TOF [ns]')
plt.title('TOF, YAP_right (ch5)')
plt.legend()
plt.grid(which='both')


plt.figure(3)
plt.hist(df1.qdc_lg_ch1, range=(0,20000), bins=200, histtype='step', label=label1, linewidth=2)
plt.hist(df2.qdc_lg_ch1, range=(0,20000), bins=200, histtype='step', label=label2, linewidth=2)
plt.yscale('log')
plt.ylabel('counts')
plt.xlabel('QDC [arb. units.]')
plt.legend()
plt.grid(which='both')

plt.show()