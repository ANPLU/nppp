import sys
# import pandas as pd
# import numpy as np
# import matplotlib.pyplot as plt
# import dask.dataframe as dd
# import dask.array as da
# import random
# from math import cos, pi, radians, factorial
# from scipy import signal
# from scipy.stats import chisquare
# from scipy.integrate import quad
# from scipy.signal import find_peaks
# from tqdm import tqdm
# from scipy.optimize import curve_fit
# from matplotlib.colors import LogNorm
# from dask.diagnostics import ProgressBar
# from scipy.interpolate import UnivariateSpline, interp1d, splrep
# # import seaborn as sns

# import gaussEdgeFit_parametersEJ321P as gefEJ321P #Library with fit parameters for compton edges.
# import gaussEdgeFit_parametersNE213 as gefNE213 #Library with fit parameters for compton edges.

# # IMPORT FROM LOCAL LIBRARY
sys.path.insert(0, "../library/") #Import my own libraries
import processing_math as promath
# import processing_utility as prout
# import processing_pd as propd
# import processing_data as prodata
# import nicholai_plot_helper as nplt
# import nicholai_exclusive as ne
# import SignalGenerator as sg

# data = np.load('Na22_data.npy')

# numBins = 100
# binRange = (2860, 3437)#(2500, 4500)
# val, bins = np.histogram(data, bins=numBins, range=binRange)
# bins = prodata.getBinCenters(bins)
# val_err = np.sqrt(val)

# plt.fill_between(bins, val-val_err, val+val_err, color='green', alpha=.4)
# plt.plot(bins, val, 'b', label='Data')

# #param guess:
# const = 1500
# mean = 3000
# std = 500

# #curve_fit no error
# abs=False
# cf_val, cf_err = curve_fit(promath.gaussFunc, bins, val, (const, mean, std))
# cf_err = np.sqrt(np.diag(cf_err))
# cf_fit = promath.gaussFunc(bins, cf_val[0], cf_val[1], cf_val[2])

# plt.plot(bins, promath.gaussFunc(bins, cf_val[0], cf_val[1], cf_val[2]), lw=2, label='curve_fit: Gauss, no err')

# print(f'curve_fit:, abs={abs}')
# print(f'const = {round(cf_val[0],4)} +/- {round(cf_err[0],4)}')
# print(f'mean = {round(cf_val[1],4)} +/- {round(cf_err[1],4)}')
# print(f'std = {round(cf_val[2],4)} +/- {round(cf_err[2],4)}')
# print(f'reduced Chi2 = {round(promath.chi2red(val, cf_fit, val_err, 3),4)}')
# print('-------------------------------------')

# #curve_fit yval_err
# abs = True
# cf_val, cf_err = curve_fit(promath.gaussFunc, bins, val, (const, mean, std), sigma=val_err, absolute_sigma=abs)
# cf_err = np.sqrt(np.diag(cf_err))
# cf_fit = promath.gaussFunc(bins, cf_val[0], cf_val[1], cf_val[2])

# plt.plot(bins, promath.gaussFunc(bins, cf_val[0], cf_val[1], cf_val[2]), lw=2, label='curve_fit: Gauss, err')

# print(f'curve_fit: err, abs={abs}')
# print(f'const = {round(cf_val[0],4)} +/- {round(cf_err[0],4)}')
# print(f'mean = {round(cf_val[1],4)} +/- {round(cf_err[1],4)}')
# print(f'std = {round(cf_val[2],4)} +/- {round(cf_err[2],4)}')
# print(f'reduced Chi2 = {round(promath.chi2red(val, cf_fit, val_err, 3),4)}')
# print('-------------------------------------')

# #curve_fit 1/yval_err
# abs = False
# cf_val, cf_err = curve_fit(promath.gaussFunc, bins, val, (const, mean, std), sigma=1/val_err, absolute_sigma=abs)
# cf_err = np.sqrt(np.diag(cf_err))
# cf_fit = promath.gaussFunc(bins, cf_val[0], cf_val[1], cf_val[2])

# plt.plot(bins, promath.gaussFunc(bins, cf_val[0], cf_val[1], cf_val[2]), lw=4, label='curve_fit: Gauss, 1/err')

# print(f'curve_fit: 1/err, abs={abs}')
# print(f'const = {round(cf_val[0],4)} +/- {round(cf_err[0],4)}')
# print(f'mean = {round(cf_val[1],4)} +/- {round(cf_err[1],4)}')
# print(f'std = {round(cf_val[2],4)} +/- {round(cf_err[2],4)}')
# print(f'reduced Chi2 = {round(promath.chi2red(val, cf_fit, val_err, 3),4)}')
# print('-------------------------------------')

# #curve_fit 1/yval_err**2
# abs = False
# cf_val, cf_err = curve_fit(promath.gaussFunc, bins, val, (const, mean, std), sigma=1/val_err**2, absolute_sigma=abs)
# cf_err = np.sqrt(np.diag(cf_err))
# cf_fit = promath.gaussFunc(bins, cf_val[0], cf_val[1], cf_val[2])

# plt.plot(bins, promath.gaussFunc(bins, cf_val[0], cf_val[1], cf_val[2]), lw=2, label='curve_fit: Gauss, 1/err**2')

# print(f'curve_fit: 1/err**2, abs={abs}')
# print(f'const = {round(cf_val[0],4)} +/- {round(cf_err[0],4)}')
# print(f'mean = {round(cf_val[1],4)} +/- {round(cf_err[1],4)}')
# print(f'std = {round(cf_val[2],4)} +/- {round(cf_err[2],4)}')
# print(f'reduced Chi2 = {round(promath.chi2red(val, cf_fit, val_err, 3),4)}')
# print('-------------------------------------')


# #curve_fit yval_err/yval
# abs = False
# cf_val, cf_err = curve_fit(promath.gaussFunc, bins, val, (const, mean, std), sigma=val_err/val, absolute_sigma=abs)
# cf_err = np.sqrt(np.diag(cf_err))
# cf_fit = promath.gaussFunc(bins, cf_val[0], cf_val[1], cf_val[2])

# plt.plot(bins, promath.gaussFunc(bins, cf_val[0], cf_val[1], cf_val[2]), lw=2, label='curve_fit: Gauss, err/y')

# print(f'curve_fit: err/y, abs={abs}')
# print(f'const = {round(cf_val[0],4)} +/- {round(cf_err[0],4)}')
# print(f'mean = {round(cf_val[1],4)} +/- {round(cf_err[1],4)}')
# print(f'std = {round(cf_val[2],4)} +/- {round(cf_err[2],4)}')
# print(f'reduced Chi2 = {round(promath.chi2red(val, cf_fit, val_err, 3),4)}')
# print('-------------------------------------')







# plt.legend()
# plt.show()




#:::: EXAMPLE TAKEN FROM: https://stackoverflow.com/questions/31704940/using-the-absolute-sigma-parameter-in-scipy-optimize-curve-fit

import numpy as np
from scipy.optimize import curve_fit
from scipy.stats import norm
import matplotlib.pyplot as plt

# defining a model
def model(x, a, b):
    return a * np.exp(-b * x)

# defining the x vector and the real value of some parameters
x_vector = np.arange(100)
a_real, b_real = 1, 0.05

# some toy data with multiplicative uncertainty
y_vector = model(x_vector, a_real, b_real) * (1 + norm.rvs(scale=0.08, size=100))+.05

# plot Data
plt.plot(x_vector, y_vector, label='Data')

# fit the parameters, equal weighting on all data points
params, cov = curve_fit(model, x_vector, y_vector )
chi2_red = round(promath.chi2red(y_vector, model(x_vector, params[0], params[1]), np.ones(len(y_vector)), 2),5) #calculate chi2/ndf
plt.plot(x_vector, model(x_vector, params[0], params[1]), label=f'sigma=1, abs_sigma=False, chi2={chi2_red}')

print(params)
print(cov)
print('..........')


# fit the parameters, weighting each data point by its inverse value
params, cov = curve_fit(model, x_vector, y_vector, 
                        sigma=1/y_vector, absolute_sigma=False)
chi2_red = round(promath.chi2red(y_vector, model(x_vector, params[0], params[1]), 1/y_vector, 2),5) #calculate chi2/ndf
plt.plot(x_vector, model(x_vector, params[0], params[1]), lw=5, label=f'sigma=1/y_vector, abs_sigma=False, chi2={chi2_red}')

print(params)
print(cov)
print('..........')

# with absolute_sigma=False:
## multiplicative transformations of y_data don't matter
params, cov = curve_fit(model, x_vector, y_vector, 
                        sigma=100/y_vector, absolute_sigma=False)
chi2_red = round(promath.chi2red(y_vector, model(x_vector, params[0], params[1]), 100/y_vector, 2),5) #calculate chi2/ndf
plt.plot(x_vector, model(x_vector, params[0], params[1]), label=f'sigma=100/y_vector, abs_sigma=False, chi2={chi2_red}')

print(params)
print(cov)
print('..........')

# but absolute_sigma=True:
## multiplicative transformations of sigma carry through to pcov
params, cov = curve_fit(model, x_vector, y_vector,
                        sigma=100/y_vector, absolute_sigma=True)
chi2_red = round(promath.chi2red(y_vector, model(x_vector, params[0], params[1]), 100/y_vector, 2),5) #calculate chi2/ndf
plt.plot(x_vector, model(x_vector, params[0], params[1]), '--k', lw=2,label=f'sigma=100/y_vector, abs_sigma=True, chi2={chi2_red}')

print(params)
print(cov)
print('..........')

# wighting by n/N where n is is the counts in the bins and N is the number of total counts in the data.

params, cov = curve_fit(model, x_vector, y_vector,
                        sigma=y_vector/sum(y_vector), absolute_sigma=False)
chi2_red = round(promath.chi2red(y_vector, model(x_vector, params[0], params[1]), y_vector/sum(y_vector), 2),5) #calculate chi2/ndf
plt.plot(x_vector, model(x_vector, params[0], params[1]), linestyle=':', color='yellow', lw=2,label=f'sigma=n/N, abs_sigma=False, chi2={chi2_red}')

print(params)
print(cov)
print('..........')



plt.legend()
plt.show()