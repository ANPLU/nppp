import sys
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import dask.dataframe as dd
import dask.array as da
import random
from math import cos, pi, radians, factorial
from scipy import signal
from scipy.stats import chisquare
from scipy.integrate import quad
from scipy.signal import find_peaks
from tqdm import tqdm
from scipy.optimize import curve_fit
from matplotlib.colors import LogNorm
from dask.diagnostics import ProgressBar
from scipy.interpolate import UnivariateSpline, interp1d, splrep

"""
Script for loading several runs of data and processing with TOF slicer.
Uses a lot of memory so will be run remotely on workstation. File paths have been coded to work with ws-01
"""

# IMPORT FROM LOCAL LIBRARY
sys.path.insert(0, "../library/") #Import my own libraries
import processing_math as promath
import processing_utility as prout
import processing_pd as propd
import processing_data as prodata
import nicholai_plot_helper as nplt
import nicholai_exclusive as ne
import SignalGenerator as sg

# IMPORT FROM MAIN DIRECTORY
sys.path.insert(0, "../")
import processing



# ==================================================
#  !EDIT: select relevant data/detector to process
# ==================================================
#Load calibration data from disk
# E_data, QDC_data, QDC_err = np.load('calibration_EJ321P_lg_qdc_89.npy') #Calibration data for EJ321P detector
E_data, QDC_data, QDC_err = np.load('calibration_EJ321P_lg_qdc_89.npy') #Calibration data for NE213 detector

#::: LIST OF RUN NUMBER FOR EJ321P TOF DATA :::
# runList = np.arange(329,410)
# runList = np.arange(410,500)
# runList = np.arange(500,590)
# runList = np.arange(590,680)
# runList = np.arange(680,710)

#::: LIST OF RUN NUMBER FOR NE213 TOF DATA :::
# runList = np.arange(758,840)
# runList = np.arange(841,898)



# ================================================
#                Load ToF data
# ================================================

def load_data_EJ321(runNum, path):
    #path to data files
    #list of columns to exlude from loading, to save memory space
    exclude_col=['samples_ch1', 'samples_ch2', 'samples_ch3', 'samples_ch4', 'samples_ch5', 'evtno', 'ts', 'digitizer', 
                'baseline_ch1', 'peak_index_ch1',  
                'baseline_ch2', 'peak_index_ch2', 
                'baseline_ch3', 'peak_index_ch3',  
                'baseline_ch4', 'peak_index_ch4',  
                'baseline_ch5', 'peak_index_ch5', 
                'qdc_sg_ch1', 'qdc_ps_ch1',
                'qdc_sg_ch2', 'qdc_ps_ch2', 
                'qdc_sg_ch3', 'qdc_ps_ch3', 
                'qdc_sg_ch4', 'qdc_ps_ch4',
                'qdc_sg_ch5', 'qdc_ps_ch5',
                'global_ts', 
                'CFD_drop_ch1', 'risetime_ch1', 'siglen_ch1',
                'min_val_ch2', 'max_val_ch2', 'CFD_drop_ch2', 'risetime_ch2',
                'siglen_ch2', 'min_val_ch3', 'max_val_ch3', 'CFD_drop_ch3',
                'risetime_ch3', 'siglen_ch3', 'min_val_ch4', 'max_val_ch4',
               'CFD_drop_ch4', 'risetime_ch4', 'siglen_ch4', 'min_val_ch5',
                'max_val_ch5', 'CFD_drop_ch5', 'risetime_ch5', 'siglen_ch5']

    df = propd.load_parquet_merge(path = path,
                                runNum = runNum,
                                exclude_col = exclude_col)
    print('load_data() -> DONE!')
    return df

# ================================================
#                 Import data
# ================================================
# path = '/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/cooked/' #path on Nicholais laptop
path = '/drive2/EJ321P_char/cooked/' #path on ws-01
df = load_data_EJ321(runList, path)

# ================================================
#       ToF align and energy calculation
# ================================================
#give ToF column names and set fitting inverval for gamma flash (interval is given for the uncalibrated ToF spectra).
flashrange = {'tof_ch2':[40,60], 'tof_ch3':[40,60], 'tof_ch4':[40,60], 'tof_ch5':[40,60]} 
#distance to center of "cup" detector + wall thickness in meters
distance = (1.305+(0.0615/2)+0.0032)
#unit of sampling time in seconds (i.e. time-difference between samples)
calUnit = 1e-9 
#run calibration function
prodata.tof_calibration(df, flashrange, distance, calUnit, energyCal = True) 

print('tof_align() -> DONE!')

# ================================================
#                 Calibrate QDCs
# ================================================

#fit QDC data (incl errors) to get QDC->MeVee conversion equation
calFit_val, calFit_err = curve_fit(promath.linearFunc, E_data, QDC_data, sigma=QDC_err, absolute_sigma=True) 
calFit_err = np.sqrt(np.diag(calFit_err)) #calculating the errors

#plot calibration data
# nplt.QDCcalibrationPlot(E_data, QDC_data, QDC_err)

df.qdc_lg_ch1 = promath.linearFuncInv(df.qdc_lg_ch1, calFit_val[0], calFit_val[1])
#TODO: add calibration routine for YAPs

# ================================================
#         Slicing Energy spectrum -> QDC
# ================================================
y_thr = ((110,300),(130,340),(130,340),(130,340)) #set amplitude threshold for YAP detectors
n_thr = 0 #set amplitude threshold for neutron detector
df_sliced = prodata.tof_slicer(df, qdc_col='qdc_lg_ch1', tof_energy_cols=['tofE_ch2', 'tofE_ch3','tofE_ch4','tofE_ch5'], n_thr=n_thr, y_thr=y_thr)


# ================================================
#                     Plots
# ================================================
def plot_all(plot_sliced=False, plot_2D=False, plot_panel=False, plot_QDC_cal=False, plot_ps=False, plot_tof=False, plot_tof_gflash=False, plot_cfd_neutron=False, plot_cfd_YAP=False, plot_eCal=False):
    color = 'inferno'

    if plot_sliced:
        nplt.tof_slicer_plot(df_sliced, scale='MeVee')
    
    if plot_2D:
        plt.figure(1)
        plt.subplot(3,1,1)
        plt.title('YAP ch2 only')
        plt.hist2d(df.query('amplitude_ch2>10').amplitude_ch2, df.query('amplitude_ch2>10').amplitude_ch1, bins=200, range=[[0,400],[0,1000]], norm=LogNorm(), cmap=prout.get_bad_pixel_color(color))
        plt.xlabel('YAP QDC ch2 [arb. units]')
        plt.ylabel('QDC ch1 [arb. units]')
        plt.colorbar()
        plt.subplot(3,1,2)
        plt.hist2d(df.query('amplitude_ch2>10').tof_ch2, df.query('amplitude_ch2>10').amplitude_ch2, bins=100, range=[[0,110*1e-9],[0,500]], norm=LogNorm(), cmap=prout.get_bad_pixel_color(color))
        plt.xlabel('ToF ch2 [s]')
        plt.ylabel('YAP ch2 Pulse-height [mV]')
        plt.colorbar()
        plt.subplot(3,1,3)
        plt.hist2d(df.query('amplitude_ch2>10').tof_ch2, df.query('amplitude_ch2>10').amplitude_ch1, bins=200, range=[[0,100*1e-9],[0,1000]], norm=LogNorm(), cmap=prout.get_bad_pixel_color(color))
        plt.xlabel('ToF ch2 [s]')
        plt.ylabel('QDC ch1 [arb. units]')
        plt.colorbar()

    if plot_panel:
        # Like Julius' panel-plot
        
        plt.figure(2)
        plt.subplot(3,1,1)
        plt.title('YAP ch2 only')
        plt.hist2d(df.tof_ch2, df.amplitude_ch2, bins=100, range=[[0,110*1e-9],[0,500]], norm=LogNorm(), cmap=prout.get_bad_pixel_color(color))
        plt.xlabel('ToF ch2 [ns]')
        plt.ylabel('YAP ch2 Pulse-height [mV]')
        plt.colorbar()

        plt.subplot(3,1,2)
        plt.hist(df.query('amplitude_ch2>200 and amplitude_ch2<300').tof_ch2, bins=200, range=(0,100*1e-9))
        plt.xlabel('ToF ch2 [ns]')
        plt.ylabel('counts')
        
        plt.subplot(3,1,3)
        plt.hist2d(df.query('amplitude_ch2>200 and amplitude_ch2<300').tof_ch2, df.query('amplitude_ch2>200 and amplitude_ch2<300').qdc_lg_ch1, bins=100, range=[[0,110*1e-9],[0,20000]], norm=LogNorm(), cmap=prout.get_bad_pixel_color(color))
        plt.xlabel('ToF ch2 [ns]')
        plt.ylabel('QDC ch1 [arb. units]')
        plt.colorbar()

    if plot_ps:
        plt.figure(3)
        plt.subplot(2,1,1)
        plt.title('Pulse-Shape')
        plt.hist2d(df.query('amplitude_ch2>20').qdc_lg_ch2, df.query('amplitude_ch2>20').qdc_ps_ch2, bins=200, range=[[0,20000],[-2,2]], norm=LogNorm(), cmap = prout.get_bad_pixel_color(color))     
        plt.colorbar()
        plt.ylabel('YAP Pulse-Shape [1]')
        plt.xlabel('YAP QDC [arb. units]')
        plt.subplot(2,1,2)
        plt.hist2d(df.query('amplitude_ch1>20').qdc_lg_ch1, df.query('amplitude_ch1>20').qdc_ps_ch1, bins=200, range=[[0,20000],[-2,2]], norm=LogNorm(), cmap = prout.get_bad_pixel_color(color))     
        plt.ylabel('ch1 Pulse-Shape [1]')
        plt.xlabel('ch1 QDC [arb. units]')
        plt.colorbar()

    if plot_tof:
        plt.figure(4)
        plt.subplot(2,2,1)
        plt.hist(df.query('amplitude_ch2>20').tof_ch2, bins=100, range=(0,100*1e-9), label='YAP ch2')
        plt.legend()
        plt.xlabel('time [s]')
        plt.ylabel('counts')    
        plt.subplot(2,2,2)
        plt.hist(df.query('amplitude_ch3>20').tof_ch3, bins=100, range=(0,100*1e-9), label='YAP ch3')
        plt.legend()
        plt.xlabel('time [s]')
        plt.ylabel('counts')    
        plt.subplot(2,2,3)
        plt.hist(df.query('amplitude_ch4>20').tof_ch4, bins=100, range=(0,100*1e-9), label='YAP ch4')
        plt.legend()
        plt.xlabel('time [s]')
        plt.ylabel('counts')    
        plt.subplot(2,2,4)
        plt.hist(df.query('amplitude_ch5>20').tof_ch5, bins=100, range=(0,100*1e-9), label='YAP ch5')
        plt.legend()
        plt.xlabel('time [s]')
        plt.ylabel('counts')    

    if plot_tof_gflash:
        bins = 200
        N = len(df.query('amplitude_ch2>20').tof_ch2)
        c = 299792458.0 #speed of light in ns
        gtime = distance/c*1e9 #time it takes for photon to reach center of detector
        ntime_low = prodata.EnergytoTOF(1, 0.05)*1e9 #time it takes 4 MeV neutron to reach YAP
        ntime_high = prodata.EnergytoTOF(6, 0.05)*1e9 #time it takes 4 MeV neutron to reach YAP

        plt.figure(5)

        plt.subplot(2,2,1)
        plt.suptitle(f'Number of events = {4*N}')
        x = np.arange(-5,15,.1)
        y_val, x_val = np.histogram(df.query(f'amplitude_ch2>20').tof_ch2*1e9, range=(-5,15), bins=bins)
        popt = promath.gaussFit(prodata.getBinCenters(x_val), y_val, error=False)
        plt.plot(prodata.getBinCenters(x_val),y_val, lw=3, label='YAP ch2 data')
        plt.plot(x, promath.gaussFunc(x, popt[0], popt[1], popt[2]), lw=3, label=f'Gauss fit, mu={round(popt[1],2)} ns')
        plt.plot((gtime,gtime), (.1,1000), '--k', lw=3, label=f'Expected c @ {round(distance,3)} m, mu={round(gtime,2)} ns', alpha=.8)
        plt.axvspan(ntime_low, ntime_high, alpha=0.1, color='black', label='1-6 MeV neutrons')
        plt.legend()
        # plt.yscale('log')
        plt.xlabel('time [ns]')
        plt.ylabel('counts') 

        plt.subplot(2,2,2)
        y_val, x_val = np.histogram(df.query(f'amplitude_ch3>20').tof_ch3*1e9, range=(-5,15), bins=bins)
        popt = promath.gaussFit(prodata.getBinCenters(x_val), y_val, error=False)
        plt.plot(prodata.getBinCenters(x_val),y_val, lw=3, label='YAP ch3 data')
        plt.plot(x, promath.gaussFunc(x, popt[0], popt[1], popt[2]), lw=3, label=f'Gauss fit, mu={round(popt[1],2)} ns')
        plt.plot((gtime,gtime), (.1,1000), '--k', lw=3, label=f'Expected c @ {round(distance,3)} m: {round(gtime,2)} ns', alpha=.8)
        plt.legend()
        # plt.yscale('log')
        plt.xlabel('time [ns]')
        plt.ylabel('counts') 

        plt.subplot(2,2,3)
        y_val, x_val = np.histogram(df.query(f'amplitude_ch4>20').tof_ch4*1e9, range=(-5,15), bins=bins)
        popt = promath.gaussFit(prodata.getBinCenters(x_val), y_val, error=False)
        plt.plot(prodata.getBinCenters(x_val),y_val, lw=3, label='YAP ch4 data')
        plt.plot(x, promath.gaussFunc(x, popt[0], popt[1], popt[2]), lw=3, label=f'Gauss fit, mu={round(popt[1],2)} ns')
        plt.plot((gtime,gtime), (.1,1000), '--k', lw=3, label=f'Expected c @ {round(distance,3)} m: {round(gtime,2)} ns', alpha=.8)
        plt.legend()
        # plt.yscale('log')
        plt.xlabel('time [ns]')
        plt.ylabel('counts') 

        plt.subplot(2,2,4)
        y_val, x_val = np.histogram(df.query(f'amplitude_ch5>20').tof_ch5*1e9, range=(-5,15), bins=bins)
        popt = promath.gaussFit(prodata.getBinCenters(x_val), y_val, error=False)
        plt.plot(prodata.getBinCenters(x_val),y_val, lw=3, label='YAP ch5 data')
        plt.plot(x, promath.gaussFunc(x, popt[0], popt[1], popt[2]), lw=3, label=f'Gauss fit, mu={round(popt[1],2)} ns')
        plt.plot((gtime,gtime), (.1,1000), '--k', lw=3, label=f'Expected c @ {round(distance,3)} m: {round(gtime,2)} ns', alpha=.8)
        plt.legend()
        # plt.yscale('log')
        plt.xlabel('time [ns]')
        plt.ylabel('counts') 

    if plot_cfd_YAP:
        #Make 2d plot of tof vs amplitude around gamma flash to determine if the cfd is good.
        bins = 100
        plt.figure(6)
        plt.subplot(2,2,1)
        plt.suptitle(f'Number of events = {len(df)}')
        plt.hist2d(df.query('amplitude_ch2>30').tof_ch2, df.query('amplitude_ch2>30').amplitude_ch2, bins=bins, range=[[0,10*1e-9],[0,400]], norm=LogNorm(), cmap=prout.get_bad_pixel_color(f'{color}')) 
        plt.xlabel('ToF ch2 [s]')
        plt.ylabel('YAP ch2 amplitude [mV]') 

        plt.subplot(2,2,2)
        plt.hist2d(df.query('amplitude_ch3>30').tof_ch3, df.query('amplitude_ch3>30').amplitude_ch3, bins=bins, range=[[0,10*1e-9],[0,400]], norm=LogNorm(), cmap=prout.get_bad_pixel_color(f'{color}')) 
        plt.xlabel('ToF ch3 [s]')
        plt.ylabel('YAP ch3 amplitude [mV]') 

        plt.subplot(2,2,3)
        plt.hist2d(df.query('amplitude_ch4>30').tof_ch4, df.query('amplitude_ch4>30').amplitude_ch4, bins=bins, range=[[0,10*1e-9],[0,400]], norm=LogNorm(), cmap=prout.get_bad_pixel_color(f'{color}')) 
        plt.xlabel('ToF ch4 [s]')
        plt.ylabel('YAP ch4 amplitude [mV]') 

        plt.subplot(2,2,4)
        plt.hist2d(df.query('amplitude_ch5>30').tof_ch5, df.query('amplitude_ch5>30').amplitude_ch5, bins=bins, range=[[0,10*1e-9],[0,400]], norm=LogNorm(), cmap=prout.get_bad_pixel_color(f'{color}')) 
        plt.xlabel('ToF ch5 [s]')
        plt.ylabel('YAP ch5 amplitude [mV]') 

    if plot_cfd_neutron:
        bins=200
        plt.figure(7)
        plt.title(f'Number of events = {len(df)}')
        plt.hist2d(df.CFD_rise_ch1, df.amplitude_ch1, bins=bins, range=[[190,230],[0,800]], norm=LogNorm(), cmap=prout.get_bad_pixel_color(f'{color}'))
        plt.xlabel('CFD ch1 [s]')
        plt.ylabel('Neutron detector ch1 amplitude [mV]') 

    if plot_eCal:
        xVal = np.arange(1.5,7)
        plt.figure(8)
        plt.suptitle('Proton recoil energy, $E_p$ [MeV] vs. electron recoil energy, $E_e$ [MeVee]')
        plt.subplot(1,2,1)
        plt.plot(E, HH_save, lw=3, label='Half-height method')
        plt.plot(xVal, promath.kornilovEq(xVal, fitVal_kornilov_HH[0], fitVal_kornilov_HH[1]), label='Fit: Kornilov')        
        plt.plot(xVal, promath.cecilEq(xVal, fitVal_cecil_HH[0]), label='Fit: Cecil')        
        plt.xlabel('Proton recoil energy [MeV]')
        plt.ylabel('Electron recoil energy [MeVee]')
        plt.legend()
        
        plt.subplot(1,2,2)
        plt.plot(E, FD_save, lw=3, label='First derivative method')
        plt.plot(xVal, promath.kornilovEq(xVal, fitVal_kornilov_FD[0], fitVal_kornilov_HH[1]), label='Fit: Kornilov')        
        plt.plot(xVal, promath.cecilEq(xVal, fitVal_cecil_FD[0]), label='Fit: Cecil')        
        plt.xlabel('Proton recoil energy [MeV]')
        plt.ylabel('Electron recoil energy [MeVee]')
        plt.legend()    
    

    plt.show()
