import sys
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import dask.dataframe as dd
import dask.array as da
import random
from math import cos, pi, radians, factorial
from scipy import signal
from scipy.stats import chisquare
from scipy.integrate import quad
from scipy.signal import find_peaks
from tqdm import tqdm
from scipy.optimize import curve_fit
from matplotlib.colors import LogNorm
from dask.diagnostics import ProgressBar
from scipy.interpolate import UnivariateSpline, interp1d, splrep
# import seaborn as sns

import gaussEdgeFit_parametersEJ321P as gefEJ321P #Library with fit parameters for compton edges.
import gaussEdgeFit_parametersNE213 as gefNE213 #Library with fit parameters for compton edges.

# IMPORT FROM LOCAL LIBRARY
sys.path.insert(0, "../library/") #Import my own libraries
import processing_math as promath
import processing_utility as prout
import processing_pd as propd
import processing_data as prodata
import nicholai_plot_helper as nplt
import nicholai_exclusive as ne
import SignalGenerator as sg

# IMPORT FROM MAIN DIRECTORY
sys.path.insert(0, "../")
import processing


# ================================================
#         Load and append sliced data
# ================================================
 
df_sliced = pd.read_pickle('/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/analysis/df_sliced_329_409.pkl')
df_TEMP2 = pd.read_pickle('/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/analysis/df_sliced_410_499.pkl')
df_TEMP3 = pd.read_pickle('/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/analysis/df_sliced_500_589.pkl')
df_TEMP4 = pd.read_pickle('/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/analysis/df_sliced_590_679.pkl')
df_TEMP5 = pd.read_pickle('/media/gheed/Seagate_Expansion_Drive1/data/EJ321P_char/analysis/df_sliced_680_709.pkl')
df_sliced = df_sliced.append(df_TEMP2, ignore_index=True)
df_sliced = df_sliced.append(df_TEMP3, ignore_index=True)
df_sliced = df_sliced.append(df_TEMP4, ignore_index=True)
df_sliced = df_sliced.append(df_TEMP5, ignore_index=True)
# # del df_TEMP1
del df_TEMP2
del df_TEMP3
del df_TEMP4
del df_TEMP5

# ================================================
#         Fitting sliced energy spectrum
# ================================================

print('Fitting sliced DataFrame...')

#fitRange for data
fitRange = {'1.0_MeV':[0.043, 0.269], 
            '1.2_MeV':[0.043, 0.269], 
            '1.4_MeV':[0.043, 0.269], 
            '1.6_MeV':[0.043, 0.269], 
            '1.8_MeV':[0.043, 0.300], 
            '2.0_MeV':[0.130, 0.400], 
            '2.2_MeV':[0.242, 0.485], 
            '2.4_MeV':[0.193, 0.549], 
            '2.6_MeV':[0.293, 0.641], 
            '2.8_MeV':[0.308, 0.787], 
            '3.0_MeV':[0.333, 0.820], 
            '3.2_MeV':[0.388, 0.933], 
            '3.4_MeV':[0.439, 0.985], 
            '3.6_MeV':[0.497, 1.037], 
            '3.8_MeV':[0.500, 1.300], 
            '4.0_MeV':[0.625, 1.207],
            '4.2_MeV':[0.615, 1.310],
            '4.4_MeV':[0.740, 1.422],
            '4.6_MeV':[0.837, 1.505], 
            '4.8_MeV':[0.924, 1.537], 
            '5.0_MeV':[0.996, 1.691], 
            '5.2_MeV':[1.060, 1.800], 
            '5.4_MeV':[1.100, 1.850],
            '5.6_MeV':[1.173, 2.042], 
            '5.8_MeV':[1.260, 2.110], 
            '6.0_MeV':[1.407, 2.120]} 

#fit range for first derivative data
fitRange_der = {'1.0_MeV':[0.043, 0.269], 
                '1.2_MeV':[0.043, 0.269], 
                '1.4_MeV':[0.043, 0.269], 
                '1.6_MeV':[0.043, 0.269], 
                '1.8_MeV':[0.043, 0.300], 
                '2.0_MeV':[0.237, 0.539], 
                '2.2_MeV':[0.262, 0.530], 
                '2.4_MeV':[0.311, 0.604], 
                '2.6_MeV':[0.370, 0.693], 
                '2.8_MeV':[0.408, 0.787], 
                '3.0_MeV':[0.463, 0.820], 
                '3.2_MeV':[0.467, 0.983], 
                '3.4_MeV':[0.581, 1.026], 
                '3.6_MeV':[0.650, 1.117], 
                '3.8_MeV':[0.656, 1.150], 
                '4.0_MeV':[0.686, 1.330], 
                '4.2_MeV':[0.879, 1.433],
                '4.4_MeV':[0.758, 1.562],
                '4.6_MeV':[1.095, 1.756], 
                '4.8_MeV':[1.115, 1.807], 
                '5.0_MeV':[1.004, 2.031], 
                '5.2_MeV':[1.139, 2.049], 
                '5.4_MeV':[1.250, 2.180], 
                '5.6_MeV':[1.173, 2.260], 
                '5.8_MeV':[1.490, 2.267], 
                '6.0_MeV':[1.680, 2.522]} 


FD_save = np.arange(0) #container for holding final values
FD_error_save = np.arange(0) #container for holding final values
HH_save = np.arange(0) #container for holding final values
HH_error_save = np.arange(0) #container for holding final values
E = np.arange(0) #container for holding energy values
chi2_save_HH = np.arange(0) #container for holding the chi2/ndf values of the fits
chi2_save_FD = np.arange(0) #container for holding the chi2/ndf values of the fits

#set number of bins for binning data
numBins = 310 #310
#set how many bins each loop should decrase with. (in order to get a nicer looking histogram)
numBinDecrease = 9

for col in df_sliced.columns:
    #deacrase the number of bins
    numBins -= numBinDecrease
    #set the range of the binning
    binRange = (0, 7) #binning range
    #bin the data
    yval, xval = np.histogram(df_sliced[f'{col}'], bins=numBins, range=binRange)
    #get the centers of the bins
    xval = prodata.getBinCenters(xval)
    

    # plt.figure(figsize=(15,10))
    # plt.title(f'{col}')
    # plt.step(xval, yval)
    # # plt.hist(df_sliced[f'{col}'], bins=numBins, range=binRange, label=f'numBins={numBins}')
    # plt.plot([fitRange[f'{col}'][0], fitRange[f'{col}'][0]], [10,3000], 'k--', lw=1, alpha=.5)
    # plt.plot([fitRange[f'{col}'][1], fitRange[f'{col}'][1]], [10,3000], 'k--', lw=1, alpha=.5)
    # # plt.yscale('log')
    # plt.xlim(-.1,3)
    # plt.show()



    #define container for x and y values in fitRange
    xTEMP = np.arange(0)
    yTEMP = np.arange(0)
    #select out value to use for the fit based on the 'fitRange'
    for i in range(len(xval)):
        if xval[i]>=fitRange[col][0] and xval[i]<=fitRange[col][1]:
            xTEMP = np.append(xTEMP, xval[i])
            yTEMP = np.append(yTEMP, yval[i])
    #gaussian constant guess
    const = max(yTEMP) 
    #gaussian mean guess
    mean = np.mean(xTEMP) 
    #gaussian standard deviation guess
    std = np.std(xTEMP) 
    #statistical error, based on number of counts
    stat_error_gauss = np.sqrt(yTEMP)

    try:
        #fitting the binned data with a Gaussian function
        popt, pcov = curve_fit(promath.gaussFunc, xTEMP, yTEMP, p0 = [const, mean, std], sigma=np.abs(stat_error_gauss/yTEMP), absolute_sigma=False)
        #calculate errors of Gaussian fit
        pcov = np.sqrt(np.diag(pcov))

        print('.......................')
        print(f'HH gaussian fit for E={col}')
        chi2_HH = round(promath.chi2red(yTEMP, promath.gaussFunc(xTEMP, popt[0], popt[1], popt[2]), stat_error_gauss, 3), 3)
        print('.......................')

        # +~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~
        # HH - Half-height, ref 35, Naqvi et al., gaussian fit of edge, take half-height as the value
        # +~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~

        #create higher resolution x values
        xhr = np.arange(xTEMP[0], xTEMP[-1], 1/10000) 
        #create y-values based on Gaussian fit
        yhr = promath.gaussFunc(xhr, popt[0], popt[1], popt[2]) 
        #set tolerance to find the HH value
        tolerance = 3
        #check for half maximum values
        HH_mask = [(yhr >= popt[0]/2-tolerance) & (yhr <= popt[0]/2+tolerance)]
        #fetch the xvalues which fullfill the critera above
        HH = xhr[tuple(HH_mask)]
        #save the central index value
        HH = HH[int(round(len(HH)/2))]
        #get error in current HH postion
        HH_error =  promath.errorPropComptonEdgeFit(pcov[1], pcov[2], .5)

        # +~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~
        # FD - First derivative, ref 36, Kornilov et al., energy minimum in first derivative is the value (no fit)
        # +~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~

        # create interpolated splined data of binned data
        #set smoothing level (see scipy.UnivariateSpline documentation)
        s = len(yval)*np.var(yval)
        #interpolate data
        spl = UnivariateSpline(xval, yval, k=5, s=s, w=np.sqrt(yval))
        yval_der = spl.derivative(1)
        yval_der = yval_der(xval)
        yval_der2 = np.gradient(yval, xval)#promath.gaussFuncDer(xhr, popt[0], popt[1], popt[2])#np.diff(yval)
        #::::::::Fit FD data:::::::::::::
        #1. filter data
        xTEMP_der = np.arange(0)#define container for x and y values in fitRange for derivative
        yTEMP_der = np.arange(0)#define container for x and y values in fitRange for derivative
        for i in range(len(xval)):
            if xval[i]>=fitRange_der[col][0] and xval[i]<=fitRange_der[col][1]: #removed fitRange_der and now using same range as HH
                xTEMP_der = np.append(xTEMP_der, xval[i])
                yTEMP_der = np.append(yTEMP_der, yval_der[i])
        
        #2. fit data with gaussian incl. statistical errors.
        stat_error_der = np.sqrt(np.abs(yTEMP_der))#calculate stat. errors.
        der_fit, der_err = curve_fit(promath.gaussFunc, xTEMP_der, yTEMP_der, [np.min(yval_der), np.mean(popt[1]), np.std(xTEMP_der)], sigma=np.abs(stat_error_der/yTEMP_der), absolute_sigma=False)
        der_err = np.sqrt(np.diag(der_err))
        print('.......................')
        print(f'FD gaussian fit for E={col}]')
        chi2_FD = round(promath.chi2red(yTEMP_der, promath.gaussFunc(xTEMP_der, der_fit[0], der_fit[1], der_fit[2]), stat_error_der, 3), 3)
        print('.......................')
        #create higher resolution x values for derivaive gaussian plot
        xhr_der = np.arange(xTEMP_der[0], xTEMP_der[-1], 1/10000)
                
        #3. get FD value from fit
        FD = der_fit[1] #gaussian mean value
        #::::::::::::::::::::::::::::::::

        #:::::::::: OLD :::::::::::::::::
        # #mask for only slecting relevant part of the data.
        # FD_mask = [xval > popt[1]] #[prodata.getBinCenters(xval) > popt[1]] 
        # #find difference in lenght with and without mask. Used to find the correct index value for FD.
        # offset = len(yval_der)-len(yval_der[tuple(FD_mask)]) 
        # #find value of FD by adding the offset in index values
        # FD = xval[np.argmin(yval_der[tuple(FD_mask)])+offset]#prodata.getBinCenters(xval)[np.argmin(der[tuple(FD_mask)])+offset]
        # #calculate error in FD
        #:::::::::::::::::::::::::::::::::#

        FD_error = promath.errorPropComptonEdgeFit(der_err[1], der_err[2], .5) 
        print(f'FD_error = {FD_error}')
        
        #:::::::::::::::::::::::::::::::::

        # +~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~
        # TP - Turning point, same at FD when gaussian fits perfectly.
        # +~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~
        #TODO: What is the turning point? Normally it would be when it crosses zero but that does not make sense for a gaussian.


        # +~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~
        #      Save HH and FD data to final data set
        # +~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~+~
        #append current HH value to final data set
        HH_save = np.append(HH_save, HH)
        #append current HH error
        HH_error_save = np.append(HH_error_save, HH_error)
        #append current FD value to final data set
        FD_save = np.append(FD_save, FD)
        #append current FD error
        FD_error_save = np.append(FD_error_save, FD_error)
        #append curent energy value (MeV) to final data set.
        E = np.append(E, float(col.split('_')[0]))
        #append current chi2 value for HH method fit.
        chi2_save_HH = np.append(chi2_save_HH, chi2_HH)
        #append current chi2 value for FD method fit.
        chi2_save_FD = np.append(chi2_save_FD, chi2_FD)



        plt.figure(figsize=(15,10))
        plt.subplot(2,1,1)
        plt.suptitle(f'Energy = {col}') 
        plt.plot(xTEMP, yTEMP,':r', lw=5, label='fit_range')
        plt.scatter(xval, yval, marker='s', s=70, facecolor='b', edgecolors='b', label='Data')
        plt.plot(xhr, promath.gaussFunc(xhr, popt[0], popt[1], popt[2]), lw=3, color='orange', label=f'Gaussian fit, chi2/ndf={chi2_HH}') 
        plt.plot([HH, HH], [popt[0]/3, popt[0]-popt[0]*0.2], '--', color='orange', lw=2, label=f'HH={round(HH, 2)} MeVee')
        plt.plot([FD, FD], [popt[0]/3, popt[0]-popt[0]*0.2], '--', color='green', lw=2, label=f'FD={round(FD, 2)} MeVee')
        plt.xlim([.2, fitRange[col][1]*2])
        plt.legend()
        plt.xlabel('Energy [MeVee]')
        plt.ylabel('Counts')
        plt.subplot(2,1,2)
        # plt.plot(xTEMP_der, yTEMP_der,':r', lw=5, label='fit_range')
        plt.plot(np.arange(.1, 5.5, 1/100), spl.derivative()(np.arange(.1, 5.5, 1/100)), ':b', lw=2, label='Data derivative (smoothing)')
        plt.scatter(xval, yval_der2, marker='o', s=70, facecolor='none', edgecolors='k', label='Data derivative (np.gradient()')
        # plt.plot(xhr_der, promath.gaussFunc(xhr_der, der_fit[0], der_fit[1], der_fit[2]), lw=3, color='orange',label=f'Gaussian fit, chi2/ndf={chi2_FD}') 
        plt.xlim([.2, fitRange[col][1]*2])

        plt.legend()
        plt.xlabel('Energy [MeVee]')
        plt.ylabel('Derivative')
        plt.show(block=True)





    except RuntimeError:
        print(f'error: Optimal parameters (Gaussian fit) not found for {col} after 800 iterrations.')
        #In case fit was not possible save only nan values to final output
        E = np.append(E, np.nan)
        FD_save = np.append(FD_save, np.nan)
        FD_error_save = np.append(FD_error_save, np.nan)
        HH_save = np.append(HH_save, np.nan)
        HH_error_save = np.append(HH_error_save, np.nan)
        chi2_save_HH = np.append(chi2_save_HH, np.nan)
        chi2_save_FD = np.append(chi2_save_FD, np.nan)
    except IndexError:
        print(f'error: Gaussian fit failed for {col}: Mean value to low!')
        #In case fit was not possible save only nan values to final output
        E = np.append(E, np.nan)
        FD_save = np.append(FD_save, np.nan)
        FD_error_save = np.append(FD_error_save, np.nan)
        HH_save = np.append(HH_save, np.nan)
        HH_error_save = np.append(HH_error_save, np.nan)
        chi2_save_HH = np.append(chi2_save_HH, np.nan)
        chi2_save_FD = np.append(chi2_save_FD, np.nan)

# =========================================================
#   Fit maximum proton transfer edge data with two methods
# =========================================================
E = E[~np.isnan(E)] #remove NAN values from array
HH_save = HH_save[~np.isnan(HH_save)] #remove NAN values from array
FD_save = FD_save[~np.isnan(FD_save)] #remove NAN values from array
HH_error_save = HH_error_save[~np.isnan(HH_error_save)] #remove NAN values from array
FD_error_save = FD_error_save[~np.isnan(FD_error_save)] #remove NAN values from array

lim_min = 5 #slicing values for light yeild fit (ignore "bad" values)
lim_max = -5 #slicing values for light yeild fit (ignore "bad" values)

#fit data
fitVal_kornilov_HH, fitErr_kornilov_HH = curve_fit(
                                        promath.kornilovEq, 
                                        E[lim_min:lim_max], 
                                        HH_save[lim_min:lim_max], 
                                        p0 = [0.452, 2.817], 
                                        sigma = HH_error_save[lim_min:lim_max], 
                                        absolute_sigma = False)
fitErr_kornilov_HH = np.sqrt(np.diag(fitErr_kornilov_HH))

fitVal_cecil_HH, fitErr_cecil_HH = curve_fit(
                                        promath.cecilEq, 
                                        E[lim_min:lim_max], 
                                        HH_save[lim_min:lim_max], 
                                        p0 = [.638], 
                                        sigma = HH_error_save[lim_min:lim_max], 
                                        absolute_sigma = False)
fitErr_cecil_HH = np.sqrt(np.diag(fitErr_cecil_HH))

fitVal_kornilov_FD, fitErr_kornilov_FD = curve_fit(
                                        promath.kornilovEq, 
                                        E[lim_min:lim_max], 
                                        FD_save[lim_min:lim_max], 
                                        p0 = [.4, 2.38], 
                                        sigma = FD_error_save[lim_min:lim_max], 
                                        absolute_sigma = False)
fitErr_kornilov_FD = np.sqrt(np.diag(fitErr_kornilov_FD))

fitVal_cecil_FD, fitErr_cecil_FD = curve_fit(
                                        promath.cecilEq, 
                                        E[lim_min:lim_max], 
                                        FD_save[lim_min:lim_max], 
                                        p0 = [.615], 
                                        sigma = FD_error_save[lim_min:lim_max], 
                                        absolute_sigma = False)
fitErr_cecil_FD = np.sqrt(np.diag(fitErr_cecil_FD))

plt.figure(100)
plt.subplot(2,1,1)
plt.plot(E[lim_min:lim_max], HH_save[lim_min:lim_max], 'o', color='black', label='Half-Height')
#plot HH Kornolov
obs = HH_save[lim_min:lim_max] 
exp = promath.kornilovEq(E[lim_min:lim_max], fitVal_kornilov_HH[0], fitVal_kornilov_HH[1])
obs_var = HH_error_save[lim_min:lim_max]
chi2_red = round(promath.chi2red(obs, exp, obs_var, 2),2)
plt.plot(E[lim_min:lim_max], promath.kornilovEq(E[lim_min:lim_max], fitVal_kornilov_HH[0], fitVal_kornilov_HH[1]), ':', color='black', lw=2, label=f'Kornilov fit, L0={round(fitVal_kornilov_HH[0],3)}+/-{round(fitErr_kornilov_HH[0],3)}, L1={round(fitVal_kornilov_HH[1],3)}+/-{round(fitErr_kornilov_HH[1],3)}, Chi2/ndf={chi2_red}')

#plot HH Cecil
exp = promath.cecilEq(E[lim_min:lim_max], fitVal_cecil_HH[0])
chi2_red = round(promath.chi2red(obs, exp, obs_var, 1),2)
plt.plot(E[lim_min:lim_max], promath.cecilEq(E[lim_min:lim_max], fitVal_cecil_HH[0]), '--', color='black', lw=2, label=f'Cecil fit, A={round(fitVal_cecil_HH[0],3)}+/-{round(fitErr_cecil_HH[0],3)}, Chi2/ndf={chi2_red}')
plt.xlabel('T [MeV]')
plt.ylabel('L [MeVee]')
plt.grid(which='both')
plt.legend()


plt.subplot(2,1,2)
plt.plot(E[lim_min:lim_max], FD_save[lim_min:lim_max], 'o', color='green', label='First Derivative')
#plot FD Kornilov
obs = FD_save[lim_min:lim_max] 
exp = promath.kornilovEq(E[lim_min:lim_max], fitVal_kornilov_FD[0], fitVal_kornilov_FD[1])
obs_var = FD_error_save[lim_min:lim_max]
chi2_red = round(promath.chi2red(obs, exp, obs_var, 2),2)
plt.plot(E[lim_min:lim_max], promath.kornilovEq(E[lim_min:lim_max], fitVal_kornilov_FD[0], fitVal_kornilov_FD[1]), ':', color='green', lw=2, label=f'Kornilov fit, L0={round(fitVal_kornilov_FD[0],3)}+/-{round(fitErr_kornilov_FD[0],3)}, L1={round(fitVal_kornilov_FD[1],3)}+/-{round(fitErr_kornilov_FD[1],3)}, Chi2/ndf={chi2_red}')

#plot FD cecil
exp = promath.cecilEq(E[lim_min:lim_max], fitVal_cecil_FD[0])
obs_var = FD_error_save[lim_min:lim_max]
chi2_red = round(promath.chi2red(obs, exp, obs_var, 1),2)
plt.plot(E[lim_min:lim_max], promath.cecilEq(E[lim_min:lim_max], fitVal_cecil_FD[0]), '--', color='green', lw=2, label=f'Cecil fit, A={round(fitVal_cecil_FD[0],3)}+/-{round(fitErr_cecil_FD[0],3)}, Chi2/ndf={chi2_red}')
plt.xlabel('T [MeV]')
plt.ylabel('L [MeVee]')
plt.grid(which='both')

plt.legend()
plt.show()
