import sys
from scipy import *
import matplotlib.pyplot as plt
import numpy as np
from math import *
from scipy.integrate import quad
import pandas as pd
import re


#::: HYDROGEN :::::
with open('data/neutron_TOTcross_H1.txt', 'r') as file: 
    H1 = file.read()#.replace('\n', ',')
H1 = re.sub(' +', ' ', H1) #remove irregular white space in string.
H1 = H1.replace('\n',',')
H1 = H1.replace(' Lin-Lin','')     
H1 = H1.replace(', ',',')     
H1 = H1.replace(' ',',')     
#convert data from string to float
a = ''
data_H = np.arange(0)
for i in H1:
    if i == ',':
        if a != '':
            data_H = np.append(data_H, np.float(a))
            a='' #reset
    if i != ',':
        a += i
#sort the data in two columns
xval_H = np.arange(0)
yval_H = np.arange(0)
for i in range(len(data_H)):
    if (i % 2) == 0:
        xval_H = np.append(xval_H, data_H[i])
        # print(i)
    else:
        yval_H = np.append(yval_H, data_H[i])

plt.figure(0)
plt.plot(xval_H, yval_H)
plt.yscale('log')
plt.xscale('log')
plt.ylabel('Total cross-section [barns]')
plt.xlabel('Energy [eV]')
plt.title('Neutron total cross section on H-1')



#::::: CARBON ::::::
with open('data/neutron_TOTcross_C12.txt', 'r') as file: 
    C12 = file.read()#.replace('\n', ',')
C12 = re.sub(' +', ' ', C12) #remove irregular white space in string.
C12 = C12.replace('\n',',')
C12 = C12.replace(' Lin-Lin','')     
C12 = C12.replace(', ',',')     
C12 = C12.replace(' ',',')     
#convert data from string to float
a = ''
data_C = np.arange(0)
for i in C12:
    if i == ',':
        if a != '':
            data_C = np.append(data_C, np.float(a))
            a='' #reset
    if i != ',':
        a += i
#sort the data in two columns
xval_C = np.arange(0)
yval_C = np.arange(0)
for i in range(len(data_C)):
    if (i % 2) == 0:
        xval_C = np.append(xval_C, data_C[i])
        # print(i)
    else:
        yval_C = np.append(yval_C, data_C[i])

plt.figure(1)
plt.plot(xval_C, yval_C)
plt.yscale('log')
plt.xscale('log')
plt.ylabel('Total cross-section [barns]')
plt.xlabel('Energy [eV]')
plt.title('Neutron total cross section on C-12')

plt.show()