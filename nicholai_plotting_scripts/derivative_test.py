import sys
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import dask.dataframe as dd
import dask.array as da
import random

from math import cos, pi, radians, factorial
from scipy import signal
from scipy.stats import chisquare
from scipy.integrate import quad
from scipy.signal import find_peaks
from tqdm import tqdm
from scipy.optimize import curve_fit
from matplotlib.colors import LogNorm
from dask.diagnostics import ProgressBar
from scipy.interpolate import UnivariateSpline, interp1d, splrep
# import seaborn as sns

import gaussEdgeFit_parametersEJ321P as gefEJ321P #Library with fit parameters for compton edges.
import gaussEdgeFit_parametersNE213 as gefNE213 #Library with fit parameters for compton edges.

# IMPORT FROM LOCAL LIBRARY
sys.path.insert(0, "../library/") #Import my own libraries
import processing_math as promath
import processing_utility as prout
import processing_pd as propd
import processing_data as prodata
import nicholai_plot_helper as nplt
import nicholai_exclusive as ne
import SignalGenerator as sg

# IMPORT FROM MAIN DIRECTORY
sys.path.insert(0, "../")
import processing
import csv

def f(data):
    """
    data.....this is an array of the original data
    """
    grad = np.gradient(data) #calculate the gradient/derivative of data set
    der = np.arange(0)
    for i in range(len(grad[:-1])):
        der = np.append(der, np.log(grad[i+1]/grad[i]))

    return der



jul_data = np.genfromtxt('data/julius_data.csv', delimiter=',')
jul_data_der = np.genfromtxt('data/julius_data_derivative.csv', delimiter=',')
# data = np.genfromtxt('data/light_yield_data_test.csv', delimiter=',')
# data2 = np.genfromtxt('data/light_yield_derivative_test.csv', delimiter=',')
# xval = data[:,0]
# yval = data[:,1]*1/307
# yval2 = data2[:,1]

xval = jul_data_der[:,0]
yval = jul_data[:,1]
yval2 = jul_data_der[:,1]

#interpolation
#smoothing value for UnivariateSpline(), choosen to "look good"
s = len(yval)*np.var(yval)/700
#interpolate data
spl = UnivariateSpline(xval, yval, k=5, s=s)
# calculate first order derivative of interpolation and save as array
spl_der = spl.derivative()

xhr = np.arange(0.5, 3, 1/100)

#plotting
plt.figure(0)
plt.scatter(xval, yval, lw=2, marker='s', color='black', label='Julius data')
plt.step(xval, yval2, '--', color='teal', alpha=.6, lw=2, label='Julius derivative')
# plt.plot(xhr, spl(xhr), ':k', lw=2, alpha=.6, label='UnivariateSpline interpolation')
plt.step(xval, np.gradient(yval,xval), alpha=.8, lw=2, label='np.gradient(n=1))')
plt.step(xval, np.gradient(yval,xval)*-0.2486, alpha=.8, lw=2, label='np.gradient(n=1)*0.2486*(-1))')
# plt.step(xval, spl_der(xval), alpha=.8, lw=2, label='UnivariateSpline derivative /w binwidths')
# plt.step(prodata.getBinCenters(xval), np.diff(yval), lw=2, alpha=.8, label='np.diff')
# plt.step(prodata.getBinCenters(xval), f(yval), lw=2, label='$ln \\left( \\frac{Y(x+dx)}{Y(x)} \\right)$ (from article)')
# plt.step(xhr, spl_der(xhr), alpha=.8, lw=2, label='UnivariateSpline interpolation derivative')
# plt.step(xhr, np.gradient(spl(xhr), xhr), alpha=.8, lw=2, label='np.gradient: UnivariateSpline interpolation derivative')
plt.legend(loc='upper right')
plt.grid(which='both')
# plt.xlim([.2, 1.2])
# plt.ylim([-3000, 1200])
plt.show()
