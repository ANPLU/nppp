import logging

def init_logging(logger, log_level = None):
    """ initializes logging """
    log = logging.getLogger(logger)  # get reference to logger
    # test if we have set up the logger before
    if not len(log.handlers):
        # perform setup by adding handler:
        formatter = logging.Formatter('%(asctime)s %(name)s(%(levelname)s): %(message)s',"%H:%M:%S")
        handler_stream = logging.StreamHandler()
        handler_stream.setFormatter(formatter)
        log.addHandler(handler_stream)
        # using this decorator, we can count the number of error messages
        class callcounted(object):
            """Decorator to determine number of calls for a method"""
            def __init__(self,method):
                self.method=method
                self.counter=0
            def __call__(self,*args,**kwargs):
                self.counter+=1
                return self.method(*args,**kwargs)
        log.error=callcounted(log.error)
        if log_level is None:
            log_level = "INFO"
    if not log_level is None:
        # set the logging level
        numeric_level = getattr(logging, log_level.upper(),
                                None)  # default: INFO messages and above
        log.setLevel(numeric_level)
    return log


def parseIntegerString(nputstr=""):
    """
    return a list of selected values when a string in the form:
    1-4,6
    would return:
    1,2,3,4,6
    as expected...
    (from http://thoughtsbyclayg.blogspot.de/2008/10/parsing-list-of-numbers-in-python.html)
    """
    selection = list()
    # tokens are comma seperated values
    tokens = [substring.strip() for substring in nputstr.split(',')]
    for i in tokens:
        try:
            # typically tokens are plain old integers
            selection.append(int(i))
        except ValueError:
            # if not, then it might be a range
            token = [int(k.strip()) for k in i.split('-')]
            if len(token) > 1:
                token.sort()
                # we have items seperated by a dash
                # try to build a valid range
                first = token[0]
                last = token[len(token)-1]
                for value in range(first, last+1):
                    selection.append(value)
    return selection # end parseIntegerString

def ireplace(old, new, text):
    """
    case insensitive search and replace function searching through string and returning the filtered string
    (based on http://stackoverflow.com/a/4773614)
    """
    idx = 0
    occur = 0
    while idx < len(text):
        index_l = text.lower().find(old.lower(), idx)
        if index_l == -1:
            if occur == 0:
                raise EOFError("Could not find string "+old)
            return text
        text = text[:index_l] + new + text[index_l + len(old):]
        idx = index_l + len(new)
        occur = occur+1
    if occur == 0:
        raise EOFError("Could not find string "+old)
    return text
