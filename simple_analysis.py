import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import tabulate as tb
import os

import digviz as dv


def loadrun(run, path, max_evts=10000000):
    print('test',os.path)
    df = dv.load_h5(os.path.join(path,f"jadaq-{run:05}.h5"))#, max_evts=max_evts)

    # calculate baseline based on the first few samples
    df['baseline'] = df.samples.apply(lambda x: np.mean(x[:20]))
    # calculate simple pulse-height
    df['PH'] = np.abs(df.samples.apply(lambda x: min(x))-df['baseline'])
        # calculate overshoot
    df['overshoot'] = np.abs(df.samples.apply(lambda x: max(x))-df['baseline'])
    # find number of samples
    nsamples = df.samples.head(1).apply(lambda x: len(x)).values[0]
    # calculate simple charge-integration
    df['QDC'] = np.abs(df.samples.apply(lambda x: np.sum(x))-(nsamples*df['baseline']))

    # calc. number of evtno overflows
    novflow = df.evtno.diff().lt(0).sum()
    # calc. number of total triggers (incl. missed)
    ntrgg = df.evtno.diff().dropna().sum() + 1 + ( novflow * np.iinfo(df.evtno.dtype).max )
    # calc. number of events recorded (grouped by channel, take first active channel)
    nevts = df.groupby(['ch']).size().iloc[0]
    # data loss
    dloss = (ntrgg - nevts) / ntrgg
    print(f"Number of trigger: {ntrgg}; number of events: {nevts}; Calculated data loss: {round(dloss*100)}%")

    print("Done with loading and calculations")
    return df

def plot_samples(df, channel, run, filter="", criteria="", nsamples=5):
    # plot events with certain characteristics
    fig, ax = plt.subplots()
    if criteria:
        samples = df.query(f"{criteria} and ch=={channel} {filter}").head(nsamples).samples
        ax.set_title(f"run {run}, ch{channel}, ${criteria}$")
    else:
        samples = df.query(f"ch=={channel} {filter}").head(nsamples).samples
        ax.set_title(f"run {run}, ch{channel}")
    for s in samples:
        ax.plot(range(len(s)), s)
    ax.set_xlabel("time [sample]")
    ax.set_ylabel("V [ADC]")
    #ax.set_xlim(left=50,right=200)
    ax.grid(True)
    return fig, ax

def plot_baseline(df, channel, run, filter=""):
    # plot baseline
    fig, ax = plt.subplots()
    df.query(f"ch=={channel} {filter}").baseline.hist(bins=40)
    ax.set_title(f"run {run}, channel {channel}")
    ax.set_xlabel("baseline [ADC]")
    ax.set_ylabel("counts")
    ax.set_yscale('log')
    return fig, ax

def plot_ph(df, channel, run, filter=""):
    # plot PH
    fig, ax = plt.subplots()
    df.query(f"ch=={channel} {filter}").PH.hist(bins=150)
    ax.set_title(f"run {run}, channel {channel}")
    ax.set_xlabel("PH [ADC]")
    ax.set_ylabel("counts")
    ax.set_yscale('log')
    return fig, ax

def plot_qdc(df, channel, run, filter=""):
    # plot QDC
    fig, ax = plt.subplots()
    df.query(f"ch=={channel} {filter}").QDC.hist(bins=150)
    #ax.set_yscale("log")
    ax.set_title(f"run {run}, channel {channel}")
    ax.set_xlabel("QDC [a.u.]")
    ax.set_ylabel("counts")
    ax.set_yscale('log')
    return fig, ax


def main():
    print("Libraries loaded:")
    print("   - Matplotlib version {}".format(matplotlib.__version__))
    print("   - Pandas version {}".format(pd.__version__))
    print("   - Numpy version {}".format(np.__version__))

    # configuration
    # TODO use argparse to set these options
    datapath = './data'#"/media/gheed/Seagate_Expansion_Drive1/data/TNT/digital/NE213_cup/"
    #datapath='C:/Users/linus/Git/STF/data/'
    channels = [3]
    run = 187
    bl_filter = " and baseline < 945 and baseline>940" # events where first samples are far from the baseline are typically tails of pulses being caught
    ph_filter = " and PH > 1 and PH<500"       # reinforces the threshold cut, removes events with trigger on other channel(s)
    os_filter=  "and overshoot<5" # overshootfilter to remove noise
    # for output from the digviz library:
    # only done on first run
    try:
        log
    except NameError:
        import logging
        formatter = logging.Formatter('%(asctime)s %(name)s(%(levelname)s): %(message)s',"%H:%M:%S")
        handler_stream = logging.StreamHandler()
        handler_stream.setFormatter(formatter)
        log = logging.getLogger('digviz')
        log.addHandler(handler_stream)
        log.setLevel(logging.INFO) # or logging.DEBUG, logging.WARN etc

    plt.style.use('dark_background')

    # enable LaTeX
    from matplotlib import rc
    rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
    ## for Palatino and other serif fonts use:
    #rc('font',**{'family':'serif','serif':['Palatino']})
    rc('text', usetex=True)

    rc('figure', dpi=150 ) # resolution of plots

    df = loadrun(run, datapath)
    for channel in channels:
        print(f'Number of samples for channel {channel}: {len(df.query(f"ch=={channel} {bl_filter} {ph_filter} {os_filter}") .samples)} (filtered by {bl_filter}{ph_filter}{os_filter}) and {len(df.query(f"ch=={channel}").samples)} (raw)')
        fig, ax = plot_samples(df, channel, run, filter=bl_filter+ph_filter+os_filter, nsamples=10)
        #ax.set_xlim(left=400,right=1000)
#        ax.set_ylim(bottom=950,top=1020)
        fig, ax = plot_baseline(df, channel, run, filter=ph_filter+os_filter)
        fig, ax = plot_ph(df, channel, run, filter=bl_filter+ph_filter+os_filter)
        fig, ax = plot_qdc(df, channel, run, filter=bl_filter+ph_filter+os_filter)
        # investigate samples with large QDC values
        criteria = "QDC > 4000"
        nselevts = len(df.query(f"PH>1 and {criteria} and ch=={channel} {bl_filter} {ph_filter} {os_filter}").samples)
        print(f"Number of events fullfilling '{criteria}' in channel {channel}: {nselevts}")
        fig, ax = plot_samples(df, channel, run, filter=bl_filter+ph_filter+os_filter, criteria=criteria,nsamples=nselevts)
    # put matplotlib into non-interactive mode
    plt.ioff()
#    ## final commands to show resulting plots (not needed in an interactive session)
    plt.show(block=False)  # block to allow "hit enter to close"
#    plt.pause(0.001)  # <-------
#    input("<Hit Enter To Close>")
#    plt.close('all')


if __name__ == '__main__':
    main()
