#Keras stuff
#for defining and training the model
from keras.models import Sequential
from keras.layers import Dense, Flatten
from keras.layers import Conv1D, MaxPooling1D
from keras import optimizers
#for saving intermediate models
from keras.callbacks import ModelCheckpoint

#Random seed settings for reproducability
from numpy.random import seed
seed(1)
from tensorflow import set_random_seed
set_random_seed(2)
#plotting dependencies
import matplotlib.pyplot as plt
import seaborn as sns
sns.set(style='ticks')
#Dataframe and numpy
import pandas as pd
import numpy as np
#For averaging accuray accross neighbouring epochs
from scipy.signal import convolve

import dask.dataframe as dd
from dask.diagnostics import ProgressBar

#============#
#Prepare data#
#============#
#load parquet
D=dd.read_parquet('data/jadaq-00012.pq', engine='pyarrow')
#limits of tof peaks
lims_ch2 = (75000, 85000, 88000, 115000)
lims_ch3 = (75000, 85000, 88000, 115000)
lims_ch4 = (75000, 85000, 88000, 115000)
lims_ch5 = (75000, 85000, 88000, 115000)
lims = lims_ch2 + lims_ch3 + lims_ch4 + lims_ch5

#keep only those in tof peaks
D=D.query('(%s<tof_ch2<%s) or (%s<tof_ch2<%s) or (%s<tof_ch3<%s) or (%s<tof_ch3<%s) or (%s<tof_ch4<%s) or (%s<tof_ch4<%s) or (%s<tof_ch5<%s) or (%s<tof_ch5<%s)'%lims)
#target=1 for neutrons and target=0 for gamma-rays.
D['target'] = 0
D['target'] = D['target'].where( ((D['tof_ch2']>lims_ch2[0]) & (D['tof_ch2']<lims_ch2[1])) | ((D['tof_ch3']>lims_ch3[0]) & (D['tof_ch3']<lims_ch3[1])) | ((D['tof_ch4']>lims_ch4[0]) & (D['tof_ch4']<lims_ch4[1])) | ((D['tof_ch5']>lims_ch5[0]) & (D['tof_ch5']<lims_ch5[1])), 1)

#create training and test datasets
Train, Test = D.random_split([0.5, 0.5], random_state=None)
print('Loading data')
with ProgressBar():
    Train = Train.compute()
    Test = Test.compute()
Train = Train.reset_index()
Test = Test.reset_index();


### It is necessary to reshape data into a tensor of shape (L, window_width, 1)###
print('Reshaping data')
def reshape_samples(df, waveFormLength):
    S = np.array([None]*df.shape[0])
    for i in range(0, len(df)):
        pretrig=20
        S[i] = df.samples[i][int(0.5 + df.cfd_trig_rise[i]/1000)-pretrig: int(0.5 + df.cfd_trig_rise[i]/1000)+waveFormLength-pretrig].astype(np.float64)
    S = np.stack(S[0:len(S)])
    S=S.reshape(len(S),waveFormLength,1)
    return S

WaveFormLength=300
x_train = reshape_samples(Train, WaveFormLength)
y_train = Train.target

x_test = reshape_samples(Test, WaveFormLength)
y_test = Test.target



#======================#
#Define model and train#
#======================#
#model definition
model = Sequential()
#2X conv+maxpool
model.add(Conv1D(filters=10, kernel_size=9, strides=4, activation='relu', input_shape=(WaveFormLength, 1), padding='same'))
model.add(MaxPooling1D(2, strides=2))
model.add(Conv1D(filters=16, kernel_size=5, strides=2, activation='relu', padding='same'))
model.add(MaxPooling1D(2, stride=2))
#Fully connected
model.add(Flatten(name='flat'))
model.add(Dense(1, activation='sigmoid', name='preds'))
#Choose training parameters
opt = optimizers.Adam(lr=0.0005, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0, amsgrad=False)
model.compile(loss='binary_crossentropy', optimizer=opt, metrics=['accuracy'])
#Save models as training progresses
#https://keras.io/callbacks/
filepath="data/CNN_models/weights-improvement-{epoch:02d}-{val_acc:.2f}.hdf5"
checkpoint = ModelCheckpoint(filepath, monitor='val_acc', verbose=1, save_best_only=False, mode='max')
callbacks_list = [checkpoint]
#Fit the model
epochs = 90
hist = model.fit(x_train, y_train, batch_size=10, epochs=epochs, validation_data=(x_test, y_test),verbose=2, callbacks=callbacks_list)
print(hist.history)
#Make predictions
predictions = model.predict(x_test)
Test['pred'] = predictions

#============#
#Plot results#
#============#
#Accuaracy as function of epoch
df_test0 = Test.query('pred<0.5')
df_test1 = Test.query('pred>=0.5')
kernel=np.array([1,1,1,1,1,1,1,1,1])/9
plt.figure(figsize=(6.2,2.4))
plt.plot(hist.history['acc'], label='Training accuracy', color='blue')
plt.plot(range(4,epochs-4), convolve(hist.history['acc'], kernel, method='direct',mode='same')[4:-4], label='Accuracy averaged', color='green')
plt.plot(hist.history['val_acc'], label='Validation accuracy', color='red')
plt.plot(range(4, epochs-4), convolve(hist.history['val_acc'], kernel, method='direct', mode='same')[4:-4], label='Validation accuracy averaged', color='orange')
ax = plt.gca()
ax.tick_params(axis = 'both', which = 'both', labelsize = 12)
plt.xlabel('Iteration', fontsize=12)
plt.ylabel('Accuracy', fontsize=12)
plt.title('Training and validation accuracy')
plt.legend()
plt.tight_layout()
plt.show()

#ToF histogram
plt.hist(Test.tof_ch5/1000, bins=75, range=(-20,130), alpha=0.25, label='Sum')
plt.hist(df_test0.tof_ch5/1000, bins=50, range=(70,120), histtype='step', lw=1.5, label='Gamma')
plt.hist(df_test1.tof_ch5/1000, bins=50, range=(70,120), histtype='step', lw=1.5, label='Neutron')
plt.legend()
plt.title('ToF spectrum \nfiltered by convolutional neural network\nTrained on 45 minute dataset, here tested on 15 minute dataset')
plt.ylabel('Counts')
plt.xlabel('t(ns)')
plt.show()

#Prediction space histrogram
plt.hist(Test.pred, bins=50, label='Neutron region = 0.5-1, gamma region=0-0.5')
plt.legend()
plt.title('CNN prediction space\n the final layers output is the logistic function, so it is bounded between 0 and 1')
plt.ylabel('Counts')
plt.xlabel('CNN prediction')
plt.show()
