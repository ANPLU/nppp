#!/usr/bin/env python3
"""
Generic data analysis for digitized waveforms
"""

import sys
import logging
import os

import configparser
import argparse

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

import dask
import dask.dataframe as dd
import dask.array as da
from dask.diagnostics import ProgressBar

import pyarrow # for saving and loading parquet files

import ast # For parsing of strings into lists, arrays and dictionaries
from distutils.util import strtobool # for str->bool conversion

import digidata
import processing

import helpers as hlp
import plotting as digplt

#                        _   _
# _____  _____ ___ _ __ | |_(_) ___  _ __  ___
#/ _ \ \/ / __/ _ \ '_ \| __| |/ _ \| '_ \/ __|
#  __/>  < (_|  __/ |_) | |_| | (_) | | | \__ \
#\___/_/\_\___\___| .__/ \__|_|\___/|_| |_|___/
#                 |_|
#
class Error(Exception):
    """Base class for exceptions in this module."""
    pass

class ParquetError(Error):
    """Exception raised for errors writing to parquet.

    Attributes:
        message -- explanation of the error
    """

    def __init__(self, message):
        self.message = message

class InputError(Error):
    """Exception raised for errors in the input.

    Attributes:
        expression -- input expression in which the error occurred
        message -- explanation of the error
    """

    def __init__(self, expression, message):
        self.expression = expression
        self.message = message


#    _        _   _
#   / \   ___| |_(_) ___  _ __  ___
#  / _ \ / __| __| |/ _ \| '_ \/ __|
# / ___ \ (__| |_| | (_) | | | \__ \
#/_/   \_\___|\__|_|\___/|_| |_|___/
#
# Actions always have the same signature and are called from the main routine.
# After adding a new function here, you have to extend the
# 'implemented_actions' dictionary below. In the config file, add "actions =
# myfunction, otherfunction" to call the functions in that order and process
# the dataframe.
# TODO: add Action: qdc for claculating QCD values

def save(ddf, run, parameters):
    """ Saves the dataframe to parquet format to the path specified by 'save'"""
    log = init_logging('save', parameters['loglevel'])
    # construct file name specified by the 'save' parameter; replace placeholder for run number
    filename = get_filename(parameters['save'], run, parameters['datapath'])
    with ProgressBar():
        log.info(f'Saving DataFrame to {filename}:')
        #Save DataFrame to disk.
        try:
            ddf.to_parquet(filename, engine='pyarrow', compression='snappy')
            # TODO consider to provide a 'schema=' argument to avoid
            # TypeError/inconsistent schemas if any of the partitions are empty
            # (after filtering) (https://github.com/dask/dask/issues/4194)
        except pyarrow.lib.ArrowCapacityError as err:
            log.error(f"Caught CapacityError exception from pyArrow while writing file to disk: {err}")
            log.error("Try reducing the partition size of the data frame (by setting 'jadaq_npart_redfac' to a lower number)")
            raise ParquetError("pyarrow.lib.ArrowCapacityError encountered")
    return ddf

def compute(ddf, run, parameters):
    """ Computes a Pandas Dataframe from a Dask DataFrame and returns result to be used in further functions """
    parameters.setdefault('compute_npartitions', '-1')
    log = init_logging('compute', parameters['loglevel'])
    return get_pddf(ddf, int(parameters['compute_npartitions']), log)

def cook(ddf, run, parameters):
    """ cooks the dataframe appending columns baseline, peak amplitude and peak index calculated from the samples """
    log = init_logging('cook', parameters['loglevel'])
    # adjust loglevel for external logger
    init_logging('sample_processing', parameters['loglevel'])

    # set default parameters in case they are not yet present
    # NOTE this modifies the dictionary and will affect the parameters passed on to ACTIONS scheduled later
    parameters.setdefault('baseline_samples', 20)
    parameters.setdefault('cfd_algorithm', 'fractional_amplitude')
    parameters.setdefault('pulse_polarity', '-') # NOTE can be a comma-separated list for each ch
    parameters.setdefault('cook_with_stats', "true")
    # CFD parameters' interpretation depends on the algorithm:
    cfd_param = {}
    if parameters['cfd_algorithm'] == 'fractional_amplitude':
        parameters.setdefault('cfd_frac', 0.3)
        cfd_param['CFD_frac'] = float(parameters['cfd_frac'])
    elif parameters['cfd_algorithm'] == 'zero_crossing':
        parameters.setdefault('cfd_frac', 0.36)
        parameters.setdefault('cfd_delay', 7)
        cfd_param['CFD_frac'] = float(parameters['cfd_frac'])
        cfd_param['CFD_delay'] = int(parameters['cfd_delay'])
    else:
        # not a valid CFD algorithm!
        log.error(f"Invalid CFD algorithm specified: '{parameters['cfd_algorithm']}'. Must be either 'fractional_amplitude' or 'zero_crossing'.")

    if "," in parameters['pulse_polarity']:
        # need to separate by channel
        polarity = [substring.strip() for substring in parameters['pulse_polarity'].split(' ') if substring.strip()]
    else:
        polarity = parameters['pulse_polarity'].strip()
    processing.sample_processing(
                                df = ddf,
                                baseline_samples = int(parameters['baseline_samples']),
                                CFD_type = str(parameters['cfd_algorithm']),
                                CFD_param = cfd_param,
                                polarity = polarity
                                )
    # if requested by user, calculate additional stats
    if strtobool(parameters['cook_with_stats']):
        # adjust loglevel for external logger
        init_logging('sample_stats', parameters['loglevel'])
        parameters.setdefault('cfd_drop_frac', 0.1)
        processing.sample_stats(
            df = ddf,
            CFD_drop_frac = float(parameters['cfd_drop_frac']),
            polarity = polarity
    )
    return ddf

def store_cfg(ddf, run, parameters):
    """ Saves the config parameters to a file in the directory given by 'confout_file' """
    # set default parameters in case they are not yet present
    parameters.setdefault('confout_file', 'config_@RunNumber@.cfg')
    log = init_logging('store_cfg', parameters['loglevel'])
    filename = get_filename(parameters['confout_file'], run, parameters['basepath'])
    config = configparser.ConfigParser()
    config['DEFAULT'] = parameters
    with open(filename, 'w') as configfile:
        config.write(configfile)
    return ddf

def store_stats(ddf, run, parameters):
    """ stores useful stats around the run into a orgtbl/csv file """
    # NOTE function assumes one-ch-per-row format
    # TODO fix so that singlerow data will work as well.
    #      Could e.g. pass data arrays to plot routine and
    #      either get them from a query for the ch #
    #      or by taking the appropriate column in the singlerow df
    log = init_logging('stats', parameters['loglevel'])
    # set default parameters used in this routine
    parameters.setdefault('stats_channels', '')
    parameters.setdefault('stats_file', 'stats_@RunNumber@.org')
    parameters.setdefault('stats_npartitions', '1')

    statsfile = get_filename(parameters['stats_file'], run, parameters['logpath'])
    chlist = [substring.strip() for substring in parameters['stats_channels'].split(' ') if substring.strip()]
    if len(chlist) == 0:
        log.warning("No channels specified to retrieve stats from. Use 'stats_channels' to specify which channels to use, e.g. '1,2'.")
        chlist.append(None)
    pdf = get_pddf(ddf, int(parameters['stats_npartitions']), log)
    stats = [] # list of dataframes with stats
    # prepare stats
    for ch in chlist:
        if not ch:
            chdf = pdf
        else:
            chdf = pdf.query(f"channel=={ch}") # TODO modify this to suit singlerow data
        # dictionary holding this channel's stats
        s = {}
        s['channel'] = ch
        s['nevts'] = len(chdf)
        # pulse amplitude
        s['amp_min']  = chdf['amplitude'].min()
        s['amp_max']  = chdf['amplitude'].max()
        s['amp_mean'] = chdf['amplitude'].mean()
        # pulse baseline
        s['baseline_min']  = chdf['baseline'].min()
        s['baseline_max']  = chdf['baseline'].max()
        s['baseline_mean'] = chdf['baseline'].mean()
        # CFD value
        s['CFD_rise_min']  = chdf['CFD_rise'].min()
        s['CFD_rise_max']  = chdf['CFD_rise'].max()
        s['CFD_rise_mean'] = chdf['CFD_rise'].mean()
        # rise time
        s['rise_time_min']  = (chdf["peak_index"]-chdf["CFD_rise"]).min()
        s['rise_time_max']  = (chdf["peak_index"]-chdf["CFD_rise"]).max()
        s['rise_time_mean'] = (chdf["peak_index"]-chdf["CFD_rise"]).mean()
        if "CFD_drop" in pdf.columns:
            s['CFD_drop_min']  = chdf['CFD_drop'].min()
            s['CFD_drop_max']  = chdf['CFD_drop'].max()
            s['CFD_drop_mean'] = chdf['CFD_drop'].mean()
            s['siglen_min']  = (chdf["CFD_drop"]-chdf["CFD_rise"]).min()
            s['siglen_max']  = (chdf["CFD_drop"]-chdf["CFD_rise"]).max()
            s['siglen_mean'] = (chdf["CFD_drop"]-chdf["CFD_rise"]).mean()
        if "min_val" in pdf.columns:
            s['min_val_min']  = chdf['min_val'].min()
            s['min_val_max']  = chdf['min_val'].max()
            s['min_val_mean'] = chdf['min_val'].mean()
        if "max_val" in pdf.columns:
            s['max_val_min']  = chdf['max_val'].min()
            s['max_val_max']  = chdf['max_val'].max()
            s['max_val_mean'] = chdf['max_val'].mean()
        # for the QDC-related plots we drop any rows that have infs or nans:
        if "qdc_ps" in pdf.columns:
            with pd.option_context('mode.use_inf_as_na', True):
                chdf = chdf.dropna(subset=['qdc_lg', 'qdc_sg', 'qdc_ps'], how='any')
            s['qdc_lg_min']  = chdf['qdc_lg'].min()
            s['qdc_lg_max']  = chdf['qdc_lg'].max()
            s['qdc_lg_mean'] = chdf['qdc_lg'].mean()
        # create dataframe with stats
        stats.append(pd.DataFrame(s, index=[0]))
    # concatenate results (if needed)
    if len(stats) == 1:
        statsdf = stats[0]
    else:
        statsdf = pd.concat(stats)
    # write to file
    log.debug(f"Writing stats to {statsfile}")
    filename, file_extension = os.path.splitext(statsfile)
    if file_extension == '.org':
        import orgtbl
        orgtbl.write_orgtbl(statsdf, statsfile)
    elif file_extension == '.csv':
        statsdf.to_csv(statsfile)
    else:
        log.error(f"Unknown file extension for stats_file ('{file_extension}'): has to be 'org' or 'csv'.")
    return ddf

def plot_rate(ddf, run, parameters):
    """ Plots information such as data rate. Requires global_ts to be loaded from jadaq data. """
    log = init_logging('plot_rate', parameters['loglevel'])
    # set default parameters used in this routine
    parameters.setdefault('plot_rate_prefix', 'plot_@RunNumber@_rate_')
    parameters.setdefault('plot_rate_channels', '')

    fnprefix = get_filename(parameters['plot_rate_prefix'], run, parameters['plotpath'])
    chlist = [substring.strip() for substring in parameters['plot_rate_channels'].split(' ') if substring.strip()]
    if len(chlist) == 0:
        log.warning("No channels specified to plot data rate for. Use 'plot_rate_channels' to specify which channels to use, e.g. '1,2'. Will integrate over all channels instead.")
        chlist.append(None)
    log.info("Preparing plots...")
    # prepare plots
    for ch in chlist:
        if ch:
            chdf = ddf.query(f"channel=={ch}")
        else:
            chdf = ddf
        # group by global time stamp
        g = chdf.groupby(chdf.global_ts).count().compute()
        # might not be correctly sorted, fix now
        g = g.reset_index().sort_values("global_ts")
        if len(g.global_ts)<2:
            log.warning("Not enought global time stamps present in data to calculate data rate")
        else:
            digplt.plot_evtrate(global_ts=g.global_ts, counts=g.evtno, channel=ch, run=run)
            plt.savefig(f"{fnprefix}_ch{ch}_evtrate.pdf")
        digplt.plot_evtvstime(global_ts=g.global_ts, counts=g.evtno, channel=ch, run=run)
        plt.savefig(f"{fnprefix}_ch{ch}_evtvstime.pdf")
    return ddf

def plot(ddf, run, parameters):
    """ Plots values calculated during cooking step. """
    # NOTE function assumes one-ch-per-row format
    # TODO fix so that singlerow data will work as well.
    #      Could e.g. pass data arrays to plot routine and
    #      either get them from a query for the ch #
    #      or by taking the appropriate column in the singlerow df
    log = init_logging('plot', parameters['loglevel'])
    # set default parameters used in this routine
    parameters.setdefault('plot_prefix', 'plot_@RunNumber@_cook_')
    parameters.setdefault('plot_npartitions', '1')
    parameters.setdefault('plot_channels', '')
    parameters.setdefault('plot_nsamples', 5)

    fnprefix = get_filename(parameters['plot_prefix'], run, parameters['plotpath'])
    chlist = [substring.strip() for substring in parameters['plot_channels'].split(' ') if substring.strip()]
    if len(chlist) == 0:
        log.warning("No channels specified to plot. Use 'plot_channels' to specify which channels to use, e.g. '1,2'.")
        chlist.append(None)
    pdf = get_pddf(ddf, int(parameters['plot_npartitions']), log)
    log.info("Preparing plots...")
    # prepare plots
    for ch in chlist:
        if ch:
            chdf = pdf.query(f"channel=={ch}") # TODO modify this to suit singlerow data
        else:
            chdf = pdf
        # plot selected samples
        if "samples" in pdf.columns:
            digplt.plot_samples(chdf.head(int(parameters['plot_nsamples'])).samples, ch, run)
            plt.savefig(f"{fnprefix}_ch{ch}_samples.pdf")
        # plot pulse amplitude
        digplt.plot_ph(chdf.amplitude, ch, run)
        plt.savefig(f"{fnprefix}_ch{ch}_amplitude.pdf")
        # plot pulse baseline
        digplt.plot_baseline(chdf.baseline, ch, run)
        plt.savefig(f"{fnprefix}_ch{ch}_baseline.pdf")
        # plot CFD value
        digplt.plot_risetime(chdf.CFD_rise.dropna(), ch, run)
        plt.savefig(f"{fnprefix}_ch{ch}_cfd_rise.pdf")
        # plot rise time
        digplt.plot_risetime((chdf["peak_index"]-chdf["CFD_rise"]).dropna(),
                           ch, run)
        plt.savefig(f"{fnprefix}_ch{ch}_risetime.pdf")
        if "CFD_drop" in pdf.columns:
            digplt.plot_siglen((chdf["CFD_drop"]-chdf["CFD_rise"]).dropna(),
                               ch, run)
            plt.savefig(f"{fnprefix}_ch{ch}_siglen.pdf")
        if "min_val" in pdf.columns:
            digplt.plot_minval(chdf.min_val, ch, run)
            plt.savefig(f"{fnprefix}_ch{ch}_minval.pdf")
        if "max_val" in pdf.columns:
            digplt.plot_maxval(chdf.max_val, ch, run)
            plt.savefig(f"{fnprefix}_ch{ch}_maxval.pdf")
        # for the QDC-related plots we drop any rows that have infs or nans:
        if "qdc_ps" in pdf.columns:
            with pd.option_context('mode.use_inf_as_na', True):
                chdf = chdf.dropna(subset=['qdc_lg', 'qdc_sg', 'qdc_ps'], how='any')
            digplt.plot_qdc(chdf["qdc_lg"], ch, run)
            plt.savefig(f"{fnprefix}_ch{ch}_qdc_lg.pdf")
            digplt.plot_qdc(chdf["qdc_sg"], ch, run)
            plt.savefig(f"{fnprefix}_ch{ch}_qdc_sg.pdf")
            digplt.plot_qdcps(chdf["qdc_ps"], ch, run)
            plt.savefig(f"{fnprefix}_ch{ch}_qdc_ps.pdf")
            digplt.plot_qdcps2d(chdf["qdc_ps"], chdf["qdc_lg"], ch, run)
            plt.savefig(f"{fnprefix}_ch{ch}_qdclg_vs_ps.pdf")
        # plot mean values against time (if time stamp available)
        if "global_ts" in pdf.columns:
            # group by global time stamp
            g = chdf.groupby(chdf.global_ts).mean()
            # might not be correctly sorted, fix now
            g = g.reset_index().sort_values("global_ts")
            if not g.empty:
                digplt.plot_valvstime(g.global_ts, g.baseline, ch, run)
                plt.ylabel("mean baseline value")
                plt.savefig(f"{fnprefix}_ch{ch}_meanbaseline_vs_time.pdf")
                digplt.plot_valvstime(g.global_ts, g.amplitude, ch, run)
                plt.ylabel("mean amplitude value")
                plt.savefig(f"{fnprefix}_ch{ch}_meanamplitude_vs_time.pdf")
        plt.close('all') # close all figure windows to save memory
    return ddf

def filter(ddf, run, parameters):
    """ Filters dataframe. NOTE: assumes one-ch-per-row format. """
    log = init_logging('filter', parameters['loglevel'])
    # set default parameters used in this routine
    parameters.setdefault('filter_amp_thresh', '')
    parameters.setdefault('filter_channels', '')
    parameters.setdefault('filter_query', '')
    chlist = [substring.strip() for substring in parameters['filter_channels'].split(' ') if substring.strip()]
    query = []
    if len(chlist) == 0:
        log.warning("No channels specified to filter in 'filter_channels'. Applying same criteria to all channels.")
        if parameters['filter_amp_thresh']:
            query.append(f"amplitude > {parameters['filter_amp_thresh']}")
    else:
        if parameters['filter_amp_thresh']:
            q = []
            thresh = [substring.strip() for substring in parameters['filter_amp_thresh'].split(' ') if substring.strip()]
            for idx,ch in enumerate(chlist):
                try:
                    q.append(f"channel=={ch} and amplitude > {thresh[idx]}")
                except IndexError:
                    raise InputError('filter_amp_thresh', "Not enough values specified")
            query.append(" or ".join([f"({s})" for s in q])) # conditions liked by OR and in parenthesis
    # now link all conditions together to a final query
    query = " and ".join([f"({s})" for s in query])
    # add filter string specified in config
    if parameters['filter_query']:
        if not query:
            query = parameters['filter_query']
        else:
            # join statements together
            query = " and ".join([query, parameters['filter_query']])
    if not query:
        log.warning("No filter criteria to apply!")
        return ddf
    else:
        log.debug(f"Final query: {query}")
    try:
        ddf = ddf.query(query)
    except ValueError as err:
        raise InputError('query', f"Dask dataframe query raised ValueError. Please make sure all fields in the query '{query}' are present in the dataframe.\n Original error message: {err}")
    return ddf

def drop(ddf, run, parameters):
    """ Drops all columns of the dataframe given by a comma-seperated list of their labels specified in the `drop_columns` parameter. """
    parameters.setdefault('drop_columns', 'samples')
    # extract the column labels from the parameter:
    c = [substring.strip() for substring in parameters['drop_columns'].split(',') if substring.strip()]
    ddf = ddf.drop(labels=c, axis=1) # drop coluns; newer Dask versions support 'column' argument which is identical
    return ddf

def interact(ddf, run, parameters):
    """ will start an interactive iPython shell """
    log = hlp.init_logging('interact', parameters['loglevel'])
    log.info(" Interactive IPython shell ")
    log.info(" ========================= ")
    log.info(" Quick command usage:")
    log.info("  - 'who' or 'whos' to see all (locally) defined variables")
    log.info("  - data is stored in Dask DataFrame: 'ddf'")
    log.info("  - configuration is stored in dictionary: 'parameters'")
    log.info("  - to generate plots, run for example 'plot( ddf, run, parameters )'")
    log.info("  - if the plots are shown only as black area, run '%gui qt'")
    log.info("  - to make cmd prompt usable while plots are shown use 'plt.ion()' for interactive mode")
    log.info("  - use 'exit' to continue analysis")
    import IPython
    IPython.embed()
    return ddf

def psd(ddf, run, parameters):
    """ calculates PSD values based on long/short gate integrations for all active channels """
    log = hlp.init_logging('psd', parameters['loglevel'])
    # set default parameters in case they are not yet present
    # NOTE this modifies the dictionary and will affect the parameters passed on to ACTIONS scheduled later
    parameters.setdefault('psd_lg', 500)
    parameters.setdefault('psd_sg', 30)
    parameters.setdefault('psd_pre_trig', 25)
    parameters.setdefault('psd_lg_offset', 0)
    parameters.setdefault('psd_sg_offset', 0)

    for chidx, col in enumerate([col for col in ddf.columns if col.startswith('sample')]):
        try:
            suffix = "_"+col.split('_')[1] # save channel number suffix as string.
        except IndexError:
            suffix = "" # when jadaq_singlerow == False, suffix is saved as empty string.
        log.info(f"Processing channel: {suffix}: lg={int(parameters['psd_lg'])}, sg={int(parameters['psd_sg'])}")

        processing.pulse_integration(
            df = ddf,
            lg = int(parameters['psd_lg']),
            sg = int(parameters['psd_sg']),
            pre_trig = int(parameters['psd_pre_trig']),
            lg_offset = float(parameters['psd_lg_offset']),
            sg_offset = float(parameters['psd_sg_offset']),
            suffix = suffix
        )
    return ddf

def tof(ddf, run, parameters):
    """ calculates ToF values between specified N and YAP channels """
    # NOTE: call to tof_processing currently only works with single row data.
    log = hlp.init_logging('tof', parameters['loglevel'])
    # adjust loglevel for external logger
    init_logging('tof_processing', parameters['loglevel'])

    if len([col for col in ddf.columns if col.startswith('sample')])<2:
        log.error("Dataframe has less than two 'samples' columns -- is data really in single row format? Specify 'jadaq_singlerow = true' to load datafile in correct format for ToF calculations")
        return ddf

    # set default parameters in case they are not yet present
    # NOTE this modifies the dictionary and will affect the parameters passed on to ACTIONS scheduled later
    parameters.setdefault('tof_nchannel', 0)
    parameters.setdefault('tof_ychannel', 1)

    processing.tof_processing(
        df = ddf,
        Nchannel = int(parameters['tof_nchannel']),
        Ychannel = ast.literal_eval(parameters['tof_ychannel']),
    )
    return ddf

def unknown(ddf, run, parameters):
    log = init_logging('analysis')
    raise InputError("Action", "Unknown action")
#     _ _      _            __   _                 _
#  __| (_) ___| |_    ___  / _| (_)_ __ ___  _ __ | |
# / _` | |/ __| __|  / _ \| |_  | | '_ ` _ \| '_ \| |
#| (_| | | (__| |_  | (_) |  _| | | | | | | | |_) | |_
# \__,_|_|\___|\__|  \___/|_|   |_|_| |_| |_| .__/|_(_)
#                                           |_|
#            _   _
#  __ _  ___| |_(_) ___  _ __  ___
# / _` |/ __| __| |/ _ \| '_ \/ __|
#| (_| | (__| |_| | (_) | | | \__ \
# \__,_|\___|\__|_|\___/|_| |_|___/
#
# NOTE new functions have to be added to this dictionary to be callable
implemented_actions = {'save': save,
                       'compute': compute,
                       'cook': cook,
                       'drop' : drop,
                       'filter': filter,
                       'store_cfg': store_cfg,
                       'store_stats': store_stats,
                       'plot': plot,
                       'plot_rate': plot_rate,
                       'interact': interact,
                       'psd': psd,
                       'tof': tof
                       }


# _          _
#| |__   ___| |_ __   ___ _ __ ___
#| '_ \ / _ \ | '_ \ / _ \ '__/ __|
#| | | |  __/ | |_) |  __/ |  \__ \
#|_| |_|\___|_| .__/ \___|_|  |___/
#             |_|
#
def get_filename(pattern, run, basepath):
    """ construct filename from pattern, run number and the basepath (for relative paths) """
    log = init_logging('analysis')

    # construct file name specified by the 'pattern' argument; replace placeholder for run number
    runnr = str(run).zfill(5)
    try:
        filename = hlp.ireplace("@RunNumber@", runnr, pattern)
    except EOFError:
        log.error("No reference to run number ('@RunNumber@') found in file name template: "+pattern)
        filename = pattern
    # resolve relative paths if needed
    if not os.path.isabs(filename):
        filename=os.path.normpath(os.path.join(basepath,filename))
    return filename

def init_logging(logname, loglevel=None):
    try:
        log = hlp.init_logging(logname, loglevel)
    except TypeError:
        raise InputError(loglevel, "Log level not recognized: please use 'DEBUG', INFO', 'WARNING' or 'ERROR'")
    return log

def get_pddf(ddf, npartitions, log):
    """ returns a pandas dataframe from npartitions of a dask dataframe """
    # check that we have a dask dataframe:
    if isinstance(ddf, dask.dataframe.core.DataFrame):
        log.info(f"Preparing {npartitions} partitions of the dask dataframe.")
        with ProgressBar():
            # compute a number of partitions of the dataframe into pandas dataframe
            if ddf.npartitions > 1:
                pdf = ddf.partitions[:npartitions].compute()
            else:
                pdf = ddf.compute()
    else:
        pdf = ddf # already pandas
    return pdf

#                 _        ____
# _ __ ___   __ _(_)_ __  / /\ \
#| '_ ` _ \ / _` | | '_ \| |  | |
#| | | | | | (_| | | | | | |  | |
#|_| |_| |_|\__,_|_|_| |_| |  | |
#                         \_\/_/
#

def main(argv=None):
    log = init_logging('analysis',"info")

    # command line argument parsing
    argv = sys.argv
    progName = os.path.basename(argv.pop(0))
    parser = argparse.ArgumentParser(
        prog=progName,
        description=
        "Generic data analysis for digitized waveforms" # TODO: check accuracy some time later again
    )
    parser.add_argument(
        "-l",
        "--log-level",
        help=
        "Sets the verbosity of log messages where LEVEL is either debug, info, warning or error",
        metavar="LEVEL")
    parser.add_argument("--log-file", help="Save output log to specified file", metavar="FILE")
    parser.add_argument('--option', '-o', action='append',
                        metavar="NAME=VALUE",
                        help="Specify further options such as 'short_gate=30'. This switch be specified several times for multiple options or can parse a comma-separated list of options. This switch overrides any config file options.")
    parser.add_argument("-c", "--conf-file", "--config", help="Load specified config file with global and task specific variables", metavar="FILE")
    parser.add_argument("-lgbk", "--logbook", help="Load additional run-specific variables from logbook in text table format (csv or org)", metavar="FILE")
    parser.add_argument("task",
                        help="Which task to execute; task names are arbitrary and can be set up by the user; they determine e.g. the config section and default output file names. If no config file is specified (or the relevant section does not give any ACTIONS), these will be taken as the ACTIONS to execute and can be a comma-separated list in that case. Possible ACTIONS: "+', '.join(map(str, implemented_actions.keys())))
    parser.add_argument("runs", help="The runs to be analyzed; can be a list of single runs and/or a range, e.g. 1056-1060.", nargs='*')

    # parse the arguments
    args = parser.parse_args(argv)
    
    # set up writing to log file if requested on command line
    if args.log_file:
        handler_file = logging.FileHandler(args.log_file)
        handler_file.setFormatter(formatter)
        handler_file.setLevel(numeric_level)
        log.addHandler(handler_file)

    # update the log-level
    if args.log_level:
        log = init_logging('analysis', args.log_level)

        
    log.debug("Command line arguments used: %s ", args)
    log.debug("Libraries loaded:")
    log.debug("   - Matplotlib version {}".format(matplotlib.__version__))
    log.debug("   - Pandas version {}".format(pd.__version__))
    log.debug("   - Dask version {}".format(dask.__version__))
    log.debug("   - Numpy version {}".format(np.__version__))
    log.debug(f"   - PyArrow version: {pyarrow.__version__}")

    # parse run list
    runs = list()
    for runnum in args.runs:
        try:
            log.debug("Parsing run-range argument: '%s'", runnum)
            runs = runs + hlp.parseIntegerString(runnum)
        except ValueError:
            log.error("The list of runs contains non-integer and non-range values: '%s'", runnum)
            return 2

    if not runs:
        log.error("No run numbers were specified. Please see '"+progName+" --help' for details.")
        return 2

    if len(runs) > len(set(runs)): # sets items are unique
        log.error("At least one run is specified multiple times!")
        return 2

    # dictionary keeping parameters; set some minimal default config values that will (possibly) be overwritten by the config file
    parameters = {
        "basepath":"./",
        "datapath":"./data",
        "load":"@RunNumber@/",
        "save":"@RunNumber@/",
        "logpath":"./logs",
        "plotpath":"./plots",
        "loglevel": args.log_level if args.log_level else "info",
        # loading data
        "jadaq_npart_redfac":20,   # reduce number of partitions by a factor (can speed up processing of DataFrame)
        "jadaq_singlerow": "false",# load each evt on a single row if true; if false, 'transpose' channel data into separate rows for individual channels
        "jadaq_global_ts": "true"  # add global time stamp based on DAQ PC's clock to DataFrame
    }

    # read in config file if specified on command line
    if args.conf_file:
        config = configparser.ConfigParser(interpolation=configparser.ExtendedInterpolation())
        # local variables useful in the context of the config; access using %(NAME)s in config
        config.set("DEFAULT", "HOME",str(os.environ.get('HOME')))
        try:
            if not config.read([args.conf_file]): # loads global defaults
                log.error("Could not read config file '%s'!", args.conf_file)
                return 1
            # merge with defaults and create final set of configuration parameters
            if config.has_section(args.task):
                parameters.update(dict(config.items(args.task)))
            else:
                log.warning("Config file '%s' is missing a section [%s]!", args.conf_file, args.task)
                parameters.update(dict(config.items("DEFAULT"))) # just use default values
            log.info("Loaded config file %s", args.conf_file)
        except configparser.InterpolationMissingOptionError as err: # if interpolation during config parsing fails
            log.error('Bad value substitution in config file '+str(args.conf_file)+ ": missing '%s' key in section [%s] for option '%s'."%(err.reference, err.section, err.option))
            return 1
    else:
        log.warning("No config file specified")
        #TODO if no config file then set args.task -> action(s)

    # update log-level from config (if not overriden on cmdline)
    log = init_logging('analysis', parameters['loglevel'].upper())
    # TODO add a log-file handler if fn given in cfg (if not already done due to cmdline argument)

    # Parse option part of the  argument here -> overwriting config options
    if args.option is None:
        log.debug("Nothing to parse: No additional config options specified through command line arguments. ")
    else:
        try:
            # now parse any options given through the -o cmd line switch
            cmdoptions = dict(opt.strip().split('=', 1) for optlist in args.option for opt in optlist.split(',')) # args.option is a list of lists of strings we need to split at every '='
        except ValueError:
            log.error( "Command line error: cannot parse --option argument(s). Please use a '--option name=value' format. ")
            return 2
        for key in cmdoptions: # and overwrite our current config settings
            log.debug( "Parsing cmd line: Setting "+key+" to value '"+cmdoptions[key]+"', possibly overwriting corresponding config file option")
            parameters[key.lower()] = cmdoptions[key]

    # load logbook
    if args.logbook:
        filename, file_extension = os.path.splitext(args.logbook)
        if file_extension == 'org':
            import orgtbl
            lgbk = orgtbl.read_orgtbl(args.logbook)
        elif file_extension == 'csv':
            lgbk = pd.read_csv(args.logbook)
        else:
            log.error(f"Unknown file extension for logbook ('{file_extension}'): has to be 'org' or 'csv'.")
            return 3

    #create substructure of output folders
    for ipath in ("logpath","plotpath","datapath"):
        if not os.path.isabs(ipath):
            parameters[ipath]=os.path.normpath(os.path.join(parameters["basepath"],parameters[ipath]))
        if not os.path.isdir(parameters[ipath]):
            os.makedirs(parameters[ipath])
            log.info("Create path for " + ipath + " to " + parameters[ipath])

    log.debug( "Our final config:")
    for key, value in parameters.items():
        log.debug (f"     {key} = {value}")

    # determine the actions to perform on the data
    try:
        actions = [substring.strip() for substring in parameters['actions'].split(' ') if substring.strip()]
    except KeyError:
        # no actions specified in config file or no config file given
        log.warning("No 'ACTIONS' specified in config file (or no config file given). Will take 'task' argument as ACTIONS.")
        actions = [substring.strip() for substring in args.task.split(',') if substring.strip()]

    log.info("Scheduled the following actions: "+', '.join(map(str, actions)))
    log.info("Will now start processing the following run(s): "+', '.join(map(str, runs)))
    if not "save" in actions:
        log.warning("No 'save' ACTION scheduled -- data will not be saved to disk!")
    for run in runs:

        log.info (f"Now processing run {run}..")
        # TODO join all configs for this particular run (parameters and logbook)

        # adjust loglevel for digidata logger
        init_logging('digidata', parameters['loglevel'])

        # load the file specified by the 'load' parameter; replace placeholder for run number
        filename = get_filename(parameters['load'], run, parameters['datapath'])
        # load file
        if filename.endswith(".h5"):
            # dealing with a file produced by jadaq
            ddf = digidata.read_jadaq(filename,
                                redfacnpart = float(parameters["jadaq_npart_redfac"]),
                                singlerow = strtobool(parameters["jadaq_singlerow"]),
                                append_gts = strtobool(parameters["jadaq_global_ts"])
            )
        else:
            parameters.setdefault('ignore_columns', "") # default: do not ignore any columns when loading data from file
            ddf = digidata.read_parquet(filename,
                                        ignore_cols=[s.strip() for s in parameters['ignore_columns'].split(',') if s.strip()])
        log.debug(f"Dataframe has columns: {ddf.columns}")

        # perform all scheduled actions of the loaded data frame
        for a in actions:
            log.info(f"Executing ACTION: {a}")
            fcn = implemented_actions.get(a, unknown)
            try:
                ddf = fcn(ddf, run, parameters)
                log.debug(f"Dataframe has columns: {ddf.columns}")
            except InputError as err:
                log.error(f"Input error in expression '{err.expression}' encountered while running '{a}':")
                log.error(f"    {err.message}")
                break
            except ParquetError as err:
                log.error(f"Error with parquet file encountered running '{a}': {err.message}")
                break
            #TODO: add error catcher for AttributeError

    log.info("Analysis done!")
    if log.error.counter>0:
        log.warning("There were "+str(log.error.counter)+" error messages reported")

if __name__ == "__main__":
    sys.exit(main())
